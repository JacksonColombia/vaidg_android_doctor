package com.vaidg.pro.pojo.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ali on 1/19/2018.
 */

public class ChatEncryptDecryptPublicKey implements Serializable
{
  @SerializedName("message")
  @Expose
  private String message;
  @SerializedName("data")
  @Expose
  private ChatPublicKey data;


  public ChatPublicKey getData() {
    return data;
  }

  public String getMessage() {
    return message;
  }

  public class ChatPublicKey implements Serializable {
    @SerializedName("publickKey")
    @Expose
    private String publickKey;

    public String getPublickKey() {
      return publickKey;
    }
  }
}
