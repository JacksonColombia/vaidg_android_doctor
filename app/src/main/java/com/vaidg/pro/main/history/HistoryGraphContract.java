package com.vaidg.pro.main.history;

import com.github.mikephil.charting.data.BarEntry;
import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.pojo.history.HistoryPojo;
import com.vaidg.pro.pojo.history.HistoryWeekPojo;

import java.util.ArrayList;

/**
 * <h1>HistoryGraphContract</h1>
 * <p>This contract define interface with required methods for view and presenter</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @see HistoryGraphActivity
 * @see HistoryGraphPresenterImple
 * @since 02/07/2020
 **/
public interface HistoryGraphContract {

    interface View extends BaseView {
        /**
         * <p>This method is used to initialize views</p>
         */
        void initViews();

        /**
         * <p>This method is provide data for displaying it in current tab bar chart.</p>>
         *
         * @param wholeBarEntries the list of {@link BarEntry} objects
         * @param highestPosition the list of {@link Integer} objects
         * @param apiFormatDates  the list of {@link String} objects
         * @param tabFormatDates  the list of {@link String} objects
         * @param days            the list of {@link String} objects
         */
        void setTabBarChart(ArrayList<ArrayList<BarEntry>> wholeBarEntries, ArrayList<Integer> highestPosition, ArrayList<String> apiFormatDates, ArrayList<String> tabFormatDates, ArrayList<String> days);

        /**
         * <p>This method is provide data for dispaly list of booking and total earnings.</p>
         *
         * @param bookings the list of {@link Booking} objects
         * @param total    the total amount of all bookings
         */
        void onSuccessBookingHistory(ArrayList<Booking> bookings, String total);

        /**
         * <p>This method is indicate API is failed.</p>
         */
        void onFailure();

        /**
         * This method is display passed error message in snack bar.
         *
         * @param error the error message
         */
        void showError(String error);
    }

    interface Presenter extends BasePresenter {

        /**
         * <p>This method is make api call for get booking history data by week.</p>
         */
        void getBookingHistoryByWeek();

        /**
         * <p>This method is make api call for get history for selected date.</p>
         *
         * @param selectedDate  the selected date
         * @param isCurrentWeek the true if from current week
         */
        void getHistory(String selectedDate, boolean isCurrentWeek);

        /**
         * <p>This method is take weekly history data and prepare it for displaying in bar chart.</p>
         *
         * @param data the weekly history data {@link HistoryWeekPojo} object.
         */
        void prepareChartData(String data);

        /**
         * <p>This method is take booking history data and prepare it for displaying in list.</p>
         *
         * @param data the booking history data {@link HistoryPojo} object
         */
        void prepareBookingHistoryData(String data);

    }

}
