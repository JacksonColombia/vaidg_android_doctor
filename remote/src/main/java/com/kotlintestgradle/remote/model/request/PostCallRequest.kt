package com.kotlintestgradle.remote.model.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author 3Embed
 *
 * model class for login request class
 *
 * @since 1.0 (23-Aug-2019)
 */
data class PostCallRequest(
        @Expose
        @SerializedName("type") // audio or video
        val type: String,
        @Expose
        @SerializedName("room")
        val room: String,
        @Expose
        @SerializedName("to")
        val to: ArrayList<String>,
        @Expose
        @SerializedName("bookingId")
        val bookingId: String,
        @Expose
        @SerializedName("recording")
        val recording: String,
        @Expose
        @SerializedName("sessionId")
        val sessionId: String,
        @Expose
        @SerializedName("width")
        val width: Int,
        @Expose
        @SerializedName("height")
        val height: Int
)