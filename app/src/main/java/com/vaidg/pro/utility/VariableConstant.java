package com.vaidg.pro.utility;


import android.Manifest;
import android.os.Build;

import com.amazonaws.regions.Regions;
import com.vaidg.pro.BuildConfig;

import java.util.HashMap;
import javax.crypto.spec.IvParameterSpec;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>VariableConstant</h1>
 * The class that contain all varible contant field for whole app
 */

public class VariableConstant {
    /*                          Changable Field           */
    public static final String AUTHORIZATION = "Authorization";
    public static final String LAN = "lan";
    public static String RAZORPAY_KEY = "";
    public static final String BASE_AUTH_USERNAME = BuildConfig.BASIC_AUTH_USERNAME;
    public static final String BASE_AUTH_PASSWORD = BuildConfig.BASIC_AUTH_USERNAME;

    /*                           App details     */
    public static final String TERMS_OF_SERVICE = BuildConfig.TERMS_LINK;
    public static final String LEGAL = "https://admin.service-genie.xyz/supportText/provider/en_termsAndConditions.php";
    public static final String PRIVACY_POLICY = BuildConfig.PRIVECY_LINK;
    public static final String WEB_SITE_LINK_SHOWING = "www.vaidg.com";
    public static final String WEB_SITE_LINK = "https://www.vaidg.com/";
    public static final String FACEBOOK_LINK = "https://www.facebook.com/appscrip/";
    public static final String FACEBOOK_PAGE_ID = "appscrip";
    public static final String TWITTER_LINK = "appscrip";
    public static final String YOUTUBE_PAGE = "https://www.youtube.com/appscrip";

    public static final String GOOGLE_API_KEY = BuildConfig.SERVER_KEY;
    public static final int ZERO = 0;
    public static final int DUMMY_ARRAY_SIZE = 3;
    public static final int LAST_TEXT_EDIT = 0;
    public static final int DELAY = 1000;
    public static final int REDUCED_DELAY = 500;
    public static final String SEARCH_LOC_LAT = "search loc lat";
    public static final String SEARCH_LOC_LONG = "search loc long";

    /*                           Sign up key constants     */
    public static final String HTTP = "http";
    public static final String SIGNUP_PROFILE_PIC = "profilePic";
    public static final String CLINIC_LOGO = "clinicLogo";
    public static final String CLINIC_PHOTOS = "clinicPhotos";
    public static final String SPECIALIZATION_PIC_UPLOAD = "specializationPicUpload";
    public static final String SPECIALIZATION_VIDEO_UPLOAD = "specializationVideoUpload";
    public static final String SPECIALIZATION_DOCUMENT_UPLOAD = "specializationDocumentUpload";
    public static final String IS_DISPLAY = "is_display";
    public static final String DATA = "data";
    public static final int PROVIDER_INDEPENDENT_DOCTOR = 1;
    public static final int PROVIDER_UNDER_HOSPITAL = 2;

    /*               preference name and Parent folder in phone*/
    public static final String PREF_NAME = BuildConfig.PREF_NAME;
    public static final String PARENT_FOLDER = BuildConfig.PARENT_FOLDER;

    public static final String MIXPANEL_TOKEN = "f85f4ed5518a9980140c8ab30f034b6c";

    /*                 Amazon Details              */
    public static String COGNITO_TOKEN = "";
    public static String COGNITO_POOL_ID = BuildConfig.AMAZON_POOL_ID;
    public static final Regions AMAZON_REGION = Regions.AP_SOUTH_1;
    public static final String AMAZON_BASE_URL = BuildConfig.AMAZON_BASE_URL;
    public static String BUCKET_NAME = BuildConfig.AMAZON_BUCKET_NAME;
    public static final int GALLERY_CODE = 124;
    public static final int CAMERA_CODE = 123;
    public static final long PROFILE_VIDEO_SIZE = 30 * 1024 * 1024;
    public static final long DOCUMENT_SIZE = 5 * 1024 * 1024;
    public static final int PICKFILE_RESULT_CODE = 365;
    public static final int REQUEST_CODE_CONSULTATION = 985;
    public static final int REQUEST_CODE_EDUCATION = 561;
    public static final int REQUEST_CODE_YOE = 989;
    public static final int REQUEST_CODE_FIND_HOSPITAL = 101;
    public static final int REQUEST_CODE_OWN_CLINIC = 1102;
    ;
    /*                         Custom Intent Action            */
    public static final String INTENT_ACTION_REFRESH_BOOKING = "com.vaidg.provider.refreshbooking";
    public static final String INTENT_ACTION_CANCEL_BOOKING = "com.vaidg.provider.cancelbooking";
    public static final String INTENT_ACTION_BOOKING_UPDATED = "com.vaidg.provider.bookingupdated";
    public static final String INTENT_ACTION_NEW_CHAT = "com.vaidg.provider.newchat";
    public static final String INTENT_ACTION_MAKE_ONLINE = "com.vaidg.provider.makeonline";
    public static final String INTENT_ACTION_MAKE_OFFLINE = "com.vaidg.provider.makeoffline";
    public static final String INTENT_ACTION_PROFILE_ACTIVATION = "com.vaidg.provider.profileactivation";
    public static final String INTENT_ACTION_GPS_OFF = "com.vaidg.provider.gpsoff";
    public static final String INTENT_ACTION_CALL = "com.vaidg.provider.call";
    /*                          AccountManager           */
    public static final String ACCOUNT_TYPE = BuildConfig.APPLICATION_ID;
    public static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";
    /*                                Play store link      */
    public static final String PLAY_STORE_LINK = BuildConfig.ANDROID_PLAYSTORE_LINK  + BuildConfig.APPLICATION_ID;
    /*                          File Names      */
    public static final String PROFILE_FILE_NAME = "Profile";
    public static final String WORK_IMAGE_FILE_NAME = "WorkImage";
    public static final String DOCUMENT_FILE_NAME = "Document";
    public static final String BANK_FILE_NAME = "Bank";

    /*                          Constant for Every App            */
    /*                        App details    */
    public static final int USER_TYPE = 2;
    public static final String DEVICE_TYPE = "2";
    public static final String APP_VERSION = BuildConfig.VERSION_NAME;
    public static final String DEVICE_MODEL = android.os.Build.MODEL;
    public static final String DEVICE_MAKER = android.os.Build.MANUFACTURER;
    public static final String OS_VERSION = Build.VERSION.RELEASE;
    /*                    Folder Names */
    public static final String CROP_PIC_DIR = PARENT_FOLDER + "/Media/Images/CropedPics";
    public static final String PROFILE_PIC_DIR = PARENT_FOLDER + "/Media/Images/ProfilePictures";
    public static final String DOCUMENT_PIC_DIR = PARENT_FOLDER + "/Media/Images/Documents";
    public static final String SIGNATURE_PIC_DIR = PARENT_FOLDER + "/Media/Images/Signatures";
    public static final String BANK_PIC_DIR = PARENT_FOLDER + "/Media/Images/Signatures";
    public static final String DOWNLOAD_FILE_DIR = PARENT_FOLDER + "/TempDownload";
    /*                      Response Code*/
    public static final int SUCCESS_RESPONSE = 200;
    public final static int BAD_REQUEST = 400;
    public final static int NOT_FOUND = 404;
    public final static int RAZOR_NOT_FOUND = 204;
    public static final int SESSION_EXPIRED = 406;
    public static final int SESSION_LOGOUT = 401;
    public final static int INTERNAL_SERVER_ERROR = 500;
    public final static int UNAUTHORIZATION = 498;
    public final static int FORBIDDEN = 403;
    public final static int LENGTH_REQUIRED = 411;
    public final static int PHONENUMBER_NOTREGISTER = 406;
    public final static int EMAIL_NOTREGISTER = 409;
    public final static int EXCEED_PASSWORD_LIMIT = 429;
    // public static final int NOT_ACCEPTABLE = 406;//440
    public static final int SESSION_NoDues = 410;
    public static final int CONFLICT = 409;
    public static final int PRECONDITION_FAILED = 412;

    public static final String RESPONSE_CODE_SUCCESS = "200";
    public static final String RESPONSE_CODE_NO_CONTENT = "204";
    public static final String RESPONSE_CODE_INTERNAL_SERVER_ERROR = "500";
    public static final String RESPONSE_CODE_NOT_FOUND = "404";
    public static final String RESPONSE_CODE_NO_ACCOUNT_FOUND = "401";
    public static final String RESPONSE_CODE_ALREADY_IN_CALL = "409";
    public static final String RESPONSE_CODE_NO_STRIPE_FOUND = "400";
    public static final String RESPONSE_CODE_TOKEN_EXPIRE = "440"; //Need to call refresh token
    public static final String RESPONSE_CODE_INVALID_TOKEN = "498"; // logout
    /*                     Provider stats*/
    public static final String ONLINE_STATUS = "3";
    public static final String OFFLINE_STATUS = "4";
    public static final String TIME_OUT = "5";
    public static final String PROFLIE_ACTIVE_STATUS = "1";
    public static final String PROFILE_IN_ACTIVE_STATUS = "0";
    /*                         CATEGORIES TYPE   (Fixed and Hourly)   */
    public static final String CATEGORY_FIXED_NON_EDIT = "1";
    public static final String CATEGORY_FIXED_HOURLY_NON_EDIT = "2";
    public static final String CATEGORY_HOURLY_NON_EDIT = "3";
    public static final String CATEGORY_HOURLY_EDIT = "4";
    public static final String CATEGORY_FIXED_EDIT = "5";
    public static final String CATEGORY_FIXED_HOURLY_EDIT = "6";
    /*                        BookingStatus         */
    public static final String TIMER_STARTED = "1";
    public static final String TIMER_PAUSED = "0";
    public static final String NEW_BOOKING = "1";
    public static final String RECEIVED = "2";
    public static final String ACCEPT = "3";
    public static final String REJECT = "4";
    public static final String BOOKING_EXPIRED = "5";
    public static final String ON_THE_WAY = "6";
    public static final String ARRIVED = "7";
    public static final String JOB_TIMER_STARTED = "8";
    public static final String JOB_TIMER_COMPLETED = "9";
    public static final String JOB_COMPLETED_RAISE_INVOICE = "10";
    public static final String CANCELLED_BY_PROVIDER = "11";
    public static final String CANCELLED_BY_CUTOMER = "12";
    public static final String JOB_TIMER_INCOMPLETE = "15";
    public static final String ACTIVE_BIDS = "17";

    /*                          booking type              */
    public static final String CALL_TYPE_IN = "1";
    public static final String CALL_TYPE_OUT = "2";
    public static final String CALL_TYPE_TELE = "3";

    /*                          Request code              */
    public static final int REQUEST_CODE_GALLERY = 101;
    public static final int REQUEST_CODE_TAKE_PICTURE = 102;
    public static final int REQUEST_CODE_DOCUMENT = 100;
    public static final int REQUEST_CODE_CROP_IMAGE = 103;
    public static final int REQUEST_CODE_CITY = 104;
    public static final int REQUEST_CODE_LOCATION = 105;
    public static final int REQUEST_CODE_SUB_CATEGORY = 106;
    public static final int REQUEST_CODE_CATEGORY_DOCUMENT = 107;
    public static final int REQUEST_CODE_ADDRESS = 108;
    public static final int REQUEST_CODE_REPEAT_MODE = 109;
    public static final int REQUEST_CODE_DAYS = 110;
    public static final int REQUEST_CODE_MONTH = 111;
    public static final int REQUEST_CODE_SLOT_DURATION = 112;
    public static final int REQUEST_CODE_GPS = 113;
    public static final int REQUEST_CODE_SERVICE_CHANGED = 114;
    public static final int REQUEST_CODE_SPECIALIZATION = 124;
    public static final int REQUEST_CODE_SPECIALIZATION_CATEGORY = 224;
    public static final int REQUEST_CODE_DEGREE = 115;
    public static final int REQUEST_CODE_UPDATE_FEES = 125;
    public static final String[] STORAGE_CAMERA_PERMISSION = {"android.permission.CAMERA","android.permission.WRITE_EXTERNAL_STORAGE","android.permission.READ_EXTERNAL_STORAGE"};
    public static final String READ_PHONE_STATE_PERMISSION = Manifest.permission.READ_PHONE_STATE;
    //    public static final String[] SMS_PERMISSION ={Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS};
    public static final String[] CALL_PERMISSIONS = {
        "android.permission.RECORD_AUDIO", "android.permission.MODIFY_AUDIO_SETTINGS",
        "android.permission.INTERNET","android.permission.CAMERA","android.permission.WRITE_EXTERNAL_STORAGE","android.permission.READ_EXTERNAL_STORAGE"
    };
    private static final String PARENT_AMAZON_FOLDER = "iserve2.0/Provider/";
    public static final String PROFILE_PIC = PARENT_AMAZON_FOLDER + "ProfilePics";
    public static final String DOCUMENTS = PARENT_AMAZON_FOLDER + "Documents";
    public static final String WORK_IMAGE = PARENT_AMAZON_FOLDER + "WorkImage";
    public static final String SIGNATURE_UPLOAD = PARENT_AMAZON_FOLDER + "Signature";
    public static final String BANK_PROOF = PARENT_AMAZON_FOLDER + "Bankproof";
    public static final String SIGNUPPROFILEIMAGESFOLDER = PARENT_AMAZON_FOLDER + "SignupProfileImages";
    public static String LiveTrackBookingPid = "";
    public static String LANGUAGE = "en";
    public static String PLATFORM = "ANDROID";
    public static String CUSTOMER = "CUSTOMER";
    public static String LANGUAGENAME = "";
    /*                         for refreshing api     */
    public static boolean IS_NETWORK_ERROR_SHOWED = false;
    public static boolean IS_APPLICATION_RUNNING = false;
    public static boolean IS_MYBOOKING_OPENED = false;
    public static boolean IS_ACCEPTEDBOOKING_OPENED = false;
    public static boolean IS_BOOKING_UPDATED = false;
    public static boolean IS_WALLET_UPDATED = false;
    public static boolean IS_BOOKING_ACCEPTED = false;
    public static boolean IS_BID_BOOKING_ACCEPTED = false;
    public static boolean IS_MY_LIST_EDITED = false;
    public static boolean IS_PROFILE_PHOTO_UPDATED = false;
    public static boolean IS_PROFILE_EDITED = false;
    public static boolean IS_ADDRESS_LIST_CHANGED = false;
    public static boolean IS_STRIPE_ADDED = false;
    public static boolean IS_SHEDULE_EDITED = false;
    public static boolean IS_DOCUMENT_UPDATED = false;
    public static boolean IS_CALLTYPE_UPDATED = false;
    public static boolean IS_TICKET_UPDATED = false;
    public static boolean IS_CARD_UPDATED = false;
    public static boolean IS_CHATTING_OPENED = false;
    public static boolean IS_CHATTING_RESUMED = false;
    public static boolean IS_IN_CALL = false;
    public static boolean isAppOpened = false;
    /*           To check mantadory document uploaded */
    private static HashMap<String, Boolean> hashMapIsMandatoryUploaded;
    /*           To check subcategory selected     */
    private static HashMap<String, Boolean> hashMapIsSubCategorySelected;

    public static HashMap<String, Boolean> getHashMapIsMandatoryUploaded() {
        if (hashMapIsMandatoryUploaded == null) {
            hashMapIsMandatoryUploaded = new HashMap<>();
        }
        return hashMapIsMandatoryUploaded;
    }

    public static HashMap<String, Boolean> getHashMapIsSubCategorySelected() {
        if (hashMapIsSubCategorySelected == null) {
            hashMapIsSubCategorySelected = new HashMap<>();
        }
        return hashMapIsSubCategorySelected;
    }

    public interface ACTION {
        String STARTFOREGROUND_ACTION = "com.vaidg.foregroundservice.action.startforeground";
        String STOPFOREGROUND_ACTION = "com.vaidg.foregroundservice.action.stopforeground";

        String STARTOFFLINELOCATIONSERVICE = "com.vaidg.offlinelocationservice.action.start";
        String STOPOFFLINELOCATIONSERVICE = "com.vaidg.offlinelocationservice.action.stop";
    }

    public static final String  CERTIFICATE_TYPE = "X.509";
    public static final String ALIAS_NAME = "ca";
    public static final String REQUESTED_PROTOCOL = "SSL";
    public static final int ARRAY_SIZE = 1;
    public static final String SEARCH_ITEM = "searchItem";
    public static final String CERTIFICATION_ERROR = "Unexpected default trust managers:";
    public static final int LOWER_BOUND = 0;

    public static final String KEY_PRIVATEALIAS = "PRIVATEALIAS";
    public static final String KEY_PUBLICALIAS = "PUBLICALIAS";
    public static final String ANDROID_KEYSTORE = "AndroidKeyStore";
    public static final String RSA_MODE =  "RSA/ECB/PKCS1Padding";
    public static final String AES_MODE = "AES/GCM/NoPadding";
    public static final String FIXED_IV = "appscripName";
    private final static byte[] IV = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
    public static final IvParameterSpec IV_PARAMETER_SPEC = new IvParameterSpec(IV);

    public static final int PROFILE_SCROLL_POSITION = 465;
    public static final int SEVEN = 7;
    public static final String ITEM_POSITION = "itemPosition";

    /*                         specialization view types     */
    public static final int INTENT_UPDATE_META_DATA = 1000;
    public static final int SPECIALIZATION_VIEW_RADIO_BUTTON = 1;
    public static final int SPECIALIZATION_VIEW_CHECK_BOX = 2;
    public static final int SPECIALIZATION_VIEW_DROPDOWN = 3;
    public static final int SPECIALIZATION_VIEW_NUMBER = 4;
    public static final int SPECIALIZATION_VIEW_NUMBER_SLIDER = 5;
    public static final int SPECIALIZATION_VIEW_FEE = 6;
    public static final int SPECIALIZATION_VIEW_TEXT_AREA = 7;
    public static final int SPECIALIZATION_VIEW_TEXT_AREA_COMMA_SEPARATED = 8;
    public static final int SPECIALIZATION_VIEW_DATE_FUTURE = 9;
    public static final int SPECIALIZATION_VIEW_DATE_PAST = 10;
    public static final int SPECIALIZATION_VIEW_TIME = 11;
    public static final int SPECIALIZATION_VIEW_PICTURE_UPLOAD = 12;
    public static final int SPECIALIZATION_VIEW_VIDEO_UPLOAD = 13;
    public static final int SPECIALIZATION_VIEW_DOCUMENT = 14;

    /*                         date and time formats     */

    public static final String FORMAT_DD = "dd";
    public static final String FORMAT_DASH_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String FORMAT_SPACE_DD_MM_YYYY = "dd MMM yyyy";
    public static final String FORMAT_SPACE_COMMA_MMM_DD_YYYY = "MMM dd, yyyy";
    public static final String FORMAT_SPACE_DD_MMMM_YYYY = "dd MMMM yyyy";
    public static final String FORMAT_SPACE_MMMM_YYYY = "MMMM yyyy";
    public static final String FORMAT_DASH_MM_YYYY = "MM-yyyy";
    public static final String FORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS = "yyyy-MM-dd hh:mm:ss a";
    public static final String FORMAT_SPACE_DD_MMMM_YYYY_TIME_0_12_HRS = "dd MMMM yyyy hh:mm a";
    public static final String FORMAT_SPACE_COLON_DD_MMMM_YYYY_TIME_0_12_HRS = "dd MMMM yyyy | hh:mm a";
    public static final String FORMAT_TIME_0_23_HRS = "HH:mm";
    public static final String FORMAT_TIME_1_12_HRS = "hh:mm a";

}
