package com.vaidg.pro.landing.newLogin;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>LogInDaggerModule</h1>
 * <p>This dagger module created for LogInActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @see com.vaidg.pro.dagger.ActivityBindingModule
 * @since 02/06/2020
 **/
@Module
public abstract class LogInDaggerModule {

    @ActivityScoped
    @Binds
    abstract Activity getActivity(NewLogInActivity newLogInActivity);

    @ActivityScoped
    @Binds
    abstract LogInContract.View getView(NewLogInActivity newLogInActivity);

    @ActivityScoped
    @Binds
    abstract LogInContract.Presenter getPresenter(LogInPresenter logInPresenter);

}
