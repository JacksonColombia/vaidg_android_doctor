package com.vaidg.pro.landing.newSignup.SpecializationCatagory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivitySpecializationCategoryBinding;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.DocumentSelectionFragment;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection.ListSelectionFragment;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection.MediaSelectFragment;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.previewScreen.PreviewSceenFragment;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.slider.SliderFragment;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.textAndDateCategories.OtherInputAndSelectionFragment;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;
import com.vaidg.pro.utility.exception.NoMoreDataException;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

import static com.amazonaws.util.json.JsonUtils.JsonEngine.Gson;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_CHECK_BOX;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DATE_FUTURE;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DOCUMENT;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DROPDOWN;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_NUMBER;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_NUMBER_SLIDER;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DATE_PAST;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_PICTURE_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_RADIO_BUTTON;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_FEE;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_TEXT_AREA;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_TEXT_AREA_COMMA_SEPARATED;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_TIME;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_VIDEO_UPLOAD;

/**
 * <h1>@SpecializationCategoryActivity</h1>
 * <p>this activity is main screen for all specialization category .
 * Used to manage 14 types of category.
 * this activity holds 5 fragments ,where all 14 type are managed or re used
 * </P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class SpecializationCategoryActivity extends BaseDaggerActivity implements SpecializationCategoryContract.View {

    public static final String ITEM_POSITION = "pref_item";
    private static final String TAG = SpecializationCategoryActivity.class.getSimpleName();

    @Inject
    SpecializationCategoryContract.Presenter presenter;

    @Inject
    FragmentManager fragmentManager;

    SelectSpecializationData specializationData;
    private ArrayList<Attribute> data;
    private ActivitySpecializationCategoryBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySpecializationCategoryBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getData();
        openPreviewFrg();
    }

    private void getData() {
        specializationData = (SelectSpecializationData) getIntent().getParcelableExtra("specialization");
        data = (ArrayList<Attribute>) specializationData.getAttributes();
    }

    private void openPreviewFrg() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.frg_enter_from_right, R.anim.frg_exit_from_left);
        fragmentTransaction.add(R.id.fragment_container, new PreviewSceenFragment());
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        int count = fragmentManager.getBackStackEntryCount();
        if (count > 1) {
            super.onBackPressed();
        } else {
            this.finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }

    @Override
    public void hidePreview() {
        binding.fragmentContainer.setVisibility(View.GONE);
        openNextFrag(0, 0, null);
    }

    @Override
    public void openNextFrag(int FromList, int next, Attribute updatedAttribute) {
        if (updatedAttribute != null) {
            specializationData.getAttributes().set(next-1, updatedAttribute);
        }
        Attribute attribute = getRequiredItem(FromList, next);
        if (attribute != null) {
            attribute.setData("");
            DaggerFragment frg = openGetRequiredFragment(attribute);
            openFragment(frg, true);
        }
    }


    private void openFragment(DaggerFragment fragment, boolean keepBack) {

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.frg_enter_from_right, R.anim.frg_exit_from_left);
        fragmentTransaction.add(R.id.fragment_container_pref, fragment);
        if (keepBack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    private DaggerFragment openGetRequiredFragment(Attribute item) {
        if(item != null) {
            Bundle data;
            switch (item.getType()) {
                case SPECIALIZATION_VIEW_RADIO_BUTTON: // Radio Button
                case SPECIALIZATION_VIEW_CHECK_BOX: // CheckBox (multi select)
                case SPECIALIZATION_VIEW_DROPDOWN: // Dropdown Menu
                {
                    ListSelectionFragment fragment = new ListSelectionFragment();
                    fragment.setPreferenceItem(item);
                    data = new Bundle();
                    data.putInt(SpecializationCategoryActivity.ITEM_POSITION, item.getPosition());
                    data.putBoolean(SpecializationCategoryActivity.ITEM_POSITION, item.getType() != 2);
                    fragment.setArguments(data);
                    return fragment;
                }

                case SPECIALIZATION_VIEW_NUMBER_SLIDER: // Number (slider)
                {

                    SliderFragment fragment = new SliderFragment();
                    fragment.setPreferenceItem(item);
                    data = new Bundle();
                    data.putInt(SpecializationCategoryActivity.ITEM_POSITION, item.getPosition());
                    fragment.setArguments(data);
                    return fragment;
                }

                case SPECIALIZATION_VIEW_NUMBER: // Number
                case SPECIALIZATION_VIEW_FEE: // Fee
                case SPECIALIZATION_VIEW_TEXT_AREA: // Text-area
                case SPECIALIZATION_VIEW_TEXT_AREA_COMMA_SEPARATED: // Text-area (comma separated)
                case SPECIALIZATION_VIEW_DATE_FUTURE: // Date(from current to future
                case SPECIALIZATION_VIEW_DATE_PAST: // Date(from past to current)
                case SPECIALIZATION_VIEW_TIME: // Date(from past to current)
                {
                    OtherInputAndSelectionFragment fragment = new OtherInputAndSelectionFragment();
                    fragment.setPreferenceItem(item);
                    data = new Bundle();
                    data.putInt(SpecializationCategoryActivity.ITEM_POSITION, item.getPosition());
                    fragment.setArguments(data);
                    return fragment;
                }

                case SPECIALIZATION_VIEW_PICTURE_UPLOAD: // Picture upload
                case SPECIALIZATION_VIEW_VIDEO_UPLOAD: // Video Upload
                {
                    MediaSelectFragment fragment = new MediaSelectFragment();
                    fragment.setPreferenceItem(item);
                    data = new Bundle();
                    data.putInt(SpecializationCategoryActivity.ITEM_POSITION, item.getPosition());
                    fragment.setArguments(data);
                    return fragment;
                }
                case SPECIALIZATION_VIEW_DOCUMENT: // Document
                {
                    DocumentSelectionFragment fragment = new DocumentSelectionFragment();
                    fragment.setPreferenceItem(item);
                    data = new Bundle();
                    data.putInt(SpecializationCategoryActivity.ITEM_POSITION, item.getPosition());
                    fragment.setArguments(data);
                    return fragment;
                }

                default:
                    return null;
            }
        }
        return  null;
    }

    private Attribute getRequiredItem(int fromList, int next){
        ArrayList<Attribute> items = (ArrayList<Attribute>) specializationData.getAttributes();
        if (next < items.size()) {
            Attribute attribute = items.get(next);
            attribute.setPosition(next);
            attribute.setMax_count(items.size());
            attribute.setList_no(fromList);
            return attribute;
        } else {
            sendDataBack();
            return null;
        }
    }

    private void sendDataBack() {
        Intent intent = new Intent();
        intent.putExtra("specialization", specializationData);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void showError(String message) {
        Snackbar snackbar = Snackbar
                .make(binding.clSpecializationCategory, "" + message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

}
