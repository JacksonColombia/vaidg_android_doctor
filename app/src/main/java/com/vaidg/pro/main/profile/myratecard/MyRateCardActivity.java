package com.vaidg.pro.main.profile.myratecard;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.MyRateCardCategoryAdapter;
import com.vaidg.pro.pojo.profile.ratecard.CategoryData;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MyRateCardActivity extends AppCompatActivity implements MyRateCardPresenter.View {

    private static final String TAG = "MyRateCardActivity";
    private MyRateCardCategoryAdapter myRateCardCategoryAdapter;
    private ArrayList<CategoryData> categoryData;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private MyRateCardPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_category);
        init();
    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.selectCategory));
        Typeface fontBold = Utility.getFontBold(this);
        tvTitle.setTypeface(fontBold);

        categoryData = new ArrayList<>();
        RecyclerView selectCategory = findViewById(R.id.recycler_select_category);
        selectCategory.setLayoutManager(new LinearLayoutManager(this));
        myRateCardCategoryAdapter = new MyRateCardCategoryAdapter(this, categoryData);
        selectCategory.setAdapter(myRateCardCategoryAdapter);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingCategory));
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new MyRateCardPresenter(this);
        presenter.getCategory(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();
    }

    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccess(ArrayList<CategoryData> categoryData) {
        this.categoryData.clear();

        Collections.sort(categoryData, new Comparator<CategoryData>() {
            @Override
            public int compare(CategoryData o1, CategoryData o2) {
                return o1.getCat_name().compareTo(o2.getCat_name());
            }
        });

        this.categoryData.addAll(categoryData);
        myRateCardCategoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == VariableConstant.REQUEST_CODE_SERVICE_CHANGED) {
            int position = data.getIntExtra("position", 0);
            CategoryData category = (CategoryData) data.getSerializableExtra("category");
            categoryData.set(position, category);
            myRateCardCategoryAdapter.notifyDataSetChanged();
        }
    }
}
