package com.vaidg.pro.main.profile.wallet.walletransaction;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Context;
import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.profile.wallet.WalletTransData;
import com.vaidg.pro.pojo.profile.wallet.WalletTransPojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by murashid on 29-Mar-18.
 */

public class WalletTransactionPresenter implements WalletTansactionContract.Presenter {

    private static final String TAG = "WalletTranPres";
    @Inject
    Context context;
    @Inject
    WalletTansactionContract.View view;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;

    @Inject
    WalletTransactionPresenter() {
    }
    @Override
   public void getTransaction(final int index) {
      if(view != null)
        view.startProgressBar();
        service.getwalletTransction(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, String.valueOf(index))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                              if(view != null)
                                view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    WalletTransPojo walletTransPojo = new Gson().fromJson(response, WalletTransPojo.class);
                                  if(view != null)
                                    view.onSuccess(walletTransPojo.getData());
                                }else {
                                  if (view != null)
                                    view.onFailure();
                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(Utility.getAuth(sessionManager.getEmail()),  sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                      if(view != null)
                                        view.hideProgress();
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        getTransaction(index);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                      if(view != null) {
                                        view.stopProgressBar();
                                        view.onFailure();
                                      }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                      if (view != null) {
                                        view.stopProgressBar();
                                        view.sessionExpired(Utility.getMessage(msg));
                                      }
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                              if(view != null) {
                                view.stopProgressBar();
                                view.sessionExpired(Utility.getMessage(errorBody));
                              }
                              break;
                            default:
                              if(view != null) {
                                view.stopProgressBar();
                                view.onFailure(Utility.getMessage(errorBody));
                              }
                              break;
                        }
                    } catch (Exception e) {
                      if(view != null) {
                        view.stopProgressBar();
                        view.onFailure();
                      }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                  if(view != null) {
                    view.stopProgressBar();
                    view.onFailure();
                  }
                }

                @Override
                public void onComplete() {
                }
            });
    }


    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    interface View {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String failureMsg);

        void onFailure();

        void onSuccess(WalletTransData walletTransData);

        void onNewToken(String newToken);

        void sessionExpired(String msg);
    }
}
