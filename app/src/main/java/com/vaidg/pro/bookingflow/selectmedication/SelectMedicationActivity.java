package com.vaidg.pro.bookingflow.selectmedication;

import static com.vaidg.pro.utility.Utility.pixelsToSp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.SelectMedicationListAdapter;
import com.vaidg.pro.pojo.addMedication.Direction;
import com.vaidg.pro.pojo.addMedication.DrugRoute;
import com.vaidg.pro.pojo.addMedication.Frequency;
import com.vaidg.pro.pojo.addMedication.Unit;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import java.util.ArrayList;

public class SelectMedicationActivity extends AppCompatActivity implements View.OnClickListener {
  private static final String TAG = "EventStarted";
  @BindView(R.id.tveventId)
  TextView tveventId;
  @BindView(R.id.ivBackButton)
  ImageView ivBackButton;
  @BindView(R.id.btnSave)
  Button btnSave;
  @BindView(R.id.rvSelectMedication)
  RecyclerView rvSelectMedication;
  private Context mContext;
  private SessionManager sessionManager;
  private Typeface fontMedium, fontRegular, fontBold, fontLight;
  private Animation fade_open, fade_close, rotate_forward, rotate_backward;
  private ArrayList<Unit> unitArrayList = new ArrayList<>();
  private boolean isFromInstruction = false, isFromFrequency = false, isFromSelectDate = false, isFromUnit = false, isFromDirection = false, isFromDuration = false, isFromDrugRoute = false;
  private ArrayList<Frequency> frequencyArrayList = new ArrayList<>();
  private ArrayList<Direction> directionArrayList = new ArrayList<>();
  private ArrayList<String> stringArrayList = new ArrayList<>();
  private ArrayList<DrugRoute> drugRouteArrayList = new ArrayList<>();
  private SelectMedicationListAdapter mMedicationListAdapter;
  private Booking booking;
  private String mDrugRouteName = "", mDirectionName = "", mFrequencyName = "", mUnitName = "", mDurationName = "", mInstruction = "";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_selectmedication);
    ButterKnife.bind(this);
    mContext = this;
    Bundle bundle = getIntent().getExtras();
    booking = (Booking) (bundle != null ? bundle.getSerializable("booking") : null);
    initView();
    typeFace();
  }

  private void typeFace() {
    tveventId.setTypeface(fontBold);
    tveventId.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_18), mContext));
    btnSave.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
    btnSave.setTypeface(fontMedium);
    btnSave.setOnClickListener(this);
    ivBackButton.setOnClickListener(this);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    closeActivity();
  }

  private void initView() {
    sessionManager = SessionManager.getSessionManager(this);
    fontBold = Utility.getFontBold(this);
    fontMedium = Utility.getFontMedium(this);
    fontRegular = Utility.getFontRegular(this);
    fontLight = Utility.getFontLight(this);
    fade_open = AnimationUtils.loadAnimation(this, R.anim.fade_open);
    fade_close = AnimationUtils.loadAnimation(this, R.anim.fade_close);
    rotate_forward = AnimationUtils.loadAnimation(this, R.anim.rotate_center_to_left);
    rotate_backward = AnimationUtils.loadAnimation(this, R.anim.rotate_left_to_center);
    Bundle bundle = getIntent().getExtras();
    frequencyArrayList = (ArrayList<Frequency>) bundle.getSerializable("Frequency");
    isFromFrequency = bundle.getBoolean("isFromFrequency");
    mFrequencyName = bundle.getString("FrequencyName");
    drugRouteArrayList = (ArrayList<DrugRoute>) bundle.getSerializable("DrugRoute");
    isFromDrugRoute = bundle.getBoolean("isFromDrugRoute");
    mDrugRouteName = bundle.getString("DrugRouteName");
    directionArrayList = (ArrayList<Direction>) bundle.getSerializable("Direction");
    isFromDirection = bundle.getBoolean("isFromDirection");
    mDirectionName = bundle.getString("DirectionName");
    isFromDuration = bundle.getBoolean("isFromDuration");
    mDurationName = bundle.getString("DurationName");
    isFromInstruction = bundle.getBoolean("isFromInstruction");
    mInstruction = bundle.getString("InstructionName");
    isFromSelectDate = bundle.getBoolean("isFromSelectDate");
    if (isFromDrugRoute)
      tveventId.setText(getResources().getString(R.string.drugRoute));
    else if (isFromFrequency)
      tveventId.setText(getResources().getString(R.string.frequency));
    else if (isFromInstruction)
      tveventId.setText(getResources().getString(R.string.instruction));
    else if (isFromDirection)
      tveventId.setText(getResources().getString(R.string.direction));
    else if (isFromDuration)
      tveventId.setText(getResources().getString(R.string.duration));
    else if(isFromSelectDate)
      tveventId.setText(getResources().getString(R.string.nextVisitOn));

    rvSelectMedication.setHasFixedSize(true);
    rvSelectMedication.setLayoutManager(new LinearLayoutManager(mContext));
    if (!isFromDuration)
      rvSelectMedication.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));
    mMedicationListAdapter = new SelectMedicationListAdapter(frequencyArrayList, unitArrayList, drugRouteArrayList, directionArrayList, mInstruction, isFromInstruction, isFromDirection, isFromDrugRoute, isFromUnit, isFromDuration, isFromFrequency, mDirectionName, mDurationName, mDrugRouteName, mFrequencyName, mUnitName,isFromSelectDate, new SelectMedicationListAdapter.OnItemClickListener() {
      @Override
      public void onDrugRouteItemClick(int position, String unitName, String id) {
        for (int i = 0; i < drugRouteArrayList.size(); i++) {
          drugRouteArrayList.get(i).setChecked(false);
        }
        drugRouteArrayList.get(position).setChecked(true);
        mDrugRouteName = drugRouteArrayList.get(position).getName();
        mMedicationListAdapter.notifyDataSetChanged();
      }

      @Override
      public void onFrequencyItemClick(int position, String frequencyName, String id) {
        for (int i = 0; i < frequencyArrayList.size(); i++) {
          frequencyArrayList.get(i).setChecked(false);
        }
        frequencyArrayList.get(position).setChecked(true);
        mFrequencyName = frequencyArrayList.get(position).getName();
        mMedicationListAdapter.notifyDataSetChanged();
      }

      @Override
      public void onDirectionItemClick(int position, String directioName, String id) {
        directionArrayList.get(position).setChecked(!directionArrayList.get(position).isChecked());
        if (directionArrayList.get(position).isChecked())
          directionArrayList.get(position).setChecked(true);
        else
          directionArrayList.get(position).setChecked(false);
        stringArrayList.clear();
        for (int i = 0; i < directionArrayList.size(); i++) {
          if (directionArrayList.get(i).isChecked())
            stringArrayList.add(directionArrayList.get(i).getName());
        }
        mDirectionName = TextUtils.join(", ", stringArrayList);
        Log.w(TAG, "onDirectionItemClick: " + mDirectionName);
        // directionArrayList.get(position).setChecked(true);
        //mDirectionName = directionArrayList.get(position).getName();
        mMedicationListAdapter.notifyDataSetChanged();
      }

      @Override
      public void onDurationItemClick(String durantionDay) {
        mDurationName = durantionDay + " " + "Days";
        // mMedicationListAdapter.notifyDataSetChanged();
      }

      @Override
      public void onInstructionItemClick(String instructionName) {
        mInstruction = instructionName;
        // mMedicationListAdapter.notifyDataSetChanged();
      }
    });
    rvSelectMedication.setAdapter(mMedicationListAdapter);
  }

  private void closeActivity() {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
      finishAfterTransition();
    } else {
      finish();
      overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.ivBackButton:
        closeActivity();
        break;
      case R.id.btnSave:
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        if (isFromDuration) {
          intent.putExtra("isFromDuration", true);
          bundle.putString("DurationName", mDurationName);
        } else if (isFromInstruction) {
          intent.putExtra("isFromInstruction", true);
          bundle.putString("InstructionName", mInstruction);
        } else if (isFromDirection) {
          if (mDirectionName != null && !mDirectionName.isEmpty()) {
            intent.putExtra("isFromDirection", true);
            bundle.putSerializable("Direction", directionArrayList);
            bundle.putString("DirectionName", mDirectionName);
          } else {
            AppController.getInstance().toast("Please select the option");
          }
        } else if (isFromDrugRoute) {
          intent.putExtra("isFromDrugRoute", true);
          bundle.putSerializable("DrugRoute", drugRouteArrayList);
          bundle.putString("DrugRouteName", mDrugRouteName);
        } else if (isFromSelectDate) {
          intent.putExtra("isFromSelectDate", true);
          bundle.putSerializable("DurationName", mDurationName);
          bundle.putString("InstructionName", mInstruction);
        } else if (isFromFrequency) {
          intent.putExtra("isFromFrequency", true);
          bundle.putSerializable("Frequency", frequencyArrayList);
          bundle.putString("FrequencyName", mFrequencyName);
        }
        bundle.putSerializable("booking", booking);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
        closeActivity();
        break;
    }
  }
}
