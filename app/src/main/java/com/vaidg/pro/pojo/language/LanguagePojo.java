package com.vaidg.pro.pojo.language;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 24-Apr-18.
 */

public class LanguagePojo implements Serializable {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<LanguageData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<LanguageData> getData() {
        return data;
    }

    public void setData(ArrayList<LanguageData> data) {
        this.data = data;
    }
}
