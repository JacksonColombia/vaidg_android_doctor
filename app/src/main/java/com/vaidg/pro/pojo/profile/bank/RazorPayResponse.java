package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RazorPayResponse implements Parcelable
{

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private RazorPayData data;
    public final static Creator<RazorPayResponse> CREATOR = new Creator<RazorPayResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RazorPayResponse createFromParcel(android.os.Parcel in) {
            return new RazorPayResponse(in);
        }

        public RazorPayResponse[] newArray(int size) {
            return (new RazorPayResponse[size]);
        }

    }
            ;

    protected RazorPayResponse(android.os.Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((RazorPayData) in.readValue((RazorPayData.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public RazorPayResponse() {
    }

    /**
     *
     * @param data
     * @param message
     */
    public RazorPayResponse(String message, RazorPayData data) {
        super();
        this.message = message;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RazorPayData getData() {
        return data;
    }

    public void setData(RazorPayData data) {
        this.data = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

}
