package com.vaidg.pro.utility;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class OnClearFromRecentService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Utility.printLog("ClearFromRecentService", "Service Started");
        VariableConstant.isAppOpened = true;
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utility.printLog("ClearFromRecentService", "Service Destroyed");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Utility.printLog("ClearFromRecentService", "END");
        //Code here
        VariableConstant.isAppOpened = false;
        stopSelf();
    }
}
