package com.vaidg.pro.bookingflow.popUpWindow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.CategoryDropdownAdapter;
import com.vaidg.pro.adapters.FrquencyDropdownAdapter;
import com.vaidg.pro.pojo.addMedication.Frequency;
import com.vaidg.pro.pojo.addMedication.Unit;
import java.util.ArrayList;

public class FrequencyDropdownMenu extends PopupWindow {
  private Context context;
  private RecyclerView rvCategory;
  private ArrayList<Frequency> units = new ArrayList<>();
  private FrquencyDropdownAdapter dropdownAdapter;

  public FrequencyDropdownMenu(Context context, ArrayList<Frequency> units) {
    super(context);
    this.context = context;
    this.units = units;
    setupView(units);
  }

  public void setCategorySelectedListener(FrquencyDropdownAdapter.CategorySelectedListener categorySelectedListener) {
    dropdownAdapter.setCategorySelectedListener(categorySelectedListener);
  }

  private void setupView(ArrayList<Frequency> units) {
    View view = LayoutInflater.from(context).inflate(R.layout.item_popup_category, null);
    rvCategory = view.findViewById(R.id.rvCategory);
    rvCategory.setHasFixedSize(true);
    rvCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
    // rvCategory.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
    dropdownAdapter = new FrquencyDropdownAdapter(units);
    rvCategory.setAdapter(dropdownAdapter);
    setContentView(view);
  }
}