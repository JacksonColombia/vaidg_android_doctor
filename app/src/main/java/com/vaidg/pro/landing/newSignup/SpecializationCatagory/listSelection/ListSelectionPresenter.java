package com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection;

import android.app.Activity;

import com.vaidg.pro.R;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection.model.ListSelectItemCallBack;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection.model.ListSelectionModel;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.pojo.PreDefined;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * <h1>@ListSelectionPresenter</h1>
 * <p>this fragment presenter is used used for business logic</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class ListSelectionPresenter implements ListSelectionContract.Presenter, ListSelectItemCallBack {

    @Inject
    ListSelectionModel model;

    @Inject
    Activity activity;

    private ListSelectionContract.View view;

    @Inject
    public ListSelectionPresenter() {
    }

    @Override
    public void onItemSelected(int position, boolean isSingleSelection) {
        if (isSingleSelection)
            model.selectSingleList(position);
        else
            model.selectMultiList(position);
        if (view != null)
            view.notifyAdapter();
    }

    @Override
    public void isCheck() {
        if (model.isCheck() || !view.isMandatoryForProvider()) {
            view.moveNextFrag();
        } else
            view.showError(activity.getString(R.string.list_selection_error));
    }

    @Override
    public void saveData(Attribute current_item) {
        model.saveData(current_item);
    }

    @Override
    public void attachView(ListSelectionContract.View view) {
        if(view.getData() != null && view.getData().size() > 0)
        model.getData(view.getData());
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
