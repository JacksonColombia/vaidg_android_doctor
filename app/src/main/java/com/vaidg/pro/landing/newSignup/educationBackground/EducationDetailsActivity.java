package com.vaidg.pro.landing.newSignup.educationBackground;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityEducationDetailsBinding;
import com.vaidg.pro.landing.newSignup.editEducation.AddEditEducationActivity;
import com.vaidg.pro.landing.newSignup.educationBackground.model.EducationDetailAdapter;
import com.vaidg.pro.landing.newSignup.educationBackground.model.EducationDetailsCallBack;
import com.vaidg.pro.pojo.profile.DegreeDetailsData;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import static com.vaidg.pro.utility.VariableConstant.DATA;
import static com.vaidg.pro.utility.VariableConstant.IS_DISPLAY;

/**
 * <h1>@EducationDetailsActivity</h1>
 * <p> EducationDetailsActivity activity to View and Add Educational Details</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 27/04/2020.
 **/
public class EducationDetailsActivity extends BaseDaggerActivity implements EducationDetailsContract.View {

    public static final int ADD_EDIT_EDUCATION = 101;

    @Inject
    EducationDetailAdapter mEducationDetailAdapter;

    @Inject
    EducationDetailsContract.Presenter presenter;

    private ActivityEducationDetailsBinding binding;

    private boolean isDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEducationDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
        initOnClickListener();
        educationDetailDataSetUp();
    }

    private void initOnClickListener() {
        binding.btnAddEducation.setOnClickListener(this::onClick);
        binding.includeToolbar.ivBackButton.setOnClickListener(this::onClick);
        binding.includeToolbar.btnDone.setOnClickListener(this::onClick);

    }

    private void initViews() {
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        binding.includeToolbar.tvTitle.setText(getString(R.string.education));

        if (getIntent() != null && getIntent().hasExtra(DATA)) {
            isDisplay = getIntent().getBooleanExtra(IS_DISPLAY, false);
            presenter.addAllDataToList(new Gson().fromJson(getIntent().getStringExtra(DATA), new TypeToken<ArrayList<DegreeDetailsData>>() {
            }.getType()));
        }


        if (!isDisplay) {
            binding.includeToolbar.btnDone.setText(getString(R.string.save));
            Utility.setTextColor(this, binding.includeToolbar.btnDone, R.color.normalTextColor);
            binding.includeToolbar.btnDone.setVisibility(View.VISIBLE);
            binding.btnAddEducation.setVisibility(View.VISIBLE);
        } else {
            binding.rvEducationDetail.setPaddingRelative(0, getResources().getDimensionPixelSize(R.dimen.dp_10), 0, 0);
        }

    }

    private void educationDetailDataSetUp() {
        mEducationDetailAdapter.setEditMode(!isDisplay);
        binding.rvEducationDetail.setAdapter(mEducationDetailAdapter);
        mEducationDetailAdapter.setCallback((EducationDetailsCallBack) presenter);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddEducation:
                Intent intent = new Intent(this, AddEditEducationActivity.class);
                startActivityForResult(intent, ADD_EDIT_EDUCATION);
                break;

            case R.id.ivBackButton:
                onBackPressed();
                break;

            case R.id.btnDone:
                presenter.sendBackSaveData();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finishAfterTransition();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.parseResult(requestCode, resultCode, data);
    }

    @Override
    public void notifyAdapter() {
        mEducationDetailAdapter.notifyDataSetChanged();
    }

    @Override
    public void EditOnSelect(DegreeDetailsData positionData) {
        Intent intent = new Intent(this, AddEditEducationActivity.class);
        intent.putExtra("editData", positionData);
        startActivityForResult(intent, ADD_EDIT_EDUCATION);
    }

    @Override
    public void sendBackSaveData(Intent intent) {
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
