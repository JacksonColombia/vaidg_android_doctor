package com.vaidg.pro.main.earning;

import android.app.Activity;

import com.vaidg.pro.bookingflow.UpdateStatusContract;
import com.vaidg.pro.bookingflow.UpdateStatusPresenterImple;
import com.vaidg.pro.bookingflow.acceptReject.AcceptRejectActivity;
import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>EarningWebDaggerModule</h1>
 * <p>This dagger module created for LogInActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
@Module
public abstract class EarningWebDaggerModule {
  @ActivityScoped
  @Binds
  abstract Activity getActivity(EarningWebActivity activity);

  @ActivityScoped
  @Binds
  abstract EarningWebContract.Presenter getPresenter(EarningWebPresenterImple presenterImple);
}
