package com.vaidg.pro.bookingflow.medication;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.MedicationListAdapter;
import com.vaidg.pro.bookingflow.addmedication.AddMedicationActivity;
import com.vaidg.pro.adapters.MedicationListAdapter.OnItemClickListener;
import com.vaidg.pro.bookingflow.addmedication.AddMedicationActivity;
import com.vaidg.pro.bookingflow.invoice.InvoiceActivity;
import com.vaidg.pro.bookingflow.review.RateCustomerActivity;
import com.vaidg.pro.bookingflow.selectmedication.SelectMedicationActivity;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.pojo.medication.MedicationData;
import com.vaidg.pro.utility.CircleTransform;
import com.vaidg.pro.utility.MixpanelEvents;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

import static com.vaidg.pro.AppController.getInstance;

/**
 * Created by murashid on 29-Sep-17.
 * <h1>InCallActivity</h1>
 * Activity for starting and enting the event time
 */
public class MedicationActivity extends DaggerAppCompatActivity implements MedicationContract.View, View.OnClickListener {
    private static final String TAG = "EventStarted";
    @Inject
    MedicationContract.Presenter presenter;
    @BindView(R.id.tvBookForDep)
    TextView tvBookForDep;
    @BindView(R.id.tvCustomerName)
    TextView tvCustomerName;
    @BindView(R.id.tvCategory)
    TextView tvCategory;
    @BindView(R.id.btnAddMedication)
    TextView btnAddMedication;
    @BindView(R.id.tvNextVisit)
    TextView tvNextVisit;
    @BindView(R.id.tveventId)
    TextView tveventId;
    @BindView(R.id.etSelectDate)
    EditText etSelectDate;
    @BindView(R.id.ivCustomerImage)
    ImageView ivCustomerImage;
    @BindView(R.id.rvMedication)
    RecyclerView rvMedication;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private SeekBar seekBar;
    private Booking booking;
    private String bookidId = "", medicationId = "0", mInstruction = "",mSelectedDate ="";
    private String bookingEndtime = "";
    private String cancelId = "";
    private Typeface fontMedium, fontRegular, fontBold;
    private Animation fade_open, fade_close, rotate_forward, rotate_backward;
    private TextView tvSeekbarText;
    private MedicationListAdapter mMedicationListAdapter;
    private ArrayList<MedicationData> mMedicationData = new ArrayList<>();
    private Context mContext;

    /**
     * method for showing custome alertDialog
     *
     * @param mActivity    context
     * @param title        title
     * @param msg          message
     * @param action       action
     * @param bookidId
     * @param medicationId
     * @param presenter
     */
    public static void customAlertDialog(final Activity mActivity, String title, String msg, String action, String bookidId, String medicationId, MedicationContract.Presenter presenter) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        final View view = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_simple_message, null);
        alertDialogBuilder.setView(view);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        TextView tvMsg = view.findViewById(R.id.tvMsg);
        TextView btnOk = view.findViewById(R.id.btnOk);

        tvTitle.setText(title);
        tvMsg.setText(msg);
        btnOk.setText(action);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        //alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final Animator[] animHide = {null};
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            alertDialog.show();
            view.post(new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    int cx = view.getWidth() / 2;
                    int cy = view.getHeight() / 2;
                    float finalRadius = (float) Math.hypot(cx, cy);
                    Animator animVisible = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);
                    animHide[0] = ViewAnimationUtils.createCircularReveal(view, cx, cy, finalRadius, 0);
                    animVisible.start();

                }
            });
        } else {
            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogThemeBottom;
            alertDialog.show();
        }

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.delete(bookidId, medicationId);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    if (animHide[0] != null) {
                        animHide[0].addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                alertDialog.dismiss();

                            }
                        });
                        animHide[0].start();
                    } else {
                        alertDialog.dismiss();
                    }
                } else {
                    alertDialog.dismiss();
                }
            }
        });

        ImageView ivClose = view.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    if (animHide[0] != null) {
                        animHide[0].addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                alertDialog.dismiss();

                            }
                        });
                        animHide[0].start();
                    } else {
                        alertDialog.dismiss();
                    }
                } else {
                    alertDialog.dismiss();
                }
            }
        });
        //alertDialog.setCancelable(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medication);
        ButterKnife.bind(this);
        mContext = this;
        Bundle bundle = getIntent().getExtras();
        booking = (Booking) (bundle != null ? bundle.getSerializable("booking") : null);
        bookidId = booking != null ? booking.getBookingId() : "";
        bookingEndtime = booking != null ? booking.getBookingEndtime() : "";
        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.refreshing));
        progressDialog.setCancelable(false);
        fontBold = Utility.getFontBold(this);
        fontMedium = Utility.getFontMedium(this);
        fontRegular = Utility.getFontRegular(this);
        Typeface fontLight = Utility.getFontLight(this);
        presenter.getMedication(bookidId, medicationId);
        tveventId.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_18), this));
        tveventId.setTypeface(fontBold);
        etSelectDate.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_16), this));
        etSelectDate.setTypeface(fontMedium);
        etSelectDate.setOnClickListener(v -> {
          Intent intent = new Intent(this, SelectMedicationActivity.class);
          Bundle bundle = new Bundle();
          intent.putExtra("isFromSelectDate", true);
          bundle.putString("DurationName", mSelectedDate);
          bundle.putString("InstructionName", mInstruction);
          bundle.putSerializable("booking", booking);
          intent.putExtras(bundle);
          if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            startActivityForResult(intent, 2, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
          } else {
            startActivityForResult(intent, 2);
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
          }
        });
        tvNextVisit.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_14), this));
        tvNextVisit.setTypeface(fontBold);
        tvSeekbarText = findViewById(R.id.tvSeekbarText);
        seekBar = findViewById(R.id.seekBar);
        tvSeekbarText.setTypeface(fontRegular);
        tvSeekbarText.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_16), mContext));
        tvCustomerName.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_18), mContext));
        tvBookForDep.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_16), mContext));
        tvCategory.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_16), mContext));
        btnAddMedication.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_16), mContext));
        btnAddMedication.setTypeface(fontMedium);
        tvCustomerName.setTypeface(fontBold);
        tvBookForDep.setTypeface(fontMedium);
        tvCategory.setTypeface(fontRegular);
        rvMedication.setHasFixedSize(true);
        rvMedication.setLayoutManager(new LinearLayoutManager(mContext));
        mMedicationListAdapter = new MedicationListAdapter(mMedicationData, new OnItemClickListener() {
            @Override
            public void onEditClick(String id) {
                medicationId = id;
                Intent intent = new Intent(mContext, AddMedicationActivity.class);
                Bundle bundle = new Bundle();
                intent.putExtra("isFromEdit", true);
                bundle.putSerializable("booking", booking);
                bundle.putString("medicationId", medicationId);
                intent.putExtras(bundle);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    startActivityForResult(intent, 1, ActivityOptions.makeSceneTransitionAnimation(MedicationActivity.this).toBundle());
                } else {
                    startActivityForResult(intent, 1);
                    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
            }

            @Override
            public void onDeleteClick(String id) {
                medicationId = id;
                customAlertDialog(MedicationActivity.this,
                        getString(R.string.alert),
                        "Are you sure, you want to delete",
                        getString(R.string.oK), bookidId, medicationId, presenter);
            }
        });
        rvMedication.setAdapter(mMedicationListAdapter);
        ImageView ivBackButton = findViewById(R.id.ivBackButton);
        ivBackButton.setOnClickListener(this);
        //tveventId.setText(getText(R.string.jobId) + " " + booking.getBookingId());
        btnAddMedication.setOnClickListener(this);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 75) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("bookingId", bookidId);
                        jsonObject.put("latitude", sessionManager.getCurrentLat());
                        jsonObject.put("longitude", sessionManager.getCurrentLng());
                        if(getIntent().getBooleanExtra("isFromInvocie",false)) {
                            jsonObject.put("status", VariableConstant.JOB_COMPLETED_RAISE_INVOICE);
                            ArrayList<String> etAmounts = (ArrayList<String>) getIntent().getSerializableExtra("ITEM_EXTRA");
                            ArrayList<String> etExtraFees = (ArrayList<String>) getIntent().getSerializableExtra("ITEM_EXTRAS");
                            JSONArray jsonArray = new JSONArray();
                            for (int i = 0; i < etAmounts.size(); i++) {
                                JSONObject jsonObjectExtra = new JSONObject();
                                jsonObjectExtra.put("serviceName", etExtraFees.get(i).toString().trim());
                                jsonObjectExtra.put("price", etAmounts.get(i).toString().trim());
                                jsonArray.put(jsonObjectExtra);
                            }
                            jsonObject.put("additionalService", jsonArray);
                        }else{
                            jsonObject.put("status", VariableConstant.JOB_TIMER_COMPLETED);
                            jsonObject.put("distance", "" + sessionManager.getOnJobDistance(booking.getBookingId()));
                        }
                        if(mSelectedDate.isEmpty())
                        {
                           /* Calendar c = Calendar.getInstance();
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);// HH:mm:ss");
                            String reg_date = df.format(c.getTime());
                            Utility.printLog("Currrent Date Time : ",reg_date);

                            c.add(Calendar.DATE, 0);  // number of days to add
                            String end_date = df.format(c.getTime());
                            Date date = df.parse(end_date);
                            long milliseconds = 0;
                            if (date != null) {
                                milliseconds = date.getTime();
                            }
                            Utility.printLog("end Time : ",end_date);*/
                            jsonObject.put("nextVisitDate",""/*milliseconds/1000*/);
                        }else{
                            String[] separated = mSelectedDate.split(" ");
                            long milliseconds = 0;/*System.currentTimeMillis() + (Integer.parseInt(separated[0]) * 24 * 60 * 60 * 1000);*/
                            Calendar c = Calendar.getInstance();
                            c.setTimeZone(getInstance().getTimeZone());
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);// HH:mm:ss");
                            df.setTimeZone(getInstance().getTimeZone());
                            c.add(Calendar.DATE, Integer.parseInt(separated[0]));  // number of days to add
                            String end_date = df.format(c.getTime());
                            Date date = df.parse(end_date);
                            if (date != null) {
                                milliseconds = date.getTime();
                            }
                            Utility.printLog("end Time : ", String.valueOf(milliseconds));
                            jsonObject.put("nextVisitDate", TimeUnit.MILLISECONDS.toSeconds(milliseconds));
                        }
                        jsonObject.put("appointmentMemo",mInstruction);
                        Log.d(TAG, "c: " + jsonObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    seekBar.setProgress(100);
                    startProgressBar();
                    presenter.updateStaus(jsonObject, true);
                } else {
                    seekBar.setProgress(0);
                }
            }
        });
        fade_open = AnimationUtils.loadAnimation(this, R.anim.fade_open);
        fade_close = AnimationUtils.loadAnimation(this, R.anim.fade_close);
        rotate_forward = AnimationUtils.loadAnimation(this, R.anim.rotate_center_to_left);
        rotate_backward = AnimationUtils.loadAnimation(this, R.anim.rotate_left_to_center);

        if (booking.getDependentData() != null && booking.getDependentData().getDependentId() != null && !booking.getDependentData().getDependentId().isEmpty() &&!booking.getDependentData().getDependentId().equals("1")) {
            tvBookForDep.setVisibility(View.VISIBLE);
            tvBookForDep.setText(mContext.getResources().getString(R.string.bookForDependent) + " " + booking.getFirstName() + " " + booking.getLastName());
            tvCustomerName.setText(booking.getDependentData().getFirstName() + " " + booking.getDependentData().getLastName());
            tvCategory.setText(Utility.getAge(booking.getDependentData().getDateOfBirth())+ " " + mContext.getResources().getString(R.string.years) + " " + mContext.getResources().getString(R.string.comma) + " " + Utility.gender(String.valueOf(booking.getDependentData().getGender()))  + " " + mContext.getResources().getString(R.string.comma) + " " + booking.getDependentData().getRelationship());
            if (booking.getDependentData().getProfilePic() != null && !booking.getDependentData().getProfilePic().isEmpty()) {
                Glide.with(mContext).setDefaultRequestOptions(new RequestOptions().error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(mContext))
                    .placeholder(R.drawable.profile_default_image))
                    .load(booking.getDependentData().getProfilePic())
                    .into(ivCustomerImage);
            }
        } else {
            tvCustomerName.setText(booking.getFirstName() + " " + booking.getLastName());
            tvCategory.setText(booking.getAgeString() + " " + mContext.getResources().getString(R.string.comma) + " " + Utility.gender(booking.getGender()));
            if (booking.getProfilePic() != null && !booking.getProfilePic().isEmpty()) {
                Glide.with(mContext).setDefaultRequestOptions(new RequestOptions().error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(mContext))
                    .placeholder(R.drawable.profile_default_image))
                    .load(booking.getProfilePic())
                    .into(ivCustomerImage);
            }
        }

        /*
         * Bug id : Trello 189
         * Bug Description : There is no option to cancel a job with reason for InCall
         * Fixed Description : Added Cancel textview in layout and handled the action.
         * Dev : Murashid
         * Date : 29-11-2018
         * */
      /*  if (booking.getStatus().equals("9")) {
            findViewById(R.id.seekBar).setVisibility(View.GONE);
        }
        if (booking.getStatus().equals(VariableConstant.JOB_TIMER_COMPLETED)) {
            seekBar.setVisibility(View.GONE);
            tvSeekbarText.setText(booking.getStatusMsg());
        }*/
    }

    private Date getDateOfTheDay(long startOfDayInMillisToday) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
        calendar.setTimeInMillis(startOfDayInMillisToday);
        return calendar.getTime();
    }

    public long getStartOfDayInMillis(Date date) {
        Calendar calendar = Calendar.getInstance();//
        calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
        calendar.setTime(date);//.getTime()
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        //  if(isStartTime)
        return calendar.getTimeInMillis() + (24 * 60 * 60 * 1000);
       /* else
            return calendar.getTimeInMillis();*/
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utility.checkAndShowNetworkError(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utility.progressDialogCancel(this, progressDialog);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        Utility.progressDialogCancel(this, progressDialog);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackButton:
                closeActivity();
                break;
            case R.id.btnAddMedication:
                medicationId = "0";
                Intent intent = new Intent(this, AddMedicationActivity.class);
                Bundle bundle = new Bundle();
                intent.putExtra("isFromEdit", false);
                bundle.putSerializable("booking", booking);
                bundle.putString("medicationId", medicationId);
                intent.putExtras(bundle);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    startActivityForResult(intent, 1, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                } else {
                    startActivityForResult(intent, 1);
                    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
                break;
        }
    }

    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(mContext, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onFailure(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        seekBar.setProgress(0);
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, this);
    }

    @Override
    public void onSuccess(String msg, boolean bookingDetailsRefreshed) {
        Utility.progressDialogDismiss(this, progressDialog);
        VariableConstant.IS_BOOKING_UPDATED = true;
/*
        seekBar.setProgress(0);
        double travelFee = 0;
        double total = 0;
        try {
            JSONObject jsonObjectResponse = new JSONObject(msg);
            JSONObject jsonData = jsonObjectResponse.getJSONObject("data");
            total = Double.parseDouble(booking.getAccounting().getTotal());
            travelFee = Double.parseDouble(jsonData.getString("travelFee"));
            total += travelFee;
*/
/*
      if (booking.getServiceType().equals("2")) {
        total = total - hourFee;
        booking.getAccounting().setTotalActualJobTimeMinutes(jsonData.getString("totalActualJobTimeMinutes"));
        booking.getAccounting().setTotalActualHourFee(jsonData.getString("totalActualHourFee"));
        total = total + Double.parseDouble(jsonData.getString("totalActualHourFee"));
      }
*//*

        } catch (Exception e) {
            e.printStackTrace();
        }
        booking.getAccounting().setTravelFee("" + travelFee);
        booking.getAccounting().setTotal("" + total);
        booking.setStatus(VariableConstant.JOB_TIMER_COMPLETED);
        sessionManager.setBookingStr(presenter.getBookingStr(booking, sessionManager.getBookingStr(), VariableConstant.JOB_TIMER_COMPLETED));
//        AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.JobCompleted.value, bookidId);
        seekBar.setVisibility(View.GONE);
        tvSeekbarText.setText(R.string.job_complete_payment_pending);
*/

      AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.BookingCancelled.value, booking.getBookingId());
      finish();
        if(getIntent().getBooleanExtra("isFromInvocie",false)) {
            sessionManager.setBookingStr(presenter.getBookingStr(booking, sessionManager.getBookingStr(), VariableConstant.JOB_COMPLETED_RAISE_INVOICE));
        }
     // sessionManager.setBookingStr(presenter.getBookingStr(booking, sessionManager.getBookingStr(),VariableConstant.JOB_COMPLETED_RAISE_INVOICE));
      Intent intent = new Intent(this, RateCustomerActivity.class);
      Bundle bundle = new Bundle();
      bundle.putBoolean("rateNotfcnCustomer", false);
      bundle.putSerializable("booking", booking);
      intent.putExtras(bundle);
      if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
        startActivity(intent , ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
      } else {
        startActivity(intent);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
      }
      AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.RaiseInvoice.value, booking.getBookingId());

    }

    @Override
    public void onSuccessResonse(ArrayList<MedicationData> data) {
        mMedicationData.clear();
        mMedicationData.addAll(data);
        mMedicationListAdapter.notifyDataSetChanged();
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                if (data != null) {
                    booking = (Booking) data.getSerializableExtra("booking");
                    bookidId = booking != null ? booking.getBookingId() : "";
                    //medicationId =data.getStringExtra("medicationId");
                    presenter.getMedication(bookidId, medicationId);
                }
            }
            if (requestCode == 2) {
                if (data != null) {
                    booking = (Booking) data.getSerializableExtra("booking");
                    bookidId = booking != null ? booking.getBookingId() : "";
                    mSelectedDate =data.getStringExtra("DurationName");
                    mInstruction =data.getStringExtra("InstructionName");
                    etSelectDate.setText(mSelectedDate);
                }
            }
        }
    }


}
