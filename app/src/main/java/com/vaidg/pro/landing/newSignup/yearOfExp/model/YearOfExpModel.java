package com.vaidg.pro.landing.newSignup.yearOfExp.model;

import com.vaidg.pro.BaseModel;

import javax.inject.Inject;

/**
 * <h1>yearOfExpModel</h1>
 * <p> model class for data </P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 28/04/2020.
 **/
public class YearOfExpModel extends BaseModel {

    @Inject
    public YearOfExpModel() {
    }
}
