package com.vaidg.pro.main.history.historyinvoice;

import android.app.Activity;
import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.main.schedule.bookingschedule.BookingScheculeContract;
import com.vaidg.pro.main.schedule.bookingschedule.BookingSchedulePresenter;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>ProfileDaggerModule</h1>
 * <p>This dagger module created for ProfileFragment to bind injected objects</p>
 *
 * @author 3embed.
 * @version 1.0.20.
 * @since 19/06/2020.
 **/
@Module
public abstract class HistoryInvoiceDaggerModule {

    @ActivityScoped
    @Binds
    abstract Activity getActivity(HistoryInvoiceActivity mainActivity);


    @ActivityScoped
    @Binds
    abstract BookingScheculeContract.Presenter providePresenter(BookingSchedulePresenter presenter);

    @ActivityScoped
    @Binds
    abstract BookingScheculeContract.View provideView(HistoryInvoiceActivity activity);

}
