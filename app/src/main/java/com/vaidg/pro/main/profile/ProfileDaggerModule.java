package com.vaidg.pro.main.profile;

import com.vaidg.pro.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>ProfileDaggerModule</h1>
 * <p>This dagger module created for ProfileFragment to bind injected objects</p>
 *
 * @author 3embed.
 * @version 1.0.20.
 * @since 19/06/2020.
 **/
@Module
public abstract class ProfileDaggerModule {

    @FragmentScoped
    @Binds
    abstract ProfileContract.Presenter providePresenter(ProfilePresenter presenter);

    @FragmentScoped
    @Binds
    abstract ProfileContract.View provideView(ProfileFragment fragment);
}
