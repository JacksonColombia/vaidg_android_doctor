package com.vaidg.pro.changepassword;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>ChangePasswordDaggerModule</h1>
 * <p>This class is collection of object that bind with dagger.</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 24/06/2020
 **/
@Module
public abstract class ChangePasswordDaggerModule {

    @ActivityScoped
    @Binds
    abstract ChangePasswordContract.Presenter providePresenter(ChangePasswordPresenterImple changePasswordPresenterImple);

    @ActivityScoped
    @Binds
    abstract ChangePasswordContract.View provideView(ChangePasswordActivity changePasswordActivity);

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(ChangePasswordActivity activity);
}
