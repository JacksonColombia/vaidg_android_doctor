package com.vaidg.pro.utility;

import android.content.Context;
import android.util.Log;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.ServiceFactory;
import com.vaidg.pro.pojo.cognitoid.CognitoIdRespomse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.MESSAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.AMAZON_BASE_URL;
import static com.vaidg.pro.utility.VariableConstant.BUCKET_NAME;
import static com.vaidg.pro.utility.VariableConstant.COGNITO_POOL_ID;
import static com.vaidg.pro.utility.VariableConstant.COGNITO_TOKEN;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

public class UploadFileAmazonS3 {
    private static UploadFileAmazonS3 uploadAmazonS3;
    private static SessionManager sessionManager;
    private CognitoCachingCredentialsProvider credentialsProvider = null;
    private AmazonS3Client s3Client = null;
    private TransferUtility transferUtility = null;
    private DeveloperAuthenticationProvider developerProvider;

    /**
     * Creating single tone object by defining private.
     * <p>
     * At the time of creating
     * </P>
     */
    private UploadFileAmazonS3(Context context) {
        /**
         * Creating the object of the getCredentialProvider object. */
        // credentialsProvider = getCredentialProvider(context);
        /**
         * Creating the object  of the s3Client */
      //  s3Client = getS3Client(credentialsProvider);

        /**
         * Creating the object of the TransferUtility of the Amazone.*/
       // transferUtility = getTransferUtility(context, s3Client);

    }

    public static UploadFileAmazonS3 getInstance(Context context) {
        sessionManager = SessionManager.getSessionManager(context);
        if (uploadAmazonS3 == null) {
            uploadAmazonS3 = new UploadFileAmazonS3(context);
            return uploadAmazonS3;
        } else {
            return uploadAmazonS3;
        }
    }

    /**
     * This method is used to upload the image on AWS.
     *
     * @param bucket_name , contains the bucket name.
     * @param key         , contains the folder structure with file name.
     * @param file,       contains the actual file.
     * @param callBack,   provides the instance of user defined callback.
     */
    public void Upload_data(final String bucket_name, final String key, final File file, final UploadCallBack callBack) {
        getCognitoToken(bucket_name, file, callBack);
    }

    private void getCognitoToken(String bukkate_name, File file,
                                 UploadCallBack callBack) {

        NetworkService networkService = ServiceFactory.getClient(NetworkService.class,AppController.getInstance());

        Observable<Response<ResponseBody>> obsResponseBody = networkService.onTOGetCognitoToken(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM);
        obsResponseBody.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> value) {
                        //  Log.d(TAG, "onNext: " + value.code());
                        int code = value.code();
                        try {
                            String response = value.body() != null ? value.body().string() : null;
                            String errorBody = value.errorBody() != null ? value.errorBody().string() : null;
                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        CognitoIdRespomse cognitoIdRespomse = new Gson().fromJson(response,
                                                CognitoIdRespomse.class);
                                        BUCKET_NAME = cognitoIdRespomse.getData().getBucket();
                                        COGNITO_POOL_ID = cognitoIdRespomse.getData().getIdentityId();
                                        COGNITO_TOKEN = cognitoIdRespomse.getData().getToken();
                                        //Constants.AmazonRegion = cognitoIdRespomse.getData().getRegion();
                                        developerProvider = new DeveloperAuthenticationProvider(null,
                                                cognitoIdRespomse.getData().getIdentityId(), Regions.fromName(cognitoIdRespomse.getData().getRegion()));

                                        onSuccessCognitoTo(bukkate_name, file, callBack,cognitoIdRespomse.getData().getRegion());
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()),  sessionManager.getRefreshAuthToken(), networkService, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                getCognitoToken(bukkate_name, file,
                                                        callBack);

                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                AppController.getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, AppController.getInstance());
                                            }
                                        });

                                    }
                                        break;
                                default:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        AppController.getInstance().toast(Utility.getMessage(errorBody));
                                    }
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

    private void onSuccessCognitoTo(String bukkate_name, File file,
                                    UploadCallBack callBack,String region) {
        getCredentialProvider(region);

        if (transferUtility != null && file != null) {

            TransferObserver observer = transferUtility.upload(BUCKET_NAME,
                    bukkate_name + "/" + file.getName(), file, CannedAccessControlList.PublicRead);

            observer.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (state.equals(TransferState.COMPLETED)) {
                        callBack.sucess("https://" + BUCKET_NAME + "." + AMAZON_BASE_URL
                                + bukkate_name + "/"
                                + file.getName());

                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

                }

                @Override
                public void onError(int id, Exception ex) {
                    Log.e("Error", id + ":" + ex.toString());
                    callBack.error("Unable to upload file . Check your internet connection and Retry!");
                }
            });


        } else {
            callBack.error("Amamzon s3 is not intialize or File is empty !");
        }

    }


    /**
     * <h3>Upload_data</h3>
     * <p>
     * Method is use to download file from amazone.
     * </P>
     *
     * @param bukkate_name      The name of the bucket containing the object to download.
     * @param keyname           The key under which the object to download is stored.
     * @param StorageFile       The file to download the object's data to.
     * @param download_callBack return sucess and failure of the amazone.
     */

    public void download_File(String bukkate_name, String keyname, File StorageFile, final Download_CallBack download_callBack) {
        TransferObserver transferObserver = transferUtility.download(bukkate_name, keyname, StorageFile);
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.equals(TransferState.COMPLETED)) {
                    download_callBack.sucess("Sucess !");

                } else if (state.equals(TransferState.CANCELED)) {
                    download_callBack.error("Canceled !");
                }

            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int percentage = (int) (bytesCurrent / bytesTotal * 100);
            }

            @Override
            public void onError(int id, Exception ex) {
                download_callBack.error(ex.toString());
            }
        });
    }

    /**
     * <h2>delete_Item</h2>
     * <P>Deleting the item from aws.</P>
     *
     * @param bucketName Aws folder name
     * @param keyName    file name in the aws folder.
     */
    public void delete_Item(String bucketName, String keyName) {
        try {
            if (s3Client != null)
                s3Client.deleteObject(new DeleteObjectRequest(bucketName, keyName));
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("BookingStatus ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }

    /**
     * This method is used to get the CredentialProvider and we provide only context as a parameter.
     *
     * Here, we are getting the context from calling Activity.
     */
    private CognitoCachingCredentialsProvider getCredentialProvider(String  region) {
        //Call this method from success response of fetch token
        if (!COGNITO_TOKEN.isEmpty()
                && !COGNITO_POOL_ID.isEmpty()) {

            developerProvider.updateCredentials();

            credentialsProvider = new CognitoCachingCredentialsProvider(AppController.getInstance(), developerProvider, Regions.fromName(region));
            /* *
             * Creating the object  of the s3Client */
            s3Client = getS3Client();

            /* *
             * Creating the object of the TransferUtility of the Amazone.*/
            transferUtility = getTransferUtility(AppController.getInstance(), s3Client);
        }

       /* if (credentialsProvider == null)
        {
            credentialsProvider = new CognitoCachingCredentialsProvider(
                    context.getApplicationContext(),
                    pool_id, // Identity Pool ID
                    Regions.US_EAST_1 // Region//us-east-1
                    //Regions.AP_NORTHEAST_1 // Region
            );
        }*/
        return credentialsProvider;
    }

    /**
     * This method is used to get the AmazonS3 Client
     * and we provide only context as a parameter.
     * and from here we are calling getCredentialProvider() function.
     */
    private AmazonS3Client getS3Client() {
        s3Client = new AmazonS3Client(credentialsProvider);
      /*  if (s3Client == null)
        {
            s3Client = new AmazonS3Client(cognitoCachingCredentialsProvider);
        }*/
        return s3Client;
    }

    /**
     * This method is used to get the Transfer Utility
     * and we provide only context as a parameter.
     * and from here we are, calling getS3Client() function.
     *
     * @param context Here, we are getting the context from calling Activity.
     */
    private TransferUtility getTransferUtility(Context context, AmazonS3Client amazonS3Client) {
        if (transferUtility == null) {
            transferUtility = new TransferUtility(amazonS3Client, context.getApplicationContext());
        }
        return transferUtility;
    }


    /**
     * Interface for the sucess callback fro the Amazon uploading .
     */
    public interface UploadCallBack {
        /**
         * Method for sucess .
         *
         * @param sucess it is true on sucess and false for falure.
         */
        void sucess(String sucess);

        /**
         * Method for falure.
         *
         * @param errormsg contains the error message.
         */
        void error(String errormsg);

    }

    /**
     * Interface for the sucess callback for downloading file. .
     */
    public interface Download_CallBack {
        /**
         * Method for sucess .
         *
         * @param url it is true on sucess and false for falure.
         */
        void sucess(String url);

        /**
         * Method for falure.
         *
         * @param errormsg contains the error message.
         */
        void error(String errormsg);

    }
}
