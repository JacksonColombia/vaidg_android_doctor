package com.vaidg.pro.pojo.shedule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 05-Jul-17.
 */

public class ScheduleViewPojo implements Serializable {
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ScheduleData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ScheduleData getData() {
        return data;
    }

    public void setData(ScheduleData data) {
        this.data = data;
    }
}
