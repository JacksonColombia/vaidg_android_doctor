package com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.model;

import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.pojo.PreDefined;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class DocumentSelectionModel {

    public static final int TEXT_BOX = 1;
    public static final int DATE_TIME_CURRENT_TO_FUTURE = 2;
    public static final int DATE_TIME_PAST_TO_CURRENT = 4;
    public static final int UPLOAD_DOCUMENT = 3;

    @Inject
    public DocumentSelectionModel() {
    }

    @Inject
    List<PreDefined> preDefined;

    public void addPreDefinedData(List<PreDefined> list) {
        preDefined.addAll(list);
    }

    public List<PreDefined> getData() {
        return preDefined;
    }

    public void setData(int position, String data) {
        preDefined.get(position).setData(data);
    }

    public void deleteDocument(int position) {
        preDefined.get(position).setData("");
    }

    public void saveData(Attribute currentItem) {
        currentItem.setPreDefined(preDefined);
    }

    public void clearData() {
        for (PreDefined preDefined : preDefined) {
            preDefined.setData("");
        }
    }

}
