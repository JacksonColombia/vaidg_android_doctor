package com.vaidg.pro.main.profile.support;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.SupportListAdapter;
import com.vaidg.pro.databinding.ActivitySupportBinding;
import com.vaidg.pro.pojo.profile.support.SupportData;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>SupportActivity</h1>
 * SupportActivity for showing list of supports
 */

public class SupportActivity extends BaseDaggerActivity implements SupportContract.View {

    @Inject
    SupportContract.Presenter presenter;

    private ActivitySupportBinding binding;

    private ProgressDialog progressDialog;
    private String TAG = SupportActivity.this.getClass().getSimpleName();
    private ArrayList<SupportData> supportDataList;
    private SupportListAdapter supportListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySupportBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initViews();
        presenter.getSupport();
    }

    @Override
    public void onBackPressed() {
        Utility.progressDialogCancel(this, progressDialog);
        closeActivity();
    }

    private void closeActivity() {
        finishAfterTransition();
    }

    @Override
    public void initViews() {
        Typeface fontBold = Utility.getFontBold(this);

        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        binding.includeToolbar.tvTitle.setText(getString(R.string.faq));
        binding.includeToolbar.tvTitle.setTypeface(fontBold);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingSupport));
        progressDialog.setCancelable(false);

        supportDataList = new ArrayList<>();
        RecyclerView recyclerView = findViewById(R.id.rvSupport);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        supportListAdapter = new SupportListAdapter(this, supportDataList, true);
        recyclerView.setAdapter(supportListAdapter);
    }

    @Override
    public void showError(String failureMsg) {
        Snackbar snackbar = Snackbar.make(binding.clParent, failureMsg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void showError() {
        showError(getString(R.string.serverError));
    }

    @Override
    public void onSuccess(ArrayList<SupportData> supportDataList) {
        this.supportDataList.addAll(supportDataList);
        supportListAdapter.notifyDataSetChanged();
    }

    @Override
    public void showProgress() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void hideProgress() {
        Utility.progressDialogDismiss(this, progressDialog);
    }
}
