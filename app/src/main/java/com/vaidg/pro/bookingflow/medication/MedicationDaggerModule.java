package com.vaidg.pro.bookingflow.medication;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>LogInDaggerModule</h1>
 * <p>This dagger module created for LogInActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
@Module
public abstract class MedicationDaggerModule {
  @ActivityScoped
  @Binds
  abstract Activity getActivity(MedicationActivity activity);

  @ActivityScoped
  @Binds
  abstract MedicationContract.View getView(MedicationActivity activity);

  @ActivityScoped
  @Binds
  abstract MedicationContract.Presenter getPresenter(MedicationPresenterImple presenterImple);
}
