package com.vaidg.pro.main;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.appconfig.AppConfigData;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface MainActivityContract {

    interface View extends BaseView {
        void onSuccess(AppConfigData appConfigData, String pojo);

        void onFailure();

        void sessionExpired(String msg);

        void onNewToken(String newToken);

        void onFailure(String message);

        void onSuccesBooking(String result, boolean b);

    }

    interface Presenter extends BasePresenter {
        void getAppConfig();

        void getBookingByDate(final String startDate, final String endDate, final boolean isBackground);
    }
}