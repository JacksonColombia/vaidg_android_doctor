package com.vaidg.pro.main.profile.myratecard.categoryratebycalltype;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.ViewPagerAdapter;
import com.vaidg.pro.main.profile.myratecard.singlecategory.FixedFragment;
import com.vaidg.pro.main.profile.myratecard.singlecategory.HourlyFragment;
import com.vaidg.pro.pojo.profile.ratecard.CategoryData;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

public class OutCallRateCardFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static int position;
    private static ViewPagerAdapter viewPagerAdapter;
    private static ViewPager vpBooking;
    private LayoutInflater inflater;
    private HourlyFragment hourlyFragment;
    private Typeface fondBold, fontMedium, fontRegular;
    private FixedFragment fixedFragment;
    private CategoryData categoryData;
    private int currentPage = 0;
    private boolean setOptionMenu = false;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment InCallTeleCallRateCard.
     */
    // TODO: Rename and change types and number of parameters
    public static OutCallRateCardFragment newInstance(CategoryData categoryData, boolean isPriceSetByProvider) {
        OutCallRateCardFragment fragment = new OutCallRateCardFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, categoryData);
        args.putBoolean(ARG_PARAM2, isPriceSetByProvider);
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment getTabSelected() {
        return viewPagerAdapter.getItem(vpBooking.getCurrentItem());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.inflater = inflater;
        View rootView = inflater.inflate(R.layout.out_call_fragment, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {
        fondBold = Utility.getFontBold(getActivity());
        fontRegular = Utility.getFontRegular(getActivity());
        fontMedium = Utility.getFontMedium(getActivity());
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        TabLayout tablayoutBooking = rootView.findViewById(R.id.tablayoutMyEvents);
        vpBooking = rootView.findViewById(R.id.viewPagerCategory);
        TextView tvPriceType = rootView.findViewById(R.id.tvPriceType);
        tvPriceType.setTypeface(fontMedium);
        categoryData = (CategoryData) getArguments().getSerializable(ARG_PARAM1);
        position = getArguments().getInt("position", 0);
        switch (categoryData.getBilling_model()) {
            case VariableConstant.CATEGORY_FIXED_NON_EDIT:
                tvPriceType.setText(getString(R.string.priceSetByAdmin));
                fixedFragment = FixedFragment.newInstance(categoryData, false);
                break;

            case VariableConstant.CATEGORY_FIXED_EDIT:
                tvPriceType.setText(getString(R.string.priceSetByProvider));
                fixedFragment = FixedFragment.newInstance(categoryData, true);
                break;

            case VariableConstant.CATEGORY_HOURLY_NON_EDIT:
                tvPriceType.setText(getString(R.string.priceSetByAdmin));
                hourlyFragment = HourlyFragment.newInstance(categoryData, false);
                break;

            case VariableConstant.CATEGORY_HOURLY_EDIT:
                tvPriceType.setText(getString(R.string.priceSetByProvider));
                hourlyFragment = HourlyFragment.newInstance(categoryData, true);
                break;

            case VariableConstant.CATEGORY_FIXED_HOURLY_NON_EDIT:
                tvPriceType.setText(getString(R.string.priceSetByAdmin));
                hourlyFragment = HourlyFragment.newInstance(categoryData, false);
                fixedFragment = FixedFragment.newInstance(categoryData, false);
                break;

            case VariableConstant.CATEGORY_FIXED_HOURLY_EDIT:
                tvPriceType.setText(getString(R.string.priceSetByProvider));
                hourlyFragment = HourlyFragment.newInstance(categoryData, true);
                fixedFragment = FixedFragment.newInstance(categoryData, true);
                break;
        }

        if (hourlyFragment != null) {
            viewPagerAdapter.addFragment(hourlyFragment, getString(R.string.hourly));
        }
        if (fixedFragment != null) {
            viewPagerAdapter.addFragment(fixedFragment, getString(R.string.fixed));
        }

        vpBooking.setAdapter(viewPagerAdapter);
        tablayoutBooking.setupWithViewPager(vpBooking);
        vpBooking.addOnPageChangeListener(new PageListener());
    }

    public void setOptionMenu(boolean setOptionMenu) {
        this.setOptionMenu = setOptionMenu;
        if (setOptionMenu) {
            if (viewPagerAdapter.getItem(vpBooking.getCurrentItem()) instanceof HourlyFragment) {
                if (hourlyFragment != null)
                    hourlyFragment.setOptionMenu(true);
                if (fixedFragment != null)
                    fixedFragment.setOptionMenu(false);
            } else {
                if (fixedFragment != null)
                    fixedFragment.setOptionMenu(true);
                if (hourlyFragment != null)
                    hourlyFragment.setOptionMenu(false);
            }
        } else {
            if (hourlyFragment != null)
                hourlyFragment.setOptionMenu(false);
            if (fixedFragment != null)
                fixedFragment.setOptionMenu(false);
        }

    }

    private class PageListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            Utility.printLog("page selected ", "" + position);
            position = i;
            if (setOptionMenu) {
                if (viewPagerAdapter.getItem(vpBooking.getCurrentItem()) instanceof HourlyFragment) {
                    if (hourlyFragment != null)
                        hourlyFragment.setOptionMenu(true);
                    if (fixedFragment != null)
                        fixedFragment.setOptionMenu(false);
                } else {
                    if (fixedFragment != null)
                        fixedFragment.setOptionMenu(true);
                    if (hourlyFragment != null)
                        hourlyFragment.setOptionMenu(false);
                }
            }
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    }
}

