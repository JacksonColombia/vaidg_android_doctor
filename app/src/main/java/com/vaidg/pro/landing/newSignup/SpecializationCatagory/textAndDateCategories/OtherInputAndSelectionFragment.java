package com.vaidg.pro.landing.newSignup.SpecializationCatagory.textAndDateCategories;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.vaidg.pro.R;
import com.vaidg.pro.databinding.FragmentOtherInputAndSelectionBinding;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.SpecializationCategoryContract;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.utility.DatePickerCommon;
import com.vaidg.pro.utility.Utility;

import org.jetbrains.annotations.NotNull;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_TIME_0_23_HRS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_TIME_1_12_HRS;

/**
 * <h1>@RegisterationNumberFragment</h1>
 * <p>this fragment is used to displaying  category  type 4,6,7,8,9,10,11
 * this fragment is re used
 * TYPE NUMBER = 4   this is for getting number by edittext view
 * TYPE FEE = 6    this is for getting fee by edittext view
 * TYPE TEXT_AREA = 7  this is for getting text by edittext view
 * TYPE TEXT_BOX = 8 this is for getting paragrams by edittext view
 * TYPE DATE_FUTURE = 9  this is for getting  date from current to future
 * TYPE DATE_PAST = 10 this is for getting date from past to current
 * TYPE TIME = 11      this is for getting time
 * </P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class OtherInputAndSelectionFragment extends DaggerFragment implements OtherInputAndSelectionContract.View, DatePickerCommon.DateSelected {

    private static final int NUMBER = 4;
    private static final int FEE = 6;
    private static final int TEXT_AREA = 7;
    private static final int TEXT_BOX = 8;
    private static final int DATE_FUTURE = 9;
    private static final int DATE_PAST = 10;
    private static final int TIME = 11;

    private static final String TAG = OtherInputAndSelectionFragment.class.getSimpleName();

    @Inject
    OtherInputAndSelectionContract.Presenter presenter;
    @Inject
    Activity activity;
    @Inject
    SpecializationCategoryContract.Presenter mainPresenter;
    @Inject
    DatePickerCommon datePickerFragment;

    private FragmentOtherInputAndSelectionBinding binding;
    private Attribute currentItem;
    private int list_number;
    private int type;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        assert bundle != null;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentOtherInputAndSelectionBinding.inflate(inflater);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setProgressBar();
        initOnclickLister();
    }

    private void initOnclickLister() {
        binding.includeToolbar.btnDone.setOnClickListener(this::onclick);
        binding.etOtherInputDateTime.setOnClickListener(this::onclick);
    }

    private void onclick(View view) {
        switch (view.getId()) {
            case R.id.btnDone:
                Utility.hideKeyboad(activity);
                String enteredData = "";
                switch (type) {
                    case FEE:
                    case NUMBER:
                        enteredData = binding.etOtherInputNumber.getText() == null ? "" : binding.etOtherInputNumber.getText().toString();
                        break;

                    case TEXT_AREA:
                    case TEXT_BOX:
                        enteredData = binding.etOtherInputTextArea.getText() == null ? "" : binding.etOtherInputTextArea.getText().toString();
                        break;

                    case TIME:
                    case DATE_PAST:
                    case DATE_FUTURE:
                        enteredData = binding.etOtherInputDateTime.getText() == null ? "" : binding.etOtherInputDateTime.getText().toString();
                        break;

                }
                presenter.valid(enteredData);
                break;

            case R.id.etOtherInputDateTime:
                if (type == TIME) {
                    getTimePicker();
                } else {
                    getDatePicker();
                }
                break;
        }
    }

    private void setProgressBar() {
        binding.progressBar.setMax(currentItem.getMax_count());
        binding.progressBar.setProgress(currentItem.getPosition() + 1);
    }

    /**
     * <p>Setting the current preference.</p>
     *
     * @param item the current attribute
     */
    public void setPreferenceItem(Attribute item) {
        currentItem = item;
        list_number = currentItem.getList_no();
        type = currentItem.getType();
    }

    @Override
    public boolean isMandatoryForProvider() {
        return currentItem.getMandatoryForProvider();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.attachView(this);
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public void showError(String error) {
        mainPresenter.showError(error);
    }

    @Override
    public void moveNextFrag() {
        saveData(type);
        mainPresenter.openNextFrag(list_number, currentItem.getPosition() + 1, currentItem);
    }

    public void initViews() {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        binding.includeToolbar.tvTitle.setText(getString(R.string.complete_your_profile));
        binding.includeToolbar.btnDone.setText(getString(R.string.next));
        if (activity != null) {
            activity.setSupportActionBar(binding.includeToolbar.toolbar);
            if (activity.getSupportActionBar() != null) {
                activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
                activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
                binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> activity.onBackPressed());
            }
        }

        datePickerFragment.setCallBack(this);

        binding.tvOtherInputTitle.setText(currentItem.getQueForProviderSignup());

        switch (type) {

            case NUMBER:
            case FEE:
                binding.etOtherInputNumber.setVisibility(View.VISIBLE);
                binding.etOtherInputNumber.setHint(currentItem.getName());
                binding.tilOtherInputTextArea.setVisibility(View.GONE);
                binding.tilOtherInputDateTime.setVisibility(View.GONE);
                binding.etOtherInputNumber.requestFocus();
                binding.etOtherInputNumber.setImeOptions(EditorInfo.IME_ACTION_DONE);
                binding.etOtherInputNumber.setRawInputType(InputType.TYPE_CLASS_NUMBER);
                break;

            case TEXT_AREA:
            case TEXT_BOX:
                binding.tilOtherInputTextArea.setVisibility(View.VISIBLE);
                binding.etOtherInputNumber.setVisibility(View.GONE);
                binding.tilOtherInputDateTime.setVisibility(View.GONE);
                if (type == TEXT_AREA) {
                    binding.etOtherInputTextArea.setImeOptions(EditorInfo.IME_ACTION_DONE);
                    binding.etOtherInputTextArea.setRawInputType(InputType.TYPE_CLASS_TEXT);
                }
                binding.etOtherInputTextArea.setHint(currentItem.getName());
                if (currentItem.getMaximum() != null && !currentItem.getMaximum().isEmpty() && TextUtils.isDigitsOnly(currentItem.getMaximum())) {
                    int maxChar = Integer.parseInt(currentItem.getMaximum());
                    binding.etOtherInputTextArea.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxChar)});
                    binding.tilOtherInputTextArea.setCounterMaxLength(maxChar);
                }
                break;

            case TIME:
            case DATE_PAST:
            case DATE_FUTURE:
                binding.etOtherInputDateTime.setHint(currentItem.getName());
                binding.tilOtherInputDateTime.setVisibility(View.VISIBLE);
                binding.tilOtherInputTextArea.setVisibility(View.GONE);
                binding.etOtherInputNumber.setVisibility(View.GONE);
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }

        binding.etOtherInputTextArea.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (type == TEXT_BOX) {
                    String str = s.toString();
                    if (str.endsWith("\n") && !str.endsWith(",\n")) {
                        str = str.substring(0, s.length() - 1).concat(",\n");
                        binding.etOtherInputTextArea.setText(str);
                        binding.etOtherInputTextArea.setSelection(str.length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void getTimePicker() {
        final Calendar c = Calendar.getInstance();
        c.setTimeZone(getInstance().getTimeZone());
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                (view, hourOfDay, minute) -> {
                    c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    c.set(Calendar.MINUTE, minute);
                    Time time = new Time(c.getTimeInMillis());
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateFormat.is24HourFormat(getContext()) ? FORMAT_TIME_0_23_HRS : FORMAT_TIME_1_12_HRS, Locale.US);
                    simpleDateFormat.setTimeZone(getInstance().getTimeZone());
                    binding.etOtherInputDateTime.setText(simpleDateFormat.format(time));
                }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), DateFormat.is24HourFormat(getContext()));
        timePickerDialog.show();
    }

    private void getDatePicker() {
//        final Calendar c = Calendar.getInstance();
//        long currentTimeStamp = new GregorianCalendar(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH))
//                .getTimeInMillis();
//        DatePickerDialog datePickerDialog = new DatePickerDialog(Objects.requireNonNull(getContext()),
//                (view, year, monthOfYear, dayOfMonth) -> {
//                    binding.etOtherInputDateTime.setText(String.format(Locale.US, "%02d-%02d-%02d", year, (monthOfYear + 1), dayOfMonth));
//                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
//        datePickerDialog.show();
//        DatePicker dp = datePickerDialog.getDatePicker();
//        switch (type) {
//            case DATE_FUTURE:
//                dp.setMinDate(currentTimeStamp);
//                break;
//            case DATE_PAST:
//                dp.setMaxDate(currentTimeStamp);
//                break;
//        }

        if (!datePickerFragment.isResumed()) {
            datePickerFragment.setDatePickerType(type == DATE_FUTURE ? 3 : 4);
            datePickerFragment.show(getChildFragmentManager(), "dataPicker");
        }
    }

    @Override
    public void onDateSelected(String sendingFormat, String displayFormat) {
        binding.etOtherInputDateTime.setText(sendingFormat);
    }

    private void saveData(int type) {
        switch (type) {
            case NUMBER:
            case FEE:
                currentItem.setData(binding.etOtherInputNumber.getText() == null ? "" : binding.etOtherInputNumber.getText().toString());
                break;

            case TEXT_AREA:
            case TEXT_BOX:
                currentItem.setData(binding.etOtherInputTextArea.getText() == null ? "" : binding.etOtherInputTextArea.getText().toString());
                break;

            case TIME:
                try {
                    currentItem.setData(binding.etOtherInputDateTime.getText() == null ? "" : Utility.changeDateTimeFormat(binding.etOtherInputDateTime.getText().toString()
                            , DateFormat.is24HourFormat(getContext()) ? FORMAT_TIME_0_23_HRS : FORMAT_TIME_1_12_HRS
                            , FORMAT_TIME_0_23_HRS));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case DATE_PAST:
            case DATE_FUTURE:
                currentItem.setData(binding.etOtherInputDateTime.getText() == null ? "" : binding.etOtherInputDateTime.getText().toString());
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }
    }
}
