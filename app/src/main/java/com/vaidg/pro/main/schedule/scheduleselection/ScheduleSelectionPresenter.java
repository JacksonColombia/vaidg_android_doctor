package com.vaidg.pro.main.schedule.scheduleselection;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;

import com.vaidg.pro.R;
import com.vaidg.pro.utility.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import static com.vaidg.pro.AppController.getInstance;

/**
 * Created by murashid on 03-Oct-17.
 */

public class ScheduleSelectionPresenter implements DatePickerDialog.OnDateSetListener, ScheculeSelectionContract.Presenter {

    private static final String TAG = "Presenter";
    @Inject
    Activity context;
    @Inject
    ScheculeSelectionContract.View view;
    private int datePickerVarID = 0;
    private Date startDay = new Date();
    private long startTime = 0;
    private SimpleDateFormat sendingFormat, displayFormat;

    @Inject
    ScheduleSelectionPresenter() {

    }

    @Override
    public void startDay() {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(getInstance().getTimeZone());
        String startDate = String.format("%d-%02d-%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
        view.startDay(startDate);
    }

    @Override
    public void endDay(int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(getInstance().getTimeZone());
        cal.setTime(new Date());
        int monthMaxDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        int remainingdays = monthMaxDays - cal.get(Calendar.DAY_OF_MONTH);
        days = days + remainingdays;
        cal.add(Calendar.DATE, days);
        String endDate = String.format("%d-%02d-%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
        view.endDay(endDate);
    }


    @Override
    public void setMinimumDateForAlreadySelectedStartDate(String startDate) {
        sendingFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        sendingFormat.setTimeZone(getInstance().getTimeZone());
        try {
            startDay = sendingFormat.parse(startDate);
            startTime = startDay.getTime();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void selectDate(int datePickerVarID, String lastSelectedDate) {
        this.datePickerVarID = datePickerVarID;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(getInstance().getTimeZone());
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int date = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = null;

        Log.d(TAG, "selectDate: " + lastSelectedDate);

        if (!lastSelectedDate.equals("")) {
            String[] lastSelected = lastSelectedDate.split("-");
            year = Integer.parseInt(lastSelected[0]);
            month = Integer.parseInt(lastSelected[1]) - 1;
            date = Integer.parseInt(lastSelected[2]);
        }

        if (datePickerVarID == 2) {
            if (startTime == 0) {
                datePickerDialog = new DatePickerDialog(context, this, year, month, date/*date + 1*/);
              //  calendar.add(Calendar.DAY_OF_YEAR, 1);
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis() - 1000);
            } else {
                if (lastSelectedDate.equals("")) {
                    year = calendar.get(Calendar.YEAR);
                    month = calendar.get(Calendar.MONTH);
                    date = calendar.get(Calendar.DAY_OF_MONTH);
                }
               // calendar.setTime(startDay);
               // calendar.add(Calendar.DAY_OF_YEAR, 1);
                datePickerDialog = new DatePickerDialog(context, this, year, month, date);
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis()-1000);
            }
        } else {
            datePickerDialog = new DatePickerDialog(context, this, year, month, date);
            datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis() - 1000);
        }

        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        displayFormat = new SimpleDateFormat("MMM yyyy", Locale.US);
        sendingFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        sendingFormat.setTimeZone(getInstance().getTimeZone());
        displayFormat.setTimeZone(getInstance().getTimeZone());
        if (datePickerVarID == 1) {
            String startDate = String.format("%d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);

            datePickerVarID = 0;

            try {
                startDay = sendingFormat.parse(startDate);
                startTime = startDay.getTime();
                view.startDayCalendar(startDate, Utility.getDayOfMonthSuffix(dayOfMonth, displayFormat.format(startDay)));

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (datePickerVarID == 2) {
            String endDate = String.format("%d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
            try {
                view.endDayCalendar(endDate, Utility.getDayOfMonthSuffix(dayOfMonth, displayFormat.format(sendingFormat.parse(endDate))));
                datePickerVarID = 0;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void addRemoveDays(ArrayList<String> selectedDays, String days, ImageView ivDay) {
        if (!selectedDays.contains(days)) {
            view.addDays(days);
            ivDay.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.vector_color_primary_add_schedule_tick));
        } else {
            view.removeDays(days);
            ivDay.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.vector_add_schedule_tick_color_grey));
        }
    }


    @Override
    public ArrayList<String> sortDays(ArrayList<String> selectedDays) {
        ArrayList<String> sorteddays = new ArrayList<>();

        if (selectedDays.contains(context.getString(R.string.monday))) {
            sorteddays.add(context.getString(R.string.monday));
        }
        if (selectedDays.contains(context.getString(R.string.tuesday))) {
            sorteddays.add(context.getString(R.string.tuesday));
        }
        if (selectedDays.contains(context.getString(R.string.wednesday))) {
            sorteddays.add(context.getString(R.string.wednesday));
        }
        if (selectedDays.contains(context.getString(R.string.thursday))) {
            sorteddays.add(context.getString(R.string.thursday));
        }
        if (selectedDays.contains(context.getString(R.string.friday))) {
            sorteddays.add(context.getString(R.string.friday));
        }
        if (selectedDays.contains(context.getString(R.string.saturday))) {
            sorteddays.add(context.getString(R.string.saturday));
        }
        if (selectedDays.contains(context.getString(R.string.sunday))) {
            sorteddays.add(context.getString(R.string.sunday));
        }
        return sorteddays;
    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    /**
     * ScheduleSelectionview interface for view implementation
     */
    interface ScheduleSelectionview {
        void startDay(String startDay);

        void endDay(String endDay);

        void startDayCalendar(String startDay, String displayStartDay);

        void endDayCalendar(String endDay, String displayEndDay);

        void addDays(String days);

        void removeDays(String days);
    }

}
