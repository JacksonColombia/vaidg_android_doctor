package com.vaidg.pro.pojo.appconfig;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 25-Oct-17.
 */

public class AppConfigData implements Serializable {
    public static final String PLATFORM = "ANDROID";
    public static final String MESSAGE = "message";
    public static final String DATA = "data";
    public static String AUTH_KEY = "123";
    public static String DEFAULT_LANGUAGE = "en";
    public static String DEFAULT_DISPLAYLANGUAGE = "English";
    @SerializedName("currencySymbol")
    @Expose
    private String currencySymbol;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("RAZORPAY_ID")
    @Expose
    private String razorPayKey;
    @SerializedName("currencyAbbr")
    @Expose
    private String currencyAbbr;//1 = > prefix 2 = > suffix
    @SerializedName("distanceMatrix")
    @Expose
    private String distanceMatrix; // 0 => kilometer 1  = > Miles
    @SerializedName("latLongDisplacement")
    @Expose
    private String latLongDisplacement = "10";
    @SerializedName("stripeKeys")
    @Expose
    private String stripeKeys;
    @SerializedName("appVersion")
    @Expose
    private String appVersion;
    @SerializedName("mandatory")
    @Expose
    private String mandatory;
    @SerializedName("providerFrequency")
    @Expose
    private ProviderFrequency providerFrequency;
    @SerializedName("pushTopics")
    @Expose
    private PushTopics pushTopics;
    @SerializedName("walletData")
    @Expose
    private WalletDataAppConfic walletData;
    @SerializedName("paymentGateways")
    @Expose
    private ArrayList<PaymentGateways> paymentGateways;

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public ArrayList<PaymentGateways> getPaymentGateways() {
        return paymentGateways;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyAbbr() {
        return currencyAbbr;
    }

    public void setCurrencyAbbr(String currencyAbbr) {
        this.currencyAbbr = currencyAbbr;
    }

    public String getDistanceMatrix() {
        return distanceMatrix;
    }

    public void setDistanceMatrix(String distanceMatrix) {
        this.distanceMatrix = distanceMatrix;
    }

    public String getLatLongDisplacement() {
        return latLongDisplacement;
    }

    public void setLatLongDisplacement(String latLongDisplacement) {
        this.latLongDisplacement = latLongDisplacement;
    }

    public String getStripeKeys() {
        return stripeKeys;
    }

    public void setStripeKeys(String stripeKeys) {
        this.stripeKeys = stripeKeys;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public ProviderFrequency getProviderFrequency() {
        return providerFrequency;
    }

    public void setProviderFrequency(ProviderFrequency providerFrequency) {
        this.providerFrequency = providerFrequency;
    }

    public PushTopics getPushTopics() {
        return pushTopics;
    }

    public void setPushTopics(PushTopics pushTopics) {
        this.pushTopics = pushTopics;
    }

    public WalletDataAppConfic getWalletData() {
        return walletData;
    }

    public void setWalletData(WalletDataAppConfic walletData) {
        this.walletData = walletData;
    }

    public String getRazorPayKey() {
        return razorPayKey;
    }
}
