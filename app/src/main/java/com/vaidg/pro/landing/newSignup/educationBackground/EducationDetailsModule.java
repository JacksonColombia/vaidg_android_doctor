package com.vaidg.pro.landing.newSignup.educationBackground;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>@EducationDetailsModule</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 27/04/2020.
 **/
@Module
public abstract class EducationDetailsModule {

    @ActivityScoped
    @Binds
    abstract EducationDetailsContract.Presenter providePresenter(EducationDetailsPresenter presenter);

    @ActivityScoped
    @Binds
    abstract EducationDetailsContract.View provideView(EducationDetailsActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(EducationDetailsActivity activity);
}
