package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankRazorPayResponse implements Parcelable
{

	@SerializedName("message")
	@Expose
	private String message;
	@SerializedName("data")
	@Expose
	private BankRazorPayData data;
	public final static Creator<BankRazorPayResponse> CREATOR = new Creator<BankRazorPayResponse>() {


		@SuppressWarnings({
				"unchecked"
		})
		public BankRazorPayResponse createFromParcel(Parcel in) {
			return new BankRazorPayResponse(in);
		}

		public BankRazorPayResponse[] newArray(int size) {
			return (new BankRazorPayResponse[size]);
		}

	}
			;

	protected BankRazorPayResponse(Parcel in) {
		this.message = ((String) in.readValue((String.class.getClassLoader())));
		this.data = ((BankRazorPayData) in.readValue((BankRazorPayData.class.getClassLoader())));
	}

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public BankRazorPayResponse() {
	}

	/**
	 *
	 * @param data
	 * @param message
	 */
	public BankRazorPayResponse(String message, BankRazorPayData data) {
		super();
		this.message = message;
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public BankRazorPayData getData() {
		return data;
	}

	public void setData(BankRazorPayData data) {
		this.data = data;
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(message);
		dest.writeValue(data);
	}

	public int describeContents() {
		return 0;
	}

}