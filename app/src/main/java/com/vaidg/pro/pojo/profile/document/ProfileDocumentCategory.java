package com.vaidg.pro.pojo.profile.document;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vaidg.pro.pojo.signup.CategoryDocument;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 22-Mar-18.
 */

public class ProfileDocumentCategory implements Serializable {
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;

    private ArrayList<CategoryDocument> documents;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public ArrayList<CategoryDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<CategoryDocument> documents) {
        this.documents = documents;
    }
}
