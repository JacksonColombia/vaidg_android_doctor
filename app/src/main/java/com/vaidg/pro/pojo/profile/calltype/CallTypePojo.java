package com.vaidg.pro.pojo.profile.calltype;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 10-Sep-17.
 */

public class CallTypePojo implements Serializable {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<CallTypeData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CallTypeData> getData() {
        return data;
    }

    public void setData(ArrayList<CallTypeData> data) {
        this.data = data;
    }
}
