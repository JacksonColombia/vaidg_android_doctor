package com.kotlintestgradle.repository

interface UserRepository {
    fun isLogged(): Boolean
    fun getUserId(): String
    fun getMQTTTopic(): String
    fun getWillopic(): String
    fun getFcmTopic(): String
    fun setAuthToken(authToken:String)
    fun setLanguageCode(lang: String)
    fun setUserId(userId: String)
}