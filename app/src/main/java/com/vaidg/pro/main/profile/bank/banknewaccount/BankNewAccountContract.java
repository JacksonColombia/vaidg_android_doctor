package com.vaidg.pro.main.profile.bank.banknewaccount;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import org.json.JSONObject;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface BankNewAccountContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onSuccess(String msg);

    void onFailure(String msg);

    void onFailure();

    void onNameError();

    void onErrorEmail();

    void onAccountNumberError();

    void onRoutingNumberError();

    void onCountryError();

    void onNoError();

  }

  interface Presenter extends BasePresenter {


    /**
     * method for calling api for adding bank account and check the field is empty or not
     *
     * @param jsonObject required field in json objec
     */
    void addBankDetails(JSONObject jsonObject);

  }
}