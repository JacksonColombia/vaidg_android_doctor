package com.vaidg.pro.landing.newSignup.selfOwnedClinic;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class SelfOwnedClinicDaggerModule {

    @ActivityScoped
    @Binds
    abstract SelfOwnedClinicContract.View provideView(SelfOwnedClinicActivity activity);

    @ActivityScoped
    @Binds
    abstract SelfOwnedClinicContract.Presenter providePresenter(SelfOwnedClinicPresenter presenter);

    @ActivityScoped
    @Binds
    abstract Activity provide(SelfOwnedClinicActivity activity);
}
