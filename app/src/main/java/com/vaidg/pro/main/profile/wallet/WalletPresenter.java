package com.vaidg.pro.main.profile.wallet;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Context;
import com.appscrip.stripe.AccountsDelegate;
import com.appscrip.stripe.UserAccounts;
import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.profile.wallet.CardData;
import com.vaidg.pro.pojo.profile.wallet.CardPojo;
import com.vaidg.pro.pojo.profile.wallet.WalletData;
import com.vaidg.pro.pojo.profile.wallet.WalletPojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.ArrayList;
import retrofit2.Response;

/**
 * Created by murashid on 28-Mar-18.
 */
public class WalletPresenter implements WalletContract.Presenter{
    private static final String TAG = "WalletPresenter";
    @Inject
    Context context;
    @Inject
    WalletContract.View view;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;

    @Inject
    WalletPresenter() {
    }


    @Override
    public void getWalletDetails() {
        view.startProgressBar();
        service.getPaymentsettings(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                 view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    WalletPojo walletPojo = new Gson().fromJson(response, WalletPojo.class);
                                    view.onSuccessWalletDetails(walletPojo.getData());
                                } else
                                    view.onFailure();
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        getWalletDetails();
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        getInstance().toast(Utility.getMessage(msg));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                    }
                                });
                                view.stopProgressBar();
                                break;
                            case SESSION_LOGOUT:
                                view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                Utility.logoutSessionExiperd(sessionManager, context);
                                break;
                            default:
                                view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                break;
                        }
                    } catch (Exception e) {
                        view.stopProgressBar();
                        view.onFailure();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    view.stopProgressBar();
                    view.onFailure();
                }

                @Override
                public void onComplete() {
                }
            });
    }


    @Override
    public  void rechargeWallet(JSONObject jsonObject , String selectedCardId) {
        if (selectedCardId.isEmpty()) {
            if(view != null)
            view.onErrorCardSelection();
        } else {
            if(view != null)
            view.startProgressBar();

            service.addRechargeWallet (Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if(view != null)
                                    view.stopProgressBar();
                                    if (response != null && !response.isEmpty()) {
                                        if(view != null)
                                        view.onSuccessWalletRecharge(Utility.getMessage(response));
                                    } else {
                                        if (view != null)
                                            view.onFailure();
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            if(view != null)
                                                view.stopProgressBar();
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            rechargeWallet(jsonObject, sessionManager.getSelectedCardId());
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            if(view != null)
                                            {
                                                view.stopProgressBar();
                                                view.onFailure();
                                            }
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null)
                                                view.stopProgressBar();
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, context);
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    if(view != null)
                                    view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, context);
                                    break;
                                default:
                                    if(view != null)
                                    view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            if(view != null) {
                                view.stopProgressBar();
                                view.onFailure();
                            }
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
        }
    }


    @Override
    public void getCardDetails() {
        if(view != null)
        view.startProgressBar();
        UserAccounts.INSTANCE.getCards(Utility.getAuth(sessionManager.getEmail()),DEFAULT_LANGUAGE, sessionManager.getProviderId(), new AccountsDelegate() {
            @Override
            public void onSuccess(@NotNull Object successData) {
                CardPojo card_pojo = new Gson().fromJson(successData.toString(), CardPojo.class);
                if(view != null)
                view.onSuccessCardDetails(card_pojo.getData());
            }

            @Override
            public void onFailure(@NotNull String failure) {
                if(view != null)
                    view.stopProgressBar();
            }
        });

    }

    @Override
    public void validate(String selectedCardNumber, String amount) {
        if (selectedCardNumber.equals("")) {
            view.onErrorCardSelection();
        } else if (amount.equals("")) {
            view.onErrorAmount();
        } else {
            view.successValidation();
        }
    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    interface View {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String failureMsg);

        void onFailure();

        void onSuccessWalletDetails(WalletData walletData);

        void onSuccessWalletRecharge(String msg);

        void onSuccessCardDetails(ArrayList<CardData> data);

        void onErrorCardSelection();

        void onErrorAmount();

        void successValidation();

        void onNewToken(String newToken);

        void sessionExpired(String msg);
    }
}
