package com.vaidg.pro.landing.introsilider2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import androidx.appcompat.widget.ListPopupWindow;

import androidx.viewpager2.widget.ViewPager2;

import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityIntroSliderNewBinding;
import com.vaidg.pro.landing.introsilider2.model.IntroSliderAdapter;
import com.vaidg.pro.landing.introslider.IntroSliderActivity;
import com.vaidg.pro.landing.newLogin.NewLogInActivity;
import com.vaidg.pro.landing.newSignup.SignUpActivity;
import com.vaidg.pro.pojo.appconfig.AppConfigData;
import com.vaidg.pro.pojo.introslider.SliderData;
import com.vaidg.pro.pojo.language.LanguageData;
import com.vaidg.pro.utility.DialogHelper;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_DISPLAYLANGUAGE;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;

/**
 * <h1>IntroSliderNewActivity</h1>
 * <p>This activity is load after splash screen as an landing screen and it's contain slider info panel and login and sign up options.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
public class IntroSliderNewActivity extends BaseDaggerActivity implements IntroSliderContract.View, AdapterView.OnItemClickListener {

    @Inject
    IntroSliderContract.Presenter presenter;

    @Inject
    IntroSliderAdapter introSliderAdapter;

    private ActivityIntroSliderNewBinding binding;

    private int mSliderCurrentPage = 0;

    private Timer sliderTimer;

    private String msg = null;

    private boolean isResume = false;

    private ListPopupWindow listPopupWindow;

    private ArrayList<LanguageData> languageData = new ArrayList<>();

    private String[] languages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityIntroSliderNewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
        initViewOnClickListeners();
    }

    private void initViewOnClickListeners() {
        binding.btnIntroSliderLanguage.setOnClickListener(this::onClick);
        binding.btnIntroSliderLogin.setOnClickListener(this::onClick);
        binding.btnIntroSliderSignUp.setOnClickListener(this::onClick);
    }

    @Override
    public void initViews() {
        msg = getIntent().getStringExtra("msg");
        binding.btnIntroSliderLanguage.setText(DEFAULT_DISPLAYLANGUAGE);
        presenter.getLanguage();
        setupSlider();
    }

    @Override
    public void onResume() {
        super.onResume();
        isResume = true;
        if (msg != null) {
            new Handler().postDelayed(() -> {
                try {
                    if (isResume)
                        DialogHelper.customAlertDialog(IntroSliderNewActivity.this, getString(R.string.message), msg, getString(R.string.oK));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                msg = null;
            }, 500);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isResume = false;
    }

    @Override
    public void setupSlider() {
        presenter.addSliderData();
        binding.viewpagerIntroSlider.setAdapter(introSliderAdapter);
        binding.viewpagerIntroSlider.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                presenter.changeSliderData(position);
            }
        });

        Handler sliderHandler = new Handler();

        Runnable sliderRunnable = () -> {
            if (mSliderCurrentPage == introSliderAdapter.getItemCount()) {
                mSliderCurrentPage = 0;
            }

            binding.viewpagerIntroSlider.setCurrentItem(mSliderCurrentPage++, true);
        };

        sliderTimer = new Timer();
        sliderTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                sliderHandler.post(sliderRunnable);
            }
        }, 3000, 3000);
    }

    @Override
    public void setSliderContent(SliderData sliderData) {
        if (sliderData != null) {
            binding.tvIntroSliderTitle.setText(sliderData.getTitle());
            binding.tvIntroSliderDescription.setText(sliderData.getDescription());
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_intro_slider_login:
                redirectToLogin();
                break;
            case R.id.btn_intro_slider_sign_up:
                redirectToSignUp();
                break;
            case R.id.btn_intro_slider_language:
                selectLanguage();
                break;
        }
    }

    private void selectLanguage() {
        if(languageData != null && languageData.size() > 0)
        listPopupWindow.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sliderTimer.cancel();
    }

    @Override
    public void redirectToLogin() {
        Intent intent = new Intent(IntroSliderNewActivity.this, NewLogInActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.stay, R.anim.fade_open);
    }

    @Override
    public void redirectToSignUp() {
        Intent intent = new Intent(IntroSliderNewActivity.this, SignUpActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.stay, R.anim.fade_open);
    }

    @Override
    public void onSuccess(ArrayList<LanguageData> result) {
        if(result.size() > 0) {
            languageData.clear();
            languageData.addAll(result);
            languages = new String[languageData.size()];
            for (int i = 0; i < languageData.size(); i++) {
                languages[i] = languageData.get(i).getLan_name();
            }
            ListAdapter listAdapter = new ArrayAdapter<>(this, R.layout.single_row_text_view_language, languages);
            listPopupWindow = new ListPopupWindow(this);
            //listPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            listPopupWindow.setWidth((int)getResources().getDimension(R.dimen.dp_150));
            listPopupWindow.setAnchorView(binding.btnIntroSliderLanguage);
            listPopupWindow.setAdapter(listAdapter);
            listPopupWindow.setOnItemClickListener(this);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        binding.btnIntroSliderLanguage.setText(languageData.get(i).getLan_name());
        DEFAULT_LANGUAGE = languageData.get(i).getCode();
        DEFAULT_DISPLAYLANGUAGE = languageData.get(i).getLan_name();
        listPopupWindow.dismiss();
       Utility.changeLanguageConfig(languageData.get(i).getCode(),this);
        this.recreate();
    }

    public void reload() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }
}