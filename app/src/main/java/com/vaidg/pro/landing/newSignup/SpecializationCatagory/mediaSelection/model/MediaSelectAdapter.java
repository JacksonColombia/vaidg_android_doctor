package com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection.model;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.MediaSelectItemBinding;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection.MediaSelectFragment;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.utility.PhotoFullPopupWindow;
import com.vaidg.pro.utility.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.vaidg.pro.utility.Utility.getBaseDialog;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_PICTURE_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_VIDEO_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.ZERO;

public class MediaSelectAdapter extends RecyclerView.Adapter<MediaSelectAdapter.MediaSelectViewHolder> {

    private int type;
    private MediaSelectItemBinding binding;
    private MediaSelectItemCallBack callBack;
    private boolean isEditMode;
    private ArrayList<String> media;
    private Context context;
    private long mLastClickTime = System.currentTimeMillis();
    private static final long CLICK_TIME_INTERVAL = 300;

    public MediaSelectAdapter(int type, ArrayList<String> media) {
        this(type, media, true);
    }

    public MediaSelectAdapter(int type, ArrayList<String> media, boolean isEditMode) {
        this.type = type;
        this.media = media;
        this.isEditMode = isEditMode;
    }

    public void setEditMode(boolean editMode) {
        isEditMode = editMode;
        notifyDataSetChanged();
    }

    public void provideCallBack(MediaSelectItemCallBack callBack) {
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public MediaSelectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = MediaSelectItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        context = parent.getContext();
        return new MediaSelectViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MediaSelectViewHolder holder, int position) {
        holder.binding.ivClose.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        switch (type) {
            case SPECIALIZATION_VIEW_PICTURE_UPLOAD:
                holder.binding.sdvThumbnail.setImageURI(media.get(position).startsWith("http") ? Uri.parse(media.get(position)) : Uri.fromFile(new File(media.get(position))));
                break;
            case SPECIALIZATION_VIEW_VIDEO_UPLOAD:
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.isMemoryCacheable();
                Glide.with(context)
                        .setDefaultRequestOptions(requestOptions)
                        .load(media.get(position))
                        .into(holder.binding.sdvThumbnail);
                /*try {
                    if (media.get(position).startsWith("http")) {
                        RequestOptions requestOptions = new RequestOptions();
                        requestOptions.isMemoryCacheable();
                        Glide.with(context)
                                .setDefaultRequestOptions(requestOptions)
                                .load(media.get(position))
                                .into(holder.sdvThumbnail);

                    } else {
                        holder.sdvThumbnail.setImageBitmap(ThumbnailUtils.createVideoThumbnail(media.get(position),
                                MediaStore.Images.Thumbnails.MINI_KIND));
                    }
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }*/
                break;
            default:
        }
    }

    @Override
    public int getItemCount() {
        return media != null ? media.size() : ZERO;
    }

    class MediaSelectViewHolder extends RecyclerView.ViewHolder {

        MediaSelectItemBinding binding;

        MediaSelectViewHolder(@NonNull MediaSelectItemBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
            binding.ivClose.setOnClickListener(this::Onclick);
            binding.sdvThumbnail.setOnClickListener(this::Onclick);
        }

        private void Onclick(View view) {
            if (callBack != null) {
                long now = System.currentTimeMillis();
                if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
                    return;
                }
                mLastClickTime = now;
                if (view.getId() == R.id.ivClose) {
                    callBack.onCancel(getAdapterPosition(), type);
                } else {
                    if (type == SPECIALIZATION_VIEW_VIDEO_UPLOAD) {
                        getVideoDialog(context, media.get(getAdapterPosition()), true);
                    } else {
                        new PhotoFullPopupWindow(context, R.layout.popup_photo_full, view, media.get(getAdapterPosition()), null);
                    }
                }
            }
        }
    }

    public void getVideoDialog(Context context, String videoLocation, boolean autoplay) {
        final Dialog dialog = getBaseDialog(context, true, R.layout.dialog);

        ((Activity) context).getWindow().setFormat(PixelFormat.TRANSLUCENT);
        final VideoView videoHolder = (VideoView) dialog.findViewById(R.id.videoDialog);
        ImageView image = dialog.findViewById(R.id.imageDialog);
        videoHolder.setVideoURI(Uri.parse(videoLocation));
        //videoHolder.setRotation(90);
        MediaController mediaController = new MediaController(context);
        videoHolder.setMediaController(mediaController);
        mediaController.setAnchorView(videoHolder);
        videoHolder.requestFocus();
        if (autoplay) {
            videoHolder.start();
        }
        dialog.show();
        videoHolder.setOnCompletionListener(mp -> {
            dialog.dismiss();
            notifyDataSetChanged();
        });


        // if decline button is clicked, close the custom dialog
        image.setOnClickListener(v -> {
            // Close dialog
            dialog.dismiss();
        });
        // return dialog;
    }

    public void addData(List<String> data) {
        media.clear();
        media.addAll(data);
    }

    public ArrayList<String> getAllData() {
        return media;
    }

    public void deleteData(int position) {
        media.remove(position);
        notifyItemRemoved(position);
    }
}
