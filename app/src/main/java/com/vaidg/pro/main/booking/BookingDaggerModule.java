package com.vaidg.pro.main.booking;

import com.vaidg.pro.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>LogInDaggerModule</h1>
 * <p>This dagger module created for LogInActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
@Module
public abstract class BookingDaggerModule {

    @FragmentScoped
    @Binds
    abstract BookingContract.Presenter getPresenter(BookingPresenter bookingPresenter);

   /* @FragmentScoped
    @Binds
    abstract BookingContract.View provideView(BookingFragment fragment);
*/
}
