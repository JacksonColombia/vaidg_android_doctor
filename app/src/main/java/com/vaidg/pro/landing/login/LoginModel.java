package com.vaidg.pro.landing.login;

import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.google.gson.Gson;
import com.vaidg.pro.pojo.login.LogInPojo;
import com.vaidg.pro.pojo.login.LoginData;
import com.vaidg.pro.utility.OkHttp3ConnectionStatusCode;
import com.vaidg.pro.utility.ServiceUrl;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>LoginModel</h1>
 * Model class for LoginActivity
 *
 * @see LoginActivity
 */

public class LoginModel {
    private static final String TAG = "LoginModel";
    private LoginModelImple modelImplement;

    LoginModel(LoginModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * local validation for login fields
     *
     * @param username   email or phone number
     * @param password   password
     * @param jsonObject required field in json object
     */
    public void login(String username, String password, JSONObject jsonObject) {
        if (Utility.isTextEmpty(username)) {
            modelImplement.onUsernameError();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(username).matches() && !Patterns.PHONE.matcher(
                username).matches()) {
            modelImplement.onUsernameError();
        } else if (Utility.isTextEmpty(password)) {
            modelImplement.onPasswordError();
        } else {
            login(jsonObject);
        }
    }


    /**
     * method for calling api for login
     *
     * @param jsonObject required fields for login
     */
    private void login(JSONObject jsonObject) {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(Utility.getBasicAuth(),
                ServiceUrl.LOGIN, OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject,
                new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                                Gson gson = new Gson();
                                LogInPojo loginPojo = gson.fromJson(result, LogInPojo.class);
                                if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                    modelImplement.onSuccess(loginPojo.getData());
                                } else {
                                    modelImplement.onFailure(loginPojo.getMessage());
                                }
                            } else {
                                modelImplement.onFailure();
                            }
                        } catch (Exception e) {
                            modelImplement.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        modelImplement.onFailure();
                    }
                });
    }


    /**
     * <h1>LoginModelImple</h1>
     * LoginModelImple interface for Presenter Implementation
     *
     * @see LoginPresenter
     */
    interface LoginModelImple {
        void onFailure(String failureMsg);

        void onFailure();

        void onSuccess(LoginData loginData);

        void onUsernameError();

        void onPasswordError();

        void onForgotPasswordEmailError();

        void onForgotPasswordInvalidEmailError();

        void onSuccessForgotPassword(String msg);
    }
}
