package com.vaidg.pro.landing.newSignup.selfOwnedClinic;

import android.content.Intent;
import android.net.Uri;

import java.io.File;

public interface SelfOwnedClinicContract {

    interface View {

        void showError(String message);

        void openCamera(Uri uri_path);

        void openGallery();

        /**
         * <p>This method is update UI with set data to display.</p>
         */
        void updateUI();

        void notifyAdapter();

        void HandlePostDeleteBtn(boolean hasItem);

        void setLogo(String url, Uri fileUri);

        boolean isLogo();

        void showLocatioon(String locationDetail);

        void sendBackData(Intent intent);
    }

    interface Presenter {

        void openChooser();

        void compressedMedia(String recentTemp);

        String getRecentTemp();

        void deletePostImage(int currentItem);

        void parseOnActivityData(int resultCode, int requestCode, Intent data);

        void validate(String clinicName, String location);

        void upDateToGallery();
    }
}
