package com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.pojo.PreDefined;

import java.util.ArrayList;
import java.util.List;

public interface DocumentSelectionContract {

    interface View {

        /**
         * <p>used to set the data from activity</p>
         *
         * @param item the current {@link Attribute} object.
         */
        void setPreferenceItem(Attribute item);

        /**
         * <p>This method is return boolean flag either it's mandatory or not.</p>
         *
         * @return the true if madatory else false
         */
        boolean isMandatoryForProvider();

        /**
         * <p>this is used for notifying recycler view adapter</p>
         */
        void notifyAdapter();

        /**
         * <p>this is used for notifying particular positioned item in recycler view adapter</p>
         *
         * @param position the position of notifying  item
         * @param listSize the size of list
         */
        void notifyAdapter(int position, int listSize);

        /**
         * <p>This method is display passed error message in snack bar.</p>>
         *
         * @param error the error message
         */
        void showError(String error);

        /**
         * <p>This method is launch selected document or image viewer.</p>
         *
         * @param filePath the selected document or image viewer path
         */
        void launchViewer(String filePath);

        /**
         * <p>This method is intent to document picker.</p>
         */
        void openDocPicker();

        /**
         * <p>This method request for display date picker so, user can select date.</p>
         *
         * @param type the type of date
         *             2 - date current to future
         *             3- data past to current
         */
        void openDatePicker(int type);

        /**
         * <p>This method validate that all mandatory info filled and moved to next step.</p>
         */
        void moveNextFrag();
    }

    interface Presenter extends BasePresenter<View> {

        /**
         * <p>This method is add data into list and display it.</p>
         *
         * @param list list of {@link PreDefined} objects
         */
        void addPreDefinedData(List<PreDefined> list);

        /**
         * <p>This method is set user selected data into positioned object.s</p>
         *
         * @param position the position of clicked item
         * @param data     the entered or seleted data
         */
        void setData(int position, String data);

        /**
         * <p>This method is delete data from the clicked position of object.</p>
         *
         * @param position the position of clicked item
         */
        void deleteDocument(int position);

        /**
         * <p>This method is validate selected document size.</p>
         *
         * @param filePath the path of selected document file
         * @return the true or false based on validation
         */
        boolean isValidDocumentSize(String filePath);

        /**
         * <p>It's save user selected or entered data into {@link Attribute} object.</p>
         *
         * @param attribute the current attribute
         */
        void saveData(Attribute attribute);

        /**
         * <p>This method is validate that all mandatory fields are filled by user.</p>
         *
         * @return the true if all details are entered
         */
        boolean isMandatoryFilled();

        /**
         * <p>This method is clear all entered or select data.</p>
         */
        void clearData();

    }
}
