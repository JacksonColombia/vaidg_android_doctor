package com.vaidg.pro.main.profile.wallet;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.main.profile.wallet.cardlist.CardListActivity;
import com.vaidg.pro.main.profile.wallet.walletransaction.WalletTransactionActivity;
import com.vaidg.pro.pojo.profile.wallet.CardData;
import com.vaidg.pro.pojo.profile.wallet.WalletData;
import com.vaidg.pro.razorpay.RazorPayPaymentActivity;
import com.vaidg.pro.utility.DecimalDigitsInputFilter;
import com.vaidg.pro.utility.DialogHelper;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import dagger.android.support.DaggerAppCompatActivity;
import eu.janmuller.android.simplecropimage.Util;

import java.util.ArrayList;
import javax.inject.Inject;
import org.json.JSONObject;

import static android.view.View.VISIBLE;
import static com.vaidg.pro.utility.Utility.isFromIndia;

public class WalletActivity extends DaggerAppCompatActivity implements View.OnClickListener, WalletContract.View {
  @Inject
  WalletContract.Presenter presenter;
  private TextView tvWalletBalance, tvSoftLimitValue, tvHardLimitValue, tvCardNo, tvCurrencySymbol, tvCurrencySymbolSuffix;
  private ImageView ivCardImage;
  private View view2;
  private EditText etPayAmountValue;
  private ProgressDialog progressDialog;
  private SessionManager sessionManager;
  private Context mContext;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_wallet);
    mContext = this;
    init();
  }

  /**
   * <h2>initViews</h2>
   * <p>
   * custom method to load top views of the screen
   * </P>
   */
  private void init() {
    sessionManager = SessionManager.getSessionManager(this);
    progressDialog = new ProgressDialog(this);
    progressDialog.setMessage(getString(R.string.loading));
    progressDialog.setCancelable(false);
    Typeface fontRegular = Utility.getFontRegular(this);
    Typeface fontMedium = Utility.getFontMedium(this);
    Typeface fontBold = Utility.getFontBold(this);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayShowTitleEnabled(false);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
    }
    TextView tvTitle = findViewById(R.id.tvTitle);
    tvTitle.setText(getString(R.string.wallet));
    tvTitle.setTypeface(fontBold);
    tvTitle.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_20), mContext));
    TextView tvCurCreditLabel = findViewById(R.id.tvCurCreditLabel);
    tvWalletBalance = findViewById(R.id.tvWalletBalance);
    TextView tvSoftLimitLabel = findViewById(R.id.tvSoftLimitLabel);
    tvSoftLimitValue = findViewById(R.id.tvSoftLimitValue);
    TextView tvHardLimitLabel = findViewById(R.id.tvHardLimitLabel);
    tvHardLimitValue = findViewById(R.id.tvHardLimitValue);
    tvCurrencySymbol = findViewById(R.id.tvCurrencySymbol);
    tvCurrencySymbolSuffix = findViewById(R.id.tvCurrencySymbolSuffix);
    TextView tvPayUsingCardLabel = findViewById(R.id.tvPayUsingCardLabel);
    TextView tvPayAmountLabel = findViewById(R.id.tvPayAmountLabel);
    TextView tvSoftLimitMsg = findViewById(R.id.tvSoftLimitMsg);
    TextView tvHardLimitMsg = findViewById(R.id.tvHardLimitMsg);
    TextView tvConfirm = findViewById(R.id.tvConfirm);
    TextView walletCreditMsg = findViewById(R.id.walletCreditMsg);
    Button btnRecentTransactions = findViewById(R.id.btnRecentTransactions);
    etPayAmountValue = findViewById(R.id.etPayAmountValue);
    view2 = findViewById(R.id.view2);
    etPayAmountValue.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(15, 2)});
    tvCurCreditLabel.setTypeface(fontBold);
    tvWalletBalance.setTypeface(fontBold);
    tvSoftLimitLabel.setTypeface(fontMedium);
    tvSoftLimitValue.setTypeface(fontMedium);
    tvHardLimitLabel.setTypeface(fontMedium);
    tvHardLimitValue.setTypeface(fontMedium);
    tvPayUsingCardLabel.setTypeface(fontBold);
    tvCardNo = findViewById(R.id.tvCardNo);
    ivCardImage = findViewById(R.id.ivCardImage);
    tvCardNo.setTypeface(fontMedium);
    tvCardNo.setOnClickListener(this);
    tvPayAmountLabel.setTypeface(fontBold);
    walletCreditMsg.setTypeface(fontMedium);
    tvCurrencySymbolSuffix.setTypeface(fontMedium);
    tvSoftLimitMsg.setTypeface(fontMedium);
    tvHardLimitMsg.setTypeface(fontMedium);
    tvConfirm.setTypeface(fontMedium);
    btnRecentTransactions.setTypeface(fontMedium);
    etPayAmountValue.setTypeface(fontMedium);
    tvCurrencySymbol.setTypeface(fontMedium);
    tvCurCreditLabel.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
    tvWalletBalance.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_35), mContext));
    etPayAmountValue.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_35), mContext));
    tvCurrencySymbol.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_35), mContext));
    tvSoftLimitLabel.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_12), mContext));
    tvSoftLimitValue.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_12), mContext));
    tvHardLimitLabel.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_12), mContext));
    tvHardLimitValue.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_12), mContext));
    tvSoftLimitMsg.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_12), mContext));
    tvHardLimitMsg.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_12), mContext));
    walletCreditMsg.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
    tvPayUsingCardLabel.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
    tvPayAmountLabel.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
    tvConfirm.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
    tvCardNo.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
    btnRecentTransactions.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
    btnRecentTransactions.setOnClickListener(this);
    tvConfirm.setOnClickListener(this);

    view2.setVisibility((isFromIndia(this)) ? View.GONE : VISIBLE);
    tvPayUsingCardLabel.setVisibility((isFromIndia(this)) ? View.GONE : VISIBLE);
    ivCardImage.setVisibility((isFromIndia(this)) ? View.GONE : VISIBLE);
    tvCardNo.setVisibility((isFromIndia(this)) ? View.GONE : VISIBLE);

    etPayAmountValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View view, boolean b) {
        if (!b && !etPayAmountValue.getText().toString().equals("")) {
          etPayAmountValue.setText((Utility.isFromIndia(mContext)) ? etPayAmountValue.getText().toString() : Utility.getFormattedPrice(etPayAmountValue.getText().toString()));
        }
      }
    });
    presenter.getWalletDetails();
  }
  //====================================================================

  @Override
  protected void onResume() {
    super.onResume();
    if (!sessionManager.getSelectedCardNumber().equals("")) {
      tvCardNo.setText(getString(R.string.cardEndingNumber) + " " + sessionManager.getSelectedCardNumber());
      ivCardImage.setImageBitmap(Utility.getCardBrand(sessionManager.getCardBrand(), this));
    }
  }

  @Override
  public void onClick(View view) {
    Utility.hideKeyboad(this);
    etPayAmountValue.clearFocus();
    switch (view.getId()) {
      case R.id.btnRecentTransactions:
        Intent intent = new Intent(this, WalletTransactionActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        break;
      case R.id.tvCardNo:
        Intent cardsIntent = new Intent(this, CardListActivity.class);
        startActivityForResult(cardsIntent, 1);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        break;
      case R.id.tvConfirm:
        if(isFromIndia(mContext) && etPayAmountValue.getText().toString().trim().length() > 0) {
          Intent razorPayIntent = new Intent(WalletActivity.this, RazorPayPaymentActivity.class);
          razorPayIntent.putExtra("email", sessionManager.getEmail());
          razorPayIntent.putExtra("name", sessionManager.getFirstName() + sessionManager.getLastName());
          razorPayIntent.putExtra("contact", sessionManager.getPhoneNumber());
          razorPayIntent.putExtra("currency", sessionManager.getCurrency());
          razorPayIntent.putExtra("razorPayKey", VariableConstant.RAZORPAY_KEY);
          razorPayIntent.putExtra("description", "");
          razorPayIntent.putExtra("amount",Integer.parseInt(etPayAmountValue.getText().toString().trim()) * 100);
          razorPayIntent.putExtra("image", sessionManager.getProfilePic());
          razorPayIntent.putExtra("userId", sessionManager.getProviderId());
          razorPayIntent.putExtra("paymentAction", "2");
          razorPayIntent.putExtra("userType", "2");
          razorPayIntent.putExtra("recieverName", sessionManager.getFirstName() + sessionManager.getLastName());
          startActivityForResult(razorPayIntent, 108);
        }else {
          presenter.validate(sessionManager.getSelectedCardNumber(), etPayAmountValue.getText().toString());
        }
        break;
    }
  }
  //====================================================================

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      closeActivity();
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    closeActivity();
  }

  private void closeActivity() {
    Utility.progressDialogCancel(this, progressDialog);
    if (getIntent().getBooleanExtra("isFromBookingFrag", false)) {
      finish();
      overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    } else {
      finishAfterTransition();
    }
  }

  @Override
  public void startProgressBar() {
    runOnUiThread(() -> {
        Utility.progressDialogDismiss(this, progressDialog);
    });
  }

  @Override
  public void stopProgressBar() {
    runOnUiThread(() -> {
      Utility.progressDialogDismiss(this, progressDialog);
    });
  }

  @Override
  public void onFailure(String msg) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onFailure() {
    AppController.toast();
  }

  @Override
  public void onSuccessWalletDetails(WalletData walletData) {
    if (walletData.isReachedHardLimit()) {
      Utility.setTextColor(this, tvWalletBalance, R.color.customRed);
    } else if (walletData.isReachedSoftLimit()) {
      Utility.setTextColor(this, tvWalletBalance, R.color.yellow);
    } else {
      Utility.setTextColor(this, tvWalletBalance, R.color.black);
    }

       /* if (Double.valueOf(walletData.getWalletAmount())>0){
            tvalertMsg.setText(getString(R.string.positive_wallet_alert)+getString(R.string.app_name)+sessionManager.getCurrencySymbol()+walletDataPojo.getWalletAmount());
        }else if (Double.valueOf(walletDataPojo.getWalletAmount())<0){
            tvalertMsg.setText(getString(R.string.app_name)+getString(R.string.app_name)+sessionManager.getCurrencySymbol()+walletDataPojo.getWalletAmount().substring(1));
        }*/
    tvWalletBalance.setText(Utility.getPrice(walletData.getWalletAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
    tvSoftLimitValue.setText(Utility.getPrice(String.valueOf(walletData.getSoftLimit()), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
    tvHardLimitValue.setText(Utility.getPrice(String.valueOf(walletData.getHardLimit()), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
    tvCurrencySymbol.setText(sessionManager.getCurrencySymbol());
    tvCurrencySymbolSuffix.setText(sessionManager.getCurrencySymbol());
    sessionManager.setWalletAmount(walletData.getWalletAmount());
    if (sessionManager.getCurrencyAbbrevation().equals("2")) {
      tvCurrencySymbolSuffix.setVisibility(View.VISIBLE);
      tvCurrencySymbol.setVisibility(View.GONE);
    }
    if (sessionManager.getSelectedCardNumber().equals("") && !Utility.isFromIndia(this)) {
      presenter.getCardDetails();
    }
  }

  @Override
  public void onSuccessWalletRecharge(String msg) {
    etPayAmountValue.setText("");
    presenter.getWalletDetails();
    DialogHelper.customAlertDialog(this, getString(R.string.message), msg, getString(R.string.oK));
    VariableConstant.IS_WALLET_UPDATED = true;
  }

  @Override
  public void onSuccessCardDetails(ArrayList<CardData> data) {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        // Stuff that updates the UI
        stopProgressBar();
        if (data.size() > 0) {
          for (CardData cardData : data) {
            if (cardData.isDefault()) {
              sessionManager.setSelectedCardId(cardData.getId());
              sessionManager.setSelectedCardNumber(cardData.getLast4());
              sessionManager.setCardBrand(cardData.getBrand());
              tvCardNo.setText(getString(R.string.cardEndingNumber) + " " + sessionManager.getSelectedCardNumber());
              ivCardImage.setImageBitmap(Utility.getCardBrand(sessionManager.getCardBrand(), WalletActivity.this));
            }
          }
        } else {
          tvCardNo.setText(getString(R.string.addcard));
        }
      }
    });
  }

  @Override
  public void onErrorCardSelection() {
    Toast.makeText(this, getString(R.string.plsSelectCard), Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onErrorAmount() {
    Toast.makeText(this, getString(R.string.plsEnterPrice), Toast.LENGTH_SHORT).show();
  }

  @Override
  public void successValidation() {
    String amt = "";
    if (sessionManager.getCurrencyAbbrevation().equals("1")) {
      amt = sessionManager.getCurrencySymbol() + " " + etPayAmountValue.getText().toString();
    } else {
      amt = etPayAmountValue.getText().toString() + " " + sessionManager.getCurrencySymbol();
    }
    showRechargeConfirmationAlert(amt);
  }

  @Override
  public void onNewToken(String token) {
    AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
        sessionManager.getPassword(), token);
  }

  @Override
  public void sessionExpired(String msg) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    Utility.logoutSessionExiperd(sessionManager, this);
  }

  /**
   * <h2>showRechargeConfirmationAlert</h2>
   * <p>
   * method to show an alert dialog to take user
   * confirmation to proceed to recharge
   * </p>
   */
  private void showRechargeConfirmationAlert(final String amount) {
    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
    alertDialog.setTitle(getString(R.string.confirmPayment));
    alertDialog.setCancelable(false);
    alertDialog.setMessage(getString(R.string.paymentMsg1)
        + " " + getString(R.string.app_name) + " " + getString(R.string.paymentMsg2) + " " + amount);
    alertDialog.setPositiveButton(getString(
        R.string.confirm), new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        try {
          JSONObject jsonObject = new JSONObject();
          jsonObject.put("cardId", sessionManager.getSelectedCardId());
          jsonObject.put("amount", etPayAmountValue.getText().toString());

                  /*  RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("cardId", sessionManager.getSelectedCardId())
                        .addFormDataPart("amount", etPayAmountValue.getText().toString())
                        .build();*/
          presenter.rechargeWallet(jsonObject, sessionManager.getSelectedCardId());
        } catch (Exception e) {
          e.printStackTrace();
        }
        dialog.dismiss();
      }
    });
    // on pressing cancel button
    alertDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    });
    alertDialog.show();
  }

  @Override
  public void showProgress() {
  }

  @Override
  public void hideProgress() {
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if(resultCode == RESULT_OK)
    {
      etPayAmountValue.setText("");
      Toast.makeText(this, "Transaction completed successfully amount will be credited in some time", Toast.LENGTH_SHORT).show();
      presenter.getWalletDetails();
    }
  }

  //====================================================================
}
