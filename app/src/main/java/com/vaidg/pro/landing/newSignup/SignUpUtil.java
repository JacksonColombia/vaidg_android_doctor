package com.vaidg.pro.landing.newSignup;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.DatePickerCommon;
import com.vaidg.pro.utility.MediaBottomSelector;
import com.vaidg.pro.utility.SessionManager;
import com.videocompressor.com.CompressImage;
import com.videocompressor.com.VideoCompressor;

import dagger.Module;
import dagger.Provides;

@Module
public class SignUpUtil {

    @ActivityScoped
    @Provides
    ProgressDialog ProvideProgressDialog(Activity activity) {
        return new ProgressDialog(activity);
    }

    @ActivityScoped
    @Provides
    DatePickerCommon provideDatePickerCommon() {
        return new DatePickerCommon();
    }

    @Provides
    @ActivityScoped
    App_permission getApp_permission(Activity activity) {
        return new App_permission(activity);
    }

    @Provides
    @ActivityScoped
    MediaBottomSelector getMediaBottomSelector(Activity activity) {
        return new MediaBottomSelector(activity);
    }

    @ActivityScoped
    @Provides
    CompressImage compressImage() {
        return new CompressImage();
    }

    @ActivityScoped
    @Provides
    VideoCompressor getVideoCompressor(Context context) {
        return new VideoCompressor(context);
    }

    @ActivityScoped
    @Provides
    SessionManager provide(Activity activity) {
        return SessionManager.getSessionManager(activity);
    }

}
