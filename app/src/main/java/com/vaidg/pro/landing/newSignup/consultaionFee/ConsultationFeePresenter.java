package com.vaidg.pro.landing.newSignup.consultaionFee;

import android.app.Activity;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.landing.newSignup.consultaionFee.model.ConsultationFeeModel;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.addMedication.AddMedicationPojo;
import com.vaidg.pro.pojo.profile.calltype.CallTypePojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

/**
 * <h1>@SignUpPresenter</h1>
 * <p> this is presenter class which provides bussiness logics</p>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public class ConsultationFeePresenter implements ConsultationFeeContract.Presenter {

    @Inject
    Activity activity;
    @Inject
    ConsultationFeeContract.View view;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    SessionManager sessionManager;
    @Inject
    NetworkService service;
    @Inject
    ConsultationFeeModel model;
    @Inject
    Gson gson;

    @Inject
    public ConsultationFeePresenter() {
    }

    @Override
    public void sendBackData() {
        if (model.isFeeEntered()) {
            Intent intent = new Intent();
            intent.putExtra("fee", model.getData());
            if (view != null)
                view.finish(intent);
        } else {
            if (view != null)
                view.notifyData();
        }
    }

    @Override
    public void getCallTypeSetting() {
        if (networkStateHolder.isConnected()) {
            if (view != null)
                view.showProgress();
            service.getCallTypeSetting(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        if (view != null)
                                            view.hideProgress();
                                        if (response != null && !response.isEmpty()) {
                                            CallTypePojo callTypePojo = gson.fromJson(response, CallTypePojo.class);
                                            model.setCallTypeDataList(callTypePojo.getData());
                                            if (view != null)
                                                view.setFeesData();
                                        } else {
                                            if (view != null)
                                                view.showError();
                                        }
                                        break;
                                    case SESSION_EXPIRED:
                                        if (view != null)
                                            view.hideProgress();
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                updateFees();
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                if (view != null) {
                                                    view.hideProgress();
                                                    view.showError();
                                                }
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, activity);
                                            }
                                        });
                                        break;
                                    case SESSION_LOGOUT:
                                        if (view != null)
                                            view.hideProgress();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        Utility.logoutSessionExiperd(sessionManager, activity);
                                        break;
                                    default:
                                        if (view != null)
                                            view.hideProgress();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        break;
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.showError();
                            }
                        }

                        @Override
                        public void onComplete() {
                        }
                    });

        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void updateCallTypeSetting() {
        Utility.hideKeyboad(activity);
        if (!model.isFeeEntered()) {
            return;
        }

        if (networkStateHolder.isConnected()) {
            if (view != null)
                view.showProgress();

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("categoryId", model.getCallTypeDataList().get(0).getCategoryId());
                jsonObject.put("inCall", model.getData().get(1).isChecked() ? 1 : 0);
                jsonObject.put("outCall", model.getData().get(0).isChecked() ? 1 : 0);
                jsonObject.put("teleCall", model.getData().get(2).isChecked() ? 1 : 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            service.updateCallTypeSetting(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        if (response != null && !response.isEmpty()) {
                                            updateFees();
                                        } else {
                                            if (view != null) {
                                                view.hideProgress();
                                                view.showError();
                                            }
                                        }
                                        break;
                                    case SESSION_EXPIRED:
                                        if (view != null)
                                            view.hideProgress();
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                updateFees();
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                if (view != null) {
                                                    view.hideProgress();
                                                    view.showError();
                                                }
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, activity);
                                            }
                                        });
                                        break;
                                    case SESSION_LOGOUT:
                                        if (view != null)
                                            view.hideProgress();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        Utility.logoutSessionExiperd(sessionManager, activity);
                                        break;
                                    default:
                                        if (view != null)
                                            view.hideProgress();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        break;
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.showError();
                            }
                        }

                        @Override
                        public void onComplete() {
                        }
                    });

        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void updateFees() {
        if (networkStateHolder.isConnected()) {
            if (view != null)
                view.showProgress();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("inCallFee", model.getData().get(1).getFee());
                jsonObject.put("outCallFee", model.getData().get(0).getFee());
                jsonObject.put("teleCallFee", model.getData().get(2).getFee());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            service.updateProfile(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        if (view != null)
                                            view.hideProgress();
                                        if (response != null && !response.isEmpty()) {
                                            Intent intent = new Intent();
                                            intent.putExtra("inCallFee", jsonObject.getString("inCallFee"));
                                            intent.putExtra("outCallFee", jsonObject.getString("outCallFee"));
                                            intent.putExtra("teleCallFee", jsonObject.getString("teleCallFee"));
                                            if (view != null)
                                                view.onSuccessUpdateProfile(intent, Utility.getMessage(response));
                                        } else {
                                            if (view != null)
                                                view.showError();
                                        }
                                        break;
                                    case SESSION_EXPIRED:
                                        if (view != null)
                                            view.hideProgress();
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                updateFees();
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                if (view != null) {
                                                    view.hideProgress();
                                                    view.showError();
                                                }
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, activity);
                                            }
                                        });
                                        break;
                                    case SESSION_LOGOUT:
                                        if (view != null)
                                            view.hideProgress();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        Utility.logoutSessionExiperd(sessionManager, activity);
                                        break;
                                    default:
                                        if (view != null)
                                            view.hideProgress();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        break;
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.showError();
                            }
                        }

                        @Override
                        public void onComplete() {
                        }
                    });

        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void setData(String currencyAbrr,String currencySymbol, int minFee, int maxFee, ArrayList<String> fees) {
        model.setData(currencyAbrr,currencySymbol, minFee, maxFee, fees);
        view.notifyData();
    }
}
