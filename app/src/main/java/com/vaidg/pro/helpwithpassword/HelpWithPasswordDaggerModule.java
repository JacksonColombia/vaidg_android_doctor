package com.vaidg.pro.helpwithpassword;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>HelpWithPasswordDaggerModule</h1>
 * <p>This class is collection of object that bind with dagger.</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 23/06/2020
 **/
@Module
public abstract class HelpWithPasswordDaggerModule {

    @ActivityScoped
    @Binds
    abstract HelpWithPasswordContract.Presenter providePresenter(HelpWithPasswordPresenterImple helpWithPasswordPresenterImple);

    @ActivityScoped
    @Binds
    abstract HelpWithPasswordContract.View provideView(HelpWithPasswordActivity helpWithPasswordActivity);

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(HelpWithPasswordActivity activity);
}
