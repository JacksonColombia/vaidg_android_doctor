package com.vaidg.pro.changepassword;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityChangePasswordBinding;
import com.vaidg.pro.utility.DialogHelper;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import javax.inject.Inject;

/**
 * Created by murashid on 11-Sep-17.
 * <h1>ChangePasswordActivity</h1>
 * ChangePassword Activity is used to change the password of the user or reset the password of the user once he forgot
 */
public class ChangePasswordActivity extends BaseDaggerActivity implements ChangePasswordContract.View, View.OnClickListener {

    @Inject
    ChangePasswordContract.Presenter presenter;

    private ActivityChangePasswordBinding binding;

    private String userId = "";
    private boolean isForgotPassword;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChangePasswordBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
    }


    /**
     * Initialize the views
     */
    private void initViews() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        binding.includeToolbar.tvTitle.setText(getString(R.string.changePassword));
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        binding.btnSave.setOnClickListener(this);

        TextView tvMobileVerifiedMsg = findViewById(R.id.tvMobileVerifiedMsg);
        tvMobileVerifiedMsg.setTypeface(fontBold);

        binding.etOldPassword.setTypeface(fontRegular);
        binding.etNewPassword.setTypeface(fontRegular);
        binding.etConfirmPassword.setTypeface(fontRegular);

        Intent intent = getIntent();
        isForgotPassword = intent.getBooleanExtra("isForgotPassword", false);

        if (isForgotPassword) {
//            binding.includeToolbar.tvTitle.setText(getString(R.string.resetPassword));
            intent.getStringExtra("phone");
            intent.getStringExtra("countryCode");
            userId = intent.getStringExtra("userId");
            binding.tilOldPassword.setVisibility(View.GONE);
            tvMobileVerifiedMsg.setVisibility(View.VISIBLE);
            binding.tilNewPassword.setHint(getString(R.string.yourNewPassword));
            binding.tilConfirmPassword.setHint(getString(R.string.reenterpassword));
            progressDialog.setMessage(getString(R.string.changingPassword));
        } else {
//            binding.includeToolbar.tvTitle.setText(getString(R.string.changePassword));
            binding.tilNewPassword.setHint(getString(R.string.newPassword));
            binding.tilConfirmPassword.setHint(getString(R.string.confirmNewPassword));
            progressDialog.setMessage(getString(R.string.updatingPassword));
        }

    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboad(this);
        Utility.progressDialogCancel(this, progressDialog);
        finish();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btnSave) {
            if (!isForgotPassword) {
                presenter.changePassword(binding.etOldPassword.getText() == null ? "" : binding.etOldPassword.getText().toString(),
                        binding.etNewPassword.getText() == null ? "" : binding.etNewPassword.getText().toString(),
                        binding.etConfirmPassword.getText() == null ? "" : binding.etConfirmPassword.getText().toString());
            } else {
                presenter.forgotPassword(userId,
                        binding.etNewPassword.getText() == null ? "" : binding.etNewPassword.getText().toString(),
                        binding.etConfirmPassword.getText() == null ? "" : binding.etConfirmPassword.getText().toString());
            }
        }
    }

    @Override
    public void onSuccess(String msg) {
        DialogHelper.customAlertDialogCloseActivity(this, getString(R.string.message), msg, getString(R.string.oK));
    }

    @Override
    public void onOldPasswordError() {
        resetTil(binding.tilOldPassword, getString(R.string.enterPassword));
    }

    @Override
    public void onNewPasswordError() {
        resetTil(binding.tilNewPassword, getString(R.string.enterValidPassword));
    }

    @Override
    public void onConfirmPasswordError() {
        resetTil(binding.tilConfirmPassword, getString(R.string.enterConfirmPassword));
    }

    @Override
    public void onPasswordMismatch() {
        resetTil(binding.tilConfirmPassword, getString(R.string.passwordMistach));
    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clParent, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    /**
     * <h1>resetTil</h1>
     *
     * @param til   TextInputLayout of error field
     * @param error Error string
     */
    private void resetTil(TextInputLayout til, String error) {
        binding.tilOldPassword.setErrorEnabled(false);
        binding.tilNewPassword.setErrorEnabled(false);
        binding.tilConfirmPassword.setErrorEnabled(false);

        if (til != null) {
            til.setErrorEnabled(true);
            til.setError(error);
        }
    }

    @Override
    public void showProgress() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void hideProgress() {
        Utility.progressDialogDismiss(this, progressDialog);
    }
}
