package com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.FragmentListSelectionBinding;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.SpecializationCategoryActivity;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.SpecializationCategoryContract;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection.model.ListSelectItemCallBack;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection.model.ListSelectionAdapter;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.utility.Utility;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

/**
 * <h1>ListSelectionFragment</h1>
 * <p>this fragment is used to display single selection or mutli selection</p>
 * this fragment is reused for type 1 - Radio button, 2 - CheckBox (multi selection) and 3 - dropdown menu
 * </P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class ListSelectionFragment extends DaggerFragment implements ListSelectionContract.View {

    private static final String TAG = ListSelectionFragment.class.getSimpleName();

    @Inject
    Activity activity;

    @Inject
    SpecializationCategoryContract.Presenter mainPresenter;

    @Inject
    ListSelectionContract.Presenter presenter;

    private FragmentListSelectionBinding binding;
    private Attribute currentItem;
    private int list_number;
    private boolean isSingleSelection;
    private ListSelectionAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        assert bundle != null;
        isSingleSelection = bundle.getBoolean(SpecializationCategoryActivity.ITEM_POSITION);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentListSelectionBinding.inflate(inflater);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRv();
        initViews();
        setProgressBar();
        initOnclickLister();
    }

    private void initRv() {
        adapter = new ListSelectionAdapter(currentItem.getPreDefined(), isSingleSelection);
        adapter.provideCallBack((ListSelectItemCallBack) presenter);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(activity, LinearLayoutManager.VERTICAL);
        itemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_light, null));
        binding.rvListSelection.addItemDecoration(itemDecoration);
        binding.rvListSelection.setAdapter(adapter);
    }

    private void initOnclickLister() {
        binding.includeToolbar.btnDone.setOnClickListener(this::onclick);
    }

    private void onclick(View view) {
        if (view.getId() == R.id.btnDone) {
            presenter.isCheck();
        }
    }


    private void setProgressBar() {
        binding.progressBar.setMax(currentItem.getMax_count());
        binding.progressBar.setProgress(currentItem.getPosition() + 1);
    }

    /**
     * <h2>Setting the current preference.</h2>
     *
     * @param item
     */
    public void setPreferenceItem(Attribute item) {
        if(item != null) {
            currentItem = item;
            list_number = currentItem.getList_no();
        }
    }

    public void initViews() {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        binding.includeToolbar.tvTitle.setText(getString(R.string.complete_your_profile));
        binding.tvListSelectionTitle.setText(currentItem.getQueForProviderSignup());
        binding.includeToolbar.btnDone.setText(getString(R.string.next));
        if (activity != null) {
            activity.setSupportActionBar(binding.includeToolbar.toolbar);
            if (activity.getSupportActionBar() != null) {
                activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
                activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
                binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> activity.onBackPressed());
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.attachView(this);
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void notifyAdapter() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public ArrayList<PreDefined> getData() {
        return (ArrayList<PreDefined>) currentItem.getPreDefined();
    }

    @Override
    public boolean isMandatoryForProvider() {
        return currentItem.getMandatoryForProvider();
    }

    @Override
    public void moveNextFrag() {
        presenter.saveData(currentItem);
        mainPresenter.openNextFrag(list_number, currentItem.getPosition() + 1, currentItem);
    }

    @Override
    public void showError(String error) {
        mainPresenter.showError(error);
    }
}
