package com.vaidg.pro.landing.newSignup.findHospital.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.databinding.ItemFindHospitalLocBinding;

import java.util.ArrayList;
import java.util.List;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * <h1>@FindHospitalAddressAdapter</h1>
 * <p>HospitalAddressAdapter adapter to list out the searched places</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 25/04/2020.
 **/

public class FindHospitalAddressAdapter extends RecyclerView.Adapter<FindHospitalAddressAdapter.FindHospitalAddressViewHolder> {

    FindHospitalCallback callBack;
    private List<HospitalDataPojo> mAddressList;
    private Context mContext;

    public FindHospitalAddressAdapter(ArrayList<HospitalDataPojo> addressList) {
        this.mAddressList = addressList;
    }

    public void setCallBack(FindHospitalCallback callBack) {
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public FindHospitalAddressViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        ItemFindHospitalLocBinding binding = ItemFindHospitalLocBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new FindHospitalAddressViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FindHospitalAddressViewHolder holder, int position) {
        holder.binding.tvHospitalName.setText(mAddressList.get(position).getBusinessName());
        holder.binding.tvItemFindLocSub.setText(mAddressList.get(position).getAddress());
    }

    @Override
    public int getItemCount() {
        return mAddressList != null ? mAddressList.size() : ZERO;
    }

    /**
     * <h1>@FindHospitalAddressAdapter</h1>
     * <p>viewHolder used for initialize the textView...</P>
     *
     * @author hemanth.
     * @version 1.0.20.
     * @since 25/04/2020.
     **/
    class FindHospitalAddressViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemFindHospitalLocBinding binding;
        FindHospitalAddressViewHolder(@NonNull ItemFindHospitalLocBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.itemFindHospLoc.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            callBack.onItemSelect(getAdapterPosition());
        }

    }


}
