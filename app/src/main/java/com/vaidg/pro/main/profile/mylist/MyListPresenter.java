package com.vaidg.pro.main.profile.mylist;

import android.util.Log;

import com.vaidg.pro.pojo.profile.ProfileData;
import com.vaidg.pro.pojo.profile.events.MyListEvents;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.AMAZON_BASE_URL;
import static com.vaidg.pro.utility.VariableConstant.BUCKET_NAME;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_DASH_YYYY_MM_DD;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_SPACE_MMMM_YYYY;

/**
 * Created by murashid on 13-Sep-17.
 * <h1>MyListPresenter</h1>
 * MyListPresenter presenter for MyListActivity
 *
 * @see MyListActivity
 */

public class MyListPresenter implements MyListModel.MyListModelImple {
    private MyListModel model;
    private MyListPresenterImple presenterImple;

    MyListPresenter(MyListPresenterImple presenterImple) {
        model = new MyListModel(this);
        this.presenterImple = presenterImple;
    }

    /**
     * method for passing values from view to model
     *
     * @param sessiontoken sessionToken
     */
    void getProfile(String sessiontoken) {
        presenterImple.startProgressBar();
        model.getProfile(sessiontoken);
    }

    /**
     * method for passing values from view to model
     *
     * @param sessiontoken session token
     * @param param        param of the value
     * @param value        new value
     */
    void updateProfileStatus(String sessiontoken, String param, String value) {
        presenterImple.startProgressBar();
        model.updateProfileStatus(sessiontoken, param, value);
    }


    /**
     * method for uploading imag in amazon
     *
     * @param amazonS3  object of UploadFileAmazonS3
     * @param mFileTemp file which has been to upload
     */
    String amazonUpload(UploadFileAmazonS3 amazonS3, File mFileTemp) {
        String BUCKETSUBFOLDER = VariableConstant.WORK_IMAGE;
        final String imageUrl = "https://" + BUCKET_NAME + "." + AMAZON_BASE_URL
                + BUCKETSUBFOLDER + "/"
                + mFileTemp.getName();
        Log.d("amazon", "amzonUpload: " + imageUrl);
        amazonS3.Upload_data(BUCKETSUBFOLDER, mFileTemp.getName(), mFileTemp, new UploadFileAmazonS3.UploadCallBack() {
            @Override
            public void sucess(String success) {
                Log.d("amazon", "sucess: " + imageUrl);
            }

            @Override
            public void error(String errormsg) {
                Log.d("amazon", "error: " + errormsg);
            }
        });

        return imageUrl;
    }

    String returmSendingToDisplayDate(String sendingDate) {
        String displayDate = "";
        String day = sendingDate.split("-")[2];
        displayDate = Utility.getDayOfMonthSuffix(Integer.parseInt(day), Utility.changeDateTimeFormat(sendingDate, FORMAT_DASH_YYYY_MM_DD, FORMAT_SPACE_MMMM_YYYY));
        return displayDate;
    }

    /**
     * method for calling api for upadater the mylist field based on values and param
     *
     * @param sessiontoken session Token
     * @param jsonObject   jsonObject
     */
    void updateWorkImage(String sessiontoken, JSONObject jsonObject) {
        presenterImple.startProgressBar();
        model.updateWorkImage(sessiontoken, jsonObject);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onFailureUpdateStatus() {
        presenterImple.stopProgressBar();
        presenterImple.onFailureUpdateStatus();
    }

    @Override
    public void onFailureLocation() {
        presenterImple.stopProgressBar();
        presenterImple.onFailureLocation();
    }

    @Override
    public void onSuccessUpdateStatus(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessUpdateStatus(msg);
    }

    @Override
    public void onSuccess(ProfileData profileData) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(profileData);
    }

    /**
     * method for returing the selected events from events list
     *
     * @param events events list
     * @return selected events
     */
    public String getEventsInString(ArrayList<MyListEvents> events) {

        StringBuilder eventsStringBuiler = new StringBuilder("");
        String prefix = "";
        for (MyListEvents event : events) {
            if (event.getStatus().equals("true")) {
                eventsStringBuiler.append(prefix);
                prefix = ",";
                eventsStringBuiler.append(event.getName());
            }
        }
        return new String(eventsStringBuiler);
    }

    interface MyListPresenterImple {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String msg);

        void onFailure();

        void onFailureUpdateStatus();

        void onSuccessUpdateStatus(String msg);

        void onSuccess(ProfileData profileData);

        void onFailureLocation();
    }
}
