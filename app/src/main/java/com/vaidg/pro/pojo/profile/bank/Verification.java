package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Verification implements Serializable {

	@SerializedName("document")
	@Expose
	private Document document;

	@SerializedName("details")
	@Expose
	private Object details;

	@SerializedName("additional_document")
	@Expose
	private AdditionalDocument additionalDocument;

	@SerializedName("details_code")
	@Expose
	private Object detailsCode;

	@SerializedName("status")
	@Expose
	private String status;

	public Document getDocument(){
		return document;
	}

	public Object getDetails(){
		return details;
	}

	public AdditionalDocument getAdditionalDocument(){
		return additionalDocument;
	}

	public Object getDetailsCode(){
		return detailsCode;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Verification{" + 
			"document = '" + document + '\'' + 
			",details = '" + details + '\'' + 
			",additional_document = '" + additionalDocument + '\'' + 
			",details_code = '" + detailsCode + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}