package com.vaidg.pro.landing.newSignup.findHospital;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.landing.newSignup.findHospital.model.FindHospitalAddressAdapter;
import com.vaidg.pro.landing.newSignup.findHospital.model.HospitalDataPojo;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class FindHospitalUtil {

    @ActivityScoped
    @Provides
    ArrayList<HospitalDataPojo> provideArrayList() {
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    FindHospitalAddressAdapter provideAdapter(ArrayList<HospitalDataPojo> arrayList) {
        return new FindHospitalAddressAdapter(arrayList);
    }
}
