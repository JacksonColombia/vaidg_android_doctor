package com.vaidg.pro.main.profile.support;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.profile.support.SupportData;

import java.util.ArrayList;

public interface SupportContract {

    interface View extends BaseView {

        /**
         * <p>This method is used to initialize views</p>
         */
        void initViews();

        /**
         * <p>This method is display passed error message in snack bar.</p>
         *
         * @param failureMsg the error message
         */
        void showError(String failureMsg);

        /**
         * <p>This method is display passed error message in snack bar.</p>
         */
        void showError();

        /**
         * <p>This method is provide list of {@link SupportData} objects from support API.</p>
         * @param supportDataList the list of {@link SupportData} objects
         */
        void onSuccess(ArrayList<SupportData> supportDataList);
    }

    interface Presenter extends BasePresenter {

        /**
         * <p>This method is make support API call.</p>
         */
        void getSupport();
    }
}
