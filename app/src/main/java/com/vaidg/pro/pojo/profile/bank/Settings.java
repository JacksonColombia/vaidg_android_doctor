package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Settings implements Serializable {

	@SerializedName("payouts")
	@Expose
	private Payouts payouts;

	@SerializedName("branding")
	@Expose
	private Branding branding;

	@SerializedName("payments")
	@Expose
	private Payments payments;

	@SerializedName("card_payments")
	@Expose
	private CardPayments cardPayments;

	@SerializedName("dashboard")
	@Expose
	private Dashboard dashboard;


	public Payouts getPayouts(){
		return payouts;
	}

	public Branding getBranding(){
		return branding;
	}

	public Payments getPayments(){
		return payments;
	}

	public CardPayments getCardPayments(){
		return cardPayments;
	}

	public Dashboard getDashboard(){
		return dashboard;
	}

	@Override
 	public String toString(){
		return 
			"Settings{" + 
			"payouts = '" + payouts + '\'' + 
			",branding = '" + branding + '\'' + 
			",payments = '" + payments + '\'' + 
			",card_payments = '" + cardPayments + '\'' + 
			",dashboard = '" + dashboard + '\'' + 
			"}";
		}
}