package com.vaidg.pro.main.schedule;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.ScheduleListAdapter;
import com.vaidg.pro.main.schedule.addschedule.ScheduleAddActivity;
import com.vaidg.pro.main.schedule.viewschedule.ScheduleViewActivity;
import com.vaidg.pro.pojo.shedule.ScheduleMonthPojo;
import com.vaidg.pro.pojo.shedule.Slot;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import dagger.android.support.DaggerFragment;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import javax.inject.Inject;

import static com.vaidg.pro.AppController.getInstance;

/**
 * Created by murashid on 23-Oct-17.
 * <h1>ScheduleFragment</h1>
 * ScheduleFragment for showing the month wise schedule
 */

public class ScheduleFragment extends DaggerFragment implements CompactCalendarView.CompactCalendarViewListener, View.OnClickListener, ScheculeFragmentContract.View {

    private static final String TAG = "ScheduleFragment";
    public boolean isFabOpen = false;
    private CompactCalendarView compactCalendarView;
    private TextView tvMonth, tvNoScheduleMsg;
    private SimpleDateFormat displayMonthFormat, sendingMonthFormat;
    private RecyclerView rvShedule;
    private ScheduleListAdapter scheduleListAdapter;
    private ArrayList<Slot> slots;
    private ArrayList<Event> calendarEvents;
    private FloatingActionButton fabAddSchedule, fabView, fabAdd;
    private TextView tvViewSchedule, tvAddSchedule;
    private Animation fade_open, fade_close, rotate_forward, rotate_backward;
    private Animation fade_half_open, fade_half_close;
    private LinearLayout llSchedule;

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    @Inject
    ScheculeFragmentContract.Presenter presenter;
    private boolean isFirstTime;
    private String currentDate = "";

    private BroadcastReceiver receiver;
    private IntentFilter filter;
    private Context mContext;

    public static ScheduleFragment newInstance() {
        return new ScheduleFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_schedule, container, false);
        init(rootView);
        return rootView;
    }

    /**
     * init the views
     *
     * @param rootView parent view
     */
    private void init(View rootView) {
        filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                presenter.getSchedule( sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()), false);
            }
        };

        sessionManager = SessionManager.getSessionManager(mContext);
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage(getString(R.string.gettingSchedule));
        progressDialog.setCancelable(false);

        Typeface fontBold = Utility.getFontBold(mContext);
        Typeface fontMedium = Utility.getFontMedium(mContext);
        Typeface fontRegular = Utility.getFontRegular(mContext);
        displayMonthFormat = new SimpleDateFormat("MMMM yyyy", Locale.US);
        sendingMonthFormat = new SimpleDateFormat("MM-yyyy", Locale.US);
        displayMonthFormat.setTimeZone(getInstance().getTimeZone());
        sendingMonthFormat.setTimeZone(getInstance().getTimeZone());
        calendarEvents = new ArrayList<>();
        slots = new ArrayList<>();
        scheduleListAdapter = new ScheduleListAdapter(((Activity)mContext), slots);

        tvMonth = rootView.findViewById(R.id.tvMonth);
        tvNoScheduleMsg = rootView.findViewById(R.id.tvNoScheduleMsg);
        TextView tvTitle = rootView.findViewById(R.id.tvTitle);
        compactCalendarView = rootView.findViewById(R.id.compactCalendarView);
        rvShedule = rootView.findViewById(R.id.rvShedule);
        ImageView ivPrevBtn = rootView.findViewById(R.id.ivPrevBtn);
        ImageView ivnextBtn = rootView.findViewById(R.id.ivnextBtn);
        fabAddSchedule = rootView.findViewById(R.id.fabAddSchedule);
        fabView = rootView.findViewById(R.id.fabView);
        fabAdd = rootView.findViewById(R.id.fabAdd);
        tvViewSchedule = rootView.findViewById(R.id.tvViewSchedule);
        tvAddSchedule = rootView.findViewById(R.id.tvAddSchedule);
        llSchedule = rootView.findViewById(R.id.llSchedule);

        fade_open = AnimationUtils.loadAnimation(mContext, R.anim.fade_open);
        fade_close = AnimationUtils.loadAnimation(mContext, R.anim.fade_close);
        rotate_forward = AnimationUtils.loadAnimation(mContext, R.anim.rotate_center_to_left);
        rotate_backward = AnimationUtils.loadAnimation(mContext, R.anim.rotate_left_to_center);
        fade_half_open = AnimationUtils.loadAnimation(mContext, R.anim.fade_half_open);
        fade_half_close = AnimationUtils.loadAnimation(mContext, R.anim.fade_half_close);

        rvShedule.setLayoutManager(new LinearLayoutManager(mContext));
        rvShedule.setAdapter(scheduleListAdapter);
        tvMonth.setTypeface(fontMedium);
        tvNoScheduleMsg.setTypeface(fontRegular);
        tvTitle.setTypeface(fontBold);
        tvMonth.setText(displayMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()));
        compactCalendarView.setUseThreeLetterAbbreviation(true);
        compactCalendarView.setDayColumnNames(getResources().getStringArray(R.array.calendarDays));
        tvMonth.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_18), mContext));
        compactCalendarView.setListener(this);

        ivPrevBtn.setOnClickListener(this);
        ivnextBtn.setOnClickListener(this);
        fabAddSchedule.setOnClickListener(this);
        fabView.setOnClickListener(this);
        fabAdd.setOnClickListener(this);
        tvAddSchedule.setOnClickListener(this);
        tvViewSchedule.setOnClickListener(this);

        isFirstTime = true;
        presenter.getSchedule( sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()), true);
        currentDate = sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth());
    }

    @Override
    public void onResume() {
        super.onResume();
        mContext.registerReceiver(receiver, filter);

        if (VariableConstant.IS_SHEDULE_EDITED) {
            VariableConstant.IS_SHEDULE_EDITED = false;
            isFirstTime = true;
            presenter.getSchedule( sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()), false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mContext.unregisterReceiver(receiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivnextBtn:
                compactCalendarView.showNextMonth();
                break;

            case R.id.ivPrevBtn:
                compactCalendarView.showPreviousMonth();
                break;

            case R.id.fabAddSchedule:
               animateFAB();
                break;

            case R.id.fabAdd:
            case R.id.tvAddSchedule:
                animateFAB();
                Intent intentAdd = new Intent(mContext, ScheduleAddActivity.class);
                startActivity(intentAdd);
                ((Activity)mContext).overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                break;

            case R.id.fabView:
            case R.id.tvViewSchedule:
                animateFAB();
                Intent intentView = new Intent(mContext, ScheduleViewActivity.class);
                startActivity(intentView);
                ((Activity)mContext).overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                break;
        }
    }

    @Override
    public void onDayClick(Date dateClicked) {
        if (isFabOpen) {
            animateFAB();
        }
        slots.clear();
        slots.addAll(presenter.createSlotFromSchedules(compactCalendarView.getEvents(dateClicked)));
        scheduleListAdapter.notifyDataSetChanged();

        if (slots.size() == 0) {
            tvNoScheduleMsg.setVisibility(View.VISIBLE);
        } else {
            tvNoScheduleMsg.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMonthScroll(Date firstDayOfNewMonth) {
        tvMonth.setText(displayMonthFormat.format(firstDayOfNewMonth));
        if (currentDate.equals(sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()))) {
            presenter.getSchedule( sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()), true);
        } else {
            presenter.getSchedule( sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()), false);
        }
    }

    /**
     * animate the whole view when we click the addSchedule button
     */
    public void animateFAB() {

        if (isFabOpen) {
            fabAddSchedule.startAnimation(rotate_backward);
            fabView.startAnimation(fade_close);
            fabAdd.startAnimation(fade_close);
            fabAdd.startAnimation(fade_close);
            tvViewSchedule.startAnimation(fade_close);
            tvAddSchedule.startAnimation(fade_close);
            llSchedule.startAnimation(fade_half_open);
            rvShedule.startAnimation(fade_half_open);
            fabView.setClickable(false);
            fabAdd.setClickable(false);
            tvViewSchedule.setClickable(false);
            tvAddSchedule.setClickable(false);
            isFabOpen = false;
        } else {
            fabAddSchedule.startAnimation(rotate_forward);
            fabView.startAnimation(fade_open);
            fabAdd.startAnimation(fade_open);
            tvViewSchedule.startAnimation(fade_open);
            tvAddSchedule.startAnimation(fade_open);
            llSchedule.startAnimation(fade_half_close);
            rvShedule.startAnimation(fade_half_close);
            fabView.setClickable(true);
            fabAdd.setClickable(true);
            tvViewSchedule.setClickable(true);
            tvAddSchedule.setClickable(true);
            isFabOpen = true;
        }
    }

    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(mContext, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(mContext, progressDialog);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, mContext);
    }


    @Override
    public void onSuccessGetSchedule(ScheduleMonthPojo scheduleMonthPojo) {
        calendarEvents.clear();
        compactCalendarView.removeAllEvents();
        compactCalendarView.addEvents(presenter.getEvents(scheduleMonthPojo.getData(), calendarEvents));
        if (isFirstTime) {
            isFirstTime = false;
            onDayClick(new Date());
        } else {
            onDayClick(compactCalendarView.getFirstDayOfCurrentMonth());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.attachView(this);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        Utility.progressDialogCancel(mContext, progressDialog);
        presenter.detachView();
        this.mContext =null;
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        //presenter.detachView();
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }
}
