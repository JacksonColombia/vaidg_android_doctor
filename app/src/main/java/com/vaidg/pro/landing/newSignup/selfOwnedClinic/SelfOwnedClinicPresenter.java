package com.vaidg.pro.landing.newSignup.selfOwnedClinic;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.vaidg.pro.R;
import com.vaidg.pro.landing.newSignup.findHospital.model.HospitalDataPojo;
import com.vaidg.pro.landing.newSignup.selfOwnedClinic.model.SelfOwnedClinicModel;
import com.vaidg.pro.location.model.SelectedLocationHolder;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.MediaBottomSelector;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.fileUtil.AppFileManger;
import com.videocompressor.com.CompressImage;
import com.videocompressor.com.DataModel.CompressedData;
import com.videocompressor.com.RxCompressObservable;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.vaidg.pro.utility.VariableConstant.AMAZON_BASE_URL;
import static com.vaidg.pro.utility.VariableConstant.BUCKET_NAME;
import static com.vaidg.pro.utility.VariableConstant.PROVIDER_INDEPENDENT_DOCTOR;

public class SelfOwnedClinicPresenter implements SelfOwnedClinicContract.Presenter, App_permission.Permission_Callback, MediaBottomSelector.Callback {


    private static final String TAG = SelfOwnedClinicPresenter.class.getSimpleName();
    private final String GALLERY = "gallery";
    private final String CAMERA = "camera";

    @Inject
    SelfOwnedClinicContract.View view;
    @Inject
    SelfOwnedClinicModel model;
    @Inject
    MediaBottomSelector mediaBottomSelector;
    @Inject
    App_permission app_permission;
    @Inject
    Activity activity;
    @Inject
    Utility utility;
    @Inject
    AppFileManger appFileManger;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    CompressImage compressImage;

    private File temp_file = null;
    private SelectedLocationHolder location;

    @Inject
    public SelfOwnedClinicPresenter() {
    }

    @Override
    public void openChooser() {
        temp_file = null;
        mediaBottomSelector.showBottomSheet(this);
    }

    @Override
    public void onCamera() {
        ArrayList<App_permission.Permission> permissions = new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.CAMERA);
        app_permission.getPermission_for_Sup_v4Fragment(CAMERA, permissions, null, this);

    }

    @Override
    public void onGallery() {
        ArrayList<App_permission.Permission> permissions = new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
        app_permission.getPermission_for_Sup_v4Fragment(GALLERY, permissions, null, this);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        if (isAllGranted && tag.equals(CAMERA)) {
            if (view != null) {
                try {

                    temp_file = appFileManger.getImageFile();

                    view.openCamera(utility.getUri_Path(temp_file));
                } catch (Exception e) {
                    view.showError(e.getMessage());
                }
            }
        } else if (isAllGranted && tag.equals(GALLERY)) {
            if (view != null)
                view.openGallery();
        }
    }

    @Override
    public void upDateToGallery() {
        if (temp_file == null)
            return;
      /*  Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(temp_file.getPath());
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);*/
        //Log.d(TAG, "upDateToGallery: 3"+temp_file.getPath()+ "=====>\n"+temp_file.getAbsolutePath());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            final Uri contentUri = Uri.fromFile(temp_file);
            scanIntent.setData(contentUri);
            activity.sendBroadcast(scanIntent);
        } else {
            activity.sendBroadcast(
                    new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(temp_file.getAbsolutePath())));
        }
    }
    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        if (tag.equals(GALLERY)) {
            app_permission.show_Alert_Permission(activity.getString(R.string.photo_access_text), activity.getString(R.string.gallery_acess_subtitle),
                    activity.getString(R.string.gallery_acess_message), stringArray);
        } else if (tag.equals(CAMERA)) {
            app_permission.show_Alert_Permission(activity.getString(R.string.camera_access_text), activity.getString(R.string.camera_acess_subtitle),
                    activity.getString(R.string.camera_acess_message), stringArray);
        }
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent) {
        if (parmanent) {
            if (tag.equals(GALLERY)) {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.photo_denied_text), activity.getString(R.string.gallery_denied_subtitle),
                        activity.getString(R.string.gallery_denied_message));
            } else if (tag.equals(CAMERA)) {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.camera_denied_text), activity.getString(R.string.camera_denied_subtitle),
                        activity.getString(R.string.camera_denied_message));
            }
        }
    }

    private void compressImage(String filePath) {
        Observer<CompressedData> observer = new Observer<CompressedData>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(CompressedData value) {
                if (value != null) {
//                    upload(new File(value.getPath()));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                if (view != null)
                    view.showError("Failed to collect!");
            }

            @Override
            public void onComplete() {
            }
        };
        RxCompressObservable observable = compressImage.compressImage(activity, filePath);
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }

    @Override
    public void compressedMedia(String file_path) {
        if (!view.isLogo()) {
//            model.addFileToAdapter(imageUrl);
            model.addFileToAdapterPath(file_path);
            view.notifyAdapter();
            view.HandlePostDeleteBtn(model.hasItem());
        } else {
            model.setImageLogoUrl(file_path);
            view.setLogo(file_path, Uri.fromFile(new File(file_path)));
        }
        //        if (networkStateHolder.isConnected()) {
//            compressImage(file_path);
//        } else {
//            if (view != null)
//                view.showError(activity.getString(R.string.no_internet_error));
//        }
    }

    @Override
    public String getRecentTemp() {
        return temp_file.getPath();
    }

    @Override
    public void deletePostImage(int currentItem) {
        model.deletePostImage(currentItem);
        view.HandlePostDeleteBtn(model.hasItem());
        view.notifyAdapter();

    }

    @Override
    public void parseOnActivityData(int resultCode, int requestCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case VariableConstant.REQUEST_CODE_LOCATION:
                    location = (SelectedLocationHolder) data.getSerializableExtra("location");
                    view.showLocatioon(location.getLocationDetail());
                    break;

            }
        }
    }

    @Override
    public void validate(String clinicName, String location) {
        if (Utility.isTextEmpty(clinicName))
            view.showError(activity.getString(R.string.errorEnterClinicName));
        else if (Utility.isTextEmpty(location))
            view.showError(activity.getString(R.string.errorSelectLocation));
        else if (Utility.isTextEmpty(model.getImageLogoUrl()))
            view.showError(activity.getString(R.string.errorSelectClinicLogo));
        else if (model.isMediaSizeEmpty())
            view.showError(activity.getString(R.string.errorAddClinicLogos));
        else
            sendBackData(clinicName);

    }

    private void sendBackData(String clinicName) {
        Intent intent = new Intent();
        HospitalDataPojo dataPojo = new HospitalDataPojo();
        dataPojo.setClinicName(clinicName);
        dataPojo.setPlaceId(this.location.getPlaceId());
        dataPojo.setClinicLogoApp(model.getImageLogoUrl());
        dataPojo.setClinicLogoWeb(model.getClinicImages());
        dataPojo.setLatitude(this.location.getLatitude());
        dataPojo.setLongitude(this.location.getLongitude());
        dataPojo.setAddress(this.location.getLocationDetail());
        dataPojo.setCity(this.location.getCity());
        dataPojo.setState(this.location.getState());
        dataPojo.setCountry(this.location.getCountry());
        dataPojo.setPostcode(this.location.getPincode());
        dataPojo.setType(PROVIDER_INDEPENDENT_DOCTOR);
        intent.putExtra("clinic", dataPojo);
        view.sendBackData(intent);
    }

}
