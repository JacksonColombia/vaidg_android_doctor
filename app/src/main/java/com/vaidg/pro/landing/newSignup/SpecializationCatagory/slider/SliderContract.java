package com.vaidg.pro.landing.newSignup.SpecializationCatagory.slider;

import com.vaidg.pro.BasePresenter;

/**
 * <h1>@SliderContract</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public interface SliderContract {

    interface View {
    }

    interface Presenter extends BasePresenter<View> {
    }
}
