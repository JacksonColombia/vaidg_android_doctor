package com.vaidg.pro.pojo.shedule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 05-Jul-17.
 */

public class ScheduleViewData implements Serializable {
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("addressId")
    @Expose
    private String addresssId;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("inCall")
    @Expose
    private String inCall;
    @SerializedName("outCall")
    @Expose
    private String outCall;
    @SerializedName("teleCall")
    @Expose
    private String teleCall;
    @SerializedName("radius")
    @Expose
    private String radius;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("days")
    @Expose
    private ArrayList<String> days;

    public String getInCall() {
        return inCall;
    }

    public String getOutCall() {
        return outCall;
    }

    public String getTeleCall() {
        return teleCall;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getAddresssId() {
        return addresssId;
    }

    public void setAddresssId(String addresssId) {
        this.addresssId = addresssId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public ArrayList<String> getDays() {
        return days;
    }

    public void setDays(ArrayList<String> days) {
        this.days = days;
    }
}
