package com.vaidg.pro.helpwithpassword;

import androidx.annotation.StringRes;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.phonevalidation.PhoneValidationPojo;

/**
 * <h1>HelpWithPasswordContract</h1>
 * <p>This contract define interface with required methods for view and presenter</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @see HelpWithPasswordActivity
 * @see HelpWithPasswordPresenterImple
 * @since 23/06/2020
 **/
public interface HelpWithPasswordContract {

    interface View extends BaseView {

        void onPhoneNumberInCorrect(@StringRes int resId);

        boolean isPhoneNumberCorrect();

        void onPhoneNumberCorrect();

        void onEmailInCorrect();

        void onEmailCorrect();

        void sessionExpired(String msg);

        void onSuccessPhone(PhoneValidationPojo phoneValidationPojo);

        void onSuccessEmail(String msg);

        /**
         * <p>This method is display passed error message in snack bar.</p>
         *
         * @param error the error message
         */
        void showError(String error);
    }

    interface Presenter extends BasePresenter {

        boolean verifyPhone(String phone, String countryCode);

        boolean verifyEmail(String email);

        void forgotPassword(String countryCode, String emailPhone, String type);
    }
}
