package com.vaidg.pro.landing.newSignup.selectCity.model;

/**
 * <h1>CitySelectionListener</h1>
 * <p>This adapter is used to load list of city with select on tap</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 13/06/2020
 **/
public interface CitySelectionListener {

    /**
     * <p>It's listen item click event and provide that item position</p>
     * @param position int clicked item position
     */
    void onItemSelected(int position);

    void onItemSelected(int position,String name);
}
