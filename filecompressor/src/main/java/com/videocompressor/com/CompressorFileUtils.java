package com.videocompressor.com;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;

import java.io.File;

/**
 * <h2>CompressorFileUtils</h2>
 * <p>
 * File handler for the video compressor.
 * </P>
 *
 * @author Suresh.
 * @version 1.0.
 */
public class CompressorFileUtils {
    private static CompressorFileUtils UTIL = null;

    private CompressorFileUtils() {
    }

    public static CompressorFileUtils getInstance() {
        if (UTIL == null) {
            UTIL = new CompressorFileUtils();
        }
        return UTIL;
    }

    /*
     * Video dir*/
    String dirVideoDir(Context context) {
        File f;
        if (checkWriteExternalPermission(context)) {
            f = new File(Environment.getExternalStorageDirectory(), File.separator + Config.COMPRESSED_DIR_NAME);
            f.mkdirs();
            f = new File(Environment.getExternalStorageDirectory(), File.separator + Config.COMPRESSED_DIR_NAME + Config.COMPRESSED_VIDEOS_DIR);
            f.mkdirs();
        } else {
            f = new File(context.getFilesDir(), File.separator + Config.COMPRESSED_DIR_NAME);
            f.mkdirs();
            f = new File(context.getFilesDir(), File.separator + Config.COMPRESSED_DIR_NAME + Config.COMPRESSED_VIDEOS_DIR);
            f.mkdirs();
        }
        return f.getPath();
    }

    /*
     *Image dir. */
    String dirImageDir(Context context) {
        File f;
        if (checkWriteExternalPermission(context)) {
            f = new File(Environment.getExternalStorageDirectory(), File.separator + Config.COMPRESSED_DIR_NAME);
            f.mkdirs();
            f = new File(Environment.getExternalStorageDirectory(), File.separator + Config.COMPRESSED_DIR_NAME + Config.COMPRESSED_IMAGE_DIR);
            f.mkdirs();
        } else {
            f = new File(context.getFilesDir(), File.separator + Config.COMPRESSED_DIR_NAME);
            f.mkdirs();
            f = new File(context.getFilesDir(), File.separator + Config.COMPRESSED_DIR_NAME + Config.COMPRESSED_IMAGE_DIR);
            f.mkdirs();
        }
        return f.getPath();
    }

    /*
     * Video merged dir*/
    String dirMergedDir(Context context) {
        File f;
        if (checkWriteExternalPermission(context)) {
            f = new File(Environment.getExternalStorageDirectory(), File.separator + Config.COMPRESSED_DIR_NAME);
            f.mkdirs();
            f = new File(Environment.getExternalStorageDirectory(), File.separator + Config.COMPRESSED_DIR_NAME + Config.MERGED_DIR);
            f.mkdirs();
        } else {
            f = new File(context.getFilesDir(), File.separator + Config.COMPRESSED_DIR_NAME);
            f.mkdirs();
            f = new File(context.getFilesDir(), File.separator + Config.COMPRESSED_DIR_NAME + Config.MERGED_DIR);
            f.mkdirs();
        }
        return f.getPath();
    }

    /*
     * Temp file dir*/
    String dirTempDir(Context context) {
        File f;
        if (checkWriteExternalPermission(context)) {
            f = new File(Environment.getExternalStorageDirectory(), File.separator + Config.COMPRESSED_DIR_NAME);
            f.mkdirs();
            f = new File(Environment.getExternalStorageDirectory(), File.separator + Config.COMPRESSED_DIR_NAME + Config.COMPRESSOR_TEMP_DIR);
            f.mkdirs();
        } else {
            f = new File(context.getFilesDir(), File.separator + Config.COMPRESSED_DIR_NAME);
            f.mkdirs();
            f = new File(context.getFilesDir(), File.separator + Config.COMPRESSED_DIR_NAME + Config.COMPRESSOR_TEMP_DIR);
            f.mkdirs();
        }
        return f.getPath();
    }


    /*
     * Video dir*/
    void dirVideoDir() {
        File f;
        f = new File(Environment.getExternalStorageDirectory(), File.separator + Config.COMPRESSED_DIR_NAME);
        f.mkdirs();
        f = new File(Environment.getExternalStorageDirectory(), File.separator + Config.COMPRESSED_DIR_NAME + Config.COMPRESSED_VIDEOS_DIR);
        f.mkdirs();
    }

    /*
     *Checking external storage is granted or not.*/
    private boolean checkWriteExternalPermission(Context context) {
        String permission = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

}
