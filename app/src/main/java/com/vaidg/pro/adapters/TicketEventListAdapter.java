package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.pojo.profile.helpcenter.TicketEvents;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_SPACE_DD_MMMM_YYYY;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_TIME_1_12_HRS;

/**
 * Created by murashid on 28-Dec-17.
 */

public class TicketEventListAdapter extends RecyclerView.Adapter<TicketEventListAdapter.ViewHolder> {

    private ArrayList<TicketEvents> events;
    private Typeface fontRegular, fontMedium;

    public TicketEventListAdapter(Context context, ArrayList<TicketEvents> events) {
        this.events = events;
        fontRegular = Utility.getFontRegular(context);
        fontMedium = Utility.getFontMedium(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_ticket_event, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvNameHead.setText(String.valueOf(events.get(position).getName().charAt(0)).toUpperCase());
        holder.tvName.setText(events.get(position).getName());
        holder.tvMessage.setText(events.get(position).getBody());

        try {
            holder.tvDate.setText(Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(events.get(position).getTimeStamp()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_DD_MMMM_YYYY));
            holder.tvTime.setText(Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(events.get(position).getTimeStamp()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_TIME_1_12_HRS));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvNameHead, tvName, tvDate, tvMessage, tvTime;

        public ViewHolder(View itemView) {
            super(itemView);
            tvNameHead = itemView.findViewById(R.id.tvNameHead);
            tvName = itemView.findViewById(R.id.tvName);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            tvTime = itemView.findViewById(R.id.tvTime);

            tvNameHead.setTypeface(fontMedium);
            tvName.setTypeface(fontRegular);
            tvDate.setTypeface(fontRegular);
            tvMessage.setTypeface(fontRegular);
            tvTime.setTypeface(fontRegular);
        }
    }

}
