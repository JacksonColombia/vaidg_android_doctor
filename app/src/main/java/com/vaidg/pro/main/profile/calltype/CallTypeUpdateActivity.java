package com.vaidg.pro.main.profile.calltype;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;

import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.pojo.profile.calltype.CallTypeData;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import org.json.JSONObject;

import java.util.ArrayList;

public class CallTypeUpdateActivity extends AppCompatActivity implements CallTypeCategoryListPresenter.View, CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "CategoryDocument";
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;

    private int position = 0;

    private CallTypeCategoryListPresenter presenter;
    private CallTypeData callTypeData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_type_update);

        init();
    }

    /**
     * initialize the values
     */
    private void init() {
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new CallTypeCategoryListPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.updating));
        progressDialog.setCancelable(false);

        Intent intent = getIntent();
        callTypeData = (CallTypeData) intent.getSerializableExtra("category");
        position = intent.getIntExtra("position", 0);
        position = intent.getIntExtra("position", 0);

        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontMedium = Utility.getFontMedium(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setTypeface(fontBold);
        tvTitle.setText(callTypeData.getCategoryName());

        TextView tvSave = findViewById(R.id.tvDone);
        tvSave.setText(getString(R.string.save));
        tvSave.setTypeface(fontBold);

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("categoryId", callTypeData.getCategoryId());
                    jsonObject.put("inCall", callTypeData.getInCall());
                    jsonObject.put("outCall", callTypeData.getOutCall());
                    jsonObject.put("teleCall", callTypeData.getTeleCall());

                    presenter.updateCallType(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail())
                            , jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        SwitchCompat switchInCall = findViewById(R.id.switchInCall);
        SwitchCompat switchOutCall = findViewById(R.id.switchOutCall);
        SwitchCompat switchTeleCall = findViewById(R.id.switchTeleCall);
        switchInCall.setTypeface(fontMedium);
        switchOutCall.setTypeface(fontMedium);
        switchTeleCall.setTypeface(fontMedium);
        switchInCall.setOnCheckedChangeListener(this);
        switchOutCall.setOnCheckedChangeListener(this);
        switchTeleCall.setOnCheckedChangeListener(this);

        setValues(callTypeData.getInCall(), switchInCall, findViewById(R.id.vInCall));
        setValues(callTypeData.getOutCall(), switchOutCall, findViewById(R.id.vOutCall));
        setValues(callTypeData.getTeleCall(), switchTeleCall, findViewById(R.id.vTeleCall));
    }

    private void setValues(String callType, SwitchCompat swith, View view) {
        Log.d(TAG, "setValues: " + callType);

        switch (callType) {
            case "0":
                swith.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                break;

            case "1":
                swith.setChecked(true);
                break;

            case "2":
                swith.setChecked(false);
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.switchInCall:
                if (b) {
                    callTypeData.setInCall("1");
                } else {
                    callTypeData.setInCall("2");
                }
                break;

            case R.id.switchOutCall:
                if (b) {
                    callTypeData.setOutCall("1");
                } else {
                    callTypeData.setOutCall("2");
                }
                break;

            case R.id.switchTeleCall:
                if (b) {
                    callTypeData.setTeleCall("1");
                } else {
                    callTypeData.setTeleCall("2");
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();
    }

    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccessGetCallType(ArrayList<CallTypeData> callTypeData) {

    }

    @Override
    public void onErrorUpdateCallType() {
        Toast.makeText(this, getString(R.string.plsActiveAtleastOneType), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessUpdateCallType(String msg) {
        Intent intent = new Intent();
        intent.putExtra("category", callTypeData);
        intent.putExtra("position", position);
        setResult(RESULT_OK, intent);
        closeActivity();
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, this);
    }
}
