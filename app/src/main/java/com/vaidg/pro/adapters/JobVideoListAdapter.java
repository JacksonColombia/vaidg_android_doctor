package com.vaidg.pro.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.VideoView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.vaidg.pro.R;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * Created by murashid on 20/5/18.
 */
public class JobVideoListAdapter extends RecyclerView.Adapter<JobVideoListAdapter.ViewHolder> {

    private ArrayList<String> imageUrls;
    private Activity mAcitvity;

    public JobVideoListAdapter(Activity mAcitvity, ArrayList<String> imageUrls) {
        this.mAcitvity = mAcitvity;
        this.imageUrls = imageUrls;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_video_view, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (imageUrls.get(position) != null && !imageUrls.get(position).equals("")) {

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.ic_notification);
            requestOptions.error(R.drawable.ic_notification);


            Glide.with(mAcitvity)
                    .load(imageUrls.get(position))
                    .apply(requestOptions)
                    .thumbnail(Glide.with(mAcitvity).load(imageUrls.get(position)))
                    .into(holder.ivJobPhoto);

          /*
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.isMemoryCacheable();
            Glide.with(mAcitvity).setDefaultRequestOptions(requestOptions).load(imageUrls.get(position)).into(holder.ivJobPhoto);*/
            holder.ivJobPhoto.setOnClickListener(view -> {
                // videoView.setBackground(null);
                showAlert(imageUrls.get(position));

            });
        } else {
            Glide.with(mAcitvity).setDefaultRequestOptions(new RequestOptions())
                    .load(R.drawable.ic_notification)
                    .into(holder.ivJobPhoto);
        }
    }

    public void showAlert(String questionImages) {
        if (Utility.isNetworkAvailable(mAcitvity))
            getVideoDialog(mAcitvity, questionImages, true);
        else
            getInstance().toast(mAcitvity.getResources().getString(R.string.networkErrorMsg));
    }

    /**
     * Creates a simple dialog box with as many buttons as you want
     *
     * @param context     The context of the dialog
     * @param cancelable  whether the dialog can be closed/cancelled by the user
     * @param layoutResID the resource id of the layout you want within the dialog
     * @return the dialog
     */
    public static Dialog getBaseDialog(Context context, boolean cancelable, int layoutResID) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(cancelable);
        dialog.setContentView(layoutResID);

        return dialog;
    }

    public void getVideoDialog(Context context, String videoLocation, boolean autoplay) {
        final Dialog dialog = getBaseDialog(context, true, R.layout.dialog);
        ((Activity) context).getWindow().setFormat(PixelFormat.TRANSLUCENT);
        final VideoView videoHolder = (VideoView) dialog.findViewById(R.id.videoDialog);
        final ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.pdDialog);
        ImageView image = dialog.findViewById(R.id.imageDialog);
        SimpleExoPlayerView exoPlayerView;
        SimpleExoPlayer exoPlayer;

        exoPlayerView = (SimpleExoPlayerView) dialog.findViewById(R.id.exoplayer);
        try {


            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
            exoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector);

            Uri videoURI = Uri.parse(videoLocation);

            DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
            ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
            MediaSource mediaSource = new ExtractorMediaSource(videoURI, dataSourceFactory, extractorsFactory, null, null);

            exoPlayerView.setPlayer(exoPlayer);
            exoPlayer.prepare(mediaSource);
            exoPlayer.setPlayWhenReady(true);
        } catch (Exception e) {
            Log.e("MainAcvtivity", " exoplayer error " + e.toString());
        }
        dialog.show();

        image.setOnClickListener(v -> {
            dialog.dismiss();
            // notifyDataSetChanged();
        });
       /* videoHolder.setVideoURI(Uri.parse(videoLocation));
        //videoHolder.setRotation(90);
        progressBar.setVisibility(View.VISIBLE);
        videoHolder.setOnPreparedListener(mp -> {
            mp.start();
            mp.setOnInfoListener((mp1, what, extra) -> {
                switch (what) {
                    case MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START: {
                        progressBar.setVisibility(View.GONE);
                        Log.w("TAG", "getVideoDialog: RENDER START"+"GONE" );
                        return true;
                    }
                    case MediaPlayer.MEDIA_INFO_BUFFERING_START: {
                        progressBar.setVisibility(View.VISIBLE);
                        Log.w("TAG", "getVideoDialog: BUFFER START"+"GONE" );
                        return true;
                    }
                    case MediaPlayer.MEDIA_INFO_BUFFERING_END: {
                       progressBar.setVisibility(View.GONE);
                        Log.w("TAG", "getVideoDialog:  END"+"GONE" );
                        return true;
                    }
                }
                return false;
            });
        });
     *//*   MediaController mediaController = new MediaController(context);
        videoHolder.setMediaController(mediaController);
        mediaController.setAnchorView(videoHolder);
        videoHolder.requestFocus();
        if (autoplay) {
            videoHolder.start();
        }*//*
        dialog.show();
        videoHolder.setOnCompletionListener(mp -> {
            dialog.dismiss();
            notifyDataSetChanged();
        });

        image.setOnClickListener(v -> {
            dialog.dismiss();
            notifyDataSetChanged();
        });*/
    }

    @Override
    public int getItemCount() {
        return imageUrls != null ? imageUrls.size() : ZERO;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivJobPhoto;

        public ViewHolder(View itemView) {
            super(itemView);
            ivJobPhoto = (ImageView) itemView.findViewById(R.id.ivJobPhoto);
        }
    }

}
