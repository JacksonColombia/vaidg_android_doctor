package com.vaidg.pro.landing.newSignup.SpecializationCatagory.previewScreen;

/**
 * <h1>@PreviewSceenContract</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public interface PreviewSceenContract {

    interface View {

    }

    interface Presenter {

    }
}
