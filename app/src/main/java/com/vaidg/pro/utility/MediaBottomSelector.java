package com.vaidg.pro.utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.WindowManager;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.vaidg.pro.databinding.PicChooserBottomsheetBinding;

/**
 * @author 3Embed.
 * @version 1.0.
 * @since 2/19/2018.
 */
public class MediaBottomSelector {
    private Activity activity;
    private BottomSheetDialog bottomSheetDialog;
    private Callback callback;

    public MediaBottomSelector(Activity activity) {
        this.activity = activity;
    }

    public void showBottomSheet(Callback mcallback) {
        this.callback = mcallback;
        if (bottomSheetDialog != null && bottomSheetDialog.isShowing()) {
            bottomSheetDialog.dismiss();
            bottomSheetDialog.cancel();
        }
        bottomSheetDialog = new BottomSheetDialog(activity);
        @SuppressLint("InflateParams")
        PicChooserBottomsheetBinding binding = PicChooserBottomsheetBinding.inflate(activity.getLayoutInflater());
        binding.cameraView.setOnClickListener(view ->
        {
            bottomSheetDialog.dismiss();
            bottomSheetDialog.cancel();
            if (callback != null) {
                callback.onCamera();
            }
        });

        binding.galleryView.setOnClickListener(view -> {
            bottomSheetDialog.dismiss();
            bottomSheetDialog.cancel();
            if (callback != null) {
                callback.onGallery();
            }
        });
        bottomSheetDialog.setContentView(binding.getRoot());
        WindowManager.LayoutParams lp = bottomSheetDialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        bottomSheetDialog.getWindow().setAttributes(lp);
        bottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        bottomSheetDialog.show();
    }


    public interface Callback {
        void onCamera();

        void onGallery();
    }
}
