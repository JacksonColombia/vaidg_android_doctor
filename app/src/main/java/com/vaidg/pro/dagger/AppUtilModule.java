package com.vaidg.pro.dagger;

import android.app.Activity;
import android.content.Context;
import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.utility.DeviceUuidFactory;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.fileUtil.AppFileManger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppUtilModule {
    @Singleton
    @Provides
    NetworkStateHolder getNetworkStateHolder() {
        return new NetworkStateHolder();
    }
    
    @Singleton
    @Provides
    Utility getUtility(Context context) {
        return new Utility(context);
    }

    @Singleton
    @Provides
    AppFileManger appFileManger(Context context) {
        return new AppFileManger(context);
    }

    @Provides
    UploadFileAmazonS3 amazonS3(Context context) {
        return UploadFileAmazonS3.getInstance(context);
    }

    @Singleton
    @Provides
    DeviceUuidFactory getDeviceUuidFactory(Context context) {
        return new DeviceUuidFactory(context);
    }

    @Singleton
    @Provides
    Gson provideGSON(){return new Gson();}

}

