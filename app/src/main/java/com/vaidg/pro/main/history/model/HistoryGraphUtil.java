package com.vaidg.pro.main.history.model;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.utility.SessionManager;

import dagger.Module;
import dagger.Provides;

/**
 * <h1>HistoryGraphUtil</h1>
 * <p>This dagger util is used to provide object reference to injected objects when declare in activity.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/07/2020
 **/
@Module
public class HistoryGraphUtil {
  @ActivityScoped
  @Provides
  SessionManager provide(Activity activity) {
    return SessionManager.getSessionManager(activity);
  }
}
