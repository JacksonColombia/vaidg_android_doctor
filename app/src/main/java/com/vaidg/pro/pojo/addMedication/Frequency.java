package com.vaidg.pro.pojo.addMedication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class Frequency implements Serializable {
  @SerializedName("_id")
  @Expose
  public String id;
  @SerializedName("name")
  @Expose
  public String name;
  @SerializedName("num")
  @Expose
  public String num;
  public boolean isChecked = false;

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNum() {
    return num;
  }

  public boolean isChecked() {
    return isChecked;
  }

  public void setChecked(boolean checked) {
    isChecked = checked;
  }
}