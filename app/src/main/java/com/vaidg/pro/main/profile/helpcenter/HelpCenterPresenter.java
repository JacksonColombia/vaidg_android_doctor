package com.vaidg.pro.main.profile.helpcenter;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;
import static com.vaidg.pro.utility.VariableConstant.USER_TYPE;

import android.app.Activity;
import android.util.Log;
import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.profile.helpcenter.HelpCenterData;
import com.vaidg.pro.pojo.profile.helpcenter.HelpCenterPojo;
import com.vaidg.pro.pojo.profile.helpcenter.Ticket;
import com.vaidg.pro.pojo.profile.support.SupportPojo;
import com.vaidg.pro.utility.OkHttp3ConnectionStatusCode;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.ServiceUrl;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 10-Sep-17.
 */

public class HelpCenterPresenter implements HelpCenterContract.Presenter {

    @Inject
    Activity activity;
    @Inject
    HelpCenterContract.View view;
    @Inject
    NetworkService service;
    @Inject
    SessionManager sessionManager;
    @Inject
    Gson gson;

    @Inject
    HelpCenterPresenter() {
    }

    @Override
    public void getAllTickets(String sessionToken, String email) {
        view.startProgressBar();
        service.getTicket(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, email)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if (view != null)
                                    view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    Gson gson = new Gson();
                                    HelpCenterPojo helpCenterPojo = gson.fromJson(response, HelpCenterPojo.class);
                                    HelpCenterData data = helpCenterPojo.getData();

                                    ArrayList<Ticket> wholeTicketData = new ArrayList<>();

                                    for (int i = 0; i < data.getOpen().size(); i++) {
                                        if (i == 0) {
                                            data.getOpen().get(i).setHeader(true);
                                            data.getOpen().get(i).setHeadername("open");
                                        }
                                        wholeTicketData.add(data.getOpen().get(i));
                                    }
                                    for (int i = 0; i < data.getClose().size(); i++) {
                                        if (i == 0) {
                                            data.getClose().get(i).setHeader(true);
                                            data.getClose().get(i).setHeadername("close");
                                        }
                                        wholeTicketData.add(data.getClose().get(i));
                                    }

                                    view.onSuccess(wholeTicketData);                                } else {
                                    if (view != null)
                                        view.onFailure(Utility.getMessage(errorBody));

                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        getAllTickets(sessionToken,email);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        if (view != null) {
                                            view.stopProgressBar();
                                            view.onFailure(Utility.getMessage(errorBody));
                                        }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().toast(Utility.getMessage(msg));
                                        Utility.logoutSessionExiperd(sessionManager, activity);
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                                if (view != null)
                                    view.stopProgressBar();
                                Utility.logoutSessionExiperd(sessionManager, activity);
                                break;
                            default:
                                if (view != null) {
                                    view.stopProgressBar();
                                    view.onFailure(Utility.getMessage(errorBody));
                                }
                                break;
                        }
                    } catch (Exception e) {
                        if (view != null) {
                            view.stopProgressBar();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if (view != null) {
                        view.stopProgressBar();
                        view.onFailure(Utility.getMessage(e.getMessage()));

                    }
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {

                }
            });

        // model.getAllTickets(sessionToken, email);
    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    interface HelpCenterPresenterImple {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String msg);

        void onFailure();

        void onSuccess(ArrayList<Ticket> wholeTicketData);
    }
}
