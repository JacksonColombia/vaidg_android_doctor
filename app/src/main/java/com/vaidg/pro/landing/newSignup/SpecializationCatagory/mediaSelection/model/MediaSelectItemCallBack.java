package com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection.model;

/**
 * <h1>@MediaSelectItemCallBack</h1>
 * <p> this is used for call backs</p>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public interface MediaSelectItemCallBack {

    /**
     * <h>@onCancel</h>
     * <p> this method used to delete uploaded file</p>
     *
     * @param position
     * @param Type
     */
    void onCancel(int position, int Type);
}
