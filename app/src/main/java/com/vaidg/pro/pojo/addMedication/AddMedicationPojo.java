package com.vaidg.pro.pojo.addMedication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class AddMedicationPojo implements Serializable {
  @SerializedName("message")
  @Expose
  public String message;
  @SerializedName("data")
  @Expose
  public AddMedicationData data;

  public String getMessage() {
    return message;
  }

  public AddMedicationData getData() {
    return data;
  }
}
