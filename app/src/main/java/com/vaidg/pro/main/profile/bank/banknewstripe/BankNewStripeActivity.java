package com.vaidg.pro.main.profile.bank.banknewstripe;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.hbb20.CountryCodePicker;
import com.vaidg.pro.AppController;
import com.vaidg.pro.BuildConfig;
import com.vaidg.pro.R;
import com.vaidg.pro.utility.DatePickerCommon;
import com.vaidg.pro.utility.MyImageHandler;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import dagger.android.support.DaggerAppCompatActivity;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.json.JSONObject;

import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by murashid on 26-Aug-17.
 * <h1>BankNewStripeActivity</h1>
 * BankNewStripeActivity activity for adding new stripe account
 */


public class BankNewStripeActivity extends DaggerAppCompatActivity implements View.OnClickListener, BankNewStripeContract.View, DatePickerCommon.DateSelected {

    private static final String TAG = "BankNewStripe";
    private final int REQUEST_CODE_GALLERY = 0x1;
    private final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    private final int REQUEST_CODE_CROP_IMAGE = 0x3;
    ImageView ivAddFile;
    Typeface fontMedium;
    String[] dob;
    private EditText etName, etLName, etEmail, etPersonalId, etState, etPostalCode, etCity, etCountry, etAddress;
    private TextInputLayout tilName, tilLastName, tilEmail, tilDob, tilPersonalId, tilState, tilPostalCode, tilCity, tilCountry, tilAddress;
    private ProgressDialog progressDialog;
    private String imageUrl = "";
    private boolean isPicturetaken;
    private File mFileTemp;
    private String token = "", ip = "";
    private SessionManager sessionManager;
    @Inject
    BankNewStripeContract.Presenter presenter;

    private EditText etDob;
    private DatePickerCommon datePickerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_new_stripe_details);
        initViews();
        presenter.getIp();
    }

    /**
     * <h1>initActionBar</h1>
     * initilize the action bar
     */

    private void initViews() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        datePickerFragment = new DatePickerCommon();
        datePickerFragment.setDatePickerType(2);
        datePickerFragment.setCallBack(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_x);
        }

        String state = Environment.getExternalStorageState();
        sessionManager = SessionManager.getSessionManager(this);
        token = AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail());

        String filename = VariableConstant.BANK_FILE_NAME + System.currentTimeMillis() + ".png";
        MyImageHandler myImageHandler = MyImageHandler.getInstance();
        mFileTemp = new File(myImageHandler.getAlbumStorageDir(this, VariableConstant.BANK_PIC_DIR, true), filename);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.submitting));
        progressDialog.setCancelable(false);

        fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.addStripeAccount));
        tvTitle.setTypeface(fontBold);

        MaterialButton tvDone = findViewById(R.id.btnDone);
        tvDone.setText(getString(R.string.save));
        tvDone.setTypeface(fontBold);
        tvDone.setOnClickListener(v -> addBankStripeDetais());

        ivAddFile = findViewById(R.id.ivAddFile);
        ivAddFile.setOnClickListener(this);

        etName = findViewById(R.id.etName);
        etLName = findViewById(R.id.etLName);
        etEmail = findViewById(R.id.etEmail);
        etDob = findViewById(R.id.etDob);
        etPersonalId = findViewById(R.id.etPersonalId);
        etState = findViewById(R.id.etState);
        etPostalCode = findViewById(R.id.etPostalCode);
        etCity = findViewById(R.id.etCity);
        etCountry = findViewById(R.id.etCountry);
        etAddress = findViewById(R.id.etAddress);

        tilName = findViewById(R.id.tilName);
        tilLastName = findViewById(R.id.tilLastName);
        tilEmail = findViewById(R.id.tilEmail);
        tilDob = findViewById(R.id.tilDob);
        tilPersonalId = findViewById(R.id.tilPersonalId);
        tilState = findViewById(R.id.tilState);
        tilPostalCode = findViewById(R.id.tilPostalCode);
        tilCity = findViewById(R.id.tilCity);
        tilCountry = findViewById(R.id.tilCountry);
        tilAddress = findViewById(R.id.tilAddress);

        etName.setTypeface(fontMedium);
        etLName.setTypeface(fontMedium);
        etDob.setTypeface(fontMedium);
        etPersonalId.setTypeface(fontMedium);
        etState.setTypeface(fontMedium);
        etPostalCode.setTypeface(fontMedium);
        etCity.setTypeface(fontMedium);
        etCountry.setTypeface(fontMedium);
        etAddress.setTypeface(fontMedium);

        dob = new String[3];
        dob[0] = "";

        etDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!datePickerFragment.isResumed()) {
                    datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                }
            }
        });


        etDob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (!datePickerFragment.isResumed()) {
                        datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                    }
                }
            }
        });


        final CountryCodePicker ccp = findViewById(R.id.ccp);

        etCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ccp.launchCountrySelectionDialog();
            }
        });


        etCountry.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ccp.launchCountrySelectionDialog();
                }
            }
        });


        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                etCountry.setText(ccp.getSelectedCountryNameCode());
                etPostalCode.requestFocus();
            }
        });


        try {
            ccp.setCountryForPhoneCode(Integer.parseInt(sessionManager.getCountryCode().substring(1)));
            etCountry.setText(ccp.getSelectedCountryNameCode());
        } catch (Exception e) {
            e.printStackTrace();
        }

        etName.setText(sessionManager.getFirstName());
        etEmail.setText(sessionManager.getEmail());
        etLName.setText(sessionManager.getLastName());

        if (!sessionManager.getDob().equals("")) {
            datePickerFragment.convetSendingToDisplayDate(sessionManager.getDob());
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        Utility.progressDialogCancel(this, progressDialog);
        finish();
        overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivAddFile:
                String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
                if (EasyPermissions.hasPermissions(this, perms)) {
                    selectImage();
                } else {
                    // Do not have permissions, requesting permission
                    EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
                            102, perms);
                }

                break;

            /*case R.id.tvDone:
                addBankStripeDetais();
                break;*/
        }
    }

    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onSuccess(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        closeActivity();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    /**********************************************************************************************/

    @Override
    public void ipAddress(String ip) {
        if (ip.contains("localhost/")) {
            ip = ip.replace("localhost/", "");
        }
        this.ip = ip;
        Log.d(TAG, "ipAddress: " + ip);
    }

    @Override
    public void onErrorImageUpload() {
        Toast.makeText(this, getString(R.string.plsUploadIdProf), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorFirstName() {
        resetTile(tilName, getString(R.string.enterAccountHoldername));
    }

    @Override
    public void onErrorLastName() {
        resetTile(tilLastName, getString(R.string.enterLastName));
    }

    @Override
    public void onErrorEmail() {
        resetTile(tilEmail, getString(R.string.enterValidEmail));
    }

    @Override
    public void onErrorDob() {
        resetTile(tilDob, getString(R.string.selectDob));
    }

    @Override
    public void onErrorSSN() {
        resetTile(tilPersonalId, getString(R.string.enterPersonalid));
    }

    @Override
    public void onErrorAddress() {
        resetTile(tilAddress, getString(R.string.enterAddress));
    }

    @Override
    public void onErrorCity() {
        resetTile(tilCity, getString(R.string.enterCity));
    }

    @Override
    public void onErrorCountry() {
        resetTile(tilCity, getString(R.string.enterCountry));
    }

    @Override
    public void onErrorState() {
        resetTile(tilState, getString(R.string.enterState));
    }

    @Override
    public void onErrorZipcode() {
        resetTile(tilPostalCode, getString(R.string.enterPostalCode));
    }

    @Override
    public void getImage(String imageUrl) {
        try {
            UploadFileAmazonS3 amazonS3 = UploadFileAmazonS3.getInstance(this);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("business_type", "individual");
            jsonObject.put("line1", etAddress.getText().toString().trim());
            jsonObject.put("city", etCity.getText().toString().trim());
            jsonObject.put("state", etState.getText().toString().trim());
            jsonObject.put("country", etCountry.getText().toString().trim());
            jsonObject.put("postal_code", etPostalCode.getText().toString().trim());
            jsonObject.put("day", dob[2]);
            jsonObject.put("month", dob[1]);
            jsonObject.put("year", dob[0]);
            jsonObject.put("url","www.appscrip.com");
            jsonObject.put("email", etEmail.getText().toString().trim());
            jsonObject.put("first_name", etName.getText().toString().trim());
            jsonObject.put("last_name", etLName.getText().toString().trim());
            jsonObject.put("gender",getGender(sessionManager.getGender()).toLowerCase());
            jsonObject.put("id_number", etPersonalId.getText().toString().trim());
            jsonObject.put("phone", sessionManager.getPhoneNumber());
            jsonObject.put("ssn_last_4", etPersonalId.length() > 4 ? etPersonalId.getText().toString().trim().substring(etPersonalId.length() - 4) :
                    etPersonalId.getText().toString().trim());
            //jsonObject.put("date", Utility.getCurrentTime());
            jsonObject.put("ip", ip);
            jsonObject.put("document", imageUrl);
            presenter.addBankDetails(jsonObject, isPicturetaken, amazonS3, mFileTemp);

            Log.d(TAG, "addBankStripeDetais: Request " + jsonObject);

        } catch (Exception e) {
            Log.d(TAG, "addBankStripeDetais: " + e);
            e.printStackTrace();
        }

    }

    /**
     * set the error for empty field
     *
     * @param textInputLayout empty field
     * @param errorMsg        error msg
     */
    void resetTile(TextInputLayout textInputLayout, String errorMsg) {
        tilName.setErrorEnabled(false);
        tilLastName.setErrorEnabled(false);
        tilEmail.setErrorEnabled(false);
        tilDob.setErrorEnabled(false);
        tilPersonalId.setErrorEnabled(false);
        tilState.setErrorEnabled(false);
        tilState.setErrorEnabled(false);
        tilPostalCode.setErrorEnabled(false);
        tilCity.setErrorEnabled(false);
        tilCountry.setErrorEnabled(false);
        tilAddress.setErrorEnabled(false);

        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(errorMsg);
        }
    }

    /**
     * method for senting data to presenter for adding bank account
     */
    private void addBankStripeDetais() {
        try {

            UploadFileAmazonS3 amazonS3 = UploadFileAmazonS3.getInstance(this);
            presenter.amazonUpload(amazonS3, mFileTemp);
        } catch (Exception e) {
            Log.d(TAG, "addBankStripeDetais: " + e);
            e.printStackTrace();
        }
    }

    /**
     * <p>This method is return displaying value of gender from the passed id.</p>
     *
     * @param gender the id of gender type
     * @return the string of displaying gender value
     */
    public String getGender(String gender) {
        switch (gender) {
            case "1":
                return getString(R.string.male);
            case "2":
                return getString(R.string.feMale);
            case "3":
                return getString(R.string.others);
            default:
                return "-";
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * method for creating alert dialog for choose option for select image
     */
    private void selectImage() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        View view = LayoutInflater.from(this).inflate(R.layout.profile_pic_options, null);
        alertDialogBuilder.setView(view);

        final AlertDialog mDialog = alertDialogBuilder.create();
        mDialog.setCancelable(false);
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        Button btnCamera = view.findViewById(R.id.camera);
        Button btnCancel = view.findViewById(R.id.cancel);
        Button btnGallery = view.findViewById(R.id.gallery);
        Button btnRemove = view.findViewById(R.id.removephoto);
        TextView tvHeader = view.findViewById(R.id.tvHeader);

        btnCamera.setTypeface(fontMedium);
        btnCancel.setTypeface(fontMedium);
        btnGallery.setTypeface(fontMedium);
        btnRemove.setTypeface(fontMedium);
        tvHeader.setTypeface(fontMedium);

        if (isPicturetaken) {
            btnRemove.setVisibility(View.VISIBLE);
        } else {
            btnRemove.setVisibility(View.GONE);
        }

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
                mDialog.dismiss();
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
                mDialog.dismiss();
            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivAddFile.setImageResource(R.drawable.vector_color_primary_add_file);
                isPicturetaken = false;
                mDialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    /**
     * method for opening camera
     */
    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mImageCaptureUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", mFileTemp);
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (Exception e) {
            Log.d("error", "cannot take picture", e);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * mehtod for opening gallery
     */
    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode != RESULT_OK) {
                return;
            }

            switch (requestCode) {

                case REQUEST_CODE_GALLERY:

                    Bitmap bitmap;
                    try {
                        InputStream inputStream = this.getContentResolver().openInputStream(
                                data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                        Utility.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();

                        bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                        bitmap = Bitmap.createScaledBitmap(bitmap, ivAddFile.getWidth(), ivAddFile.getHeight(), true);
                        ivAddFile.setImageBitmap(bitmap);

                        isPicturetaken = true;

                    } catch (Exception e) {

                        Log.d("", "Error while creating temp file", e);
                    }

                    break;
                case REQUEST_CODE_TAKE_PICTURE:
                    isPicturetaken = true;
                    bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                    bitmap = Bitmap.createScaledBitmap(bitmap, ivAddFile.getWidth(), ivAddFile.getHeight(), true);
                    ivAddFile.setImageBitmap(bitmap);
                    break;
            }
        } catch (Exception e) {
            Log.d(TAG, "onActivityResult: " + e);
        }
    }

    @Override
    public void onDateSelected(String sendingFormat, String displayFormat) {
        etDob.setText(displayFormat);
        dob = sendingFormat.split("-");
        etPersonalId.requestFocus();
        Utility.showKeyBoard(this, etPersonalId);
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

}
