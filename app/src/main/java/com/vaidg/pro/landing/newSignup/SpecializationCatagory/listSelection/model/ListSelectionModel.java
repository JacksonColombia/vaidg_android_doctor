package com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection.model;

import com.vaidg.pro.BaseModel;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.pojo.PreDefined;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * <h1>@ListSelectionModel</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class ListSelectionModel extends BaseModel {

    private ArrayList<PreDefined> pareDefined;

    @Inject
    public ListSelectionModel() {
    }

    public void selectSingleList(int position) {
        for (int i = 0; i < pareDefined.size(); i++) {
            if (position == i)
                pareDefined.get(i).setSelected(true);
            else
                pareDefined.get(i).setSelected(false);
        }
    }

    public void getData(ArrayList<PreDefined> preDefineds) {
        this.pareDefined = preDefineds;
    }

    public void selectMultiList(int position) {
        if(position != -1) {
            if (pareDefined.get(position).isSelected())
                pareDefined.get(position).setSelected(false);
            else
                pareDefined.get(position).setSelected(true);
        }
    }

    public boolean isCheck() {
        for (PreDefined preDefined : pareDefined) {
            if (preDefined.isSelected())
                return true;
        }
        return false;
    }

    public void saveData(Attribute currentItem) {
        String data = "";
        if (pareDefined.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (PreDefined preDefined : pareDefined) {
                if (preDefined.isSelected())
                    sb.append(preDefined.getId()).append(",");
            }
            data = sb.deleteCharAt(sb.length() - 1).toString();
        }

        currentItem.setData(data);
    }
}
