package com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection;

import com.vaidg.pro.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>MediaSelectDaggerModule</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
@Module
public interface MediaSelectDaggerModule {

    @FragmentScoped
    @Binds
    MediaSelectContract.Presenter providePresenter(MediaSelectPresenter presenter);

}
