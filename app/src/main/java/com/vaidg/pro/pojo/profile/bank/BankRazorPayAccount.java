package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BankRazorPayAccount implements Parcelable
{

    @SerializedName("ifsc")
    @Expose
    private String ifsc;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("notes")
    @Expose
    private List<Object> notes = null;
    @SerializedName("account_number")
    @Expose
    private String accountNumber;
    public final static Creator<BankRazorPayAccount> CREATOR = new Creator<BankRazorPayAccount>() {


        @SuppressWarnings({
                "unchecked"
        })
        public BankRazorPayAccount createFromParcel(android.os.Parcel in) {
            return new BankRazorPayAccount(in);
        }

        public BankRazorPayAccount[] newArray(int size) {
            return (new BankRazorPayAccount[size]);
        }

    }
            ;

    protected BankRazorPayAccount(android.os.Parcel in) {
        this.ifsc = ((String) in.readValue((String.class.getClassLoader())));
        this.bankName = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.notes, (Object.class.getClassLoader()));
        this.accountNumber = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public BankRazorPayAccount() {
    }

    /**
     *
     * @param notes
     * @param name
     * @param bankName
     * @param ifsc
     * @param accountNumber
     */
    public BankRazorPayAccount(String ifsc, String bankName, String name, List<Object> notes, String accountNumber) {
        super();
        this.ifsc = ifsc;
        this.bankName = bankName;
        this.name = name;
        this.notes = notes;
        this.accountNumber = accountNumber;
    }

    public String getIfsc() {
        return ifsc;
    }

    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getNotes() {
        return notes;
    }

    public void setNotes(List<Object> notes) {
        this.notes = notes;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(ifsc);
        dest.writeValue(bankName);
        dest.writeValue(name);
        dest.writeList(notes);
        dest.writeValue(accountNumber);
    }

    public int describeContents() {
        return 0;
    }

}