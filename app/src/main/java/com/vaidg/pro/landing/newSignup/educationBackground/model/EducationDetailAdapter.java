package com.vaidg.pro.landing.newSignup.educationBackground.model;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.databinding.ItemEducationDetailBinding;
import com.vaidg.pro.pojo.profile.DegreeDetailsData;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * <h1>@EducationDetailsActivity</h1>
 * <p> EducationDetailAdapter adapter to list out the added education details from AddEditEducationActivity</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 27/04/2020.
 **/
public class EducationDetailAdapter extends RecyclerView.Adapter<EducationDetailAdapter.EducationDetailViewHolder> {

    private ArrayList<DegreeDetailsData> arrayList;
    private EducationDetailsCallBack callback;
    private boolean isEditMode;

    public EducationDetailAdapter(ArrayList<DegreeDetailsData> arrayList) {
        this(arrayList, true);
    }

    public EducationDetailAdapter(ArrayList<DegreeDetailsData> arrayList, boolean isEditMode) {
        this.arrayList = arrayList;
        this.isEditMode = isEditMode;
    }

    public void setEditMode(boolean editMode) {
        isEditMode = editMode;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public EducationDetailAdapter.EducationDetailViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ItemEducationDetailBinding binding = ItemEducationDetailBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        return new EducationDetailAdapter.EducationDetailViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull EducationDetailAdapter.EducationDetailViewHolder holder, int position) {
        holder.binding.ivEditEducation.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        holder.binding.tvDegree.setText(arrayList.get(position).getName());
        holder.binding.tvUniversity.setText(arrayList.get(position).getCollege());
        holder.binding.tvYear.setText(arrayList.get(position).getYear());
        arrayList.get(position).setId(arrayList.get(position).getId());
        arrayList.get(position).setEdited(true);

    }

    @Override
    public int getItemCount() {
        return  arrayList != null ? arrayList.size() : ZERO;
    }

    public void setCallback(EducationDetailsCallBack callback) {
        this.callback = callback;
    }

    class EducationDetailViewHolder extends RecyclerView.ViewHolder {

        ItemEducationDetailBinding binding;

        EducationDetailViewHolder(@NonNull ItemEducationDetailBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            if (isEditMode)
                this.binding.ivEditEducation.setOnClickListener(v -> callback.EditOnSelect(getAdapterPosition()));
        }
    }
}
