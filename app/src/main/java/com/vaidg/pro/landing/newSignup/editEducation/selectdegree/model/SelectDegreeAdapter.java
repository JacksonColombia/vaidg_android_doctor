package com.vaidg.pro.landing.newSignup.editEducation.selectdegree.model;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.databinding.ItemSelectDegeeBinding;
import com.vaidg.pro.landing.newSignup.editEducation.selectdegree.SelectDegreeActivity;
import com.vaidg.pro.landing.newSignup.editEducation.selectdegree.SelectDegreeContract;
import com.vaidg.pro.pojo.signup.DegreeData;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * <h1>SelectDegreeAdapter</h1>
 * <p>This adapter is used to load list of degree with select on tap</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @see com.vaidg.pro.landing.newSignup.editEducation.selectdegree.SelectDegreeActivity
 * @see com.vaidg.pro.landing.newSignup.editEducation.selectdegree.SelectDegreeUtil
 * @since 15/06/2020
 **/
public class SelectDegreeAdapter extends RecyclerView.Adapter<SelectDegreeAdapter.SelectDegreeViewHolder> {

    private ArrayList<DegreeData> arrayList;
    private SelectDegreeContract.Presenter callBack;

    public SelectDegreeAdapter(ArrayList<DegreeData> arrayList) {
        this.arrayList = arrayList;
    }

    public void setCallBack(SelectDegreeContract.Presenter callBack) {
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public SelectDegreeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemSelectDegeeBinding binding = ItemSelectDegeeBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new SelectDegreeViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectDegreeViewHolder holder, int position) {
        holder.binding.tvDegree.setText(arrayList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return  arrayList != null ? arrayList.size() : ZERO;
    }

    class SelectDegreeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemSelectDegeeBinding binding;

        public SelectDegreeViewHolder(@NonNull ItemSelectDegeeBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
            binding.tvDegree.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (callBack != null)
                callBack.onItemSelected(getAdapterPosition());
        }
    }
}
