package com.vaidg.pro.main.profile.profiledetail;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.changepassword.ChangePasswordActivity;
import com.vaidg.pro.databinding.ActivityProfileDetailBinding;
import com.vaidg.pro.landing.newSignup.educationBackground.EducationDetailsActivity;
import com.vaidg.pro.landing.newSignup.selfOwnedClinic.SelfOwnedClinicActivity;
import com.vaidg.pro.main.profile.editemailphone.EditEmailPhoneActivity;
import com.vaidg.pro.main.profile.profiledetail.model.ProfileAdditionalDetailsAdapter;
import com.vaidg.pro.pojo.profile.DegreeDetailsData;
import com.vaidg.pro.pojo.profile.ProfileData;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import java.util.Locale;

import javax.inject.Inject;

import eu.janmuller.android.simplecropimage.CropImage;

import static com.vaidg.pro.utility.VariableConstant.DATA;
import static com.vaidg.pro.utility.VariableConstant.IS_DISPLAY;
import static com.vaidg.pro.utility.VariableConstant.PROVIDER_UNDER_HOSPITAL;

public class ProfileDetailActivity extends BaseDaggerActivity implements ProfileDetailContract.View, View.OnClickListener {

    @Inject
    SessionManager sessionManager;

    @Inject
    ProfileDetailContract.Presenter presenter;

    @Inject
    ProfileAdditionalDetailsAdapter profileAdditionalDetailsAdapter;

    @Inject
    App_permission app_permission;

    private ActivityProfileDetailBinding binding;

    private ProfileData profileData;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProfileDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
        initRv();
        initViewOnClickListeners();
        updateUI();
    }

    @Override
    public void initViews() {

        if (getIntent() != null && getIntent().hasExtra("data")) {
            profileData = getIntent().getParcelableExtra("data");
        } else {
            onBackPressed();
        }

        setSupportActionBar(binding.includeToolbar.toolbar);
        binding.includeToolbar.toolbar.setElevation(0f);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        binding.nsvMain.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (scrollY > oldScrollY && scrollY > (binding.tvName.getY() + binding.tvName.getHeight() - 5)) {
                binding.includeToolbar.tvTitle.animate().getInterpolator().getInterpolation(1);
                binding.includeToolbar.tvTitle.setVisibility(View.VISIBLE);
                binding.includeToolbar.tvTitle.setText(sessionManager.getTitle().concat(". ").concat(sessionManager.getFirstName().concat(Utility.isTextEmpty(sessionManager.getLastName()) ? "" : " ".concat(sessionManager.getLastName()))));
            }

            if (scrollY < oldScrollY && scrollY < (binding.tvName.getY() + binding.tvName.getHeight() - 5)) {
                binding.includeToolbar.tvTitle.animate().getInterpolator().getInterpolation(0);
                binding.includeToolbar.tvTitle.setVisibility(View.GONE);
            }

            if (scrollY == 0) {
                Utility.printLog("ProfileDetails", "Scroll Y 0");
            }
            if (scrollY == (v.getMeasuredHeight() - v.getChildAt(0).getMeasuredHeight())) {
                Utility.printLog("ProfileDetails", "Bottom Scroll");
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.msgUpdatingProfile));


    }

    @Override
    public void initViewOnClickListeners() {
        binding.ivEditProfile.setOnClickListener(this);
        binding.tvEmail.setOnClickListener(this);
        binding.tvPhone.setOnClickListener(this);
        binding.tvChangePassword.setOnClickListener(this);
        binding.tvClinicName.setOnClickListener(this);
        binding.tvDegree.setOnClickListener(this);
        binding.btnLogout.setOnClickListener(this);
    }

    @Override
    public void initRv() {
        binding.rvAdditionalDetails.setLayoutManager(new LinearLayoutManager(this));
        binding.rvAdditionalDetails.setAdapter(profileAdditionalDetailsAdapter);
        profileAdditionalDetailsAdapter.setonAdditionalDetailsClickListener(presenter);
    }

    @Override
    public void updateUI() {
        binding.sdvProfile.setImageURI(sessionManager.getProfilePic());
        binding.tvName.setText(sessionManager.getTitle().concat(". ").concat(sessionManager.getFirstName().concat(Utility.isTextEmpty(sessionManager.getLastName()) ? "" : " ".concat(sessionManager.getLastName()))));
        binding.tvDob.setText(sessionManager.getDob().isEmpty() ? "-" : sessionManager.getDob());
        binding.tvGender.setText(getGender(sessionManager.getGender()));
        binding.tvEmail.setText(sessionManager.getEmail());
        binding.tvPhone.setText(sessionManager.getCountryCode().concat(" ").concat(sessionManager.getPhoneNumber()));
        binding.tvChangePassword.setText(sessionManager.getPassword());
        binding.tvChangePassword.setTransformationMethod(new PasswordTransformationMethod());
        binding.tvCity.setText(profileData.getCityName());
        binding.tvSpecialization.setText(profileData.getCatName());
        binding.tvExperience.setText(profileData.getYearOfExp());
        if (profileData.getDoctorType().equalsIgnoreCase(String.valueOf(PROVIDER_UNDER_HOSPITAL))) {
            binding.tvClinicName.setText(profileData.getHospitalName());
        } else {
            binding.tvClinicName.setText(profileData.getClinicName());
        }
        String degreeDelim = "";
        StringBuilder degreeBuilder = new StringBuilder();
        for (DegreeDetailsData degreeDetailsData : profileData.getDegree()) {
            degreeBuilder.append(degreeDelim).append(degreeDetailsData.getName());
            degreeDelim = ", ";
        }
        binding.tvDegree.setText(degreeBuilder.toString());
        presenter.addAdditionalDetailsData(profileData.getMetaDataArr());
    }

    /**
     * <p>This method is return displaying value of gender from the passed id.</p>
     *
     * @param gender the id of gender type
     * @return the string of displaying gender value
     */
    public String getGender(String gender) {
        switch (gender) {
            case "1":
                return getString(R.string.male);
            case "2":
                return getString(R.string.feMale);
            case "3":
                return getString(R.string.others);
            default:
                return "-";
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivEditProfile:
                presenter.openChooser();
                break;
            case R.id.tvEmail:
                launchEditEmailPhone(false);
                break;
            case R.id.tvPhone:
                launchEditEmailPhone(true);
                break;
            case R.id.tvChangePassword:
                launchChangePassword();
                break;
            case R.id.tvClinicName:
                launchClinicDetails();
                break;
            case R.id.tvDegree:
                launchEducationDetails();
                break;
            case R.id.btnLogout:
                presenter.logout();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
       // if (VariableConstant.IS_PROFILE_EDITED) {
         //   VariableConstant.IS_PROFILE_EDITED = false;
            presenter.getProfile();
       // }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        presenter.onActivityResultParse(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void launchEditEmailPhone(boolean isPhone) {
        Intent emailEditIntent = new Intent(this, EditEmailPhoneActivity.class);
        emailEditIntent.putExtra("isPhoneEdit", isPhone);
        startActivity(emailEditIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    @Override
    public void launchChangePassword() {
        Intent emailEditIntent = new Intent(this, ChangePasswordActivity.class);
        emailEditIntent.putExtra("isPhoneEdit", false);
        startActivity(emailEditIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    @Override
    public void launchClinicDetails() {
        Intent clinicDetailsIntent = new Intent(this, SelfOwnedClinicActivity.class);
        clinicDetailsIntent.putExtra("isDisplayMode", true);
        clinicDetailsIntent.putExtra("doctorType", String.valueOf(profileData.getDoctorType()));
        if (profileData.getDoctorType().equalsIgnoreCase(String.valueOf(PROVIDER_UNDER_HOSPITAL))) {
            clinicDetailsIntent.putExtra("clinicHospitalName", profileData.getHospitalName());
        } else {
            clinicDetailsIntent.putExtra("clinicHospitalName", profileData.getClinicName());
            clinicDetailsIntent.putExtra("clinicImages", profileData.getClinicLogoWeb());
        }
        clinicDetailsIntent.putExtra("address", profileData.getAddress());
        clinicDetailsIntent.putExtra("logo", profileData.getClinicLogoApp());
        startActivity(clinicDetailsIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    @Override
    public void launchEducationDetails() {
        Intent intentDegree = new Intent(this, EducationDetailsActivity.class);
        intentDegree.putExtra(IS_DISPLAY, true);
        intentDegree.putExtra(DATA, new Gson().toJson(profileData.getDegree()));
        startActivity(intentDegree, ActivityOptions
                .makeSceneTransitionAnimation(this).toBundle());
    }

    @Override
    public void notifyAdapter() {
        profileAdditionalDetailsAdapter.notifyDataSetChanged();
    }

    @Override
    public void notifyAdapter(int position) {
        if (position != -1 && position <= profileAdditionalDetailsAdapter.getItemCount()) {
            profileAdditionalDetailsAdapter.notifyItemChanged(position);
        }
    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboad(this);
        Utility.progressDialogCancel(this, progressDialog);
        finish();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    @Override
    public void onSuccessProfile(ProfileData profileData) {
        this.profileData = profileData;
        updateUI();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }


    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clParent, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void openGallery() {
        String chooseTitle;
        Intent intent = new Intent();
        intent.setType("image/*");
        chooseTitle = "select Image";
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, chooseTitle), VariableConstant.REQUEST_CODE_GALLERY);
    }

    @Override
    public void openCamera(Uri uri) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_TAKE_PICTURE);
    }

    @Override
    public void startCropImage(String recentTemp) {
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, recentTemp);
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        intent.putExtra("locale", Locale.US.getDisplayLanguage());
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_CROP_IMAGE);
    }

    @Override
    public void updateProfilePic(String newPath) {
        VariableConstant.IS_PROFILE_PHOTO_UPDATED = true;
        binding.sdvProfile.setImageURI(newPath);
        profileData.setProfilePic(newPath);
    }

    @Override
    public void showProgress() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void hideProgress() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utility.progressDialogCancel(this, progressDialog);
    }
}