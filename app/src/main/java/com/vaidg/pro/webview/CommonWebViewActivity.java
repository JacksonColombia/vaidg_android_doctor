package com.vaidg.pro.webview;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityCommonWebViewBinding;

/**
 * <h1>CommonWebViewActivity</h1>
 * <p>This activity is made to load any URL with specific title in WebView.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 16/06/2020
 **/
public class CommonWebViewActivity extends AppCompatActivity {

    private ActivityCommonWebViewBinding binding;
    private String title, url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCommonWebViewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initViews();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initViews() {
        if (getIntent() != null) {
            title = getIntent().getStringExtra("title");
            url = getIntent().getStringExtra("url");
        } else {
            onBackPressed();
        }

        binding.includeToolbar.tvTitle.setText(title);
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        binding.webView.setHorizontalScrollBarEnabled(false);
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.getSettings().setUseWideViewPort(false);
        binding.webView.getSettings().setDomStorageEnabled(true);

        binding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                binding.progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                binding.progressBar.setVisibility(View.GONE);
            }
        });

        binding.webView.loadUrl(url);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    }

}