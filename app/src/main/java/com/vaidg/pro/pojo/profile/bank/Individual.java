package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Individual implements Serializable {

	@SerializedName("metadata")
	@Expose
	private Metadata metadata;

	@SerializedName("requirements")
	@Expose
	private Requirements requirements;

	@SerializedName("address")
	@Expose
	private Address address;

	@SerializedName("gender")
	@Expose
	private String gender;

	@SerializedName("created")
	@Expose
	private int created;

	@SerializedName("last_name")
	@Expose
	private String lastName;

	@SerializedName("id_number_provided")
	@Expose
	private boolean idNumberProvided;

	@SerializedName("phone")
	@Expose
	private String phone;

	@SerializedName("dob")
	@Expose
	private Dob dob;

	@SerializedName("id")
	@Expose
	private String id;

	@SerializedName("relationship")
	@Expose
	private Relationship relationship;

	@SerializedName("first_name")
	@Expose
	private String firstName;

	@SerializedName("account")
	@Expose
	private String account;

	@SerializedName("email")
	@Expose
	private String email;

	@SerializedName("ssn_last_4_provided")
	@Expose
	private boolean ssnLast4Provided;

	@SerializedName("verification")
	@Expose
	private Verification verification;

	@SerializedName("object")
	@Expose
	private String object;

	public Metadata getMetadata(){
		return metadata;
	}

	public Requirements getRequirements(){
		return requirements;
	}

	public Address getAddress(){
		return address;
	}

	public String getGender(){
		return gender;
	}

	public int getCreated(){
		return created;
	}

	public String getLastName(){
		return lastName;
	}

	public boolean isIdNumberProvided(){
		return idNumberProvided;
	}

	public String getPhone(){
		return phone;
	}

	public Dob getDob(){
		return dob;
	}

	public String getId(){
		return id;
	}

	public Relationship getRelationship(){
		return relationship;
	}

	public String getFirstName(){
		return firstName;
	}

	public String getAccount(){
		return account;
	}

	public String getEmail(){
		return email;
	}

	public boolean isSsnLast4Provided(){
		return ssnLast4Provided;
	}

	public Verification getVerification(){
		return verification;
	}

	public String getObject(){
		return object;
	}

	@Override
	public String toString(){
		return
				"Individual{" +
						"metadata = '" + metadata + '\'' +
						",requirements = '" + requirements + '\'' +
						",address = '" + address + '\'' +
						",gender = '" + gender + '\'' +
						",created = '" + created + '\'' +
						",last_name = '" + lastName + '\'' +
						",id_number_provided = '" + idNumberProvided + '\'' +
						",phone = '" + phone + '\'' +
						",dob = '" + dob + '\'' +
						",id = '" + id + '\'' +
						",relationship = '" + relationship + '\'' +
						",first_name = '" + firstName + '\'' +
						",account = '" + account + '\'' +
						",email = '" + email + '\'' +
						",ssn_last_4_provided = '" + ssnLast4Provided + '\'' +
						",verification = '" + verification + '\'' +
						",object = '" + object + '\'' +
						"}";
	}
}