package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.pojo.notification.NotificationData;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static com.vaidg.pro.utility.VariableConstant.FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_SPACE_DD_MM_YYYY;
import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * Created by murashid on 09-Jun-18.
 */

public class NotificatonAdapter extends RecyclerView.Adapter<NotificatonAdapter.ViewHolder> {

    private Context context;
    private ArrayList<NotificationData> notificationData;
    private Typeface fontRegular;


    public NotificatonAdapter(Context context, ArrayList<NotificationData> notificationData) {
        this.context = context;
        this.notificationData = notificationData;
        fontRegular = Utility.getFontRegular(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_notification_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvNotification.setText(notificationData.get(position).getMsg());
        //   holder.tvTime.setText(getDisplayTime(notificationData.get(position).getTimeStamp(), ""));
        holder.tvTime.setText(Utility.covertTimeToText(notificationData.get(position).getTimeStamp()));
    }

    @Override
    public int getItemCount() {
        return notificationData != null ? notificationData.size() : ZERO;
    }

    private String getDisplayTime(String date, String serverTime) {
        long start = Utility.convertUTCToTimeStamp(date);
        long current = System.currentTimeMillis();
        //long current = Utility.convertUTCToTimeStamp(serverTime);
        long totalSecs = (current - start) / 1000;
        if (totalSecs > 86400) {
            try {
                return Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(date), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_DD_MM_YYYY);
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        } else if (totalSecs < 86400 && totalSecs > 3600) {
            return String.valueOf(TimeUnit.SECONDS.toHours(totalSecs)) + context.getString(R.string.hrs);
        } else if (totalSecs < 3600 && totalSecs > 60) {
            return String.valueOf(TimeUnit.SECONDS.toMinutes(totalSecs)) + context.getString(R.string.mins);
        } else {
            return String.valueOf(totalSecs) + context.getString(R.string.secs);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvNotification, tvTime;

        public ViewHolder(View itemView) {
            super(itemView);

            tvNotification = itemView.findViewById(R.id.tvNotification);
            tvTime = itemView.findViewById(R.id.tvTime);

            tvNotification.setTypeface(fontRegular);
            tvTime.setTypeface(fontRegular);
        }

    }

}
