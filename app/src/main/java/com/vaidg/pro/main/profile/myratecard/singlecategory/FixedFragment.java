package com.vaidg.pro.main.profile.myratecard.singlecategory;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.MyRateCardFixedServiceAdapter;
import com.vaidg.pro.main.profile.myratecard.EditCategoryServicePresenter;
import com.vaidg.pro.pojo.profile.ratecard.CategoryData;
import com.vaidg.pro.pojo.profile.ratecard.CategoryService;
import com.vaidg.pro.pojo.profile.ratecard.SubCategoryRateCard;
import com.vaidg.pro.utility.DialogHelper;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link FixedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FixedFragment extends Fragment implements EditCategoryServicePresenter.View, MyRateCardFixedServiceAdapter.FixedServiceCallBack, CompoundButton.OnCheckedChangeListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "FixedFragment";
    public ArrayList<CategoryService> categoryServices;
    public ArrayList<SubCategoryRateCard> subCategoryRateCards;
    public ArrayList<CategoryService> categoryServicesAll;
    public boolean isUpdated = false;
    private CategoryData categoryData;
    private boolean isPriceSetByProvider = false;
    private String action = "0";
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private EditCategoryServicePresenter presenter;
    private MyRateCardFixedServiceAdapter myRateCardFixedServiceAdapter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FixedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FixedFragment newInstance(CategoryData categoryData, boolean isPriceSetByProvider) {
        FixedFragment fragment = new FixedFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, categoryData);
        args.putBoolean(ARG_PARAM2, isPriceSetByProvider);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryData = (CategoryData) getArguments().getSerializable(ARG_PARAM1);
            isPriceSetByProvider = getArguments().getBoolean(ARG_PARAM2);

            /*if ((CategoryRateByCallType.getTabSelected() instanceof OutCallRateCardFragment)&& (OutCallRateCardFragment.getTabSelected() instanceof FixedFragment)&& isPriceSetByProvider) {
                setHasOptionsMenu(true);
            }*/
        }
    }

    public void setOptionMenu(boolean iscurrenttab) {
        if (iscurrenttab && isPriceSetByProvider) {
            setHasOptionsMenu(true);
        } else {
            setHasOptionsMenu(false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fixed, container, false);
        init(view);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.option_menu_fixed_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        final MenuItem checkBoxMenu = menu.findItem(R.id.menu_select_all);
        CheckBox cbSelectAll = (CheckBox) checkBoxMenu.getActionView();
        cbSelectAll.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (categoryServicesAll.size() > 0) {
            String action = "0";
            if (b) {
                action = "1";
            }
            updateAdapter(action);

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("service", presenter.getServiceIdByComma(categoryServicesAll));
                jsonObject.put("action", action);

                presenter.updateServices(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void init(View view) {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.updating));
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getSessionManager(getActivity());
        presenter = new EditCategoryServicePresenter(this);

        categoryServicesAll = new ArrayList<>();
        categoryServices = categoryData.getService();
        subCategoryRateCards = categoryData.getSubCatArr();

        categoryServicesAll.addAll(categoryServices);
        for (int i = 0; i < categoryData.getSubCatArr().size(); i++) {
            for (int j = 0; j < categoryData.getSubCatArr().get(i).getService().size(); j++) {
                CategoryService categoryService = categoryData.getSubCatArr().get(i).getService().get(j);
                categoryService.setSubCatName(categoryData.getSubCatArr().get(i).getSub_cat_name());
                categoryService.setServiceUnderSubCat(true);
                categoryService.setSubCatPosition(i);
                categoryService.setServicePosition(j);
                if (j == 0) {
                    categoryService.setSubCategoryHead(true);
                }
                categoryServicesAll.add(categoryService);
            }
        }


        RecyclerView fixedService = view.findViewById(R.id.recycler_fixed_category);
        fixedService.setLayoutManager(new LinearLayoutManager(getActivity()));
        myRateCardFixedServiceAdapter = new MyRateCardFixedServiceAdapter(getActivity(), this, categoryServicesAll, isPriceSetByProvider, this);
        fixedService.setAdapter(myRateCardFixedServiceAdapter);
    }

    private void updateAdapter(String action) {
        for (CategoryService service : categoryServicesAll) {
            service.setStatus(action);
        }
        myRateCardFixedServiceAdapter.notifyDataSetChanged();
    }


    @Override
    public void onClickReadMore(int position) {
        DialogHelper.customAlertDialogServiceDetails(getActivity(),
                categoryServicesAll.get(position).getSer_name(),
                categoryServicesAll.get(position).getSer_desc());
    }

    @Override
    public void onServiceSelected(int position, String serviceId, String action) {
        categoryServices.get(position).setStatus(action);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("service", serviceId);
            jsonObject.put("action", action);

            presenter.updateServices(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onServiceSelected(int position, int subCatPosition, String serviceId, String action) {
        subCategoryRateCards.get(subCatPosition).getService().get(position).setStatus(action);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("service", serviceId);
            jsonObject.put("action", action);

            presenter.updateServices(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(getActivity(), progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(getActivity(), progressDialog);
    }

    @Override
    public void invalidPrice() {

    }

    @Override
    public void invalidTime() {

    }

    @Override
    public void invalidDesc() {

    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccess(String msg) {
        isUpdated = true;
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, getActivity());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == VariableConstant.REQUEST_CODE_SERVICE_CHANGED) {
            isUpdated = true;
            CategoryService service = (CategoryService) data.getSerializableExtra("service");
            int position = data.getIntExtra("position", 0);
            boolean isServiceUnderSubCat = data.getBooleanExtra("isServiceUnderSubCat", false);

            if (isServiceUnderSubCat) {
                int servicePosition = data.getIntExtra("servicePosition", 0);
                int subCatPosition = data.getIntExtra("subCatPosition", 0);
                subCategoryRateCards.get(subCatPosition).getService().set(servicePosition, service);
            } else {
                categoryServices.set(position, service);
            }

            categoryServicesAll.set(position, service);
            myRateCardFixedServiceAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onDetach() {
        Utility.progressDialogCancel(getActivity(), progressDialog);
        super.onDetach();
    }
}
