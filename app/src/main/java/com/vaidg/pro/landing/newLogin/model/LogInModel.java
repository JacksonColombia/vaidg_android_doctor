package com.vaidg.pro.landing.newLogin.model;

import com.google.gson.JsonObject;
import com.vaidg.pro.BaseModel;
import com.vaidg.pro.landing.newLogin.LogInPresenter;
import com.vaidg.pro.pojo.login.LogInPojo;
import com.vaidg.pro.utility.ApiOnServer;
import com.vaidg.pro.utility.DeviceUuidFactory;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import static android.os.Build.VERSION.RELEASE;
import static com.vaidg.pro.utility.VariableConstant.APP_VERSION;
import static com.vaidg.pro.utility.VariableConstant.DEVICE_MAKER;
import static com.vaidg.pro.utility.VariableConstant.DEVICE_MODEL;
import static com.vaidg.pro.utility.VariableConstant.DEVICE_TYPE;
import static com.vaidg.pro.utility.VariableConstant.SEVEN;

/**
 * <h1>LogInModel</h1>
 * <p>This model is contain objects that's all used at LogInActivity through LogInPresenter
 *
 * @author 3embed
 * @version 1.0.20
 * @see LogInPresenter
 * @since 08/06/2020
 **/
public class LogInModel extends BaseModel {

    @Inject
    Utility utility;

    @Inject
    DeviceUuidFactory deviceUuidFactory;

    @Inject
    SessionManager sessionManager;

    LogInPojo logInPojo;

    @Inject
    public LogInModel() {

    }

    public boolean isValidPassword(String pass) {
        String regex = "(.)*(\\d)(.)*";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pass);
        if (pass.length() < SEVEN) {
            return false;
        } else if (pass.equals(pass.toLowerCase())) {
            return false;
        } else if (pass.equals(pass.toUpperCase())) {
            return false;
        } else return matcher.matches();
    }

    public JSONObject getLoginPayload(String emailPhone, String password) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ApiOnServer.LogIn.MOBILE_OR_EMAIL, emailPhone);
            jsonObject.put(ApiOnServer.LogIn.MOBILE_OR_EMAIL, emailPhone);
            jsonObject.put(ApiOnServer.LogIn.PASSWORD, password);
            jsonObject.put(ApiOnServer.LogIn.DEVICE_TYPE, DEVICE_TYPE);
            jsonObject.put(ApiOnServer.LogIn.DEVICE_ID, deviceUuidFactory.getDeviceUuid());
            jsonObject.put(ApiOnServer.LogIn.APP_VERSION, APP_VERSION);
            jsonObject.put(ApiOnServer.LogIn.DEVICE_MAKE, DEVICE_MAKER);
            jsonObject.put(ApiOnServer.LogIn.DEVICE_MODEL, DEVICE_MODEL);
            jsonObject.put(ApiOnServer.LogIn.DEVICE_OS_VERSION, RELEASE);
            jsonObject.put(ApiOnServer.LogIn.DEVICE_TIME, Utility.getCurrentTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public void parseLogInData(String response) {
        if (response != null && !response.isEmpty()) {
            LogInPojo logInPojo = utility.getGson().fromJson(response, LogInPojo.class);
            if (logInPojo != null) {
                setLogInPojo(logInPojo);
            }
        }
    }

    public LogInPojo getLogInPojo() {
        return logInPojo;
    }

    public void setLogInPojo(LogInPojo logInPojo) {
        this.logInPojo = logInPojo;
    }
}
