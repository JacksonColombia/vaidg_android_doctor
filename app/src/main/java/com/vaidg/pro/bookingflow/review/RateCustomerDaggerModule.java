package com.vaidg.pro.bookingflow.review;

import android.app.Activity;
import com.vaidg.pro.bookingflow.medication.MedicationActivity;
import com.vaidg.pro.bookingflow.medication.MedicationContract;
import com.vaidg.pro.bookingflow.medication.MedicationPresenterImple;
import com.vaidg.pro.dagger.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>LogInDaggerModule</h1>
 * <p>This dagger module created for LogInActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
@Module
public abstract class RateCustomerDaggerModule {
  @ActivityScoped
  @Binds
  abstract Activity getActivity(RateCustomerActivity activity);

  @ActivityScoped
  @Binds
  abstract RateCustomerContract.View getView(RateCustomerActivity activity);

  @ActivityScoped
  @Binds
  abstract RateCustomerContract.Presenter getPresenter(RateCustomerPresenter presenterImple);
}
