package com.vaidg.pro.main.chats;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.shedule.ScheduleViewPojo;
import com.vaidg.pro.utility.OkHttp3ConnectionStatusCode;
import com.vaidg.pro.utility.RefreshToken;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.ServiceUrl;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 09-Apr-18.
 */

public class ChatCutomerPresenter implements ChatCustomerContract.Presenter {
    private static final String TAG = "ChatCutomer";

    private ChatCustomerContract.View view;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;
    @Inject
    Context context;

    @Inject
    ChatCutomerPresenter() {
    }

    /**
     * <h2>getWalletDetails</h2>
     * <p>
     * api call to get wallet setting details
     * method for calling api for geting the Category details
     * </p>
     */
    @Override
    public void getCustomerList(final boolean isBackground) {
        if (!isBackground) {
            if(view != null)
            view.startProgressBar();
        }
        service.getChatBooking(Utility.getAuth(sessionManager.getEmail()),DEFAULT_LANGUAGE, PLATFORM)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if(view != null)
                                view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    if(view != null)
                                        view.onSuccess(response, true);
                                } else {
                                    if(view != null)
                                        view.onFailure();
                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        if(view != null)
                                            view.stopProgressBar();
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        getCustomerList(isBackground);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        view.onFailure();
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        getInstance().toast(Utility.getMessage(msg));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                                if(view != null)
                                    view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                Utility.logoutSessionExiperd(sessionManager, context);
                                break;
                            default:
                                if(view != null)
                                    view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                view.onFailure(Utility.getMessage(errorBody));
                                break;
                        }
                    } catch (Exception e) {
                        if(view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if(view != null) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                }

                @Override
                public void onComplete() {
                }
            });

    }

    @Override
    public void attachView(ChatCustomerContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    interface View {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String failureMsg);

        void onFailure();

        void onSuccess(String chatPojo, boolean isFromApi);

        void onNewToken(String newToken);

        void sessionExpired(String msg);
    }
}
