package com.vaidg.pro.main.notification;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.notification.NotificationPojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;
import okhttp3.ResponseBody;

import retrofit2.Response;

/**
 * Created by murashid on 09-Apr-18.
 */

public class NotificationPresenter implements NotificationContract.Presenter{
    private static final String TAG = "ChatCutomer";
    @Inject
    NotificationContract.View view;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;
    @Inject
    NetworkService service;

    @Inject
    NotificationPresenter() {
    }

    @Override
    public void getNotificationList(int skip, int limit){
        if(view != null)
        view.startProgressBar();
        service.getNotification(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, limit, skip)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if (response != null && !response.isEmpty()) {
                                    if(view != null)
                                    view.stopProgressBar();
                                    NotificationPojo notificationPojo = gson.fromJson(response, NotificationPojo.class);
                                    if(view != null)
                                    view.onSuccess(notificationPojo.getData());
                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(Utility.getAuth(sessionManager.getEmail()),  sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        if(view != null)
                                            view.stopProgressBar();
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        getNotificationList(skip, limit);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        if(view != null) {
                                            view.stopProgressBar();
                                            view.onFailure();
                                        }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if (view != null) {
                                            view.stopProgressBar();
                                            view.sessionExpired(Utility.getMessage(msg));
                                        }
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                                if(view != null) {
                                    view.stopProgressBar();
                                    view.sessionExpired(Utility.getMessage(errorBody));
                                }
                                break;
                            default:
                                if(view != null) {
                                    view.hideProgress();
                                    view.onFailure(Utility.getMessage(errorBody));
                                }
                                break;
                        }
                    } catch (Exception e) {
                        if(view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if(view != null) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                }

                @Override
                public void onComplete() {
                }
            });

    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }
}
