package com.vaidg.pro.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.databinding.SingleRowSupportTextBinding;
import com.vaidg.pro.main.profile.support.SupportSubCategoryActivity;
import com.vaidg.pro.pojo.profile.support.SupportData;
import com.vaidg.pro.utility.WebViewActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>SupportListAdapter</h1>
 * Support Recycler view adapter for displaying the list of supports in  SupportActivity and SupportSubCategoryActivity
 *
 * @see com.vaidg.pro.main.profile.support.SupportActivity
 * @see com.vaidg.pro.main.profile.support.SupportSubCategoryActivity
 */

public class SupportListAdapter extends RecyclerView.Adapter<SupportListAdapter.SupportDetailViewHolder> implements View.OnClickListener {

    private ArrayList<SupportData> supportDataList;
    private boolean isMainCategory;
    private Activity mActivity;

    public SupportListAdapter(Activity mActivity, ArrayList<SupportData> supportDatas, boolean isMainCategory) {
        this.mActivity = mActivity;
        this.supportDataList = supportDatas;
        this.isMainCategory = isMainCategory;
    }

    @NotNull
    @Override
    public SupportDetailViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        SingleRowSupportTextBinding binding = SingleRowSupportTextBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new SupportDetailViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(SupportDetailViewHolder holder, int position) {
        holder.binding.tvName.setText(supportDataList.get(position).getName());
        holder.binding.tvName.setOnClickListener(this);
        holder.binding.tvName.setTag(holder);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvName) {
            int adapterPosition = ((SupportDetailViewHolder) v.getTag()).getAdapterPosition();
            if (isMainCategory) {
                if (supportDataList.get(adapterPosition).getSubcat().size() > 0) {
                    Intent supportSubCatIntent = new Intent(mActivity, SupportSubCategoryActivity.class);
                    supportSubCatIntent.putExtra("data", supportDataList.get(adapterPosition).getSubcat());
                    supportSubCatIntent.putExtra("title", supportDataList.get(adapterPosition).getName());
                    mActivity.startActivity(supportSubCatIntent);
                    mActivity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                } else {
                    Intent webIntent = new Intent(mActivity, WebViewActivity.class);
                    webIntent.putExtra("URL", supportDataList.get(adapterPosition).getLink());
                    webIntent.putExtra("title", supportDataList.get(adapterPosition).getName());
                    webIntent.putExtra("isFromSupport", true);
                    mActivity.startActivity(webIntent);
                    mActivity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
            } else {
                Intent webIntent = new Intent(mActivity, WebViewActivity.class);
                webIntent.putExtra("URL", supportDataList.get(adapterPosition).getLink());
                webIntent.putExtra("title", supportDataList.get(adapterPosition).getName());
                webIntent.putExtra("isFromSupport", true);
                mActivity.startActivity(webIntent);
                mActivity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
            }
        }
    }

    @Override
    public int getItemCount() {
        return supportDataList == null ? 0 : supportDataList.size();
    }

    static class SupportDetailViewHolder extends RecyclerView.ViewHolder {

        SingleRowSupportTextBinding binding;

        public SupportDetailViewHolder(SingleRowSupportTextBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }
    }


}
