package com.vaidg.pro.address;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.button.MaterialButton;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.landing.signup.SignupActivity;
import com.vaidg.pro.utility.location.AcessLocation;
import com.vaidg.pro.utility.LocationUtil;
import com.vaidg.pro.utility.location.NetworkNotifier;
import com.vaidg.pro.utility.location.NetworkUtil;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by murashid on 29-Sep-17.
 * <h1>AddressSaveActivity</h1>
 * AddressSaveActivity for showing map for pick up the address
 */

public class AddressSaveActivity extends AppCompatActivity implements View.OnClickListener, AddressSavePresenter.AddressSavePresenterImple, OnMapReadyCallback, CompoundButton.OnCheckedChangeListener, LocationUtil.GetLocationListener, NetworkNotifier {

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 99;
    private final String TAG = AddressSaveActivity.this.getClass().getSimpleName();
    private SessionManager sessionManager;
    private ProgressDialog progressDialog;
    private AddressSavePresenter presenter;
    private EditText etAddressHeader;
    private TextView tvAddress;
    private String addressId = "", placeId = "", latitude, longitude, taggedAs = "others";
    private String city, state, pincode;
    private boolean isEdit = false, isSignup = false;
    private GoogleMap googleMap;
    private View mapView;
    private AlertDialog dialogGps;
    private LocationUtil locationUtil;
    private boolean isCameraMoved = false;

    private int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private NetworkUtil networkUtilObj;

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utility.checkAndShowNetworkError(this);
/*
        if (hasFocus) {
            if (Utility.isGpsEnabled(this)) {
                if (dialogGps != null && dialogGps.isShowing()) {
                    dialogGps.dismiss();
                }

                if (locationUtil != null) {
                    if (locationUtil.isGoogleApiClientConnected()) {
                        locationUtil.startLocationUpdates();
                    } else {
                        locationUtil.connectGoogleApiClient();
                    }
                }
            }
        }
*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_save);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            networkUtilObj = new NetworkUtil(this, this);

            networkUtilObj.connectGoogleApiClient();
        } else {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                networkUtilObj = new NetworkUtil(this, this);

                networkUtilObj.connectGoogleApiClient();
            } else {
                int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 2;
                AcessLocation.getLocation(AddressSaveActivity.this, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
        }
        init();
    }

    /**
     * init the views
     */
    private void init() {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.saving));
        progressDialog.setCancelable(false);
        presenter = new AddressSavePresenter(this, this);

        //  locationUtil = new LocationUtil(this, this);
        // dialogGps = DialogHelper.adEnableGPS(this);

        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        fm.getMapAsync(this);
        mapView = fm.getView();


        // Initialize Places.
//        Places.initialize(getApplicationContext(), "AIzaSyBALyIFVmPCuZs4QAbYQYemU3nSMb12ueg");//api key dont use REQUEST_DENIED
        Places.initialize(getApplicationContext(), VariableConstant.GOOGLE_API_KEY);//server key
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);

// Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);


        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                LatLng MOUNTAIN_VIEW = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(MOUNTAIN_VIEW)
                        .zoom(17)
                        .tilt(30)
                        .build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                tvAddress.setText(place.getAddress());
                etAddressHeader.setText(place.getName());
                latitude = String.valueOf(place.getLatLng().latitude);
                longitude = String.valueOf(place.getLatLng().longitude);
                placeId = place.getId();
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });

        isSignup = getIntent().getBooleanExtra("isSignup", false);
/*
        Intent intent = getIntent();
        placeId = intent.getStringExtra("placeId");
        name = intent.getStringExtra("name");
        address = intent.getStringExtra("address");
        latitude = intent.getStringExtra("latitude");
        longitude = intent.getStringExtra("longitude");
        isEdit = intent.getBooleanExtra("isEdit", false);
        taggedAs = intent.getStringExtra("taggedAs");*/


        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontLight = Utility.getFontLight(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.location));
        tvTitle.setTypeface(fontBold);

        MaterialButton tvDone = findViewById(R.id.btnDone);
        tvDone.setText(getString(R.string.save));
        tvDone.setTypeface(fontBold);
        tvDone.setOnClickListener(this);

        RadioButton rbHome = findViewById(R.id.rbHome);
        rbHome.setTypeface(fontLight);
        rbHome.setOnCheckedChangeListener(this);
        RadioButton rbOffice = findViewById(R.id.rbOffice);
        rbOffice.setTypeface(fontLight);
        rbOffice.setOnCheckedChangeListener(this);
        RadioButton rbOthers = findViewById(R.id.rbOthers);
        rbOthers.setTypeface(fontLight);
        rbOthers.setOnCheckedChangeListener(this);

        etAddressHeader = findViewById(R.id.etAddressHeader);
        etAddressHeader.setTypeface(fontRegular);

        tvAddress = findViewById(R.id.tvAddress);
        tvAddress.setTypeface(fontRegular);
        tvAddress.setOnClickListener(this);

     //   presenter.getAddress(sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
    }

    @Override
    public void onMapReady(GoogleMap Map) {
        googleMap = Map;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(false);
        latitude = String.valueOf(googleMap.getCameraPosition().target.latitude);
        longitude = String.valueOf(googleMap.getCameraPosition().target.longitude);
        LatLng latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).tilt(30).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                //apiCallerForAddress.getAddress(googleMap.getCameraPosition().target.latitude, googleMap.getCameraPosition().target.longitude, AddressSaveActivity.this);

                /*latitude = String.valueOf(googleMap.getCameraPosition().target.latitude);
                longitude = String.valueOf(googleMap.getCameraPosition().target.longitude);*/
                presenter.getAddress(String.valueOf(googleMap.getCameraPosition().target.latitude), String.valueOf(googleMap.getCameraPosition().target.longitude));

            }
        });

        try {
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            // position on right bottom
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            rlp.setMargins(0, 0, 30, 30);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // locationUtil.connectGoogleApiClient();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // locationUtil.disconnectGoogleApiClient();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnDone:
                if (isSignup) {
                    taggedAs = etAddressHeader.getText().toString();
                    if (tvAddress.getText().toString().equals("")) {
                        onEmptyAddressLine();
                        return;
                    } else if (taggedAs.equals("")) {
                        onEmptyTagged();
                        return;
                    }
                    Utility.hideKeyboad(this);
                    Intent intent = new Intent(this, SignupActivity.class);
                    intent.putExtra("address", tvAddress.getText().toString());
                    intent.putExtra("taggedAs", taggedAs);
                    intent.putExtra("latitude", latitude);
                    intent.putExtra("longitude", longitude);
                    intent.putExtra("city", city);
                    intent.putExtra("state", state);
                    intent.putExtra("pincode", pincode);

                    setResult(RESULT_OK, intent);
                    onBackPressed();
                    return;
                }
                try {

                    taggedAs = etAddressHeader.getText().toString();
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("addLine1", tvAddress.getText().toString());
                    //jsonObject.put("addLine2","");
                    jsonObject.put("placeId", placeId);
                    jsonObject.put("latitude", latitude);
                    jsonObject.put("longitude", longitude);
                    jsonObject.put("taggedAs", taggedAs);
                    jsonObject.put("userType", VariableConstant.USER_TYPE);

                    presenter.saveEditAddress(isEdit, addressId, AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.tvAddress:
                findPlace();
                break;
        }
    }

    private void findPlace() {
       /* try {
            LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds
                    (new LatLng(Double.parseDouble(sessionManager.getCurrentLat()),Double.parseDouble(sessionManager.getCurrentLng())),
                            new LatLng(Double.parseDouble(sessionManager.getCurrentLat()),Double.parseDouble(sessionManager.getCurrentLng())));

            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                    .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        }catch (Exception e) {
            e.printStackTrace();
            // TODO: Handle the error.

        }*/
    }


    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        VariableConstant.IS_ADDRESS_LIST_CHANGED = true;
        onBackPressed();
    }

    @Override
    public void onEmptyTagged() {
        Toast.makeText(this, getString(R.string.plsEnterAddressName), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEmptyAddressLine() {
        Toast.makeText(this, getString(R.string.fetchingAddress), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailureFetchingAddresss() {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            switch (buttonView.getId()) {
                case R.id.rbHome:
                    etAddressHeader.setVisibility(View.GONE);
                    etAddressHeader.setText(R.string.home);
                    break;

                case R.id.rbOffice:
                    etAddressHeader.setVisibility(View.GONE);
                    etAddressHeader.setText(R.string.office);
                    break;

                case R.id.rbOthers:
                    etAddressHeader.setVisibility(View.VISIBLE);
                    etAddressHeader.setText("");
                    break;
            }
        }
    }

    @Override
    public void onAddress(final LocationPlaces places) {
        tvAddress.setText(places.getAddress());
        latitude = String.valueOf(places.getLatitude());
        longitude = String.valueOf(places.getLongitude());
        placeId = places.getPlace_id();

        city = places.getCity();
        state = places.getState();
        pincode = places.getPincode();

        Log.d("Addressss", "onAddress: " + city + "/n" + state + "/n" + pincode);

    }

    @Override
    public void onAddress(String address) {
        tvAddress.setText(address);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*if(requestCode==PLACE_AUTOCOMPLETE_REQUEST_CODE && resultCode == RESULT_OK)
        {
            Place place = PlaceAutocomplete.getPlace(this, data);
            LatLng MOUNTAIN_VIEW = new LatLng(place.getLatLng().latitude,place.getLatLng().longitude);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(MOUNTAIN_VIEW)
                    .zoom(17)
                    .tilt(30)
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            tvAddress.setText(place.getAddress());
            etAddressHeader.setText(place.getName());
            latitude = String.valueOf(place.getLatLng().latitude);
            longitude = String.valueOf(place.getLatLng().longitude);
            placeId = place.getId();
        }
*/
        if (requestCode == VariableConstant.REQUEST_CODE_GPS) {
            if (resultCode == RESULT_OK) {
               /* if (dialogGps.isShowing()) {
                    dialogGps.dismiss();
                }
                if (locationUtil.isGoogleApiClientConnected()) {
                    locationUtil.startLocationUpdates();
                } else {
                    locationUtil.connectGoogleApiClient();
                }*/
            } else if (resultCode == RESULT_CANCELED) {
               /* if (!dialogGps.isShowing()) {
                    dialogGps.show();
                }*/
            }
        }
    }


    @Override
    public void updateLocation(Location location) {
        if (googleMap != null && !isCameraMoved) {
            isCameraMoved = true;

            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).tilt(30).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            latitude = String.valueOf(latLng.latitude);
            longitude = String.valueOf(latLng.longitude);
        }
    }

    @Override
    public void location_Error(String error) {

    }

    @Override
    public void locationUpdates(Location location) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        }
        googleMap.getUiSettings().setCompassEnabled(false);
        latitude = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());
        LatLng latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).tilt(30).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                //apiCallerForAddress.getAddress(googleMap.getCameraPosition().target.latitude, googleMap.getCameraPosition().target.longitude, AddressSaveActivity.this);

                /*latitude = String.valueOf(googleMap.getCameraPosition().target.latitude);
                longitude = String.valueOf(googleMap.getCameraPosition().target.longitude);*/
                presenter.getAddress(String.valueOf(googleMap.getCameraPosition().target.latitude), String.valueOf(googleMap.getCameraPosition().target.longitude));

            }
        });

        try {
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            // position on right bottom
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            rlp.setMargins(0, 0, 30, 30);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(networkUtilObj != null)
            networkUtilObj.disconnectGoogleApiClient();
        /*latitude = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());
        //  Toast.makeText(AddressSaveActivity.this, "Latitude: " + latitude + "" + " Longitude: " + longitude + "", Toast.LENGTH_LONG).show();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(false);
        LatLng latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).tilt(30).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        presenter.getAddress(latitude,longitude);*/

    }


    @Override
    public void locationFailed(String message)
    {

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(networkUtilObj != null)
        {
            networkUtilObj.disconnectGoogleApiClient();
        }
    }



    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults)
    {
        switch (requestCode)
        {
            case 2:
            {
                Map<String, Integer> perms = new HashMap<>();

                perms.put(android.Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);

                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (perms.get(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                {
                    networkUtilObj = new NetworkUtil(this, this);

                    networkUtilObj.connectGoogleApiClient();
                }

                if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                {
                    boolean should = ActivityCompat.shouldShowRequestPermissionRationale(AddressSaveActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION);
                    if (should)
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AddressSaveActivity.this);
                        builder.setTitle(R.string.location_access_denied);
                        builder.setMessage(R.string.location_denied_subtitle);
                        builder.setPositiveButton(R.string.i_am_sure, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                dialog.dismiss();
                            }
                        });
                        builder.setNegativeButton(R.string.re_try, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                dialog.dismiss();
                                ActivityCompat.requestPermissions(AddressSaveActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                            }
                        });
                        builder.show();

                    }
                }

                break;
            }

            case 1:
            {
                Map<String, Integer> perms = new HashMap<>();

                perms.put(android.Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);

                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (perms.get(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                {
                    networkUtilObj = new NetworkUtil(this, this);

                    networkUtilObj.connectGoogleApiClient();
                }

                break;
            }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }
}
