package com.vaidg.pro.pojo.profile.wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @since 19/09/17.
 */

public class WalletTransPojo implements Serializable {
        /*"errNum":200,
    "errMsg":"Got The Details",
    "errFlag":0,
    "data":{
    "debitArr":[],
    "creditArr":[],
    "creditDebitArr":[*/

    @SerializedName("errNum")
    @Expose
    private int errNum;
    @SerializedName("errFlag")
    @Expose
    private int errFlag;
    @SerializedName("errMsg")
    @Expose
    private String errMsg;
    @SerializedName("data")
    @Expose
    private WalletTransData data;

    public int getErrNum() {
        return errNum;
    }

    public void setErrNum(int errNum) {
        this.errNum = errNum;
    }

    public int getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(int errFlag) {
        this.errFlag = errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public WalletTransData getData() {
        return data;
    }

    public void setData(WalletTransData data) {
        this.data = data;
    }


    @Override
    public String toString() {
        return "WalletTransPojo{" +
                "errNum=" + errNum +
                ", errFlag=" + errFlag +
                ", errMsg='" + errMsg + '\'' +
                ", data=" + data +
                '}';
    }
}
