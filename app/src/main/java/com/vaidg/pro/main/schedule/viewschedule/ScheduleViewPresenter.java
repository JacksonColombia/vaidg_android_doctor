package com.vaidg.pro.main.schedule.viewschedule;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.app.Activity;
import android.content.Context;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.shedule.ScheduleData;
import com.vaidg.pro.pojo.shedule.ScheduleViewPojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 04-Oct-17.
 * <h1>ScheduleViewPresenter</h1>
 * ScheduleViewPresenter presenter for ScheduleViewActivity
 *
 * @see ScheduleViewActivity
 */

public class ScheduleViewPresenter implements ScheculeViewContract.Presenter {

    @Inject
    Context context;
    @Inject
    ScheculeViewContract.View view;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;

    @Inject
    public ScheduleViewPresenter() {
    }



    /**
     * method for passing value from view to model
     *
     */
    @Override
    public void getShedule() {
        view.startProgressBar();
        //model.getShedule(sessionToken);
        service.getSchedule(Utility.getAuth(sessionManager.getEmail()),DEFAULT_LANGUAGE, PLATFORM)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                               view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    ScheduleViewPojo scheduleViewPojo = gson.fromJson(response, ScheduleViewPojo.class);
                                    view.onSuccess(scheduleViewPojo.getData());
                                } else {
                                    view.onFailure();
                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                       view.stopProgressBar();
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        getShedule();
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        view.onFailure();
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        getInstance().toast(Utility.getMessage(msg));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                                view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                Utility.logoutSessionExiperd(sessionManager, context);
                                break;
                            default:
                                view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                view.onFailure(Utility.getMessage(errorBody));
                                break;
                        }
                    } catch (Exception e) {
                       view.stopProgressBar();
                        view.onFailure();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                   view.stopProgressBar();
                    view.onFailure();
                }

                @Override
                public void onComplete() {
                }
            });

    }


    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    /**
     * ScheduleViewview interface for view implementation
     */
    interface ScheduleViewview {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String msg);

        void onFailure();

        void onSuccess(ScheduleData data);

        void onSuccessDeleteSchedule(String msg);
    }

}
