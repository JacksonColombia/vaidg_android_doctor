package com.vaidg.pro.pojo.profile;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DegreeDetailsData implements Parcelable
{

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("college")
    @Expose
    public String college;
    @SerializedName("year")
    @Expose
    public String year;
    @SerializedName("id")
    @Expose
    public String id;

    private boolean isEdited;
    public final static Parcelable.Creator<DegreeDetailsData> CREATOR = new Creator<DegreeDetailsData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DegreeDetailsData createFromParcel(Parcel in) {
            return new DegreeDetailsData(in);
        }

        public DegreeDetailsData[] newArray(int size) {
            return (new DegreeDetailsData[size]);
        }

    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isEdited() {
        return isEdited;
    }

    public void setEdited(boolean edited) {
        isEdited = edited;
    }

    protected DegreeDetailsData(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.college = ((String) in.readValue((String.class.getClassLoader())));
        this.year = ((String) in.readValue((String.class.getClassLoader())));
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.isEdited = ((boolean) in.readValue((boolean.class.getClassLoader())));
    }

    public DegreeDetailsData() {
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeValue(college);
        dest.writeValue(year);
        dest.writeValue(id);
        dest.writeValue(isEdited);
    }

    public int describeContents() {
        return 0;
    }

}

