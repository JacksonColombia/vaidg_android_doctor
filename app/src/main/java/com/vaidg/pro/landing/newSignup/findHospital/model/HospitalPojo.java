package com.vaidg.pro.landing.newSignup.findHospital.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HospitalPojo {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<HospitalDataPojo> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<HospitalDataPojo> getData() {
        return data;
    }

    public void setData(List<HospitalDataPojo> data) {
        this.data = data;
    }
}
