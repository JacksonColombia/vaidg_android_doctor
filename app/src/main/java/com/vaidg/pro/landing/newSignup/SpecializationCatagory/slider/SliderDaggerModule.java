package com.vaidg.pro.landing.newSignup.SpecializationCatagory.slider;

import com.vaidg.pro.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>SliderDaggerModule</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
@Module
public interface SliderDaggerModule {

    @FragmentScoped
    @Binds
    SliderContract.Presenter providePresenter(SliderPresenter presenter);
}
