package com.vaidg.pro.adapters;

import static com.vaidg.pro.utility.Utility.getFontBold;
import static com.vaidg.pro.utility.Utility.getFontMedium;
import static com.vaidg.pro.utility.Utility.getFontRegular;
import static com.vaidg.pro.utility.Utility.pixelsToSp;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.pro.R;
import com.vaidg.pro.pojo.medication.MedicationData;

import java.util.ArrayList;

/**
 * Created by murashid on 29-Sep-17.
 * <h1>AddressListAdapter</h1>
 * Recycler View Adapter for Address List for selecting address during schedule
 *
 * @see com.vaidg.pro.address.AddressListActivity
 */
public class MedicationListAdapter extends RecyclerView.Adapter<MedicationListAdapter.ViewHolder>{
  private Context mContext;
  private ArrayList<MedicationData> mMedicationData = new ArrayList<>();
  private OnItemClickListener listener;

  public MedicationListAdapter(ArrayList<MedicationData> medicationData, OnItemClickListener listener) {
    this.mMedicationData = medicationData;
    this.listener = listener;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_medication, parent, false);
    mContext = parent.getContext();
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    holder.tvCompoundTitle.setText(String.format("%02d", position+1)+"."+" "+"Compound name");
    holder.tvCompoundName.setText(mMedicationData.get(position).getCompoundName());
    holder.tvDrugStrengthDesc.setText(mMedicationData.get(position).getDrugStrength()+" "+mMedicationData.get(position).getDrugUnit());
    holder.tvFrequencyDesc.setText(mMedicationData.get(position).getFrequency());

    holder.ivEdit.setOnClickListener(v -> {
        if(listener != null)
          listener.onEditClick(mMedicationData.get(position).getId());
    });
    holder.ivDelete.setOnClickListener(v -> {
        if(listener != null)
          listener.onDeleteClick(mMedicationData.get(position).getId());
    });
  }

  public interface OnItemClickListener {
    void onEditClick(String id);
    void onDeleteClick(String id);
  }


  @Override
  public int getItemCount() {
     return mMedicationData == null ? 0 : mMedicationData.size();
  }


  class ViewHolder extends RecyclerView.ViewHolder {
    ImageView ivHomeOffice;
    TextView tvHome;
    @BindView(R.id.tvCompoundTitle)
    TextView tvCompoundTitle;
    @BindView(R.id.tvCompoundName)
    TextView tvCompoundName;
    @BindView(R.id.tvDrugStrength)
    TextView tvDrugStrength;
    @BindView(R.id.tvDrugStrengthDesc)
    TextView tvDrugStrengthDesc;
    @BindView(R.id.tvFrequency)
    TextView tvFrequency;
    @BindView(R.id.tvFrequencyDesc)
    TextView tvFrequencyDesc;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;
    @BindView(R.id.ivDelete)
    ImageView ivDelete;
    private Typeface fontRegular, fontMedium,fontBold;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      fontMedium = getFontMedium(mContext);
      fontRegular = getFontRegular(mContext);
      fontBold = getFontBold(mContext);
      tvCompoundName.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_18),mContext));
      tvCompoundTitle.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14),mContext));
      tvDrugStrength.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16),mContext));
      tvDrugStrengthDesc.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16),mContext));
      tvFrequency.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16),mContext));
      tvFrequencyDesc.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16),mContext));
      tvCompoundName.setTypeface(fontBold);
      tvCompoundTitle.setTypeface(fontMedium);
      tvDrugStrength.setTypeface(fontRegular);
      tvDrugStrengthDesc.setTypeface(fontMedium);
      tvFrequency.setTypeface(fontRegular);
      tvFrequencyDesc.setTypeface(fontMedium);
    }
  }
}
