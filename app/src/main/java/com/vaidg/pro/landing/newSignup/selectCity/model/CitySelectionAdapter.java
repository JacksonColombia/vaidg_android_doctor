package com.vaidg.pro.landing.newSignup.selectCity.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.databinding.ItemCitySelectionBinding;
import com.vaidg.pro.pojo.signup.CityData;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * <h1>CitySelectionAdapter</h1>
 * <p>This adapter is used to load list of city with select on tap</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @see com.vaidg.pro.landing.newSignup.selectCity.SelectCityActivity
 * @see com.vaidg.pro.landing.newSignup.selectCity.SelectCityUtil
 * @since 13/06/2020
 **/
public class CitySelectionAdapter extends RecyclerView.Adapter<CitySelectionAdapter.CitySelectionViewHolder> {

    private ArrayList<CityData> cityData;
    private CitySelectionListener callBack;
    private Context context;

    public CitySelectionAdapter(ArrayList<CityData> cityData) {
        this.cityData = cityData;
    }

    public void provideCallBack(CitySelectionListener callBack) {
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public CitySelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        ItemCitySelectionBinding binding = ItemCitySelectionBinding.inflate(LayoutInflater.from(context), parent, false);
        return new CitySelectionViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CitySelectionViewHolder holder, int position) {
        holder.binding.tvCityName.setText(cityData.get(position).getCity());
    }

    @Override
    public int getItemCount() {
        return  cityData != null ? cityData.size() : ZERO;
    }

    class CitySelectionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemCitySelectionBinding binding;

        CitySelectionViewHolder(@NonNull ItemCitySelectionBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
            itemView.tvCityName.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (callBack != null)
                callBack.onItemSelected(getAdapterPosition(),cityData.get(getAdapterPosition()).getCity());
        }
    }


    // method for filtering our recyclerview items.
    public void filterList(ArrayList<CityData> filterllist) {
        // below line is to add our filtered
        // list in our course array list.
        cityData = filterllist;
        // below line is to notify our adapter
        // as change in recycler view data.
        notifyDataSetChanged();
    }
}
