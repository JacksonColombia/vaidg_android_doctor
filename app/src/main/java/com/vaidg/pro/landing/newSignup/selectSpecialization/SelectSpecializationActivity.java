package com.vaidg.pro.landing.newSignup.selectSpecialization;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.vaidg.pro.R;
import com.vaidg.pro.landing.newSignup.selectSpecialization.model.SelectSpecializationModel;
import com.vaidg.pro.landing.newSignup.selectSpecialization.model.SpecializationSelectionAdapter;
import com.vaidg.pro.databinding.ActivitySelectCityBinding;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.SpecializationCategoryActivity;
import com.vaidg.pro.landing.newSignup.selectCity.model.CitySelectionListener;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.pojo.signup.CityData;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * <h1>@SelectSpecializationActivity</h1>
 * <p>  SelectSpecializationActivity activity for selection of Specialization for doctor</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public class SelectSpecializationActivity extends BaseDaggerActivity implements SelectSpecializationContract.View {

    @Inject
    SpecializationSelectionAdapter specializationSelectionAdapter;

    @Inject
    SelectSpecializationContract.Presenter presenter;
    @Inject
    SelectSpecializationModel model;
    SelectSpecializationData specializationData;
    private ActivitySelectCityBinding binding;
    private String cityId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySelectCityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        init();
        getData();
        presenter.getSpecializationCategory(cityId);
        initRv();
        initOnclickListener();
    }

    private void getData() {
        cityId = getIntent().getStringExtra("cityId");
    }

    private void initOnclickListener() {
        binding.includeToolbar.ivBackButton.setOnClickListener(this::onClick);
        binding.includeToolbar.btnDone.setOnClickListener(this::onClick);
    }

    private void init() {
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("");
        binding.includeToolbar.tvTitle.setText(getString(R.string.select_specialization));
        binding.includeToolbar.btnDone.setText(getString(R.string.next));
        binding.includeToolbar.ivBackButton.setVisibility(View.VISIBLE);
        specializationSelectionAdapter.provideCallBack((CitySelectionListener) presenter);
    }


    private void initRv() {
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        itemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_light, null));
        binding.rvSelectCity.addItemDecoration(itemDecoration);
        binding.rvSelectCity.setAdapter(specializationSelectionAdapter);
        binding.svCity.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.trim().length() > 0)
                    filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.trim().length() > 0)
                    filter(newText);
                return true;
            }
        });

        binding.svCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.svCity.setIconified(false);
            }
        });

        binding.svCity.postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.svCity.setIconified(false);
                binding.svCity.clearFocus();
            }
        }, 300);
    }

    private void filter(String text) {
        // creating a new array list to filter our data.
        ArrayList<SelectSpecializationData> filteredlist = new ArrayList<>();

        // running a for loop to compare elements.
        for (SelectSpecializationData item : model.getSelectSpecializationData()) {
            // checking if the entered string matched with any item of our recycler view.
            if (item.getCatName().toLowerCase().contains(text.toLowerCase())) {
                // if the item is matched we are
                // adding it to our filtered list.
                filteredlist.add(item);
            }
        }
        if (filteredlist.isEmpty()) {
            // if no item is added in filtered list we are
            // displaying a toast message as no data found.
            binding.rvSelectCity.setVisibility(View.INVISIBLE);
            Toast.makeText(this, "No Data Found..", Toast.LENGTH_SHORT).show();
        } else {
            binding.rvSelectCity.setVisibility(View.VISIBLE);
            // at last we are passing that filtered
            // list to our adapter class.
            specializationSelectionAdapter.filterList(filteredlist);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ivBackButton:
                onBackPressed();
                break;

            case R.id.btnDone:
                presenter.isCheck();

                break;

        }
    }

    @Override
    public void notifyAdapter() {
        specializationSelectionAdapter.notifyDataSetChanged();
    }

    @Override
    public void showError() {
        showError(getString(R.string.serverError));
    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clSelectCityActivity, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void finishActivity(Intent intent) {
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void nextCategoryActivity(SelectSpecializationData selectedCategory) {
        Intent intent = new Intent(this, SpecializationCategoryActivity.class);
        intent.putExtra("specialization", selectedCategory);
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_SPECIALIZATION_CATEGORY);
    }

    @Override
    public void onResume() {
        super.onResume();
        hideProgress();
    }

    @Override
    public void showProgress() {
        binding.pbLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.pbLoader.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == VariableConstant.REQUEST_CODE_SPECIALIZATION_CATEGORY) {
            if (data != null) {
                specializationData = data.getParcelableExtra("specialization");
            }
            sendDataBack();
        }
    }

    private void sendDataBack() {
        Intent intent = new Intent();
        intent.putExtra("specialization", specializationData);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
