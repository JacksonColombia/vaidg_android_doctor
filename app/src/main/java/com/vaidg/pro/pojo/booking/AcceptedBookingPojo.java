package com.vaidg.pro.pojo.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AcceptedBookingPojo implements Serializable {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<Booking> data;

    @Override
    public String toString() {
        return "{" +
                "message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Booking> getData() {
        return data;
    }

    public void setData(ArrayList<Booking> data) {
        this.data = data;
    }
}
