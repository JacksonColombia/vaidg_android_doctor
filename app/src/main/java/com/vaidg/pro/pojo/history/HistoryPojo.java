package com.vaidg.pro.pojo.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vaidg.pro.pojo.booking.Booking;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 13-Nov-17.
 */

public class HistoryPojo implements Serializable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<Booking> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Booking> getData() {
        return data;
    }

    public void setData(ArrayList<Booking> data) {
        this.data = data;
    }
}
