package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Relationship implements Serializable {

	@SerializedName("owner")
	@Expose
	private boolean owner;

	@SerializedName("executive")
	@Expose
	private boolean executive;

	@SerializedName("director")
	@Expose
	private boolean director;

	@SerializedName("title")
	@Expose
	private Object title;

	@SerializedName("representative")
	@Expose
	private boolean representative;

	@SerializedName("percent_ownership")
	@Expose
	private Object percentOwnership;

	public boolean isOwner(){
		return owner;
	}

	public boolean isExecutive(){
		return executive;
	}

	public boolean isDirector(){
		return director;
	}

	public Object getTitle(){
		return title;
	}

	public boolean isRepresentative(){
		return representative;
	}

	public Object getPercentOwnership(){
		return percentOwnership;
	}

	@Override
	public String toString(){
		return
				"Relationship{" +
						"owner = '" + owner + '\'' +
						",executive = '" + executive + '\'' +
						",director = '" + director + '\'' +
						",title = '" + title + '\'' +
						",representative = '" + representative + '\'' +
						",percent_ownership = '" + percentOwnership + '\'' +
						"}";
	}
}