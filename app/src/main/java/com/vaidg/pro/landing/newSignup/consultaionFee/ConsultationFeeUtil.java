package com.vaidg.pro.landing.newSignup.consultaionFee;

import android.app.Activity;

import com.vaidg.pro.adapters.ConsultationFeeAdapter;
import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.landing.newSignup.consultaionFee.model.ConsultationData;
import com.vaidg.pro.utility.SessionManager;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;

/**
 * <h1>@ConsultationFeeContract</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 28/04/2020.
 **/
@Module
public class ConsultationFeeUtil {

    @ActivityScoped
    @Provides
    ArrayList<ConsultationData> provideList() {
        ArrayList<ConsultationData> arrayList = new ArrayList<>();
        if(DEFAULT_LANGUAGE.equals("en")) {
            arrayList.add(new ConsultationData("Fees For Home Visits"));
            arrayList.add(new ConsultationData("Fees For Clinic Visits"));
            arrayList.add(new ConsultationData("Fees For Tele Appointments"));
        }else{
            arrayList.add(new ConsultationData("Honorários para domicílios"));
            arrayList.add(new ConsultationData("Honorários para consultas no consultorio"));
            arrayList.add(new ConsultationData("Honorários para contacto online"));
        }
        return arrayList;
    }

    @Provides
    @ActivityScoped
    ConsultationFeeAdapter provideConsultationFeeAdapter(ArrayList<ConsultationData> arrayList) {
        return new ConsultationFeeAdapter(arrayList);
    }

    @ActivityScoped
    @Provides
    SessionManager provide(Activity activity) {
        return SessionManager.getSessionManager(activity);
    }

}
