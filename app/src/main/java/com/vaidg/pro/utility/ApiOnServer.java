package com.vaidg.pro.utility;

public interface ApiOnServer {

    interface SignUp {

        String TITLE = "title";
        String FIRST_NAME = "firstName";
        String LAST_NAME = "lastName";
        String DOB = "dob";
        String GENDER = "gender";
        String PROFILE_PIC = "profilePic";
        String REFERRAL_CODE = "referralCode";
        String EMAIL = "email";
        String PASSWORD = "password";
        String COUNTRY_CODE = "countryCode";
        String MOBILE = "mobile";
        String CITY_ID = "cityId";
        String CAT_LIST = "catlist";
        String DOCTOR_TYPE = "doctorType";
        String HOSPITAL_ID = "hospitalId";
        String CLINIC_NAME = "clinicName";
        String CLINIC_LOGO_WEB = "clinicLogoWeb";
        String CLINIC_LOGO_APP = "clinicLogoApp";
        String META_DATA = "metaData";
        String LATITUDE = "latitude";
        String LONGITUDE = "longitude";
        String ADD_LINE_1 = "addLine1";
        String ADD_LINE_2 = "addLine2";
        String CITY = "city";
        String STATE = "state";
        String COUNTRY = "country";
        String PLACE_ID = "placeId";
        String PINCODE = "pincode";
        String TAGGED_AS = "taggedAs";
        String DEVICE_ID = "deviceId";
        String DEVICE_TYPE = "deviceType";
        String DEVICE_MAKE = "deviceMake";
        String DEVICE_MODEL = "deviceModel";
        String DEVICE_OS_VERSION = "deviceOsVersion";
        String BATTERY_PERC = "batteryPercentage";
        String LOCATION_HEADING = "locationHeading";
        String APP_VERSION = "appVersion";
        String PUSH_TOKEN = "pushToken";
        String DEVICE_TIME = "deviceTime";
        String PUSH_KIT_TOKEN = "pushKitToken";
        String DEGREE = "degree";
        String IN_CALL_FEE = "inCallFee";
        String OUT_CALL_FEE = "outCallFee";
        String TELE_CALL_FEE = "teleCallFee";
        String YEAR_OF_EXP = "yearOfExp";
        String PRIVATE_KEY = "privateKey";
        String PUBLIC_KEY = "publickKey";
    }

    interface LogIn {
        String MOBILE_OR_EMAIL = "mobileOrEmail";
        String PASSWORD = "password";
        String DEVICE_TYPE = "deviceType";
        String DEVICE_ID = "deviceId";
        String APP_VERSION = "appVersion";
        String DEVICE_MAKE = "deviceMake";
        String DEVICE_MODEL = "deviceModel";
        String DEVICE_OS_VERSION = "deviceOsVersion";
        String DEVICE_TIME = "deviceTime";
    }
    interface BookingFrag {
        String STATUS = "status";
        String PASSWORD = "password";
    }
}
