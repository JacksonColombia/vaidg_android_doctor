package com.vaidg.pro.pojo.profile.helpcenter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 29-Dec-17.
 */

public class TicketPojo implements Serializable {
    @SerializedName("data")
    @Expose
    private TicketData data;

    public TicketData getData() {
        return data;
    }

    public void setData(TicketData data) {
        this.data = data;
    }
}
