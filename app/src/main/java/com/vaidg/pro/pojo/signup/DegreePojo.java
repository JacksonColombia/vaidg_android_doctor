package com.vaidg.pro.pojo.signup;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DegreePojo implements Parcelable {


    public static final Creator<DegreePojo> CREATOR = new Creator<DegreePojo>() {
        @Override
        public DegreePojo createFromParcel(Parcel in) {
            return new DegreePojo(in);
        }

        @Override
        public DegreePojo[] newArray(int size) {
            return new DegreePojo[size];
        }
    };

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<DegreeData> data = null;

    protected DegreePojo(Parcel in) {
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DegreeData> getData() {
        return data;
    }

    public void setData(ArrayList<DegreeData> data) {
        this.data = data;
    }

}
