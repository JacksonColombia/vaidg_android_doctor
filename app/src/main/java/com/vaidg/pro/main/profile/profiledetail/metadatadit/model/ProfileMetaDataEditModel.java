package com.vaidg.pro.main.profile.profiledetail.metadatadit.model;

import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.pojo.profile.metaData.MetaDataArr;
import com.vaidg.pro.utility.Utility;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_CHECK_BOX;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DATE_FUTURE;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DATE_PAST;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DOCUMENT;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DROPDOWN;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_FEE;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_NUMBER;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_NUMBER_SLIDER;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_PICTURE_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_RADIO_BUTTON;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_TEXT_AREA;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_TEXT_AREA_COMMA_SEPARATED;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_TIME;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_VIDEO_UPLOAD;

public class ProfileMetaDataEditModel {

    @Inject
    List<MetaDataArr> metaDataList;

    @Inject
    public ProfileMetaDataEditModel() {
    }

    public void addMetaData(MetaDataArr data) {
        metaDataList.add(data);
    }

    public void setData(String data) {
        if (metaDataList != null && metaDataList.size() > 0) {
            metaDataList.get(0).setData(data);
        }
    }

    public String getId() {
        if (metaDataList != null && metaDataList.size() > 0) {
            return metaDataList.get(0).getId();
        }
        return "";
    }

    public String getData() {
        if (metaDataList != null && metaDataList.size() > 0 && !Utility.isTextEmpty(metaDataList.get(0).getType())) {
            switch (Integer.parseInt(metaDataList.get(0).getType())) {
                case SPECIALIZATION_VIEW_RADIO_BUTTON:
                case SPECIALIZATION_VIEW_CHECK_BOX:
                case SPECIALIZATION_VIEW_DROPDOWN:
                    String data = "";
                    if (metaDataList.get(0).getPreDefined().size() > 0) {
                        StringBuilder sb = new StringBuilder();
                        for (PreDefined preDefined : metaDataList.get(0).getPreDefined()) {
                            if (preDefined.isSelected())
                                sb.append(preDefined.getId()).append(",");
                        }
                        data = sb.deleteCharAt(sb.length() - 1).toString();
                    }
                    return data;
                default:
                    return metaDataList.get(0).getData();
            }
        }
        return "";
    }

    public void setPredefineData(int position, String data) {
        if (metaDataList != null && metaDataList.size() > 0 && metaDataList.get(0).getPreDefined() != null && metaDataList.get(0).getPreDefined().size() > 0) {
            metaDataList.get(0).getPreDefined().get(position).setData(data);
        }
    }


    public void deleteDocument(int position) {
        if (metaDataList != null && metaDataList.size() > 0 && metaDataList.get(0).getPreDefined() != null && metaDataList.get(0).getPreDefined().size() > 0) {
            metaDataList.get(0).getPreDefined().get(position).setData("");
        }
    }

    public boolean isCheck() {
        if (metaDataList != null && metaDataList.size() > 0 && !Utility.isTextEmpty(metaDataList.get(0).getType())) {
            switch (Integer.parseInt(metaDataList.get(0).getType())) {
                case SPECIALIZATION_VIEW_RADIO_BUTTON:
                case SPECIALIZATION_VIEW_CHECK_BOX:
                case SPECIALIZATION_VIEW_DROPDOWN:
                    for (PreDefined preDefined : metaDataList.get(0).getPreDefined()) {
                        if (preDefined.isSelected())
                            return true;
                    }
                    break;
                case SPECIALIZATION_VIEW_DOCUMENT:
                    for (PreDefined preDefined : metaDataList.get(0).getPreDefined()) {
                        if (preDefined.getIsManadatory() == 1 && (preDefined.getData() != null && !preDefined.getData().trim().isEmpty())) {
                            return true;
                        }
                    }
                    break;
                case SPECIALIZATION_VIEW_FEE:
                case SPECIALIZATION_VIEW_NUMBER:
                case SPECIALIZATION_VIEW_NUMBER_SLIDER:
                case SPECIALIZATION_VIEW_TEXT_AREA:
                case SPECIALIZATION_VIEW_TEXT_AREA_COMMA_SEPARATED:
                case SPECIALIZATION_VIEW_DATE_FUTURE:
                case SPECIALIZATION_VIEW_DATE_PAST:
                case SPECIALIZATION_VIEW_TIME:
                case SPECIALIZATION_VIEW_PICTURE_UPLOAD:
                case SPECIALIZATION_VIEW_VIDEO_UPLOAD:
                    if (metaDataList.get(0).getData() != null && !metaDataList.get(0).getData().trim().isEmpty())
                        return true;
                    break;
            }
        }
        return false;
    }

    public void saveData(MetaDataArr metaDataArr) {
        if (Utility.isTextEmpty(metaDataArr.getType())) {
            return;
        }

        switch (Integer.parseInt(metaDataList.get(0).getType())) {
            case SPECIALIZATION_VIEW_RADIO_BUTTON:
            case SPECIALIZATION_VIEW_CHECK_BOX:
            case SPECIALIZATION_VIEW_DROPDOWN:
                String data = "";
                if (metaDataList != null && metaDataList.size() > 0) {
                    if (metaDataList.get(0).getPreDefined().size() > 0) {
                        StringBuilder sb = new StringBuilder();
                        for (PreDefined preDefined : metaDataList.get(0).getPreDefined()) {
                            if (preDefined.isSelected())
                                sb.append(preDefined.getId()).append(",");
                        }
                        data = sb.deleteCharAt(sb.length() - 1).toString();
                    }
                }
                metaDataArr.setData(data);
                break;
            case SPECIALIZATION_VIEW_DATE_FUTURE:
            case SPECIALIZATION_VIEW_DATE_PAST:
            case SPECIALIZATION_VIEW_TIME:
                if (metaDataList != null && metaDataList.size() > 0) {
                    metaDataArr.setData(metaDataList.get(0).getData());
                }
                break;
        }
    }

    /**
     * <h2>@deleteFile</h2>
     * <p>this is used to delete file</p>
     */
    public void deleteFile(File file) {
        try {
            if (file != null && file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
