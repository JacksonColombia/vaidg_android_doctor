package com.vaidg.pro.main.profile.editemailphone;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityEditEmailPhoneBinding;
import com.vaidg.pro.otpverify.OTPVerifyActivity;
import com.vaidg.pro.pojo.phonevalidation.PhoneValidationPojo;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import javax.inject.Inject;

/**
 * Created by murashid on 11-Oct-17.
 * <h1>EditEmailPhoneActivity</h1>
 * EditEmailPhoneActivity activity for changing email or phone number based on isPhoneEdit
 */

public class EditEmailPhoneActivity extends BaseDaggerActivity implements EditEmailPhoneContract.View, View.OnClickListener {


    @Inject
    EditEmailPhoneContract.Presenter presenter;
    @Inject
    SessionManager sessionManager;

    private ProgressDialog progressDialog;
    private ActivityEditEmailPhoneBinding binding;

    private boolean isPhoneEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditEmailPhoneBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
        initViewOnClickListeners();
        initOnTextChangeListener();
    }

    private void initViews() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        try {
            binding.ccp.setCountryForPhoneCode(Integer.parseInt(sessionManager.getCountryCode().substring(1)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        isPhoneEdit = getIntent().getBooleanExtra("isPhoneEdit", false);

        if (isPhoneEdit) {
            progressDialog.setMessage(getString(R.string.updatingPhone));
            binding.includeToolbar.tvTitle.setText(getString(R.string.changePhone));
            binding.btnSave.setText(getString(R.string.save));
            binding.tvChangePhoneEmailHint.setText(getString(R.string.phoneChangHint));
            binding.etEmail.setVisibility(View.GONE);
            binding.ivEmailPhone.setImageResource(R.drawable.change_mobile_icon);
            Utility.showKeyBoard(this, binding.etEmail);
        } else {
            progressDialog.setMessage(getString(R.string.updadingEmail));
            binding.includeToolbar.tvTitle.setText(getString(R.string.changeEmail));
            binding.btnSave.setText(getString(R.string.save));
            binding.tvChangePhoneEmailHint.setText(getString(R.string.emailChangHint));
            binding.llPhone.setVisibility(View.GONE);
            binding.ivEmailPhone.setImageResource(R.drawable.change_email_icon);
            Utility.showKeyBoard(this, binding.etPhone);
        }
        binding.ccp.registerCarrierNumberEditText(binding.etPhone);
    }

    private void initOnTextChangeListener() {
        binding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.tlEmail.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initViewOnClickListeners() {
        binding.btnSave.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboad(this);
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSave) {
            Utility.hideKeyboad(this);
            if (isPhoneEdit) {
                presenter.changePhoneNumber(binding.ccp.getSelectedCountryCodeWithPlus(), binding.etPhone.getText() == null ? "" : binding.etPhone.getText().toString());
            } else {
                presenter.changeEmail(binding.etEmail.getText() == null ? "" : binding.etEmail.getText().toString());
            }
        }
    }

    @Override
    public void onEmailIncorrect(String errorMessage) {
        binding.tlEmail.setError(errorMessage);
    }

    @Override
    public boolean isPhoneNumberCorrect() {
        return binding.ccp.isValidFullNumber();
    }

    @Override
    public void onPhoneNumberIncorrect(String errorMessage) {
        showError(errorMessage);
    }

    @Override
    public void onSuccessPhoneNoChange(PhoneValidationPojo phoneValidationPojo) {
        Utility.printLog("EmailPhone", phoneValidationPojo.toString());
        Intent intent = new Intent(this, OTPVerifyActivity.class);
        intent.putExtra("phone", binding.etPhone.getText() == null ? "" : binding.etPhone.getText().toString());
        intent.putExtra("countryCode", binding.ccp.getSelectedCountryCodeWithPlus());
        intent.putExtra("userId", phoneValidationPojo.getData().getSid());
        intent.putExtra("expireOtp", phoneValidationPojo.getData().getExpireOtp());
        intent.putExtra("otpOption", 3);
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        finish();
    }

    @Override
    public void onSuccessEmailChange(String msg) {
        VariableConstant.IS_PROFILE_EDITED = true;
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clParent, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void showProgress() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void hideProgress() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onFailure() {
        showError(getString(R.string.serverError));
    }
}
