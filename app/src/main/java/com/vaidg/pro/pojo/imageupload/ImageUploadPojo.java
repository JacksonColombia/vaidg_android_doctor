package com.vaidg.pro.pojo.imageupload;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageUploadPojo implements Parcelable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ImageUploadData data;
    public final static Parcelable.Creator<ImageUploadPojo> CREATOR = new Creator<ImageUploadPojo>() {
        
        public ImageUploadPojo createFromParcel(Parcel in) {
            return new ImageUploadPojo(in);
        }

        public ImageUploadPojo[] newArray(int size) {
            return (new ImageUploadPojo[size]);
        }

    };

    protected ImageUploadPojo(Parcel in) {
        this.message = in.readString();
        this.data = ((ImageUploadData) in.readValue((ImageUploadData.class.getClassLoader())));
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ImageUploadData getData() {
        return data;
    }

    public void setData(ImageUploadData data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

}
