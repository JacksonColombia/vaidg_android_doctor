package com.vaidg.pro.main.profile.wallet.cardlist;

import static com.appscrip.stripe.Constants.AUTHORIZATION;
import static com.appscrip.stripe.Constants.LANGUAGE;
import static com.appscrip.stripe.Constants.USER_ID;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.appscrip.stripe.StripePaymentIntentActivity;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.CardListAdapter;
import com.vaidg.pro.pojo.profile.wallet.CardData;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import dagger.android.support.DaggerAppCompatActivity;
import javax.inject.Inject;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class CardListActivity extends DaggerAppCompatActivity implements View.OnClickListener, CardListAdapter.CardClickListener, CardListContract.View {
    private ProgressDialog progressDialog;
    @Inject
    CardListContract.Presenter presenter;
    private SessionManager sessionManager;

    private ArrayList<CardData> cardData;
    private CardListAdapter cardListAdapter;
    private int selectedPosition;
    private RecyclerView rvCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_list);
        initialize();
    }

    /**
     * <h2>initialize</h2>
     * <p>
     * initialize the views
     * </p>
     */
    private void initialize() {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.selectCard));
        tvTitle.setTypeface(fontBold);

        TextView tvAddCard = findViewById(R.id.tvAddCard);
        tvAddCard.setTypeface(fontMedium);
        tvAddCard.setOnClickListener(this);

        rvCard = findViewById(R.id.rvCard);
        rvCard.setLayoutManager(new LinearLayoutManager(this));
        cardData = new ArrayList<>();


        presenter.getCardDetails();
    }

    @Override
    protected void onResume() {
        super.onResume();
       // if (VariableConstant.IS_CARD_UPDATED) {
            VariableConstant.IS_CARD_UPDATED = false;
            presenter.getCardDetails();
       // }
    }

    /**
     * <h1>onClick</h1>
     * <p>
     * Called when a view has been clicked.
     * </p>
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvAddCard:
               // Intent intent = new Intent(CardListActivity.this, AddCardActivity.class);
                Intent intent = new Intent(CardListActivity.this, StripePaymentIntentActivity.class);
                intent.putExtra(USER_ID, sessionManager.getProviderId());
                intent.putExtra(AUTHORIZATION,Utility.getAuth(sessionManager.getEmail()));
                intent.putExtra(LANGUAGE, DEFAULT_LANGUAGE);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();
    }

    @Override
    public void onCardSelect(int position) {
        selectedPosition = position;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cardId", cardData.get(position).getId());
            presenter.selectCard( jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCardDelete(int position) {
        selectedPosition = position;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cardId", cardData.get(position).getId());
            presenter.deleteCard(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void startProgressBar() {
        runOnUiThread(() -> {
            Utility.progressDialogShow(this, progressDialog);
        });
    }

    @Override
    public void stopProgressBar() {
        runOnUiThread(() -> {
            Utility.progressDialogDismiss(this, progressDialog);
        });
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccessCardDetails(ArrayList<CardData> data) {
        runOnUiThread(() -> {
            // Stuff that updates the UI
            stopProgressBar();
            if(data != null && data.size() > 0) {
                rvCard.setVisibility(View.VISIBLE);
                VariableConstant.IS_CARD_UPDATED = true;
                cardData.clear();
                cardData.addAll(data);
                cardListAdapter = new CardListAdapter(this, cardData, sessionManager, this);
                rvCard.setAdapter(cardListAdapter);
            }else{
                resetSelectedCard();
            }
        });

    }

    @Override
    public void onSuccessSelectCard(String msg) {
        runOnUiThread(() -> {
            // Stuff that updates the UI
            for (int i = 0; i < cardData.size(); i++) {
                if (i == selectedPosition) {
                    cardData.get(i).setDefault(true);
                } else {
                    cardData.get(i).setDefault(false);
                }
            }
            cardListAdapter.notifyDataSetChanged();
            closeActivity();
        });

    }

    @Override
    public void onSuccessDeleteCard(String msg) {
        if (sessionManager.getSelectedCardId().equals(cardData.get(selectedPosition).getId())) {
            resetSelectedCard();
        }
        cardData.remove(selectedPosition);
        cardListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, this);
    }

    private void resetSelectedCard() {
        sessionManager.setSelectedCardId("");
        sessionManager.setSelectedCardNumber("");
        sessionManager.setCardBrand("");
       rvCard.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }
}
