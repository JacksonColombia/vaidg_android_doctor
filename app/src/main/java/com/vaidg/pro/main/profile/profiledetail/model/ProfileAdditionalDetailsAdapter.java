package com.vaidg.pro.main.profile.profiledetail.model;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ItemProfileAdditionalDetailsBinding;
import com.vaidg.pro.main.profile.profiledetail.ProfileDetailContract;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.pojo.profile.metaData.MetaDataArr;
import com.vaidg.pro.utility.Utility;

import java.util.List;

import javax.inject.Inject;

import static com.vaidg.pro.utility.VariableConstant.FORMAT_TIME_0_23_HRS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_TIME_1_12_HRS;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_CHECK_BOX;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DATE_FUTURE;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DATE_PAST;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DOCUMENT;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DROPDOWN;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_FEE;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_NUMBER;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_NUMBER_SLIDER;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_PICTURE_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_RADIO_BUTTON;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_TEXT_AREA;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_TEXT_AREA_COMMA_SEPARATED;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_TIME;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_VIDEO_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.ZERO;

public class ProfileAdditionalDetailsAdapter extends RecyclerView.Adapter<ProfileAdditionalDetailsAdapter.ProfileAdditionalDetailsViewHolder> {

    private Context context;
    private List<MetaDataArr> list;
    private long mLastClickTime = System.currentTimeMillis();
    private static final long CLICK_TIME_INTERVAL = 300;

    @Inject
    ProfileDetailContract.Presenter presenter;

    public ProfileAdditionalDetailsAdapter(List<MetaDataArr> list) {
        this.list = list;
    }

    public void setonAdditionalDetailsClickListener(ProfileDetailContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ProfileAdditionalDetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        ItemProfileAdditionalDetailsBinding binding = ItemProfileAdditionalDetailsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ProfileAdditionalDetailsViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileAdditionalDetailsViewHolder holder, int position) {
        MetaDataArr dataArr = list.get(position);

        if (dataArr.getLinkedToProvider() && dataArr.getVisibleOnProviderProfile()) {
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(holder.binding.clParent);
            holder.binding.lblAdditionalTitle.setText(dataArr.getName());
            if (dataArr.getEditableOnProvider()) {
                holder.binding.tvAdditionalValue.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context, R.drawable.ic_right_arrow), null);
                holder.binding.tvAdditionalValue.setCompoundDrawablePadding(context.getResources().getDimensionPixelSize(R.dimen.dp_8));
                constraintSet.connect(R.id.tvAdditionalValue, ConstraintSet.END, R.id.glVerticalEnd, ConstraintSet.END, 0);
                constraintSet.applyTo(holder.binding.clParent);
            }

            StringBuilder additionalValue = new StringBuilder();

            if (Utility.isTextEmpty(dataArr.getType()))
                return;

            switch (Integer.parseInt(dataArr.getType())) {
                case SPECIALIZATION_VIEW_NUMBER_SLIDER: // Number (slider)
                case SPECIALIZATION_VIEW_NUMBER: // Number
                case SPECIALIZATION_VIEW_FEE: // Fee
                case SPECIALIZATION_VIEW_TEXT_AREA: // Text-area
                case SPECIALIZATION_VIEW_TEXT_AREA_COMMA_SEPARATED: // Text-area (comma separated)
                case SPECIALIZATION_VIEW_DATE_FUTURE: // Date(from current to future
                case SPECIALIZATION_VIEW_DATE_PAST: // Date(from past to current)
                    additionalValue.append(dataArr.getData());
                    break;
                case SPECIALIZATION_VIEW_TIME: // Date(from past to current)
                    String time = Utility.changeDateTimeFormat(dataArr.getData(), FORMAT_TIME_0_23_HRS, FORMAT_TIME_1_12_HRS);
                    additionalValue.append(Utility.isTextEmpty(time) ? "-" : time);
                    break;
                case SPECIALIZATION_VIEW_RADIO_BUTTON: // Radio Button
                case SPECIALIZATION_VIEW_DROPDOWN: // Dropdown Menu
                case SPECIALIZATION_VIEW_CHECK_BOX: // CheckBox (multi select)
                    if (!Utility.isTextEmpty(dataArr.getData()) && dataArr.getPreDefined() != null && dataArr.getPreDefined().size() > 0) {
                        String[] ids = dataArr.getData().contains(",") ? dataArr.getData().split(",") : new String[]{dataArr.getData()};
                        for (PreDefined preDefined : dataArr.getPreDefined()) {
                            if (preDefined.getId().equals(ids[0])) {
                                additionalValue.append(preDefined.getName());
                                break;
                            }
                        }

                        if (ids.length > 1) {
                            additionalValue.append(", +").append(ids.length - 1);
                        }
                    }
                    break;

                case SPECIALIZATION_VIEW_PICTURE_UPLOAD: // Picture upload
                case SPECIALIZATION_VIEW_VIDEO_UPLOAD: // Video Upload
                case SPECIALIZATION_VIEW_DOCUMENT: // Document
                    additionalValue.append(context.getString(R.string.view));
                    break;

            }
            if (Utility.isTextEmpty(additionalValue))
                additionalValue.append("-");
            holder.binding.tvAdditionalValue.setText(additionalValue);

        }
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : ZERO;
    }

    class ProfileAdditionalDetailsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemProfileAdditionalDetailsBinding binding;

        ProfileAdditionalDetailsViewHolder(ItemProfileAdditionalDetailsBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
            binding.tvAdditionalValue.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (presenter != null) {
                long now = System.currentTimeMillis();
                if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
                    return;
                }
                mLastClickTime = now;
                new Handler().postDelayed(() -> presenter.onItemClick(getAdapterPosition(), list.get(getAdapterPosition())), 200);
            }
        }
    }
}
