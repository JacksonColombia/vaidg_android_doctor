package com.vaidg.pro.landing.newSignup.SpecializationCatagory;

import com.vaidg.pro.pojo.Attribute;

import javax.inject.Inject;

/*
 * <h1>@SpecializationCategoryPresenter</h1>
 * <p> thios is presenter used to for business logic</p>
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class SpecializationCategoryPresenter implements SpecializationCategoryContract.Presenter {

    @Inject
    SpecializationCategoryContract.View view;

    @Inject
    public SpecializationCategoryPresenter() {
    }

    @Override
    public void hidePreview() {
        if (view != null)
            view.hidePreview();
    }

    @Override
    public void openNextFrag(int fromList, int next, Attribute updatedAttribute) {
        if (view != null)
            view.openNextFrag(fromList, next, updatedAttribute);
    }

    @Override
    public void showError(String message) {
        view.showError(message);
    }
}
