package com.vaidg.pro.pojo.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Created by murashid on 21-Jun-18.
 */

public class QuestionAnswer implements Serializable {

    /*0. PromoCode
    1. Price
    2. StartDate
    3. Address
    4. PaymentMethod
    5. TextView
    6. RadioButton - One Answer
    7. checkBox - ManyAns seprated by ,
    8. start to end Date
    9. end to start Date
    10. Photo */

    @SerializedName("questionType")
    @Expose
    private String questionType;
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("symptomName")
    @Expose
    private String symptomName;

    public String  getQuestionType() {
        return questionType;
    }

    public String getAnswer() {
        return answer;
    }

    public String get_id() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public String getSymptomName() {
        return symptomName;
    }

   }
