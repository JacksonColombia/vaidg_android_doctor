package com.vaidg.pro.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnswerLanArr implements Parcelable {
    public static final Creator<AnswerLanArr> CREATOR = new Creator<AnswerLanArr>() {
        @Override
        public AnswerLanArr createFromParcel(Parcel source) {
            return new AnswerLanArr(source);
        }

        @Override
        public AnswerLanArr[] newArray(int size) {
            return new AnswerLanArr[size];
        }
    };
    @SerializedName("ar")
    @Expose
    private String ar;
    @SerializedName("en")
    @Expose
    private String en;
    @SerializedName("PT")
    @Expose
    private String pT;

    public AnswerLanArr() {
    }

    protected AnswerLanArr(Parcel in) {
        this.ar = in.readString();
        this.en = in.readString();
        this.pT = in.readString();
    }

    public String getAr() {
        return ar;
    }

    public void setAr(String ar) {
        this.ar = ar;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getPT() {
        return pT;
    }

    public void setPT(String pT) {
        this.pT = pT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ar);
        dest.writeString(this.en);
        dest.writeString(this.pT);
    }
}
