package com.vaidg.pro.pojo.profile.helpcenter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 29-Dec-17.
 */

public class HelpCenterData implements Serializable {
    @SerializedName("open")
    @Expose
    private ArrayList<Ticket> open;
    @SerializedName("close")
    @Expose
    private ArrayList<Ticket> close;

    public ArrayList<Ticket> getOpen() {
        return open;
    }

    public void setOpen(ArrayList<Ticket> open) {
        this.open = open;
    }

    public ArrayList<Ticket> getClose() {
        return close;
    }

    public void setClose(ArrayList<Ticket> close) {
        this.close = close;
    }
}
