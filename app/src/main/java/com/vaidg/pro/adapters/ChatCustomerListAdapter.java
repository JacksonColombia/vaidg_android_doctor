package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vaidg.pro.R;
import com.vaidg.pro.pojo.chat.ChatCutomerList;
import com.vaidg.pro.utility.CircleTransform;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_TIME_1_12_HRS;
import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * Created by murashid on 09-Apr-18.
 */

public class ChatCustomerListAdapter extends RecyclerView.Adapter<ChatCustomerListAdapter.ViewHolder> implements View.OnClickListener {

    private Context context;
    private SessionManager sessionManager;
    private ArrayList<ChatCutomerList> chatCutomerLists;
    private Typeface fontMedium, fontRegular;
    private CustomerClickListener customerClickListener;

    private String today;
    private String yesterday;

    public ChatCustomerListAdapter(Context context, ArrayList<ChatCutomerList> chatCutomerLists, CustomerClickListener customerClickListener) {
        this.context = context;
        this.sessionManager = SessionManager.getSessionManager(context);
        this.chatCutomerLists = chatCutomerLists;
        fontMedium = Utility.getFontMedium(context);
        fontRegular = Utility.getFontRegular(context);
        this.customerClickListener = customerClickListener;

        today = Utility.convertUTCToTodayDateFormat(String.valueOf(System.currentTimeMillis()));
        yesterday = Utility.convertUTCToYesterdayDateFormat(String.valueOf(System.currentTimeMillis()));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_chat_customer_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvCustomerName.setText(chatCutomerLists.get(position).getFirstName() + " " + chatCutomerLists.get(position).getLastName());
        holder.tvJobDetails.setText(chatCutomerLists.get(position).getCatName());

        if (!chatCutomerLists.get(position).getProfilePic().equals("")) {
            Glide.with(context).setDefaultRequestOptions(new RequestOptions().error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(context))
                    .placeholder(R.drawable.profile_default_image))
                    .load(chatCutomerLists.get(position).getProfilePic())
                    .into(holder.ivCustomer);
        }

        try {
            holder.tvDate.setText(getDisplayTime(chatCutomerLists.get(position).getBookingRequestedFor()));
            holder.tvTime.setText(Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(chatCutomerLists.get(position).getBookingRequestedFor()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_TIME_1_12_HRS));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sessionManager.getChatCount(chatCutomerLists.get(position).getBookingId()) != 0) {
            holder.tvCount.setVisibility(View.VISIBLE);
            holder.tvCount.setText("" + sessionManager.getChatCount(chatCutomerLists.get(position).getBookingId()));
            holder.tvTime.setVisibility(View.GONE);
        } else {
            holder.tvCount.setVisibility(View.GONE);
            holder.tvTime.setVisibility(View.VISIBLE);
        }

        holder.llChatCutomer.setOnClickListener(this);
        holder.llChatCutomer.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return chatCutomerLists != null ? chatCutomerLists.size() : ZERO;
    }

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position = viewHolder.getAdapterPosition();

        switch (v.getId()) {
            case R.id.llChatCutomer:
                customerClickListener.onCustomerClick(position);
                break;

        }
    }

    private String getDisplayTime(String date) {
        String responseDate = Utility.convertUTCToDateFormat(date);

        if (!responseDate.equals(today) && !responseDate.equals(yesterday)) {
            return responseDate;
        } else if (responseDate.equals(today)) {
            return context.getString(R.string.today);
        } else {
            return context.getString(R.string.yesterday);
        }
    }

    /**
     * <h1>RefreshBankDetails</h1>
     * Interface for refresh the bank list
     */
    public interface CustomerClickListener {
        void onCustomerClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivCustomer;
        TextView tvCustomerName, tvJobDetails, tvDate, tvTime, tvCount;
        LinearLayout llChatCutomer;

        public ViewHolder(View itemView) {
            super(itemView);

            ivCustomer = itemView.findViewById(R.id.ivCustomer);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvJobDetails = itemView.findViewById(R.id.tvJobDetails);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvCount = itemView.findViewById(R.id.tvCount);
            llChatCutomer = itemView.findViewById(R.id.llChatCutomer);

            tvCustomerName.setTypeface(fontMedium);
            tvJobDetails.setTypeface(fontRegular);
            tvDate.setTypeface(fontMedium);
            tvTime.setTypeface(fontRegular);
            tvCount.setTypeface(fontRegular);
        }

    }

}
