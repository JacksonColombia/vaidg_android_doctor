package com.kotlintestgradle.cache

import com.kotlintestgradle.cache.dao.LoginDao

/**
 * @author 3Embed
 *
 *To implement all CURD operations
 * @since 1.0 (23-Aug-2019)
 */
class DatabaseManagerImpl(var appDataBase: AppDataBase) : DatabaseManager {
    override fun login(): LoginDao {
        return appDataBase.loginDao()
    }
}