package com.vaidg.pro.landing.newSignup.SpecializationCatagory.slider;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.vaidg.pro.R;
import com.vaidg.pro.databinding.FragmentSliderBinding;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.SpecializationCategoryContract;
import com.vaidg.pro.pojo.Attribute;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

/**
 * <h1>@SliderFragment</h1>
 * <p>this fragment is used to displaying slider category  type 5
 * </P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class SliderFragment extends DaggerFragment implements SliderContract.View {

    private static final String TAG = SliderFragment.class.getSimpleName();

    @Inject
    Activity activity;

    @Inject
    SpecializationCategoryContract.Presenter mainPresenter;

    @Inject
    SliderContract.Presenter presenter;

    private Attribute currentItem;

    private int list_number, selectedNumber;


    private FragmentSliderBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        assert bundle != null;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentSliderBinding.inflate(inflater);
        return binding.getRoot();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setProgressBar();
        initOnclickLister();
        initSlider();
    }

    private void initSlider() {
        binding.sbSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                selectedNumber = progress;
                binding.tvSelectedNumber.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private void initOnclickLister() {
        binding.includeToolbar.btnDone.setOnClickListener(this::onclick);
    }

    private void onclick(View view) {
        if (view.getId() == R.id.btnDone) {
            currentItem.setData(String.valueOf(selectedNumber));
            mainPresenter.openNextFrag(list_number, currentItem.getPosition() + 1, currentItem);
        }
    }

    private void setProgressBar() {
        binding.progressBar.setMax(currentItem.getMax_count());
        binding.progressBar.setProgress(currentItem.getPosition() + 1);
    }

    /**
     * <h2>Setting the current preference.</h2>
     *
     * @param item the current attribute
     */
    public void setPreferenceItem(Attribute item) {
        currentItem = item;
        list_number = currentItem.getList_no();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void initViews() {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            activity.setSupportActionBar(binding.includeToolbar.toolbar);
            if (activity.getSupportActionBar() != null) {
                activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
                activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
                binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> activity.onBackPressed());
            }
        }

        binding.tvSelectedNumber.setText(String.valueOf(0));
        binding.includeToolbar.tvTitle.setText(getString(R.string.complete_your_profile));
        binding.tvSlideTitle.setText(currentItem.getQueForProviderSignup());
        binding.sbSlider.setMin(Integer.parseInt(currentItem.getMinimum()));
        binding.sbSlider.setMax(Integer.parseInt(currentItem.getMaximum()));
        binding.tvStartNumber.setText(String.format("%s %s", currentItem.getMinimum(), currentItem.getUnit()));
        binding.tvEndNumber.setText(String.format("%s %s", currentItem.getMaximum(), currentItem.getUnit()));
        binding.tvNumberUnit.setText(currentItem.getUnit());
        binding.includeToolbar.btnDone.setText(getString(R.string.next));
        binding.tvSelectedNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(currentItem.getMaximum().length())});
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.attachView(this);
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }
}
