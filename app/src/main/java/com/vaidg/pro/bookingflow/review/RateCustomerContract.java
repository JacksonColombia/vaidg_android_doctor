package com.vaidg.pro.bookingflow.review;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import org.json.JSONObject;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface RateCustomerContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onFailure(String msg);

    void onFailure();

    void onEmptyReview();

    void onSuccess(String msg, boolean isFromNotification, boolean isPageRefresh);

    void onNewToken(String newToken);

    void sessionExpired(String msg);
  }

  interface Presenter extends BasePresenter {

    void getBookingById(final String bookingId);

    void setCustomerRating(JSONObject jsonObject);

  }
}