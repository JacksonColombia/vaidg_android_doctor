package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.main.profile.bank.bankdetails.BankDetailsActivity;
import com.vaidg.pro.pojo.profile.bank.AccountRazorPayData;
import com.vaidg.pro.utility.Utility;

import java.io.Serializable;
import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * Created by murashid on 20-Mar-17.
 * <h1>BankListAdapter</h1>
 * BankList Recycler View adapter for bank list
 *
 * @see BankDetailsActivity
 */

public class BankRazorPayListAdapter extends RecyclerView.Adapter<BankRazorPayListAdapter.ViewHolder> implements View.OnClickListener {

    private static final String TAG = "BankListAdapter";
    private final ArrayList<AccountRazorPayData> bankLists;
    private final Typeface fontMedium;
    private final Typeface fontLight;
    private final RefreshRazorPayBankDetails refreshBankDetails;
    private final FragmentManager fragmentManager;
    private final Context context;

    public BankRazorPayListAdapter(Context context, ArrayList<AccountRazorPayData> bankLists, FragmentManager fragmentManager, RefreshRazorPayBankDetails refreshBankDetails) {
        this.bankLists = bankLists;
        this.context = context;
        fontMedium = Utility.getFontMedium(context);
        fontLight = Utility.getFontRegular(context);
        this.fragmentManager = fragmentManager;
        this.refreshBankDetails = refreshBankDetails;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BankRazorPayListAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_bank_details, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AccountRazorPayData accountData = bankLists.get(position);
        String accountNumber = accountData.getBankAccount().getAccountNumber().substring(0, 2);
        int length =
                accountData.getBankAccount().getAccountNumber().substring(0, accountData.getBankAccount().getAccountNumber().length() - 4).length();
        for (int i = 0; i < length; i++) {
            accountNumber = accountNumber.concat("x");
        }
        String lastFourDigits = "";
        if (accountData.getBankAccount().getAccountNumber().length() > 4) {
            lastFourDigits = accountData.getBankAccount().getAccountNumber().substring(accountData.getBankAccount().getAccountNumber().length() - 4);
        }
        accountNumber = accountNumber.concat(lastFourDigits);
        holder.tvAccountNo.setText(accountNumber);
        holder.tvBankName.setText(accountData.getBankAccount().getBankName());
        holder.tvRoutinNo.setText(context.getString(R.string.rountinNoLabel) + " " + accountNumber);
        //change to default account when we get the proper responce
      /*  if (accountData.isDefaultForCurrency()) {
            holder.ivTick.setVisibility(View.VISIBLE);
        } else {
            holder.ivTick.setVisibility(View.INVISIBLE);
        }*/

        holder.llBankDetails.setOnClickListener(this);
        holder.llBankDetails.setTag(holder);

    }

    @Override
    public int getItemCount() {
        return bankLists != null ? bankLists.size() : ZERO;
    }

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position = viewHolder.getAdapterPosition();

        switch (v.getId()) {
            case R.id.llBankDetails:
/*
                BottomSheetDialogFragment bottomSheetDialogFragment = BankBottomSheetFragment.newInstance(bankLists.get(position), refreshBankDetails);
                bottomSheetDialogFragment.show(fragmentManager, bottomSheetDialogFragment.getTag());
*/
                break;

        }
    }

    /**
     * <h1>RefreshRazorPayBankDetails</h1>
     * Interface for refresh the bank list
     */

    public interface RefreshRazorPayBankDetails extends Serializable {
        void onRefreshRazorPay();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvAccountNo, tvBankName, tvRoutinNo;
        LinearLayout llBankDetails;
        ImageView ivTick;

        public ViewHolder(View itemView) {
            super(itemView);

            tvAccountNo = itemView.findViewById(R.id.tvAccountNo);
            tvBankName = itemView.findViewById(R.id.tvBankName);
            tvRoutinNo = itemView.findViewById(R.id.tvRoutinNo);
            llBankDetails = itemView.findViewById(R.id.llBankDetails);
            ivTick = itemView.findViewById(R.id.ivTick);

            tvAccountNo.setTypeface(fontLight);
            tvBankName.setTypeface(fontLight);
            tvRoutinNo.setTypeface(fontLight);
        }

    }

}
