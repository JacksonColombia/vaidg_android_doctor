package com.vaidg.pro.landing.newSignup.yearOfExp;

import javax.inject.Inject;

/**
 * <h1>yearOfExpPresenter</h1>
 * <p>presenter class for bussiness logic and webservices</p>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 28/04/2020.
 **/
public class YearOfExpPresenter implements YearOfExpContract.Presenter {

    @Inject
    YearOfExpContract.View view;

    @Inject
    public YearOfExpPresenter() {
    }
}
