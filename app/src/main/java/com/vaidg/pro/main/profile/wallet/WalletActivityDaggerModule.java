package com.vaidg.pro.main.profile.wallet;

import android.app.Activity;
import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.main.schedule.addschedule.ScheculeAddContract;
import com.vaidg.pro.main.schedule.addschedule.ScheduleAddActivity;
import com.vaidg.pro.main.schedule.addschedule.ScheduleAddPresenter;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>ProfileDaggerModule</h1>
 * <p>This dagger module created for ProfileFragment to bind injected objects</p>
 *
 * @author 3embed.
 * @version 1.0.20.
 * @since 19/06/2020.
 **/
@Module
public abstract class WalletActivityDaggerModule {

    @ActivityScoped
    @Binds
    abstract Activity getActivity(WalletActivity mainActivity);


    @ActivityScoped
    @Binds
    abstract WalletContract.Presenter providePresenter(WalletPresenter presenter);

    @ActivityScoped
    @Binds
    abstract WalletContract.View provideView(WalletActivity activity);

}
