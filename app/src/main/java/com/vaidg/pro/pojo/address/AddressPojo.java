package com.vaidg.pro.pojo.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 29-Sep-17.
 */

public class AddressPojo implements Serializable {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<AddressData> data;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AddressData> getData() {
        return data;
    }

    public void setData(ArrayList<AddressData> data) {
        this.data = data;
    }

}
