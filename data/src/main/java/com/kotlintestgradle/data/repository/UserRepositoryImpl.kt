package com.kotlintestgradle.data.repository

import com.kotlintestgradle.data.DataSource
import com.kotlintestgradle.data.preference.PreferenceManager
import com.kotlintestgradle.repository.UserRepository
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(private val dataSource: DataSource) :
    BaseRepository(dataSource), UserRepository {
    override fun getMQTTTopic(): String {
        return preference.mqttTopic!!
    }

    override fun getWillopic(): String {
        return preference.willTopic!!
    }

    override fun getFcmTopic(): String {
        return preference.fcmTopic!!
    }

    override fun setAuthToken(authToken:String) {
        preference.authToken = authToken
        println(" token stored in preference " + preference.authToken)
    }

    override fun setLanguageCode(lang: String) {
        preference.languageCode = lang
    }

    override fun getUserId(): String {
        return preference.userId!!
    }

    private val preference: PreferenceManager = dataSource.preference()

    override fun isLogged(): Boolean {
        return preference.isLoggedIn
    }

    override fun setUserId(userId: String) {
        preference.userId = userId
    }
}