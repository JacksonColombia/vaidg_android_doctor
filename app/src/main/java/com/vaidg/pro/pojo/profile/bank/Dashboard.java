package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Dashboard implements Serializable {

	@SerializedName("timezone")
	@Expose
	private String timezone;

	@SerializedName("display_name")
	@Expose
	private String displayName;

	public String getTimezone(){
		return timezone;
	}

	public String getDisplayName(){
		return displayName;
	}

	@Override
 	public String toString(){
		return
			"Dashboard{" +
			"timezone = '" + timezone + '\'' +
			",display_name = '" + displayName + '\'' +
			"}";
		}
}