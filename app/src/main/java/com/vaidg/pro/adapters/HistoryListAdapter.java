package com.vaidg.pro.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.databinding.SingleRowHistoryBinding;
import com.vaidg.pro.main.history.historyinvoice.HistoryInvoiceActivity;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.CALL_TYPE_IN;
import static com.vaidg.pro.utility.VariableConstant.CALL_TYPE_OUT;
import static com.vaidg.pro.utility.VariableConstant.CALL_TYPE_TELE;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_SPACE_COLON_DD_MMMM_YYYY_TIME_0_12_HRS;
import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * Created by murashid on 13-Nov-17.
 * <h1>HistoryListAdapter</h1>
 * History Recycler View Adapter for showing the list of items under history
 */

public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.HistoryDetailsViewHolder> {

    private ArrayList<Booking> bookings;
    private Activity mAcitvity;
    private SessionManager sessionManager;

    public HistoryListAdapter(Activity context, ArrayList<Booking> bookings) {
        this.mAcitvity = context;
        this.bookings = bookings;
        sessionManager = SessionManager.getSessionManager(context);
    }

    @Override
    public HistoryDetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SingleRowHistoryBinding binding = SingleRowHistoryBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new HistoryDetailsViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final HistoryDetailsViewHolder holder, int position) {

        holder.binding.tvInvoiceStatus.setText(bookings.get(position).getStatusMsg());
        holder.binding.tvName.setText(Utility.toTitleCase(bookings.get(position).getFirstName().concat(" ").concat(bookings.get(position).getLastName())));
        holder.binding.tvAddress.setText(bookings.get(position).getAddLine1());
        holder.binding.tvBookingAmount.setText(Utility.getPrice(bookings.get(position).getAccounting().getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

        try {
            String dateTime = bookings.get(position).getBookingRequestedFor();
            if (dateTime != null) {
                holder.binding.tvDateTime.setText(Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(dateTime), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_COLON_DD_MMMM_YYYY_TIME_0_12_HRS));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (bookings.get(position).getCallType()) {
            case CALL_TYPE_IN:
                holder.binding.tvBookingType.setText(mAcitvity.getString(R.string.clinicVisit));
                Utility.setBackgroundColor(mAcitvity, holder.binding.tvBookingType, R.color.inCallDim);
                Utility.setTextColor(mAcitvity, holder.binding.tvBookingType, R.color.inCall);
                break;
            case CALL_TYPE_OUT:
                holder.binding.tvBookingType.setText(mAcitvity.getString(R.string.homeVisit));
                Utility.setBackgroundColor(mAcitvity, holder.binding.tvBookingType, R.color.outCallDim);
                Utility.setTextColor(mAcitvity, holder.binding.tvBookingType, R.color.outCall);
                break;
            case CALL_TYPE_TELE:
                holder.binding.tvBookingType.setText(mAcitvity.getString(R.string.teleAppointment));
                Utility.setBackgroundColor(mAcitvity, holder.binding.tvBookingType, R.color.teleCallDim);
                Utility.setTextColor(mAcitvity, holder.binding.tvBookingType, R.color.teleCallDim);
                break;
        }

        switch (bookings.get(position).getStatus()) {
            case VariableConstant.JOB_COMPLETED_RAISE_INVOICE:
                Utility.setTextColor(mAcitvity, holder.binding.tvInvoiceStatus, R.color.jobStatusCompleted);
                break;

            case VariableConstant.BOOKING_EXPIRED:
                Utility.setTextColor(mAcitvity, holder.binding.tvInvoiceStatus, R.color.jobStatusExpired);
                break;

            default:
                Utility.setTextColor(mAcitvity, holder.binding.tvInvoiceStatus, R.color.jobStatusCanceled);
                break;
        }

        holder.binding.cvHistoryDetails.setOnClickListener(view -> {
            Intent intent = new Intent(mAcitvity, HistoryInvoiceActivity.class);
            int position1 = holder.getAdapterPosition();
            Bundle bundle = new Bundle();
            bundle.putSerializable("booking", bookings.get(position1));
            intent.putExtras(bundle);
            mAcitvity.startActivity(intent);
            mAcitvity.overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
        });
    }

    @Override
    public int getItemCount() {
        return bookings != null ? bookings.size() : ZERO;
    }

    class HistoryDetailsViewHolder extends RecyclerView.ViewHolder {

        SingleRowHistoryBinding binding;

        public HistoryDetailsViewHolder(SingleRowHistoryBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }
    }
}
