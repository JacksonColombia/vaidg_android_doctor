package com.vaidg.pro.landing.newSignup.findHospital.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HospitalPhonePojo implements Parcelable {
    public static final Creator<HospitalPhonePojo> CREATOR = new Creator<HospitalPhonePojo>() {
        @Override
        public HospitalPhonePojo createFromParcel(Parcel source) {
            return new HospitalPhonePojo(source);
        }

        @Override
        public HospitalPhonePojo[] newArray(int size) {
            return new HospitalPhonePojo[size];
        }
    };
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("isCurrentlyActive")
    @Expose
    private Boolean isCurrentlyActive;

    public HospitalPhonePojo() {
    }

    protected HospitalPhonePojo(Parcel in) {
        this.countryCode = in.readString();
        this.phone = in.readString();
        this.isCurrentlyActive = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getIsCurrentlyActive() {
        return isCurrentlyActive;
    }

    public void setIsCurrentlyActive(Boolean isCurrentlyActive) {
        this.isCurrentlyActive = isCurrentlyActive;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.countryCode);
        dest.writeString(this.phone);
        dest.writeValue(this.isCurrentlyActive);
    }
}
