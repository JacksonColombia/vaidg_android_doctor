package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Branding implements Serializable {

	@SerializedName("icon")
	@Expose
	private Object icon;

	@SerializedName("logo")
	@Expose
	private Object logo;

	@SerializedName("secondary_color")
	@Expose
	private Object secondaryColor;

	@SerializedName("primary_color")
	@Expose
	private Object primaryColor;

	public Object getIcon(){
		return icon;
	}

	public Object getLogo(){
		return logo;
	}

	public Object getSecondaryColor(){
		return secondaryColor;
	}

	public Object getPrimaryColor(){
		return primaryColor;
	}

	@Override
 	public String toString(){
		return 
			"Branding{" + 
			"icon = '" + icon + '\'' + 
			",logo = '" + logo + '\'' + 
			",secondary_color = '" + secondaryColor + '\'' + 
			",primary_color = '" + primaryColor + '\'' + 
			"}";
		}
}