package com.vaidg.pro;

import com.vaidg.pro.pojo.appconfig.AppConfigData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h1>@BaseModel</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public class BaseModel {
    /**
     * @param result : API response
     * @return String message
     */
    public String getMessage(ResponseBody result) {
        try {
            JSONObject object = new JSONObject(result.string());
            return object.getString(AppConfigData.MESSAGE);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getMessage(Response<ResponseBody> result) {
        try {
            JSONObject object = new JSONObject(result.errorBody().string());
            return object.getString(AppConfigData.MESSAGE);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }


    public String getError(Response<ResponseBody> result) {
        try {
            JSONObject object = new JSONObject(result.errorBody().string());
            return object.getString(AppConfigData.MESSAGE);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @return String authorization key
     */
    public String getAuthorization() {
        return AppConfigData.AUTH_KEY;
    }

    /**
     * @return String language code
     */
    public String getLanguage() {
        return AppConfigData.DEFAULT_LANGUAGE;
    }

    /**
     * @return String platform =ANDROID
     */
    public String getPlatform() {
        return AppConfigData.PLATFORM;
    }


}
