package com.vaidg.pro.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreDefined implements Parcelable {
    public static final Creator<PreDefined> CREATOR = new Creator<PreDefined>() {
        @Override
        public PreDefined createFromParcel(Parcel source) {
            return new PreDefined(source);
        }

        @Override
        public PreDefined[] newArray(int size) {
            return new PreDefined[size];
        }
    };
    boolean selected = false;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("answerLanArr")
    @Expose
    private AnswerLanArr answerLanArr;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("isManadatory")
    @Expose
    private Integer isManadatory;

    private String data;

    public PreDefined() {
    }

    protected PreDefined(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.icon = in.readString();
        this.answerLanArr = in.readParcelable(AnswerLanArr.class.getClassLoader());
        this.type = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isManadatory = (Integer) in.readValue(Integer.class.getClassLoader());
        this.data = in.readString();
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public AnswerLanArr getAnswerLanArr() {
        return answerLanArr;
    }

    public void setAnswerLanArr(AnswerLanArr answerLanArr) {
        this.answerLanArr = answerLanArr;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIsManadatory() {
        return isManadatory;
    }

    public void setIsManadatory(Integer isManadatory) {
        this.isManadatory = isManadatory;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.icon);
        dest.writeParcelable(this.answerLanArr, flags);
        dest.writeValue(this.type);
        dest.writeValue(this.isManadatory);
        dest.writeString(this.data);
    }
}
