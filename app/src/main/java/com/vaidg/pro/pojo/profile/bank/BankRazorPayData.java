package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankRazorPayData implements Parcelable
{

	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("entity")
	@Expose
	private String entity;
	@SerializedName("contact_id")
	@Expose
	private String contactId;
	@SerializedName("account_type")
	@Expose
	private String accountType;
	@SerializedName("bank_account")
	@Expose
	private BankRazorPayAccount bankAccount;
	@SerializedName("batch_id")
	@Expose
	private Object batchId;
	@SerializedName("active")
	@Expose
	private boolean active;
	@SerializedName("created_at")
	@Expose
	private int createdAt;
	public final static Creator<BankRazorPayData> CREATOR = new Creator<BankRazorPayData>() {


		@SuppressWarnings({
				"unchecked"
		})
		public BankRazorPayData createFromParcel(Parcel in) {
			return new BankRazorPayData(in);
		}

		public BankRazorPayData[] newArray(int size) {
			return (new BankRazorPayData[size]);
		}

	}
			;

	protected BankRazorPayData(Parcel in) {
		this.id = ((String) in.readValue((String.class.getClassLoader())));
		this.entity = ((String) in.readValue((String.class.getClassLoader())));
		this.contactId = ((String) in.readValue((String.class.getClassLoader())));
		this.accountType = ((String) in.readValue((String.class.getClassLoader())));
		this.bankAccount = ((BankRazorPayAccount) in.readValue((BankRazorPayAccount.class.getClassLoader())));
		this.batchId = ((Object) in.readValue((Object.class.getClassLoader())));
		this.active = ((boolean) in.readValue((boolean.class.getClassLoader())));
		this.createdAt = ((int) in.readValue((int.class.getClassLoader())));
	}

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public BankRazorPayData() {
	}

	/**
	 *
	 * @param bankAccount
	 * @param createdAt
	 * @param contactId
	 * @param accountType
	 * @param active
	 * @param id
	 * @param batchId
	 * @param entity
	 */
	public BankRazorPayData(String id, String entity, String contactId, String accountType, BankRazorPayAccount bankAccount, Object batchId, boolean active, int createdAt) {
		super();
		this.id = id;
		this.entity = entity;
		this.contactId = contactId;
		this.accountType = accountType;
		this.bankAccount = bankAccount;
		this.batchId = batchId;
		this.active = active;
		this.createdAt = createdAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public BankRazorPayAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankRazorPayAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	public Object getBatchId() {
		return batchId;
	}

	public void setBatchId(Object batchId) {
		this.batchId = batchId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(int createdAt) {
		this.createdAt = createdAt;
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(id);
		dest.writeValue(entity);
		dest.writeValue(contactId);
		dest.writeValue(accountType);
		dest.writeValue(bankAccount);
		dest.writeValue(batchId);
		dest.writeValue(active);
		dest.writeValue(createdAt);
	}

	public int describeContents() {
		return 0;
	}

}