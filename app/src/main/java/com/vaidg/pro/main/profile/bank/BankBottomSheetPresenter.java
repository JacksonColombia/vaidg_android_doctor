package com.vaidg.pro.main.profile.bank;

import android.content.Context;
import java.util.Map;
import org.json.JSONObject;

/**
 * Created by murashid on 29-Aug-17.
 * <h1>BankBottomSheetPresenter</h1>
 * BankBottomSheetPresenter presenter for BankBottomSheetfragment
 *
 * @see BankBottomSheetFragment
 */

public class BankBottomSheetPresenter implements BankBottomSheetModel.BankBottomSheetModelImplem {

    private BankBottomSheetPresenterImplem bankBottomSheetPresenterImplem;
    private BankBottomSheetModel bankBottomSheetModel;

    BankBottomSheetPresenter(BankBottomSheetPresenterImplem bankBottomSheetPresenterImplem, Context context) {
        this.bankBottomSheetPresenterImplem = bankBottomSheetPresenterImplem;
        this.bankBottomSheetModel = new BankBottomSheetModel(this,context);
    }

    /**
     * method for passing values from view to model
     *
     * @param jsonObject required fields in json object
     */
    void makeDefault(JSONObject jsonObject) {
        bankBottomSheetPresenterImplem.startProgressBar();
        bankBottomSheetModel.setDefaultAccount(jsonObject);
    }

    /**
     * method for passing values from view to model
     *
     * @param jsonObject required fields in json object
     */
    void deleteAccount(JSONObject jsonObject) {
        bankBottomSheetPresenterImplem.startProgressBar();
        bankBottomSheetModel.deleteBankAccount(jsonObject);
    }


    @Override
    public void onFailure(String failureMsg) {
        bankBottomSheetPresenterImplem.stopProgressBar();
        bankBottomSheetPresenterImplem.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        bankBottomSheetPresenterImplem.stopProgressBar();
        bankBottomSheetPresenterImplem.onFailure();
    }

    @Override
    public void onSuccess(String msg) {
        bankBottomSheetPresenterImplem.stopProgressBar();
        bankBottomSheetPresenterImplem.onSuccess(msg);
    }

    /**
     * BankBottomSheetPresenterImplem interface for View implementation
     */
    interface BankBottomSheetPresenterImplem {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String msg);

        void onFailure();

        void onSuccess(String msg);
    }
}
