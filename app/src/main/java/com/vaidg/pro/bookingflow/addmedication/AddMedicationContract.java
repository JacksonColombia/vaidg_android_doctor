package com.vaidg.pro.bookingflow.addmedication;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.addMedication.AddMedicationData;
import com.vaidg.pro.pojo.medication.MedicationData;
import java.util.ArrayList;
import org.json.JSONObject;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface AddMedicationContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onFailure(String failureMsg);

    void onFailure();

    void onNewToken(String newToken);

    void sessionExpired(String msg);

    void onSuccessResonse(AddMedicationData data);

    void onSuccessEditResonse(ArrayList<MedicationData> data);

    void onSuccessAddResponse();
  }

  interface Presenter extends BasePresenter {
    void getMedication();

    void getEditMedication(String bookidId, String medicationId);

    void addMedication(JSONObject body);

    void editMedication(JSONObject body);
  }
}