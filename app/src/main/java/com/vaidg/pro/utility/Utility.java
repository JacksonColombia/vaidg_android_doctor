
package com.vaidg.pro.utility;

import static com.vaidg.pro.AppController.*;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_DISPLAYLANGUAGE;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.BASE_AUTH_PASSWORD;
import static com.vaidg.pro.utility.VariableConstant.BASE_AUTH_USERNAME;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.RAZORPAY_KEY;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.Manifest;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorRes;
import androidx.appcompat.app.ActionBar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.graphics.drawable.DrawableCompat;

import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.BuildConfig;
import com.vaidg.pro.R;
import com.vaidg.pro.landing.splash.SplashActivity;
import com.vaidg.pro.network.BasicAuthentication;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.ServiceFactory;
import com.vaidg.pro.pojo.Age;
import com.vaidg.pro.pojo.appconfig.AppConfigData;
import com.vaidg.pro.service.LocationPublishService;

import io.card.payment.CardType;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Response;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>Utility</h1>
 * The class contain the methods thats used more than one times in the whole app
 */

public class Utility {
    private static final String TAG = "Utility";
    private static long lastRefreshTime = 0;
    Context context;
    private Gson gson;

    public Utility(Context context) {
        gson = new Gson();
        this.context = context;
    }

    /**
     * method for printing the log
     *
     * @param TAG tag
     * @param log log message
     */
    public static void printLog(String TAG, String log) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, log);
        }

    }

    public static void setAlarm(Context context, String bookingEndTime) {
        Intent intent = new Intent("com.vaidg.pro.alarmForTelecalls");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(getInstance().getTimeZone());
        calendar.setTimeInMillis((Long.parseLong(bookingEndTime) * 1000L) - 45 * 60000);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 1000 * 60, pendingIntent);
    }


    /**
     * method for create Light font Typeface from assets folder
     *
     * @param context context
     * @return light Typeface
     */
    public static Typeface getFontLight(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/hind.light.ttf");
    }

    /**
     * method for create Regular font Typeface from assets folder
     *
     * @param context context
     * @return Regular Typeface
     */
    public static Typeface getFontRegular(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/hind.regular.ttf");
    }

    /**
     * method for create Bold font Typeface from assets folder
     *
     * @param context context
     * @return Bold Typeface
     */
    public static Typeface getFontBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/hind.semibold.ttf");
    }

    /**
     * method for create Medium font Typeface from assets folder
     *
     * @param context context
     * @return Medium Typeface
     */
    public static Typeface getFontMedium(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/hind.medium.ttf");
    }

    // gives the basic auth
    public static String getBasicAuth() {
        String credentials = BuildConfig.BASIC_AUTH_USERNAME + ":" + BuildConfig.BASIC_AUTH_PASSWORD;
        return "basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
    }

    public static String getAuth(String email) {
        return getInstance().getAccountManagerHelper().getAuthToken(email);
    }

    /**
     * method for geting the device id
     *
     * @param context context
     * @return device id
     */
    public static String getDeviceId(Context context) {
        try {
            final TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return "1234567890";
            } else {
                if (mTelephony.getDeviceId() != null) {

                    return mTelephony.getDeviceId();
                } else {
                    return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "12346666";
        }
    }

    /**
     * method for get location
     *
     * @param context context
     * @return latitude and longitude
     */
    public static double[] getLocation(Context context) {
        double[] latLng = new double[2];
        LocationManager mLocationManager;
        Location location = null;
        try {
            mLocationManager = (LocationManager) context.getSystemService(Service.LOCATION_SERVICE);
            // getting GPS status
            boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            latLng[0] = 0.0;
            latLng[1] = 0.0;

            if (isGPSEnabled && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.d("Utility", "Gps Enabled");


                location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    latLng[0] = location.getLatitude();
                    latLng[1] = location.getLongitude();
                }
                Log.i("Utility", "Gps 1  latLng[0]: " + latLng[0] + "  latLng[1]: " + latLng[1]);

                if (latLng[0] == 0.0) {
                    List<String> providers = mLocationManager.getProviders(true);
                    for (int i = providers.size() - 1; i >= 0; i--) {
                        Log.d(TAG, "getLocation: " + providers.get(i));
                        location = mLocationManager.getLastKnownLocation(providers.get(i));
                        if (location != null) break;
                    }

                    if (location != null) {
                        latLng[0] = location.getLatitude();
                        latLng[1] = location.getLongitude();
                    }

                }
                Log.i("Utility", "Gps 2  latLng[0]: " + latLng[0] + "  latLng[1]: " + latLng[1]);
            } else {
                location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latLng[0] = location.getLatitude();
                    latLng[1] = location.getLongitude();
                }
                Log.i("Utility", "Network  latLng[0]: " + latLng[0] + "  latLng[1]: " + latLng[1]);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Utility", "getAddress: " + e);
        }
        Log.i("Utility", "latLng[0]: " + latLng[0] + "  latLng[1]: " + latLng[1]);
        return latLng;
    }
    /******************************************************/

    /**
     * method for creating circle bitmap and normal bitmap
     *
     * @param bitmap input values
     * @return rounded bitmap
     */
    public static Bitmap getCircleCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    /**
     * method for returing current time
     *
     * @return current time
     */
    public static String getCurrentTime() {
        Calendar calendar = Calendar.getInstance(Locale.US);
        calendar.setTimeZone(getInstance().getTimeZone());
        Date date = calendar.getTime();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        formater.setTimeZone(getInstance().getTimeZone());
        return formater.format(date);
    }


    public static String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "'st";
            case 2:
                return "'nd";
            case 3:
                return "'rd";
            default:
                return "'th";
        }
    }

    /**
     * method for conveting utctime to custom time format
     *
     * @param time utc time
     * @return custom time format
     */
    public static String convertUTCToServerFormat(String time) {
        long timestamp = Long.parseLong(time) * 1000;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        sdf.setTimeZone(getInstance().getTimeZone());
        Calendar cal = Calendar.getInstance(Locale.US);
         cal.setTimeZone(getInstance().getTimeZone());
        cal.setTimeInMillis(timestamp);
        //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        Date currenTimeZone = cal.getTime();
        return sdf.format(currenTimeZone);
    }

    /**
     * method for conveting utctime to custom time format
     *
     * @param time utc time
     * @return custom time format
     */
    public static String convertUTCToServerFormat(String time, String format) {
        long timestamp = Long.parseLong(time) * 1000;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",  Locale.US);
        sdf.setTimeZone(getInstance().getTimeZone());
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTimeZone(getInstance().getTimeZone());
        cal.setTimeInMillis(timestamp);
        //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        Date currenTimeZone = cal.getTime();
        return sdf.format(currenTimeZone);
    }


    /**
     * method for conveting utctime to custom time format
     *
     * @param time utc time
     * @return custom time format
     */
    public static String convertUTCToDateFormat(String time) {
        long timestamp = Long.parseLong(time) * 1000;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        sdf.setTimeZone(getInstance().getTimeZone());
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTimeZone(getInstance().getTimeZone());
        cal.setTimeInMillis(timestamp);
        //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        Date currenTimeZone = cal.getTime();
        return sdf.format(currenTimeZone);
    }

    public static String convertUTCToTodayDateFormat(String time) {
        long timestamp = Long.parseLong(time);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        sdf.setTimeZone(getInstance().getTimeZone());
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTimeZone(getInstance().getTimeZone());
        cal.setTimeInMillis(timestamp);
        //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        Date currenTimeZone = cal.getTime();
        return sdf.format(currenTimeZone);
    }


    /**
     * method for conveting utctime to custom time format
     *
     * @param time utc time
     * @return custom time format
     */
    public static String convertUTCToYesterdayDateFormat(String time) {
        long timestamp = Long.parseLong(time);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        sdf.setTimeZone(getInstance().getTimeZone());
        Calendar cal = Calendar.getInstance(Locale.US);
         cal.setTimeZone(getInstance().getTimeZone());
        cal.setTimeInMillis(timestamp);
        //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        cal.add(Calendar.DATE, -1);
        Date currenTimeZone = cal.getTime();
        return sdf.format(currenTimeZone);
    }


    /**
     * method for converting utc time to timeStamp
     *
     * @param time utc time
     * @return timestamp
     */
    public static long convertUTCToTimeStamp(String time) {

        try {
            long timestamp = Long.parseLong(time) * 1000;
            Calendar cal = Calendar.getInstance(Locale.US);
            cal.setTimeZone(getInstance().getTimeZone());
            cal.setTimeInMillis(timestamp);
           // cal.setTimeZone(TimeZone.getTimeZone("GMT"));

            return cal.getTimeInMillis();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * <h2>changeDateTimeFormat</h2>
     *
     * <p>This method is take date or time and with current format and convert it into
     * new provided format then return it.</p>
     *
     * @param dateTime      the date or time in string
     * @param currentFormat the string current format
     * @param newFormat     the string required format
     * @return String the date or time with required format
     */
    public static String changeDateTimeFormat(String dateTime, String currentFormat, String newFormat) {
        if (dateTime == null || dateTime.isEmpty()) {
            return "";
        }

        try {
            if(newFormat.toLowerCase().contains("h")) {
                if (DateFormat.is24HourFormat(AppController.getContext())) {
                    if (newFormat.toLowerCase().contains("hh:mm:ss a")) {
                        newFormat = newFormat.replaceAll("hh:mm:ss", "HH:mm:ss");
                    } else if (newFormat.toLowerCase().contains("hh:mm a")) {
                        newFormat = newFormat.replaceAll("hh:mm a", "HH:mm");
                    }
                } else {
                    if (!newFormat.toLowerCase().contains("hh:mm:ss a")) {
                        newFormat = newFormat.replaceAll("hh:mm:ss", "hh:mm:ss a");
                    } else if (!newFormat.toLowerCase().contains("hh:mm a")) {
                        newFormat = newFormat.replaceAll("hh:mm", "hh:mm a");
                    }
                }
            }

            SimpleDateFormat currentSdf = new SimpleDateFormat(currentFormat, Locale.US);
            SimpleDateFormat newSdf = new SimpleDateFormat(newFormat, Locale.US);
            currentSdf.setTimeZone(AppController.getInstance().getTimeZone());
            newSdf.setTimeZone(AppController.getInstance().getTimeZone());
            Date currentFormatDate = currentSdf.parse(dateTime);
            if (currentFormatDate != null) {
                return newSdf.format(currentFormatDate);
            } else {
                return "";
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

    }


    /**
     * method for returing custome date format
     *
     * @param n         days in month
     * @param monthyear month of the year
     * @return custom date format
     */
    public static String getDayOfMonthSuffix(int n, String monthyear) {
        if (n >= 11 && n <= 13) {
            return n + "th " + monthyear;
        }
        switch (n % 10) {
            case 1:
                return n + "st " + monthyear;
            case 2:
                return n + "nd " + monthyear;
            case 3:
                return n + "rd " + monthyear;
            default:
                return n + "th " + monthyear;
        }
    }

    /**
     * custom method to check internet connectivity
     *
     * @param context context
     * @return boolean: value base on the connectivity
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) return false;
        NetworkInfo.State network = info.getState();
        return (network == NetworkInfo.State.CONNECTED || network == NetworkInfo.State.CONNECTING);
    }
    /*****************************************************/

    /**
     * custom method to check particular is Service running or not
     *
     * @param context context
     * @return boolean: value base on the Running
     */
    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * custom method to check Application is background or foreground
     *
     * @param context context
     * @return boolean: value based on the Running
     */
    public static boolean isApplicationSentToBackground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                for (String activeProcess : processInfo.pkgList) {
                    if (activeProcess.equals(context.getPackageName())) {
                        //If your app is the process in foreground, then it's not in running in background
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * custom method to check Gps connectivity
     *
     * @param context context
     * @return boolean: value base on the connectivity
     */
    public static boolean isGpsEnabled(Context context) {
        LocationManager service = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps = service.isProviderEnabled(LocationManager.GPS_PROVIDER) && service.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return service.isProviderEnabled(LocationManager.GPS_PROVIDER) && service.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    /**
     * Custom method for clearing session and stop the service if its running during logout and session expired
     *
     * @param sessionManager session manager
     * @param context        context
     */
    public static void logoutSessionExiperd(SessionManager sessionManager, Context context) {
        if (Utility.isMyServiceRunning(context, LocationPublishService.class)) {
            Intent stopIntent = new Intent(context, LocationPublishService.class);
            stopIntent.setAction(VariableConstant.ACTION.STOPFOREGROUND_ACTION);
            context.startService(stopIntent);
            getInstance().getMqttHelper().disconnect(/*sessionManager.getPhoneNumber()*/);
        }
        try {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(/*"/topics/" +*/ sessionManager.getFCMTopic());

        } catch (Exception e) {
            e.printStackTrace();
        }
        sessionManager.clearSharedPref();
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = Resources.getSystem().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            locale = Resources.getSystem().getConfiguration().locale;
        }

        if(locale.getLanguage().equals("en"))
        {
            DEFAULT_DISPLAYLANGUAGE  = "English";
            DEFAULT_LANGUAGE = "en";
        }else
        {
            DEFAULT_DISPLAYLANGUAGE  = "Portuguese";
            DEFAULT_LANGUAGE = "pt";
        }

        sessionManager.setLanguageCode(DEFAULT_LANGUAGE);

        Utility.changeLanguageConfig(DEFAULT_LANGUAGE, context);

        Intent intent = new Intent(context, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    /**
     * Custom method for clearing session and stop the service if its running during logout and session expired from fcm message
     *
     * @param sessionManager session manager
     * @param context        context
     */
    public static void logoutSessionExiperd(SessionManager sessionManager, Context context, String msg) {
        if (Utility.isMyServiceRunning(context, LocationPublishService.class)) {
            Intent stopIntent = new Intent(context, LocationPublishService.class);
            stopIntent.setAction(VariableConstant.ACTION.STOPFOREGROUND_ACTION);
            context.startService(stopIntent);
            getInstance().getMqttHelper().disconnect(/*sessionManager.getPhoneNumber()*/);
        }
        String unsubcribeTopic = sessionManager.getFCMTopic();
        sessionManager.clearSharedPref();
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = Resources.getSystem().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            locale = Resources.getSystem().getConfiguration().locale;
        }

        if(locale.getLanguage().equals("en"))
        {
            DEFAULT_DISPLAYLANGUAGE  = "English";
            DEFAULT_LANGUAGE = "en";
        }else
        {
            DEFAULT_DISPLAYLANGUAGE  = "Portuguese";
            DEFAULT_LANGUAGE = "pt";
        }

        sessionManager.setLanguageCode(DEFAULT_LANGUAGE);

        Utility.changeLanguageConfig(DEFAULT_LANGUAGE, context);

        Intent intent = new Intent(context, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        try {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(/*"/topics/" + */unsubcribeTopic);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * method for returning the price
     *
     * @param tempPrice input price
     * @return formated price
     */
    public static String getFormattedPrice(String tempPrice) {
        String price = "";
        Double dPrice = 0.00;
        if (tempPrice != null && !"".equals(tempPrice) && !"0".equals(tempPrice) && !"00".equals(tempPrice)
                && !"0.00".equals(tempPrice) && !"0.0".equals(tempPrice) && !".00".equals(tempPrice) && !"NaN".equals(tempPrice) && !"NA".equals(tempPrice)) {
            DecimalFormat priceFormat = new DecimalFormat("#.00");
            dPrice = Double.parseDouble(tempPrice);
        }

        NumberFormat nf_out = NumberFormat.getInstance(Locale.US);
        nf_out.setMaximumFractionDigits(2);
        nf_out.setMinimumFractionDigits(2);
        nf_out.setGroupingUsed(false);
        price = nf_out.format(dPrice);

        return price;
    }

    public static Double getFormattedDoublePrice(double tempPrice) {
        BigDecimal bd = new BigDecimal(tempPrice).setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    /**
     * @param tempPrice      price
     * @param currencySymbol currency symbol
     * @param currencyAbbr   1 => prefix 2 => suffix
     * @return
     */
    public static String getPrice(String tempPrice, String currencySymbol, String currencyAbbr) {
        String price = getFormattedPrice(tempPrice);
        if (currencyAbbr.equals("1")) {
            price = currencySymbol + " " + price;
        } else {
            price = price + " " + currencySymbol;
        }
        return price;
    }

    public static String roundString(String text, int roundTodecimalPlace) {
        if (text.matches("\\d*\\.?\\d+")) {
            BigDecimal b = new BigDecimal(text);
            b = b.setScale(roundTodecimalPlace, BigDecimal.ROUND_HALF_UP);
            return String.valueOf(b);
        }
        return "0.0";
    }

    /**
     * method for geting the Unix time
     *
     * @return unix time
     */
    public static int GetUnixTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(getInstance().getTimeZone());
        long now = calendar.getTimeInMillis();
        int utc = (int) (now / 1000);
        return (utc);
    }

    /**
     * method for copty stream from inputstream to outputstream
     *
     * @param input  inpurstream
     * @param output outputstream
     * @throws IOException IO exeption
     */
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    /**
     * method for getting the youtubeID from url
     *
     * @param url url of the youtube video
     * @return youtube id
     */
    public static String getYoutubeIdFromUrl(String url) {
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url);

        if (matcher.find()) {
            return matcher.group();
        }
        return "";
    }

    /**
     * method for creating bitmap from vector drawable file
     *
     * @param context    context
     * @param drawableId vector drawable file
     * @return bitmap
     */
    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static Bitmap createDrawableFromView(Context context, View view) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (context != null) {
            if (context.getApplicationContext() != null) {
                ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            }
        }
        view.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    public static void saveImage(Bitmap finalBitmap, File file) {
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Method for calling api for update the bookingAck
     *
     * @param sessionManager
     * @param sessionToken   session token
     * @param bookingid      booking id
     * @param lat            latitude
     * @param lng            longitude
     */
    public static void updateBookingAck(SessionManager sessionManager, final String sessionToken, final String bookingid, final String lat, final String lng) {
        if (lastRefreshTime + 700 < System.currentTimeMillis()) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("bookingId", bookingid);
                jsonObject.put("latitude", lat);
                jsonObject.put("longitude", lng);
            } catch (Exception e) {
                e.printStackTrace();
            }

            NetworkService service = ServiceFactory.getClient(NetworkService.class,AppController.getContext());

            service.bookingAck(sessionToken, DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        if (response != null && !response.isEmpty())

                                            break;
                                    case SESSION_EXPIRED:
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                updateBookingAck(sessionManager, sessionToken, bookingid, lat, lng);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, AppController.getInstance());
                                            }
                                        });
                                        break;
                                    case SESSION_LOGOUT:
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        Utility.logoutSessionExiperd(sessionManager, AppController.getInstance());
                                        break;
                                    default:
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        break;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                        }

                        @Override
                        public void onComplete() {
                        }
                    });

        }
        lastRefreshTime = System.currentTimeMillis();
    }


    public static void hideKeyboad(Activity activity) {
        // Check if no view has focus:
        try {
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static double distanceInMeter(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist * 1000);
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public static void checkAndShowNetworkError(Context context) {
        if (!isNetworkAvailable(context) && !VariableConstant.IS_NETWORK_ERROR_SHOWED) {
            Intent intent = new Intent(context, NetworkErrorActivity.class);
            context.startActivity(intent);
        }
    }

    public static boolean isLatestVersion(String latestVersion) {
        String s1 = normalisedVersion(VariableConstant.APP_VERSION);
        String s2 = normalisedVersion(latestVersion);
        return 0 > s1.compareTo(s2);
    }

    private static String normalisedVersion(String version) {
        String[] split = Pattern.compile(".", Pattern.LITERAL).split(version);
        StringBuilder sb = new StringBuilder();
        for (String s : split) {
            sb.append(String.format("%" + 10 + 's', s));
        }

        return sb.toString();
    }

    public static void showKeyBoard(final Context context, final EditText editText) {
        try {
            editText.postDelayed(() -> {
                // TODO Auto-generated method stub
                InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(editText, 0);
            }, 50);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getCurrentCountryCode(Context context) {
        String locale = "IN";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = context.getResources().getConfiguration().getLocales().get(0).getCountry();
        } else {
            locale = context.getResources().getConfiguration().locale.getCountry();
        }

        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String countryCodeValue = tm.getNetworkCountryIso();

            if (countryCodeValue != null && !countryCodeValue.equals("")) {
                return countryCodeValue.toUpperCase();
            } else {
                return locale;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return locale;
        }
    }

    public static String converArrayListToStringComma(ArrayList<String> stringArrayList) {
        String value = "", prefix = "";
        for (String va : stringArrayList) {
            value = value + prefix + va;
            prefix = ",";
        }
        return value;
    }

    public static void setSpannableString(Context context, TextView txtSpan, String firstString, String lastString) {
        String totalString = firstString + " " + lastString;
        ForegroundColorSpan normalTextColor = new ForegroundColorSpan(ContextCompat.getColor(context, R.color.normalTextColor));
        ForegroundColorSpan colorPrimary = new ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorPrimary));
        Spannable spanText = new SpannableString(totalString);
        spanText.setSpan(normalTextColor, 0, firstString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanText.setSpan(colorPrimary, firstString.length() + 1, totalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSpan.setText(spanText);
    }

    public static void setSpannableString(Context context, TextView txtSpan, String firstString, String lastString, @ColorRes int firstTextColor, @ColorRes int lastTextColor) {
        String totalString = firstString + " " + lastString;
        ForegroundColorSpan normalTextColor = new ForegroundColorSpan(ContextCompat.getColor(context, firstTextColor));
        ForegroundColorSpan colorPrimary = new ForegroundColorSpan(ContextCompat.getColor(context, lastTextColor));
        Spannable spanText = new SpannableString(totalString);
        spanText.setSpan(normalTextColor, 0, firstString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanText.setSpan(colorPrimary, firstString.length() + 1, totalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSpan.setText(spanText);
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static Bitmap getCardBrand(String cardMethod, Context context) {
        Bitmap anImage;
        switch (cardMethod) {
            case "Visa":
            case "visa":
                anImage = CardType.VISA.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.Visa.getIcon());
                break;
            case "MasterCard":
            case "mastercard":
                anImage = CardType.MASTERCARD.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.MasterCard.getIcon());
                break;
            case "American Express":
            case "amex":
                anImage = CardType.AMEX.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.AmericanExpress.getIcon());
                break;
            case "Discover":
            case "discover":
                anImage = CardType.DISCOVER.imageBitmap(context); //getBitmapFromVectorDrawable(context, CardBrand.Discover.getIcon());
                break;
            case "Diners Club":
            case "diners":
                anImage = CardType.DINERSCLUB.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.DinersClub.getIcon());
                break;

            case "JCB":
            case "jcb":
                anImage = CardType.JCB.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.JCB.getIcon());
                break;

            case "unionpay":
                anImage = CardType.MAESTRO.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.UnionPay.getIcon());
                break;

            default:
                anImage = CardType.UNKNOWN.imageBitmap(context);//getBitmapFromVectorDrawable(context, R.drawable.visa_card);
                break;
        }
        return anImage;
    }

/*
    public static int getCardBrand(String brand) {
        switch (brand) {
            case Card.AMERICAN_EXPRESS:
                return com.stripe.android.R.drawable.ic_amex_template_32;

            case Card.DINERS_CLUB:
                return com.stripe.android.R.drawable.ic_diners_template_32;

            case Card.DISCOVER:
                return com.stripe.android.R.drawable.ic_discover_template_32;

            case Card.JCB:
                return com.stripe.android.R.drawable.ic_jcb_template_32;

            case Card.MASTERCARD:
                return com.stripe.android.R.drawable.ic_mastercard_template_32;

            case Card.VISA:
                return com.stripe.android.R.drawable.ic_visa_template_32;

            case Card.UNKNOWN:
                return com.stripe.android.R.drawable.ic_unknown;

            default:
                return com.stripe.android.R.drawable.ic_unknown;
        }
    }
*/

    public static String converSecondToHourMinute(String totalActualJobTimeMinutes) {
        try {
            double t = Double.parseDouble(totalActualJobTimeMinutes);
            int hours = (int) (t / 60); //since both are ints, you get an int
            int minutes = (int) (t % 60);
            return String.format("%02d", hours) + " hrs : " + String.format("%02d", minutes) + " mins";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0 hrs 0 mins";
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());
        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    public static NetworkService basicAuth(NetworkService service) {
        return BasicAuthentication.ServiceGenerator.createService(NetworkService.class,
                BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);
    }


    private static Age calculateAge(Date birthDate)
    {
        int years = 0;
        int months = 0;
        int days = 0;

        //create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeZone(getInstance().getTimeZone());
        birthDay.setTimeInMillis(birthDate.getTime());

        //create calendar object for current day
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeZone(getInstance().getTimeZone());
        now.setTimeInMillis(currentTime);

        //Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;

        //Get difference between months
        months = currMonth - birthMonth;

        //if month difference is in negative then reduce years by one
        //and calculate the number of months.
        if (months < 0)
        {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
        {
            years--;
            months = 11;
        }else{
            months--;
        }

        //Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
        {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        }
        else
        {
            days = 0;
            if (months == 12)
            {
                years++;
                months = 0;
            }
        }
        //Create new Age object
        return new Age(days, months, years);
    }
    public static int getAge(String date) {
        Age age = null;
        try {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(AppController.getInstance().getTimeZone());
        Date birthDate = sdf.parse(date);
            if (birthDate != null) {
                age = calculateAge(birthDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(age != null && age.getYears() == 0)
            return age != null ? age.getMonths() : 0;
        else
        return age != null ? age.getYears() : 0;
    }
  public Gson getGson() {
        return gson;
    }

    /**
     * <h2>getUri_Path</h2>
     * <p>
     *
     * </P>
     */
    public Uri getUri_Path(File file) {
        Uri uri;
        /*
         * Checking if the build version is greater then 25 then no need ask for runtime permission.*/
        //  if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
        //  {
        uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);// }else
        //  {
        //      uri=Uri.fromFile(file);
        //  }
        return uri;
    }

    public boolean isValidVideoSize(long actualVideo) { //byte
        return actualVideo <= VariableConstant.PROFILE_VIDEO_SIZE;
    }

    public boolean isValidDocumentSize(long actualDocSize) { //byte
        return actualDocSize <= VariableConstant.DOCUMENT_SIZE;
    }

    /*
     * Getting the file path*/
    public String getRealPathFromURI(Uri contentUri) {
        String file_path = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                file_path = getFilePath(context, contentUri);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else {
            file_path = getRealPathFromURI_Under19(context, contentUri);
        }
        return file_path;
    }

    @SuppressLint("NewApi")
    private String getFilePath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                //added for crash solution on video pick
                //"raw:/storage/emulated/0/Download/1533826061357.mp4"
                if (id.startsWith("raw:")) {
                    return id.replaceFirst("raw:", "");
                }
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /*
     * Under 19 api.*/
    private String getRealPathFromURI_Under19(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    /**
     * <h2>animateFtomLeft</h2>
     * <p>
     * method to show animation effect from left
     * </p>
     *
     * @param context of the application such as activity/fragment
     */
    public static Animation animateFadeOpen(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.fade_open);
    }

    public static String getMessage(String errorBody) {
        try {
            if (errorBody != null && !errorBody.isEmpty()) {
                JSONObject jsonObject = new JSONObject(errorBody);
                if (!jsonObject.getString(
                        AppConfigData.MESSAGE).isEmpty()) {
                    return jsonObject.getString(AppConfigData.MESSAGE);
                }
            }
            return "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getData(String errorBody) {
        try {
            if (errorBody != null && !errorBody.isEmpty()) {
                JSONObject jsonObject = new JSONObject(errorBody);
                if (!jsonObject.getString(
                        AppConfigData.DATA).isEmpty()) {
                    return jsonObject.getString(AppConfigData.DATA);
                }
            }
            return "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String covertTimeToText(String dataDate) {

        String convTime = null;

        String prefix = "";
        String suffix = "Ago";

        try {
            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
            SimpleDateFormat displayFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            serverFormat.setTimeZone(AppController.getInstance().getTimeZone());
            displayFormat.setTimeZone(AppController.getInstance().getTimeZone());
            Date pasTime = serverFormat.parse(Utility.convertUTCToServerFormat(dataDate));

            Date nowTime = new Date();

            long dateDiff = nowTime.getTime() - pasTime.getTime();

            long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
            long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
            long hour = TimeUnit.MILLISECONDS.toHours(dateDiff);
            long day = TimeUnit.MILLISECONDS.toDays(dateDiff);

            if (second < 60) {
                convTime = second + " Seconds " + suffix;
            } else if (minute < 60) {
                convTime = minute + " Minutes " + suffix;
            } else if (hour < 24) {
                convTime = hour + " Hours " + suffix;
            } else if (day >= 7) {
                if (day > 360) {
                    convTime = (day / 360) + " Years " + suffix;
                } else if (day > 30) {
                    convTime = (day / 30) + " Months " + suffix;
                } else {
                    convTime = (day / 7) + " Week " + suffix;
                }
            } else if (day < 7) {
                convTime = day + " Days " + suffix;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ConvTimeE", e.getMessage());
        }

        return convTime;
    }

    public static float pixelsToSp(float px, Context context) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px / scaledDensity;
    }

    public static String getFormattedDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(getInstance().getTimeZone());
        cal.setTime(date);
        //2nd of march 2015
        int day = cal.get(Calendar.DATE);
        SimpleDateFormat simpleDateFormat;
        if (!((day > 10) && (day < 19)))
            switch (day % 10) {
                case 1: {
                  simpleDateFormat =  new SimpleDateFormat("MMM d'st' yyyy | hh:mm a");
                  simpleDateFormat.setTimeZone(getInstance().getTimeZone());
                    return simpleDateFormat.format(date);
                }
                case 2:
                {
                    simpleDateFormat =  new SimpleDateFormat("MMM d'nd' yyyy | hh:mm a");
                    simpleDateFormat.setTimeZone(getInstance().getTimeZone());
                    return simpleDateFormat.format(date);
                }
                case 3:
                {
                    simpleDateFormat =  new SimpleDateFormat("MMM d'rd' yyyy | hh:mm a");
                    simpleDateFormat.setTimeZone(getInstance().getTimeZone());
                    return simpleDateFormat.format(date);
                }
                default:
                {
                    simpleDateFormat =  new SimpleDateFormat("MMM d'th' yyyy | hh:mm a");
                    simpleDateFormat.setTimeZone(getInstance().getTimeZone());
                    return simpleDateFormat.format(date);
                }
            }
        simpleDateFormat = new SimpleDateFormat("MMM d'th' yyyy | hh:mm a");
        simpleDateFormat.setTimeZone(getInstance().getTimeZone());
        return simpleDateFormat.format(date);
    }
    public static String getFormattedDateOnly(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(getInstance().getTimeZone());
        cal.setTime(date);
        //2nd of march 2015
        int day = cal.get(Calendar.DATE);
        SimpleDateFormat simpleDateFormat;
        if (!((day > 10) && (day < 19)))
            switch (day % 10) {
                case 1: {
                  simpleDateFormat =  new SimpleDateFormat("MMM d'st' yyy");
                  simpleDateFormat.setTimeZone(getInstance().getTimeZone());
                    return simpleDateFormat.format(date);
                }
                case 2:
                {
                    simpleDateFormat =  new SimpleDateFormat("MMM d'nd' yyyy");
                    simpleDateFormat.setTimeZone(getInstance().getTimeZone());
                    return simpleDateFormat.format(date);
                }
                case 3:
                {
                    simpleDateFormat =  new SimpleDateFormat("MMM d'rd' yyyy");
                    simpleDateFormat.setTimeZone(getInstance().getTimeZone());
                    return simpleDateFormat.format(date);
                }
                default:
                {
                    simpleDateFormat =  new SimpleDateFormat("MMM d'th' yyyy");
                    simpleDateFormat.setTimeZone(getInstance().getTimeZone());
                    return simpleDateFormat.format(date);
                }
            }
        simpleDateFormat = new SimpleDateFormat("MMM d'th' yyyy");
        simpleDateFormat.setTimeZone(getInstance().getTimeZone());
        return simpleDateFormat.format(date);
    }

    public static String getFormattedTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(getInstance().getTimeZone());
        cal.setTime(date);
        //2nd of march 2015
        int day = cal.get(Calendar.DATE);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        simpleDateFormat.setTimeZone(getInstance().getTimeZone());
        return simpleDateFormat.format(date);
    }

    private static final float BITMAP_SCALE = 0.4f;
    private static final int BLUR_RADIUS = 8;

    public static Bitmap fastblur(Bitmap sentBitmap) {
        float scale = BITMAP_SCALE;
        int radius = BLUR_RADIUS;
        int width = Math.round(sentBitmap.getWidth() * scale);
        int height = Math.round(sentBitmap.getHeight() * scale);
        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);

    }

    public static RequestOptions createGlideOptionCall(Context mContext) {
        return new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.profile_default_image)
                .error(R.drawable.profile_default_image)
                .transform(new CircleTransform(mContext))
                .priority(Priority.HIGH);

    }

    /**
     * Converts a string to title casing.
     *
     * @param str The string to convert.
     * @return The converted string.
     */
    public static String toTitleCase(String str) {
        if (str == null) {
            return null;
        }
        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }

        return builder.toString();
    }

    /**
     * <p></p>
     *
     * @param context
     * @param progressDialog
     */
    public static void progressDialogShow(Context context, ProgressDialog progressDialog) {
        if (context != null && !((Activity) context).isFinishing() && progressDialog != null && !progressDialog.isShowing())
            progressDialog.show();
    }

    public static void progressDialogDismiss(Context context, ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public static void progressDialogCancel(Context context, ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    public static void setBackgroundColor(Context context, View view, @ColorRes int colorId) {
        if (context != null && view != null && colorId != -1) {
            view.setBackgroundColor(ContextCompat.getColor(context, colorId));
        }
    }

    public static void setTextColor(Context context, TextView view, @ColorRes int colorId) {
        if (context != null && view != null && colorId != -1) {
            view.setTextColor(ContextCompat.getColor(context, colorId));
        }
    }

    /**
     * Creates a simple dialog box with as many buttons as you want
     *
     * @param context     The context of the dialog
     * @param cancelable  whether the dialog can be closed/cancelled by the user
     * @param layoutResID the resource id of the layout you want within the dialog
     * @return the dialog
     */
    public static Dialog getBaseDialog(Context context, boolean cancelable, int layoutResID) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(cancelable);
        dialog.setContentView(layoutResID);

        return dialog;
    }

    /**
     * Returns true if the string is null or 0-length.
     *
     * @param str the string to be examined
     * @return true if str is null or zero length
     */
    public static boolean isTextEmpty(CharSequence str) {
        return str == null || str.toString().trim().length() == 0;
    }

    /**
     * Gets the base name, without extension, of given file name.
     * <p/>
     * e.g. getBaseName("file.txt") will return "file"
     *
     * @param fileName the provided file name
     * @return the base name
     */
    public static String getBaseName(String fileName) {
        int index = fileName.lastIndexOf('.');
        if (index == -1) {
            return fileName;
        } else {
            return fileName.substring(0, index);
        }
    }

    /**
     * @param context
     * @param url
     */
    public static void openFile(Context context, String url) {

        Uri uri = url.startsWith("http") ? Uri.parse(url) : Uri.fromFile(new File(url));
//        Uri uri = url.startsWith("http") ? Uri.parse(url) : Uri.fromFile(new File(url));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", new File(url));
        } else {
            uri = Uri.fromFile(new File(url));
        }

        Utility.printLog("openFIle", "Path : " + uri.toString());
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword");
        } else if (url.toString().contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf");
        } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel");
        } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(uri, "application/x-wav");
        } else if (url.toString().contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf");
        } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav");
        } else if (url.toString().contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif");
        } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg");
        } else if (url.toString().contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain");
        } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
            // Video files
            intent.setDataAndType(uri, "video/*");
        } else {
            //if you want you can also define the intent type for any other file
            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(uri, "*/*");
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(context, "Application not found.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }

    public static BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static String gender(String value){
        String genderStr="Male";
        switch (value)
        {
            case "1":
                genderStr = "Male";
                break;
                case "2":
                genderStr = "FeMale";
                break;
                case "3":
                genderStr = "Other";
                break;
            default:
                genderStr = "Male";
                break;
        }
        return genderStr;
    }
    /**
     * <h2>changeLanguageConfig</h2>
     * used to set the language configuration
     *
     * @param code language code
     */
    public static int changeLanguageConfig(String code, Context context) {
        Configuration configuration = context.getResources().getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLayoutDirection(new Locale(code));
            Log.d("TAG", " language direction " + configuration.getLayoutDirection());
        }
        configuration.locale = new Locale(code);
        context.getResources().updateConfiguration(configuration,
                context.getResources().getDisplayMetrics());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return configuration.getLayoutDirection();
        }
        return 0;
    }

    public static boolean isFromIndia(Context context) {
        return /*getDeviceCountryCode(context).equals("in") ||*/ RAZORPAY_KEY.length() > 0;
    }

    private static String getDeviceCountryCode(Context context) {
        String countryCode;

        // Try to get country code from TelephonyManager service
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if(tm != null) {
            // Query first getSimCountryIso()
            countryCode = tm.getSimCountryIso();
            if (countryCode != null && countryCode.length() == 2)
                return countryCode.toLowerCase();

            if (tm.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
                // Special case for CDMA Devices
                countryCode = getCDMACountryIso();
            }
            else {
                // For 3G devices (with SIM) query getNetworkCountryIso()
                countryCode = tm.getNetworkCountryIso();
            }

            if (countryCode != null && countryCode.length() == 2)
                return countryCode.toLowerCase();
        }

        // If network country not available (tablets maybe), get country code from Locale class
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            countryCode = context.getResources().getConfiguration().getLocales().get(0).getCountry();
        }
        else {
            countryCode = context.getResources().getConfiguration().locale.getCountry();
        }

        if (countryCode != null && countryCode.length() == 2)
            return  countryCode.toLowerCase();

        // General fallback to "us"
        return "us";
    }

    @SuppressLint("PrivateApi")
    private static String getCDMACountryIso() {
        try {
            // Try to get country code from SystemProperties private class
            Class<?> systemProperties = Class.forName("android.os.SystemProperties");
            Method get = systemProperties.getMethod("get", String.class);

            // Get homeOperator that contain MCC + MNC
            String homeOperator = ((String) get.invoke(systemProperties,
                    "ro.cdma.home.operator.numeric"));

            // First three characters (MCC) from homeOperator represents the country code
            int mcc = Integer.parseInt(homeOperator.substring(0, 3));

            // Mapping just countries that actually use CDMA networks
            switch (mcc) {
                case 330: return "PR";
                case 310: return "US";
                case 311: return "US";
                case 312: return "US";
                case 316: return "US";
                case 283: return "AM";
                case 460: return "CN";
                case 455: return "MO";
                case 414: return "MM";
                case 619: return "SL";
                case 450: return "KR";
                case 634: return "SD";
                case 434: return "UZ";
                case 232: return "AT";
                case 204: return "NL";
                case 262: return "DE";
                case 247: return "LV";
                case 255: return "UA";
            }
        }
        catch (ClassNotFoundException ignored) {
        }
        catch (NoSuchMethodException ignored) {
        }
        catch (IllegalAccessException ignored) {
        }
        catch (InvocationTargetException ignored) {
        }
        catch (NullPointerException ignored) {
        }

        return null;
    }
}