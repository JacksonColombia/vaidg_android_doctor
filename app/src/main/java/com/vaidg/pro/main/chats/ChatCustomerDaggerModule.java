package com.vaidg.pro.main.chats;

import com.vaidg.pro.dagger.FragmentScoped;
import com.vaidg.pro.main.booking.BookingContract;
import com.vaidg.pro.main.booking.BookingPresenter;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>LogInDaggerModule</h1>
 * <p>This dagger module created for LogInActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
@Module
public abstract class ChatCustomerDaggerModule {

    @FragmentScoped
    @Binds
    abstract ChatCustomerContract.Presenter getPresenter(ChatCutomerPresenter presenter);

   /* @FragmentScoped
    @Binds
    abstract BookingContract.View provideView(BookingFragment fragment);
*/
}
