package com.vaidg.pro.landing.newSignup.selectCity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivitySelectCityBinding;
import com.vaidg.pro.landing.newSignup.selectCity.model.CitySelectionAdapter;
import com.vaidg.pro.landing.newSignup.selectCity.model.CitySelectionListener;
import com.vaidg.pro.landing.newSignup.selectCity.model.SelectCityModel;
import com.vaidg.pro.pojo.signup.CityData;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * <h1>SelectCityActivity</h1>
 * <p>This is first get cities through API and load it as an tap to select list.</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 12/06/2020
 **/
public class SelectCityActivity extends BaseDaggerActivity implements SelectCityContract.View {

    @Inject
    CitySelectionAdapter citySelectionAdapter;

    @Inject
    SelectCityContract.Presenter presenter;
    @Inject
    SelectCityModel model;
    private ActivitySelectCityBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySelectCityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter.getCity();
        initViews();
        initRv();
    }

    @Override
    public void initViews() {
        binding.includeToolbar.tvTitle.setText(getString(R.string.select_city));
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        citySelectionAdapter.provideCallBack((CitySelectionListener) presenter);
    }


    private void initRv() {
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        itemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_light, null));
        binding.rvSelectCity.addItemDecoration(itemDecoration);
        binding.rvSelectCity.setAdapter(citySelectionAdapter);
        binding.svCity.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.trim().length() > 0)
                    filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.trim().length() > 0)
                    filter(newText);
                return true;
            }
        });

        binding.svCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.svCity.setIconified(false);
            }
        });

        binding.svCity.postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.svCity.setIconified(false);
                binding.svCity.clearFocus();
            }
        }, 300);
    }


    @Override
    public void notifyAdapter() {
        citySelectionAdapter.notifyDataSetChanged();
    }

    private void filter(String text) {
        // creating a new array list to filter our data.
        ArrayList<CityData> filteredlist = new ArrayList<>();

        // running a for loop to compare elements.
        for (CityData item : model.getCityData()) {
            // checking if the entered string matched with any item of our recycler view.
            if (item.getCity().toLowerCase().contains(text.toLowerCase())) {
                // if the item is matched we are
                // adding it to our filtered list.
                filteredlist.add(item);
            }
        }
        if (filteredlist.isEmpty()) {
            // if no item is added in filtered list we are
            // displaying a toast message as no data found.
            binding.rvSelectCity.setVisibility(View.INVISIBLE);
            Toast.makeText(this, "No Data Found..", Toast.LENGTH_SHORT).show();
        } else {
            binding.rvSelectCity.setVisibility(View.VISIBLE);
            // at last we are passing that filtered
            // list to our adapter class.
            citySelectionAdapter.filterList(filteredlist);
        }
    }

    @Override
    public void showError() {
        showError(getString(R.string.serverError));
    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clSelectCityActivity, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void finishActivity(Intent intent) {
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        hideProgress();
    }

    @Override
    public void showProgress() {
        runOnUiThread(() -> binding.pbLoader.setVisibility(View.VISIBLE));
    }

    @Override
    public void hideProgress() {
        runOnUiThread(() -> binding.pbLoader.setVisibility(View.GONE));
    }
}
