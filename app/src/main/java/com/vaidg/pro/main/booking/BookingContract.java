package com.vaidg.pro.main.booking;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface BookingContract {

    interface View extends BaseView {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String msg);

        void onFailure();

        void onSuccesBooking(String result, boolean isFromApi);

        void onSuccessUpdateProviderStatus(String msg);

        void onNewToken(String token);

        void sessionExpired(String msg);

    }

    interface Presenter extends BasePresenter<View> {
        void updateProviderStatues(final String status, final String lat, final String lng);

        void getBooking(final boolean isBackground);

        void detach();
    }
}