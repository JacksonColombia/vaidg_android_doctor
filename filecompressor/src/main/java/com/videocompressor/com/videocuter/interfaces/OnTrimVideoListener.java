package com.videocompressor.com.videocuter.interfaces;

import android.net.Uri;

/**
 * @author Suresh.
 * @version 1.0.
 * @since 1-01-2018
 */
public interface OnTrimVideoListener {
    void onTrimStarted();

    void getResult(final Uri uri);

    void cancelAction();

    void onError(final String message);
}
