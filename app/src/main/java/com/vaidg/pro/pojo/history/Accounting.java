package com.vaidg.pro.pojo.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 27-Dec-17.
 */

public class Accounting implements Serializable {

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("cancellationFee")
    @Expose
    private String cancellationFee;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("disctotalount")
    @Expose
    private String disctotalount;
    @SerializedName("appEarning")
    @Expose
    private String appEarning;
    @SerializedName("providerEarning")
    @Expose
    private String providerEarning;
    @SerializedName("pgCommissionApp")
    @Expose
    private String pgCommissionApp;
    @SerializedName("pgCommissionProvider")
    @Expose
    private String pgCommissionProvider;
    @SerializedName("totalPgCommission")
    @Expose
    private String totalPgCommission;
    @SerializedName("appEarningPgComm")
    @Expose
    private String appEarningPgComm;
    @SerializedName("paymentMethod")
    @Expose
    private String paymentMethod;
    @SerializedName("chargeId")
    @Expose
    private String chargeId;
    @SerializedName("last4")
    @Expose
    private String last4;
    @SerializedName("appCommission")
    @Expose
    private String appCommission;
    @SerializedName("visitFee")
    @Expose
    private String visitFee;
    @SerializedName("travelFee")
    @Expose
    private String travelFee;
    @SerializedName("lastDues")
    @Expose
    private String lastDues;
    @SerializedName("paidByWallet")
    @Expose
    private String paidByWallet;
    @SerializedName("totalActualJobTimeMinutes")
    @Expose
    private String totalActualJobTimeMinutes;
    @SerializedName("totalActualHourFee")
    @Expose
    private String totalActualHourFee;
    @SerializedName("captureAmount")
    @Expose
    private String captureAmount;
    @SerializedName("remainingAmount")
    @Expose
    private String remainingAmount;
    @SerializedName("bidCredit")
    @Expose
    private String bidCredit;
    @SerializedName("bidPrice")
    @Expose
    private String bidPrice;
    @SerializedName("totalShiftBooking")
    @Expose
    private String totalShiftBooking;
    @SerializedName("razorPayPaymentType")
    @Expose
    private String razorPayPaymentType;

    @Override
    public String toString() {
        return "{" +
                "amount='" + amount + '\'' +
                ", cancellationFee='" + cancellationFee + '\'' +
                ", discount='" + discount + '\'' +
                ", total='" + total + '\'' +
                ", disctotalount='" + disctotalount + '\'' +
                ", appEarning='" + appEarning + '\'' +
                ", providerEarning='" + providerEarning + '\'' +
                ", pgCommissionApp='" + pgCommissionApp + '\'' +
                ", pgCommissionProvider='" + pgCommissionProvider + '\'' +
                ", totalPgCommission='" + totalPgCommission + '\'' +
                ", appEarningPgComm='" + appEarningPgComm + '\'' +
                ", paymentMethod='" + paymentMethod + '\'' +
                ", chargeId='" + chargeId + '\'' +
                ", last4='" + last4 + '\'' +
                ", appCommission='" + appCommission + '\'' +
                ", visitFee='" + visitFee + '\'' +
                ", travelFee='" + travelFee + '\'' +
                ", lastDues='" + lastDues + '\'' +
                ", paidByWallet='" + paidByWallet + '\'' +
                ", totalActualJobTimeMinutes='" + totalActualJobTimeMinutes + '\'' +
                ", totalActualHourFee='" + totalActualHourFee + '\'' +
                ", captureAmount='" + captureAmount + '\'' +
                ", remainingAmount='" + remainingAmount + '\'' +
                ", bidCredit='" + bidCredit + '\'' +
                ", bidPrice='" + bidPrice + '\'' +
                ", totalShiftBooking='" + totalShiftBooking + '\'' +
                ", razorPayPaymentType='" + razorPayPaymentType + '\'' +
                '}';
    }

    public String getRazorPayPaymentType() {
        return razorPayPaymentType;
    }

    public void setRazorPayPaymentType(String razorPayPaymentType) {
        this.razorPayPaymentType = razorPayPaymentType;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCancellationFee() {
        return cancellationFee;
    }

    public void setCancellationFee(String cancellationFee) {
        this.cancellationFee = cancellationFee;
    }

    public String getDisctotalount() {
        return disctotalount;
    }

    public void setDisctotalount(String disctotalount) {
        this.disctotalount = disctotalount;
    }

    public String getAppEarning() {
        return appEarning;
    }

    public void setAppEarning(String appEarning) {
        this.appEarning = appEarning;
    }

    public String getProviderEarning() {
        return providerEarning;
    }

    public void setProviderEarning(String providerEarning) {
        this.providerEarning = providerEarning;
    }

    public String getPgCommissionApp() {
        return pgCommissionApp;
    }

    public void setPgCommissionApp(String pgCommissionApp) {
        this.pgCommissionApp = pgCommissionApp;
    }

    public String getPgCommissionProvider() {
        return pgCommissionProvider;
    }

    public void setPgCommissionProvider(String pgCommissionProvider) {
        this.pgCommissionProvider = pgCommissionProvider;
    }

    public String getTotalPgCommission() {
        return totalPgCommission;
    }

    public void setTotalPgCommission(String totalPgCommission) {
        this.totalPgCommission = totalPgCommission;
    }

    public String getAppEarningPgComm() {
        return appEarningPgComm;
    }

    public void setAppEarningPgComm(String appEarningPgComm) {
        this.appEarningPgComm = appEarningPgComm;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getChargeId() {
        return chargeId;
    }

    public void setChargeId(String chargeId) {
        this.chargeId = chargeId;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }

    public String getAppCommission() {
        return appCommission;
    }

    public void setAppCommission(String appCommission) {
        this.appCommission = appCommission;
    }

    public String getVisitFee() {
        return visitFee;
    }

    public void setVisitFee(String visitFee) {
        this.visitFee = visitFee;
    }

    public String getTravelFee() {
        return travelFee;
    }

    public void setTravelFee(String travelFee) {
        this.travelFee = travelFee;
    }

    public String getLastDues() {
        return lastDues;
    }

    public void setLastDues(String lastDues) {
        this.lastDues = lastDues;
    }

    public String getPaidByWallet() {
        return paidByWallet;
    }

    public void setPaidByWallet(String paidByWallet) {
        this.paidByWallet = paidByWallet;
    }

    public String getTotalActualJobTimeMinutes() {
        return totalActualJobTimeMinutes;
    }

    public void setTotalActualJobTimeMinutes(String totalActualJobTimeMinutes) {
        this.totalActualJobTimeMinutes = totalActualJobTimeMinutes;
    }

    public String getTotalActualHourFee() {
        return totalActualHourFee;
    }

    public void setTotalActualHourFee(String totalActualHourFee) {
        this.totalActualHourFee = totalActualHourFee;
    }

    public String getCaptureAmount() {
        return captureAmount;
    }

    public void setCaptureAmount(String captureAmount) {
        this.captureAmount = captureAmount;
    }

    public String getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(String remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public String getBidCredit() {
        return bidCredit;
    }

    public void setBidCredit(String bidCredit) {
        this.bidCredit = bidCredit;
    }

    public String getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(String bidPrice) {
        this.bidPrice = bidPrice;
    }

    public String getTotalShiftBooking() {
        return totalShiftBooking;
    }

    public void setTotalShiftBooking(String totalShiftBooking) {
        this.totalShiftBooking = totalShiftBooking;
    }
}
