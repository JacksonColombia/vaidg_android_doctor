package com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection;

import android.app.Activity;

import com.vaidg.pro.R;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.model.DocumentSelectionModel;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.utility.Utility;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

public class DocumentSelectionPresenterImple implements DocumentSelectionContract.Presenter {

    @Inject
    Activity activity;
    @Inject
    DocumentSelectionModel model;
    @Inject
    Utility utility;

    private DocumentSelectionContract.View view;

    @Inject
    DocumentSelectionPresenterImple() {

    }

    @Override
    public void addPreDefinedData(List<PreDefined> list) {
        model.addPreDefinedData(list);
        view.notifyAdapter();
    }

    @Override
    public void setData(int position, String data) {
        model.setData(position, data);
        view.notifyAdapter(position, model.getData().size());
    }

    @Override
    public void deleteDocument(int position) {
        model.deleteDocument(position);
        view.notifyAdapter(position, model.getData().size());
    }

    @Override
    public boolean isValidDocumentSize(String filePath) {
        boolean valid = false;
        try {
            File tempFile = new File(filePath);
            valid = utility.isValidDocumentSize(tempFile.length());
            if (!valid) {
                if (view != null)
                    view.showError(activity.getString(R.string.errorDocumentSizeLimit));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valid;
    }

    @Override
    public void saveData(Attribute attribute) {
        model.saveData(attribute);
    }

    @Override
    public boolean isMandatoryFilled() {
        for (PreDefined preDefined : model.getData()) {
            if (preDefined.getIsManadatory() == 1 && (preDefined.getData() == null || preDefined.getData().isEmpty())) {
                view.showError(activity.getString(R.string.argIsRequired, preDefined.getName()));
                return false;
            }
        }
        return true;
    }

    @Override
    public void clearData() {
        model.clearData();
    }

    @Override
    public void attachView(DocumentSelectionContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
