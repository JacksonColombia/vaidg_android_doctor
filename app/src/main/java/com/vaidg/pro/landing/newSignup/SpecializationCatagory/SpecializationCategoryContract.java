package com.vaidg.pro.landing.newSignup.SpecializationCatagory;

/*
 * <h1>@SpecializationCategoryContract</h1>
 * <p>this is interface b.w view and presenter </P>
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/

import com.vaidg.pro.pojo.Attribute;

public interface SpecializationCategoryContract {

    interface View {

        /**
         * <h2>@hidePreview</h2>
         * <p>this is used hide the initial fragment used for preview</p>
         */
        void hidePreview();

        /**
         * <h2?>@openNextFrag</h2?>
         * <p>rthis is used to open  next fragment for next category type</p>
         *
         * @param FromList
         * @param next
         * @param updatedAttribute
         */
        void openNextFrag(int FromList, int next, Attribute updatedAttribute);

        /**
         * <h2>@showError</h2>
         * <p>this is used to display error</p>
         *
         * @param message
         */
        void showError(String message);
    }

    interface Presenter {

        /**
         * <h2>@hidePreview</h2>
         * <p>this is used hide the initial fragment used for preview</p>
         */
        void hidePreview();

        /**
         * <h2?>@openNextFrag</h2?>
         * <p>rthis is used to open  next fragment for next category type</p>
         *
         * @param from_list
         * @param next
         * @param updatedAttribute
         */
        void openNextFrag(int from_list, int next, Attribute updatedAttribute);

        /**
         * <h2>@showError</h2>
         * <p>this is used to display error</p>
         *
         * @param message
         */
        void showError(String message);
    }
}


