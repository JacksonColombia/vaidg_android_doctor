package com.vaidg.pro.landing.newSignup.selectSpecialization.model;

import com.vaidg.pro.BaseModel;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationPojo;
import com.vaidg.pro.pojo.signup.CityData;
import com.vaidg.pro.pojo.signup.CityPojo;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import javax.inject.Inject;

public class SelectSpecializationModel extends BaseModel {

    @Inject
    Utility utility;
    @Inject
    ArrayList<SelectSpecializationData> arrayList;
    @Inject
    int position;
    @Inject
    public SelectSpecializationModel() {
    }

    public void selectCategory(int position) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (position == i)
                arrayList.get(i).setSelected(true);
            else
                arrayList.get(i).setSelected(false);
        }
    }
    /**
     * <p>It's change selected city flag true and rest all city false in cityData.</p>
     *
     * @param pos the selected city object position
     * @param name the selected city object name
     */
    public void selectCategory(int pos,String name) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).getCatName().equals(name)) {
                setPosition(i);
                getSelectedCategory(i);
                arrayList.get(i).setSelected(true);
            }  else
                arrayList.get(i).setSelected(false);
        }
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void parseSpecializationData(SelectSpecializationPojo specializationPojo) {
        arrayList.addAll(specializationPojo.getData());
    }

    public SelectSpecializationData getSelectedCategory(int position) {
        return arrayList.get(position);
    }

    /**
     * This method is deserializes the specified city response into the {@link CityPojo} class.
     *
     */
    public ArrayList<SelectSpecializationData> getSelectSpecializationData() {
        return arrayList;
    }
    public boolean isCheck() {
        for (SelectSpecializationData specializationData : arrayList) {
            if (specializationData.isSelected())
                return true;
        }
        return false;
    }
}
