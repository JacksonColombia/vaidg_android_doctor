package com.vaidg.pro.pojo.profile.calltype;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 10-Sep-17.
 */

public class CallTypeData implements Serializable {
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("inCall")
    @Expose
    private String inCall;
    @SerializedName("outCall")
    @Expose
    private String outCall;
    @SerializedName("teleCall")
    @Expose
    private String teleCall;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getInCall() {
        return inCall;
    }

    public void setInCall(String inCall) {
        this.inCall = inCall;
    }

    public String getOutCall() {
        return outCall;
    }

    public void setOutCall(String outCall) {
        this.outCall = outCall;
    }

    public String getTeleCall() {
        return teleCall;
    }

    public void setTeleCall(String teleCall) {
        this.teleCall = teleCall;
    }
}
