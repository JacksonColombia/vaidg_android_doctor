package com.vaidg.pro.pojo.booking;


import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DependentData implements Serializable
{

  @SerializedName("dependentId")
  @Expose
  private String dependentId;
  @SerializedName("firstName")
  @Expose
  private String firstName;
  @SerializedName("lastName")
  @Expose
  private String lastName;
  @SerializedName("profilePic")
  @Expose
  private String profilePic;
  @SerializedName("relationship")
  @Expose
  private String relationship;
  @SerializedName("countryCode")
  @Expose
  private String countryCode;
  @SerializedName("countryCodeSymbol")
  @Expose
  private String countryCodeSymbol;
  @SerializedName("phone")
  @Expose
  private String phone;
  @SerializedName("gender")
  @Expose
  private int gender;
  @SerializedName("dateOfBirth")
  @Expose
  private String dateOfBirth;
  @SerializedName("age")
  @Expose
  private String age;
  @SerializedName("deleted")
  @Expose
  private boolean deleted;

  public String getDependentId() {
    return dependentId;
  }

  public void setDependentId(String dependentId) {
    this.dependentId = dependentId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getProfilePic() {
    return profilePic;
  }

  public void setProfilePic(String profilePic) {
    this.profilePic = profilePic;
  }

  public String getRelationship() {
    return relationship;
  }

  public void setRelationship(String relationship) {
    this.relationship = relationship;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getCountryCodeSymbol() {
    return countryCodeSymbol;
  }

  public void setCountryCodeSymbol(String countryCodeSymbol) {
    this.countryCodeSymbol = countryCodeSymbol;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public int getGender() {
    return gender;
  }

  public void setGender(int gender) {
    this.gender = gender;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

  public String getAge() {
    return age;
  }

  public void setAge(String age) {
    this.age = age;
  }
}