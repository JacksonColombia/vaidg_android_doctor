package com.vaidg.pro.landing.introsilider2;

import com.vaidg.pro.pojo.introslider.SliderData;
import com.vaidg.pro.pojo.language.LanguageData;

import java.util.ArrayList;

/**
 * <h1>IntroSliderContract</h1>
 * <p>This contract define interface with required methods for view and presenter</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @see IntroSliderNewActivity
 * @see IntroSliderPresenter
 * @since 08/06/2020
 **/
public interface IntroSliderContract {

    interface View {
        /**
         * <h2>initViews</h2>
         */
        void initViews();

        /**
         * <h2>setupSlider</h2>
         */
        void setupSlider();

        /**
         * <h2>setSliderContent</h2>
         */
        void setSliderContent(SliderData sliderData);

        /**
         * <h2>onClick</h2>
         *
         * @param view
         */
        void onClick(android.view.View view);

        /**
         * <h2>redirectToLogin</h2>
         */
        void redirectToLogin();

        /**
         * <h2>redirectToSignUp</h2>
         */
        void redirectToSignUp();

        void onSuccess(ArrayList<LanguageData> result);
    }

    interface Presenter {

        void addSliderData();

        void changeSliderData(int position);

        void getLanguage();
    }
}
