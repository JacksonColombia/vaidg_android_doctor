package com.kotlintestgradle.cache

import com.kotlintestgradle.cache.dao.LoginDao

/**
 * @author 3Embed
 *
 *all functions for CURD operations
 *
 * @since 1.0 (23-Aug-2019)
 */
interface DatabaseManager {
    fun login(): LoginDao
}