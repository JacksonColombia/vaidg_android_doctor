package com.vaidg.pro.main.profile.bank.banknewstripe;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import java.io.File;
import org.json.JSONObject;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface BankNewStripeContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onSuccess(String msg);

    void onFailure();

    void onFailure(String msg);

    void ipAddress(String ip);

    void onErrorImageUpload();

    void onErrorFirstName();

    void onErrorLastName();

    void onErrorEmail();

    void onErrorDob();

    void onErrorSSN();

    void onErrorAddress();

    void onErrorCity();

    void onErrorCountry();

    void onErrorState();

    void onErrorZipcode();

    void getImage(String imageUrl);
  }

  interface Presenter extends BasePresenter {

    /**
     * execute the getIpAddress() class
     */
    void getIp();

    /**
     * method for passing values from view to model
     * @param jsonObject     required field in json object
     * @param isPictureTaken boolen for picture is taken or not
     * @param amazonS3       object of UploadFileAmazonS3 class
     * @param mFileTemp      file that need to upload in amazon
     */
    void addBankDetails(JSONObject jsonObject, boolean isPictureTaken, UploadFileAmazonS3 amazonS3, File mFileTemp);

    /**
     * mehtod for uploading image in amazon
     *
     * @param amazonS3  object of UploadFileAmazonS3 class
     * @param mFileTemp file that need to upload in amazon
     */
    void amazonUpload(UploadFileAmazonS3 amazonS3, File mFileTemp);
  }
}