package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.pojo.profile.wallet.WalletTransDetails;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.vaidg.pro.AppController.getInstance;

/**
 * <h1>WalletTransactionsAdapter</h1>
 * This class is used to inflate the wallet transactions list
 *
 * @since 20/09/17.
 */
public class WalletTransactionsAdapter extends RecyclerView.Adapter<WalletTransactionsAdapter.WalletViewHolder> {

    Typeface fontMedium;
    Typeface fontRegular;
    private SessionManager sessionManager;
    private Context mContext;
    private ArrayList<WalletTransDetails> transactionsAL;

    /**
     * <h2>WalletTransactionsAdapter</h2>
     * This is the constructor of our adapter.
     */
    public WalletTransactionsAdapter(Context context, ArrayList<WalletTransDetails> _transactionsAL) {
        this.mContext = context;
        this.sessionManager = SessionManager.getSessionManager(context);
        this.transactionsAL = _transactionsAL;

        fontMedium = Utility.getFontMedium(context);
        fontRegular = Utility.getFontRegular(context);

    }

    @Override
    public WalletTransactionsAdapter.WalletViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewList = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_wallet_transactions, parent, false);
        return new WalletTransactionsAdapter.WalletViewHolder(viewList);

    }

    @Override
    public void onBindViewHolder(WalletViewHolder walletViewHolder, final int position) {
        WalletTransDetails walletDataDetailsItem = transactionsAL.get(position);

        if (walletDataDetailsItem.getTxnType().equalsIgnoreCase("DEBIT")) {
            Utility.setBackgroundColor(mContext, walletViewHolder.iv_wallet_transaction_arrow, R.color.customRed);
        } else {
            Utility.setBackgroundColor(mContext, walletViewHolder.iv_wallet_transaction_arrow, R.color.verifiedBank);
            walletViewHolder.iv_wallet_transaction_arrow.setRotation(180);
        }
        walletViewHolder.tv_wallet_transactionId.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_12), mContext));
        walletViewHolder.tv_wallet_transaction_bid.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_12), mContext));
        walletViewHolder.tv_wallet_transaction_date.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_12), mContext));
        walletViewHolder.tv_wallet_transaction_description.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        walletViewHolder.tv_wallet_transaction_amount.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        walletViewHolder.tv_wallet_transactionId.setText(
                mContext.getString(R.string.transactionID) + " " + walletDataDetailsItem.getTxnId().trim());

        if (walletDataDetailsItem.getTripId().isEmpty() || walletDataDetailsItem.getTripId().equalsIgnoreCase("N/A")) {
            walletViewHolder.tv_wallet_transaction_bid.setVisibility(View.GONE);
        } else {
            walletViewHolder.tv_wallet_transaction_bid.setText(mContext.getString(R.string.bookingID)  + " " + walletDataDetailsItem.getTripId());
        }

        walletViewHolder.tv_wallet_transaction_amount.setText(Utility.getPrice(walletDataDetailsItem.getAmount().trim(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

        walletViewHolder.tv_wallet_transaction_description.setText(walletDataDetailsItem.getTrigger().trim());
        walletViewHolder.tv_wallet_transaction_date.setText(epochTimeConverter(walletDataDetailsItem.getTimestamp().trim()));
    }

    @Override
    public int getItemCount() {
        return transactionsAL.size();
    }
    //====================================================================

    /**
     * <h2>epochTimeConverter</h2>
     * <p>
     * method to convert received epoch seconds into formatted date time string
     * </p>
     *
     * @param milliSecsString input the milli sec
     * @return returns the date in the format dd MMM yyyy, hh:mm a
     */
    private String epochTimeConverter(String milliSecsString) {
        if (milliSecsString.isEmpty()) {
            return "";
        }
        long milliSecs = Long.parseLong(milliSecsString);
        Date date = new Date(milliSecs * 1000);
      //  DateFormat format = new SimpleDateFormat("dd MMM yyyy, hh:mm a", Locale.US);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        format.setTimeZone(getInstance().getTimeZone());
        //format.setTimeZone(TimeZone.getTimeZone("US"));
        return format.format(date);
    }
    //====================================================================

    /**
     * <h1>ListViewHolder</h1>
     * <p>
     * This method is used to hold the views
     * </p>
     */
    class WalletViewHolder extends RecyclerView.ViewHolder {
        TextView tv_wallet_transactionId, tv_wallet_transaction_bid, tv_wallet_transaction_amount;
        TextView tv_wallet_transaction_description, tv_wallet_transaction_date;
        ImageView iv_wallet_transaction_arrow;

        WalletViewHolder(View cellView) {
            super(cellView);
            tv_wallet_transactionId = (TextView) cellView.findViewById(R.id.tv_wallet_transactionId);
            tv_wallet_transactionId.setTypeface(fontRegular);

            tv_wallet_transaction_bid = (TextView) cellView.findViewById(R.id.tv_wallet_transaction_bid);
            tv_wallet_transaction_bid.setTypeface(fontRegular);

            tv_wallet_transaction_amount = (TextView) cellView.findViewById(R.id.tv_wallet_transaction_amount);
            tv_wallet_transaction_amount.setTypeface(fontMedium);

            tv_wallet_transaction_description = (TextView) cellView.findViewById(R.id.tv_wallet_transaction_description);
            tv_wallet_transaction_description.setTypeface(fontRegular);

            tv_wallet_transaction_date = (TextView) cellView.findViewById(R.id.tv_wallet_transaction_date);
            tv_wallet_transaction_date.setTypeface(fontRegular);

            iv_wallet_transaction_arrow = (ImageView) cellView.findViewById(R.id.iv_wallet_transaction_arrow);
        }
    }
    //================================================================/
}
