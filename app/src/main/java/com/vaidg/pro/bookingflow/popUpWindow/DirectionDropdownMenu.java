package com.vaidg.pro.bookingflow.popUpWindow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.DirectionDropdownAdapter;
import com.vaidg.pro.pojo.addMedication.Direction;
import java.util.ArrayList;

public class DirectionDropdownMenu extends PopupWindow {
  private Context context;
  private RecyclerView rvCategory;
  private ArrayList<Direction> units = new ArrayList<>();
  private DirectionDropdownAdapter dropdownAdapter;

  public DirectionDropdownMenu(Context context, ArrayList<Direction> units) {
    super(context);
    this.context = context;
    this.units = units;
    setupView(units);
  }

  public void setCategorySelectedListener(DirectionDropdownAdapter.CategorySelectedListener categorySelectedListener) {
    dropdownAdapter.setCategorySelectedListener(categorySelectedListener);
  }

  private void setupView(ArrayList<Direction> units) {
    View view = LayoutInflater.from(context).inflate(R.layout.item_popup_category, null);
    rvCategory = view.findViewById(R.id.rvCategory);
    rvCategory.setHasFixedSize(true);
    rvCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
    // rvCategory.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
    dropdownAdapter = new DirectionDropdownAdapter(units);
    rvCategory.setAdapter(dropdownAdapter);
    setContentView(view);
  }
}