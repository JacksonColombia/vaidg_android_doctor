package com.vaidg.pro.pojo.callpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PostCallResponse implements Serializable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private PostCallData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PostCallData getData() {
        return data;
    }

    public void setData(PostCallData data) {
        this.data = data;
    }

    public class PostCallData implements Serializable{
        @SerializedName("callId")
        @Expose
        String callId;

        public String getCallId() {
            return callId;
        }

        public void setCallId(String callId) {
            this.callId = callId;
        }

    }

}