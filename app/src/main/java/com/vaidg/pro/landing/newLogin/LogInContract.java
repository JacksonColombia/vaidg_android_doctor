package com.vaidg.pro.landing.newLogin;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.utility.encryptiondecryption.CryptographyExample;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilKeyPair;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;

/**
 * <h1>LogInContract</h1>
 * <p>This contract define interface with required methods for view and presenter</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @see NewLogInActivity
 * @see LogInPresenter
 * @since 06/06/2020
 **/
public interface LogInContract {

    interface View extends BaseView {

        /**
         * <p>This method is used to initialize views</p>
         */
        void initViews();

        /**
         * <p>This method is define view click events</p>
         *
         * @param view the clicked view
         */
        void onClick(android.view.View view);

        /**
         * <p>This method is provide boolean flag based on remember me checkbox selection</p>
         *
         * @return the boolean flag of remember me
         */
        boolean isRememberMe();

        /**
         * <p>This method is used to perform action when entered emailPhone in incorrect</p>
         */
        void onEmailPhoneInCorrect();

        /**
         * <p>This method is used to perform action when entered emailPhone in correct</p>
         */
        void onEmailPhoneCorrect();

        /**
         * <p>This method is used to perform action when entered password in incorrect</p>
         */
        void onPasswordInCorrect();

        /**
         * <p>This method is used to perform action when entered password in correct</p>
         */
        void onPasswordCorrect();

        /**
         * This method is display passed error message in snack bar.
         *
         * @param error the error message
         */
        void showError(String error);

        /**
         * <p>This method is used for notify to activity when get success in login api.<p>
         */
        void onSuccess();

        /**
         * <p>This method is redirect user to help with password screen.</p>
         */
        void launchHelpWithPassword();

        /**
         * <p>This method is redirect user to sign up screen.</p>
         */
        void launchSignUp();
        /**
         * <h2>checkForVersion</h2>
         * This method is used to check for version of the device
         */
        void checkForVersion();
    }

    interface Presenter extends BasePresenter {

        /**
         * <p>This method is verify entered phone or email address and notify to view.</p>
         *
         * @param emailPhone the entered phone or email
         * @return the true if email id valid or false
         */
        boolean verifyEmailPhone(String emailPhone);

        /**
         * <p>This method is validate entered password and notify to view.</p>
         *
         * @param password the entered password
         * @return the true if password id valid or false
         */
        boolean verifyPassword(String password);

        /**
         * <p>This method is take entered emailPhone, password and call login api</p>
         *
         * @param emailPhone the entered email or phone
         * @param password   the entered password
         */
        void logIn(String emailPhone, String password);


        /**
         * <h2>checkForKeystore</h2>
         * This method is used to check for keystore
         */
        void checkForKeystore();

        /**
         * <h2>generateEncryptionKeyPostLollipop</h2>
         * This method is used to generate the encryption key required for encryption for post lollipop
         *
         * @param exportedPublicKey
         */
        void generateEncryptionPublicKeyPostLollipop(String exportedPublicKey);

        /**
         * <h2>generateEncryptionKeyPostLollipop</h2>
         * This method is used to generate the encryption key required for encryption for post lollipop
         *
         * @param exportedPrivateKey
         */
        void generateEncryptionPrivateKeyPostLollipop(String exportedPrivateKey);

        /**
         * <h2>generateEncryptionKeyPostLollipop</h2>
         * This method is used to generate the encryption key required for encryption for pre marshmallow
         *
         * @param exportedPublicKey
         */
        void generateEncryptionPublicKeyPreMarshMallow(String exportedPublicKey);

        /**
         * <h2>generateEncryptionKeyPostLollipop</h2>
         * This method is used to generate the encryption key required for encryption for pre marshmallow
         *
         * @param exportedPrivateKey
         */
        void generateEncryptionPrivateKeyPreMarshMallow(String exportedPrivateKey);

        /**
         * <h2>encryptPrivateKeyPostLollipop</h2>
         * This method is used to encrypt the data for post lollipop
         *
         * @param input data to be encrypted
         */
        void encryptPrivateKeyPostLollipop(String input);

        /**
         * <h2>encryptPrivateKeyPostLollipop</h2>
         * This method is used to encrypt the data for pre marshmallow
         *
         * @param input data to be encrypted
         */
        void encryptPrivateKeyPreMarshMallow(String input);

        /**
         * <h2>encryptPublicKeyPostLollipop</h2>
         * This method is used to encrypt the data for post lollipop
         *
         * @param input data to be encrypted
         */
        void encryptPublicKeyPostLollipop(String input);

        /**
         * <h2>encryptPublicKeyPostLollipop</h2>
         * This method is used to encrypt the data for pre marshmallow
         *
         * @param input data to be encrypted
         */
        void encryptPublicKeyPreMarshMallow(String input);

        /**
         * <p>This method is get isRemember value from the session manager and return it here.</p>
         *
         * @return the boolean value
         */
        boolean getIsRemembered();

        /**
         * <p>This method is get user name from the session manager and return it here.</p>
         *
         * @return the string user name
         */
        String getUserName();

        /**
         * <p>This method is get password from the session manager and return it here.</p>
         *
         * @return the string password
         */
        String getPassword();

        /**
         * <h2>checkForVersion</h2>
         * This method is used to check for version of the device
         * @param cryptographyExample
         * @param crypto
         * @param keyPair
         */
        void checkForVersion(CryptographyExample cryptographyExample, VirgilCrypto crypto, VirgilKeyPair keyPair);
    }
}
