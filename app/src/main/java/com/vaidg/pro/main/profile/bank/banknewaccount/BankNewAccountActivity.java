package com.vaidg.pro.main.profile.bank.banknewaccount;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.hbb20.CountryCodePicker;
import com.vaidg.pro.R;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import dagger.android.support.DaggerAppCompatActivity;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.json.JSONObject;

/**
 * Created by murashid on 26-Aug-17.
 * <h1>BankNewAccountActivity</h1>
 * BankNewAccountActivity activity for adding new bank account
 */


public class BankNewAccountActivity extends DaggerAppCompatActivity implements BankNewAccountContract.View {

    private EditText etName, etEmail, etAccountNo, etRoutingNo, etCountry;
    private TextInputLayout tilName, tilEmail, tilAccountNo, tilRoutingNo, tilCountry;
    private ProgressDialog progressDialog;
    @Inject
    BankNewAccountContract.Presenter presenter;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_new_account);

        initViews();
    }

    /**
     * initilize the views
     */
    private void initViews() {
        sessionManager = SessionManager.getSessionManager(this);
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_x);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.addNewAccount));
        tvTitle.setTypeface(fontBold);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.saving));
        progressDialog.setCancelable(false);

        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        etAccountNo = findViewById(R.id.etAccountNo);
        etRoutingNo = findViewById(R.id.etRoutingNo);
        etCountry = findViewById(R.id.etCountry);
        etName.setTypeface(fontRegular);
        etEmail.setTypeface(fontRegular);
        etAccountNo.setTypeface(fontRegular);
        etRoutingNo.setTypeface(fontRegular);
        etCountry.setTypeface(fontRegular);

        tilName = findViewById(R.id.tilName);
        tilEmail = findViewById(R.id.tilEmail);
        tilAccountNo = findViewById(R.id.tilAccountNo);
        tilRoutingNo = findViewById(R.id.tilRoutingNo);
        tilCountry = findViewById(R.id.tilCountry);

        MaterialButton tvSave = findViewById(R.id.btnDone);
        tvSave.setText(getString(R.string.save));
        tvSave.setTypeface(fontRegular);

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addBankDetails();
            }
        });

        etName.setText(sessionManager.getFirstName() + " " + sessionManager.getLastName());
        etEmail.setText(sessionManager.getEmail());


        final CountryCodePicker ccp = findViewById(R.id.ccp);

        etCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ccp.launchCountrySelectionDialog();
            }
        });


        etCountry.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ccp.launchCountrySelectionDialog();
                }
            }
        });


        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                etCountry.setText(ccp.getSelectedCountryNameCode());
            }
        });


        try {
            ccp.setCountryForPhoneCode(Integer.parseInt(sessionManager.getCountryCode().substring(1)));
            etCountry.setText(ccp.getSelectedCountryNameCode());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addBankDetails() {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("email", etEmail.getText().toString().trim());
            jsonObject.put("account_number", etAccountNo.getText().toString().trim());
            jsonObject.put("routing_number", etRoutingNo.getText().toString().trim());
            jsonObject.put("account_holder_type", "individual");
            jsonObject.put("account_holder_name", etName.getText().toString().trim());
            jsonObject.put("country", etCountry.getText().toString().trim());
            jsonObject.put("currency",sessionManager.getCurrency().trim());

            presenter.addBankDetails(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNameError() {
        resetTile(tilName, getString(R.string.enterAccountHoldername));
    }

    @Override
    public void onErrorEmail() {
        resetTile(tilEmail, getString(R.string.invalidEmail));
    }

    @Override
    public void onAccountNumberError() {
        resetTile(tilAccountNo, getString(R.string.enterAccountNo));
    }

    @Override
    public void onRoutingNumberError() {
        resetTile(tilRoutingNo, getString(R.string.enterRoutinNo));
    }

    @Override
    public void onCountryError() {
        resetTile(tilCountry, getString(R.string.enterCountry));
    }

    @Override
    public void onNoError() {
        resetTile(null, "");
    }

    /**
     * set the error for empty field
     *
     * @param textInputLayout empty field
     * @param errorMsg        error msg
     */
    void resetTile(TextInputLayout textInputLayout, String errorMsg) {
        tilName.setErrorEnabled(false);
        tilEmail.setErrorEnabled(false);
        tilAccountNo.setErrorEnabled(false);
        tilEmail.setErrorEnabled(false);

        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(errorMsg);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }


    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onSuccess(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        closeActivity();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    private void closeActivity() {
        Utility.progressDialogCancel(this, progressDialog);
        finish();
        overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }
}
