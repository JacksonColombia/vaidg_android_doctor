package com.vaidg.pro.changepassword;

import android.app.Activity;

import com.vaidg.pro.R;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;
import static com.vaidg.pro.utility.VariableConstant.USER_TYPE;

/**
 * <h1>ChangePasswordPresenterImple</h1>
 * <p>This class is contain override method of presenter with business logic.</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 24/06/2020
 **/
public class ChangePasswordPresenterImple implements ChangePasswordContract.Presenter {

    @Inject
    ChangePasswordContract.View view;
    @Inject
    Activity activity;
    @Inject
    SessionManager sessionManager;
    @Inject
    NetworkService service;
    @Inject
    NetworkStateHolder networkStateHolder;

    @Inject
    public ChangePasswordPresenterImple() {
    }

    @Override
    public boolean isValidPassword(String password) {
        String regex = "(.)*(\\d)(.)*";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(password);

        if (password.length() < 7) {
            return false;
        } else if (password.equals(password.toLowerCase())) {
            return false;
        } else if (password.equals(password.toUpperCase())) {
            return false;
        } else return matcher.matches();
    }

    @Override
    public void forgotPassword(String userId, String password, String confirmPassword) {

        Utility.hideKeyboad(activity);

        if (!isValidPassword(password)) {
            view.onNewPasswordError();
            return;
        } else if (confirmPassword.isEmpty()) {
            view.onConfirmPasswordError();
            return;
        } else if (!password.equals(confirmPassword)) {
            view.onPasswordMismatch();
            return;
        }

        if (networkStateHolder.isConnected()) {
            if (view != null)
            view.showProgress();
            Map<String, Object> params = new HashMap<>();
            params.put("userId", userId);
            params.put("userType", USER_TYPE);
            params.put("password", password);

            Utility.basicAuth(service).resetPassword(DEFAULT_LANGUAGE, PLATFORM, params)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                if (statusCode == SUCCESS_RESPONSE) {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.onSuccess(Utility.getMessage(response));
                                    }
                                } else {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.showError(Utility.getMessage(errorBody));
                                    }
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError(activity.getString(R.string.serverError));
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.showError(activity.getString(R.string.serverError));
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void changePassword(String oldPassword, String newPassword, String confirmPassword) {

        Utility.hideKeyboad(activity);

        if (oldPassword.equals("")) {
            view.onOldPasswordError();
            return;
        } else if (!isValidPassword(newPassword)) {
            view.onNewPasswordError();
            return;
        } else if (confirmPassword.equals("")) {
            view.onConfirmPasswordError();
            return;
        } else if (!newPassword.equals(confirmPassword)) {
            view.onPasswordMismatch();
            return;
        }

        if (networkStateHolder.isConnected()) {
            if (view != null)
            view.showProgress();
            Map<String, Object> params = new HashMap<>();
            params.put("oldPassword", oldPassword);
            params.put("newPassword", newPassword);

            service.updatePassword(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, params)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        if (view != null)
                                        view.hideProgress();
                                        sessionManager.setPassword(newPassword);
                                        if (view != null)
                                        view.onSuccess(Utility.getMessage(response));
                                    case SESSION_EXPIRED:
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                changePassword(oldPassword, newPassword, confirmPassword);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                if (view != null)
                                                {
                                                    view.hideProgress();
                                                }
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, activity);
                                            }
                                        });
                                        break;
                                    case SESSION_LOGOUT:
                                        if (view != null)
                                        view.hideProgress();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        Utility.logoutSessionExiperd(sessionManager, activity);
                                        break;
                                    default:
                                        if (view != null) {
                                            view.hideProgress();
                                            view.showError(Utility.getMessage(errorBody));
                                        }
                                        break;
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError(activity.getString(R.string.serverError));
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.showError(activity.getString(R.string.serverError));
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }

}
