package com.vaidg.pro.utility;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityWebViewBinding;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>WebViewActivity</h1>
 * WebViewActivity for displaying the webview
 */
public class WebViewActivity extends AppCompatActivity {

    private ActivityWebViewBinding binding;

    private ProgressDialog progressDialog;
    private String url, title;
    private boolean isFromSupport;
    private String TAG = "WebViewActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityWebViewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);

        url = getIntent().getStringExtra("URL");
        title = getIntent().getStringExtra("title");
        isFromSupport = getIntent().getBooleanExtra("isFromSupport", false);

        initActionBar();
        initializeViews();

    }

    /**********************************************************************************************/
    /**
     * <h1>initActionBar</h1>
     * initilize the action bar
     */
    private void initActionBar() {
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        binding.includeToolbar.tvTitle.setTypeface(Utility.getFontBold(this));
        TextView tvSubTitle = findViewById(R.id.tvSubTitle);
        binding.includeToolbar.tvTitle.setText(title);
        if (isFromSupport) {
            binding.includeToolbar.tvTitle.setText(getString(R.string.faq));
            tvSubTitle.setTypeface(Utility.getFontMedium(this));
            tvSubTitle.setText(title);
            tvSubTitle.setVisibility(View.VISIBLE);
        }

    }

    /**********************************************************************************************/

    /**
     * initialize the views
     */
    @SuppressLint("SetJavaScriptEnabled")
    private void initializeViews() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(true);

        WebView webView = findViewById(R.id.webView);

        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(false);
        webView.getSettings().setDomStorageEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Utility.progressDialogShow(WebViewActivity.this, progressDialog);
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                Utility.progressDialogDismiss(WebViewActivity.this, progressDialog);
            }

        });

        webView.loadUrl(url);

    }

    @Override
    public void onBackPressed() {
        Utility.progressDialogCancel(this, progressDialog);
        finish();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }
}
