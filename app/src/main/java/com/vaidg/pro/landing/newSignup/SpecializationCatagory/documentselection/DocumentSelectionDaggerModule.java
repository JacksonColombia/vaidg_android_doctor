package com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection;

import com.vaidg.pro.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

@Module
public interface DocumentSelectionDaggerModule {

    @FragmentScoped
    @Binds
    DocumentSelectionContract.Presenter providePresenter(DocumentSelectionPresenterImple documentSelectionPresenterImple);
}
