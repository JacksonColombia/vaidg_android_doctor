package com.vaidg.pro.pojo.profile.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 14-Sep-17.
 */

public class ReviewPojo implements Serializable {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ReviewData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReviewData getData() {
        return data;
    }

    public void setData(ReviewData data) {
        this.data = data;
    }
}
