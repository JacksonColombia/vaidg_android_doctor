package com.vaidg.pro.utility.exception;

/**
 * <h1>@NoMoreDataException</h1>
 * <p>custom exception</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 30/04/2020.
 **/
public class NoMoreDataException extends Exception {
    public NoMoreDataException() {
        super();
    }

    public NoMoreDataException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
