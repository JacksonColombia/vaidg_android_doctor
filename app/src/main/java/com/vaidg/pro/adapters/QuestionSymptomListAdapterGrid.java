package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.pro.R;
import com.vaidg.pro.pojo.booking.SymptomQuestionAnswer;
import com.vaidg.pro.utility.Utility;
import java.util.ArrayList;

/**
 * Created by Ali on 7/24/2018.
 */
public class QuestionSymptomListAdapterGrid extends RecyclerView.Adapter<QuestionSymptomListAdapterGrid.MyViewHolder> {

  private Context mContext;
  private ArrayList<SymptomQuestionAnswer> questionAndAnswer;
  private Typeface fontRegular;

  public QuestionSymptomListAdapterGrid(Context context, ArrayList<SymptomQuestionAnswer> bidQuestionAnswers) {
    this.mContext = context;
    this.questionAndAnswer = bidQuestionAnswers;
    fontRegular = Utility.getFontRegular(mContext);

  }

  @NonNull
  @Override
  public QuestionSymptomListAdapterGrid.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_question_answer_list, parent, false);
    mContext = parent.getContext();
    return new MyViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(@NonNull QuestionSymptomListAdapterGrid.MyViewHolder holder, int position) {
    int pos = position;
    holder.tvJobAnswers.setText((pos + 1) + "-" + questionAndAnswer.size() + ". " + questionAndAnswer.get(position).getSymptomName());
    if(questionAndAnswer.get(position).getSymptomAnswers() != null && questionAndAnswer.get(position).getSymptomAnswers().size() > 0) {
      QuestionSymptomAdapterGrid questionSymptomAdapterGrid = new QuestionSymptomAdapterGrid(mContext, questionAndAnswer.get(position).getSymptomAnswers());
      holder.recyclerViewQuestionImage.setLayoutManager(new LinearLayoutManager(mContext));
      holder.recyclerViewQuestionImage.setAdapter(questionSymptomAdapterGrid);
    }
  }

  @Override
  public int getItemCount() {
    return questionAndAnswer == null ? 0 : questionAndAnswer.size();
  }


   class MyViewHolder extends RecyclerView.ViewHolder {
    TextView tvJobAnswers;
    RecyclerView recyclerViewQuestionImage;

    MyViewHolder(View itemView) {
      super(itemView);
      recyclerViewQuestionImage = itemView.findViewById(R.id.recyclerViewQuestionImage);
      tvJobAnswers = itemView.findViewById(R.id.tvJobAnswers);
      tvJobAnswers.setTypeface(fontRegular);
      tvJobAnswers.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_15));
    }
  }
}
