package com.vaidg.pro.main.chats;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.adapters.ChatCustomerListAdapter;
import com.vaidg.pro.pojo.chat.ChatCutomerList;
import com.vaidg.pro.utility.SessionManager;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatCustomerListFragment extends Fragment implements ChatCustomerListAdapter.CustomerClickListener {

    private static final String ARG_PARAM1 = "param1";
    private SessionManager sessionManager;
    private RelativeLayout rlNoChatFound;
    private RecyclerView rvChatCustomerList;
    private ChatCustomerListAdapter chatCustomerListAdapter;
    private ArrayList<ChatCutomerList> chatCutomerLists, itemsCopy;
    private boolean isPastChat;
    private int selectedPosition = 0;

    public static ChatCustomerListFragment newInstance(boolean isPastChat) {
        ChatCustomerListFragment fragment = new ChatCustomerListFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, isPastChat);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isPastChat = getArguments().getBoolean(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_chat_customer_list, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        sessionManager = SessionManager.getSessionManager(getActivity());
        chatCutomerLists = new ArrayList<>();
        itemsCopy = new ArrayList<>();
        chatCustomerListAdapter = new ChatCustomerListAdapter(getActivity(), chatCutomerLists, this);
        rvChatCustomerList = view.findViewById(R.id.rvChatCustomerList);
        rlNoChatFound = view.findViewById(R.id.rlNoChatFound);
        rvChatCustomerList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvChatCustomerList.setAdapter(chatCustomerListAdapter);

    }

    void notifyiDataChanged(ArrayList<ChatCutomerList> chatCutomerLists) {

        rlNoChatFound.setVisibility(View.GONE);
        rvChatCustomerList.setVisibility(View.VISIBLE);
        this.chatCutomerLists.clear();
        this.chatCutomerLists.addAll(chatCutomerLists);
        chatCustomerListAdapter.notifyDataSetChanged();

        this.itemsCopy.clear();
        this.itemsCopy.addAll(chatCutomerLists);
    }
    void noChatFound() {
        rlNoChatFound.setVisibility(View.VISIBLE);
        rvChatCustomerList.setVisibility(View.GONE);
    }

    public void filter(String query) {
        if (chatCutomerLists != null) {
            chatCutomerLists.clear();
        }

        if (query.isEmpty()) {
            if (chatCutomerLists != null) {
                chatCutomerLists.addAll(itemsCopy);
            }
        } else {
            query = query.toLowerCase();
            for (ChatCutomerList item : itemsCopy) {
                if (item.getFirstName().toLowerCase().contains(query)) {
                    chatCutomerLists.add(item);
                } else if (!item.getLastName().equals("") && item.getLastName().toLowerCase().contains(query)) {
                    chatCutomerLists.add(item);
                } else if (!item.getLastName().equals("") && (item.getFirstName().toLowerCase() + " " + item.getLastName().toLowerCase()).contains(query)) {
                    chatCutomerLists.add(item);
                }
            }
        }
        if(chatCutomerLists != null && chatCutomerLists.size() > 0)
        chatCustomerListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCustomerClick(int position) {
        selectedPosition = position;
        sessionManager.setChatBookingID(chatCutomerLists.get(position).getBookingId());
        sessionManager.setChatCustomerName(chatCutomerLists.get(position).getFirstName() + " " + chatCutomerLists.get(position).getLastName());
        sessionManager.setChatCustomerPic(chatCutomerLists.get(position).getProfilePic());
        sessionManager.setChatCustomerID(chatCutomerLists.get(position).getCustomerId());


        Intent chatIntent = new Intent(getActivity(), ChattingActivity.class);
        chatIntent.putExtra("isPastChat", isPastChat);
        chatIntent.putExtra("isActiveChat", !isPastChat);
        startActivity(chatIntent);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isPastChat) {
            chatCustomerListAdapter.notifyItemChanged(selectedPosition);
        }
    }

    public void notifyDataSetChanged() {
        chatCustomerListAdapter.notifyDataSetChanged();
    }
}
