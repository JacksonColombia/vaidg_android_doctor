package com.vaidg.pro.landing.newSignup.SpecializationCatagory;

import android.app.Activity;
import android.content.Context;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.landing.newSignup.selectSpecialization.model.SpecializationSelectionAdapter;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.MediaBottomSelector;
import com.videocompressor.com.CompressImage;
import com.videocompressor.com.VideoCompressor;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/*
 * <h1>@SpecializationCategoryUtil</h1>
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
@Module
public class SpecializationCategoryUtil {

    @Provides
    @ActivityScoped
    App_permission getApp_permission(Activity activity) {
        return new App_permission(activity);
    }

    @Provides
    @ActivityScoped
    MediaBottomSelector getMediaBottomSelector(Activity activity) {
        return new MediaBottomSelector(activity);
    }

    @ActivityScoped
    @Provides
    CompressImage compressImage() {
        return new CompressImage();
    }

    @ActivityScoped
    @Provides
    VideoCompressor getVideoCompressor(Context context) {
        return new VideoCompressor(context);
    }

    @ActivityScoped
    @Provides
    SpecializationSelectionAdapter provideSpecializationSelectionAdapter(ArrayList<SelectSpecializationData> arrayList) {
        return new SpecializationSelectionAdapter(arrayList);
    }


}
