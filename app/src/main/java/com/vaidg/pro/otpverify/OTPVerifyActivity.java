package com.vaidg.pro.otpverify;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.vaidg.pro.R;
import com.vaidg.pro.changepassword.ChangePasswordActivity;
import com.vaidg.pro.databinding.ActivityOtpVerifyBinding;
import com.vaidg.pro.main.MainActivity;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

/**
 * Created by murashid on 11-Sep-17.
 * <h1>OTPVerifyActivity</h1>
 * OTPVerifyActivity Activity is used to enter the otp for follwing option
 * otpOption 1 => Signup verificaiton, 2 => Forgot Password Verification , 3 => Change Number
 */

public class OTPVerifyActivity extends BaseDaggerActivity implements OTPVerifyContract.View, View.OnClickListener, TextWatcher {

    @Inject
    OTPVerifyContract.Presenter presenter;

    private ActivityOtpVerifyBinding binding;

    private ProgressDialog progressDialog;

    private String phone, countryCode, userId;
    private int otpOption = 0;

    private int expireOtp = 60;
    private int normalTextColor, lightestTextColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOtpVerifyBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
        initViewOnClickListeners();
        initViewTextChangeListeners();
        startTimer(expireOtp);

    }

    /**
     * <h1>initializeViews</h1>
     * <p>this is the method, for initialize the views</p>
     */
    private void initViews() {

        normalTextColor = ContextCompat.getColor(OTPVerifyActivity.this, R.color.normalTextColor);
        lightestTextColor = ContextCompat.getColor(OTPVerifyActivity.this, R.color.lightestTextColor);

        Intent intent = getIntent();
        phone = intent.getStringExtra("phone");
        countryCode = intent.getStringExtra("countryCode");
        userId = intent.getStringExtra("userId");
        expireOtp = intent.getIntExtra("expireOtp", 60);
        otpOption = intent.getIntExtra("otpOption", 0);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.verifyphoneNumber));
        progressDialog.setCancelable(false);

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontMedium = Utility.getFontMedium(this);

        binding.includeToolbar.tvTitle.setText(getString(R.string.verifyYourNumber));
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        binding.etOtp1.setTypeface(fontRegular);
        binding.etOtp2.setTypeface(fontRegular);
        binding.etOtp3.setTypeface(fontRegular);
        binding.etOtp4.setTypeface(fontRegular);

        binding.tvTimer.setTypeface(fontRegular);
        binding.tvResendCode.setTypeface(fontMedium);
        binding.tvVerifyMsg.setTypeface(fontBold);
        binding.tvVerifyMsg2.setTypeface(fontBold);
        binding.btnVerify.setTypeface(fontBold);
        binding.etOtp1.requestFocus();

        binding.tvVerifyMsg2.setText(TextUtils.concat("(", countryCode, "-", phone, ")", getString(R.string.verify_msg2)));

    }

    private void initViewOnClickListeners() {
        binding.btnVerify.setOnClickListener(this);
    }

    private void initViewTextChangeListeners() {
        binding.etOtp1.addTextChangedListener(this);
        binding.etOtp2.addTextChangedListener(this);
        binding.etOtp3.addTextChangedListener(this);
        binding.etOtp4.addTextChangedListener(this);
    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboad(this);
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    /**********************************************************************************************/
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnVerify:
                progressDialog.setMessage(getString(R.string.verifyphoneNumber));
                verifyOtp();
                break;

            case R.id.tvResendCode:
                progressDialog.setMessage(getString(R.string.resendingCode));
                presenter.resendOtp(userId, otpOption);
                break;

        }
    }

    /**
     * <h1>OTPValidation</h1>
     * <p>this is the method is call from when the OTP verify is click,
     * the method first check whether the otp entered or not if the OTP is empty then will show the error Toast message,
     * else the service call. the service call used for forgot password , Signup time and change phone number time also.
     * </p>
     */
    private void verifyOtp() {
        String otp = TextUtils.concat(binding.etOtp1.getText() == null ? "" : binding.etOtp1.getText().toString(),
                binding.etOtp2.getText() == null ? "" : binding.etOtp2.getText().toString(),
                binding.etOtp3.getText() == null ? "" : binding.etOtp3.getText().toString(),
                binding.etOtp4.getText() == null ? "" : binding.etOtp4.getText().toString()).toString();
        presenter.verifyOtp(userId, otpOption, otp);
    }

    private void startTimer(long j) {

        final long finalTime = j;
        new CountDownTimer(finalTime * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                long seconds = millisUntilFinished / 1000;
                if (seconds % 2 == 0) {
                    Utility.setTextColor(OTPVerifyActivity.this, binding.tvTimer, R.color.normalTextColor);
                } else {
                    Utility.setTextColor(OTPVerifyActivity.this, binding.tvTimer, R.color.lightestTextColor);
                }
                binding.tvTimer.setText(getDurationString(seconds));
            }

            public void onFinish() {
                binding.tvResendCode.setOnClickListener(OTPVerifyActivity.this);
                Utility.setTextColor(OTPVerifyActivity.this, binding.tvResendCode, R.color.darkTextColor);
                binding.tvTimer.setText("");
            }

        }.start();

    }

    /**
     * method for returning hours and minutes from seconds
     *
     * @param seconds second
     * @return hours and minutes
     */
    String getDurationString(long seconds) {
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        int hours = (int) (TimeUnit.SECONDS.toHours(seconds) - (day * 24));
        int minute = (int) (TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60));
        int second = (int) (TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60));

        return String.format(Locale.US, "%02d:%02d:%02d", hours, minute, second);
    }

    @Override
    public void onSuccessChangeNumberOTP(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        VariableConstant.IS_PROFILE_EDITED = true;
        onBackPressed();

    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clParent, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void showProgress() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void hideProgress() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onSuccessResendCode(String msg) {
        binding.tvResendCode.setOnClickListener(null);
        Utility.setTextColor(this, binding.tvResendCode, R.color.lightestTextColor);
        startTimer(expireOtp);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onSuccessSignUpOtp() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            startActivity(intent);
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }
        finish();
    }


    @Override
    public void onSuccessForgotPasswordOTP(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        intent.putExtra("phone", phone);
        intent.putExtra("countryCode", countryCode);
        intent.putExtra("userId", userId);
        intent.putExtra("isForgotPassword", true);
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        finish();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        try {
            View et = getCurrentFocus();
            if (count > before) {
                if (getCurrentFocus() != null) et = getCurrentFocus().focusSearch(View.FOCUS_RIGHT);
            } else {
                if (getCurrentFocus() != null) et = getCurrentFocus().focusSearch(View.FOCUS_LEFT);
            }

            if (et instanceof EditText) {
                et.requestFocus();
                if (et.getId() == R.id.etOtp4 && ((EditText) et).getText().toString().length() > 0) {
                    verifyOtp();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        View et = getCurrentFocus();
        if (et != null && et.getId() == R.id.etOtp4 && ((EditText) et).getText().toString().length() > 0) {
            verifyOtp();
        }
    }
}
