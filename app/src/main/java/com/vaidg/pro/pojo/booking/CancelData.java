package com.vaidg.pro.pojo.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 08-Nov-17.
 */

public class CancelData implements Serializable {
    @SerializedName("res_id")
    @Expose
    private String res_id;
    @SerializedName("isChecked")
    @Expose
    private boolean isChecked = false;
    @SerializedName("reason")
    @Expose
    private String reason;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getRes_id() {
        return res_id;
    }

    public void setRes_id(String res_id) {
        this.res_id = res_id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}