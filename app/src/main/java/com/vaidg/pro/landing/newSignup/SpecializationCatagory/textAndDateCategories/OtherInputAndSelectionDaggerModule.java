package com.vaidg.pro.landing.newSignup.SpecializationCatagory.textAndDateCategories;

import com.vaidg.pro.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>OtherInputAndSelectionDaggerModule</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
@Module
public interface OtherInputAndSelectionDaggerModule {

    @FragmentScoped
    @Binds
    OtherInputAndSelectionContract.Presenter providePresenter(OtherInputAndSelectionPresenter presenter);
}
