package com.vaidg.pro.otpverify;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;

import java.util.Map;

/**
 * <h1>OTPVerifyContract</h1>
 * <p>This contract define interface with required methods for view and presenter</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @see OTPVerifyActivity
 * @see OTPVerifyPresenterImple
 * @since 24/06/2020
 **/
public interface OTPVerifyContract {

    interface View extends BaseView {

        /**
         * <p>This method is called when otp verified successfully for change mobile number.</p>
         *
         * @param msg the success message come in response
         */
        void onSuccessChangeNumberOTP(String msg);

        /**
         * <p>This method is called when otp verified successfully for forgot password.</p>
         *
         * @param msg the success message come in response
         */
        void onSuccessForgotPasswordOTP(String msg);

        /**
         * <p>This method is called when otp verified successfully for signup mobile number verification.</p>
         */
        void onSuccessSignUpOtp();

        /**
         * <p>This method is called when OTP regenerated successfully and send to registered mobile number.</p>
         *
         * @param msg the success message come in response
         */
        void onSuccessResendCode(String msg);

        /**
         * <p>This method is display passed error message in snack bar.</p>
         *
         * @param error the error message
         */
        void showError(String error);

    }

    interface Presenter extends BasePresenter {

        /**
         * <p>This method is call API for regenerating OTP and send it to registered mobile number.</p>>
         *
         * @param userId    the user id
         * @param otpOption the option for otp type
         *                  1 - SignUp verification
         *                  2 - Forgot password verification
         *                  3- Change mobile number
         */
        void resendOtp(String userId, int otpOption);

        /**
         * <p>This method is take entered otp and call API to verify it.</p>
         *
         * @param userId    the user id
         * @param otpOption the option for otp type
         *                  1 - SignUp verification
         *                  2 - Forgot password verification
         *                  3- Change mobile number
         * @param otp       the entered otp
         */
        void verifyOtp(String userId, int otpOption, String otp);

        /**
         * <p>This method is call API to verify OTP for new registered mobile number.</p>
         *
         * @param params the collection of request payload
         */
        void verifyPhoneNumber(Map<String, Object> params);

        /**
         * <p>This method is call API to verify OTP for change mobile number or forgot password.</p>
         *
         * @param params    the collection of request payload
         * @param otpOption the option for otp type
         *                  1 - SignUp verification
         *                  2 - Forgot password verification
         *                  3- Change mobile number
         */
        void verifyVerificationCode(Map<String, Object> params, int otpOption);
    }
}
