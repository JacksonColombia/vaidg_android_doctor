package com.vaidg.pro.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.main.profile.bank.bankdetails.BankDetailsActivity;
import com.vaidg.pro.main.profile.mydocument.MyDocumentUploadActivity;
import com.vaidg.pro.pojo.profile.document.ProfileDocumentCategory;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * Created by murashid on 20-Mar-17.
 * <h1>BankListAdapter</h1>
 * BankList Recycler View adapter for bank list
 *
 * @see BankDetailsActivity
 */

public class MyDocumentCategoryAdapter extends RecyclerView.Adapter<MyDocumentCategoryAdapter.ViewHolder> implements View.OnClickListener {

    private static final String TAG = "CategoryListAdapter";
    private ArrayList<ProfileDocumentCategory> profileDocumentCategories;
    private Typeface fontRegular;
    private Activity mActivity;

    public MyDocumentCategoryAdapter(Activity mActivity, ArrayList<ProfileDocumentCategory> profileDocumentCategories) {
        this.profileDocumentCategories = profileDocumentCategories;
        this.mActivity = mActivity;
        fontRegular = Utility.getFontRegular(mActivity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyDocumentCategoryAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.select_category_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvCategory.setText(profileDocumentCategories.get(position).getCategoryName());
        holder.tvCategory.setOnClickListener(this);
        holder.tvCategory.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return profileDocumentCategories != null ? profileDocumentCategories.size() : ZERO;
    }

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position = viewHolder.getAdapterPosition();
        switch (v.getId()) {
            case R.id.tvCategory:
                Intent intent = new Intent(mActivity, MyDocumentUploadActivity.class);
                intent.putExtra("category", profileDocumentCategories.get(position));

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    mActivity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(mActivity).toBundle());
                } else {
                    mActivity.startActivity(intent);
                    mActivity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
                break;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvCategory.setTypeface(fontRegular);
        }
    }
}
