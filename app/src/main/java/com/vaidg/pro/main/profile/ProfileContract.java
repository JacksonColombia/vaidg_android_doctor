package com.vaidg.pro.main.profile;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.language.LanguageData;
import com.vaidg.pro.pojo.profile.ProfileData;

import java.util.ArrayList;

/**
 * <h1>ProfileContract</h1>
 * <p>This contract define interface with required methods for view and presenter</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 19/06/2020
 **/
public interface ProfileContract {

    interface View extends BaseView {

        /**
         * <p>This method is indicate API is failed.</p>
         */
        void onFailure();

        /**
         * <p>This method is provide response from profile API with {@link ProfileData} class.</p>
         * @param profileData the response of profile data
         */
        void onSuccessProfile(ProfileData profileData);

        /**
         * <p>This method is called when successfully get response from API string message.</p>
         * @param msg the response of api
         */
        void onSuccess(String msg);

        /**
         * <p>This method is called when successfully get response from API string message.</p>
         * @param data
         */
        void onLangSuccess(ArrayList<LanguageData> data);

        /**
         * This method is display passed error message in snack bar.
         * @param error the error message
         */
        void showError(String error);
    }

    interface Presenter extends BasePresenter<View> {

        /**
         * <p>This method is call profile API.</p>
         */
        void getProfile();

        /**
         * <p>This method notify when fragment view is detached from the activity</p>
         */
        void detach();

        void getLanguage();
    }
}
