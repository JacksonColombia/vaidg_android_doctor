package com.vaidg.pro.main.profile.support;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>SupportDaggerModule</h1>
 * <p>This dagger module created for LogInActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 04/07/2020
 **/
@Module
public abstract class SupportDaggerModule {
    @ActivityScoped
    @Binds
    abstract Activity getActivity(SupportActivity activity);

    @ActivityScoped
    @Binds
    abstract SupportContract.View getView(SupportActivity activity);

    @ActivityScoped
    @Binds
    abstract SupportContract.Presenter getPresenter(SupportPresenterImple presenterImple);
}
