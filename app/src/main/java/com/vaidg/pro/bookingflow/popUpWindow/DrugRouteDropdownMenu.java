package com.vaidg.pro.bookingflow.popUpWindow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.DrugRouteDropdownAdapter;
import com.vaidg.pro.adapters.FrquencyDropdownAdapter;
import com.vaidg.pro.pojo.addMedication.DrugRoute;
import com.vaidg.pro.pojo.addMedication.DrugRoute;
import java.util.ArrayList;

public class DrugRouteDropdownMenu extends PopupWindow {
  private Context context;
  private RecyclerView rvCategory;
  private ArrayList<DrugRoute> units = new ArrayList<>();
  private DrugRouteDropdownAdapter dropdownAdapter;

  public DrugRouteDropdownMenu(Context context, ArrayList<DrugRoute> units) {
    super(context);
    this.context = context;
    this.units = units;
    setupView(units);
  }

  public void setCategorySelectedListener(DrugRouteDropdownAdapter.CategorySelectedListener categorySelectedListener) {
    dropdownAdapter.setCategorySelectedListener(categorySelectedListener);
  }

  private void setupView(ArrayList<DrugRoute> units) {
    View view = LayoutInflater.from(context).inflate(R.layout.item_popup_category, null);
    rvCategory = view.findViewById(R.id.rvCategory);
    rvCategory.setHasFixedSize(true);
    rvCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
    // rvCategory.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
    dropdownAdapter = new DrugRouteDropdownAdapter(units);
    rvCategory.setAdapter(dropdownAdapter);
    setContentView(view);
  }
}