package com.vaidg.pro.landing.newSignup.editEducation;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityAddEditEducationBinding;
import com.vaidg.pro.landing.newSignup.editEducation.selectdegree.SelectDegreeActivity;
import com.vaidg.pro.pojo.profile.DegreeDetailsData;
import com.vaidg.pro.pojo.signup.DegreeData;
import com.vaidg.pro.utility.AppConfig;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.util.Calendar;

import javax.inject.Inject;

import static com.vaidg.pro.AppController.getInstance;


/**
 * <h1>AddEditEducationActivity</h1>
 * <p>activity to  Add Educational Details</P>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 15/06/2020
 **/
public class AddEditEducationActivity extends BaseDaggerActivity implements AddEditEducationContract.View {

    @Inject
    AddEditEducationContract.Presenter presenter;

    private ActivityAddEditEducationBinding binding;

    boolean isEdited;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAddEditEducationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
        initEditData();
        initOnClickListener();
    }

    private void initEditData() {
        DegreeDetailsData pojo = getIntent().getParcelableExtra("editData");
        if (pojo != null) {
            binding.etDegree.setText(pojo.getName());
            binding.etUniversity.setText(pojo.getCollege());
            binding.etYear.setText(pojo.getYear());
            isEdited = pojo.isEdited();
            DegreeData degreeData = new DegreeData();
            degreeData.setId(pojo.getId());
            degreeData.setName(pojo.getName());
            presenter.setDegreeData(degreeData);
        }
    }

    private void initOnClickListener() {
        binding.etDegree.setOnClickListener(this::onClick);
        binding.etUniversity.setOnClickListener(this::onClick);
        binding.etYear.setOnClickListener(this::onClick);
        binding.includeToolbar.btnDone.setOnClickListener(this::onClick);
    }

    private void initViews() {
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        binding.includeToolbar.tvTitle.setText(getString(R.string.titleAddEducation));
        binding.includeToolbar.btnDone.setText(getString(R.string.save));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.etDegree:
                Utility.hideKeyboad(this);
                selectDegree();
                break;

            case R.id.etUniversity:
                break;

            case R.id.etYear:
                Utility.hideKeyboad(this);
                selectYearDialog();
                break;

            case R.id.btnDone:
                Utility.hideKeyboad(this);
                returnEducationalData();
                break;

        }
    }

    private void selectYearDialog() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(getInstance().getTimeZone());
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(this, (selectedMonth, selectedYear) -> binding.etYear.setText(String.valueOf(selectedYear)), calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH));
        builder.setActivatedYear(calendar.get(Calendar.YEAR))
                .setTitle(getString(R.string.titleSelectYear))
                .showYearOnly()
                .build().show();

    }

    private void returnEducationalData() {
        String degree, university, year;
        degree = binding.etDegree.getText() == null ? "" : binding.etDegree.getText().toString();
        university = binding.etUniversity.getText() == null ? "" : binding.etUniversity.getText().toString();
        year = binding.etYear.getText() == null ? "" : binding.etYear.getText().toString();
        if (presenter.validField(degree, university, year)) {
            Intent intent = new Intent();
            DegreeDetailsData data = new DegreeDetailsData();
            data.setName(degree);
            data.setId(presenter.getDegreeData().getId());
            data.setCollege(university);
            data.setYear(year);
            data.setEdited(isEdited);
            intent.putExtra(AppConfig.AddEducationDetails, data);
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else {
            showError(getString(R.string.pleaseFillTheFields));
        }
    }

    @Override
    public void selectDegree() {
        Intent clinicDetails = new Intent(this, SelectDegreeActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startActivityForResult(clinicDetails, VariableConstant.REQUEST_CODE_DEGREE, ActivityOptions
                    .makeSceneTransitionAnimation(this).toBundle());
        } else {
            startActivityForResult(clinicDetails, VariableConstant.REQUEST_CODE_DEGREE);
            overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
        }
    }

    @Override
    public void displaySelectedDegree(DegreeData degreeData) {
        binding.etDegree.setText(degreeData.getName());
    }

    @Override
    public void showProgress() {
        runOnUiThread(() -> binding.pbLoader.setVisibility(View.VISIBLE));
    }

    @Override
    public void hideProgress() {
        runOnUiThread(() -> binding.pbLoader.setVisibility(View.GONE));
    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clParent, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        presenter.parseOnActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
