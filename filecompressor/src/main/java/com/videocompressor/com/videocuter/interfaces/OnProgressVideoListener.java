package com.videocompressor.com.videocuter.interfaces;

/**
 * @author Suresh.
 * @version 1.0.
 * @since 1-01-2018
 */
public interface OnProgressVideoListener {

    void updateProgress(int time, int max, float scale);
}
