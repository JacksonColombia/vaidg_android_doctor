package com.vaidg.pro.landing.newSignup.selfOwnedClinic.model;

import com.vaidg.pro.BaseModel;
import com.vaidg.pro.landing.newSignup.selfOwnedClinic.SelfOwnedClinicUtil;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

public class SelfOwnedClinicModel extends BaseModel {

    @Named(SelfOwnedClinicUtil.PATH)
    @Inject
    ArrayList<String> pathList;

//    @Named(SelfOwnedClinicUtil.URL)
//    @Inject
//    ArrayList<String> URLList;

    private String imageLogoUrl;

    @Inject
    public SelfOwnedClinicModel() {
    }


//    public void addFileToAdapter(String imageUrl) {
//        URLList.add(imageUrl);
//    }

    public void deletePostImage(int currentItem) {
        if (pathList.size() >= currentItem) {
            pathList.remove(currentItem);
        }
//        if (URLList.size() >= currentItem) {
//            URLList.remove(currentItem);
//            pathList.remove(currentItem);
//        }
    }


    public boolean hasItem() {
        return pathList.size() > 0;
//        return URLList.size() > 0;
    }

    public boolean isMediaSizeEmpty() {
        return pathList.isEmpty();
//        return URLList.isEmpty();
    }

    public void addFileToAdapterPath(String path) {
        pathList.add(path);
    }

    public String getClinicImages() {
        String media = "";
        if (pathList.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String s : pathList) {
                sb.append(s).append(",");
            }
            media = sb.deleteCharAt(sb.length() - 1).toString();
        }
//        if (URLList.size() > 0) {
//            StringBuilder sb = new StringBuilder();
//            for (String s : URLList) {
//                sb.append(s).append(",");
//            }
//            media = sb.deleteCharAt(sb.length() - 1).toString();
//        }
        return media;
    }

    public String getImageLogoUrl() {
        return imageLogoUrl;
    }

    public void setImageLogoUrl(String imageLogoUrl) {
        this.imageLogoUrl = imageLogoUrl;
    }
}
