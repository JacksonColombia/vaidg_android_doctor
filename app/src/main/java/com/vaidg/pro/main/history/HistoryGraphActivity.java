package com.vaidg.pro.main.history;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.HistoryListAdapter;
import com.vaidg.pro.databinding.ActivityHistoryGraphBinding;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.utility.EqualSpacingItemDecoration;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import java.util.ArrayList;
import java.util.Objects;

import javax.inject.Inject;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>HistoryGraphFragment</h1>
 * HistoryGraphActivity for showing history details
 * <p>
 * Showing the graph and tablayout text based on api
 * </P>
 */

public class HistoryGraphActivity extends BaseDaggerActivity implements HistoryGraphContract.View, TabLayout.OnTabSelectedListener {

    private String TAG = getClass().getName();

    @Inject
    HistoryGraphContract.Presenter presenter;
    @Inject
    SessionManager sessionManager;

    private ActivityHistoryGraphBinding binding;
    protected BarChart mChart;
    private ProgressDialog progressDialog;
    private HistoryListAdapter historyListAdapter;
    private ArrayList<Booking> bookings;

    private ArrayList<ArrayList<BarEntry>> wholeBarEntries;
    private ArrayList<String> apiFormatDates;

    private boolean isReselectCalled;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHistoryGraphBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int tabPosition = tab.getPosition();
        Log.d(TAG, "onTabSelected: " + tabPosition);
        if (tabPosition == apiFormatDates.size() - 1) {
            presenter.getHistory(apiFormatDates.get(tabPosition), true);
        } else {
            presenter.getHistory(apiFormatDates.get(tabPosition), false);
        }
        BarDataSet set1;
        if (mChart.getData() != null && mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(wholeBarEntries.get(tabPosition));
//            set1.setLabel(getString(R.string.theWeek) + " " + selectedWeeks);
//            set1.setHighLightColor(ContextCompat.getColor(this, R.color.colorPrimary));
//            Highlight highlight = new Highlight(highestPositions.get(tabPosition), 0, 0);
//            mChart.highlightValue(highlight, false);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(wholeBarEntries.get(tabPosition), ""/*getString(R.string.theWeek) + " " + selectedWeeks*/);
            set1.setDrawIcons(false);
            set1.setDrawValues(false);
            set1.setColors(ContextCompat.getColor(this, R.color.colorPrimary));
//            set1.setHighLightColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

            set1.setValueFormatter((value, entry, dataSetIndex, viewPortHandler) -> "" + ((int) value));

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(0.8f);
            data.setHighlightEnabled(false);

            mChart.setData(data);
            mChart.getLegend().setEnabled(false);
            mChart.getAxisLeft().setAxisMaximum(data.getYMax() + 1f);
//            Highlight highlight = new Highlight(highestPositions.get(tabPosition), 0, 0);
//            mChart.highlightValue(highlight, false);
        }

        mChart.invalidate();
        mChart.animateXY(1000, 500);
    }

    private void initTabLayout(final ArrayList<String> tabFormatDates) {
        binding.tabLayout.removeAllTabs();
        for (int i = 0; i < tabFormatDates.size(); i++) {
            binding.tabLayout.addTab(binding.tabLayout.newTab());
            Objects.requireNonNull(binding.tabLayout.getTabAt(i)).setText(tabFormatDates.get(i));
        }

        new Handler().postDelayed(
                () -> Objects.requireNonNull(binding.tabLayout.getTabAt(tabFormatDates.size() - 1)).select(), 100);

        binding.tabLayout.addOnTabSelectedListener(this);
    }


    private void initBarChart(ArrayList<String> days) {
        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(false);
        mChart.getDescription().setEnabled(false);
        mChart.setDrawBarShadow(false);
        mChart.setDrawGridBackground(false);
        mChart.setScaleEnabled(false); //Enables/disables scaling for the chart on both axes.
        mChart.setDrawBorders(true);

        Typeface fondMedium = Utility.getFontMedium(this);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setValueFormatter(new IndexAxisValueFormatter(days));
        xAxis.setLabelCount(7);
        xAxis.setTextColor(ContextCompat.getColor(this, R.color.black));
        xAxis.setTypeface(fondMedium);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setValueFormatter((value, axis) -> "" + ((int) value));
      /*  if(bookings != null && bookings.size() > 0) {
            leftAxis.setValueFormatter(new IndexAxisValueFormatter(maxCount));
            leftAxis.setLabelCount(bookings.size(), true);
        }else{
            leftAxis.setValueFormatter((value, axis) -> "" + ((int) value));
        }*/
        leftAxis.setAxisMinimum(0);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setGridColor(ContextCompat.getColor(this, R.color.dark_gray));
        leftAxis.setGridLineWidth(1f);
        leftAxis.setTextColor(ContextCompat.getColor(this, R.color.black_shade));
        leftAxis.setTypeface(fondMedium);
    }

    private void initBarChartListener() {

        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    @Override
    public void initViews() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingHistory));
        progressDialog.setCancelable(false);

        binding.tvTitle.setText(R.string.history);
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        bookings = new ArrayList<>();

        isReselectCalled = false;

        RecyclerView rvHistory = findViewById(R.id.rvHistory);
        rvHistory.setLayoutManager(new LinearLayoutManager(this));
        rvHistory.addItemDecoration(new EqualSpacingItemDecoration(12));
        historyListAdapter = new HistoryListAdapter(this, bookings);
        rvHistory.setAdapter(historyListAdapter);

        mChart = findViewById(R.id.mChart);

        presenter.getBookingHistoryByWeek();

    }

    @Override
    public void setTabBarChart(ArrayList<ArrayList<BarEntry>> wholeBarEntries, ArrayList<Integer> highestPosition, ArrayList<String> apiFormatDates, ArrayList<String> tabFormatDates, ArrayList<String> days) {
        this.wholeBarEntries = wholeBarEntries;
        this.apiFormatDates = apiFormatDates;

        initBarChart(days);
        initBarChartListener();
        if (tabFormatDates.size() > 0) {
            initTabLayout(tabFormatDates);
        }

    }

    @Override
    public void onSuccessBookingHistory(ArrayList<Booking> bookings, String total) {
        this.bookings.clear();
        this.bookings.addAll(bookings);

      /*  ArrayList<String> maxCount = new ArrayList<>();

        if(bookings.size() > 0) {
            for (int i = 0; i < bookings.size(); i++) {
                maxCount.add(String.valueOf(i));
            }
        }

        YAxis leftAxis = mChart.getAxisLeft();
        if(bookings.size() > 0) {
            leftAxis.setValueFormatter(new IndexAxisValueFormatter(maxCount));
            leftAxis.setLabelCount(bookings.size(), true);
        }else{
            leftAxis.setValueFormatter((value, axis) -> "" + ((int) value));
        }*/
        Utility.printLog(TAG, "Booking Data : " + new Gson().toJson(bookings));
        historyListAdapter.notifyDataSetChanged();
        if (bookings.size() > 0) {
            binding.tvWeekEarning.setText(Utility.getPrice(total, sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            binding.tvNoHistoryMsg.setVisibility(View.GONE);
        } else {
            binding.tvWeekEarning.setText(Utility.getPrice("0", sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            binding.tvNoHistoryMsg.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onFailure() {
        showError(getString(R.string.serverError));
    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clParent, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void showProgress() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void hideProgress() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        Log.d(TAG, "onTabReselected: ");
        if (!isReselectCalled) {
            isReselectCalled = true;
            onTabSelected(tab);
        }
    }

    @Override
    public void onBackPressed() {
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();
    }
}

