package com.vaidg.pro.main.profile.myratecard;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.ViewPagerAdapter;
import com.vaidg.pro.main.profile.myratecard.categoryratebycalltype.InCallTeleCallRateCard;
import com.vaidg.pro.main.profile.myratecard.categoryratebycalltype.OutCallRateCardFragment;
import com.vaidg.pro.pojo.profile.ratecard.CategoryData;
import com.vaidg.pro.utility.Utility;

public class CategoryRateByCallType extends AppCompatActivity {
    private static int position;
    private static ViewPagerAdapter viewPagerAdapter;
    private static ViewPager viewPagerCategory;
    ImageView ivCloseButton;
    private CategoryData categoryData;
    private InCallTeleCallRateCard inCallTeleCallRateCard;
    private OutCallRateCardFragment outCallRateCard;

    public static Fragment getTabSelected() {
        return viewPagerAdapter.getItem(viewPagerCategory.getCurrentItem());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_call_category);
        ivCloseButton = findViewById(R.id.ivCloseButton);
        initializer();

    }

    private void initializer() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setTypeface(Utility.getFontBold(this));


        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        TabLayout tablayoutMyEvents = findViewById(R.id.tablayoutMyEvents);
        viewPagerCategory = findViewById(R.id.viewPagerCategory);

        Intent intent = getIntent();
        categoryData = (CategoryData) intent.getSerializableExtra("category");
        position = intent.getIntExtra("position", 0);

        tvTitle.setText(categoryData.getCat_name());

        if (categoryData.getCallType().isIncall() || categoryData.getCallType().isTelecall()) {
            inCallTeleCallRateCard = InCallTeleCallRateCard.newInstance(categoryData, true);
        }
        if (categoryData.getCallType().isOutcall()) {
            outCallRateCard = OutCallRateCardFragment.newInstance(categoryData, false);
        }
        if (inCallTeleCallRateCard != null) {
            viewPagerAdapter.addFragment(inCallTeleCallRateCard, getString(R.string.inCall) + "/" + getString(R.string.teleCall));
        }
        if (outCallRateCard != null) {
            viewPagerAdapter.addFragment(outCallRateCard, getString(R.string.outCall));
        }

        viewPagerCategory.setAdapter(viewPagerAdapter);
        tablayoutMyEvents.setupWithViewPager(viewPagerCategory);
        viewPagerCategory.addOnPageChangeListener(new PageListener());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    private void closeActivity() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }

    private class PageListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            Utility.printLog("page selected ", "" + position);
            position = i;

            if (viewPagerAdapter.getItem(viewPagerCategory.getCurrentItem()) instanceof InCallTeleCallRateCard) {
                inCallTeleCallRateCard.setOptionMenu(true);
                outCallRateCard.setOptionMenu(false);
            } else {
                outCallRateCard.setOptionMenu(true);
                inCallTeleCallRateCard.setOptionMenu(false);
            }


        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    }
}