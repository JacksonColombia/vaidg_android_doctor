package com.vaidg.pro.pojo.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 07-Oct-17.
 */

public class BookingData implements Serializable {
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("profileActivationStatus")
    @Expose
    private int profileActivationStatus;
    @SerializedName("profileStatus")
    @Expose
    private int profileStatus;
    @SerializedName("walletAmount")
    @Expose
    private double walletAmount;
    @SerializedName("bid")
    @Expose
    private boolean bid;
    @SerializedName("notificationUnreadCount")
    @Expose
    private int notificationUnreadCount;
    @SerializedName("request")
    @Expose
    private ArrayList<Booking> request;
    @SerializedName("accepted")
    @Expose
    private ArrayList<Booking> accepted;
    @SerializedName("activeBid")
    @Expose
    private ArrayList<Booking> activeBid;

    public boolean isBid() {
        return bid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<Booking> getRequest() {
        return request;
    }

    public void setRequest(ArrayList<Booking> request) {
        this.request = request;
    }

    public ArrayList<Booking> getAccepted() {
        return accepted;
    }

    public void setAccepted(ArrayList<Booking> accepted) {
        this.accepted = accepted;
    }

    public ArrayList<Booking> getActiveBid() {
        return activeBid;
    }

    public void setActiveBid(ArrayList<Booking> activeBid) {
        this.activeBid = activeBid;
    }

    public int getProfileActivationStatus() {
        return profileActivationStatus;
    }

    public void setProfileActivationStatus(int profileActivationStatus) {
        this.profileActivationStatus = profileActivationStatus;
    }

    public int getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(int profileStatus) {
        this.profileStatus = profileStatus;
    }

    public double getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(double walletAmount) {
        this.walletAmount = walletAmount;
    }

    public int getNotificationUnreadCount() {
        return notificationUnreadCount;
    }

    public void setNotificationUnreadCount(int notificationUnreadCount) {
        this.notificationUnreadCount = notificationUnreadCount;
    }
}
