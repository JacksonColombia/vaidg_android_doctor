package com.vaidg.pro.pojo.signup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 */

public class CityPojo implements Serializable {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<CityData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CityData> getData() {
        return data;
    }

    public void setData(ArrayList<CityData> data) {
        this.data = data;
    }
}
