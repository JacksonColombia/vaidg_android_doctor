package com.vaidg.pro.landing.introsilider2;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.landing.introsilider2.model.IntroSliderAdapter;
import com.vaidg.pro.pojo.introslider.SliderData;
import com.vaidg.pro.utility.SessionManager;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * <h1>IntroSliderUtil</h1>
 * <p>This dagger util is used to provide object reference to injected objects when declare in activity.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
@Module
public class IntroSliderUtil {

    @ActivityScoped
    @Provides
    ArrayList<SliderData> provideSliderModelArrayList() {
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    IntroSliderAdapter provideIntroSliderAdapter(ArrayList<SliderData> arrayList) {
        return new IntroSliderAdapter(arrayList);
    }

    @ActivityScoped
    @Provides
    SessionManager provide(Activity activity) {
        return SessionManager.getSessionManager(activity);
    }
}
