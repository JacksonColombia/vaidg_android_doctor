package com.vaidg.pro.main.profile.profiledetail;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.main.profile.profiledetail.metadatadit.ProfileMetaDataEditActivity;
import com.vaidg.pro.main.profile.profiledetail.model.ProfileAdditionalDetailsModel;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.profile.ProfilePojo;
import com.vaidg.pro.pojo.profile.metaData.MetaDataArr;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.FileUtils;
import com.vaidg.pro.utility.MediaBottomSelector;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.fileUtil.AppFileManger;
import com.videocompressor.com.CompressImage;
import com.videocompressor.com.DataModel.CompressedData;
import com.videocompressor.com.RxCompressObservable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import eu.janmuller.android.simplecropimage.CropImage;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.AMAZON_BASE_URL;
import static com.vaidg.pro.utility.VariableConstant.BUCKET_NAME;
import static com.vaidg.pro.utility.VariableConstant.DATA;
import static com.vaidg.pro.utility.VariableConstant.INTENT_UPDATE_META_DATA;
import static com.vaidg.pro.utility.VariableConstant.ITEM_POSITION;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

/**
 * <h1>ProfileDetailPresenter</h1>
 * <p>This presenter is define to handle activity interactions done by user.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 20/06/2020
 **/
public class ProfileDetailPresenterImple implements ProfileDetailContract.Presenter, App_permission.Permission_Callback, MediaBottomSelector.Callback, UploadFileAmazonS3.UploadCallBack {

    private static final String TAG = "ProfileDetailPresenterImple";

    private final String GALLERY = "gallery";
    private final String CAMERA = "camera";

    @Inject
    Activity activity;
    @Inject
    ProfileDetailContract.View view;
    @Inject
    NetworkService service;
    @Inject
    SessionManager sessionManager;
    @Inject
    Gson gson;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    ProfileAdditionalDetailsModel model;
    @Inject
    App_permission app_permission;
    @Inject
    Utility utility;
    @Inject
    AppFileManger appFileManger;
    @Inject
    MediaBottomSelector mediaBottomSelector;
    @Inject
    CompressImage compressImage;
    @Inject
    UploadFileAmazonS3 uploadFileAmazonS3;

    private File temp_file = null;

    @Inject
    ProfileDetailPresenterImple() {
    }

    @Override
    public void openChooser() {
        temp_file = null;
        mediaBottomSelector.showBottomSheet(this);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        if (isAllGranted && tag.equals(CAMERA)) {
            if (view != null) {
                try {

                    temp_file = appFileManger.getImageFile();

                    view.openCamera(utility.getUri_Path(temp_file));
                } catch (Exception e) {
                    view.showError(e.getMessage());
                }
            }
        } else if (isAllGranted && tag.equals(GALLERY)) {
            if (view != null)
                view.openGallery();
        }
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        if (tag.equals(GALLERY)) {
            app_permission.show_Alert_Permission(activity.getString(R.string.photo_access_text), activity.getString(R.string.gallery_acess_subtitle),
                    activity.getString(R.string.gallery_acess_message), stringArray);
        } else if (tag.equals(CAMERA)) {
            app_permission.show_Alert_Permission(activity.getString(R.string.camera_access_text), activity.getString(R.string.camera_acess_subtitle),
                    activity.getString(R.string.camera_acess_message), stringArray);
        }
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent) {
        if (parmanent) {
            if (tag.equals(GALLERY)) {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.photo_denied_text), activity.getString(R.string.gallery_denied_subtitle),
                        activity.getString(R.string.gallery_denied_message));
            } else if (tag.equals(CAMERA)) {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.camera_denied_text), activity.getString(R.string.camera_denied_subtitle),
                        activity.getString(R.string.camera_denied_message));
            }
        }
    }

    @Override
    public void onCamera() {
        ArrayList<App_permission.Permission> permissions = new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.CAMERA);
        app_permission.getPermission_for_Sup_v4Fragment(CAMERA, permissions, null, this);

    }

    @Override
    public void onGallery() {
        ArrayList<App_permission.Permission> permissions = new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
        app_permission.getPermission_for_Sup_v4Fragment(GALLERY, permissions, null, this);
    }

    @Override
    public void compressImage(String filePath, UploadFileAmazonS3.UploadCallBack callBack) {
        Observer<CompressedData> observer = new Observer<CompressedData>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(CompressedData value) {
                if (value != null) {
                    amazonUpload(new File(value.getPath()), callBack);
                } else {
                    view.hideProgress();
                    view.showError(activity.getString(R.string.somethingWentWrong));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                if (view != null) {
                    view.hideProgress();
                    view.showError("Failed to collect!");
                }
            }

            @Override
            public void onComplete() {
            }
        };
        RxCompressObservable observable = compressImage.compressImage(activity, filePath);
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }

    @Override
    public void amazonUpload(File mFileTemp, UploadFileAmazonS3.UploadCallBack callBack) {
        String BUCKETSUBFOLDER = VariableConstant.WORK_IMAGE;
        final String imageUrl = "https://" + BUCKET_NAME + "." + AMAZON_BASE_URL
                + BUCKETSUBFOLDER + "/"
                + mFileTemp.getName();
        Utility.printLog(TAG, "Upload Path : " + imageUrl);
        uploadFileAmazonS3.Upload_data(BUCKETSUBFOLDER, mFileTemp.getName(), mFileTemp, callBack);
    }

    @Override
    public void getProfile() {
        if (networkStateHolder.isConnected()) {
            view.showProgress();
            service.getProfile(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        if (view != null)
                                            view.hideProgress();
                                        if (response != null && !response.isEmpty()) {
                                            ProfilePojo profilePojo = gson.fromJson(response, ProfilePojo.class);
                                            sessionManager.setTitle(profilePojo.getData().getTitle());
                                            sessionManager.setFirstName(profilePojo.getData().getFirstName());
                                            sessionManager.setFirstName(profilePojo.getData().getFirstName());
                                            sessionManager.setLastName(profilePojo.getData().getLastName());
                                            sessionManager.setEmail(profilePojo.getData().getEmail());
                                            sessionManager.setPhoneNumber(profilePojo.getData().getMobile());
                                            sessionManager.setCountryCode(profilePojo.getData().getCountryCode());
                                            sessionManager.setProfilePic(profilePojo.getData().getProfilePic());
                                            sessionManager.setGender(profilePojo.getData().getGender());
                                            sessionManager.setDob(profilePojo.getData().getDob());
                                            if (view != null)
                                                view.onSuccessProfile(profilePojo.getData());
                                        } else {
                                            if (view != null)
                                                view.onFailure();
                                        }
                                        break;
                                    case SESSION_EXPIRED:
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                getProfile();
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                if (view != null) {
                                                    view.hideProgress();
                                                    view.onFailure();
                                                }
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, activity);
                                            }
                                        });
                                        break;
                                    case SESSION_LOGOUT:
                                        if (view != null)
                                            view.hideProgress();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        Utility.logoutSessionExiperd(sessionManager, activity);
                                        break;
                                    default:
                                        if (view != null) {
                                            view.hideProgress();
                                            view.showError(Utility.getMessage(errorBody));
                                        }
                                        break;
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.onFailure();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.onFailure();
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void addAdditionalDetailsData(List<MetaDataArr> data) {
        model.addAdditionalDetailsData(data);
        view.notifyAdapter();
    }

    @Override
    public void logout() {
        if (networkStateHolder.isConnected()) {
            if (view != null)
                view.showProgress();
            service.logout(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        if (view != null) {
                                            view.hideProgress();
                                            Utility.logoutSessionExiperd(sessionManager, activity);
                                        }
                                      /*  if (response != null && !response.isEmpty()) {
                                        } else {
                                            if (view != null)
                                                view.onFailure();
                                        }*/
                                        break;
                                    case SESSION_EXPIRED:
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                getProfile();
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                if (view != null) {
                                                    view.hideProgress();
                                                    view.onFailure();
                                                }
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, activity);
                                            }
                                        });
                                        break;
                                    case SESSION_LOGOUT:
                                        if (view != null)
                                            view.hideProgress();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        Utility.logoutSessionExiperd(sessionManager, activity);
                                        break;
                                    default:
                                        if (view != null) {
                                            view.hideProgress();
                                            view.showError(Utility.getMessage(errorBody));
                                        }
                                        break;
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.onFailure();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.onFailure();
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void onItemClick(int position, MetaDataArr metaDataArr) {
        Intent intent = new Intent(activity, ProfileMetaDataEditActivity.class);
        intent.putExtra(ITEM_POSITION, position);
        intent.putExtra(DATA, metaDataArr);
        activity.startActivityForResult(intent, INTENT_UPDATE_META_DATA, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
    }

    @Override
    public void updateMetaData(int position, MetaDataArr metaDataArr) {
        if (position != -1 && metaDataArr != null) {
            model.updateData(position, metaDataArr);
            view.notifyAdapter(position);
        }
    }

    @Override
    public void onActivityResultParse(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case INTENT_UPDATE_META_DATA:
                    if (data != null && data.getIntExtra(ITEM_POSITION, -1) != -1 && data.getParcelableExtra(DATA) != null) {
                        updateMetaData(data.getIntExtra(ITEM_POSITION, -1), data.getParcelableExtra(DATA));
                    }
                    break;
                case VariableConstant.REQUEST_CODE_TAKE_PICTURE:
                    view.startCropImage(temp_file.getPath());
                    break;

                case VariableConstant.REQUEST_CODE_GALLERY:
                    if (data != null) {
                        Uri uri = data.getData();
                        String file_path = FileUtils.getPath(activity, uri);
                        Log.e("TAG", "onActivityResultParse: Gallery : " + file_path);
                        view.startCropImage(file_path);
/*
                        if (view != null)
                            view.showProgress();
*/
                       // compressImage(file_path, this);
                    }
                    break;
                case VariableConstant.REQUEST_CODE_CROP_IMAGE:
                    if (data != null) {
                        String path = data.getStringExtra(CropImage.IMAGE_PATH);
                        if (path == null)
                            return;

                        Log.e("TAG", "onActivityResultParse: Crop : " + path);
                        if (view != null)
                            view.showProgress();
                        compressImage(path, this);
                    }
                    break;
            }
        }
    }

    @Override
    public void sucess(String sucess) {
        if (sucess != null && !sucess.isEmpty()) {
            callUpdateProfileApi(sucess);
        } else {
            if (view != null) {
                view.hideProgress();
                view.showError(activity.getString(R.string.serverError));
            }
        }
    }

    @Override
    public void error(String errormsg) {
        if (view != null) {
            view.hideProgress();
            view.showError(errormsg);
        }
    }

    @Override
    public void callUpdateProfileApi(String profilePic) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("profilePic", profilePic);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        service.updateProfile(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if (view != null)
                                        view.hideProgress();
                                    if (response != null && !response.isEmpty()) {
                                        sessionManager.setProfilePic(profilePic);
                                        if (view != null)
                                            view.updateProfilePic(profilePic);
                                    } else {
                                        if (view != null)
                                            view.onFailure();
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    if (view != null)
                                        view.hideProgress();
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            if (view != null)
                                                view.hideProgress();
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            callUpdateProfileApi(profilePic);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            if (view != null) {
                                                view.hideProgress();
                                            }
                                            getInstance().toast(activity.getResources().getString(R.string.serverError));
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if (view != null)
                                                view.hideProgress();
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, activity);
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    if (view != null)
                                        view.hideProgress();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, activity);
                                    break;
                                default:
                                    if (view != null)
                                        view.hideProgress();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            if (view != null) {
                                view.hideProgress();
                                view.onFailure();
                            }
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.hideProgress();
                            view.onFailure();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

}
