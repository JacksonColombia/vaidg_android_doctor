package com.vaidg.pro.pojo.profile.helpcenter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 29-Dec-17.
 */

public class HelpCenterPojo implements Serializable {
    @SerializedName("data")
    @Expose
    private HelpCenterData data;

    public HelpCenterData getData() {
        return data;
    }

    public void setData(HelpCenterData data) {
        this.data = data;
    }
}
