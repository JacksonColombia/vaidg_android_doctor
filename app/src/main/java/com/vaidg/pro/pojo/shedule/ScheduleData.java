package com.vaidg.pro.pojo.shedule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 17-May-18.
 */

public class ScheduleData implements Serializable {
    @SerializedName("active")
    @Expose
    private ArrayList<ScheduleViewData> active;
    @SerializedName("past")
    @Expose
    private ArrayList<ScheduleViewData> past;

    public ArrayList<ScheduleViewData> getActive() {
        return active;
    }

    public void setActive(ArrayList<ScheduleViewData> active) {
        this.active = active;
    }

    public ArrayList<ScheduleViewData> getPast() {
        return past;
    }

    public void setPast(ArrayList<ScheduleViewData> past) {
        this.past = past;
    }
}
