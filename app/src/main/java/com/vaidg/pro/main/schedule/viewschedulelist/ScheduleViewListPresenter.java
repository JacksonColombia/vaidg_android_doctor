package com.vaidg.pro.main.schedule.viewschedulelist;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.app.Activity;
import android.content.Context;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.main.schedule.viewschedule.ScheculeViewContract;
import com.vaidg.pro.main.schedule.viewschedule.ScheduleViewActivity;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.shedule.ScheduleData;
import com.vaidg.pro.pojo.shedule.ScheduleViewPojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 04-Oct-17.
 * <h1>ScheduleViewPresenter</h1>
 * ScheduleViewPresenter presenter for ScheduleViewActivity
 *
 * @see ScheduleViewActivity
 */

public class ScheduleViewListPresenter implements ScheculeViewListContract.Presenter {

    @Inject
    Context context;
    private ScheculeViewListContract.View view;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;

    @Inject
    public ScheduleViewListPresenter() {
    }


    @Override
    public void deleteSchedule(String id) {
      if(view != null)
        view.startProgressBar();
    //    model.deleteSchedule( id);
      JSONObject jsonObject = new JSONObject();
      try {
            jsonObject.put("scheduleId", id);
        } catch (Exception e) {
            e.printStackTrace();
        }
      service.deleteSchedule(Utility.getAuth(sessionManager.getEmail()),DEFAULT_LANGUAGE, PLATFORM,jsonObject.toString())
          .subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(new Observer<Response<ResponseBody>>() {
              @Override
              public void onSubscribe(Disposable d) {
              }

              @Override
              public void onNext(Response<ResponseBody> responseBodyResponse) {
                  int statusCode = responseBodyResponse.code();
                  try {
                      String response = responseBodyResponse.body() != null
                          ? responseBodyResponse.body().string() : null;
                      String errorBody = responseBodyResponse.errorBody() != null
                          ? responseBodyResponse.errorBody().string() : null;
                      switch (statusCode) {
                          case SUCCESS_RESPONSE:
                            if(view != null)
                             view.stopProgressBar();
                              if (response != null && !response.isEmpty()) {
                                if(view != null)
                                  view.onSuccessDeleteSchedule(Utility.getMessage(response));
                              } else {
                                if(view != null)
                                  view.onFailure();
                              }
                              break;
                          case SESSION_EXPIRED:
                              onRefreshToken(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                  @Override
                                  public void onSuccessRefreshToken(String newToken) {
                                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                    if(view != null)
                                      view.stopProgressBar();
                                      deleteSchedule(id);
                                  }

                                  @Override
                                  public void onFailureRefreshToken() {
                                    if(view != null) {
                                      view.hideProgress();
                                      view.onFailure();
                                    }
                                  }

                                  @Override
                                  public void sessionExpired(String msg) {
                                    if(view != null)
                                      view.hideProgress();
                                      getInstance().toast(Utility.getMessage(msg));
                                      Utility.logoutSessionExiperd(sessionManager, context);
                                  }
                              });
                              break;
                          case SESSION_LOGOUT:
                            if(view != null)
                              view.stopProgressBar();
                              getInstance().toast(Utility.getMessage(errorBody));
                              Utility.logoutSessionExiperd(sessionManager, context);
                              break;
                          default:
                            if(view != null)
                              view.stopProgressBar();
                              getInstance().toast(Utility.getMessage(errorBody));
                            if(view != null)
                              view.onFailure(Utility.getMessage(errorBody));
                              break;
                      }
                  } catch (Exception e) {
                    if(view != null) {
                      view.stopProgressBar();
                      view.onFailure();
                    }
                      e.printStackTrace();
                  }
              }

              @Override
              public void onError(Throwable e) {
                if(view != null) {
                  view.stopProgressBar();
                  view.onFailure();
                }
              }

              @Override
              public void onComplete() {
              }
          });
    }

    @Override
    public void attachView(ScheculeViewListContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    /**
     * ScheduleViewview interface for view implementation
     */
    interface ScheduleViewview {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String msg);

        void onFailure();

        void onSuccess(ScheduleData data);

        void onSuccessDeleteSchedule(String msg);
    }

}
