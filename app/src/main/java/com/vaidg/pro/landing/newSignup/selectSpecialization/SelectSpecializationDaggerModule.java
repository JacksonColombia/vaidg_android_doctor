package com.vaidg.pro.landing.newSignup.selectSpecialization;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>SelectSpecializationDaggerModule</h1>
 * <p>dagger module class</p>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
@Module
public abstract class SelectSpecializationDaggerModule {

    @ActivityScoped
    @Binds
    abstract SelectSpecializationContract.Presenter providePresenter(SelectSpecializationPresenter selectCityPresenter);

    @ActivityScoped
    @Binds
    abstract SelectSpecializationContract.View provideView(SelectSpecializationActivity selectSpecializationActivity);

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(SelectSpecializationActivity activity);
}
