package com.vaidg.pro.adapters;

import static com.vaidg.pro.main.chats.ChattingActivity.getMimeType;
import static com.vaidg.pro.utility.VariableConstant.ZERO;
import static org.webrtc.ContextUtils.getApplicationContext;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vaidg.pro.R;
import com.vaidg.pro.pojo.chat.ChatData;
import com.vaidg.pro.utility.CircleTransform;
import com.vaidg.pro.utility.PhotoFullPopupWindow;
import com.vaidg.pro.utility.Utility;
import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

/**
 * <h>ChattingAdapter</h>
 * Created by Ali on 12/22/2017.
 */

public class ChattingAdapter extends RecyclerView.Adapter<ChattingAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<ChatData> chatData;
    private String fromId;

    public ChattingAdapter(ArrayList<ChatData> chatData, String fromId) {
        this.chatData = chatData;
        this.fromId = fromId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chatting_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(position != RecyclerView.NO_POSITION) {
            // Do your binding here
            if (chatData.get(position).getFromID().equals(fromId)) {
                holder.rlSentMsg.setVisibility(View.VISIBLE);
                holder.rlReceiveMsg.setVisibility(View.GONE);
                holder.ivSendProfilePic.setVisibility(View.VISIBLE);
                holder.ivReceivedProfilePic.setVisibility(View.GONE);
                if (chatData.get(position).getProfilePic() != null && !chatData.get(position).getProfilePic().isEmpty()) {
                    Glide.with(mContext).setDefaultRequestOptions(new RequestOptions().error(R.drawable.profile_default_image)
                            .transform(new CircleTransform(mContext))
                            .placeholder(R.drawable.profile_default_image))
                            .load(chatData.get(position).getProfilePic())
                            .into(holder.ivSendProfilePic);
                }

                if (chatData.get(position).getType().equals("1")) {
                    holder.tvSentMsg.setText(chatData.get(position).getContent());
                    holder.tvSentMsg.setVisibility(View.VISIBLE);
                    holder.ivSendPic.setVisibility(View.GONE);
                    holder.tvSendDoc.setVisibility(View.GONE);
                    holder.pBSentImage.setVisibility(View.GONE);
                } else if (chatData.get(position).getType().equals("2")) {
                    holder.tvSentMsg.setVisibility(View.GONE);
                    holder.tvSendDoc.setVisibility(View.GONE);
                    holder.ivSendPic.setVisibility(View.VISIBLE);
                    String url = chatData.get(position).getContent();
                    if (!url.isEmpty()) {
                        Glide.with(mContext).setDefaultRequestOptions(new RequestOptions())
                                .load(url)
                                .into(holder.ivSendPic);

                        holder.pBSentImage.setVisibility(View.GONE);
                        holder.ivSendPic.setOnClickListener(view -> new PhotoFullPopupWindow(mContext, R.layout.popup_photo_full, view, url, null));
                    } else {
                        holder.pBSentImage.setVisibility(View.VISIBLE);
                    }
                } else {
                    holder.tvSentMsg.setVisibility(View.GONE);
                    holder.tvSendDoc.setVisibility(View.VISIBLE);
                    holder.ivSendPic.setVisibility(View.GONE);

                    String fileName = chatData.get(position).getContent().substring(chatData.get(position).getContent().lastIndexOf('/') + 1, chatData.get(position).getContent().length());
                    if (!fileName.isEmpty()) {
                        holder.tvSendDoc.setText(fileName);
                        switch (getMimeType(mContext, Uri.parse(chatData.get(position).getContent()))) {
                            case "pdf":
                                holder.tvSendDoc.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable(R.drawable.icon_file_pdf), null);
                                break;
                            case "doc":
                            case "docx":
                            case "dot":
                            case "dotx":
                                holder.tvSendDoc.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable(R.drawable.icon_file_doc), null);
                                break;
                            case "ppt":
                            case "pptx":
                                holder.tvSendDoc.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable(R.drawable.icon_file_ppt), null);
                                break;
                            case "xls":
                            case "xlsx":
                                holder.tvSendDoc.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable(R.drawable.icon_file_xls), null);
                                break;
                            default:
                                holder.tvSendDoc.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable(R.drawable.icon_file_unknown), null);
                                break;
                        }
                        holder.tvSendDoc.setOnClickListener(v -> {
                            if (Utility.isNetworkAvailable(mContext))
                                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(chatData.get(position).getContent())));
                            else
                                Toast.makeText(mContext, "Please check your internetConnection", Toast.LENGTH_SHORT).show();
                        });

                    }
                }

            } else {
                holder.rlSentMsg.setVisibility(View.GONE);
                holder.rlReceiveMsg.setVisibility(View.VISIBLE);
                holder.ivReceivedProfilePic.setVisibility(View.VISIBLE);
                holder.ivSendProfilePic.setVisibility(View.GONE);
                if (chatData.get(position).getProfilePic() != null && !chatData.get(position).getProfilePic().isEmpty()) {
                    Glide.with(mContext).setDefaultRequestOptions(new RequestOptions().error(R.drawable.profile_default_image)
                            .transform(new CircleTransform(mContext))
                            .placeholder(R.drawable.profile_default_image))
                            .load(chatData.get(position).getProfilePic())
                            .into(holder.ivReceivedProfilePic);
                }
                if (chatData.get(position).getType().equals("1")) {
                    holder.tvReceiveMsg.setText(chatData.get(position).getContent());
                    holder.tvReceiveMsg.setVisibility(View.VISIBLE);
                    holder.ivReceivedPic.setVisibility(View.GONE);
                    holder.tvReceiveDoc.setVisibility(View.GONE);

                } else if (chatData.get(position).getType().equals("2")) {
                    holder.tvReceiveMsg.setVisibility(View.GONE);
                    holder.ivReceivedPic.setVisibility(View.VISIBLE);
                    holder.tvReceiveDoc.setVisibility(View.GONE);
                    String url = chatData.get(position).getContent();
                    if (!url.equals("")) {
                        Glide.with(mContext).setDefaultRequestOptions(new RequestOptions())
                                .load(url)
                                .into(holder.ivReceivedPic);
                        holder.ivReceivedPic.setOnClickListener(view -> new PhotoFullPopupWindow(mContext, R.layout.popup_photo_full, view, url, null));
                    }
                } else {
                    holder.tvReceiveMsg.setVisibility(View.GONE);
                    holder.tvReceiveDoc.setVisibility(View.VISIBLE);
                    holder.ivReceivedPic.setVisibility(View.GONE);
                    String fileName = chatData.get(position).getContent().substring(chatData.get(position).getContent().lastIndexOf('/') + 1, chatData.get(position).getContent().length());
                    if (!fileName.isEmpty()) {
                        holder.tvReceiveDoc.setText(fileName);
                        holder.pBSentImage.setVisibility(View.GONE);
                        switch (getMimeType(mContext, Uri.parse(chatData.get(position).getContent()))) {
                            case "pdf":
                                holder.tvReceiveDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_pdf), null, null, null);
                                break;
                            case "doc":
                            case "docx":
                            case "dot":
                            case "dotx":
                                holder.tvReceiveDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_doc), null, null, null);
                                break;
                            case "ppt":
                            case "pptx":
                                holder.tvReceiveDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_ppt), null, null, null);
                                break;
                            case "xls":
                            case "xlsx":
                                holder.tvReceiveDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_xls), null, null, null);
                                break;
                            default:
                                holder.tvReceiveDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_unknown), null, null, null);
                                break;
                        }
                        holder.tvReceiveDoc.setOnClickListener(v -> {
                            if (Utility.isNetworkAvailable(mContext))
                                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(chatData.get(position).getContent())));
                            else
                                Toast.makeText(mContext, "Please check your internetConnection", Toast.LENGTH_SHORT).show();
                        });

                    } else {
                        holder.pBSentImage.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    public static String getMimeType(Context context, Uri uri) {
        String extension ="";
        if(uri != null) {
            //Check uri format to avoid null
            if (Objects.equals(uri.getScheme(), ContentResolver.SCHEME_CONTENT)) {
                //If scheme is a content
                final MimeTypeMap mime = MimeTypeMap.getSingleton();
                extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
            } else {
                //If scheme is a File
                //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
                extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(Objects.requireNonNull(uri.getPath()))).toString());
            }
        }

        return extension;
    }

    @Override
    public int getItemCount() {
        return chatData != null ? chatData.size() : ZERO;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout rlSentMsg, rlReceiveMsg;
        private ImageView ivSendPic, ivReceivedPic,ivSendProfilePic,ivReceivedProfilePic;
        private TextView tvSentMsg, tvReceiveMsg,tvSendDoc,tvReceiveDoc;
        private ProgressBar pBSentImage;
        private Typeface fontRegular;

        ViewHolder(View itemView) {
            super(itemView);

            ivSendPic = itemView.findViewById(R.id.ivSendPic);
            ivSendProfilePic = itemView.findViewById(R.id.ivSendProfilePic);
            ivReceivedProfilePic = itemView.findViewById(R.id.ivReceivedProfilePic);
            ivReceivedPic = itemView.findViewById(R.id.ivReceivedPic);
            tvSentMsg = itemView.findViewById(R.id.tvSentMsg);
            tvSendDoc = itemView.findViewById(R.id.tvSendDoc);
            tvReceiveDoc = itemView.findViewById(R.id.tvReceiveDoc);
            tvReceiveMsg = itemView.findViewById(R.id.tvReceiveMsg);
            rlSentMsg = itemView.findViewById(R.id.rlSentMsg);
            rlReceiveMsg = itemView.findViewById(R.id.rlReceiveMsg);

            pBSentImage = itemView.findViewById(R.id.pBSentImage);
            fontRegular = Utility.getFontRegular(mContext);
            tvSentMsg.setTypeface(fontRegular);
            tvReceiveMsg.setTypeface(fontRegular);
        }
    }
}
