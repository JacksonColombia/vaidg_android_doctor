package com.vaidg.pro.main.notification;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.notification.NotificationData;
import java.util.ArrayList;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface NotificationContract {

    interface View extends BaseView {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String failureMsg);

        void onFailure();

        void onSuccess(ArrayList<NotificationData> notificationData);

        void onNewToken(String newToken);

        void sessionExpired(String msg);

    }

    interface Presenter extends BasePresenter {
        void getNotificationList(final int skip, final int limit);
    }
}