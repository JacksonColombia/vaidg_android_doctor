package com.vaidg.pro.pojo.profile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h2>ImageData</h2>
 *
 * @author Suresh.
 * @version 1.0.
 * @since 12/13/2017.
 */
public class ImageData implements Parcelable {
    public static final Creator<ImageData> CREATOR = new Creator<ImageData>() {
        @Override
        public ImageData createFromParcel(Parcel in) {
            return new ImageData(in);
        }

        @Override
        public ImageData[] newArray(int size) {
            return new ImageData[size];
        }
    };
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("standard_resolution")
    @Expose
    private String standard_resolution;
    @SerializedName("low_resolution")
    @Expose
    private String low_resolution;
    @SerializedName("pagePosition")
    @Expose
    private int pagePosition;

    public ImageData() {
    }

    public ImageData(Parcel in) {
        id = in.readString();
        thumbnail = in.readString();
        standard_resolution = in.readString();
        low_resolution = in.readString();
        pagePosition = in.readInt();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getStandard_resolution() {
        return standard_resolution;
    }

    public void setStandard_resolution(String standard_resolution) {
        this.standard_resolution = standard_resolution;
    }

    public String getLow_resolution() {
        return low_resolution;
    }

    public void setLow_resolution(String low_resolution) {
        this.low_resolution = low_resolution;
    }

    public int getPagePosition() {
        return pagePosition;
    }

    public void setPagePosition(int pagePosition) {
        this.pagePosition = pagePosition;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(thumbnail);
        parcel.writeString(standard_resolution);
        parcel.writeString(low_resolution);
        parcel.writeInt(pagePosition);
    }
}
