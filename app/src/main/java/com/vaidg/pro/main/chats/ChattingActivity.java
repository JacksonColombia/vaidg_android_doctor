package com.vaidg.pro.main.chats;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.Utility.isApplicationSentToBackground;
import static com.vaidg.pro.utility.VariableConstant.*;
import static com.vaidg.pro.utility.VariableConstant.AES_MODE;
import static com.vaidg.pro.utility.VariableConstant.AMAZON_BASE_URL;
import static com.vaidg.pro.utility.VariableConstant.BUCKET_NAME;
import static com.vaidg.pro.utility.VariableConstant.IV_PARAMETER_SPEC;
import static com.vaidg.pro.utility.VariableConstant.KEY_PUBLICALIAS;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.RSA_MODE;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.BuildConfig;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.ChattingAdapter;
import com.vaidg.pro.bookingflow.ontheway.OnTheWayActivity;
import com.vaidg.pro.fcm.MyFirebaseMessagingService;
import com.vaidg.pro.landing.introsilider2.IntroSliderNewActivity;
import com.vaidg.pro.main.MainActivity;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.ServiceFactory;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.pojo.chat.ChatApiResponse;
import com.vaidg.pro.pojo.chat.ChatBookingDetailsPojo;
import com.vaidg.pro.pojo.chat.ChatData;
import com.vaidg.pro.pojo.chat.ChatEncryptDecryptPublicKey;
import com.vaidg.pro.utility.CircleTransform;
import com.vaidg.pro.utility.DataBaseChat;
import com.vaidg.pro.utility.FileUtils;
import com.vaidg.pro.utility.MyImageHandler;
import com.vaidg.pro.utility.NotificationHelper;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.ServiceUrl;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.encryptiondecryption.CryptographyExample;
import com.vaidg.pro.utility.fileUtil.AppFileManger;
import com.vaidg.pro.utility.fileUtil.Config;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilPrivateKey;
import com.virgilsecurity.sdk.crypto.VirgilPublicKey;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;
import com.virgilsecurity.sdk.utils.ConvertionUtils;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Collections;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import eu.janmuller.android.simplecropimage.CropImage;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class ChattingActivity extends AppCompatActivity implements View.OnClickListener, EasyPermissions.PermissionCallbacks, ChatOnMessageCallback, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "Chatting";
    private Typeface fontRegular, fontSemiBold;

    private EditText etMsg;
    private SwipeRefreshLayout srlChat;
    private RecyclerView rcvChatMsg;

    private String msgTypeText = "1";
    private String msgTypeImage = "2";
    private String msgTypeDocument = "3";
    private long timeMillisImage;
    private int imagePosition = 0;
    private int documentPosition = 0;

    private SessionManager sessionManager;
    private UploadFileAmazonS3 amazonS3;
    private TextView tvCategory, tvPrice;
    private RelativeLayout rlJobDetails;
    private Booking booking;
    private String bid = "";
    private String customerId;

    private ArrayList<ChatData> chatDataArry;
    private ChattingAdapter chattingAdapter;
    private DataBaseChat db;

    private File mFileTemp;
    private AppFileManger appFileManger;
    private LayoutInflater inflater;
    private NotificationManager notificationManager;

    private Gson gson;
    private int pageIndex = 0;
    private boolean isMoreDataAvailable = true;
    private String call_id;
    private boolean isFromNotification;
    private NetworkService service;
    private KeyStore keyStore;
    private Context mContext;
    private final int IMAGE_REQUEST_CODE = 1000;

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utility.checkAndShowNetworkError(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);

        init();

    }

    private void init() {
        gson = new Gson();
        service = ServiceFactory.getClient(NetworkService.class,this);
        chatDataArry = new ArrayList<>();
        mContext = this;
        sessionManager = SessionManager.getSessionManager(this);
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        fontRegular = Utility.getFontRegular(this);
        fontSemiBold = Utility.getFontBold(this);
        inflater = LayoutInflater.from(this);
        bid = sessionManager.getChatBookingID();
        customerId = sessionManager.getChatCustomerID();
        customerPublicKey(customerId);
        checkForKeystore();
        //db = new DataBaseChat(this);
        //chatDataArry = db.fetchData(bid);

        ImageView ivBackButton = findViewById(R.id.ivBackButton);
        ImageView ivAddFiles = findViewById(R.id.ivAddFiles);
        TextView tvReceiverName = findViewById(R.id.tvReceiverName);
        TextView tvSend = findViewById(R.id.tvSend);
        TextView tvEventId = findViewById(R.id.tvEventId);
        ImageView ivCustomer = findViewById(R.id.ivCustomer);
        tvCategory = findViewById(R.id.tvCategory);
        tvPrice = findViewById(R.id.tvPrice);
        TextView tvJobDetails = findViewById(R.id.tvJobDetails);
        rlJobDetails = findViewById(R.id.rlJobDetails);
        etMsg = findViewById(R.id.etMsg);


        srlChat = findViewById(R.id.srlChat);
        rcvChatMsg = findViewById(R.id.rcvChatMsg);
        chattingAdapter = new ChattingAdapter(chatDataArry, sessionManager.getProviderId());
        rcvChatMsg.setLayoutManager(new LinearLayoutManager(this));
        rcvChatMsg.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(this, R.anim.layoutanimation_from_bottom));
        rcvChatMsg.setAdapter(chattingAdapter);

        srlChat.setOnRefreshListener(this);

        tvReceiverName.setTypeface(fontRegular);
        tvEventId.setTypeface(fontRegular);
        tvPrice.setTypeface(fontRegular);
        tvJobDetails.setTypeface(fontRegular);


        tvReceiverName.setText(sessionManager.getChatCustomerName());
        tvEventId.setText(getResources().getString(R.string.jobId) + " " + bid);

        if (!sessionManager.getChatCustomerPic().equals("")) {
            Glide.with(this).setDefaultRequestOptions(new RequestOptions().error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(this))
                    .placeholder(R.drawable.profile_default_image))
                    .load(sessionManager.getChatCustomerPic())

                    .into(ivCustomer);
        }

        etMsg.setTypeface(fontRegular);
        tvSend.setTypeface(fontSemiBold);

        ivBackButton.setOnClickListener(this);
        ivAddFiles.setOnClickListener(this);
        tvSend.setOnClickListener(this);

        IS_CHATTING_OPENED = true;
        if (!AppController.getInstance().getMqttHelper().isMqttConnected()) {
            AppController.getInstance().getMqttHelper().createMQttConnection(sessionManager.getProviderId(), false);
        }

        AppController.getInstance().getMqttHelper().setChatListener(this);
        MyFirebaseMessagingService.setChatListener(this);


        if (getIntent().getBooleanExtra("isActiveChat", false)) {
            getBookingDetails();
        } else {
            getMessage();
        }

        if (getIntent().getBooleanExtra("isPastChat", false)) {
            findViewById(R.id.cardViewChat).setVisibility(View.GONE);
            findViewById(R.id.booking_complete_text).setVisibility(View.VISIBLE);

        }

        etMsg.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                scrollToBottomSlowly();
            }
        });
        etMsg.setOnClickListener(this);


    }

    private void customerPublicKey(String custId) {
        service.getPublicKey(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, CUSTOMER, custId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<retrofit2.Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(retrofit2.Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;

                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    ChatEncryptDecryptPublicKey chatEncryptDecryptPublicKey = gson.fromJson(response, ChatEncryptDecryptPublicKey.class);
                                    if (chatEncryptDecryptPublicKey.getData() != null) {
                                        ChatEncryptDecryptPublicKey.ChatPublicKey chatPublicKey = chatEncryptDecryptPublicKey.getData();
                                       // if (chatPublicKey.getPublickKey() != null && !chatPublicKey.getPublickKey().isEmpty()) {
                                            sessionManager.setVirgilDoctorPublicKey(chatPublicKey.getPublickKey());
                                       // }
                                    }
                                    break;
                                case SESSION_LOGOUT:
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, mContext);
                                    break;
                                case SESSION_EXPIRED:

                                    onRefreshToken(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            //view.stopProgressBar();
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            customerPublicKey(custId);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            // view.onFailure();
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, mContext);
                                        }
                                    });
                                    break;
                                default:
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    //view.onFailure(Utility.getMessage(errorBody));
                                    break;
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackButton:
                onBackPressed();
                break;

            case R.id.tvSend:
                if (!etMsg.getText().toString().trim().isEmpty()) {
                    String contentMsg = etMsg.getText().toString().trim();
                    etMsg.setText("");

                    long msgId = System.currentTimeMillis();
                    sendMessage(contentMsg, msgTypeText, String.valueOf(msgId));
                }
                break;

            case R.id.ivAddFiles:
                //checkPermission();
                if(isPermissionGranted())
                {
                    selectImage();
                }
                break;

            case R.id.etMsg:
                scrollToBottomSlowly();
                break;

            case R.id.rlJobDetails:
                final Intent intent = new Intent(ChattingActivity.this, OnTheWayActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("booking", booking);
                bundle.putBoolean("isFromChat", true);
                intent.putExtras(bundle);
                startActivity(intent);

                       /* if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

                            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(ChattingActivity.this).toBundle());
                        } else {
                            startActivity(intent);
                            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                        }*/


                break;

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isFromNotification = getIntent().getBooleanExtra("isFromNotification", false);
        IS_CHATTING_RESUMED = true;
        notificationManager.cancelAll();
        sessionManager.setChatCount(bid, 0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        IS_CHATTING_RESUMED = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppController.getInstance().getMqttHelper().setChatListener(null);
        MyFirebaseMessagingService.setChatListener(null);
        IS_CHATTING_OPENED = false;
    }

    @Override
    public void onMessageReceived(ChatData chatData) {
        if (chatData.getBid().equals(bid)) {
            notifyAdapter(String.valueOf(chatData.getTimestamp()), chatData.getType(), chatData.getContent(), customerId, sessionManager.getProviderId(), "1", false);
            if (!sessionManager.getIsTeleCallRunning()) {
                NotificationHelper.playIncomingTone(this);
            }
        }
    }

    @Override
    public void onRefresh() {
        pageIndex++;
        if (isMoreDataAvailable) {
            getMessage();
        } else {
            srlChat.setRefreshing(false);
        }
    }

    private void getBookingDetails() {
        srlChat.setRefreshing(true);
        service.getBookingId(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, bid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<retrofit2.Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(retrofit2.Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    getMessage();
                                    if (response != null && !response.isEmpty()) {
                                        try {
                                            Log.d(TAG, "onSuccess: " + statusCode + "\n" + response);

                                            ChatBookingDetailsPojo chatBookingDetailsPojo = gson.fromJson(response, ChatBookingDetailsPojo.class);
                                            booking = chatBookingDetailsPojo.getData();
                                            rlJobDetails.setVisibility(View.VISIBLE);
                                            rlJobDetails.setOnClickListener(ChattingActivity.this);
                                            tvCategory.setText(booking.getCategory());
                                            tvPrice.setText(getResources().getString(R.string.totalBillAmount) + " " + Utility.getPrice(booking.getAccounting().getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            getBookingDetails();
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, getApplicationContext());
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, getApplicationContext());
                                    break;
                                default:
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }

                        } catch (Exception e) {
                            getMessage();
                            e.printStackTrace();
                        }
                        srlChat.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMessage();
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }

    private void getMessage() {
        // srlChat.setRefreshing(true);
        service.getChatHistory(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, bid, String.valueOf(pageIndex))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<retrofit2.Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @SuppressLint("NewApi")
                    @Override
                    public void onNext(retrofit2.Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        try {
                                            Log.d(TAG, "onSuccess: " + statusCode + "\n" + response);

                                            ChatApiResponse chatApiResponse = gson.fromJson(response, ChatApiResponse.class);
                                            if (chatApiResponse.getData() != null) {
                                                if (chatApiResponse.getData().size() == 0) {
                                                    isMoreDataAvailable = false;

                                                }


                                                chatDataArry.clear();
                                                //  chatDataArry.addAll(chatApiResponse.getData());
                                                // Collections.reverse(chatDataArry);
                                                //Collections.sort(chatDataArry, (o1, o2) -> (int) (o1.getTimestamp() - o2.getTimestamp()));
                                                //chattingAdapter.notifyDataSetChanged();
                                                for(int i = 0;i <chatApiResponse.getData().size();i++)
                                                {
                                                    notifyAdapter(String.valueOf(chatApiResponse.getData().get(i).getTimestamp()), chatApiResponse.getData().get(i).getType(), chatApiResponse.getData().get(i).getContent(), chatApiResponse.getData().get(i).getFromID(),chatApiResponse.getData().get(i).getTargetId(), chatApiResponse.getData().get(i).getCustProType(), true);
                                                }
                                                srlChat.setRefreshing(false);
                                                if (pageIndex == 0) {
                                                    scrollToBottom();
                                                }
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            getMessage();
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, getApplicationContext());
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, getApplicationContext());
                                    break;
                                default:
                                    if (response != null && !response.isEmpty()) {
                                        ChatApiResponse chatApiResponse = gson.fromJson(response, ChatApiResponse.class);
                                        if (chatApiResponse.getMessage() != null && chatApiResponse.getMessage().equals("No Data found")) {
                                            isMoreDataAvailable = false;
                                        }
                                    } else
                                        getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            srlChat.setRefreshing(false);
                            e.printStackTrace();
                        }
                        srlChat.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        srlChat.setRefreshing(false);
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }

    /**
     * <h2>sendmessage</h2>
     * <p>
     * sending request to channel a "Message" of socket
     *
     * @param msg     message to be send
     * @param msgType message type, i.e what type of message is it. example text message type ,image message type, video message type
     *                for txt msgtype = 1, for Image = 2;
     * @param msgId   timeStamp in GMT
     */
    private void sendMessage(String msg, String msgType, String msgId) {
        if (!sessionManager.getIsTeleCallRunning()) {
            NotificationHelper.playSentTone(ChattingActivity.this);
        }

        String messageFromDecrypt = "";
        if (msgType.equals("1")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decryptPrivateKeyPostLollipop();
                decryptPublicKeyPostLollipop();
                messageFromDecrypt = encryptData(decryptPrivateKeyPostLollipop(), decryptPublicKeyPostLollipop(), messageFromDecrypt, msg);
            } else {
                decryptPrivateKeyPreMarshMallow();
                decryptPublicKeyPreMarshMallow();
                messageFromDecrypt = encryptData(decryptPrivateKeyPreMarshMallow(), decryptPublicKeyPreMarshMallow(), messageFromDecrypt, msg);
            }
        } else {
            messageFromDecrypt = msg;
        }
        try {
            JSONObject jsonobj = new JSONObject();
            jsonobj.put("type", msgType);
            jsonobj.put("timestamp", msgId);
            jsonobj.put("content", messageFromDecrypt);
            jsonobj.put("fromID", sessionManager.getProviderId());
            jsonobj.put("bid", bid + "");
            jsonobj.put("targetId", customerId);


            service.sendChatMessage(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonobj.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<retrofit2.Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(retrofit2.Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        // chatDataArry.clear();
                                        Log.d(TAG, "onSuccess: chat" + statusCode);
                                        Log.d(TAG, "onSuccess: chat" + response);
                                        break;
                                    case SESSION_EXPIRED:
                                        onRefreshToken(getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);

                                            }

                                            @Override
                                            public void onFailureRefreshToken() {

                                            }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, getApplicationContext());
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, getApplicationContext());
                                    break;
                                default:
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "onError: chat");
                            e.printStackTrace();
                        }
                        srlChat.setRefreshing(false);
                    }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "onError: chat");
                        }

                        @Override
                        public void onComplete() {
                        }
                    });

            if (msgType.equals(msgTypeText)) {
                notifyAdapter(msgId, msgType, messageFromDecrypt, sessionManager.getProviderId(), customerId, "2",false);
            }

            scrollToBottom();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void notifyAdapter(String msgid, final String msgtype, String msg, String fromId, String targetId, String custProType, boolean b) {
        String messageFromDecrypt = "";
        if (msgtype.equals("1")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decryptPrivateKeyPostLollipop();
                decryptPublicKeyPostLollipop();
                messageFromDecrypt = decryptData(decryptPrivateKeyPostLollipop(), decryptPublicKeyPostLollipop(), messageFromDecrypt, msg);
            } else {
                decryptPrivateKeyPreMarshMallow();
                decryptPublicKeyPreMarshMallow();
                messageFromDecrypt = decryptData(decryptPrivateKeyPreMarshMallow(), decryptPublicKeyPreMarshMallow(), messageFromDecrypt, msg);
            }
        } else {
            messageFromDecrypt = msg;
        }
        final ChatData chatData = new ChatData();
        chatData.setTimestamp(Long.parseLong(msgid));
        chatData.setType(msgtype);
        chatData.setTargetId(targetId);
        chatData.setFromID(fromId);
        chatData.setContent(messageFromDecrypt);
        chatData.setCustProType(custProType);
        if (sessionManager.getProviderId().equals(fromId)) {
            chatData.setProfilePic(sessionManager.getProfilePic());
        } else {
            chatData.setProfilePic(sessionManager.getChatCustomerPic());

        }
        //db.addNewChat(bid,msg,fromId,targetId,msgid,msgtype,custProType);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                    Log.w(TAG, "run: " + chatData.getContent());
                    chatDataArry.add(chatData);
                if(b)
                    Collections.sort(chatDataArry, (o1, o2) -> (int) (o1.getTimestamp() - o2.getTimestamp()));
                    //Collections.reverse(chatDataArry);
                Log.w(TAG, "run: " + chatData.getContent());
                chattingAdapter.notifyDataSetChanged();
                    scrollToBottom();
                    if (msgtype.equals(msgTypeImage)) {
                        imagePosition = chatDataArry.size() - 1;
                    } else if (msgtype.equals(msgTypeDocument)) {
                        documentPosition = chatDataArry.size() - 1;
                    }
            }
        });
    }

    /*scrolling to the bottom of the recyclerview*/
    private void scrollToBottom() {
        rcvChatMsg.scrollToPosition(chattingAdapter.getItemCount() - 1);
    }

    /*scrolling to the bottom of the recyclerview*/
    private void scrollToBottomSlowly() {
        rcvChatMsg.postDelayed(new Runnable() {
            @Override
            public void run() {
                rcvChatMsg.scrollToPosition(chattingAdapter.getItemCount() - 1);
            }
        }, 200);
    }

    /**
     * <h2>checkPermission</h2>
     * <p>checking for the permission for camera and file storage at run time for
     * build version more than 22
     */
    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (EasyPermissions.hasPermissions(this, STORAGE_CAMERA_PERMISSION)) {
                selectImage();
            } else {
                EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
                    IMAGE_REQUEST_CODE, STORAGE_CAMERA_PERMISSION);
            }

        } else {
            selectImage();
        }

    }



    private boolean isPermissionGranted() {
        if (EasyPermissions.hasPermissions(mContext, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
            return true;
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
                IMAGE_REQUEST_CODE, VariableConstant.STORAGE_CAMERA_PERMISSION);
            return false;
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
/*
        if (EasyPermissions.hasPermissions(this, STORAGE_CAMERA_PERMISSION)) {
            selectImage();
        }
*/
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    private void selectImage() {
        appFileManger = new AppFileManger(this);
        mFileTemp = appFileManger.getImageFile();
        final BottomSheetDialog mDialog = new BottomSheetDialog(this);
        View view = inflater.inflate(R.layout.bottom_sheet_picture_selection, null);
        mDialog.setContentView(view);
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView tvImageSelectionHeader = view.findViewById(R.id.tvImageSelectionHeader);
        TextView tvCamera = view.findViewById(R.id.tvCamera);
        TextView tvGallery = view.findViewById(R.id.tvGallery);
        TextView tvDocument = view.findViewById(R.id.tvDocument);

        tvImageSelectionHeader.setTypeface(fontRegular);
        tvCamera.setTypeface(fontSemiBold);
        tvGallery.setTypeface(fontSemiBold);

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getIntent().getBooleanExtra("isFromCall", false))
                {
                    Toast.makeText(mContext, "can't use camera while using VaidG video call", Toast.LENGTH_SHORT).show();
                }else {
                    takePicture();
                }
                mDialog.dismiss();
            }
        });

        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
                mDialog.dismiss();
            }
        });
        tvDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDocument();
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void openDocument() {
        String[] mimeTypes =
                {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                        "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                        "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                        "text/plain",
                        "application/pdf"};

        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
        chooseFile.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            chooseFile.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            chooseFile.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }
        startActivityForResult(chooseFile, REQUEST_CODE_DOCUMENT);
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra("isFromNotification",false)) {
            Intent intent = new Intent(ChattingActivity.this, MainActivity.class);
            //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * open camera for taking image for user profile
     */
    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", mFileTemp);
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * open gallery for selecting image for user profile image
     */
    private void openGallery() {
      /*  Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);*/
        String chooseTitle;
        Intent intent = new Intent();
        intent.setType("image/*");
        chooseTitle = "select Image";
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, chooseTitle), REQUEST_CODE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode != RESULT_OK) {
                return;
            }

            switch (requestCode) {

                case REQUEST_CODE_GALLERY:
                    Uri uri = data.getData();
                    String file_path = FileUtils.getPath(this, uri);
                    startCropImage(file_path);
                    break;

                case REQUEST_CODE_DOCUMENT:
                    try {
                        if (data != null) {
                            String fileName = data.getData().getPath().substring(data.getData().getPath().lastIndexOf('/') + 1, data.getData().getPath().length());
                            String fileNameWithoutExtn = "";
                            String takenNewImage = "";
                            if (fileName.contains("."))
                                fileNameWithoutExtn = fileName.substring(0, fileName.lastIndexOf('.'));
                            else
                                fileNameWithoutExtn = fileName;
                            switch (getMimeType(this, data.getData())) {
                                case "pdf":
                                    takenNewImage = fileNameWithoutExtn + ".pdf";
                                    break;
                                case "doc":
                                    takenNewImage = fileNameWithoutExtn + ".doc";
                                    break;
                                case "docx":
                                    takenNewImage = fileNameWithoutExtn + ".docx";
                                    break;
                                case "dot":
                                    takenNewImage = fileNameWithoutExtn + ".dot";
                                    break;
                                case "dotx":
                                    takenNewImage = fileNameWithoutExtn + ".dotx";
                                    break;
                                case "ppt":
                                    takenNewImage = fileNameWithoutExtn + ".ppt";
                                    break;
                                case "pptx":
                                    takenNewImage = fileNameWithoutExtn + ".pptx";
                                    break;
                                case "xls":
                                    takenNewImage = fileNameWithoutExtn + ".xls";
                                    break;
                                case "xlsx":
                                    takenNewImage = fileNameWithoutExtn + ".xlsx";
                                    break;
                                case "txt":
                                    takenNewImage = fileNameWithoutExtn + ".txt";
                                    break;
                                default:
                                    takenNewImage = fileNameWithoutExtn + ".pdf";
                                    break;
                            }
                            String filename = DOCUMENT_FILE_NAME + System.currentTimeMillis() + takenNewImage;
                            MyImageHandler myImageHandler = MyImageHandler.getInstance();
                            mFileTemp = new File(myImageHandler.getAlbumStorageDir(this, CROP_PIC_DIR, true), filename);
                            InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
                            FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                            Utility.copyStream(inputStream != null ? inputStream : null, fileOutputStream);
                            fileOutputStream.close();
                            inputStream.close();
                            Log.w(TAG, "onActivityResult: " + mFileTemp);
                            timeMillisImage = System.currentTimeMillis();
                            notifyAdapter(String.valueOf(timeMillisImage), msgTypeDocument, "", sessionManager.getProviderId(), customerId, "2", false);
                            amazonS3 =  UploadFileAmazonS3.getInstance(this);
                            String BUCKETSUBFOLDER = DOCUMENTS;
                            final String imageUrl = "https://" + BUCKET_NAME + "." + AMAZON_BASE_URL
                                    + BUCKETSUBFOLDER + "/"
                                    + mFileTemp.getName();

                            amazonS3.Upload_data(BUCKETSUBFOLDER, mFileTemp.getName(), mFileTemp, new UploadFileAmazonS3.UploadCallBack() {
                                @Override
                                public void sucess(String success) {
                                    Log.d(TAG, "sucess: " + success);
                                    sendMessage(imageUrl, msgTypeDocument, String.valueOf(timeMillisImage));
                                    chatDataArry.get(documentPosition).setContent(imageUrl);
                                    chattingAdapter.notifyItemChanged(documentPosition);
                                    mFileTemp.delete();
                                    mFileTemp = null;
                                }

                                @Override
                                public void error(String errormsg) {
                                    Log.d(TAG, "error: " + errormsg);
                                    chatDataArry.remove(documentPosition);
                                    chattingAdapter.notifyItemChanged(documentPosition);
                                    Toast.makeText(ChattingActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();

                                }
                            });


                        }
                    } catch (Exception e) {
                        chatDataArry.remove(documentPosition);
                        chattingAdapter.notifyItemChanged(documentPosition);
                        Toast.makeText(ChattingActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();

                        e.printStackTrace();
                    }

                    break;

                case REQUEST_CODE_TAKE_PICTURE:
                    upDateToGallery();
                    startCropImage(mFileTemp.getPath());
                    break;

                case REQUEST_CODE_CROP_IMAGE:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path != null) {
                        timeMillisImage = System.currentTimeMillis();
                        notifyAdapter(String.valueOf(timeMillisImage), msgTypeImage, "", sessionManager.getProviderId(), customerId, "2", false);
                     //   new UploadFileToServer().execute(path);
                        String BUCKETSUBFOLDER = DOCUMENTS;
                        mFileTemp = new File(path);
                        amazonS3 = UploadFileAmazonS3.getInstance(this);
                        amazonS3.Upload_data(BUCKETSUBFOLDER, mFileTemp.getName(), mFileTemp, new UploadFileAmazonS3.UploadCallBack() {
                            @Override
                            public void sucess(String success) {
                                Log.d(TAG, "sucess: " + success);
                                sendMessage(success, msgTypeImage, String.valueOf(timeMillisImage));
                                chatDataArry.get(imagePosition).setContent(success);
                                chattingAdapter.notifyItemChanged(imagePosition);
                                mFileTemp.delete();
                                mFileTemp = null;
                            }

                            @Override
                            public void error(String errormsg) {
                                Log.d(TAG, "error: " + errormsg);
                                chatDataArry.remove(imagePosition);
                                chattingAdapter.notifyItemChanged(imagePosition);
                                Toast.makeText(ChattingActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getMimeType(Context context, Uri uri) {
        String extension = "";
        if (uri != null) {
            //Check uri format to avoid null
            if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                //If scheme is a content
                final MimeTypeMap mime = MimeTypeMap.getSingleton();
                extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
            } else {
                //If scheme is a File
                //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
                extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());
            }
            Log.w(TAG, "onActivityResult: " + extension);
        }
        return extension;
    }

    /**
     * method for starting CropImage Activity for crop the selected image
     * @param file
     */
    private void startCropImage(String file) {
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, file);
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        intent.putExtra("locale", Locale.US.getDisplayLanguage());
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }


    /**
     * Uploading the file to server
     */
    private class UploadFileToServer extends AsyncTask<String, Integer, String[]> {

        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            //  progressBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            //    progressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            //     progressBar.setProgress(progress[0]);

            // updating percentage value
            //     txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String[] doInBackground(String... params) {
            String result = "";
            String responseCode = "";
            String responseWithCode[] = new String[]{result, responseCode};

            try {
                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.connectTimeout(20, TimeUnit.SECONDS);
                builder.readTimeout(20, TimeUnit.SECONDS);
                builder.writeTimeout(20, TimeUnit.SECONDS);
                OkHttpClient httpClient = builder.build();
                httpClient.readTimeoutMillis();

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("photo", "Filename.png",
                                RequestBody.create(MediaType.parse("*/*"), new File(params[0])))
                        .build();

                Request request = new Request.Builder()
                        .url(ServiceUrl.CHAT_DATA_UPLOAD)
                        .post(requestBody)
                        .build();

                Response response = httpClient.newCall(request).execute();
                result = response.body().string();
                responseCode = String.valueOf(response.code());
                responseWithCode[0] = responseCode;
                responseWithCode[1] = result;

                Log.d(TAG, "doInBackground: " + responseCode);
                Log.d(TAG, "doInBackground: " + result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseWithCode;
        }

        @Override
        protected void onPostExecute(String[] result) {
            try {
                if (result[0].equals(RESPONSE_CODE_SUCCESS)) {
                    JSONObject jsonObject = new JSONObject(result[1]);
                    String image = jsonObject.getString("data");
                    sendMessage(image, msgTypeImage, String.valueOf(timeMillisImage));
                    chatDataArry.get(imagePosition).setContent(image);
                    chattingAdapter.notifyItemChanged(imagePosition);
                } else {
                    chatDataArry.remove(imagePosition);
                    chattingAdapter.notifyItemChanged(imagePosition);
                    Toast.makeText(ChattingActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                chatDataArry.remove(imagePosition);
                chattingAdapter.notifyItemChanged(imagePosition);
                Toast.makeText(ChattingActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(result);
        }

    }


    public String decryptPrivateKeyPostLollipop() {
        String str = "";
        Log.i("TEST", "decryptData marsh melloa ");
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE);
            c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
            byte[] encryptedBytes = Base64.decode(sessionManager.getVirgilPrivateKey(), Base64.DEFAULT);

            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str;
    }

    @SuppressLint("NewApi")

    public String decryptPublicKeyPostLollipop() {
        String str = "";
        Log.i("TEST", "decryptData marsh melloa ");
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE);
            c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
            byte[] encryptedBytes = Base64.decode(sessionManager.getVirgilPublicKey(), Base64.DEFAULT);

            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * <h2>getSecretPrivateKeyPreMarshMallow</h2>
     * This method is used to get the secret key for pre marsh mallow device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPrivateKeyPreMarshMallow() throws Exception {
        String enryptedKeyB64 = sessionManager.getEncryptionPrivateKey();
        Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
        // need to check null, omitted here
        byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaPrivateKeyDecrypt(encryptedKey);
        Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
        return new SecretKeySpec(key, "AES");
    }

    /**
     * <h2>getSecretPublicKeyPreMarshMallow</h2>
     * This method is used to get the secret key for pre marsh mallow device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPublicKeyPreMarshMallow() throws Exception {
        String enryptedKeyB64 = sessionManager.getEncryptionPublicKey();
        Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
        // need to check null, omitted here
        byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaPublicKeyDecrypt(encryptedKey);
        Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
        return new SecretKeySpec(key, "AES");
    }


    private byte[] rsaPrivateKeyDecrypt(byte[] encrypted) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
        CipherInputStream cipherInputStream = new CipherInputStream(
                new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }
        return bytes;
    }

    private byte[] rsaPublicKeyDecrypt(byte[] encrypted) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
        CipherInputStream cipherInputStream = new CipherInputStream(
                new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }
        return bytes;
    }


    public String decryptPrivateKeyPreMarshMallow() {
        Log.i("TEST", "decryptData else  ");
        Cipher c;
        String str = "";
        try {
            c = Cipher.getInstance(AES_MODE, "BC");
            c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPreMarshMallow(), IV_PARAMETER_SPEC);
            byte[] encryptedBytes = Base64.decode(sessionManager.getEncryptionPrivateKey(), Base64.DEFAULT);
            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);


        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }


    public String decryptPublicKeyPreMarshMallow() {
        Log.i("TEST", "decryptData else  ");
        Cipher c;
        String str = "";
        try {
            c = Cipher.getInstance(AES_MODE, "BC");
            c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPreMarshMallow(), IV_PARAMETER_SPEC);
            byte[] encryptedBytes = Base64.decode(sessionManager.getEncryptionPublicKey(), Base64.DEFAULT);
            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }


    /**
     * <h2>getSecretPrivateKeyPostLollipop</h2>
     * This method is used to get the secret key for post lollipop device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPrivateKeyPostLollipop() throws Exception {
        Key s = keyStore.getKey(KEY_PRIVATEALIAS, null);
        Log.i("TEST", "key " + s.toString());
        return keyStore.getKey(KEY_PRIVATEALIAS, null);
    }

    /**
     * <h2>getSecretPublicKeyPostLollipop</h2>
     * This method is used to get the secret key for post lollipop device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPublicKeyPostLollipop() throws Exception {
        Key s = keyStore.getKey(KEY_PUBLICALIAS, null);
        Log.i("TEST", "key " + s.toString());
        return keyStore.getKey(KEY_PUBLICALIAS, null);
    }


    public void checkForKeystore() {
        try {
            keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
            keyStore.load(null);
        } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
        }
        try {
            if (!keyStore.containsAlias(KEY_PRIVATEALIAS)) {
                //mSignUpView.checkForVersion();
            }
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }

    private String encryptData(String privateKey, String publicKey, String messageFromDecrypt, String messageToDecrypt) {

        try {
            CryptographyExample cryptographyExample = new CryptographyExample();
            VirgilCrypto crypto = new VirgilCrypto();
            List<VirgilPublicKey> receiversPublicKeys = new ArrayList<>();
            // Import private key
            VirgilPrivateKey importedPrivateKey = cryptographyExample.importPrivateKey(privateKey, crypto);
            // Import public key
            VirgilPublicKey importedPublicKey = cryptographyExample.importPublicKey(publicKey, crypto);
            VirgilPublicKey importedDoctorPublicKey = cryptographyExample.importPublicKey(sessionManager.getVirgilDoctorPublicKey(), crypto);
            byte[] signature = cryptographyExample.createSignature(importedPrivateKey, messageToDecrypt);
            // Verify data signature
            byte[] dataToSign = ConvertionUtils.toBytes(messageToDecrypt);
            boolean valid = cryptographyExample.verifySignature(signature, dataToSign, importedDoctorPublicKey, crypto);
            receiversPublicKeys.add(importedPublicKey);
            receiversPublicKeys.add(importedDoctorPublicKey);
            // Encrypt data
            byte[] encryptedData = cryptographyExample.dataEncryption(receiversPublicKeys, messageToDecrypt);
            messageFromDecrypt = Base64.encodeToString(encryptedData, Base64.DEFAULT);//Base64.encodeToString(encryptedData, Base64.DEFAULT);
        } catch (CryptoException e) {
            e.printStackTrace();
        }
        return messageFromDecrypt;
    }


    private String decryptData(String privateKey, String publicKey, String messageFromDecrypt, String messageToDecrypt) {

        try {
            CryptographyExample cryptographyExample = new CryptographyExample();
            VirgilCrypto crypto = new VirgilCrypto();
            Log.w(TAG, "encryptData: " + privateKey);
            // Import private key
            VirgilPrivateKey importedPrivateKey = cryptographyExample.importPrivateKey(privateKey, crypto);
            // Decrypt data
            //  byte[] encryptedData = Base64.decode(messageToDecrypt, Base64.DEFAULT);/*Base64.getMimeDecoder().decode(chatData.get(position).getContent());*/
            messageFromDecrypt = cryptographyExample.dataDecryption(messageToDecrypt, importedPrivateKey);

        } catch (CryptoException e) {
            e.printStackTrace();
        }
        return messageFromDecrypt;
    }

    public void upDateToGallery() {
        if (mFileTemp == null)
            return;
      /*  Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(temp_file.getPath());
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);*/
        // Log.d(TAG, "upDateToGallery: 1"+temp_file.getPath()+ "=====>\n"+temp_file.getAbsolutePath());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            final Uri contentUri = Uri.fromFile(mFileTemp);
            scanIntent.setData(contentUri);
            sendBroadcast(scanIntent);
        } else {
            sendBroadcast(
                    new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(mFileTemp.getAbsolutePath())));
        }
    }

}
