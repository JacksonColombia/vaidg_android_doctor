package com.vaidg.pro.main.profile.bank.bankrazorpay;

import android.content.Context;
import android.util.Patterns;

import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>BankRazorPayPresenter</h1>
 * BankRazorPayPresenter presenter for BankRazorPayActivity
 *
 * @see BankRazorPayActivity
 */

public class BankRazorPayPresenter implements BankRazorPayContract.Presenter {

    @Inject
    Context context;
    @Inject
    BankRazorPayContract.View view;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;

    @Inject
    BankRazorPayPresenter() {
    }


    @Override
    public void addBankDetails(JSONObject jsonObject) {
        if (view != null)
            view.startProgressBar();
        try {
            if (view != null) {
                if (jsonObject.getString("fullName").equals("")) {
                    view.onErrorFirstName();
                    return;
                } else if (jsonObject.getString("fullName").equals("")) {
                    view.onErrorLastName();
                    return;
                } else if (!Patterns.EMAIL_ADDRESS.matcher(jsonObject.getString("email")).matches()) {
                    view.onErrorEmail();
                    return;
                } else if (jsonObject.getString("contact").equals("")) {
                    view.onErrorCity();
                    return;
                }
            }
            service.createRazorPayAccount(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        if (view != null)
                                            view.stopProgressBar();
                                        if (response != null && !response.isEmpty()) {
                                            if (view != null)
                                                view.onSuccess(Utility.getMessage(response));
                                        } else {
                                            if (view != null)
                                                view.onFailure();
                                        }
                                        break;
                                    case SESSION_EXPIRED:
                                        onRefreshToken(getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                if (view != null)
                                                    view.stopProgressBar();
                                                addBankDetails(jsonObject);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                if (view != null) {
                                                    view.stopProgressBar();
                                                    view.onFailure();
                                                }
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                if (view != null)
                                                    view.stopProgressBar();
                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, context);
                                            }
                                        });
                                        break;
                                    case SESSION_LOGOUT:
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                        break;
                                    default:
                                        if (view != null) {
                                            view.stopProgressBar();
                                            view.onFailure(Utility.getMessage(errorBody));
                                        }
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        break;
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.stopProgressBar();
                                    view.onFailure();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.stopProgressBar();
                                view.onFailure();
                            }
                        }

                        @Override
                        public void onComplete() {
                        }
                    });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

}
