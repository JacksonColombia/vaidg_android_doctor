package com.vaidg.pro.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.main.profile.bank.bankdetails.BankDetailsActivity;
import com.vaidg.pro.main.profile.myratecard.CategoryRateByCallType;
import com.vaidg.pro.pojo.profile.ratecard.CategoryData;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * Created by murashid on 20-Mar-17.
 * <h1>BankListAdapter</h1>
 * BankList Recycler View adapter for bank list
 *
 * @see BankDetailsActivity
 */

public class MyRateCardCategoryAdapter extends RecyclerView.Adapter<MyRateCardCategoryAdapter.ViewHolder> implements View.OnClickListener {

    private static final String TAG = "CategoryListAdapter";
    private ArrayList<CategoryData> categoryData;
    private Typeface fontRegular;
    private Activity mActivity;

    public MyRateCardCategoryAdapter(Activity mActivity, ArrayList<CategoryData> categoryData) {
        this.categoryData = categoryData;
        this.mActivity = mActivity;
        fontRegular = Utility.getFontRegular(mActivity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyRateCardCategoryAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.select_category_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvCategory.setText(categoryData.get(position).getCat_name());

        if (!categoryData.get(position).getBilling_model().equals("7")) {
            holder.tvCategory.setOnClickListener(this);
            holder.tvCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.vector_color_primary_add_schedule_next_arrow, 0);
            holder.tvCategory.setTag(holder);
        } else {
            holder.tvCategory.setOnClickListener(null);
            holder.tvCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

    }

    @Override
    public int getItemCount() {
        return categoryData != null ? categoryData.size() : ZERO;
    }

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position = viewHolder.getAdapterPosition();
        switch (v.getId()) {
            case R.id.tvCategory:
                Intent intent = new Intent(mActivity, CategoryRateByCallType.class);
                intent.putExtra("category", categoryData.get(position));
                intent.putExtra("position", position);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    mActivity.startActivityForResult(intent, VariableConstant.REQUEST_CODE_SERVICE_CHANGED,
                            ActivityOptions.makeSceneTransitionAnimation(mActivity).toBundle());
                } else {
                    mActivity.startActivityForResult(intent, VariableConstant.REQUEST_CODE_SERVICE_CHANGED);
                    mActivity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
                mActivity.finish();
                break;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvCategory.setTypeface(fontRegular);
        }
    }
}
