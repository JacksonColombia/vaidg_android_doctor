package com.vaidg.pro.pojo.profile.helpcenter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 29-Dec-17.
 */

public class TicketData implements Serializable {
    @SerializedName("ticket_id")
    @Expose
    private String ticket_id;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("events")
    @Expose
    private ArrayList<TicketEvents> events;

    public String getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(String ticket_id) {
        this.ticket_id = ticket_id;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public ArrayList<TicketEvents> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<TicketEvents> events) {
        this.events = events;
    }
}
