package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ExternalAccounts implements Serializable {

  @SerializedName("data")
  @Expose
  private ArrayList<AccountData> data;
  @SerializedName("total_count")
  @Expose
  private int totalCount;
  @SerializedName("has_more")
  @Expose
  private boolean hasMore;
  @SerializedName("url")
  @Expose
  private String url;
  @SerializedName("object")
  @Expose
  private String object;

  public ArrayList<AccountData> getData() {
    return data;
  }

  public int getTotalCount() {
    return totalCount;
  }

  public boolean isHasMore() {
    return hasMore;
  }

  public String getUrl() {
    return url;
  }

  public String getObject() {
    return object;
  }

  @Override
  public String toString() {
    return "ExternalAccounts{" + "data = '" + data + '\'' + ",total_count = '" + totalCount + '\'' + ",has_more = '" + hasMore + '\'' + ",url = '" + url + '\'' + ",object = '" + object + '\'' + "}";
  }
}