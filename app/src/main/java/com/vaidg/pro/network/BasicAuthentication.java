package com.vaidg.pro.network;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.vaidg.pro.AppController;
import com.vaidg.pro.BuildConfig;
import com.vaidg.pro.utility.Utility;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.vaidg.pro.utility.ServiceUrl.HOST_URL;
import static com.vaidg.pro.utility.VariableConstant.AUTHORIZATION;


public class BasicAuthentication {
  public static boolean isConnected() {
    ConnectivityManager connectivityManager = (ConnectivityManager) AppController.getInstance().getSystemService(
            Context.CONNECTIVITY_SERVICE);
    NetworkInfo netInfo =
            connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
    return (netInfo != null && netInfo.isConnected());
  }

  public static class ServiceGenerator {

    //    private static OkHttpClient.Builder httpClient = SSLCertificate.getSSLCertificate(LSPApplication.get()).newBuilder();
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(HOST_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create());
    private static Retrofit retrofit = builder.build();

    public static <S> S createService(Class<S> serviceClass, String username, String password) {
      if (!Utility.isTextEmpty(username) && !Utility.isTextEmpty(password)) {
        String authToken = Credentials.basic(username, password);
        return createService(serviceClass, authToken);
      }
      return createService(serviceClass, null);
    }

    public static <S> S createService(Class<S> serviceClass, final String authToken) {
      if (!Utility.isTextEmpty(authToken)) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        AuthenticationInterceptor interceptor = new AuthenticationInterceptor(authToken);
        if (!httpClient.interceptors().contains(interceptor)) {
          httpClient.readTimeout(50, TimeUnit.SECONDS);
          httpClient.writeTimeout(30, TimeUnit.SECONDS);
          httpClient.connectTimeout(80, TimeUnit.SECONDS);
          httpClient.retryOnConnectionFailure(false);
          httpClient.addInterceptor(interceptor);
          httpClient.addInterceptor(loggingInterceptor);
          builder.client(httpClient.build());
          retrofit = builder.build();
        }
      }
      return retrofit.create(serviceClass);
    }

    public <S> S createService(Class<S> serviceClass) {
      return createService(serviceClass, null, null);
    }
  }

  private static class AuthenticationInterceptor implements Interceptor {
    private String authToken;

    public AuthenticationInterceptor(String token) {
      this.authToken = token;
    }

    @NotNull
    @Override
    public okhttp3.Response intercept(@NotNull Chain chain) throws IOException {
      if (!isConnected()) {
        throw new NoConnectivityException();
        // Throwing our custom exception 'NoConnectivityException'
      }
      okhttp3.Request original = chain.request();
      okhttp3.Request.Builder builder = original.newBuilder()
              .header(AUTHORIZATION, authToken);
      okhttp3.Request request = builder.build();
      return chain.proceed(request);
    }
  }


}
