package com.vaidg.pro.pojo.signup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 */

public class CategoriesData implements Serializable {
    @SerializedName("catId")
    @Expose
    private String catId;
    @SerializedName("catName")
    @Expose
    private String catName;
    @SerializedName("document")
    @Expose
    private ArrayList<CategoryDocument> document;
    @SerializedName("subCategory")
    @Expose
    private ArrayList<SubCategory> subCategory;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public ArrayList<CategoryDocument> getDocument() {
        return document;
    }

    public void setDocument(ArrayList<CategoryDocument> document) {
        this.document = document;
    }

    public ArrayList<SubCategory> getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(ArrayList<SubCategory> subCategory) {
        this.subCategory = subCategory;
    }
}
