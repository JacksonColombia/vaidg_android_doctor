package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.landing.signup.SignupSelectionListener;
import com.vaidg.pro.pojo.signup.CityData;
import com.vaidg.pro.pojo.signup.SubCategory;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>CCity Recycler view adapter for displaying list of city in Signup screen</h1>
 *
 * @see com.vaidg.pro.landing.signup.SignupActivity
 */

public class SignupSelectionListCheckBoxAdapter extends RecyclerView.Adapter<SignupSelectionListCheckBoxAdapter.ViewHolder> {
    private Context context;
    private ArrayList<CityData> cityDatas;
    private ArrayList<SubCategory> subCategories;
    private String type;
    private SignupSelectionListener signupSelectionListener;
    private Typeface fontMedium;

    public SignupSelectionListCheckBoxAdapter(Context context, ArrayList<CityData> cityDatas, ArrayList<SubCategory> subCategories,
                                              String type, SignupSelectionListener signupSelectionListener) {
        this.context = context;
        this.cityDatas = cityDatas;
        this.subCategories = subCategories;
        this.type = type;
        this.signupSelectionListener = signupSelectionListener;
        fontMedium = Utility.getFontMedium(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_signup_selection_checkbox, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (type.equals("city")) {
            holder.cbSignupSelection.setText(cityDatas.get(position).getCity());
            holder.cbSignupSelection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signupSelectionListener.onCitySelecting(cityDatas.get(holder.getAdapterPosition()).getId(), cityDatas.get(holder.getAdapterPosition()).getCity());
                    Utility.setTextColor(context, holder.cbSignupSelection, R.color.colorPrimary);
                }
            });
        } else {
            holder.cbSignupSelection.setText(subCategories.get(position).getSub_cat_name());
            if (subCategories.get(position).isAlreadySelected()) {
                Utility.setTextColor(context, holder.cbSignupSelection, R.color.colorPrimary);
                holder.cbSignupSelection.setChecked(true);
            } else {
                Utility.setTextColor(context, holder.cbSignupSelection, R.color.darkTextColor);
                holder.cbSignupSelection.setChecked(false);
            }

            holder.cbSignupSelection.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    signupSelectionListener.onSubCategorySelecting(subCategories.get(holder.getAdapterPosition()).get_id(), subCategories.get(holder.getAdapterPosition()).getSub_cat_name());
                    if (b) {
                        Utility.setTextColor(context, holder.cbSignupSelection, R.color.colorPrimary);
                    } else {
                        Utility.setTextColor(context, holder.cbSignupSelection, R.color.darkTextColor);
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if (type.equals("city")) {
            return cityDatas != null ? cityDatas.size() : ZERO;
        } else {
            return subCategories != null ? subCategories.size() : ZERO;
        }

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox cbSignupSelection;

        ViewHolder(View itemView) {
            super(itemView);
            cbSignupSelection = itemView.findViewById(R.id.cbSignupSelection);
            cbSignupSelection.setTypeface(fontMedium);
        }
    }


}
