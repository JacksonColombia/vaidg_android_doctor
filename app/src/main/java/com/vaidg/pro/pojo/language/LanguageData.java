package com.vaidg.pro.pojo.language;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 24-Apr-18.
 */

public class LanguageData implements Serializable{
    @SerializedName("lan_name")
    @Expose
    private String lan_name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("isChecked")
    @Expose
    private boolean isChecked = false;
    @SerializedName("res_id")
    @Expose
    private String res_id;

    public String getRes_id() {
        return res_id;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public String getLan_name() {
        return lan_name;
    }

    public void setLan_name(String lan_name) {
        this.lan_name = lan_name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
