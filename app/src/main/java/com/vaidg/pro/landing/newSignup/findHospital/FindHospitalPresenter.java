package com.vaidg.pro.landing.newSignup.findHospital;

import android.app.Activity;
import android.content.Intent;

import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.landing.newSignup.findHospital.model.FindHospitalCallback;
import com.vaidg.pro.landing.newSignup.findHospital.model.FindHospitalModel;
import com.vaidg.pro.landing.newSignup.findHospital.model.HospitalDataPojo;
import com.vaidg.pro.landing.newSignup.findHospital.model.HospitalPojo;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationPojo;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.utility.Utility;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

public class FindHospitalPresenter implements FindHospitalContract.Presenter, FindHospitalCallback {

    @Inject
    FindHospitalContract.View view;

    @Inject
    NetworkService service;

    @Inject
    NetworkStateHolder networkStateHolder;

    @Inject
    Activity activity;

    @Inject
    FindHospitalModel model;

    @Inject
    Gson gson;

    @Inject
    public FindHospitalPresenter() {
    }


    @Override
    public void findHospital(String cityId, String place) {
        if (networkStateHolder.isConnected()) {
            if (view != null)
                view.showProgress();
            Utility.basicAuth(service).findHospital(model.getLanguage(), model.getPlatform(), cityId, place, 20, 0)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            int statusCode = value.code();
                            try {
                                String response = value.body() != null
                                        ? value.body().string() : null;
                                String errorBody = value.errorBody() != null
                                        ? value.errorBody().string() : null;
                                if (statusCode == SUCCESS_RESPONSE) {
                                    HospitalPojo pojo = gson.fromJson(response, HospitalPojo.class);
                                    model.parseData(pojo);
                                    if (view != null) {
                                        view.hideProgress();
                                        view.notifyAdapter();
                                    }
                                } else {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.showError(Utility.getMessage(errorBody));
                                    }
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            view.hideProgress();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void onItemSelect(int position) {
        HospitalDataPojo dataPojo = model.getSelectedHospital(position);
        Intent intent = new Intent();
        intent.putExtra("clinic", dataPojo);
        view.sendBackData(intent);
    }
}
