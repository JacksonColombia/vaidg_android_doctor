package com.vaidg.pro.landing.newSignup.model;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import com.vaidg.pro.AppController;
import com.vaidg.pro.BaseModel;
import com.vaidg.pro.landing.newSignup.consultaionFee.model.ConsultationData;
import com.vaidg.pro.landing.newSignup.findHospital.model.HospitalDataPojo;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.pojo.profile.DegreeDetailsData;
import com.vaidg.pro.pojo.signup.CityData;
import com.vaidg.pro.pojo.signup.DegreeData;
import com.vaidg.pro.utility.ApiOnServer;
import com.vaidg.pro.utility.DeviceUuidFactory;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import static com.vaidg.pro.utility.VariableConstant.PROVIDER_UNDER_HOSPITAL;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DOCUMENT;

public class SignUpModel extends BaseModel {

    @Inject
    Utility utility;

    @Inject
    DeviceUuidFactory deviceUuidFactory;

    @Inject
    SessionManager sessionManager;

    HospitalDataPojo clinic;
    ArrayList<ConsultationData> consultationData;
    String yearOfExp;
    ArrayList<DegreeDetailsData> educationDetails;
    SelectSpecializationData specializationData;
    CityData cityData;
    String profile;
    private JSONObject profileData;
    int batterylevel;

    @Inject
    public SignUpModel() {
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public boolean isValidPassword(String pass) {
        String regex = "(.)*(\\d)(.)*";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pass);
        if (pass.length() < 7) {
            return false;
        } else if (pass.equals(pass.toLowerCase())) {
            return false;
        } else if (pass.equals(pass.toUpperCase())) {
            return false;
        } else if (!matcher.matches()) {
            return false;
        }
        return true;
    }

    public Map<String, Object> sendEmailValidate(String email) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("email", email);
        return hashMap;
    }

    public Map<String, Object> sendPhoneNumberValidate(String countryCode, String phone) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("countryCode", countryCode);
        body.put("mobile", phone);
        return body;
    }

    public HospitalDataPojo getClinic() {
        return clinic;
    }

    public void setClinic(HospitalDataPojo clinic) {
        this.clinic = clinic;
    }

    public ArrayList<ConsultationData> getConsultationData() {
        return consultationData;
    }

    public void setConsultationData(ArrayList<ConsultationData> consultationData) {
        this.consultationData = consultationData;
    }

    public String getYearOfExp() {
        return yearOfExp;
    }

    public void setYearOfExp(String yearOfExp) {
        this.yearOfExp = yearOfExp;
    }

    public ArrayList<DegreeDetailsData> getEducationDetails() {
        return educationDetails;
    }

    public void setEducationDetails(ArrayList<DegreeDetailsData> educationDetails) {
        this.educationDetails = educationDetails;
    }

    public SelectSpecializationData getSpecializationData() {
        return specializationData;
    }

    public void setSpecializationData(SelectSpecializationData specializationData) {
        this.specializationData = specializationData;
    }

    public CityData getCityData() {
        return cityData;
    }

    public void setCityData(CityData cityData) {
        this.cityData = cityData;
    }

    public JSONObject getProfileData() {
        return profileData;
    }

    public void setProfileData(String title, String firstName, String lastName,
                               String DOB, int gender, String referralCode, String email,
                               String ccp, String phoneNumber, String password) throws JSONException {
        this.profileData = new JSONObject();
        this.profileData.put(ApiOnServer.SignUp.TITLE, title);
        this.profileData.put(ApiOnServer.SignUp.FIRST_NAME, firstName);
        this.profileData.put(ApiOnServer.SignUp.LAST_NAME, lastName);
        this.profileData.put(ApiOnServer.SignUp.DOB, DOB);
        this.profileData.put(ApiOnServer.SignUp.GENDER, String.valueOf(gender));
        this.profileData.put(ApiOnServer.SignUp.REFERRAL_CODE, referralCode);
        this.profileData.put(ApiOnServer.SignUp.EMAIL, email);
        this.profileData.put(ApiOnServer.SignUp.PASSWORD, password);
        this.profileData.put(ApiOnServer.SignUp.COUNTRY_CODE, ccp);
        this.profileData.put(ApiOnServer.SignUp.MOBILE, phoneNumber);
    }

    public JSONObject sendSendData(JSONObject body) throws JSONException {
        body.put(ApiOnServer.SignUp.PROFILE_PIC, getProfile());
        body.put(ApiOnServer.SignUp.CITY_ID, getCityData().getId());
        body.put(ApiOnServer.SignUp.CAT_LIST, getSpecializationData().getCatId());
        body.put(ApiOnServer.SignUp.DOCTOR_TYPE, getClinic().getType());
        if (getClinic().getType() == PROVIDER_UNDER_HOSPITAL) {
            body.put(ApiOnServer.SignUp.HOSPITAL_ID, getClinic().getId());
        } else {
            body.put(ApiOnServer.SignUp.CLINIC_NAME, getClinic().getClinicName());
            body.put(ApiOnServer.SignUp.CLINIC_LOGO_WEB, getClinic().getClinicLogoWeb());
            body.put(ApiOnServer.SignUp.CLINIC_LOGO_APP, getClinic().getClinicLogoApp());
        }
        body.put(ApiOnServer.SignUp.META_DATA, getMetaData());

        body.put(ApiOnServer.SignUp.LATITUDE, getClinic().getLatitude());
        body.put(ApiOnServer.SignUp.LONGITUDE, getClinic().getLongitude());
        body.put(ApiOnServer.SignUp.ADD_LINE_1, getClinic().getAddress());
        body.put(ApiOnServer.SignUp.ADD_LINE_2, getClinic().getAddress());
        body.put(ApiOnServer.SignUp.CITY, Utility.isTextEmpty(getClinic().getCity()) ? "" : getClinic().getCity());
        body.put(ApiOnServer.SignUp.STATE, getClinic().getState());
        body.put(ApiOnServer.SignUp.COUNTRY, getClinic().getCountry());
        body.put(ApiOnServer.SignUp.PLACE_ID, getClinic().getPlaceId());
        body.put(ApiOnServer.SignUp.PINCODE, getClinic().getPostcode());
        body.put(ApiOnServer.SignUp.TAGGED_AS, "clinic");
        body.put(ApiOnServer.SignUp.DEVICE_ID, deviceUuidFactory.getDeviceUuid());
        body.put(ApiOnServer.SignUp.DEVICE_TYPE, VariableConstant.DEVICE_TYPE);
        body.put(ApiOnServer.SignUp.DEVICE_MAKE, VariableConstant.DEVICE_MAKER);
        body.put(ApiOnServer.SignUp.DEVICE_MODEL, VariableConstant.DEVICE_MODEL);
        body.put(ApiOnServer.SignUp.DEVICE_OS_VERSION, android.os.Build.VERSION.RELEASE);
        body.put(ApiOnServer.SignUp.BATTERY_PERC, getBattery());
        body.put(ApiOnServer.SignUp.LOCATION_HEADING, "1");
        body.put(ApiOnServer.SignUp.APP_VERSION, VariableConstant.APP_VERSION);
        body.put(ApiOnServer.SignUp.DEVICE_TIME, Utility.getCurrentTime());
        body.put(ApiOnServer.SignUp.DEGREE, getDegreeData());
        body.put(ApiOnServer.SignUp.IN_CALL_FEE, getConsultationData().get(1).getFee());
        body.put(ApiOnServer.SignUp.OUT_CALL_FEE, getConsultationData().get(0).getFee());
        body.put(ApiOnServer.SignUp.TELE_CALL_FEE, getConsultationData().get(2).getFee());
        body.put(ApiOnServer.SignUp.YEAR_OF_EXP, getYearOfExp());
        body.put(ApiOnServer.SignUp.PRIVATE_KEY, sessionManager.getVirgilPrivateKeyT());
        body.put(ApiOnServer.SignUp.PUBLIC_KEY, sessionManager.getVirgilPublicKeyT());
        return body;
    }

    private JSONArray getMetaData() throws JSONException {
        JSONArray arrayMap = new JSONArray();
        ArrayList<Attribute> attributes = (ArrayList<Attribute>) getSpecializationData().getAttributes();
        for (Attribute array : attributes) {
            JSONObject childJson = new JSONObject();
            childJson.put("metaId", array.getId());
            childJson.put("data", array.getData());
            childJson.put("name", array.getName());
            childJson.put("type", array.getType());
            if (array.getType() == SPECIALIZATION_VIEW_DOCUMENT) {
                JSONArray predefineArray = new JSONArray();
                for (PreDefined preDefined : array.getPreDefined()) {
                    JSONObject subChildJson = new JSONObject();
                    subChildJson.put("_id", preDefined.getId());
                    subChildJson.put("name", preDefined.getName());
                    subChildJson.put("icon", preDefined.getIcon());
                    subChildJson.put("type", preDefined.getType());
                    subChildJson.put("isManadatory", preDefined.getIsManadatory());
                    subChildJson.put("data", preDefined.getData());
                    predefineArray.put(subChildJson);
                }
                childJson.put("preDefined", predefineArray);
            }
            childJson.put("queForProviderSignup", array.getQueForProviderSignup());
            childJson.put("mandatoryForProvider", array.getMandatoryForProvider());
            arrayMap.put(childJson);
        }
        return arrayMap;
    }

    private int getBattery() {
        Context context = AppController.getContext();
        Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = 0;
        int scale = 0;
        if (batteryIntent != null) {
            level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        }

        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            return 50;
        }

        return (int) (((float) level / (float) scale) * 100.0f);
    }

    private JSONArray getDegreeData() throws JSONException {
        JSONArray arrayMap = new JSONArray();
        ArrayList<DegreeDetailsData> arrayList = getEducationDetails();
        for (DegreeDetailsData degreeData : arrayList) {
            JSONObject childJson = new JSONObject();
            childJson.put("name", degreeData.getName());
            childJson.put("college", degreeData.getCollege());
            childJson.put("year", degreeData.getYear());
            childJson.put("id", degreeData.getId());
            arrayMap.put(childJson);
        }
        return arrayMap;
    }
}
