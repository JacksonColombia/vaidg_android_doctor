package com.vaidg.pro.landing.newSignup.educationBackground;

import android.app.Activity;
import android.content.Intent;

import com.vaidg.pro.landing.newSignup.educationBackground.model.EducationDetailsCallBack;
import com.vaidg.pro.landing.newSignup.educationBackground.model.EducationDetailsModel;
import com.vaidg.pro.pojo.profile.DegreeDetailsData;
import com.vaidg.pro.utility.AppConfig;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * <h1>@EducationDetailsPresenter</h1>
 * <p>this is presenter class used for bussiness logic and call  webservices</p>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 27/04/2020.
 **/
public class EducationDetailsPresenter implements EducationDetailsContract.Presenter, EducationDetailsCallBack {

    @Inject
    EducationDetailsContract.View view;

    @Inject
    EducationDetailsModel model;

    private int clickedPosition;

    @Inject
    public EducationDetailsPresenter() {
    }

    @Override
    public void parseResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == EducationDetailsActivity.ADD_EDIT_EDUCATION) {
                DegreeDetailsData pojo = data.getParcelableExtra(AppConfig.AddEducationDetails);
                if (pojo != null)
                    if (!model.isEditedData(pojo)) {
                        model.addDataToList(pojo);
                    } else {
                        model.addEditData(clickedPosition, pojo);
                    }
                if (view != null)
                    view.notifyAdapter();
            }
        }
    }


    @Override
    public void addAllDataToList(ArrayList<DegreeDetailsData> data) {
        model.addAllDataToList(data);
    }

    @Override
    public void sendBackSaveData() {
        Intent intent = new Intent();
        intent.putExtra("Education", model.getdata());
        view.sendBackSaveData(intent);
    }

    @Override
    public void EditOnSelect(int position) {
        clickedPosition = position;
        view.EditOnSelect(model.getPositionData(position));
    }
}
