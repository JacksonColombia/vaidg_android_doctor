package com.vaidg.pro.landing.newSignup.SpecializationCatagory.previewScreen;


import com.vaidg.pro.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>PreviewSceenDaggerModule</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
@Module
public interface PreviewSceenDaggerModule {

    @FragmentScoped
    @Binds
    PreviewSceenContract.Presenter providePresenter(PreviewSceenPresenter presenter);

    @FragmentScoped
    @Binds
    PreviewSceenFragment provideView(PreviewSceenFragment fragment);


}
