package com.vaidg.pro.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Attribute implements Parcelable {
    public static final Creator<Attribute> CREATOR = new Creator<Attribute>() {
        @Override
        public Attribute createFromParcel(Parcel source) {
            return new Attribute(source);
        }

        @Override
        public Attribute[] newArray(int size) {
            return new Attribute[size];
        }
    };
    @SerializedName("position")
    @Expose
    private int position;
    @SerializedName("list_no")
    @Expose
    private int list_no;
    @SerializedName("max_count")
    @Expose
    private int max_count;
    @SerializedName("media")
    @Expose
    private ArrayList<String> media;
    @SerializedName("sliderData")
    @Expose
    private int sliderData;
    @SerializedName("EditTextField")
    @Expose
    private String EditTextField;
    @SerializedName("data")
    @Expose
    private String data;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("num")
    @Expose
    private Integer num;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("minimum")
    @Expose
    private String minimum;
    @SerializedName("maximum")
    @Expose
    private String maximum;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("linkedToProvider")
    @Expose
    private Boolean linkedToProvider;
    @SerializedName("queForProviderSignup")
    @Expose
    private String queForProviderSignup;
    @SerializedName("adminVerificationRequired")
    @Expose
    private Boolean adminVerificationRequired;
    @SerializedName("visibleOnProviderProfile")
    @Expose
    private Boolean visibleOnProviderProfile;
    @SerializedName("mandatoryForProvider")
    @Expose
    private Boolean mandatoryForProvider;
    @SerializedName("queEnableForBooking")
    @Expose
    private Boolean queEnableForBooking;
    @SerializedName("queForBooking")
    @Expose
    private String queForBooking;
    @SerializedName("mandatoryForCustomer")
    @Expose
    private Boolean mandatoryForCustomer;
    @SerializedName("filterable")
    @Expose
    private Boolean filterable;
    @SerializedName("filterTitle")
    @Expose
    private String filterTitle;
    @SerializedName("attributName")
    @Expose
    private AttributName attributName;
    @SerializedName("attributeDescription")
    @Expose
    private AttributeDescription attributeDescription;
    @SerializedName("queForProviderSingupByLang")
    @Expose
    private QueForProviderSingupByLang queForProviderSingupByLang;
    @SerializedName("queForBookingByLang")
    @Expose
    private QueForBookingByLang queForBookingByLang;
    @SerializedName("filterTitelLang")
    @Expose
    private FilterTitelLang filterTitelLang;
    @SerializedName("preDefined")
    @Expose
    private List<PreDefined> preDefined = null;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("editableOnProvider")
    @Expose
    private Boolean editableOnProvider;

    public Attribute() {
    }

    protected Attribute(Parcel in) {
        this.position = in.readInt();
        this.list_no = in.readInt();
        this.max_count = in.readInt();
        this.media = in.createStringArrayList();
        this.sliderData = in.readInt();
        this.EditTextField = in.readString();
        this.data = in.readString();
        this.id = in.readString();
        this.num = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.type = (Integer) in.readValue(Integer.class.getClassLoader());
        this.description = in.readString();
        this.minimum = in.readString();
        this.maximum = in.readString();
        this.unit = in.readString();
        this.linkedToProvider = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.queForProviderSignup = in.readString();
        this.adminVerificationRequired = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.visibleOnProviderProfile = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mandatoryForProvider = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.queEnableForBooking = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.queForBooking = in.readString();
        this.mandatoryForCustomer = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.filterable = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.filterTitle = in.readString();
        this.attributName = in.readParcelable(AttributName.class.getClassLoader());
        this.attributeDescription = in.readParcelable(AttributeDescription.class.getClassLoader());
        this.queForProviderSingupByLang = in.readParcelable(QueForProviderSingupByLang.class.getClassLoader());
        this.queForBookingByLang = in.readParcelable(QueForBookingByLang.class.getClassLoader());
        this.filterTitelLang = in.readParcelable(FilterTitelLang.class.getClassLoader());
        this.preDefined = in.createTypedArrayList(PreDefined.CREATOR);
        this.icon = in.readString();
        this.editableOnProvider = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMinimum() {
        return minimum;
    }

    public void setMinimum(String minimum) {
        this.minimum = minimum;
    }

    public String getMaximum() {
        return maximum;
    }

    public void setMaximum(String maximum) {
        this.maximum = maximum;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Boolean getLinkedToProvider() {
        return linkedToProvider;
    }

    public void setLinkedToProvider(Boolean linkedToProvider) {
        this.linkedToProvider = linkedToProvider;
    }

    public String getQueForProviderSignup() {
        return queForProviderSignup;
    }

    public void setQueForProviderSignup(String queForProviderSignup) {
        this.queForProviderSignup = queForProviderSignup;
    }

    public Boolean getAdminVerificationRequired() {
        return adminVerificationRequired;
    }

    public void setAdminVerificationRequired(Boolean adminVerificationRequired) {
        this.adminVerificationRequired = adminVerificationRequired;
    }

    public Boolean getVisibleOnProviderProfile() {
        return visibleOnProviderProfile;
    }

    public void setVisibleOnProviderProfile(Boolean visibleOnProviderProfile) {
        this.visibleOnProviderProfile = visibleOnProviderProfile;
    }

    public Boolean getMandatoryForProvider() {
        return mandatoryForProvider;
    }

    public void setMandatoryForProvider(Boolean mandatoryForProvider) {
        this.mandatoryForProvider = mandatoryForProvider;
    }

    public Boolean getQueEnableForBooking() {
        return queEnableForBooking;
    }

    public void setQueEnableForBooking(Boolean queEnableForBooking) {
        this.queEnableForBooking = queEnableForBooking;
    }

    public String getQueForBooking() {
        return queForBooking;
    }

    public void setQueForBooking(String queForBooking) {
        this.queForBooking = queForBooking;
    }

    public Boolean getMandatoryForCustomer() {
        return mandatoryForCustomer;
    }

    public void setMandatoryForCustomer(Boolean mandatoryForCustomer) {
        this.mandatoryForCustomer = mandatoryForCustomer;
    }

    public Boolean getFilterable() {
        return filterable;
    }

    public void setFilterable(Boolean filterable) {
        this.filterable = filterable;
    }

    public String getFilterTitle() {
        return filterTitle;
    }

    public void setFilterTitle(String filterTitle) {
        this.filterTitle = filterTitle;
    }

    public AttributName getAttributName() {
        return attributName;
    }

    public void setAttributName(AttributName attributName) {
        this.attributName = attributName;
    }

    public AttributeDescription getAttributeDescription() {
        return attributeDescription;
    }

    public void setAttributeDescription(AttributeDescription attributeDescription) {
        this.attributeDescription = attributeDescription;
    }

    public QueForProviderSingupByLang getQueForProviderSingupByLang() {
        return queForProviderSingupByLang;
    }

    public void setQueForProviderSingupByLang(QueForProviderSingupByLang queForProviderSingupByLang) {
        this.queForProviderSingupByLang = queForProviderSingupByLang;
    }

    public QueForBookingByLang getQueForBookingByLang() {
        return queForBookingByLang;
    }

    public void setQueForBookingByLang(QueForBookingByLang queForBookingByLang) {
        this.queForBookingByLang = queForBookingByLang;
    }

    public FilterTitelLang getFilterTitelLang() {
        return filterTitelLang;
    }

    public void setFilterTitelLang(FilterTitelLang filterTitelLang) {
        this.filterTitelLang = filterTitelLang;
    }

    public List<PreDefined> getPreDefined() {
        return preDefined;
    }

    public void setPreDefined(List<PreDefined> preDefined) {
        this.preDefined = preDefined;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getEditableOnProvider() {
        return editableOnProvider;
    }

    public void setEditableOnProvider(Boolean editableOnProvider) {
        this.editableOnProvider = editableOnProvider;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getList_no() {
        return list_no;
    }

    public void setList_no(int list_no) {
        this.list_no = list_no;
    }

    public int getMax_count() {
        return max_count;
    }

    public void setMax_count(int max_count) {
        this.max_count = max_count;
    }

    public ArrayList<String> getMedia() {
        return media;
    }

    public void setMedia(ArrayList<String> media) {
        this.media = media;
    }

    public int getSliderData() {
        return sliderData;
    }

    public void setSliderData(int sliderData) {
        this.sliderData = sliderData;
    }

    public String getEditTextField() {
        return EditTextField;
    }

    public void setEditTextField(String editTextField) {
        EditTextField = editTextField;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.position);
        dest.writeInt(this.list_no);
        dest.writeInt(this.max_count);
        dest.writeStringList(this.media);
        dest.writeInt(this.sliderData);
        dest.writeString(this.EditTextField);
        dest.writeString(this.data);
        dest.writeString(this.id);
        dest.writeValue(this.num);
        dest.writeString(this.name);
        dest.writeValue(this.type);
        dest.writeString(this.description);
        dest.writeString(this.minimum);
        dest.writeString(this.maximum);
        dest.writeString(this.unit);
        dest.writeValue(this.linkedToProvider);
        dest.writeString(this.queForProviderSignup);
        dest.writeValue(this.adminVerificationRequired);
        dest.writeValue(this.visibleOnProviderProfile);
        dest.writeValue(this.mandatoryForProvider);
        dest.writeValue(this.queEnableForBooking);
        dest.writeString(this.queForBooking);
        dest.writeValue(this.mandatoryForCustomer);
        dest.writeValue(this.filterable);
        dest.writeString(this.filterTitle);
        dest.writeParcelable(this.attributName, flags);
        dest.writeParcelable(this.attributeDescription, flags);
        dest.writeParcelable(this.queForProviderSingupByLang, flags);
        dest.writeParcelable(this.queForBookingByLang, flags);
        dest.writeParcelable(this.filterTitelLang, flags);
        dest.writeTypedList(this.preDefined);
        dest.writeString(this.icon);
        dest.writeValue(this.editableOnProvider);
    }
}
