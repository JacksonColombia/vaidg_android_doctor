package com.vaidg.pro.main.earning;

import com.vaidg.pro.utility.SessionManager;

import javax.inject.Inject;

public class EarningWebPresenterImple implements EarningWebContract.Presenter {

    @Inject
    SessionManager sessionManager;

    @Inject
    public EarningWebPresenterImple() {
    }

    @Override
    public String getProviderId() {
        return sessionManager.getProviderId();
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }
}
