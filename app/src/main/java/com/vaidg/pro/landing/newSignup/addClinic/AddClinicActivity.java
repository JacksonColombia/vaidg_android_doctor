package com.vaidg.pro.landing.newSignup.addClinic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;

import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityAddClinicBinding;
import com.vaidg.pro.landing.newSignup.findHospital.FindHospitalActivity;
import com.vaidg.pro.landing.newSignup.selfOwnedClinic.SelfOwnedClinicActivity;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import javax.inject.Inject;

/**
 * <h1>AddClinicActivity</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 25/04/2020.
 **/
public class AddClinicActivity extends BaseDaggerActivity implements AddClinicContract.View {

    @Inject
    AddClinicContract.Presenter presenter;

    private ActivityAddClinicBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAddClinicBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        intiView();
        initOnClickListener();
    }

    @Override
    public void initOnClickListener() {
        binding.tvFindHosp.setOnClickListener(this::onClick);
        binding.tvSelfOwnedClinic.setOnClickListener(this::onClick);
    }

    @Override
    public void intiView() {
        binding.tbAddClinic.tvTitle.setText(getString(R.string.addClinic));
        setSupportActionBar(binding.tbAddClinic.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.tbAddClinic.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvFindHosp:
                launchFindHospital();
                break;

            case R.id.tvSelfOwnedClinic:
                launchOwnClinic();
                break;
        }
    }

    @Override
    public void sendBackData(Intent intent) {
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void launchOwnClinic() {
        Intent intent = new Intent(this, SelfOwnedClinicActivity.class);
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_FIND_HOSPITAL);
    }

    private void launchFindHospital() {
        Intent intent = new Intent(this, FindHospitalActivity.class);
        intent.putExtra("cityId", getIntent().getStringExtra("cityId"));
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_OWN_CLINIC);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        presenter.parseOnActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

    }
}
