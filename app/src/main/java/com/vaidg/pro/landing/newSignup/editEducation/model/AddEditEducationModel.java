package com.vaidg.pro.landing.newSignup.editEducation.model;

import com.vaidg.pro.BaseModel;
import com.vaidg.pro.pojo.signup.DegreeData;
import com.vaidg.pro.utility.Utility;

import javax.inject.Inject;

public class AddEditEducationModel extends BaseModel {


    DegreeData degreeData;

    @Inject
    public AddEditEducationModel() {
    }

    public DegreeData getDegreeData() {
        return degreeData;
    }

    public void setDegreeData(DegreeData degreeData) {
        this.degreeData = degreeData;
    }

}
