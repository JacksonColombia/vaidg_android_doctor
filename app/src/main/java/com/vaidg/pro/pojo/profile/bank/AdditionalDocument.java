package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AdditionalDocument implements Serializable {

	@SerializedName("back")
	@Expose
	private Object back;

	@SerializedName("details")
	@Expose
	private Object details;

	@SerializedName("front")
	@Expose
	private Object front;

	@SerializedName("details_code")
	@Expose
	private Object detailsCode;


	public Object getBack() {
		return back;
	}

	public Object getDetails() {
		return details;
	}

	public Object getFront() {
		return front;
	}

	public Object getDetailsCode() {
		return detailsCode;
	}

	@Override
	public String toString() {
		return "AdditionalDocument{" + "back = '" + back + '\'' + ",details = '" + details + '\'' +
				",front = '" + front + '\'' + ",details_code = '" + detailsCode + '\'' + "}";
	}
}