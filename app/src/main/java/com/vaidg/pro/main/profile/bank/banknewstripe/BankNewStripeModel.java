package com.vaidg.pro.main.profile.bank.banknewstripe;

import android.os.AsyncTask;
import android.util.Log;
import android.util.Patterns;

import com.vaidg.pro.utility.OkHttp3ConnectionStatusCode;
import com.vaidg.pro.utility.ServiceUrl;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import com.vaidg.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>BankNewStripeModel</h1>
 * BankNewStripeModel model for BankNewStripeActivity
 *
 * @see BankNewStripeActivity
 */

public class BankNewStripeModel {

    private static final String TAG = "BankNewStripeModel";
    private BankNewStripModelImplement bankNewStripModelImplement;
    private String imageUrl = "";

    BankNewStripeModel(BankNewStripModelImplement bankNewStripModelImplement) {
        this.bankNewStripModelImplement = bankNewStripModelImplement;
    }


    void fetchIp() {
        new getIpAddress().execute();
    }

    /**
     * method for add bank account
     *
     * @param token          session token
     * @param jsonObject     required field in json object
     * @param isPictureTaken boolen for picture is taken or not
     * @param amazonS3       object of UploadFileAmazonS3 class
     * @param mFileTemp      file that need to upload in amazon
     */
    void addBankAccount(String token, JSONObject jsonObject, boolean isPictureTaken, UploadFileAmazonS3 amazonS3, File mFileTemp) {

        try {

            if (!isPictureTaken) {
                bankNewStripModelImplement.onErrorImageUpload();
                return;
            }
            amazonUpload(amazonS3, mFileTemp);
            jsonObject.put("document", imageUrl);

            Log.d(TAG, "addBankAccount: " + jsonObject);

            if (jsonObject.getString("first_name").equals("")) {
                bankNewStripModelImplement.onErrorFirstName();
                return;
            } else if (jsonObject.getString("last_name").equals("")) {
                bankNewStripModelImplement.onErrorLastName();
                return;
            } else if (!Patterns.EMAIL_ADDRESS.matcher(jsonObject.getString("email")).matches()) {
                bankNewStripModelImplement.onErrorEmail();
                return;
            } else if (jsonObject.getString("year").equals("")) {
                bankNewStripModelImplement.onErrorDob();
                return;
            } else if (jsonObject.getString("personal_id_number").equals("")) {
                bankNewStripModelImplement.onErrorSSN();
                return;
            } else if (jsonObject.getString("line1").equals("")) {
                bankNewStripModelImplement.onErrorAddress();
                return;
            } else if (jsonObject.getString("city").equals("")) {
                bankNewStripModelImplement.onErrorCity();
                return;
            } else if (jsonObject.getString("state").equals("")) {
                bankNewStripModelImplement.onErrorState();
                return;
            } else if (jsonObject.getString("country").equals("")) {
                bankNewStripModelImplement.onErrorCountry();
                return;
            } else if (jsonObject.getString("postal_code").equals("")) {
                bankNewStripModelImplement.onErrorZipcode();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, "addBankAccount: sessionToken " + token);
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(token, ServiceUrl.BANK_CONNECT_ACCOUNT_STRIPE, OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode, String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);

                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                            bankNewStripModelImplement.onSuccess(jsonObject.getString("message"));
                        } else {
                            bankNewStripModelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        bankNewStripModelImplement.onFailure();
                    }
                } catch (Exception e) {
                    bankNewStripModelImplement.onFailure();
                }
            }

            @Override
            public void onError(String error) {
                bankNewStripModelImplement.onFailure();
            }
        });
    }

    /**
     * mehtod for uploading image in amazon
     *
     * @param amazonS3  object of UploadFileAmazonS3 class
     * @param mFileTemp file that need to upload in amazon
     */
    private void amazonUpload(UploadFileAmazonS3 amazonS3, File mFileTemp) {
        String BUCKETSUBFOLDER = VariableConstant.BANK_PROOF;
        imageUrl = VariableConstant.AMAZON_BASE_URL + VariableConstant.BUCKET_NAME + "/" + BUCKETSUBFOLDER + mFileTemp.getName();

        amazonS3.Upload_data(BUCKETSUBFOLDER , mFileTemp.getName(), mFileTemp, new UploadFileAmazonS3.UploadCallBack() {
            @Override
            public void sucess(String success) {
                Log.d(TAG, "sucess: amazon " + success);
            }

            @Override
            public void error(String errormsg) {
                Log.d(TAG, "error: amazon error " + errormsg);
            }
        });
    }

    /**
     * interface for  presenter implementation
     */
    interface BankNewStripModelImplement {
        void onFailure();

        void onFailure(String msg);

        void onSuccess(String failureMsg);

        void ipAddress(String ip);

        void onErrorImageUpload();

        void onErrorFirstName();

        void onErrorLastName();

        void onErrorEmail();

        void onErrorDob();

        void onErrorSSN();

        void onErrorAddress();

        void onErrorCity();

        void onErrorState();

        void onErrorCountry();

        void onErrorZipcode();
    }

    /**
     * Asyntask for getting the ip address of the mobile connect network
     */
    private class getIpAddress extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String[] params) {
            // do above Server call here
            try {
                return InetAddress.getLocalHost().toString();
            } catch (UnknownHostException e) {
                e.printStackTrace();
                return "007";
            }
        }

        @Override
        protected void onPostExecute(String message) {
            Log.d(TAG, "onPostExecute: " + message);
            bankNewStripModelImplement.ipAddress(message);
        }
    }

}
