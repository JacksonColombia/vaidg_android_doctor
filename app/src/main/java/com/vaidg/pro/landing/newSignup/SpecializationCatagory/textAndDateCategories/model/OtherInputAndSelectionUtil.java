package com.vaidg.pro.landing.newSignup.SpecializationCatagory.textAndDateCategories.model;

import com.vaidg.pro.dagger.FragmentScoped;
import com.vaidg.pro.utility.DatePickerCommon;

import dagger.Module;
import dagger.Provides;

/**
 * <h1>OtherInputAndSelectionUtil</h1>
 * <p>This dagger util is used to provide object reference to injected objects when declare in activity.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 15/07/2020
 **/
@Module
public class OtherInputAndSelectionUtil {

    @FragmentScoped
    @Provides
    DatePickerCommon provideDatePickerCommon() {
        return new DatePickerCommon();
    }
}
