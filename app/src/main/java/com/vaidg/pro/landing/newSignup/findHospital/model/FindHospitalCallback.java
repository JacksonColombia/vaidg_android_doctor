package com.vaidg.pro.landing.newSignup.findHospital.model;

public interface FindHospitalCallback {

    /**
     * <p> this is getting hospital details</p>
     *
     * @param position
     */
    void onItemSelect(int position);
}
