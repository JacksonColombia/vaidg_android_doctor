package com.vaidg.pro.main.schedule.viewschedulelist;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.ScheduleViewListAdapter;
import com.vaidg.pro.pojo.shedule.ScheduleViewData;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import dagger.android.support.DaggerFragment;
import java.util.ArrayList;
import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleViewListFragment extends DaggerFragment implements ScheduleViewListAdapter.DeleteSchdule, ScheculeViewListContract.View {

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    @Inject
    ScheculeViewListContract.Presenter presenter;

    private ScheduleViewListAdapter scheduleViewListAdapter;
    private ArrayList<ScheduleViewData> scheduleViewDatas;
    private int deletePosition = 0;
    private Context mContext;
    private TextView tvNoScheduleMsg;
    private RecyclerView rvScheduleView;
    public static ScheduleViewListFragment getInstance() {
        return new ScheduleViewListFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_schedule_view_list, container, false);
        initView(rootView);
        return rootView;
    }


    private void initView(View rootView) {
        sessionManager = SessionManager.getSessionManager(mContext);
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage(getString(R.string.deletingSchedule));
        progressDialog.setCancelable(false);
        tvNoScheduleMsg = rootView.findViewById(R.id.tvNoScheduleMsg);
        scheduleViewDatas = new ArrayList<>();
        scheduleViewListAdapter = new ScheduleViewListAdapter(mContext, scheduleViewDatas, this);
        rvScheduleView = rootView.findViewById(R.id.rvScheduleView);
        rvScheduleView.setLayoutManager(new LinearLayoutManager(mContext));
        rvScheduleView.setAdapter(scheduleViewListAdapter);
    }

    @Override
    public void onDelete(final int position, final String slotId) {
        deletePosition = position;
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(getString(R.string.alert));
        builder.setMessage(getString(R.string.slotDeleteWarning));
        builder.setPositiveButton(getString(R.string.oK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                progressDialog.setMessage(getString(R.string.deletingSchedule));
                presenter.deleteSchedule(slotId);
            }
        });

        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(mContext, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(mContext, progressDialog);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(mContext, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }
    

    @Override
    public void onSuccessDeleteSchedule(String msg) {
        VariableConstant.IS_SHEDULE_EDITED = true;
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
        scheduleViewDatas.remove(deletePosition);
        scheduleViewListAdapter.notifyDataSetChanged();
    }

    public void setValues(ArrayList<ScheduleViewData> scheduleViewDatas, boolean b) {
        if(scheduleViewDatas.size() == 0)
        {
            tvNoScheduleMsg.setVisibility(View.VISIBLE);
            if(b)
            {
                tvNoScheduleMsg.setText(getResources().getString(R.string.activeSchedule));
            }else{
                tvNoScheduleMsg.setText(getResources().getString(R.string.pastSchedule));
            }
        }else {
            rvScheduleView.setVisibility(View.VISIBLE);
            try {
                this.scheduleViewDatas.clear();
                this.scheduleViewDatas.addAll(scheduleViewDatas);
                scheduleViewListAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.attachView(this);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        Utility.progressDialogCancel(mContext, progressDialog);
        presenter.detachView();
        this.mContext = null;
        super.onDetach();
    }
}
