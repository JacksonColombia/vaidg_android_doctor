package com.vaidg.pro.landing.newSignup.editEducation;

import android.content.Intent;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.signup.DegreeData;

/**
 * <h1>@AddEditEducationContract</h1>
 * <p>interface b.w activity and presenter</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 27/04/2020.
 **/
public class AddEditEducationContract {

    interface View extends BaseView {

        void showError(String error);

        void selectDegree();

        void displaySelectedDegree(DegreeData degreeData);
    }

    public interface Presenter extends BasePresenter {

        boolean validField(String degree, String university, String year);

        void setDegreeData(DegreeData degreeData);

        DegreeData getDegreeData();

        void parseOnActivityResult(int requestCode, int resultCode, Intent data);

    }
}
