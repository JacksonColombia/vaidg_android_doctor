package com.vaidg.pro.landing.newSignup.editEducation;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>AddEditEducationDaggerModule</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 27/04/2020.
 **/
@Module
public abstract class AddEditEducationDaggerModule {

    @ActivityScoped
    @Binds
    abstract AddEditEducationContract.Presenter providePresenter(AddEditEducationPresenter presenter);

    @ActivityScoped
    @Binds
    abstract AddEditEducationContract.View provideView(AddEditEducationActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(AddEditEducationActivity activity);

}
