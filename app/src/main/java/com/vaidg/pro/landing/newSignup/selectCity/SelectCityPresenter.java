package com.vaidg.pro.landing.newSignup.selectCity;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.landing.newSignup.selectCity.model.CitySelectionListener;
import com.vaidg.pro.landing.newSignup.selectCity.model.SelectCityModel;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationPojo;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.signup.CityData;
import com.vaidg.pro.pojo.signup.CityPojo;
import com.vaidg.pro.utility.Utility;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

/**
 * <h1>SelectCityPresenter</h1>
 * <p>This class is contain override method of presenter with business logic.</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 12/06/2020
 **/
public class SelectCityPresenter implements SelectCityContract.Presenter, CitySelectionListener {

    @Inject
    SelectCityContract.View view;

    @Inject
    SelectCityModel model;

    @Inject
    NetworkService service;

    @Inject
    NetworkStateHolder networkStateHolder;

    @Inject
    Activity activity;

    @Inject
    Gson gson;

    private int position;

    @Inject
    public SelectCityPresenter() {
    }

    @Override
    public void onItemSelected(int position) {
        model.selectCity(position);
        CityData cityData = model.getSelectedCity(position);
        Intent intent = new Intent();
        intent.putExtra("city", cityData);
        view.finishActivity(intent);
    }

    @Override
    public void onItemSelected(int position, String name) {
        model.selectCity(position,name);
        Log.d("TAG", "onItemSelected: "+model.getPosition());
        CityData cityData = model.getSelectedCity(model.getPosition());
        Intent intent = new Intent();
        intent.putExtra("city", cityData);
        view.finishActivity(intent);
    }

    @Override
    public void getCity() {
        if (networkStateHolder.isConnected()) {
            if (view != null)
                view.showProgress();
            Utility.basicAuth(service).getCity(model.getLanguage(), model.getPlatform())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {

                            int statusCode = value.code();
                            try {
                                String response = value.body() != null
                                        ? value.body().string() : null;
                                String errorBody = value.errorBody() != null
                                        ? value.errorBody().string() : null;
                                if (statusCode == SUCCESS_RESPONSE) {
                                    CityPojo cityPojo = gson.fromJson(response, CityPojo.class);
                                    model.parseCityData(cityPojo);
                                    if (view != null) {
                                        view.hideProgress();
                                        view.notifyAdapter();
                                    }
                                } else {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.showError(Utility.getMessage(errorBody));
                                    }
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            view.hideProgress();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }
}