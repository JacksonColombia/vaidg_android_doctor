package com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection;

import com.vaidg.pro.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>ListSelectionDaggerModule</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
@Module
public interface ListSelectionDaggerModule {

    @FragmentScoped
    @Binds
    ListSelectionContract.Presenter providePresenter(ListSelectionPresenter presenter);
}
