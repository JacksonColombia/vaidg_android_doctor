package com.vaidg.pro.main.profile.profiledetail.metadatadit.model;


import android.app.Activity;
import android.content.Context;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.pojo.profile.metaData.MetaDataArr;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.MediaBottomSelector;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import com.videocompressor.com.CompressImage;
import com.videocompressor.com.VideoCompressor;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;

/**
 * <h1>ProfileMetaDataEditUtil</h1>
 * <p>This dagger util is used to provide object reference to injected objects when declare in activity.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 08/06/2020
 **/
@Module
public class ProfileMetaDataEditUtil {

    @ActivityScoped
    @Provides
    List<MetaDataArr> getMetaDataList() { return new ArrayList<>(); }

    @ActivityScoped
    @Provides
    ProfileMetaDataEditAdapter getProfileMetaDataEditAdapter(List<MetaDataArr> metaDataList) {
        return new ProfileMetaDataEditAdapter(metaDataList);
    }

    @Provides
    @ActivityScoped
    App_permission getApp_permission(Activity activity) {
        return new App_permission(activity);
    }

    @Provides
    @ActivityScoped
    MediaBottomSelector getMediaBottomSelector(Activity activity) {
        return new MediaBottomSelector(activity);
    }

    @ActivityScoped
    @Provides
    CompressImage compressImage() {
        return new CompressImage();
    }

    @ActivityScoped
    @Provides
    VideoCompressor getVideoCompressor(Context context) {
        return new VideoCompressor(context);
    }

    @ActivityScoped
    @Provides
    SessionManager provide(Activity activity) {
        return SessionManager.getSessionManager(activity);
    }
}
