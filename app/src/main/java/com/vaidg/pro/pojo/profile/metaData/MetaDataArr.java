package com.vaidg.pro.pojo.profile.metaData;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vaidg.pro.pojo.AttributName;
import com.vaidg.pro.pojo.AttributeDescription;
import com.vaidg.pro.pojo.FilterTitelLang;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.pojo.QueForBookingByLang;
import com.vaidg.pro.pojo.QueForProviderSingupByLang;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by murashid on 16-Feb-18.
 */

public class MetaDataArr implements Parcelable {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("num")
    @Expose
    private String num;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("minimum")
    @Expose
    private String minimum;
    @SerializedName("maximum")
    @Expose
    private String maximum;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("linkedToProvider")
    @Expose
    private Boolean linkedToProvider;
    @SerializedName("queForProviderSignup")
    @Expose
    private String queForProviderSignup;
    @SerializedName("adminVerificationRequired")
    @Expose
    private Boolean adminVerificationRequired;
    @SerializedName("visibleOnProviderProfile")
    @Expose
    private Boolean visibleOnProviderProfile;
    @SerializedName("mandatoryForProvider")
    @Expose
    private Boolean mandatoryForProvider;
    @SerializedName("queEnableForBooking")
    @Expose
    private Boolean queEnableForBooking;
    @SerializedName("queForBooking")
    @Expose
    private String queForBooking;
    @SerializedName("mandatoryForCustomer")
    @Expose
    private Boolean mandatoryForCustomer;
    @SerializedName("filterable")
    @Expose
    private Boolean filterable;
    @SerializedName("filterTitle")
    @Expose
    private String filterTitle;
    @SerializedName("attributName")
    @Expose
    private AttributName attributName;
    @SerializedName("attributeDescription")
    @Expose
    private AttributeDescription attributeDescription;
    @SerializedName("queForProviderSingupByLang")
    @Expose
    private QueForProviderSingupByLang queForProviderSingupByLang;
    @SerializedName("queForBookingByLang")
    @Expose
    private QueForBookingByLang queForBookingByLang;
    @SerializedName("filterTitelLang")
    @Expose
    private FilterTitelLang filterTitelLang;
    @SerializedName("preDefined")
    @Expose
    private List<PreDefined> preDefined = new ArrayList<>();
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("editableOnProvider")
    @Expose
    private Boolean editableOnProvider;
    @SerializedName("data")
    @Expose
    private String data;
    public final static Parcelable.Creator<MetaDataArr> CREATOR = new Creator<MetaDataArr>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MetaDataArr createFromParcel(Parcel in) {
            return new MetaDataArr(in);
        }

        public MetaDataArr[] newArray(int size) {
            return (new MetaDataArr[size]);
        }

    }
            ;

    protected MetaDataArr(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.num = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.minimum = ((String) in.readValue((String.class.getClassLoader())));
        this.maximum = ((String) in.readValue((String.class.getClassLoader())));
        this.unit = ((String) in.readValue((String.class.getClassLoader())));
        this.linkedToProvider = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.queForProviderSignup = ((String) in.readValue((String.class.getClassLoader())));
        this.adminVerificationRequired = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.visibleOnProviderProfile = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.mandatoryForProvider = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.queEnableForBooking = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.queForBooking = ((String) in.readValue((String.class.getClassLoader())));
        this.mandatoryForCustomer = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.filterable = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.filterTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.attributName = ((AttributName) in.readValue((AttributName.class.getClassLoader())));
        this.attributeDescription = ((AttributeDescription) in.readValue((AttributeDescription.class.getClassLoader())));
        this.queForProviderSingupByLang = ((QueForProviderSingupByLang) in.readValue((QueForProviderSingupByLang.class.getClassLoader())));
        this.queForBookingByLang = ((QueForBookingByLang) in.readValue((QueForBookingByLang.class.getClassLoader())));
        this.filterTitelLang = ((FilterTitelLang) in.readValue((FilterTitelLang.class.getClassLoader())));
        in.readList(this.preDefined, (PreDefined.class.getClassLoader()));
        this.icon = ((String) in.readValue((String.class.getClassLoader())));
        this.editableOnProvider = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.data = ((String) in.readValue((String.class.getClassLoader())));
    }

    public MetaDataArr() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMinimum() {
        return minimum;
    }

    public void setMinimum(String minimum) {
        this.minimum = minimum;
    }

    public String getMaximum() {
        return maximum;
    }

    public void setMaximum(String maximum) {
        this.maximum = maximum;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Boolean getLinkedToProvider() {
        return linkedToProvider;
    }

    public void setLinkedToProvider(Boolean linkedToProvider) {
        this.linkedToProvider = linkedToProvider;
    }

    public String getQueForProviderSignup() {
        return queForProviderSignup;
    }

    public void setQueForProviderSignup(String queForProviderSignup) {
        this.queForProviderSignup = queForProviderSignup;
    }

    public Boolean getAdminVerificationRequired() {
        return adminVerificationRequired;
    }

    public void setAdminVerificationRequired(Boolean adminVerificationRequired) {
        this.adminVerificationRequired = adminVerificationRequired;
    }

    public Boolean getVisibleOnProviderProfile() {
        return visibleOnProviderProfile;
    }

    public void setVisibleOnProviderProfile(Boolean visibleOnProviderProfile) {
        this.visibleOnProviderProfile = visibleOnProviderProfile;
    }

    public Boolean getMandatoryForProvider() {
        return mandatoryForProvider;
    }

    public void setMandatoryForProvider(Boolean mandatoryForProvider) {
        this.mandatoryForProvider = mandatoryForProvider;
    }

    public Boolean getQueEnableForBooking() {
        return queEnableForBooking;
    }

    public void setQueEnableForBooking(Boolean queEnableForBooking) {
        this.queEnableForBooking = queEnableForBooking;
    }

    public String getQueForBooking() {
        return queForBooking;
    }

    public void setQueForBooking(String queForBooking) {
        this.queForBooking = queForBooking;
    }

    public Boolean getMandatoryForCustomer() {
        return mandatoryForCustomer;
    }

    public void setMandatoryForCustomer(Boolean mandatoryForCustomer) {
        this.mandatoryForCustomer = mandatoryForCustomer;
    }

    public Boolean getFilterable() {
        return filterable;
    }

    public void setFilterable(Boolean filterable) {
        this.filterable = filterable;
    }

    public String getFilterTitle() {
        return filterTitle;
    }

    public void setFilterTitle(String filterTitle) {
        this.filterTitle = filterTitle;
    }

    public AttributName getAttributName() {
        return attributName;
    }

    public void setAttributName(AttributName attributName) {
        this.attributName = attributName;
    }

    public AttributeDescription getAttributeDescription() {
        return attributeDescription;
    }

    public void setAttributeDescription(AttributeDescription attributeDescription) {
        this.attributeDescription = attributeDescription;
    }

    public QueForProviderSingupByLang getQueForProviderSingupByLang() {
        return queForProviderSingupByLang;
    }

    public void setQueForProviderSingupByLang(QueForProviderSingupByLang queForProviderSingupByLang) {
        this.queForProviderSingupByLang = queForProviderSingupByLang;
    }

    public QueForBookingByLang getQueForBookingByLang() {
        return queForBookingByLang;
    }

    public void setQueForBookingByLang(QueForBookingByLang queForBookingByLang) {
        this.queForBookingByLang = queForBookingByLang;
    }

    public FilterTitelLang getFilterTitelLang() {
        return filterTitelLang;
    }

    public void setFilterTitelLang(FilterTitelLang filterTitelLang) {
        this.filterTitelLang = filterTitelLang;
    }

    public List<PreDefined> getPreDefined() {
        return preDefined;
    }

    public void setPreDefined(List<PreDefined> preDefined) {
        this.preDefined = preDefined;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getEditableOnProvider() {
        return editableOnProvider;
    }

    public void setEditableOnProvider(Boolean editableOnProvider) {
        this.editableOnProvider = editableOnProvider;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(num);
        dest.writeValue(name);
        dest.writeValue(type);
        dest.writeValue(description);
        dest.writeValue(minimum);
        dest.writeValue(maximum);
        dest.writeValue(unit);
        dest.writeValue(linkedToProvider);
        dest.writeValue(queForProviderSignup);
        dest.writeValue(adminVerificationRequired);
        dest.writeValue(visibleOnProviderProfile);
        dest.writeValue(mandatoryForProvider);
        dest.writeValue(queEnableForBooking);
        dest.writeValue(queForBooking);
        dest.writeValue(mandatoryForCustomer);
        dest.writeValue(filterable);
        dest.writeValue(filterTitle);
        dest.writeValue(attributName);
        dest.writeValue(attributeDescription);
        dest.writeValue(queForProviderSingupByLang);
        dest.writeValue(queForBookingByLang);
        dest.writeValue(filterTitelLang);
        dest.writeList(preDefined);
        dest.writeValue(icon);
        dest.writeValue(editableOnProvider);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }
}
