package com.vaidg.pro.landing.newSignup.selectSpecialization.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CallType implements Parcelable {
    public static final Creator<CallType> CREATOR = new Creator<CallType>() {
        @Override
        public CallType createFromParcel(Parcel source) {
            return new CallType(source);
        }

        @Override
        public CallType[] newArray(int size) {
            return new CallType[size];
        }
    };
    @SerializedName("incall")
    @Expose
    private Boolean incall;
    @SerializedName("outcall")
    @Expose
    private Boolean outcall;
    @SerializedName("telecall")
    @Expose
    private Boolean telecall;

    public CallType() {
    }

    protected CallType(Parcel in) {
        this.incall = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.outcall = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.telecall = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public Boolean getIncall() {
        return incall;
    }

    public void setIncall(Boolean incall) {
        this.incall = incall;
    }

    public Boolean getOutcall() {
        return outcall;
    }

    public void setOutcall(Boolean outcall) {
        this.outcall = outcall;
    }

    public Boolean getTelecall() {
        return telecall;
    }

    public void setTelecall(Boolean telecall) {
        this.telecall = telecall;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.incall);
        dest.writeValue(this.outcall);
        dest.writeValue(this.telecall);
    }
}
