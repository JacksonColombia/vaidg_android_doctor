package com.vaidg.pro.main.schedule;

import android.widget.TimePicker;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.shedule.ScheduleMonthData;
import com.vaidg.pro.pojo.shedule.ScheduleMonthPojo;
import com.vaidg.pro.pojo.shedule.Slot;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface ScheculeFragmentContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onFailure(String msg);

    void onFailure();

    void onSuccessGetSchedule(ScheduleMonthPojo scheduleMonthPojo);

    void onNewToken(String token);

    void sessionExpired(String msg);

  }

  interface Presenter extends BasePresenter<View> {
    /**
     *
     *
     *
     *
     * methof for passing value from view to model
     *
     * @param date         date of selected month
     */
    void getSchedule(String date, boolean isCurrentMonth);

    /**
     * Create slot for Each based on booked and arrange according to the time
     *
     * @param eventList calendar event list
     * @return list of Slot
     */
    ArrayList<Slot> createSlotFromSchedules(List<Event> eventList);


    /**
     * Returing the created events based on the sheduleMontDatas
     * create blue color for schedule day and differrent color for booked color
     *
     * @param scheduleMonthDatas list of scheduleMonthDatas
     * @param calendarEvents     List of calendar events
     * @return calendarEvents
     */
    ArrayList<Event> getEvents(ArrayList<ScheduleMonthData> scheduleMonthDatas, ArrayList<Event> calendarEvents);


  }
}