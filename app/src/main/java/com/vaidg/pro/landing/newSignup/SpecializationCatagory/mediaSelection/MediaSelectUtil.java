package com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection;

import com.vaidg.pro.dagger.FragmentScoped;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * <h1>@MediaSelectUtil</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
@Module
public class MediaSelectUtil {

    @Named("urls")
    @Provides
    @FragmentScoped
    ArrayList<String> getUrls() {
        return new ArrayList<>();
    }

    @Named("filePath")
    @Provides
    @FragmentScoped
    ArrayList<String> getFilePath() {
        return new ArrayList<>();
    }
}
