package com.vaidg.pro.main.profile.profiledetail;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>ProfileDetailDaggerModule</h1>
 * <p>This dagger module created for {@link ProfileDetailActivity} to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
@Module
public abstract class ProfileDetailDaggerModule {

    @ActivityScoped
    @Binds
    abstract Activity getActivity(ProfileDetailActivity profileDetailActivity);

    @ActivityScoped
    @Binds
    abstract ProfileDetailContract.View getView(ProfileDetailActivity profileDetailActivity);

    @ActivityScoped
    @Binds
    abstract ProfileDetailContract.Presenter getPresenter(ProfileDetailPresenterImple profileDetailPresenterImple);
}
