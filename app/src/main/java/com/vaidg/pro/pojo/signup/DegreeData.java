package com.vaidg.pro.pojo.signup;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DegreeData implements Parcelable {


    public static final Creator<DegreeData> CREATOR = new Creator<DegreeData>() {
        @Override
        public DegreeData createFromParcel(Parcel in) {
            return new DegreeData(in);
        }

        @Override
        public DegreeData[] newArray(int size) {
            return new DegreeData[size];
        }
    };

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;

    public DegreeData() {
    }

    protected DegreeData(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
