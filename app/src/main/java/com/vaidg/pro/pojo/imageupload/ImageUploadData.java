package com.vaidg.pro.pojo.imageupload;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageUploadData implements Parcelable {

    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    public final static Parcelable.Creator<ImageUploadData> CREATOR = new Creator<ImageUploadData>() {

        public ImageUploadData createFromParcel(Parcel in) {
            return new ImageUploadData(in);
        }

        public ImageUploadData[] newArray(int size) {
            return (new ImageUploadData[size]);
        }

    };

    protected ImageUploadData(Parcel in) {
        this.imageUrl = in.readString();
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(imageUrl);
    }

    public int describeContents() {
        return 0;
    }

}
