package com.vaidg.pro.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Age implements Serializable {

  @SerializedName("days")
  @Expose
  private int days;
  @SerializedName("months")
  @Expose
  private int months;
  @SerializedName("years")
  @Expose
  private int years;

  private Age()
  {
    //Prevent default constructor
  }

  public Age(int days, int months, int years)
  {
    this.days = days;
    this.months = months;
    this.years = years;
  }

  public int getDays()
  {
    return this.days;
  }

  public int getMonths()
  {
    return this.months;
  }

  public int getYears()
  {
    return this.years;
  }

  @Override
  public String toString()
  {
    return years + " Years, " + months + " Months, " + days + " Days";
  }
}
