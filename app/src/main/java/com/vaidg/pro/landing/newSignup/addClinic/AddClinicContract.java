package com.vaidg.pro.landing.newSignup.addClinic;

import android.content.Intent;

import com.vaidg.pro.BasePresenter;

/**
 * <h1>AddClinicContract</h1>
 * <p>This class is define implementation method for View an Presenter class.</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 13/06/2020
 **/
public interface AddClinicContract {

    interface View {


        /**
         * <p>This method is used to initialize views</p>
         */
        void intiView();

        /**
         * <p>This method is used to set listener for view click</p>
         */
        void initOnClickListener();

        /**
         * <p>This method is define view click events</p>
         *
         * @param v the clicked view
         */
        void onClick(android.view.View v);

        /**
         * <p>This method is return intent with result and data.</p>
         * @param intent the object of intent
         */
        void sendBackData(Intent intent);

    }

    interface Presenter extends BasePresenter {

        /**
         * <p>This method is handle activity result</p>
         * @param requestCode the request code set when startActivityForResult
         * @param resultCode the result code success or cancel
         * @param data the object of intent
         */
        void parseOnActivityResult(int requestCode, int resultCode, Intent data);

    }
}
