package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Capabilities implements Serializable {

	@SerializedName("transfers")
	@Expose
	private String transfers;

	@SerializedName("card_payments")
	@Expose
	private String cardPayments;

	public String getTransfers(){
		return transfers;
	}

	public String getCardPayments(){
		return cardPayments;
	}

	@Override
 	public String toString(){
		return 
			"Capabilities{" + 
			"transfers = '" + transfers + '\'' + 
			",card_payments = '" + cardPayments + '\'' + 
			"}";
		}
}