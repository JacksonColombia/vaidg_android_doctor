package com.vaidg.pro.otpverify;

import android.app.Activity;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.login.LogInPojo;
import com.vaidg.pro.pojo.login.LoginData;
import com.vaidg.pro.utility.MixpanelEvents;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;
import static com.vaidg.pro.utility.VariableConstant.USER_TYPE;

/**
 * <h1>OTPVerifyPresenterImple</h1>
 * <p>This class is contain override method of presenter with business logic.</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 24/06/2020
 **/
public class OTPVerifyPresenterImple implements OTPVerifyContract.Presenter {

    @Inject
    OTPVerifyContract.View view;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;
    @Inject
    Activity activity;
    @Inject
    NetworkService service;
    @Inject
    NetworkStateHolder networkStateHolder;

    @Inject
    public OTPVerifyPresenterImple() {
    }

    @Override
    public void resendOtp(String userId, int otpOption) {
        if (networkStateHolder.isConnected()) {
            if (view != null)
            view.showProgress();

            Map<String, Object> params = new HashMap<>();
            params.put("userId", userId);
            params.put("userType", USER_TYPE);
            params.put("trigger", otpOption);

            Utility.basicAuth(service).resendOtp(DEFAULT_LANGUAGE, PLATFORM, params)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                if (statusCode == SUCCESS_RESPONSE) {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.onSuccessResendCode(Utility.getMessage(response));
                                    }
                                } else {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.showError(Utility.getMessage(errorBody));
                                    }
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError(activity.getString(R.string.serverError));
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.showError(activity.getString(R.string.serverError));
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void verifyOtp(String userId, int otpOption, String otp) {
        Utility.hideKeyboad(activity);
        Map<String, Object> params = new HashMap<>();
        params.put("code", otp);
        switch (otpOption) {
            case 1:
                params.put("providerId", userId);
                verifyPhoneNumber(params);
                break;
            case 2:
            case 3:
                params.put("userId", userId);
                params.put("userType", USER_TYPE);
                params.put("trigger", otpOption);
                verifyVerificationCode(params, otpOption);
                break;
        }

    }

    @Override
    public void verifyPhoneNumber(Map<String, Object> params) {
        if (networkStateHolder.isConnected()) {
            if (view != null)
            view.showProgress();
            Utility.basicAuth(service).verifyPhoneNumber(DEFAULT_LANGUAGE, PLATFORM, params)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                if (statusCode == SUCCESS_RESPONSE) {
                                    if (view != null)
                                    view.hideProgress();
                                    if (response != null && !response.isEmpty()) {
                                        LogInPojo loginPojo = gson.fromJson(response, LogInPojo.class);
                                        LoginData loginData = loginPojo.getData();
                                        sessionManager.setProviderId(loginData.getId());
                                        sessionManager.setEmail(loginData.getEmail());
                                        sessionManager.setFirstName(loginData.getFirstName());
                                        sessionManager.setLastName(loginData.getLastName());
                                        sessionManager.setProfilePic(loginData.getProfilePic());
                                        sessionManager.setPhoneNumber(loginData.getMobile());
                                        sessionManager.setCountryCode(loginData.getCountryCode());
                                        sessionManager.setReferalCode(loginData.getReferralCode());
                                        sessionManager.setIsDriverLogin(true);
                                        sessionManager.setDeviceId(Utility.getDeviceId(activity));
                                        sessionManager.setFCMTopic(loginData.getFcmTopic());
                                        sessionManager.setZendeskRequesterId(loginData.getRequester_id());
                                        sessionManager.setIsBidBooking(loginData.getBid());
                                       // sessionManager.setVirgilPublicKey(loginData.getPublickKey());
                                       // sessionManager.setVirgilPrivateKey(loginData.getPrivateKey());
                                        sessionManager.setCallToken(loginData.getCall().getAuthToken());
                                        sessionManager.setCallWillTopic(loginData.getCall().getWillTopic());
                                        AppController.getInstance().getAccountManagerHelper().setAuthToken(loginData.getEmail(),
                                                sessionManager.getPassword(), loginData.getToken().getAccessToken());
                                        FirebaseMessaging.getInstance().subscribeToTopic(sessionManager.getFCMTopic());
                                        AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.Login.value);
                                        if (view != null)
                                        view.onSuccessSignUpOtp();
                                    } else{
                                        if (view != null)
                                        view.showError(activity.getString(R.string.serverError));
                                } }else {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.showError(Utility.getMessage(errorBody));
                                    }
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError(activity.getString(R.string.serverError));
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.showError(activity.getString(R.string.serverError));
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void verifyVerificationCode(Map<String, Object> params, int otpOption) {
        if (networkStateHolder.isConnected()) {
            if (view != null)
            view.showProgress();
            Utility.basicAuth(service).verifyVerificationCode(DEFAULT_LANGUAGE, PLATFORM, params)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                if (statusCode == SUCCESS_RESPONSE) {
                                    if (view != null)
                                    view.hideProgress();
                                    if (response != null && !response.isEmpty()) {
                                        if (otpOption == 2) {
                                            if (view != null)
                                            view.onSuccessForgotPasswordOTP(Utility.getMessage(response));
                                        } else {
                                            if (view != null)
                                            view.onSuccessChangeNumberOTP(Utility.getMessage(response));
                                        }
                                    } else {
                                        if (view != null)
                                        view.showError(activity.getString(R.string.serverError));
                                    }
                                } else {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.showError(Utility.getMessage(errorBody));
                                    }
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError(activity.getString(R.string.serverError));
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.showError(activity.getString(R.string.serverError));
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }
}
