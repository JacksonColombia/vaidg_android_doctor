package com.vaidg.pro.pojo.profile.wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @since 19/09/17.
 */

public class WalletTransData implements Serializable {
   /* "data":{
    "debitArr":[],
    "creditArr":[],
    "creditDebitArr":[]}*/
   @SerializedName("debitArr")
   @Expose
    private ArrayList<WalletTransDetails> debitArr;
    @SerializedName("creditArr")
    @Expose
    private ArrayList<WalletTransDetails> creditArr;

    @SerializedName("paymentArr")
    @Expose
    private ArrayList<WalletTransDetails> paymentArr;
    @SerializedName("creditDebitArr")
    @Expose
    private ArrayList<WalletTransDetails> creditDebitArr;


    public ArrayList<WalletTransDetails> getDebitArr() {
        return debitArr;
    }

    public void setDebitArr(ArrayList<WalletTransDetails> debitArr) {
        this.debitArr = debitArr;
    }

    public ArrayList<WalletTransDetails> getCreditArr() {
        return creditArr;
    }

    public void setCreditArr(ArrayList<WalletTransDetails> creditArr) {
        this.creditArr = creditArr;
    }

    public ArrayList<WalletTransDetails> getCreditDebitArr() {
        return creditDebitArr;
    }

    public void setCreditDebitArr(ArrayList<WalletTransDetails> creditDebitArr) {
        this.creditDebitArr = creditDebitArr;
    }

    public ArrayList<WalletTransDetails> getPaymentArr() {
        return paymentArr;
    }


}
