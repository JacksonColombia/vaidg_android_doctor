package com.vaidg.pro.landing.newLogin;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityNewLogInBinding;
import com.vaidg.pro.helpwithpassword.HelpWithPasswordActivity;
import com.vaidg.pro.landing.introsilider2.IntroSliderNewActivity;
import com.vaidg.pro.landing.newSignup.SignUpActivity;
import com.vaidg.pro.main.MainActivity;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;
import com.vaidg.pro.utility.encryptiondecryption.CryptographyExample;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilKeyPair;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;

import javax.inject.Inject;

/**
 * <h1>LogInActivity</h1>
 * <p>This activity is connected with intro screen and it is allow user to login through credential or redirect to forgot password,
 * sign up through provided option</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 08/06/2020
 **/
public class NewLogInActivity extends BaseDaggerActivity implements LogInContract.View, View.OnFocusChangeListener {

    @Inject
    LogInContract.Presenter presenter;

    private ActivityNewLogInBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityNewLogInBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
        initViewOnClickListeners();
        initFocusOnViewListener();
    }

    private void initFocusOnViewListener() {
        binding.etLogInEmailPhone.setOnFocusChangeListener(this);
        binding.etLogInPassword.setOnFocusChangeListener(this);
    }

    private void initViewOnClickListeners() {
        binding.btnLogInForgotPassword.setOnClickListener(this::onClick);
        binding.btnLogIn.setOnClickListener(this::onClick);
        binding.tvSignUp.setOnClickListener(this::onClick);
    }

    @Override
    public void initViews() {
        binding.toolbarLogIn.tvTitle.setText(R.string.login);
        setSupportActionBar(binding.toolbarLogIn.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.toolbarLogIn.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        if (presenter.getIsRemembered()) {
            binding.cbLogInRememberMe.setChecked(true);
            binding.etLogInEmailPhone.setText(presenter.getUserName());
            binding.etLogInPassword.setText(presenter.getPassword());
        } else {
            binding.etLogInEmailPhone.requestFocus();
        }

        Utility.setSpannableString(this, binding.tvSignUp, getString(R.string.donTHaveAnAccount), getString(R.string.signup), R.color.dark_grey, R.color.colorPrimary);

    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboad(this);
        finish();
        overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    }

    @Override
    public void onClick(View view) {
        if (binding.pbLoader.getVisibility() == View.VISIBLE) {
            return;
        }

        switch (view.getId()) {
            case R.id.btnLogInForgotPassword:
                launchHelpWithPassword();
                break;
            case R.id.btnLogIn:
                presenter.logIn(binding.etLogInEmailPhone.getText() == null ? "" : binding.etLogInEmailPhone.getText().toString().trim(),
                        binding.etLogInPassword.getText() == null ? "" : binding.etLogInPassword.getText().toString().trim());
                break;
            case R.id.tvSignUp:
                launchSignUp();
                break;
        }
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (!hasFocus) {
            switch (view.getId()) {
                case R.id.etLogInEmailPhone:
                    presenter.verifyEmailPhone(binding.etLogInEmailPhone.getText() == null ? "" : binding.etLogInEmailPhone.getText().toString().trim());
                    break;
                case R.id.etLogInPassword:
                    presenter.verifyPassword(binding.etLogInPassword.getText() == null ? "" : binding.etLogInPassword.getText().toString().trim());
                    break;
            }
        }
    }

    @Override
    public boolean isRememberMe() {
        return binding.cbLogInRememberMe.isChecked();
    }

    @Override
    public void onEmailPhoneInCorrect() {
        setTilError(binding.tlLogInEmailPhone, R.string.invalidEmailPhone);
    }

    @Override
    public void onEmailPhoneCorrect() {
        binding.tlLogInEmailPhone.setErrorEnabled(false);
    }

    @Override
    public void onPasswordInCorrect() {
        setTilError(binding.tlLogInPassword, R.string.enterValidPassword);
    }

    @Override
    public void onPasswordCorrect() {
        binding.tlLogInPassword.setErrorEnabled(false);
    }

    @Override
    public void showProgress() {
        runOnUiThread(() -> binding.pbLoader.setVisibility(View.VISIBLE));
    }

    @Override
    public void hideProgress() {
        runOnUiThread(() -> binding.pbLoader.setVisibility(View.GONE));
    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clLogInActivity, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void onSuccess() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }

    /**
     * <p>This method is enable input error and set error message to that.</p>
     * @param textInputLayout the input view
     * @param err the error message
     */
    private void setTilError(TextInputLayout textInputLayout, @StringRes int err) {
        try {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(getString(err));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void launchHelpWithPassword() {
        Utility.hideKeyboad(this);
        Intent intent = new Intent(this, HelpWithPasswordActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }

    @Override
    public void launchSignUp() {
        Utility.hideKeyboad(this);
        Intent intent = new Intent(NewLogInActivity.this, SignUpActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }

    @Override
    public void checkForVersion() {
        try {
            CryptographyExample cryptographyExample = new CryptographyExample();
            // Generate keys
            VirgilCrypto crypto = new VirgilCrypto();
            VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
            presenter.checkForVersion(cryptographyExample,crypto,keyPair);
        } catch (CryptoException e) {
            e.printStackTrace();
        }
    }

}