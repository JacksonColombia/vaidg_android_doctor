package com.vaidg.pro.main.profile.profiledetail.model;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.main.profile.profiledetail.metadatadit.model.ProfileMetaDataEditAdapter;
import com.vaidg.pro.main.profile.profiledetail.model.ProfileAdditionalDetailsAdapter;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.pojo.profile.metaData.MetaDataArr;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.MediaBottomSelector;
import com.vaidg.pro.utility.SessionManager;
import com.videocompressor.com.CompressImage;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;

/**
 * <h1>ProfileDetailUtil</h1>
 * <p>This dagger util is used to provide object reference to injected objects when declare in activity.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 20/06/2020
 **/
@Module
public class ProfileDetailUtil {

    @ActivityScoped
    @Provides
    SessionManager provide(Activity activity) {
        return SessionManager.getSessionManager(activity);
    }

    @ActivityScoped
    @Provides
    List<MetaDataArr> getAdditionalDetailsList() {
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    ProfileAdditionalDetailsAdapter getProfileAdditionalDetailsAdapter(List<MetaDataArr> additionalDetailsList) {
        return new ProfileAdditionalDetailsAdapter(additionalDetailsList);
    }

    @Provides
    @ActivityScoped
    MediaBottomSelector getMediaBottomSelector(Activity activity) {
        return new MediaBottomSelector(activity);
    }

    @Provides
    @ActivityScoped
    App_permission getApp_permission(Activity activity) {
        return new App_permission(activity);
    }

    @ActivityScoped
    @Provides
    CompressImage compressImage() {
        return new CompressImage();
    }


}
