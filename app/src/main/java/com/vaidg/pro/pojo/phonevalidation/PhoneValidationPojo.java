package com.vaidg.pro.pojo.phonevalidation;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by murashid on 06-Oct-17.
 */

public class PhoneValidationPojo implements Parcelable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private PhoneValidationData data;

    public final static Parcelable.Creator<PhoneValidationPojo> CREATOR = new Parcelable.Creator<PhoneValidationPojo>() {

        public PhoneValidationPojo createFromParcel(Parcel in) {
            return new PhoneValidationPojo(in);
        }

        public PhoneValidationPojo[] newArray(int size) {
            return (new PhoneValidationPojo[size]);
        }

    };

    protected PhoneValidationPojo(Parcel in) {
        this.message = in.readString();
        this.data = ((PhoneValidationData) in.readValue((PhoneValidationData.class.getClassLoader())));
    }

    public PhoneValidationPojo() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PhoneValidationData getData() {
        return data;
    }

    public void setData(PhoneValidationData data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }
}
