package com.vaidg.pro.landing.newSignup.selectSpecialization;

import com.vaidg.pro.landing.newSignup.selectSpecialization.model.SpecializationSelectionAdapter;
import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class SelectSpecializationUtil {

    @Provides
    @ActivityScoped
    ArrayList<SelectSpecializationData> provideArrayList() {
        return new ArrayList<>();
    }

    @Provides
    @ActivityScoped
    SpecializationSelectionAdapter provideCitySelectionAdapter(ArrayList<SelectSpecializationData> specializationData) {
        return new SpecializationSelectionAdapter(specializationData);
    }

    @ActivityScoped
    @Provides
    int providePosition() {
        return 0;
    }

}
