package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Document implements Serializable {

	@SerializedName("back")
	@Expose
	private Object back;

	@SerializedName("details")
	@Expose
	private Object details;

	@SerializedName("front")
	@Expose
	private String front;

	@SerializedName("details_code")
	@Expose
	private Object detailsCode;

	protected Document(Parcel in) {
		front = in.readString();
	}

	public Object getBack(){
		return back;
	}

	public Object getDetails(){
		return details;
	}

	public String getFront(){
		return front;
	}

	public Object getDetailsCode(){
		return detailsCode;
	}

	@Override
 	public String toString(){
		return 
			"Document{" + 
			"back = '" + back + '\'' + 
			",details = '" + details + '\'' + 
			",front = '" + front + '\'' + 
			",details_code = '" + detailsCode + '\'' + 
			"}";
		}
}