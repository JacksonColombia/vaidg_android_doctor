package com.vaidg.pro.pojo.phonevalidation;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by murashid on 06-Oct-17.
 */

public class PhoneValidationData implements Parcelable {

    @SerializedName("sid")
    @Expose
    private String sid;
    @SerializedName("expireOtp")
    @Expose
    private Integer expireOtp;
    public final static Parcelable.Creator<PhoneValidationData> CREATOR = new Creator<PhoneValidationData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PhoneValidationData createFromParcel(Parcel in) {
            return new PhoneValidationData(in);
        }

        public PhoneValidationData[] newArray(int size) {
            return (new PhoneValidationData[size]);
        }

    }
            ;

    protected PhoneValidationData(Parcel in) {
        this.sid = in.readString();
        this.expireOtp = in.readInt();
    }

    public PhoneValidationData() {
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public Integer getExpireOtp() {
        return expireOtp;
    }

    public void setExpireOtp(Integer expireOtp) {
        this.expireOtp = expireOtp;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(sid);
        dest.writeValue(expireOtp);
    }

    public int describeContents() {
        return 0;
    }
}
