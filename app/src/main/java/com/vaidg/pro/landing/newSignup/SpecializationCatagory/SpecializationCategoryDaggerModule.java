package com.vaidg.pro.landing.newSignup.SpecializationCatagory;

import android.app.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.dagger.FragmentScoped;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.DocumentSelectionDaggerModule;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.DocumentSelectionFragment;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.DocumentSelectionUtil;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection.ListSelectionFragment;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection.ListSelectionDaggerModule;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection.MediaSelectFragment;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection.MediaSelectDaggerModule;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection.MediaSelectUtil;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.previewScreen.PreviewSceenFragment;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.previewScreen.PreviewSceenDaggerModule;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.slider.SliderFragment;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.slider.SliderDaggerModule;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.textAndDateCategories.OtherInputAndSelectionFragment;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.textAndDateCategories.OtherInputAndSelectionDaggerModule;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.textAndDateCategories.model.OtherInputAndSelectionUtil;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

/**
 * <h1>SpecializationCategoryDaggerModule</h1>
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
@Module
public abstract class SpecializationCategoryDaggerModule {

    @Provides
    @ActivityScoped
    static FragmentManager activityFragmentManager(Activity activity) {
        return ((AppCompatActivity) activity).getSupportFragmentManager();
    }

    @ActivityScoped
    @Binds
    abstract SpecializationCategoryContract.Presenter providePresenter(SpecializationCategoryPresenter presenter);

    @ActivityScoped
    @Binds
    abstract SpecializationCategoryContract.View provideView(SpecializationCategoryActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(SpecializationCategoryActivity activity);

    @FragmentScoped
    @ContributesAndroidInjector(modules = {PreviewSceenDaggerModule.class})
    abstract PreviewSceenFragment getPreviewSceenFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {OtherInputAndSelectionDaggerModule.class, OtherInputAndSelectionUtil.class})
    abstract OtherInputAndSelectionFragment getRegisterationNumberFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {ListSelectionDaggerModule.class})
    abstract ListSelectionFragment getListSelectionFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {MediaSelectDaggerModule.class, MediaSelectUtil.class})
    abstract MediaSelectFragment getMediaSelectFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {SliderDaggerModule.class})
    abstract SliderFragment getSliderFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {DocumentSelectionDaggerModule.class, DocumentSelectionUtil.class})
    abstract DocumentSelectionFragment getDocumentSelectionFragment();
}
