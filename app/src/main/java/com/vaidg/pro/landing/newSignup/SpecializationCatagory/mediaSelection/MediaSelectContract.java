package com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection;

import android.net.Uri;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.pojo.Attribute;

import java.io.File;

public interface MediaSelectContract {

    interface View {

        void openCamera(Uri uri, int mediaType);

        void showError(String message);

        void notifyAdapter();

        void openGallery(int mediaType);

        void launchViewer(String filePath);

        void selectDoc(Uri uri_path);

        /**
         * <p>This method is return boolean flag either it's mandatory or not.</p>
         * @return the true if madatory else false
         */
        boolean isMandatoryForProvider();

        void moveNextFrag();
    }

    interface Presenter extends BasePresenter<View> {

        void openChooser(int type);

        boolean isValidMediaSize();

        boolean isValidMediaSize(String filePath);

        void upDateToGallery();

        String getRecentTemp();

        void addMedia(File mFileTemp);

        void saveData(Attribute current_item);

        void isUpload();
    }
}
