package com.vaidg.pro.pojo.appconfig;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 25-Oct-17.
 */

public class ProviderFrequency implements Serializable {
    @SerializedName("locationPublishInterval")
    @Expose
    private String locationPublishInterval;
    @SerializedName("liveTrackInterval")
    @Expose
    private String liveTrackInterval;
    @SerializedName("proTimeOut")
    @Expose
    private String proTimeOut;

    public String getLocationPublishInterval() {
        return locationPublishInterval;
    }

    public void setLocationPublishInterval(String locationPublishInterval) {
        this.locationPublishInterval = locationPublishInterval;
    }

    public String getLiveTrackInterval() {
        return liveTrackInterval;
    }

    public void setLiveTrackInterval(String liveTrackInterval) {
        this.liveTrackInterval = liveTrackInterval;
    }

    public String getProTimeOut() {
        return proTimeOut;
    }

    public void setProTimeOut(String proTimeOut) {
        this.proTimeOut = proTimeOut;
    }

}
