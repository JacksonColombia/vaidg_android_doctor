package com.vaidg.pro.main.profile.bank.banknewstripe;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.AMAZON_BASE_URL;
import static com.vaidg.pro.utility.VariableConstant.BUCKET_NAME;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Patterns;
import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import java.io.File;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>BankNewStripePresenter</h1>
 * BankNewStripePresenter presenter for BankNewStripeActivity
 *
 * @see BankNewStripeActivity
 */

public class BankNewStripePresenter implements BankNewStripeContract.Presenter {

    @Inject
    Context context;
    @Inject
    BankNewStripeContract.View view;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;
    private String imageUrl = "";

    @Inject
    BankNewStripePresenter() {
    }
    
    @Override
    public void getIp() {
        //bankNewStripeModel.fetchIp();
        new getIpAddress().execute();
    }
    
    
    @Override
    public void addBankDetails(JSONObject jsonObject, boolean isPictureTaken, UploadFileAmazonS3 amazonS3, File mFileTemp) {
        if (view != null)
        view.startProgressBar();
        //bankNewStripeModel.addBankAccount(jsonObject, isPictureTaken, amazonS3, mFileTemp);
        try {

            if (!isPictureTaken) {
                if (view != null)
                view.onErrorImageUpload();
                return;
            }
           // Log.d(TAG, "addBankAccount: " + jsonObject);
            if (view != null) {
                if (jsonObject.getString("first_name").equals("")) {
                    view.onErrorFirstName();
                    return;
                } else if (jsonObject.getString("last_name").equals("")) {
                    view.onErrorLastName();
                    return;
                } else if (!Patterns.EMAIL_ADDRESS.matcher(jsonObject.getString("email")).matches()) {
                    view.onErrorEmail();
                    return;
                } else if (jsonObject.getString("year").equals("")) {
                    view.onErrorDob();
                    return;
                } else if (jsonObject.getString("id_number").equals("")) {
                    view.onErrorSSN();
                    return;
                } else if (jsonObject.getString("line1").equals("")) {
                    view.onErrorAddress();
                    return;
                } else if (jsonObject.getString("city").equals("")) {
                    view.onErrorCity();
                    return;
                } else if (jsonObject.getString("state").equals("")) {
                    view.onErrorState();
                    return;
                } else if (jsonObject.getString("country").equals("")) {
                    view.onErrorCountry();
                    return;
                } else if (jsonObject.getString("postal_code").equals("")) {
                    view.onErrorZipcode();
                    return;
                }
            }
            service.addConnectAccount(Utility.getAuth(sessionManager.getEmail()),DEFAULT_LANGUAGE, PLATFORM,jsonObject.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if (view != null)
                                    view.stopProgressBar();
                                    if (response != null && !response.isEmpty()) {
                                        if (view != null)
                                        view.onSuccess(Utility.getMessage(response));
                                    } else {
                                        if (view != null)
                                        view.onFailure();
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken)  {
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            if (view != null)
                                            view.stopProgressBar();
                                            addBankDetails(jsonObject,isPictureTaken,amazonS3,mFileTemp);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            if (view != null) {
                                                view.stopProgressBar();
                                                view.onFailure();
                                            }
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if (view != null)
                                                view.stopProgressBar();
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, context);
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    if (view != null)
                                    view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, context);
                                    break;
                                default:
                                    if (view != null) {
                                        view.stopProgressBar();
                                        view.onFailure(Utility.getMessage(errorBody));
                                    }
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            if (view != null) {
                                view.stopProgressBar();
                                view.onFailure();
                            }
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void amazonUpload(UploadFileAmazonS3 amazonS3, File mFileTemp) {
        String BUCKETSUBFOLDER = VariableConstant.BANK_PROOF;
        imageUrl = "https://" + BUCKET_NAME + "." + AMAZON_BASE_URL
            + BUCKETSUBFOLDER + "/"
            + mFileTemp.getName();

        amazonS3.Upload_data(BUCKETSUBFOLDER , mFileTemp.getName(), mFileTemp, new UploadFileAmazonS3.UploadCallBack() {
            @Override
            public void sucess(String success) {
                Log.d("TAG", "sucess: amazon " + success);
                imageUrl = success;
                if(view != null)
                    view.getImage(imageUrl);
            }

            @Override
            public void error(String errormsg) {
                Log.d("TAG", "error: amazon error " + errormsg);
            }
        });
    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    /**
     * Asyntask for getting the ip address of the mobile connect network
     */
    private class getIpAddress extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String[] params) {
            // do above Server call here
            try {
                return InetAddress.getLocalHost().toString();
            } catch (UnknownHostException e) {
                e.printStackTrace();
                return "500";
            }
        }

        @Override
        protected void onPostExecute(String message) {
            //Log.d(TAG, "onPostExecute: " + message);
            view.ipAddress(message);
        }
    }


    interface BankNewStripePresenterImplement {
        void startProgressBar();

        void stopProgressBar();

        void onSuccess(String msg);

        void onFailure();

        void onFailure(String msg);

        void ipAddress(String ip);

        void onErrorImageUpload();

        void onErrorFirstName();

        void onErrorLastName();

        void onErrorEmail();

        void onErrorDob();

        void onErrorSSN();

        void onErrorAddress();

        void onErrorCity();

        void onErrorCountry();

        void onErrorState();

        void onErrorZipcode();

    }


}
