package com.vaidg.pro.network;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

import com.vaidg.pro.BuildConfig;
import com.vaidg.pro.utility.FileUtils;
import com.vaidg.pro.utility.Utility;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class APIHelper {
    public static <T> void enqueueWithRetry(Call<T> call, final Callback<T> callback) {
        call.enqueue(new RetryableCallback<T>(call) {
            @Override
            public void onFinalResponse(Call<T> call, Response<T> response) {
                Log.d("APIHelper", "reached onFinalResponse");
                callback.onResponse(call, response);
            }

            @Override
            public void onFinalFailure(Call<T> call, Throwable t) {
                Log.d("APIHelper", "reached onFinalFailure");
                callback.onFailure(call, t);
            }
        });
    }

    public static boolean isCallSuccess(Response response) {
        int code = response.code();
        return (code >= 200 && code < 400);
    }

    @NonNull
    public static MultipartBody.Part prepareFilePart(Context context, String partName, File file) {
        Uri fileUri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        file, MediaType.parse(context.getContentResolver().getType(fileUri))
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    @NonNull
    public static RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(descriptionString
                , okhttp3.MultipartBody.FORM);
    }
}
