package com.vaidg.pro.utility;

import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.MESSAGE;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.PLATFORM;

import android.util.Log;
import com.vaidg.pro.network.NetworkService;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 16-Oct-17.
 * <h1>RefreshToken</h1>
 * wrapper class for refreshing token
 */
public class RetrofitRefreshToken {
  private static String TAG = "RefreshToken";
  /**
   * method for calling api for  refresh token
   *
   * @param token date token
   * @param imple RefreshTokenImple success , failure callback
   */
  private static RefreshTokenImple impl;
  private static String tokens = "";
  private static String refreshtokens = "";
  private static NetworkService lspService;

  public static void onRefreshToken(final String token, NetworkService lspServices, final RefreshTokenImple imple) {
    impl = imple;
    tokens = token;
    lspService = lspServices;
    RetrofitRefreshToken refreshToken = new RetrofitRefreshToken();
    refreshToken.refreshToken();
  }

  public static void onRefreshToken(final String token, final String refreshtoken, NetworkService lspServices, final RefreshTokenImple imple) {
    impl = imple;
    tokens = token;
    refreshtokens = refreshtoken;
    lspService = lspServices;
    RetrofitRefreshToken refreshToken = new RetrofitRefreshToken();
    refreshToken.refreshToken();
  }

  private void refreshToken() {
    Observable<Response<ResponseBody>> observable = lspService.getAccessToken(tokens, refreshtokens, DEFAULT_LANGUAGE, PLATFORM);
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int responseCode = responseBodyResponse.code();
            JSONObject jsonObject;
            try {
              String responseBody = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (responseCode) {
                case 200:
                  if (responseBody != null && !responseBody.isEmpty()) {
                    jsonObject = new JSONObject(responseBody);
                    if (jsonObject.getJSONObject("data") != null) {
                      JSONObject json = jsonObject.getJSONObject("data");
                      if (json.getString("accessToken") != null && !json.getString("accessToken").isEmpty()) {
                        String accessToken = json.getString("accessToken");
                        Log.d(TAG, "onNextaccessToken: " + responseBody + " responseCode " + responseCode + "   " + accessToken);
                        impl.onSuccessRefreshToken(accessToken);
                      }
                    }
                  }
                  break;
                case 440:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    if (jsonObject.getString(MESSAGE) != null && !jsonObject.getString(MESSAGE).isEmpty()) {
                      impl.sessionExpired(jsonObject.getString(MESSAGE));
                    }
                  }
                  break;
                default:
                  impl.onFailureRefreshToken();
                  break;
              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
          }

          @Override
          public void onComplete() {
          }
        });
  }

  /**
   * RefreshTokenImple callback for failure and succes refresh token
   */
  public interface RefreshTokenImple {
    void onSuccessRefreshToken(String newToken) ;

    void onFailureRefreshToken();

    void sessionExpired(String msg);
  }
}
