package com.vaidg.pro.main.chats;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.ViewPagerAdapter;
import com.vaidg.pro.pojo.chat.ChatCustomerListPojo;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import dagger.android.support.DaggerFragment;

import java.util.Objects;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatCustomerFragment extends DaggerFragment implements ChatCustomerContract.View {

    private static final String TAG = "ChatCustomerFragment";
    private Typeface fondBold, fontRegular;
    private LayoutInflater inflater;

    private ChatCustomerListFragment activeChatFragment, pastChatFragment;
    private TextView tvCountActiveChat, tvCountPastChat;

    private SessionManager sessionManager;
    @Inject
    ChatCustomerContract.Presenter presenter;
    private Context mContext;
    private ProgressDialog progressDialog;
    private Gson gson;

    private BroadcastReceiver receiver;
    private TabLayout tablayoutChat;

    public static ChatCustomerFragment newInstance() {
        return new ChatCustomerFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.attachView(this);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        Utility.progressDialogCancel(mContext, progressDialog);
        this.mContext = null;
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_chat_customer, container, false);
        this.inflater = inflater;
        init(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (tablayoutChat != null) {

            TabLayout.Tab tab = tablayoutChat.getTabAt(0);
            tab.select();
        }

    }

    private void init(View view) {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getSessionManager(mContext);
        gson = new Gson();

        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_NEW_CHAT);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(VariableConstant.INTENT_ACTION_NEW_CHAT)) {
                    if(activeChatFragment != null)
                    activeChatFragment.notifyDataSetChanged();
                } else {
                    presenter.getCustomerList(false);
                }
            }
        };
        mContext.registerReceiver(receiver, filter);


        fondBold = Utility.getFontBold(mContext);
        fontRegular = Utility.getFontRegular(mContext);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        final SearchView svChat = view.findViewById(R.id.svChat);
        tablayoutChat = view.findViewById(R.id.tablayoutChat);
        ViewPager vpChat = view.findViewById(R.id.vpChat);

        tvTitle.setTypeface(fondBold);
        activeChatFragment = ChatCustomerListFragment.newInstance(false);
        pastChatFragment = ChatCustomerListFragment.newInstance(true);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFragment(activeChatFragment, "");
        viewPagerAdapter.addFragment(pastChatFragment, "");
        vpChat.setAdapter(viewPagerAdapter);
        tablayoutChat.setupWithViewPager(vpChat);
        for (int i = 0; i < tablayoutChat.getTabCount(); i++) {
            Objects.requireNonNull(tablayoutChat.getTabAt(i)).setCustomView(getCustomTabView(i));
        }

        svChat.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                return true;
            }
        });

        svChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                svChat.setIconified(false);
            }
        });

        svChat.postDelayed(new Runnable() {
            @Override
            public void run() {
                svChat.setIconified(false);
                svChat.clearFocus();
            }
        }, 300);


        if (sessionManager.getChatCustomerList().equals("")) {
            presenter.getCustomerList(false);
        } else {
            presenter.getCustomerList(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onSuccess(sessionManager.getChatCustomerList(), false);
                }
            }, 750);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mContext.unregisterReceiver(receiver);
    }

    private void filter(String query) {
        if(query != null && !query.isEmpty()) {
            activeChatFragment.filter(query);
            pastChatFragment.filter(query);
        }
    }

    private View getCustomTabView(int i) {
        View view = inflater.inflate(R.layout.custom_tab_view_header_with_count, null, false);
        TextView tvHeader = view.findViewById(R.id.tvHeader);
        TextView tvCount = view.findViewById(R.id.tvCount);
        tvHeader.setTypeface(fondBold);
        tvCount.setTypeface(fontRegular);
        if (i == 0) {
            tvHeader.setText(getString(R.string.activeChats).toUpperCase());
            tvCountActiveChat = tvCount;
        } else {
            tvHeader.setText(getString(R.string.pastChats).toUpperCase());
            tvCountPastChat = tvCount;
        }
        return view;
    }

    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(mContext, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(mContext, progressDialog);
    }

    @Override
    public void onSuccess(String chatPojo, boolean isFromApi) {

        if (isFromApi && sessionManager.getChatCustomerList().equals(chatPojo)) {
            return;
        }
        sessionManager.setChatCustomerList(chatPojo);
        ChatCustomerListPojo chatCustomerListPojo = gson.fromJson(chatPojo, ChatCustomerListPojo.class);
        tvCountActiveChat.setText("" + chatCustomerListPojo.getData().getAccepted().size());
        tvCountPastChat.setText("" + chatCustomerListPojo.getData().getPast().size());
        if(chatCustomerListPojo.getData().getAccepted() != null && chatCustomerListPojo.getData().getAccepted().size() >0)
        activeChatFragment.notifyiDataChanged(chatCustomerListPojo.getData().getAccepted());
        else
            activeChatFragment.noChatFound();

        if(chatCustomerListPojo.getData().getPast() != null && chatCustomerListPojo.getData().getPast().size() >0)
            pastChatFragment.notifyiDataChanged(chatCustomerListPojo.getData().getPast());
        else
            pastChatFragment.noChatFound();
    }


    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, mContext);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        //Toast.makeText(mContext,mContext.getString(R.string.serverError),Toast.LENGTH_SHORT).show();
        AppController.toast();
    }


    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }
}
