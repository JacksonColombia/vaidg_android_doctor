package com.vaidg.pro.landing.newSignup.consultaionFee;

import android.content.Intent;

import com.vaidg.pro.BaseView;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;

import java.util.ArrayList;

/**
 * <h1>@ConsultationFeeContract</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 28/04/2020.
 **/
public interface ConsultationFeeContract {

    interface View extends BaseView {

        void notifyData();

        /**
         * <p>This method is display passed error message in snack bar.</p>
         */
        void showError();

        /**
         * <p>This method is display passed error message in snack bar.</p>
         *
         * @param error the error message
         */
        void showError(String error);

        /**
         * <p>This method is set fees data to display.</p>
         */
        void setFeesData();

        /**
         * <p>This method is called when get success from profile update api.</p>
         * @param intent the intent with return result
         * @param msg the server response message
         */
        void onSuccessUpdateProfile(Intent intent, String msg);

        void finish(Intent intent);
    }

    interface Presenter {

        void sendBackData();

        void getCallTypeSetting();

        void updateCallTypeSetting();

        void updateFees();

        void setData(String currencyAbrr, String currencySymbol, int minFee, int maxFee, ArrayList<String> fees);

    }
}
