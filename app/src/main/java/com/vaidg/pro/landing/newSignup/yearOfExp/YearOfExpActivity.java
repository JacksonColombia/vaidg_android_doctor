package com.vaidg.pro.landing.newSignup.yearOfExp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityYearOfExpBinding;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import javax.inject.Inject;

import static com.vaidg.pro.utility.VariableConstant.DATA;

/**
 * <h1>yearOfExpActivity</h1>
 * <p> activity for entering there year of experience in careers</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 28/04/2020.
 **/
public class YearOfExpActivity extends BaseDaggerActivity implements YearOfExpContract.View {

    @Inject
    YearOfExpContract.Presenter presenter;

    private ActivityYearOfExpBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityYearOfExpBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        init();
        initOnclickLister();
    }

    private void initOnclickLister() {
        binding.includeToolbar.btnDone.setOnClickListener(v -> {
            String year = binding.etYOE.getText() == null ? "" : binding.etYOE.getText().toString().trim();
            if (!Utility.isTextEmpty(year))
                sendBackData(year);
            else
                showError(getString(R.string.enter_yoe));

        });
    }

    private void sendBackData(String year) {
        Intent intent = new Intent();
        intent.putExtra("year", year);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void init() {
        binding.includeToolbar.tvTitle.setText(getString(R.string.yearsOfExperience));
        binding.includeToolbar.btnDone.setText(getString(R.string.save));
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        if (getIntent() != null && getIntent().hasExtra(DATA)) {
            binding.etYOE.setText(getIntent().getStringExtra(DATA));
        }
    }

    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clyearOfExp, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }
}
