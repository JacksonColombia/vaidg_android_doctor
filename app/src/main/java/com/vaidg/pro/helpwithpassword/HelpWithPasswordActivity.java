package com.vaidg.pro.helpwithpassword;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.vaidg.pro.R;
import com.vaidg.pro.otpverify.OTPVerifyActivity;
import com.vaidg.pro.databinding.ActivityHelpWithPasswordBinding;
import com.vaidg.pro.pojo.phonevalidation.PhoneValidationPojo;
import com.vaidg.pro.utility.DialogHelper;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import javax.inject.Inject;


/**
 * Created by murashid on 11-Sep-17.
 * <h1>HelpWithPasswordActivity</h1>
 * HelpWithPasswordActivity Activity is used to change the password of the user by email or phone number
 */

public class HelpWithPasswordActivity extends BaseDaggerActivity implements HelpWithPasswordContract.View, View.OnClickListener, View.OnFocusChangeListener {

    private static final String TAG = "HelpWithPassword";

    @Inject
    HelpWithPasswordContract.Presenter presenter;

    private ActivityHelpWithPasswordBinding binding;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHelpWithPasswordBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
        initViewOnClickListeners();
        initFocusOnViewListener();
        initOnKeyListener();
    }

    private void initViews() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.validatingPhone));
        progressDialog.setCancelable(false);

        binding.includeToolbar.tvTitle.setText(getString(R.string.titleHelpWithPassword));
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        binding.ccp.registerCarrierNumberEditText(binding.etPhone);
    }

    private void initViewOnClickListeners() {
        binding.btnNext.setOnClickListener(this);
        binding.llPhoneSelection.setOnClickListener(this);
        binding.llEmailSelection.setOnClickListener(this);
    }

    private void initFocusOnViewListener() {
        binding.etPhone.setOnFocusChangeListener(this);
        binding.etEmail.setOnFocusChangeListener(this);
    }

    private void initOnKeyListener() {
        binding.etPhone.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                presenter.forgotPassword(binding.ccp.getSelectedCountryCodeWithPlus(), binding.etPhone.getText() == null ? "" : binding.etPhone.getText().toString(), "1");
                return true;
            }
            return false;
        });

        binding.etEmail.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                presenter.forgotPassword(binding.ccp.getSelectedCountryCodeWithPlus(), binding.etEmail.getText() == null ? "" : binding.etEmail.getText().toString(), "2");
                return true;
            }
            return false;
        });

    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboad(this);
        Utility.progressDialogCancel(this, progressDialog);
        finish();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (b) {
            switch (view.getId()) {
                case R.id.etPhone:
                    Utility.showKeyBoard(this, binding.etPhone);
                    break;

                case R.id.etEmail:
                    Utility.showKeyBoard(this, binding.etEmail);
                    break;

            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnNext:
                if (binding.tlEmail.getVisibility() == View.VISIBLE) {
                    presenter.forgotPassword(binding.ccp.getSelectedCountryCodeWithPlus(), binding.etEmail.getText() == null ? "" : binding.etEmail.getText().toString(), "2");
                } else {
                    presenter.forgotPassword(binding.ccp.getSelectedCountryCodeWithPlus(), binding.etPhone.getText() == null ? "" : binding.etPhone.getText().toString(), "1");
                }
                break;


            case R.id.llEmailSelection:
                progressDialog.setMessage(getString(R.string.validatingEmail));
                binding.tvHelpWithPasswordMsg.setText(getString(R.string.helpWithPasswordEmailMsg));
                binding.llPhone.setVisibility(View.GONE);
                binding.tlEmail.setVisibility(View.VISIBLE);
                binding.btnNext.setText(getString(R.string.oK));

                Utility.setTextColor(this, binding.tvEmail, R.color.colorPrimary);
                Utility.setBackgroundColor(this, binding.vEmail, R.color.colorPrimary);
                Utility.setTextColor(this, binding.tvPhoneNumber, R.color.lightestTextColor);
                Utility.setBackgroundColor(this, binding.vPhoneNumber, R.color.lightestTextColor);
                binding.etEmail.postDelayed(() -> binding.etEmail.requestFocus(), 200);
                break;

            case R.id.llPhoneSelection:
                if (getCurrentFocus() != null) {
                    InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
                progressDialog.setMessage(getString(R.string.validatingPhone));
                binding.tvHelpWithPasswordMsg.setText(getString(R.string.helpWithPasswordPhoneMsg));
                binding.llPhone.setVisibility(View.VISIBLE);
                binding.tlEmail.setVisibility(View.GONE);
                binding.btnNext.setText(getString(R.string.next));

                Utility.setTextColor(this, binding.tvPhoneNumber, R.color.colorPrimary);
                Utility.setBackgroundColor(this, binding.vPhoneNumber, R.color.colorPrimary);
                Utility.setTextColor(this, binding.tvEmail, R.color.lightestTextColor);
                Utility.setBackgroundColor(this, binding.vEmail, R.color.lightestTextColor);
                break;
        }
    }

    @Override
    public void onPhoneNumberInCorrect(int resId) {
        showError(getString(resId));
    }

    @Override
    public boolean isPhoneNumberCorrect() {
        return binding.ccp.isValidFullNumber();
    }

    @Override
    public void onPhoneNumberCorrect() {

    }

    @Override
    public void onEmailInCorrect() {
        setTilError(binding.tlEmail, getString(R.string.enterValidEmail));

    }

    @Override
    public void onEmailCorrect() {
        binding.tlEmail.setErrorEnabled(false);

    }

    @Override
    public void sessionExpired(String msg) {

    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clParent, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void showProgress() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void hideProgress() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    private void setTilError(TextInputLayout textInputLayout, String err) {
        try {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(err);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccessPhone(PhoneValidationPojo phoneValidationPojo) {
        finish();
        Intent intent = new Intent(this, OTPVerifyActivity.class);
        intent.putExtra("phone", binding.etPhone.getText() == null ? "" : binding.etPhone.getText().toString());
        intent.putExtra("countryCode", binding.ccp.getSelectedCountryCodeWithPlus());
        intent.putExtra("userId", phoneValidationPojo.getData().getSid());
        intent.putExtra("expireOtp", phoneValidationPojo.getData().getExpireOtp());
        intent.putExtra("otpOption", 2);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            startActivity(intent);
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }
    }

    @Override
    public void onSuccessEmail(String msg) {
        DialogHelper.customAlertDialogCloseActivity(this, getString(R.string.emailVerification), msg, getString(R.string.oK));
    }

}
