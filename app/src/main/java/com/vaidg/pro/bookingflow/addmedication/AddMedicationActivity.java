package com.vaidg.pro.bookingflow.addmedication;

import static com.vaidg.pro.utility.Utility.pixelsToSp;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.bookingflow.popUpWindow.CategoryDropdownMenu;
import com.vaidg.pro.bookingflow.popUpWindow.DirectionDropdownMenu;
import com.vaidg.pro.bookingflow.popUpWindow.DrugRouteDropdownMenu;
import com.vaidg.pro.bookingflow.popUpWindow.FrequencyDropdownMenu;
import com.vaidg.pro.bookingflow.selectmedication.SelectMedicationActivity;
import com.vaidg.pro.pojo.addMedication.AddMedicationData;
import com.vaidg.pro.pojo.addMedication.Direction;
import com.vaidg.pro.pojo.addMedication.DrugRoute;
import com.vaidg.pro.pojo.addMedication.Frequency;
import com.vaidg.pro.pojo.addMedication.Unit;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.pojo.medication.MedicationData;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import dagger.android.support.DaggerAppCompatActivity;
import java.util.ArrayList;
import javax.inject.Inject;
import org.json.JSONObject;

public class AddMedicationActivity extends DaggerAppCompatActivity implements AddMedicationContract.View, View.OnClickListener {
    private static final String TAG = "EventStarted";
    @Inject
    AddMedicationContract.Presenter presenter;
    @BindView(R.id.tvCompoundTitle)
    TextView tvCompoundTitle;
    @BindView(R.id.tvDrugStrengthTitle)
    TextView tvDrugStrengthTitle;
    @BindView(R.id.tveventId)
    TextView tveventId;
    @BindView(R.id.tvDrugRouteTitle)
    TextView tvDrugRouteTitle;
    @BindView(R.id.tvFrequencyTitle)
    TextView tvFrequencyTitle;
    @BindView(R.id.tvDurationTitle)
    TextView tvDurationTitle;
    @BindView(R.id.tvDirectionTitle)
    TextView tvDirectionTitle;
    @BindView(R.id.tvInstructionTitle)
    TextView tvInstructionTitle;
    @BindView(R.id.etCompountName)
    EditText etCompountName;
    @BindView(R.id.etInstructionName)
    EditText etInstructionName;
    @BindView(R.id.etDirectionName)
    EditText etDirectionName;
    @BindView(R.id.etDurationName)
    EditText etDurationName;
    @BindView(R.id.etDrugStrength)
    EditText etDrugStrength;
    @BindView(R.id.etFrequencyName)
    EditText etFrequencyName;
    @BindView(R.id.etDrugUnit)
    EditText etDrugUnit;
    @BindView(R.id.etDrugRouteName)
    EditText etDrugRouteName;
    @BindView(R.id.ivBackButton)
    ImageView ivBackButton;
    @BindView(R.id.btnSaveMedication)
    Button btnSaveMedication;
    private ProgressDialog progressDialog;
    private Context mContext;
    private SessionManager sessionManager;
    private Typeface fontMedium, fontRegular, fontBold, fontLight;
    private Animation fade_open, fade_close, rotate_forward, rotate_backward;
    private ArrayList<Unit> unitArrayList = new ArrayList<>();
    private ArrayList<Frequency> frequencyArrayList = new ArrayList<>();
    private ArrayList<Direction> directionArrayList = new ArrayList<>();
    private ArrayList<DrugRoute> drugRouteArrayList = new ArrayList<>();
    private ArrayList<String> stringArrayList = new ArrayList<>();
    private String mDrugStrengthName = "", mDrugRouteName = "", mDirectionName = "", mFrequencyName = "", mUnitName = "", mInstruction = "", mDuration = "", mCompoundName = "";
    private Booking booking;
    private boolean isFromEdit = false;
    private String bookidId = "", medicationId = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addmedication);
        ButterKnife.bind(this);
        mContext = this;
        Bundle bundle = getIntent().getExtras();
        booking = (Booking) (bundle != null ? bundle.getSerializable("booking") : null);
        bookidId = booking != null ? booking.getBookingId() : "";
        medicationId = bundle != null ? bundle.getString("medicationId") : "";
        isFromEdit = bundle != null && bundle.getBoolean("isFromEdit");
        initView();
        typeFace();
    }

    private void typeFace() {
        tvCompoundTitle.setTypeface(fontBold);
        tvDrugStrengthTitle.setTypeface(fontBold);
        tvFrequencyTitle.setTypeface(fontBold);
        tvDirectionTitle.setTypeface(fontBold);
        tvInstructionTitle.setTypeface(fontBold);
        tvDurationTitle.setTypeface(fontBold);
        tveventId.setTypeface(fontBold);
        btnSaveMedication.setTypeface(fontMedium);
        tvDrugRouteTitle.setTypeface(fontBold);
        etCompountName.setTypeface(fontMedium);
        etDurationName.setTypeface(fontMedium);
        etDrugStrength.setTypeface(fontMedium);
        etFrequencyName.setTypeface(fontMedium);
        etDirectionName.setTypeface(fontMedium);
        etDrugUnit.setTypeface(fontMedium);
        etInstructionName.setTypeface(fontMedium);
        etDrugRouteName.setTypeface(fontMedium);
        etCompountName.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        etDrugStrength.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        etFrequencyName.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        tvCompoundTitle.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvDirectionTitle.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvInstructionTitle.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvDurationTitle.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvDrugStrengthTitle.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvDrugRouteTitle.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvFrequencyTitle.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tveventId.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_18), mContext));
        btnSaveMedication.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        etDrugUnit.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        etDrugRouteName.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        etDurationName.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        etDirectionName.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        etInstructionName.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
      //  etFrequencyName.setOnClickListener(this);
      //  etDrugRouteName.setOnClickListener(this);
      //  etDirectionName.setOnClickListener(this);
        etDurationName.setOnClickListener(this);
        etInstructionName.setOnClickListener(this);
        btnSaveMedication.setOnClickListener(this);
    }

    private void initView() {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.refreshing));
        progressDialog.setCancelable(false);
        if (medicationId != null && !medicationId.equals("0")) {
            presenter.getEditMedication(bookidId, medicationId);
        }
        presenter.getMedication();
        fontBold = Utility.getFontBold(this);
        fontMedium = Utility.getFontMedium(this);
        fontRegular = Utility.getFontRegular(this);
        fontLight = Utility.getFontLight(this);
        fade_open = AnimationUtils.loadAnimation(this, R.anim.fade_open);
        fade_close = AnimationUtils.loadAnimation(this, R.anim.fade_close);
        rotate_forward = AnimationUtils.loadAnimation(this, R.anim.rotate_center_to_left);
        rotate_backward = AnimationUtils.loadAnimation(this, R.anim.rotate_left_to_center);
        ivBackButton.setOnClickListener(this);
        etDrugUnit.setOnClickListener(v -> {
            if (unitArrayList != null && unitArrayList.size() > 0)
                showCategoryMenu();
        });
        etFrequencyName.setOnClickListener(v -> {
            if (frequencyArrayList != null && frequencyArrayList.size() > 0)
                showFrequencyCategoryMenu();
        });
        etDrugRouteName.setOnClickListener(v -> {
            if (drugRouteArrayList != null && drugRouteArrayList.size() > 0)
                showDrugRouteCategoryMenu();
        });
        etDirectionName.setOnClickListener(v -> {
            if (directionArrayList != null && directionArrayList.size() > 0)
                showDirectionCategoryMenu();
        });
/*
        etDurationName.setOnClickListener(v -> {
            if (directionArrayList != null && directionArrayList.size() > 0)
                showDirectionCategoryMenu();
        });
*/
    }

    private void showCategoryMenu() {
        final CategoryDropdownMenu menu = new CategoryDropdownMenu(this, unitArrayList);
        menu.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        menu.setWidth(getPxFromDp(150));
        menu.setOutsideTouchable(true);
        menu.setFocusable(true);
        menu.showAsDropDown(etDrugUnit);
        menu.setCategorySelectedListener((position, category) -> {
            menu.dismiss();
            etDrugUnit.setText(category.getName());
            // Toast.makeText(mContext, "Your favourite programming language : " + category.getName(), Toast.LENGTH_SHORT).show();
        });
    }
    private void showFrequencyCategoryMenu() {
        final FrequencyDropdownMenu menu = new FrequencyDropdownMenu(this, frequencyArrayList);
        menu.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        menu.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        menu.setOutsideTouchable(true);
        menu.setFocusable(true);
        menu.showAsDropDown(etFrequencyName);
        menu.setCategorySelectedListener((position, category) -> {
            menu.dismiss();
            for (int i = 0; i < frequencyArrayList.size(); i++) {
                frequencyArrayList.get(i).setChecked(false);
            }
            frequencyArrayList.get(position).setChecked(true);
            mFrequencyName = frequencyArrayList.get(position).getName();
            etFrequencyName.setText(mFrequencyName);
            // Toast.makeText(mContext, "Your favourite programming language : " + category.getName(), Toast.LENGTH_SHORT).show();
        });
    }
    private void showDrugRouteCategoryMenu() {
        final DrugRouteDropdownMenu menu = new DrugRouteDropdownMenu(this, drugRouteArrayList);
        menu.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        menu.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        menu.setOutsideTouchable(true);
        menu.setFocusable(true);
        menu.showAsDropDown(etDrugRouteName);
        menu.setCategorySelectedListener((position, category) -> {
            menu.dismiss();
            for (int i = 0; i < drugRouteArrayList.size(); i++) {
                drugRouteArrayList.get(i).setChecked(false);
            }
            drugRouteArrayList.get(position).setChecked(true);
            mDrugRouteName = drugRouteArrayList.get(position).getName();
            etDrugRouteName.setText(mDrugRouteName);
            // Toast.makeText(mContext, "Your favourite programming language : " + category.getName(), Toast.LENGTH_SHORT).show();
        });
    }
    private void showDirectionCategoryMenu() {
        final DirectionDropdownMenu menu = new DirectionDropdownMenu(this, directionArrayList);
        menu.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        menu.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        menu.setOutsideTouchable(true);
        menu.setFocusable(true);
        menu.showAsDropDown(etDirectionName);
        menu.setCategorySelectedListener((position, category) -> {
            menu.dismiss();
            stringArrayList.clear();
            for (int i = 0; i < category.size(); i++) {
                if (category.get(i).isChecked()) {
                    stringArrayList.add(category.get(i).getName());
                    directionArrayList.get(i).setChecked(true);
                }
            }
            mDirectionName = TextUtils.join(", ", stringArrayList);
            etDirectionName.setText(mDirectionName);
            // Toast.makeText(mContext, "Your favourite programming language : " + category.getName(), Toast.LENGTH_SHORT).show();
        });
    }

    //Convert DP to Pixel
    private int getPxFromDp(int dp) {
        return (int) (dp * getResources().getDisplayMetrics().density);
    }

    @Override
    public void startProgressBar() {
        if (!isFinishing() && progressDialog != null) {
            progressDialog.setMessage(getString(R.string.refreshing));
            progressDialog.show();
        }
    }

    @Override
    public void stopProgressBar() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, this);
    }

    @Override
    public void onSuccessResonse(AddMedicationData data) {
        if (data.getFrequency() != null && data.getFrequency().size() > 0) {
            frequencyArrayList.clear();
            frequencyArrayList.addAll(data.getFrequency());
        }
        if (data.getUnits() != null && data.getUnits().size() > 0) {
            unitArrayList.clear();
            unitArrayList.addAll(data.getUnits());
        }
        if (data.getDrugRoute() != null && data.getDrugRoute().size() > 0) {
            drugRouteArrayList.clear();
            drugRouteArrayList.addAll(data.getDrugRoute());
        }
        if (data.getDirection() != null && data.getDirection().size() > 0) {
            directionArrayList.clear();
            directionArrayList.addAll(data.getDirection());
        }
    }

    @Override
    public void onSuccessEditResonse(ArrayList<MedicationData> data) {
        for (MedicationData mMedicationData : data) {
            mDrugRouteName = mMedicationData.getDrugRoute();
            mDirectionName = mMedicationData.getDirection();
            mFrequencyName = mMedicationData.getFrequency();
            mUnitName = mMedicationData.getDrugUnit();
            mInstruction = mMedicationData.getInstruction();
            mDuration = mMedicationData.getDuration();
            mCompoundName = mMedicationData.getCompoundName();
            mDrugStrengthName = mMedicationData.getDrugStrength();
        }
        etDrugStrength.setText(mDrugStrengthName);
        etCompountName.setText(mCompoundName);
        etDrugRouteName.setText(mDrugRouteName);
        etDirectionName.setText(mDirectionName);
        etFrequencyName.setText(mFrequencyName);
        etInstructionName.setText(mInstruction);
        etDurationName.setText(mDuration);
        etDrugUnit.setText(mUnitName);
    }

    @Override
    public void onSuccessAddResponse() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable("booking", booking);
        bundle.putString("medicationId", medicationId);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        closeActivity();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeActivity();
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        Bundle bundle;
        switch (v.getId()) {
           /* case R.id.etFrequencyName:
                intent = new Intent(this, SelectMedicationActivity.class);
                bundle = new Bundle();
                intent.putExtra("isFromFrequency", true);
                bundle.putSerializable("Frequency", frequencyArrayList);
                bundle.putSerializable("booking", booking);
                bundle.putString("FrequencyName", mFrequencyName);
                intent.putExtras(bundle);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    startActivityForResult(intent, 1, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                } else {
                    startActivityForResult(intent, 1);
                    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
                break;
            case R.id.etDrugRouteName:
                intent = new Intent(this, SelectMedicationActivity.class);
                bundle = new Bundle();
                intent.putExtra("isFromDrugRoute", true);
                bundle.putSerializable("DrugRoute", drugRouteArrayList);
                bundle.putString("DrugRouteName", mDrugRouteName);
                bundle.putSerializable("booking", booking);
                intent.putExtras(bundle);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    startActivityForResult(intent, 2, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                } else {
                    startActivityForResult(intent, 2);
                    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
                break;
            case R.id.etDirectionName:
                intent = new Intent(this, SelectMedicationActivity.class);
                bundle = new Bundle();
                intent.putExtra("isFromDirection", true);
                bundle.putSerializable("Direction", directionArrayList);
                bundle.putString("DirectionName", mDirectionName);
                bundle.putSerializable("booking", booking);
                intent.putExtras(bundle);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    startActivityForResult(intent, 3, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                } else {
                    startActivityForResult(intent, 3);
                    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
                break;*/
            case R.id.etDurationName:
                intent = new Intent(this, SelectMedicationActivity.class);
                bundle = new Bundle();
                intent.putExtra("isFromDuration", true);
                bundle.putString("DurationName", mDuration);
                bundle.putSerializable("booking", booking);
                intent.putExtras(bundle);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    startActivityForResult(intent, 4, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                } else {
                    startActivityForResult(intent, 4);
                    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
                break;
            case R.id.etInstructionName:
                intent = new Intent(this, SelectMedicationActivity.class);
                bundle = new Bundle();
                intent.putExtra("isFromInstruction", true);
                bundle.putString("InstructionName", mInstruction);
                bundle.putSerializable("booking", booking);
                intent.putExtras(bundle);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    startActivityForResult(intent, 5, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                } else {
                    startActivityForResult(intent, 5);
                    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
                break;
            case R.id.ivBackButton:
                closeActivity();
                break;
            case R.id.btnSaveMedication:
                try {
                    JSONObject body = new JSONObject();
                    body.put("bookingId", bookidId);
                    body.put("compoundName", etCompountName.getText().toString().trim());
                    body.put("drugStrength", etDrugStrength.getText().toString().trim());
                    body.put("drugUnit", etDrugUnit.getText().toString().trim());
                    body.put("frequency", etFrequencyName.getText().toString().trim());
                    body.put("drugRoute", etDrugRouteName.getText().toString().trim());
                    body.put("direction", etDirectionName.getText().toString().trim());
                    body.put("duration", etDurationName.getText().toString().trim());
                    body.put("instruction", etInstructionName.getText().toString().trim());
                    if (isFromEdit) {
                        body.put("medicationId", medicationId);
                        presenter.editMedication(body);
                    } else
                        presenter.addMedication(body);
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                break;
        }
    }

    private void closeActivity() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1:
                    if (data != null)
                        if (data.getBooleanExtra("isFromFrequency", false)) {
                            frequencyArrayList = (ArrayList<Frequency>) data.getSerializableExtra("Frequency");
                            mFrequencyName = data.getStringExtra("FrequencyName");
                            etFrequencyName.setText(mFrequencyName);
                        }
                    break;
                case 2:
                    if (data != null)
                        if (data.getBooleanExtra("isFromDrugRoute", false)) {
                            drugRouteArrayList = (ArrayList<DrugRoute>) data.getSerializableExtra("DrugRoute");
                            mDrugRouteName = data.getStringExtra("DrugRouteName");
                            etDrugRouteName.setText(mDrugRouteName);
                        }
                    break;
                case 3:
                    if (data != null)
                        if (data.getBooleanExtra("isFromDirection", false)) {
                            directionArrayList = (ArrayList<Direction>) data.getSerializableExtra("Direction");
                            mDirectionName = data.getStringExtra("DirectionName");
                            etDirectionName.setText(mDirectionName);
                        }
                    break;
                case 4:
                    if (data != null)
                        if (data.getBooleanExtra("isFromDuration", false)) {
                            mDuration = data.getStringExtra("DurationName");
                            etDurationName.setText(mDuration);
                        }
                    break;
                case 5:
                    if (data != null)
                        if (data.getBooleanExtra("isFromInstruction", false)) {
                            mInstruction = data.getStringExtra("InstructionName");
                            etInstructionName.setText(mInstruction);
                        }
                    break;
            }
            if (data != null) {
                booking = (Booking) data.getSerializableExtra("booking");
                bookidId = booking != null ? booking.getBookingId() : "";
            }
        }
    }
}
