package com.vaidg.pro.pojo.shedule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vaidg.pro.pojo.booking.Booking;

import java.io.Serializable;

/**
 * Created by murashid on 11-Jan-18.
 */

public class ScheduleBookngPojo implements Serializable {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Booking data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Booking getData() {
        return data;
    }

    public void setData(Booking data) {
        this.data = data;
    }
}
