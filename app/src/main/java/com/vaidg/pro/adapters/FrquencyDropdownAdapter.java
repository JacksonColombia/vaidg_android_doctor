package com.vaidg.pro.adapters;

import static com.vaidg.pro.utility.Utility.getFontBold;
import static com.vaidg.pro.utility.Utility.getFontMedium;
import static com.vaidg.pro.utility.VariableConstant.ZERO;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.pro.R;
import com.vaidg.pro.pojo.addMedication.Frequency;
import com.vaidg.pro.utility.Utility;
import java.util.ArrayList;

public class FrquencyDropdownAdapter extends RecyclerView.Adapter<FrquencyDropdownAdapter.CategoryViewHolder> {

  private ArrayList<Frequency> frequencyArrayList = new ArrayList<>();
  private Context mContext;
  private CategorySelectedListener categorySelectedListener;

  public FrquencyDropdownAdapter(ArrayList<Frequency> frequencyArrayList){
    super();
    this.frequencyArrayList = frequencyArrayList;
  }


  public void setCategorySelectedListener(FrquencyDropdownAdapter.CategorySelectedListener categorySelectedListener) {
    this.categorySelectedListener = categorySelectedListener;
  }

  @NonNull
  @Override
  public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View  view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selection_category, parent, false);
    mContext = parent.getContext();
    return new CategoryViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull CategoryViewHolder holder, final int position) {
    final Frequency frequency = frequencyArrayList.get(position);
    holder.tvCategory.setText(frequency.getName());
    if(frequency.isChecked())
    {
      check(holder);
    }else{
      uncheck(holder);
    }
    holder.itemView.setOnClickListener(view -> {
      if(categorySelectedListener != null){
        categorySelectedListener.onCategorySelected(position, frequencyArrayList);
      }
    });
  }
  private void uncheck(CategoryViewHolder holder) {
    holder.tvCategory.setCompoundDrawablesWithIntrinsicBounds(R.drawable.checkbox_unselected,0,0,0);
    Utility.setTextColor(mContext, holder.tvCategory, R.color.normalTextColor);
    holder.tvCategory.setTypeface(holder.fontMedium);
  }

  private void check(CategoryViewHolder holder) {
    holder.tvCategory.setCompoundDrawablesWithIntrinsicBounds(R.drawable.checkbox_selected,0,0,0);
    Utility.setTextColor(mContext, holder.tvCategory, R.color.colorAccent);
    holder.tvCategory.setTypeface(holder.fontBold);
  }
  @Override
  public int getItemCount() {
    return frequencyArrayList != null ? frequencyArrayList.size() : ZERO;
  }

   class CategoryViewHolder extends RecyclerView.ViewHolder{
    AppCompatTextView tvCategory;
    AppCompatImageView ivIcon;
    Typeface fontMedium,fontBold;

    public CategoryViewHolder(View itemView) {
      super(itemView);
      tvCategory = itemView.findViewById(R.id.tvCategory);
      ivIcon = itemView.findViewById(R.id.ivIcon);
      fontMedium = getFontMedium(mContext);
      fontBold = getFontBold(mContext);
     /* tvCategory.setTypeface(fontMedium);
      tvCategory.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
*/
    }
  }

  public interface CategorySelectedListener {
    void onCategorySelected(int position, ArrayList<Frequency> category);
  }
}