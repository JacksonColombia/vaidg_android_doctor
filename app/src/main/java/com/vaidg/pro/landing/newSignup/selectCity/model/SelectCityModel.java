package com.vaidg.pro.landing.newSignup.selectCity.model;

import com.vaidg.pro.BaseModel;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.pojo.signup.CityData;
import com.vaidg.pro.pojo.signup.CityPojo;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * <h1>SelectCityModel</h1>
 * <p>This model is contain objects that's all used at activity through presenter
 *
 * @author 3embed
 * @version 1.0.20
 * @since 12/06/2020
 **/
public class SelectCityModel extends BaseModel {

    @Inject
    ArrayList<CityData> cityData;
    @Inject
    Utility utility;
    @Inject
    int position;

    @Inject
    public SelectCityModel() {
    }

    /**
     * <p>It's change selected city flag true and rest all city false in cityData.</p>
     *
     * @param position the selected city object position
     */
    public void selectCity(int position) {
        for (int i = 0; i < cityData.size(); i++) {
            if (position == i)
                cityData.get(i).setSelected(true);
            else
                cityData.get(i).setSelected(false);
        }
    }

    /**
     * <p>It's change selected city flag true and rest all city false in cityData.</p>
     *
     * @param pos the selected city object position
     * @param name the selected city object name
     */
    public void selectCity(int pos,String name) {
        for (int i = 0; i < cityData.size(); i++) {
            if (cityData.get(i).getCity().equals(name)) {
                setPosition(i);
                getCityData(i);
                cityData.get(i).setSelected(true);
            }  else
                cityData.get(i).setSelected(false);
        }
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    /**
     * This method is deserializes the specified city response into the {@link CityData} class.
     */
    public CityData getCityData(int position) {
        return cityData.get(position);
    }

    /**
     * This method is deserializes the specified city response into the {@link CityPojo} class.
     *
     * @param cityPojo the response of city data
     */
    public void parseCityData(CityPojo cityPojo) {
        cityData.addAll(cityPojo.getData());
    }

    /**
     * This method is deserializes the specified city response into the {@link CityPojo} class.
     *
     */
    public ArrayList<CityData> getCityData() {
       return cityData;
    }

    /**
     * <p>This method is return specified position {@link CityData} from the list.</p>
     *
     * @param position
     * @return an object of selected {@link CityData}
     */
    public CityData getSelectedCity(int position) {
        return cityData.get(position);
    }

    /**
     * <p>This method is check if any city is selected from the list of city
     * and return boolean flag.</p>
     *
     * @return an boolean flag true if city selected either false.
     */
    public boolean isCheck() {
        for (CityData preDefined : cityData) {
            if (preDefined.isSelected())
                return true;
        }
        return false;
    }
}
