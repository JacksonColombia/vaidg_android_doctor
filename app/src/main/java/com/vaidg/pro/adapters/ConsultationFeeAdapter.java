package com.vaidg.pro.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.textfield.TextInputEditText;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ItemConsulationSelectionBinding;
import com.vaidg.pro.landing.newSignup.consultaionFee.model.ConsultationData;
import com.vaidg.pro.utility.DecimalDigitsInputFilter;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * <h1>@ConsultationFeeAdapter</h1>
 * <p> ConsultationFeeAdapter adapter to list out the Fee Details of User</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 28/04/2020.
 **/

public class ConsultationFeeAdapter extends RecyclerView.Adapter<ConsultationFeeAdapter.ConsultationSelectionViewHolder> {

    private ArrayList<ConsultationData> arrayList;
    private Context context;

    public ConsultationFeeAdapter(ArrayList<ConsultationData> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ConsultationSelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemConsulationSelectionBinding binding = ItemConsulationSelectionBinding.
            inflate(LayoutInflater.from(parent.getContext()), parent, false);
        context = parent.getContext();
        return new ConsultationSelectionViewHolder(binding,new MyCustomEditTextListener());
    }

    @Override
    public void onBindViewHolder(@NonNull ConsultationSelectionViewHolder holder, int position) {
        holder.binding.etConsultationFee.setTag(position);
        // update MyCustomEditTextListener every time we bind a new item
        // so that it knows what item in mDataset to update
        holder.myCustomEditTextListener.updatePosition(holder.binding,holder.getAdapterPosition());
        holder.binding.tvConsultTitle.setText(arrayList.get(position).getTitle());
        holder.binding.spinnerConsult.setText(arrayList.get(position).getCurrencyAbbr());

        holder.binding.etConsultationFee.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(15, 2)});
        holder.binding.switchConsult.setOnCheckedChangeListener((buttonView, isChecked) -> {
            arrayList.get(position).setChecked(isChecked);
            if (holder.binding.switchConsult.isChecked()) {
                holder.binding.hideField.setVisibility(View.VISIBLE);
                holder.binding.tlConsultationFee.setErrorEnabled(false);
            } else {
                holder.binding.hideField.setVisibility(View.GONE);
                Utility.printLog("CFAdapter", "62 ==>>"+position );
                arrayList.get(position).setFee("0.0");
            }
        });

        if (arrayList.get(position).getFee() != null && !arrayList.get(position).getFee().trim().isEmpty()) {
            holder.binding.etConsultationFee.setText(arrayList.get(position).getFee());
            holder.binding.switchConsult.setChecked(true);
        }
        Utility.printLog("CFAdapter", "70 ==>>"+position );
        arrayList.get(position).setFee(holder.binding.etConsultationFee.getText() == null ? "0" : holder.binding.etConsultationFee.getText().toString().trim());

/*
        holder.binding.etConsultationFee.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                holder.binding.tlConsultationFee.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
*/

        if (arrayList.get(position).isChecked() && (arrayList.get(position).getFee() == null || arrayList.get(position).getFee().trim().isEmpty())) {
            holder.binding.tlConsultationFee.setError(context.getString(R.string.errorEnterConsultationFee));
        } else if (arrayList.get(position).isChecked() && Double.parseDouble(arrayList.get(position).getFee()) <= 0) {
            holder.binding.tlConsultationFee.setError(context.getString(R.string.errorEnterValidConsultationFee));
        }

    }

    @Override
    public int getItemCount() {
        return arrayList != null ? arrayList.size() : ZERO;
    }

    static class ConsultationSelectionViewHolder extends RecyclerView.ViewHolder {

        ItemConsulationSelectionBinding binding;
        public MyCustomEditTextListener myCustomEditTextListener;

        ConsultationSelectionViewHolder(@NonNull ItemConsulationSelectionBinding itemView,MyCustomEditTextListener myCustomEditTextListener) {
            super(itemView.getRoot());
            this.binding = itemView;
            this.myCustomEditTextListener = myCustomEditTextListener;
            this.binding.etConsultationFee.addTextChangedListener(myCustomEditTextListener);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }




    // we make TextWatcher to be aware of the position it currently works with
    // this way, once a new item is attached in onBindViewHolder, it will
    // update current position MyCustomEditTextListener, reference to which is kept by ViewHolder
    private class MyCustomEditTextListener implements TextWatcher {
        private int position;
        private ItemConsulationSelectionBinding binding;

        public void updatePosition(ItemConsulationSelectionBinding binding, int position) {
            this.position = position;
            this.binding = binding;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            binding.tlConsultationFee.setErrorEnabled(false);
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // no op
            try {
                String fee = editable.toString().trim();
                double amt = 0;
                binding.tlConsultationFee.setError(null);
                if(fee.length() > 0)
                    amt = Double.parseDouble(fee);
/*
                    Utility.printLog("CFAdapter", "Min Fee : " + arrayList.get(position).getMinFee()
                        + ", Max Fee : " + arrayList.get(position).getMaxFee());
*/
                Utility.printLog("CFAdapter", "94 ==>>"+position );
                arrayList.get(position).setFee(amt + "");


                if (amt < arrayList.get(position).getMinFee() || amt > arrayList.get(position).getMaxFee())
                    binding.tlConsultationFee.setError(context.getString(R.string.errorFeesMustBeBetween,
                        arrayList.get(position).getMinFee(),
                        arrayList.get(position).getMaxFee()));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
