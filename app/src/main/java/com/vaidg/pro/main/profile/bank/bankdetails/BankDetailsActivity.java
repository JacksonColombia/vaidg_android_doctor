package com.vaidg.pro.main.profile.bank.bankdetails;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.BankListAdapter;
import com.vaidg.pro.adapters.BankRazorPayListAdapter;
import com.vaidg.pro.main.profile.bank.backrazorpayaccount.BankRazorPayAccountActivity;
import com.vaidg.pro.main.profile.bank.banknewaccount.BankNewAccountActivity;
import com.vaidg.pro.main.profile.bank.banknewstripe.BankNewStripeActivity;
import com.vaidg.pro.main.profile.bank.bankrazorpay.BankRazorPayActivity;
import com.vaidg.pro.pojo.profile.bank.AccountData;
import com.vaidg.pro.pojo.profile.bank.AccountRazorPayData;
import com.vaidg.pro.pojo.profile.bank.BankList;
import com.vaidg.pro.pojo.profile.bank.BankRazorPayAccount;
import com.vaidg.pro.pojo.profile.bank.LegalEntity;
import com.vaidg.pro.pojo.profile.bank.RazorPayData;
import com.vaidg.pro.pojo.profile.bank.StripeData;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import dagger.android.support.DaggerAppCompatActivity;
import java.util.ArrayList;
import javax.inject.Inject;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>BankDetailsActivity</h1>
 * BankDetailsActivity activity for showing the BankList and Stripe Account
 */

public class BankDetailsActivity extends DaggerAppCompatActivity implements BankDetailsContract.View, BankListAdapter.RefreshBankDetails, BankRazorPayListAdapter.RefreshRazorPayBankDetails, View.OnClickListener {

    SessionManager sessionManager;

    private ProgressDialog progressDialog;
    private TextView tvStatus;
    private TextView tvStipeAccountNo;
    private TextView tvAddBankAccount;
    private TextView tvAddStripeAccount;
    private CardView cvStipeDetails, cvLinkBankAcc;
    private BankListAdapter bankListAdapter;
    private BankRazorPayListAdapter bankRazorPayListAdapter;
    private ArrayList<AccountData> bankLists;
    private ArrayList<AccountRazorPayData> razorPayBankLists;
    @Inject
    BankDetailsContract.Presenter presenter;
    private Bundle bundleBankDetails;
    private ImageView ivStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_details);
        init();
    }

    /**
     * init the views
     */
    private void init() {
        bundleBankDetails = new Bundle();
        sessionManager = SessionManager.getSessionManager(this);
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingBankDetails));
        progressDialog.setCancelable(false);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.bankDetails));
        tvTitle.setTypeface(fontBold);


        tvStatus = findViewById(R.id.tvStatus);
        TextView tvStep2 = findViewById(R.id.tvStep2);
        TextView tvStep1 = findViewById(R.id.tvStep1);
        ivStatus = findViewById(R.id.ivStatus);
        tvStipeAccountNo = findViewById(R.id.tvStipeAccountNo);
        tvAddStripeAccount = findViewById(R.id.tvAddStripeAccount);
        tvAddBankAccount = findViewById(R.id.tvAddBankAccount);
        cvStipeDetails = findViewById(R.id.cvStipeDetails);
        cvLinkBankAcc = findViewById(R.id.cvLinkBankAcc);

        tvStep2.setTypeface(fontRegular);
        tvStep1.setTypeface(fontRegular);
        tvStatus.setTypeface(fontRegular);
        tvStipeAccountNo.setTypeface(fontRegular);
        tvAddBankAccount.setTypeface(fontRegular);
        tvAddStripeAccount.setTypeface(fontRegular);
        tvStep1.setText((Utility.isFromIndia(this)) ? getResources().getString(R.string.bankRazorStep1) : getResources().getString(R.string.bankStep1));
        tvStipeAccountNo.setText((Utility.isFromIndia(this)) ? getResources().getString(R.string.connectRazorPayAccount) : getResources().getString(R.string.connectStipeAccount));

        RecyclerView rvBank = findViewById(R.id.rvBank);
        rvBank.setLayoutManager(new LinearLayoutManager(this));
        bankLists = new ArrayList<>();
        razorPayBankLists = new ArrayList<>();
      if(Utility.isFromIndia(this))
          bankRazorPayListAdapter = new BankRazorPayListAdapter(this, razorPayBankLists, getSupportFragmentManager(), this);
      else
        bankListAdapter = new BankListAdapter(this, bankLists, getSupportFragmentManager(), this);
       rvBank.setAdapter(Utility.isFromIndia(this) ? bankRazorPayListAdapter : bankListAdapter);

        tvAddStripeAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BankDetailsActivity.this,Utility.isFromIndia(BankDetailsActivity.this) ? BankRazorPayActivity.class : BankNewStripeActivity.class);                startActivity(intent);
                overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
            }
        });

        cvStipeDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  Intent intent = new Intent(BankDetailsActivity.this, BankNewStripeActivity.class);
                intent.putExtras(bundleBankDetails);
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);*/
            }
        });

        tvAddBankAccount.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(Utility.isFromIndia(this)) {
            presenter.fetchContactAndCache();
            presenter.fetchFundAccountAndCache();
        }else{
            presenter.getBankDetails();
        }
    }

    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        stripeError();
    }

    private void stripeError() {
        tvAddStripeAccount.setVisibility(View.VISIBLE);
        tvStatus.setVisibility(View.GONE);
        ivStatus.setVisibility(View.GONE);
        tvStipeAccountNo.setVisibility(View.GONE);
        tvAddStripeAccount.setOnClickListener(null);
        tvAddStripeAccount.setBackground(ContextCompat.getDrawable(this, R.drawable.rectangle_corner_gray_background));
        Utility.setTextColor(this, tvAddStripeAccount, R.color.gunsmoke);
        tvAddBankAccount.setOnClickListener(null);
        tvAddBankAccount.setBackground(ContextCompat.getDrawable(this, R.drawable.rectangle_corner_gray_background));
        Utility.setTextColor(this, tvAddBankAccount, R.color.gunsmoke);
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
        stripeError();
    }

    @Override
    public void onSuccess(StripeData stripeData) {
        VariableConstant.IS_STRIPE_ADDED = true;
        tvAddStripeAccount.setVisibility(View.GONE);
        tvStatus.setVisibility(View.VISIBLE);
        ivStatus.setVisibility(View.VISIBLE);
        tvStipeAccountNo.setVisibility(View.VISIBLE);
        if (stripeData.getExternalAccounts().getData() != null && stripeData.getExternalAccounts().getData().size() > 0)
        tvStipeAccountNo.setText(getText(R.string.stripeAccountNo) + " xxxxxx" + stripeData.getExternalAccounts().getData().get(0).getLast4());
        String status = stripeData.getIndividual().getVerification().getStatus();
        tvStatus.setText(status.substring(0, 1).toUpperCase() + status.substring(1));
        if (status.equals("verified")) {
            ivStatus.setImageResource(R.drawable.vector_tick_account_no_verified);
            ivStatus.setVisibility(View.VISIBLE);
            Utility.setTextColor(this, tvStatus, R.color.verifiedBank);
            tvAddBankAccount.setFocusable(true);
            tvAddBankAccount.setOnClickListener(this);
            tvAddBankAccount.setBackground(ContextCompat.getDrawable(this, R.drawable.selector_for_rectangle_color_primary_with_stroke));
            Utility.setTextColor(this, tvAddBankAccount, R.color.selector_colorprimary_white);

            if (stripeData.getExternalAccounts().getData().size() > 0) {
                tvAddBankAccount.setVisibility(View.GONE);
            }

        } else {
            tvStatus.setText(getString(R.string.waitingForVerification));
            ivStatus.setImageResource(R.drawable.vector_error);
            Utility.setTextColor(this, tvStatus, R.color.nonVerifiedBank);
            tvAddBankAccount.setFocusable(false);
            tvAddBankAccount.setOnClickListener(null);
            tvAddBankAccount.setBackground(ContextCompat.getDrawable(this, R.drawable.rectangle_corner_gray_background));
            Utility.setTextColor(this, tvAddBankAccount, R.color.gunsmoke);
        }

        this.bankLists.clear();
        this.bankLists.addAll(stripeData.getExternalAccounts().getData());
        bankListAdapter.notifyDataSetChanged();

        bundleBankDetails.putString("fname", stripeData.getIndividual().getFirstName());
        bundleBankDetails.putString("lname", stripeData.getIndividual().getLastName());
        bundleBankDetails.putString("country", stripeData.getIndividual().getAddress().getCountry());
        bundleBankDetails.putString("state", stripeData.getIndividual().getAddress().getState());
        bundleBankDetails.putString("city", stripeData.getIndividual().getAddress().getCity());
        bundleBankDetails.putString("address", stripeData.getIndividual().getAddress().getLine1());
        bundleBankDetails.putString("postalcode", stripeData.getIndividual().getAddress().getPostalCode());
        bundleBankDetails.putString("month", stripeData.getIndividual().getDob().getMonth());
        bundleBankDetails.putString("day", stripeData.getIndividual().getDob().getDay());
        bundleBankDetails.putString("year", stripeData.getIndividual().getDob().getYear());
    }

    @Override
    public void onRazorPaySuccess(RazorPayData razorPayData) {
        tvAddStripeAccount.setVisibility((razorPayData.isActive()) ? View.GONE : View.VISIBLE);
        tvStatus.setVisibility((razorPayData.isActive()) ? View.VISIBLE : View.GONE);
        ivStatus.setVisibility((razorPayData.isActive()) ? View.VISIBLE : View.GONE);
        tvStatus.setText((razorPayData.isActive()) ? getResources().getString(R.string.verification) : getResources().getString(R.string.waitingForVerification));
        ivStatus.setImageResource((razorPayData.isActive()) ? R.drawable.vector_tick_account_no_verified : R.drawable.vector_error);
        Utility.setTextColor(this, tvStatus,(razorPayData.isActive()) ? R.color.verifiedBank : R.color.nonVerifiedBank);
        Utility.setTextColor(this, tvAddBankAccount,(razorPayData.isActive()) ? R.color.selector_colorprimary_white : R.color.gunsmoke);
        tvAddBankAccount.setFocusable(razorPayData.isActive());
        tvAddBankAccount.setOnClickListener((razorPayData.isActive()) ? this : null);
        tvAddBankAccount.setBackground(ContextCompat.getDrawable(this,(razorPayData.isActive()) ? R.drawable.selector_for_rectangle_color_primary_with_stroke : R.drawable.rectangle_corner_gray_background));
    }

    @Override
    public void onRazorPayList(ArrayList<AccountRazorPayData> accountRazorPayData) {
        tvAddBankAccount.setVisibility((accountRazorPayData != null && accountRazorPayData.size() > 0) ? View.VISIBLE : View.VISIBLE);
        tvAddBankAccount.setFocusable(accountRazorPayData != null && accountRazorPayData.size() > 0);
        tvAddBankAccount.setOnClickListener((accountRazorPayData != null && accountRazorPayData.size() > 0) ? this : null);
        tvAddBankAccount.setBackground(ContextCompat.getDrawable(this,(accountRazorPayData != null && accountRazorPayData.size() > 0) ? R.drawable.rectangle_corner_gray_background : R.drawable.selector_for_rectangle_color_primary_with_stroke));
        Utility.setTextColor(this, tvAddBankAccount,(accountRazorPayData != null && accountRazorPayData.size() > 0) ? R.color.selector_colorprimary_white : R.color.gunsmoke);
        tvStipeAccountNo.setVisibility((accountRazorPayData != null && accountRazorPayData.size() > 0) ? View.VISIBLE : View.GONE);
        if(accountRazorPayData != null && accountRazorPayData.size() > 0) {
            String accountNumber = accountRazorPayData.get(0).getBankAccount().getAccountNumber().substring(0, 2);
            int length =
                    accountRazorPayData.get(0).getBankAccount().getAccountNumber().substring(0, accountRazorPayData.get(0).getBankAccount().getAccountNumber().length() - 4).length();
            for (int i = 0; i < length; i++) {
                accountNumber = accountNumber.concat("x");
            }
            String lastFourDigits = "";
            if (accountRazorPayData.get(0).getBankAccount().getAccountNumber().length() > 4) {
                lastFourDigits = accountRazorPayData.get(0).getBankAccount().getAccountNumber().substring(accountRazorPayData.get(0).getBankAccount().getAccountNumber().length() - 4);
            }
            accountNumber = accountNumber.concat(lastFourDigits);
            tvStipeAccountNo.setText(accountNumber);
            razorPayBankLists.clear();
            razorPayBankLists.addAll(accountRazorPayData);
            bankRazorPayListAdapter.notifyDataSetChanged();
        }
    }
    @Override
    public void showAddStipe(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        tvAddStripeAccount.setVisibility(View.VISIBLE);
        tvStatus.setVisibility(View.GONE);
        ivStatus.setVisibility(View.GONE);
        tvStipeAccountNo.setVisibility(View.GONE);
        tvAddBankAccount.setOnClickListener(null);
        tvAddBankAccount.setBackground(ContextCompat.getDrawable(this, R.drawable.rectangle_corner_gray_background));
        Utility.setTextColor(this, tvAddBankAccount, R.color.gunsmoke);
    }

    @Override
    public void showAddRazorPay(String msg,boolean active) {
     //   Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        tvAddStripeAccount.setVisibility((active) ? View.VISIBLE :View.GONE);
        tvStatus.setVisibility((active) ? View.GONE : View.VISIBLE);
        ivStatus.setVisibility((active) ? View.GONE : View.VISIBLE);
        tvStipeAccountNo.setVisibility((active) ? View.GONE : View.VISIBLE);
        tvAddBankAccount.setOnClickListener((active) ? null : this);
        tvAddBankAccount.setBackground(ContextCompat.getDrawable(this,(active) ?  R.drawable.rectangle_corner_gray_background : R.drawable.selector_for_rectangle_color_primary_with_stroke));
        Utility.setTextColor(this, tvAddBankAccount,(active) ? R.color.gunsmoke : R.color.selector_colorprimary_white);
    }
    @Override
    public void showAddRazorPayList(String msg,boolean active) {
     //   Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
   //     tvAddStripeAccount.setVisibility((active) ? View.GONE :View.VISIBLE);
   //     tvStatus.setVisibility((active) ? View.VISIBLE : View.GONE);
   //     ivStatus.setVisibility((active) ? View.VISIBLE : View.GONE);
    //    tvStipeAccountNo.setVisibility((active) ? View.VISIBLE : View.GONE);
        tvAddBankAccount.setOnClickListener((active) ? this : null);
        tvAddBankAccount.setBackground(ContextCompat.getDrawable(this,(active) ?  R.drawable.selector_for_rectangle_color_primary_with_stroke : R.drawable.rectangle_corner_gray_background));
        Utility.setTextColor(this, tvAddBankAccount,(active) ? R.color.selector_colorprimary_white : R.color.gunsmoke);
    }
    @Override
    public void onRefresh() {
        presenter.getBankDetails();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvAddBankAccount) {
            Intent intent = new Intent(this,Utility.isFromIndia(BankDetailsActivity.this) ? BankRazorPayAccountActivity.class : BankNewAccountActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
        }
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }

    @Override
    public void onRefreshRazorPay() {

    }
}
