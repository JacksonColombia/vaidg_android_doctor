package com.vaidg.pro.landing.newSignup.educationBackground;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.landing.newSignup.educationBackground.model.EducationDetailAdapter;
import com.vaidg.pro.pojo.profile.DegreeDetailsData;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * <h1>@EducationDetailsUtil</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 27/04/2020.
 **/
@Module
public class EducationDetailsUtil {

    @ActivityScoped
    @Provides
    ArrayList<DegreeDetailsData> provideArrayList() {
        return new ArrayList<>();
    }

    ;

    @ActivityScoped
    @Provides
    EducationDetailAdapter provideEducationDetailAdapter(ArrayList<DegreeDetailsData> arrayList) {
        return new EducationDetailAdapter(arrayList);
    }
}
