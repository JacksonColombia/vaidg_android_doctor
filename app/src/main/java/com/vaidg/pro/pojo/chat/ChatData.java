package com.vaidg.pro.pojo.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h>ChatData</h>
 * Created by Ali on 12/22/2017.
 */

public class ChatData implements Serializable {

    @SerializedName("bid")
    @Expose
    private String bid;
    @SerializedName("timestamp")
    @Expose
    private long timestamp;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("fromID")
    @Expose
    private String fromID;
    @SerializedName("targetId")
    @Expose
    private String targetId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("custProType")
    @Expose
    private String custProType;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getCustProType() {
        return custProType;
    }

    public void setCustProType(String custProType) {
        this.custProType = custProType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFromID() {
        return fromID;
    }

    public void setFromID(String fromID) {
        this.fromID = fromID;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }
}
