package com.vaidg.pro.landing.newSignup.consultaionFee.model;


import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.pojo.profile.calltype.CallTypeData;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class ConsultationFeeModel {

    @Inject
    ArrayList<ConsultationData> arrayList;

    private List<CallTypeData> callTypeDataList;

    @Inject
    public ConsultationFeeModel() {
    }

    public List<CallTypeData> getCallTypeDataList() {
        return callTypeDataList;
    }

    public void setCallTypeDataList(List<CallTypeData> callTypeDataList) {
        this.callTypeDataList = callTypeDataList;
    }

    public ArrayList<ConsultationData> getData() {
        return arrayList;
    }

    public void setData(String currencyAbrr, String currencySymbol, int minFee, int maxFee, ArrayList<String> fees) {
        for (int i = 0; i < arrayList.size(); i++) {
            arrayList.get(i).setCurrency(currencySymbol);
            arrayList.get(i).setCurrencyAbbr(currencyAbrr);
            arrayList.get(i).setMinFee(minFee);
            arrayList.get(i).setMaxFee(maxFee);
            if (getCallTypeDataList() != null && getCallTypeDataList().size() > 0) {
                CallTypeData callTypeData = getCallTypeDataList().get(0);
                boolean isChecked = (i == 0 ? callTypeData.getInCall() : i == 1 ? callTypeData.getOutCall() : callTypeData.getTeleCall()).equals("1");
                arrayList.get(i).setChecked(isChecked);
            }
            if (fees != null && fees.size() > 0 && fees.get(i) != null)
                arrayList.get(i).setFee((Utility.isTextEmpty(fees.get(i)) || Double.parseDouble(fees.get(i)) <= 0) ? "" : fees.get(i));
            Utility.printLog("CFAdapter", "62 ==>>"+i);
        }
    }

    public boolean isFeeEntered() {
        for (ConsultationData data : arrayList) {
            if (data.isChecked() && (data.getFee() == null || data.getFee().trim().isEmpty() || Double.parseDouble(data.getFee()) <= 0 || Double.parseDouble(data.getFee()) < data.getMinFee() || Double.parseDouble(data.getFee()) > data.getMaxFee())) {
                return false;
            }
        }
        return true;
    }
}
