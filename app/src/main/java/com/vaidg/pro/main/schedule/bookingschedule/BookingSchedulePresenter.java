package com.vaidg.pro.main.schedule.bookingschedule;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Context;
import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.booking.CancelPojo;
import com.vaidg.pro.pojo.shedule.ScheduleBookngPojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 11-Jan-18.
 */

public class BookingSchedulePresenter implements BookingScheculeContract.Presenter {

    @Inject
    BookingScheculeContract.View view;
    @Inject
    SessionManager sessionManager;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    Context context;


    @Inject
    BookingSchedulePresenter( ) {
    }


    @Override
    public void getBooking(String bookingId) {
        if(view != null)
        view.startProgressBar();
        //model.getBooking(sessionToken, bookingId);

        service.getBookingId(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM,bookingId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if(view != null)
                                    view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    ScheduleBookngPojo scheduleBookngPojo = gson.fromJson(response, ScheduleBookngPojo.class);
                                    if(view != null)
                                        view.onSuccess(scheduleBookngPojo);
                                }else
                                if(view != null)
                                    view.onFailure();
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(Utility.getAuth(sessionManager.getEmail()),  sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        getBooking(bookingId);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        if(view != null) {
                                            view.stopProgressBar();
                                            view.onFailure();
                                        }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if(view != null)
                                            view.stopProgressBar();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                    }
                                });
                                if(view != null)
                                    view.stopProgressBar();
                                break;
                            case SESSION_LOGOUT:
                                if(view != null)
                                    view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                Utility.logoutSessionExiperd(sessionManager, context);
                                break;
                            default:
                                if(view != null) {
                                    view.stopProgressBar();
                                    view.onFailure(Utility.getMessage(errorBody));
                                }
                                break;
                        }
                    } catch (Exception e) {
                        if (view != null){
                            view.stopProgressBar();
                        view.onFailure();
                    }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if(view != null) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                }

                @Override
                public void onComplete() {
                }
            });
    }


    @Override
    public  void getCancelReason(String bookingId) {
        if(view != null)
            view.startProgressBar();
       // model.getCancelReasons(bookingId);
        service.getCancelReasons(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM,bookingId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if(view != null)
                                    view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    CancelPojo cancelPojo = gson.fromJson(response, CancelPojo.class);
                                    if(view != null)
                                        view.onSuccessCancelReason(cancelPojo);
                                }else
                                if(view != null)
                                    view.onFailure();
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(Utility.getAuth(sessionManager.getEmail()),  sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        getCancelReason(bookingId);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        if(view != null) {
                                            view.stopProgressBar();
                                            view.onFailure();
                                        }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if(view != null)
                                            view.stopProgressBar();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                    }
                                });
                                if(view != null)
                                    view.stopProgressBar();
                                break;
                            case SESSION_LOGOUT:
                                if(view != null)
                                    view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                Utility.logoutSessionExiperd(sessionManager, context);
                                break;
                            default:
                                if(view != null) {
                                    view.stopProgressBar();
                                    view.onFailure(Utility.getMessage(errorBody));
                                }
                                break;
                        }
                    } catch (Exception e) {
                        if(view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if(view != null) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                }

                @Override
                public void onComplete() {
                }
            });

    }


    @Override
    public void cancelBooking(JSONObject jsonObject) {
        if(view != null)
            view.startProgressBar();
       // model.cancelBooking(sessionToken, jsonObject);

        service.cancelBooking(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM,jsonObject.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if(view != null)
                                    view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    if(view != null)
                                        view.onCancelBooking(Utility.getMessage(response));
                                }else {
                                    if (view != null)
                                        view.onFailure();
                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(Utility.getAuth(sessionManager.getEmail()),  sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        if(view != null)
                                            view.hideProgress();
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        cancelBooking(jsonObject);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        if(view != null) {
                                            view.stopProgressBar();
                                            view.onFailure();
                                        }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if(view != null)
                                            view.stopProgressBar();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                                if(view != null)
                                    view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                Utility.logoutSessionExiperd(sessionManager, context);
                                break;
                            default:
                                if(view != null) {
                                    view.stopProgressBar();
                                    view.onFailure(Utility.getMessage(errorBody));
                                }
                                break;
                        }
                    } catch (Exception e) {
                        if(view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if(view != null) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                }

                @Override
                public void onComplete() {
                }
            });

    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    /**
     * Reviewview interface for view implementation
     */
    public interface BookingScheduleview {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String msg);

        void onFailure();

        void onSuccess(ScheduleBookngPojo scheduleBookngPojo);

        void onSuccessCancelReason(CancelPojo cancelPojo);

        void onCancelBooking(String msg);
    }

}
