package com.vaidg.pro.landing.newSignup.editEducation;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;

import com.vaidg.pro.landing.newSignup.editEducation.model.AddEditEducationModel;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.signup.DegreeData;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import javax.inject.Inject;

/**
 * <h1>@AddEditEducationContract</h1>
 * <p>presenter class for busiiness logic and web services</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 27/04/2020.
 **/
public class AddEditEducationPresenter implements AddEditEducationContract.Presenter {

    @Inject
    AddEditEducationContract.View view;

    @Inject
    NetworkService service;

    @Inject
    AddEditEducationModel model;

    @Inject
    NetworkStateHolder networkStateHolder;

    @Inject
    Activity activity;

    @Inject
    public AddEditEducationPresenter() {
    }

    @Override
    public boolean validField(String degree, String university, String year) {
        return (!Utility.isTextEmpty(degree.trim()) && !Utility.isTextEmpty(university.trim()) && !Utility.isTextEmpty(year.trim()));
    }

    @Override
    public void setDegreeData(DegreeData degreeData) {
        model.setDegreeData(degreeData);
    }

    @Override
    public DegreeData getDegreeData() {
        return model.getDegreeData();
    }

    @Override
    public void parseOnActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == VariableConstant.REQUEST_CODE_DEGREE) {
                DegreeData degreeData = data.getParcelableExtra("degree");
                model.setDegreeData(degreeData);
                view.displaySelectedDegree(degreeData);
            }
        }
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }
}
