package com.vaidg.pro.main.profile.bank.bankrazorpay;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import org.json.JSONObject;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by murashid on 26-Aug-17.
 * <h1>BankRazorPayActivity</h1>
 * BankRazorPayActivity activity for adding new stripe account
 */


public class BankRazorPayActivity extends DaggerAppCompatActivity implements View.OnClickListener, BankRazorPayContract.View {

    private static final String TAG = "BankRazorPay";
    Typeface fontMedium;
    @Inject
    BankRazorPayContract.Presenter presenter;
    private EditText etName, etLName, etEmail, etCity;
    private TextInputLayout tilName, tilLastName, tilEmail, tilCity;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private String token ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_razorpay_details);
        initViews();
    }

    /**
     * <h1>initActionBar</h1>
     * initilize the action bar
     */

    private void initViews() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_x);
        }

        sessionManager = SessionManager.getSessionManager(this);
        token = AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail());

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.submitting));
        progressDialog.setCancelable(false);

        fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.addRazorPayAccount));
        tvTitle.setTypeface(fontBold);

        MaterialButton tvDone = findViewById(R.id.btnDone);
        tvDone.setText(getString(R.string.save));
        tvDone.setTypeface(fontBold);
        tvDone.setOnClickListener(v -> addBankStripeDetais());

        etName = findViewById(R.id.etName);
        etLName = findViewById(R.id.etLName);
        etEmail = findViewById(R.id.etEmail);
        etCity = findViewById(R.id.etCity);

        tilName = findViewById(R.id.tilName);
        tilLastName = findViewById(R.id.tilLastName);
        tilEmail = findViewById(R.id.tilEmail);
        tilCity = findViewById(R.id.tilCity);

        etName.setTypeface(fontMedium);
        etLName.setTypeface(fontMedium);
        etCity.setTypeface(fontMedium);


        etName.setText(sessionManager.getFirstName());
        etEmail.setText(sessionManager.getEmail());
        etLName.setText(sessionManager.getLastName());
        etCity.setText(sessionManager.getPhoneNumber());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        Utility.progressDialogCancel(this, progressDialog);
        finish();
        overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /*case R.id.tvDone:
                addBankStripeDetais();
                break;*/
        }
    }

    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onSuccess(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        closeActivity();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorFirstName() {
        resetTile(tilName, getString(R.string.enterAccountHoldername));
    }

    @Override
    public void onErrorLastName() {
        resetTile(tilLastName, getString(R.string.enterLastName));
    }

    @Override
    public void onErrorEmail() {
        resetTile(tilEmail, getString(R.string.enterValidEmail));
    }


    @Override
    public void onErrorCity() {
        resetTile(tilCity, getString(R.string.enterPhone));
    }

    /**
     * set the error for empty field
     *
     * @param textInputLayout empty field
     * @param errorMsg        error msg
     */
    void resetTile(TextInputLayout textInputLayout, String errorMsg) {
        tilName.setErrorEnabled(false);
        tilLastName.setErrorEnabled(false);
        tilEmail.setErrorEnabled(false);
        tilCity.setErrorEnabled(false);

        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(errorMsg);
        }
    }

    /**
     * method for senting data to presenter for adding bank account
     */
    private void addBankStripeDetais() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("fullName", etName.getText().toString().trim().concat("").concat(etLName.getText().toString().trim()));
            jsonObject.put("email", etEmail.getText().toString().trim());
            jsonObject.put("contact", etCity.getText().toString().trim());
            presenter.addBankDetails(jsonObject);
        } catch (Exception e) {
            Log.d(TAG, "addBankStripeDetais: " + e);
            e.printStackTrace();
        }
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }

}
