package com.vaidg.pro.landing.newSignup.selectCity;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.landing.newSignup.selectCity.model.CitySelectionAdapter;
import com.vaidg.pro.pojo.signup.CityData;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * <h1>SelectCityUtil</h1>
 * <p>This class is collection of object that bind with dagger.</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 12/06/2020
 **/

@Module
public class SelectCityUtil {

    @ActivityScoped
    @Provides
    ArrayList<CityData> provideCityData() {
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    int providePosition() {
        return 0;
    }

    @Provides
    @ActivityScoped
    CitySelectionAdapter provideCitySelectionAdapter(ArrayList<CityData> arrayList) {
        return new CitySelectionAdapter(arrayList);
    }

}
