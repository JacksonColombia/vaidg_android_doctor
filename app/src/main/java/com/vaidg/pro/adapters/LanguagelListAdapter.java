package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.pojo.language.LanguageData;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;

/**
 * Created by murashid on 08-Nov-17.
 */

public class LanguagelListAdapter extends RecyclerView.Adapter<com.vaidg.pro.adapters.LanguagelListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<LanguageData> cancelData;
    private CancelSelected cancelSelected;
    private TextView tvTemp;
    private RadioButton rbCancel;
    private Typeface fontMedium, fontRegular;
    private SessionManager sessionManager;


    public LanguagelListAdapter(Context context, ArrayList<LanguageData> cancelData,
                                CancelSelected cancelSelected) {
        this.context = context;
        this.cancelData = cancelData;
        this.cancelSelected = cancelSelected;
        sessionManager = SessionManager.getSessionManager(context);
        fontRegular = Utility.getFontRegular(context);
        fontMedium = Utility.getFontMedium(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.single_cancel_row, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.tvCancelReason.setText(cancelData.get(position).getLan_name());
/*
        if (cancelData.get(position).isChecked()) {
            tvTemp = holder.tvCancelReason;
            rbCancel = holder.rbCancel;
        }
*/


        if (cancelData.get(position).getCode().equals(DEFAULT_LANGUAGE) ||cancelData.get(position).isChecked()) {
            holder.rbCancel.setChecked(true);
            Utility.setTextColor(context, holder.tvCancelReason, R.color.colorPrimary);
        } else {
            holder.rbCancel.setChecked(false);
                Utility.setTextColor(context, holder.tvCancelReason, R.color.darkTextColor);
        }
        holder.tvCancelReason.setOnClickListener(v -> {
            if(position == holder.getAdapterPosition()) {
                cancelSelected.onCancelSelected(cancelData.get(holder.getAdapterPosition()).getCode(), String.valueOf(cancelData.get(holder.getAdapterPosition()).getLan_name()));
                Utility.setTextColor(context, holder.tvCancelReason, R.color.colorPrimary);
                holder.tvCancelReason.setTypeface(fontMedium);
                holder.rbCancel.setChecked(true);
            }else{
                Utility.setTextColor(context, tvTemp, R.color.darkTextColor);
                holder.rbCancel.setChecked(false);
                tvTemp.setTypeface(fontRegular);
            }
            notifyDataSetChanged();
          /*  if (tvTemp != null && tvTemp != holder.tvCancelReason) {
                Utility.setTextColor(context, tvTemp, R.color.darkTextColor);
              holder.rbCancel.setChecked(false);
                tvTemp.setTypeface(fontRegular);
            }
            tvTemp = holder.tvCancelReason;
            rbCancel = holder.rbCancel;*/
        });
    }

    @Override
    public int getItemCount() {
        return cancelData == null ? 0 : cancelData.size();
    }

    /**
     * <h1>SignupSelectionListener</h1>
     * Interface for selected the city callback
     */

    public interface CancelSelected {
        void onCancelSelected(String code, String name);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCancelReason;
        RadioButton rbCancel;

        ViewHolder(View itemView) {
            super(itemView);
            tvCancelReason = itemView.findViewById(R.id.tvCancelReason);
            rbCancel = itemView.findViewById(R.id.rbCancel);
            tvCancelReason.setTypeface(fontRegular);

        }
    }
}