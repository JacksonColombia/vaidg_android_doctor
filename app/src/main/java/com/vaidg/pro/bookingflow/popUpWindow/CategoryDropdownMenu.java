package com.vaidg.pro.bookingflow.popUpWindow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.CategoryDropdownAdapter;
import com.vaidg.pro.pojo.addMedication.Unit;
import java.util.ArrayList;

public class CategoryDropdownMenu extends PopupWindow {
  private Context context;
  private RecyclerView rvCategory;
  private ArrayList<Unit> units = new ArrayList<>();
  private CategoryDropdownAdapter dropdownAdapter;

  public CategoryDropdownMenu(Context context, ArrayList<Unit> units) {
    super(context);
    this.context = context;
    this.units = units;
    setupView(units);
  }

  public void setCategorySelectedListener(CategoryDropdownAdapter.CategorySelectedListener categorySelectedListener) {
    dropdownAdapter.setCategorySelectedListener(categorySelectedListener);
  }

  private void setupView(ArrayList<Unit> units) {
    View view = LayoutInflater.from(context).inflate(R.layout.item_popup_category, null);
    rvCategory = view.findViewById(R.id.rvCategory);
    rvCategory.setHasFixedSize(true);
    rvCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
    // rvCategory.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
    dropdownAdapter = new CategoryDropdownAdapter(units);
    rvCategory.setAdapter(dropdownAdapter);
    setContentView(view);
  }
}