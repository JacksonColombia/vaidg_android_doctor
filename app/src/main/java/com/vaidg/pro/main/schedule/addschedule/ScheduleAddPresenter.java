package com.vaidg.pro.main.schedule.addschedule;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AlertDialog;

import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Inject;

import okhttp3.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Response;

/**
 * Created by murashid on 02-Oct-17.
 * <h1>ScheduleAddPresenter</h1>
 * ScheduleAddPresenter presenter for ScheduleAddActivity
 *
 * @see ScheduleAddActivity
 */

public class ScheduleAddPresenter implements ScheculeAddContract.Presenter {
    @Inject
    Context context;
    @Inject
    ScheculeAddContract.View view;
    private int TIME_PICKER_INTERVAL = 30;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;

    @Inject
    ScheduleAddPresenter() {
    }

    @Override
    public void getServerTime(JSONObject jsonObject) {
        // model.getServerTime(sessionToken, jsonObject);
        try {
            if (view != null) {
                if (jsonObject.getString("repeatDay").equals("")) {
                    view.onScheduleError();
                    return;
                } else if (jsonObject.getString("repeatDay").equals("4") && jsonObject.getJSONArray("days").length() == 0) {
                    view.onDaysError();
                    return;
                } else if (jsonObject.getString("startDate").equals("")) {
                    view.onDurationError();
                    return;
                } else if (jsonObject.getString("startTime").equals("")) {
                    view.onStartTimeError();
                    return;
                } else if (jsonObject.getString("endTime").equals("")) {
                    view.onEndTimeError();
                    return;
                } else if (!isValidScheduleTime(jsonObject.getString("startTime"), jsonObject.getString("endTime"))) {
                    view.onTimeSelectedError();
                    return;
                } else if ((jsonObject.getString("inCall").equals("0")) && (jsonObject.getString("outCall").equals("0")) && (jsonObject.getString("teleCall").equals("0"))) {
                    view.onJobTypeError();
                    return;
                } else if ((!(jsonObject.getString("inCall").equals("0")) && (jsonObject.getString("slotDuration").equals(""))) ||
                        (!(jsonObject.getString("teleCall").equals("0")) && (jsonObject.getString("slotDuration").equals("")))) {
                    view.onSlotDurationError();
                    return;
                }
                if (jsonObject.getString("addresssId").equals("")) {
                    view.onLocationError();
                    return;
                }
                if (view != null)
                    view.startProgressBar();
            }
        } catch (Exception e) {
            if (view != null)
                view.startProgressBar();
            e.printStackTrace();
            return;
        }

        service.serverTime(DEFAULT_LANGUAGE, PLATFORM)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if (view != null)
                                        view.hideProgress();
                                    if (response != null && !response.isEmpty()) {
                                        jsonObject.put("deviceTime", "" + Utility.convertUTCToServerFormat(new JSONObject(response).getString("data"), ""));
                                       // Log.w("TAG", "convertUTCToServerFormat: "+Utility.convertUTCToServerFormat(new JSONObject(response).getString("data"), "") );
                                          addSchedule(jsonObject);
                                    } else {
                                        if (view != null)
                                            view.onFailure();
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            if (view != null)
                                                view.stopProgressBar();
                                            getServerTime(jsonObject);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            view.onFailure();
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, context);
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    if (view != null)
                                        view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, context);
                                    break;
                                default:
                                    if (view != null)
                                        view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    view.onFailure(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            if (view != null) {
                                view.stopProgressBar();
                                view.onFailure();
                            }
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });


    }


    @Override
    public void addSchedule(JSONObject jsonObject) {
        service.addSchedule(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if (view != null)
                                        view.hideProgress();
                                    if (response != null && !response.isEmpty()) {
                                        if (view != null)
                                            view.onSuccess(Utility.getMessage(response));
                                    } else {
                                        if (view != null)
                                            view.onFailure();
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            if (view != null)
                                                view.stopProgressBar();
                                            addSchedule(jsonObject);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            view.onFailure();
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, context);
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    if (view != null)
                                        view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    //Utility.logoutSessionExiperd(sessionManager, context);
                                    break;
                                default:
                                    if (view != null) {
                                        view.stopProgressBar();
                                        view.onFailure(Utility.getMessage(errorBody));
                                    }
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            if (view != null) {
                                view.stopProgressBar();
                                view.onFailure();
                            }
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public boolean isValidScheduleTime(String startTime, String endTime) {
        String[] startingTime = startTime.split(":");
        double startingHour = Double.parseDouble(startingTime[0]);
        if (startingTime[1].equals("30")) {
            startingHour = startingHour + 0.5;
        }
        String[] endingTime = endTime.split(":");
        double endingHour = Double.parseDouble(endingTime[0]);
        if (endingTime[1].equals("30")) {
            endingHour = endingHour + 0.5;
        }
        return !(endingHour > startingHour && (endingHour - startingHour) < 1);
    }


    @Override
    public void selectMode(int mode) {
        String selectedMode = "";
        String days = "";
        ArrayList<String> sentingDays = new ArrayList<>();


        switch (mode) {
            case 1:
                sentingDays.add("Mon");
                sentingDays.add("Tue");
                sentingDays.add("Wed");
                sentingDays.add("Thu");
                sentingDays.add("Fri");
                sentingDays.add("Sat");
                sentingDays.add("Sun");
                selectedMode = context.getString(R.string.everyDays);

                days = context.getString(R.string.monday) + "," + context.getString(R.string.tuesday) + "," + context.getString(R.string.wednesday) + ","
                        + context.getString(R.string.thursday) + "," + context.getString(R.string.friday) + "," + context.getString(R.string.saturday) + ","
                        + context.getString(R.string.sunday);

                break;
            case 2:
                sentingDays.add("Mon");
                sentingDays.add("Tue");
                sentingDays.add("Wed");
                sentingDays.add("Thu");
                sentingDays.add("Fri");
                selectedMode = context.getString(R.string.weekDays);

                days = context.getString(R.string.monday) + "," + context.getString(R.string.tuesday) + "," + context.getString(R.string.wednesday) + ","
                        + context.getString(R.string.thursday) + "," + context.getString(R.string.friday);

                break;

            case 3:
                sentingDays.add("Sat");
                sentingDays.add("Sun");
                selectedMode = context.getString(R.string.weekEnds);
                days = context.getString(R.string.saturday) + "," + context.getString(R.string.sunday);

                break;

            case 4:
                selectedMode = context.getString(R.string.selectDaysSchedule);
                view.selectedMode(selectedMode);
                break;

            default:
                break;
        }

        if (mode != 4) {
            view.selectedModeConstantDays(selectedMode, days, sentingDays);
        }
    }

    @Override
    public void sortDays(ArrayList<String> days) {
        String displayDays = "";
        ArrayList<String> sentingDays = new ArrayList<>();
        StringBuilder everyDayStringBuilder = new StringBuilder("");
        String everyDayprefix = "";
        for (String weeday : days) {
            everyDayStringBuilder.append(everyDayprefix);
            everyDayprefix = ",";
            everyDayStringBuilder.append(weeday);
        }
        displayDays = everyDayStringBuilder.toString();

        if (days.contains(context.getString(R.string.monday))) {
            sentingDays.add("Mon");
        }
        if (days.contains(context.getString(R.string.tuesday))) {
            sentingDays.add("Tue");
        }
        if (days.contains(context.getString(R.string.wednesday))) {
            sentingDays.add("Wed");
        }
        if (days.contains(context.getString(R.string.thursday))) {
            sentingDays.add("Thu");
        }
        if (days.contains(context.getString(R.string.friday))) {
            sentingDays.add("Fri");
        }
        if (days.contains(context.getString(R.string.saturday))) {
            sentingDays.add("Sat");
        }
        if (days.contains(context.getString(R.string.sunday))) {
            sentingDays.add("Sun");
        }

        view.selectedDays(displayDays, sentingDays);
    }


    @Override
    public void selectDuration(int selectedMonth, String startDay, String endDay) {
        String month = "";
        switch (selectedMonth) {
            case 1:
                month = context.getString(R.string.thisMonth);
                break;
            case 2:
                month = context.getString(R.string.twoMonth);
                break;
            case 3:
                month = context.getString(R.string.threeMonth);
                break;
            case 4:
                month = context.getString(R.string.fourMonth);
                break;
            case 5:
                month = startDay + " to " + endDay;
                break;

            default:
                break;
        }
        view.selectedDuration(month);
    }

    @Override
    public void selectTime(final int timePickerDifference, String lastSelectedTime, Context applicationContext) {
        AlertDialog.Builder builder = new AlertDialog.Builder(applicationContext);
        builder.setView(R.layout.custom_time_picker_dialog);

        final AlertDialog timerPickerDialog = builder.create();
        //timerPickerDialog.setCancelable(false);
        timerPickerDialog.show();
        final TimePicker timePicker = (TimePicker) timerPickerDialog.findViewById(R.id.timePicker);
        if (timePicker != null) {
            timePicker.setIs24HourView(false);
           Calendar calendar = Calendar.getInstance();
            calendar.setTimeZone(getInstance().getTimeZone());
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int min = calendar.get(Calendar.MINUTE);
            timePicker.setHour(hour);
            timePicker.setMinute(min);
        }
        ImageView ivClose = (ImageView) timerPickerDialog.findViewById(R.id.ivClose);
        try {
            if (!lastSelectedTime.equals("")) {
                String[] time = lastSelectedTime.split(":");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (timePicker != null) {
                        timePicker.setHour(Integer.parseInt(time[0]));
                        timePicker.setMinute(Integer.parseInt(time[1]));
                    }

                } else {
                    if (timePicker != null) {
                        timePicker.setCurrentHour(Integer.parseInt(time[0]));
                        timePicker.setCurrentMinute(Integer.parseInt(time[1]));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        setTimePickerInterval(timePicker);

        TextView tvDone = (TextView) timerPickerDialog.findViewById(R.id.tvDone);
        if (tvDone != null) {
            tvDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int hours24 = 0, hours12 = 0, mintues = 00;
                    String format = "";

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (timePicker != null) {
                            hours24 = timePicker.getHour();
                            hours12 = timePicker.getHour();
                            mintues = timePicker.getMinute();
                        }
                    } else {
                        if (timePicker != null) {
                            hours24 = timePicker.getCurrentHour();
                            hours12 = timePicker.getCurrentHour();
                            mintues = timePicker.getCurrentMinute();
                        }
                    }

                    mintues = mintues * TIME_PICKER_INTERVAL;

                   // hours12 = hours24;

                    if (hours24 == 0) {
                        hours12 += 12;
                        format = "AM";
                    } else if (hours24 == 12) {
                        format = "PM";
                    } else if (hours24 > 12) {
                        hours12 -= 12;
                        format = "PM";
                    } else {
                        format = "AM";
                    }
                  /*  if (hours24 > 12) {
                        format = "PM";
                        hours12 = hours24 - 12;
                    } else {
                        format = "AM";
                    }
                    if (hours24 == 0) {
                        hours12 = 12;
                    }
                    if (hours24 == 12) {
                        format = "PM";
                    }*/

                    if (timePickerDifference == 0) {
                        view.seletedStartTime(String.format("%02d", hours12) + ":" + String.format("%02d", mintues) + " " + format, String.format("%02d", hours24) + ":" + String.format("%02d", mintues));
                    } else {
                        view.seletedEndTime(String.format("%02d", hours12) + ":" + String.format("%02d", mintues) + " " + format, String.format("%02d", hours24) + ":" + String.format("%02d", mintues));
                    }

                    timerPickerDialog.dismiss();
                }
            });
        }
        if (ivClose != null) {
            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    timerPickerDialog.dismiss();
                }
            });
        }
    }

    @Override
    public void setTimePickerInterval(TimePicker timePicker) {
        try {
            Class<?> classForid = Class.forName("com.android.internal.R$id");
            // Field timePickerField = classForid.getField("timePicker");

            Field field = classForid.getField("minute");
            NumberPicker minutePicker = (NumberPicker) timePicker
                    .findViewById(field.getInt(null));

            minutePicker.setMinValue(0);
            minutePicker.setMaxValue(1);
            ArrayList<String> displayedValues = new ArrayList<String>();
            for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                displayedValues.add(String.format("%02d", i));
            }
            for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                displayedValues.add(String.format("%02d", i));
            }
            minutePicker.setDisplayedValues(displayedValues
                    .toArray(new String[0]));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }
}
