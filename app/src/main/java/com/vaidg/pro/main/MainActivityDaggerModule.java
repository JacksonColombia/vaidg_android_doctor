package com.vaidg.pro.main;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.dagger.FragmentScoped;
import com.vaidg.pro.main.acceptedbooking.AcceptedBookingDaggerModule;
import com.vaidg.pro.main.acceptedbooking.AcceptedBookingFragment;
import com.vaidg.pro.main.booking.BookingDaggerModule;
import com.vaidg.pro.main.booking.BookingFragment;
import com.vaidg.pro.main.chats.ChatCustomerDaggerModule;
import com.vaidg.pro.main.chats.ChatCustomerFragment;
import com.vaidg.pro.main.profile.ProfileDaggerModule;
import com.vaidg.pro.main.profile.ProfileFragment;
import com.vaidg.pro.main.schedule.ScheduleFragment;
import com.vaidg.pro.main.schedule.ScheduleFragmentDaggerModule;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * <h1>LogInDaggerModule</h1>
 * <p>This dagger module created for LogInActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
@Module
public abstract class MainActivityDaggerModule {

    @ActivityScoped
    @Binds
    abstract Activity getActivity(MainActivity mainActivity);

    @ActivityScoped
    @Binds
    abstract MainActivityContract.View getView(MainActivity mainActivity);

    @ActivityScoped
    @Binds
    abstract MainActivityContract.Presenter getPresenter(MainActivityPresenter mainActivityPresenter);

    @FragmentScoped
    @ContributesAndroidInjector(modules = {BookingDaggerModule.class})
    abstract BookingFragment bookingFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {AcceptedBookingDaggerModule.class})
    abstract AcceptedBookingFragment acceptedBookingFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {ProfileDaggerModule.class})
    abstract ProfileFragment profileFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {ScheduleFragmentDaggerModule.class})
    abstract ScheduleFragment scheduleFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {ChatCustomerDaggerModule.class})
    abstract ChatCustomerFragment chatCustomerFragment();

}
