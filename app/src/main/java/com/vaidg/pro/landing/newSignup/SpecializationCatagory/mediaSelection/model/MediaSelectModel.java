package com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection.model;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;

import com.vaidg.pro.pojo.Attribute;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * <h1>@MediaSelectModel</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class MediaSelectModel {

//    @Named("urls")
//    @Inject
//    ArrayList<String> mediaUrl;
//
    @Named("filePath")
    @Inject
    ArrayList<String> mediaFilePath;

    @Inject
    public MediaSelectModel() {
    }

    /**
     * <h2>@deleteFile</h2>
     * <p>this is used to delete file</p>
     */
    public void deleteFile(File file) {
        try {
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
        }
    }

    public void deleteMediaWith(int position) {
        mediaFilePath.remove(position);
//        mediaUrl.remove(position);
    }

//    public void addFileToAdapter(String file_path) {
//        mediaUrl.add(file_path);
//    }

    public String processThumbImage(String filePath, String thumbPth) throws Exception {
        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Video.Thumbnails.MINI_KIND);
        File file = saveBitmap(thumb, thumbPth);
        return file.getPath();
    }

    /*
     * Saving the file d*/
    private File saveBitmap(Bitmap bmp, String destination) {
        OutputStream outStream = null;
        File file = new File(destination);
        try {
            outStream = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outStream != null) {
                try {
                    outStream.flush();
                    outStream.close();
                } catch (Exception e) {
                }
            }
        }
        return file;
    }

    public String getMedia(int position) {
        return mediaFilePath.get(position);
    }

//    public ArrayList<String> getData() {
//        return mediaUrl;
//    }
    public ArrayList<String> getData() {
        return mediaFilePath;
    }

    public boolean isUploaded() {
        return mediaFilePath.size() > 0;
//        return mediaUrl.size() > 0;
    }

    public void saveData(Attribute current_item) {
        String media = "";
        if (mediaFilePath.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String s : mediaFilePath) {
                sb.append(s).append(",");
            }
            media = sb.deleteCharAt(sb.length() - 1).toString();
        }
//        if (mediaUrl.size() > 0) {
//            StringBuilder sb = new StringBuilder();
//            for (String s : mediaUrl) {
//                sb.append(s).append(",");
//            }
//            media = sb.deleteCharAt(sb.length() - 1).toString();
//        }
        current_item.setData(media);    }

    public void addFilePathToAdapter(String path) {
        mediaFilePath.add(path);
    }
}
