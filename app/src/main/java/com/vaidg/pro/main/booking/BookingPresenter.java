package com.vaidg.pro.main.booking;

import android.util.Log;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.ApiOnServer.BookingFrag.STATUS;
import static com.vaidg.pro.utility.ApiOnServer.SignUp.LATITUDE;
import static com.vaidg.pro.utility.ApiOnServer.SignUp.LONGITUDE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.ResponseBody;

import org.json.JSONObject;

import retrofit2.Response;

/**
 * Created by murashid on 11-Apr-18.
 */

public class BookingPresenter implements BookingContract.Presenter {

    private static final String TAG = "BidBooking";
    private BookingContract.View view;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;

    @Inject
    BookingPresenter() {
    }


    /**
     * method for calling api for geting the Category details
     */
    @Override
    public void getBooking(final boolean isBackground) {
        if (!isBackground && view != null) {
            view.startProgressBar();
        }
        service.getBooking(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if (view != null) {
                                        view.stopProgressBar();
                                        if (response != null && !response.isEmpty()) {
                                            if (view != null)  view.onSuccesBooking(response, true);
                                        } else
                                        if (view != null)  view.onFailure();
                                    }
                                    if (view != null) {
                                        view.stopProgressBar();
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            getBooking(isBackground);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            if (view != null) {
                                                view.stopProgressBar();
                                                view.onFailure();
                                            }
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if (view != null) {
                                                view.stopProgressBar();
                                                view.sessionExpired(Utility.getMessage(errorBody));
                                            }
                                        }
                                    });
                                    if (view != null) {
                                        view.stopProgressBar();
                                    }
                                    break;
                                case SESSION_LOGOUT:
                                    if (view != null) {
                                        view.stopProgressBar();
                                        view.sessionExpired(Utility.getMessage(errorBody));
                                    }
                                    break;
                                default:
                                    if (view != null) {
                                        view.stopProgressBar();
                                        view.onFailure(Utility.getMessage(errorBody));
                                    }
                                    break;
                            }
                        } catch (Exception e) {
                            Log.w(TAG, "onNext: " + e.getMessage());
                            if (view != null) {
                                view.stopProgressBar();
                                view.onFailure();
                            }
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.stopProgressBar();
                        view.onFailure();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void detach() {
    }

    /**
     * method for calling api for update the provider status
     *
     * @param status status 3 =>online 4 => offline
     * @param lat    latitude of the current location
     * @param lng    longitude of the current location
     */
    @Override
    public void updateProviderStatues(final String status, final String lat, final String lng) {
        if (view != null) {
            view.startProgressBar();
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(STATUS, status);
            jsonObject.put(LATITUDE, lat);
            jsonObject.put(LONGITUDE, lng);
        } catch (Exception e) {
            e.printStackTrace();
        }

        service.status(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if (view != null) {
                                        view.stopProgressBar();
                                    }
                                        if (response != null && !response.isEmpty()) {
                                            if (view != null)
                                                view.onSuccessUpdateProviderStatus(Utility.getMessage(response));
                                        } else {
                                            if (view != null)
                                            view.onFailure();
                                        }

                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            if (view != null) {
                                                view.stopProgressBar();
                                            }
                                            updateProviderStatues(status, lat, lng);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            if (view != null) {
                                                view.stopProgressBar();
                                                view.onFailure();
                                            }
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if (view != null) {
                                                view.stopProgressBar();
                                                view.sessionExpired(Utility.getMessage(errorBody));
                                            }
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    if (view != null) {
                                        view.stopProgressBar();
                                        view.sessionExpired(Utility.getMessage(errorBody));
                                    }
                                    break;
                                default:
                                    if (view != null) {
                                        view.stopProgressBar();
                                        view.onFailure(Utility.getMessage(errorBody));
                                    }
                                    break;
                            }
                        } catch (Exception e) {
                            if (view != null) {
                                view.stopProgressBar();
                                view.onFailure();
                            }
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void attachView(BookingContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }


}
