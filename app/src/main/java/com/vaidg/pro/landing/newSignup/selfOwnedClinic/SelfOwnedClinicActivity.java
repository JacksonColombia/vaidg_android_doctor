package com.vaidg.pro.landing.newSignup.selfOwnedClinic;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.android.material.textfield.TextInputLayout;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivitySelfOwnedClinicBinding;
import com.vaidg.pro.landing.newSignup.selfOwnedClinic.model.SelfOwnedClinicAdapter;
import com.vaidg.pro.location.LocationActivity;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.FileUtils;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import javax.inject.Inject;

import eu.janmuller.android.simplecropimage.CropImage;
import eu.janmuller.android.simplecropimage.Util;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.vaidg.pro.utility.VariableConstant.PROVIDER_UNDER_HOSPITAL;

/**
 * <h1>SelfOwnedClinicActivity</h1>
 * <p>  SelfOwnedClinicActivity activity to add new  Clinical Details</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public class SelfOwnedClinicActivity extends BaseDaggerActivity implements SelfOwnedClinicContract.View {

    @Inject
    SelfOwnedClinicContract.Presenter presenter;

    @Inject
    App_permission app_permission;

    @Inject
    SelfOwnedClinicAdapter clinicAdapter;

    @Inject
    Utility utility;

    private ActivitySelfOwnedClinicBinding binding;
    private boolean isLogo, isDisplayMode;
    private String doctorType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySelfOwnedClinicBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
        initOnClickListener();
    }

    private void initViews() {

        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        if (getIntent() != null && getIntent().hasExtra("isDisplayMode")) {
            isDisplayMode = getIntent().getBooleanExtra("isDisplayMode", false);
            doctorType = getIntent().getStringExtra("doctorType");
            binding.includeToolbar.tvTitle.setText(Utility.isTextEmpty(doctorType)
                    ? R.string.clinicDetails
                    : doctorType.equalsIgnoreCase(String.valueOf(PROVIDER_UNDER_HOSPITAL))
                    ? R.string.hospitalDetails : R.string.clinicDetails);
        } else {
            binding.includeToolbar.tvTitle.setText(getString(R.string.addClinic));
        }

        binding.viewPager.setAdapter(clinicAdapter);
        new TabLayoutMediator(binding.indicator, binding.viewPager, (tab, position) -> {

        }).attach();

        binding.viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }
        });

        if (isDisplayMode) {
            updateUI();
        }else{
            binding.tilClinicLoc.setEndIconDrawable(R.drawable.ic_right_arrow);
            binding.tilClinicLoc.setEndIconMode(TextInputLayout.END_ICON_CUSTOM);
        }
    }

    private void initOnClickListener() {
        binding.ivCSelfOwnedClinicCameraSmall.setOnClickListener(this::onClick);
        binding.ivCSelfOwnedClinicImgDel.setOnClickListener(this::onClick);
        binding.ivCSelfOwnedClinicLogoCamSmall.setOnClickListener(this::onClick);
        binding.etClinicLoc.setOnClickListener(this::onClick);
        binding.btnSelfClinicSave.setOnClickListener(this::onClick);
    }

    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivCSelfOwnedClinicCameraSmall:
                isLogo = false;
                presenter.openChooser();
                break;

            case R.id.ivCSelfOwnedClinicImgDel:
                presenter.deletePostImage(binding.viewPager.getCurrentItem());
                break;

            case R.id.ivCSelfOwnedClinicLogoCamSmall:
                isLogo = true;
                presenter.openChooser();
                break;

            case R.id.etClinicLoc:
                launchLocation();
                break;

            case R.id.btnSelfClinicSave:
                presenter.validate(binding.etClinicName.getText() == null ? "" : binding.etClinicName.getText().toString(),
                        binding.etClinicLoc.getText() == null ? "" : binding.etClinicLoc.getText().toString());
                break;
        }
    }

    private void launchLocation() {
        Intent intent = new Intent(this, LocationActivity.class);
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_LOCATION);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == VariableConstant.CAMERA_CODE && resultCode == Activity.RESULT_OK) {
//            if(isLogo){
//                startCropImage(presenter.getRecentTemp());
//            }else {
//            presenter.compressedMedia(presenter.getRecentTemp());
//            }
                presenter.upDateToGallery();
                startCropImage(presenter.getRecentTemp());
        } else if (requestCode == VariableConstant.GALLERY_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null && data.getData() != null) {
                Uri uri = data.getData();
                String file_path = FileUtils.getPath(this, uri);
                startCropImage(file_path);
             /*   if(isLogo){
                    startCropImage(file_path);
                }else {*/
//                if (!Utility.isTextEmpty(file_path)) {
//                    presenter.compressedMedia(file_path);
//                } else {
//                    showError("file_path_error");
//                }
//                }

            } else {
                showError("file_path_error");
            }
        } else if (requestCode == VariableConstant.REQUEST_CODE_CROP_IMAGE && resultCode == Activity.RESULT_OK) {
            String file_path = data.getStringExtra(CropImage.IMAGE_PATH);
            if (!Utility.isTextEmpty(file_path)) {
                presenter.compressedMedia(file_path);
            }
        }
        presenter.parseOnActivityData(resultCode, requestCode, data);
        super.onActivityResult(requestCode, resultCode, data);

    }


    public void startCropImage(String recentTemp) {
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, recentTemp);
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        intent.putExtra("locale", Locale.US.getDisplayLanguage());
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_CROP_IMAGE);
    }

    @Override
    public void openGallery() {
        String chooseTitle = "";
        Intent intent = new Intent();
        intent.setType("image/*");
        chooseTitle = "select Image";
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, chooseTitle), VariableConstant.GALLERY_CODE);
    }

    @Override
    public void updateUI() {
        binding.ivCSelfOwnedClinicLogoCamSmall.setVisibility(GONE);
        binding.ivCSelfOwnedClinicCameraSmall.setVisibility(GONE);
        binding.btnSelfClinicSave.setVisibility(GONE);

        String clinicHospitalLogo = getIntent().getStringExtra("logo");
        if (!Utility.isTextEmpty(clinicHospitalLogo)) {
            binding.sdvSelfOwnedClinicLogo.setImageURI(Uri.parse(clinicHospitalLogo));
        }

        binding.gBGPLaceholder.setVisibility(GONE);
        binding.gLogoPlaceholder.setVisibility(GONE);

        binding.etClinicName.setEnabled(false);
        binding.etClinicLoc.setEnabled(false);
        binding.etClinicName.setText(getIntent().getStringExtra("clinicHospitalName"));
        binding.etClinicLoc.setText(getIntent().getStringExtra("address"));
        if (!doctorType.equalsIgnoreCase(String.valueOf(PROVIDER_UNDER_HOSPITAL)) && !Utility.isTextEmpty(getIntent().getStringExtra("clinicImages"))) {
            String clinicImage = getIntent().getStringExtra("clinicImages");
            String[] medias = clinicImage.contains(",") ? clinicImage.split(",") : new String[] {clinicImage};
            clinicAdapter.addAll(new ArrayList<>(Arrays.asList(medias)));
            notifyAdapter();
        }

    }

    @Override
    public void notifyAdapter() {
        clinicAdapter.notifyDataSetChanged();
        binding.indicator.setVisibility(clinicAdapter.getItemCount() > 0 ? VISIBLE : GONE);
    }

    @Override
    public void HandlePostDeleteBtn(boolean hasItem) {
        if (hasItem) {
            binding.ivCSelfOwnedClinicImgDel.setVisibility(VISIBLE);
            binding.gBGPLaceholder.setVisibility(GONE);
        } else {
            binding.ivCSelfOwnedClinicImgDel.setVisibility(GONE);
            binding.gBGPLaceholder.setVisibility(VISIBLE);
        }
    }

    @Override
    public void setLogo(String url, Uri fileUri) {
//        binding.sdvSelfOwnedClinicLogo.setImageURI(url);
      //  binding.sdvSelfOwnedClinicLogo.setImageURI(fileUri);
      binding.sdvSelfOwnedClinicLogo.setImageBitmap(Utility.getCircleCroppedBitmap(BitmapFactory.decodeFile(url)));
      binding.gLogoPlaceholder.setVisibility(GONE);
}

    @Override
    public boolean isLogo() {
        return isLogo;
    }

    @Override
    public void showLocatioon(String locationDetail) {
        binding.etClinicLoc.setText(locationDetail);
    }

    @Override
    public void sendBackData(Intent intent) {
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void openCamera(Uri uri) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, VariableConstant.CAMERA_CODE);
    }

    @Override
    public void showError(String message) {
        Snackbar snackbar = Snackbar.make(binding.clParent, message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
