package com.vaidg.pro.main.profile.bank.bankrazorpay;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>ProfileDaggerModule</h1>
 * <p>This dagger module created for ProfileFragment to bind injected objects</p>
 *
 * @author 3embed.
 * @version 1.0.20.
 * @since 19/06/2020.
 **/
@Module
public abstract class BankRazorPayDaggerModule {

    @ActivityScoped
    @Binds
    abstract Activity getActivity(BankRazorPayActivity mainActivity);


    @ActivityScoped
    @Binds
    abstract BankRazorPayContract.Presenter providePresenter(BankRazorPayPresenter presenter);

    @ActivityScoped
    @Binds
    abstract BankRazorPayContract.View provideView(BankRazorPayActivity activity);

}
