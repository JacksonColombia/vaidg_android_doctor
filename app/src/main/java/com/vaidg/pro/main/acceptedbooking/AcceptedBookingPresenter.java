package com.vaidg.pro.main.acceptedbooking;

import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.util.Log;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.booking.AcceptedBookingPojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by murashid on 12-Apr-18.
 */

public class AcceptedBookingPresenter implements AcceptedBookingContract.Presenter{

    private static final String TAG = "BidBooking";
    private AcceptedBookingContract.View view;
    @Inject
    NetworkService service;
    @Inject
    SessionManager sessionManager;
    @Inject
    Gson gson;

    @Inject
    AcceptedBookingPresenter() {
    }


    @Override
    public void getBookingByDate(String startDate, String endDate, boolean isBackground) {
        if (!isBackground) {
            if(view != null)
            view.showProgress();
        }
        Log.d(TAG, "getBookingByDate: " + startDate + " " + endDate);

        service.getBookings(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, startDate, endDate)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if (response != null && !response.isEmpty()) {
                                    if(view != null)
                                    view.hideProgress();
                                    AcceptedBookingPojo bookingPojo = gson.fromJson(response, AcceptedBookingPojo.class);
                                    if(view != null)
                                    view.onSuccesBooking(bookingPojo.getData());
                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        if(view != null) {
                                            view.onNewToken(newToken);
                                            view.hideProgress();
                                        }
                                        getBookingByDate(startDate, endDate, isBackground);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        if(view != null) {
                                                view.hideProgress();
                                            view.onFailure();
                                        }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if(view != null) {
                                            view.hideProgress();
                                            view.sessionExpired(msg);
                                        }
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                                if(view != null) {
                                    view.hideProgress();
                                    view.sessionExpired(Utility.getMessage(errorBody));
                                }
                                break;
                            default:
                                if(view != null) {
                                    view.hideProgress();
                                    view.onFailure(Utility.getMessage(errorBody));
                                }  break;
                        }
                    } catch (Exception e) {
                        if(view != null){
                        view.hideProgress();
                        view.onFailure();}
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if(view != null) {
                        view.hideProgress();
                        view.onFailure();
                    }
                }

                @Override
                public void onComplete() {
                }
            });
    }

    @Override
    public void attachView(AcceptedBookingContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

}
