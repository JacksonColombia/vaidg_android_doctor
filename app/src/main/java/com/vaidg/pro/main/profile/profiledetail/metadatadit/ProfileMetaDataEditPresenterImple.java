package com.vaidg.pro.main.profile.profiledetail.metadatadit;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.vaidg.pro.R;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.model.DocumentSelectionModel;
import com.vaidg.pro.main.profile.profiledetail.metadatadit.model.ProfileMetaDataEditAdapter;
import com.vaidg.pro.main.profile.profiledetail.metadatadit.model.ProfileMetaDataEditModel;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.pojo.profile.metaData.MetaDataArr;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.FileUtils;
import com.vaidg.pro.utility.MediaBottomSelector;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.fileUtil.AppFileManger;
import com.videocompressor.com.CompressImage;
import com.videocompressor.com.DataModel.CompressedData;
import com.videocompressor.com.RxCompressObservable;
import com.videocompressor.com.VideoCompressor;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.AMAZON_BASE_URL;
import static com.vaidg.pro.utility.VariableConstant.BUCKET_NAME;
import static com.vaidg.pro.utility.VariableConstant.HTTP;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DATE_FUTURE;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DATE_PAST;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_PICTURE_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_TIME;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_VIDEO_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

public class ProfileMetaDataEditPresenterImple implements ProfileMetaDataEditContract.Presenter, ProfileMetaDataEditAdapter.OnItemClickListener, App_permission.Permission_Callback, MediaBottomSelector.Callback, UploadFileAmazonS3.UploadCallBack {

    private static final String TAG = "ProfileMetaDataEditPresenterImple";
    @Inject
    Activity activity;
    @Inject
    ProfileMetaDataEditContract.View view;
    @Inject
    ProfileMetaDataEditModel model;
    @Inject
    App_permission app_permission;
    @Inject
    Utility utility;
    @Inject
    MediaBottomSelector mediaBottomSelector;
    @Inject
    AppFileManger appFileManger;
    @Inject
    UploadFileAmazonS3 uploadFileAmazonS3;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    SessionManager sessionManager;
    @Inject
    NetworkService service;
    @Inject
    CompressImage compressImage;
    @Inject
    VideoCompressor videoCompressor;

    private File temp_file = null;

    private int type;
    private final String CAMERA = "camera";
    private final String GALLERY = "gallery";
    private static final String DOC_PICK = "doc_pick";
    public static final String DOWNLOAD_FILE = "download_file";

    private int childPosition, uploadPosition;
    private PreDefined selectedPreDefined;

    @Inject
    public ProfileMetaDataEditPresenterImple() {
    }

    @Override
    public void addMetaData(MetaDataArr data) {
        type = Utility.isTextEmpty(data.getType()) ? -1 : Integer.parseInt(data.getType());
        model.addMetaData(data);
        view.notifyAdapter();
    }

    @Override
    public void setData(String data) {
        Utility.printLog(getClass().getSimpleName(), "Updated Media : " + data);
        if (childPosition == -1) model.setData(data);
        else model.setPredefineData(childPosition, data);
        view.notifyAdapter();
    }

    @Override
    public void openChooser() {
        temp_file = null;
        mediaBottomSelector.showBottomSheet(this);
    }

    @Override
    public void deleteDocument(int position) {
        model.deleteDocument(position);
        view.notifyAdapter();
    }

    @Override
    public boolean isValid() {
        if (!model.isCheck()) {
            view.showError(activity.getString(R.string.list_selection_error));
            return false;
        }
        updateMetaData();
        return true;
    }

    @Override
    public void saveData(MetaDataArr metaDataArr) {
        model.saveData(metaDataArr);
    }

    @Override
    public boolean isValidMediaSize() {
        boolean valid = false;
        if (type == SPECIALIZATION_VIEW_PICTURE_UPLOAD)
            return true;
        if (temp_file != null) {
            valid = utility.isValidVideoSize(temp_file.length());
        }
        if (!valid) {
            if (view != null)
                view.showError(activity.getString(R.string.profile_size_limit_msg));
            model.deleteFile(temp_file);
        }
        return valid;
    }

    @Override
    public boolean isValidMediaSize(String filePath) {
        boolean valid = false;
        if (type != SPECIALIZATION_VIEW_VIDEO_UPLOAD)
            return true;
        try {
            File tempFile = new File(filePath);
            valid = utility.isValidVideoSize(tempFile.length());
            if (!valid) {
                if (view != null)
                    view.showError(activity.getString(R.string.profile_size_limit_msg));
                model.deleteFile(tempFile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valid;
    }

    @Override
    public boolean isValidDocumentSize(String filePath) {
        boolean valid = false;
        try {
            File tempFile = new File(filePath);
            valid = utility.isValidDocumentSize(tempFile.length());
            if (!valid) {
                if (view != null)
                    view.showError(activity.getString(R.string.errorDocumentSizeLimit));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valid;
    }

    @Override
    public void upDateToGallery() {
        if (temp_file == null)
            return;
      /*  Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(temp_file.getPath());
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);*/
        // Log.d(TAG, "upDateToGallery: 1"+temp_file.getPath()+ "=====>\n"+temp_file.getAbsolutePath());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            final Uri contentUri = Uri.fromFile(temp_file);
            scanIntent.setData(contentUri);
            activity.sendBroadcast(scanIntent);
        } else {
            activity.sendBroadcast(
                    new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(temp_file.getAbsolutePath())));
        }
    }

    @Override
    public void onActivityResultParse(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == VariableConstant.PICKFILE_RESULT_CODE) {
                if (data != null && data.getData() != null) {
                    Uri uri = data.getData();
                    String file_path = FileUtils.getPath(activity, uri);
                    if (!Utility.isTextEmpty(file_path)) {
                        if (isValidDocumentSize(file_path)) {
                            model.setPredefineData(childPosition, file_path);
                            view.notifyAdapter();
                        }
                    } else {
                        view.showError(activity.getString(R.string.errorDocumentPicker));
                    }
                } else {
                    view.showError(activity.getString(R.string.errorDocumentPicker));
                }
            } else if (requestCode == VariableConstant.CAMERA_CODE) {
                if (isValidMediaSize()) {
                    upDateToGallery();
                    String currentData = model.getData();
                    if (currentData != null && !currentData.trim().isEmpty()) {
                        currentData = currentData.concat(",").concat(temp_file.getPath());
                    } else {
                        currentData = temp_file.getPath();
                    }
                    childPosition = -1;
                    setData(currentData);
                }
            } else if (requestCode == VariableConstant.GALLERY_CODE) {
                if (data != null && data.getData() != null) {
                    Uri uri = data.getData();
                    String file_path = FileUtils.getPath(activity, uri);
                    if (!Utility.isTextEmpty(file_path)) {
                        if (isValidMediaSize(file_path)) {
                            String currentData = model.getData();
                            if (currentData != null && !currentData.trim().isEmpty()) {
                                currentData = currentData.concat(",").concat(file_path);
                            } else {
                                currentData = file_path;
                            }
                            childPosition = -1;
                            setData(currentData);
                        }
                    } else {
                        view.showError("file_path_error");
                    }
                } else {
                    view.showError("file_path_error");
                }
            }
        }
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }

    @Override
    public void onItemClick(int position, MetaDataArr metaDataArr) {

        if (Utility.isTextEmpty(metaDataArr.getType()))
            return;

        childPosition = -1;
        switch (Integer.parseInt(metaDataArr.getType())) {
            case SPECIALIZATION_VIEW_DATE_FUTURE:
            case SPECIALIZATION_VIEW_DATE_PAST:
                view.displayDatePicker(Integer.parseInt(metaDataArr.getType()));
                break;
            case SPECIALIZATION_VIEW_TIME:
                view.displayTimePicker();
                break;
        }
    }

    @Override
    public void onChildItemClick(int position, int childPosition, PreDefined preDefined) {
        this.childPosition = childPosition;
        switch (preDefined.getType()) {
            case DocumentSelectionModel.UPLOAD_DOCUMENT:
                if (preDefined.getData() != null && !preDefined.getData().isEmpty()) {
                    deleteDocument(position);
                } else {
                    ArrayList<App_permission.Permission> permissions = new ArrayList<>();
                    permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
                    permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
                    app_permission.getPermission(DOC_PICK, permissions, this);
                }
                break;
            case DocumentSelectionModel.DATE_TIME_CURRENT_TO_FUTURE:
            case DocumentSelectionModel.DATE_TIME_PAST_TO_CURRENT:
                Utility.hideKeyboad(activity);
                view.displayDatePicker(preDefined.getType());
                break;
        }
    }

    @Override
    public void onChildDocumentClick(int position, int childPosition, PreDefined preDefined) {
        selectedPreDefined = preDefined;
        view.launchViewer(preDefined.getData());
    }

    @Override
    public void onChildMediaDelete(ArrayList<String> data) {
        childPosition = -1;
        setData(TextUtils.join(",", data));
    }

    @Override
    public void onChildMediaAdd() {
        openChooser();
    }

    @Override
    public void onCamera() {
        ArrayList<App_permission.Permission> permissions = new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.CAMERA);
        app_permission.getPermission(CAMERA, permissions, this);
    }

    @Override
    public void onGallery() {
        ArrayList<App_permission.Permission> permissions = new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
        app_permission.getPermission(GALLERY, permissions, this);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        if (isAllGranted && tag.equals(CAMERA)) {
            if (view != null) {
                try {
                    if (type == SPECIALIZATION_VIEW_PICTURE_UPLOAD)
                        temp_file = appFileManger.getImageFile();
                    else
                        temp_file = appFileManger.getVideoFile();

                    view.openCamera(utility.getUri_Path(temp_file), type);
                } catch (Exception e) {
                    view.showError(e.getMessage());
                }
            }
        } else if (isAllGranted && tag.equals(GALLERY)) {
            if (view != null)
                view.openGallery(type);
        } else if (isAllGranted && tag.equals(DOC_PICK)) {
            view.openDocPicker();
        } else if (isAllGranted && tag.equals(DOWNLOAD_FILE)) {
            view.downloadFile(selectedPreDefined.getData());
        }
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        switch (tag) {
            case GALLERY:
                app_permission.show_Alert_Permission(activity.getString(R.string.photo_access_text), activity.getString(R.string.gallery_acess_subtitle),
                        activity.getString(R.string.gallery_acess_message), stringArray);
                break;
            case CAMERA:
                app_permission.show_Alert_Permission(activity.getString(R.string.camera_access_text), activity.getString(R.string.camera_acess_subtitle),
                        activity.getString(R.string.camera_acess_message), stringArray);
                break;
            case DOC_PICK:
                app_permission.show_Alert_Permission(activity.getString(R.string.storage_access_denied), activity.getString(R.string.storage_denied_subtitle),
                        activity.getString(R.string.gallery_acess_message), stringArray);
                break;
            case DOWNLOAD_FILE:
                app_permission.show_Alert_Permission(activity.getString(R.string.storage_access_denied), activity.getString(R.string.download_denied_subtitle),
                        activity.getString(R.string.gallery_acess_message), stringArray);
                break;

        }
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean permanent) {
        if (permanent) {
            switch (tag) {
                case GALLERY:
                    app_permission.showAlertDeniedPermission(activity.getString(R.string.photo_denied_text), activity.getString(R.string.gallery_denied_subtitle),
                            activity.getString(R.string.gallery_denied_message));
                    break;
                case CAMERA:
                    app_permission.showAlertDeniedPermission(activity.getString(R.string.camera_denied_text), activity.getString(R.string.camera_denied_subtitle),
                            activity.getString(R.string.camera_denied_message));
                    break;
                case DOC_PICK:
                    app_permission.showAlertDeniedPermission(activity.getString(R.string.storage_access_denied), activity.getString(R.string.storage_denied_subtitle),
                            activity.getString(R.string.gallery_denied_message));
                    break;
                case DOWNLOAD_FILE:
                    app_permission.showAlertDeniedPermission(activity.getString(R.string.storage_access_denied), activity.getString(R.string.download_denied_subtitle),
                            activity.getString(R.string.gallery_acess_message));
                    break;
            }
        }
    }

    @Override
    public void compressImage(String filePath, UploadFileAmazonS3.UploadCallBack callBack) {
        Observer<CompressedData> observer = new Observer<CompressedData>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(CompressedData value) {
                if (value != null) {
                    amazonUpload(new File(value.getPath()), callBack);
                } else {
                    view.hideProgress();
                    view.showError(activity.getString(R.string.somethingWentWrong));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                if (view != null) {
                    view.hideProgress();
                    view.showError("Failed to collect!");
                }
            }

            @Override
            public void onComplete() {
            }
        };
        RxCompressObservable observable = compressImage.compressImage(activity, filePath);
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }

    @Override
    public void compressedVideo(String file_path, UploadFileAmazonS3.UploadCallBack callBack) {
        Observer<CompressedData> observer = new Observer<CompressedData>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(CompressedData value) {
                try {
                    if (value != null) {
                        amazonUpload(new File(value.getPath()), callBack);
                    } else {
                        view.hideProgress();
                        view.showError(activity.getString(R.string.somethingWentWrong));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (view != null) {
                        view.hideProgress();
                        view.showError(e.getMessage());
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                if (view != null) {
                    view.hideProgress();
                    view.showError(e.getMessage());
                }
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
            }
        };
        Utility.printLog(TAG, "Upload FIle Path : " + file_path);
        RxCompressObservable observable = videoCompressor.compressVideo(file_path);
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Override
    public boolean isRemoteUrl(String url) {
        return url.startsWith(HTTP);
    }

    @Override
    public void amazonUpload(File mFileTemp, UploadFileAmazonS3.UploadCallBack callBack) {
        String BUCKETSUBFOLDER = VariableConstant.WORK_IMAGE;
        final String imageUrl = "https://" + BUCKET_NAME + "." + AMAZON_BASE_URL
                + BUCKETSUBFOLDER + "/"
                + mFileTemp.getName();
        Utility.printLog(TAG, "Upload Path : " + imageUrl);
        uploadFileAmazonS3.Upload_data(BUCKETSUBFOLDER, mFileTemp.getName(), mFileTemp, callBack);
    }

    @Override
    public void sucess(String sucess) {
        if (sucess != null && !sucess.isEmpty()) {
            String[] mediaArray = model.getData().split(",");
            mediaArray[uploadPosition] = sucess;
            childPosition = -1;
            model.setData(TextUtils.join(",", mediaArray));
            isAllMediaUploaded();
        } else {
            if (view != null) {
                view.hideProgress();
                view.showError(activity.getString(R.string.serverError));
            }
        }

    }

    @Override
    public void error(String errormsg) {
        if (view != null) {
            view.hideProgress();
            view.showError(errormsg);
        }
    }

    @Override
    public void isAllMediaUploaded() {
        String uploadPath = null;

        String[] mediaArray = model.getData().split(",");
        for (int i = 0; i < mediaArray.length; i++) {
            if (!isRemoteUrl(mediaArray[i])) {
                uploadPosition = i;
                uploadPath = mediaArray[i];
                break;
            }
        }

        if (uploadPath == null) {
            callUpdateMetaDataApi();
            return;
        }

        if (type == SPECIALIZATION_VIEW_PICTURE_UPLOAD) {
            compressImage(uploadPath, this);
        } else if (type == SPECIALIZATION_VIEW_VIDEO_UPLOAD) {
            compressedVideo(uploadPath, this);
        }
    }

    @Override
    public void updateMetaData() {
        Utility.hideKeyboad(activity);
        if (networkStateHolder.isConnected()) {
            if (view != null)
                view.showProgress();

            if (type == SPECIALIZATION_VIEW_PICTURE_UPLOAD || type == SPECIALIZATION_VIEW_VIDEO_UPLOAD) {
                isAllMediaUploaded();
            } else {
                callUpdateMetaDataApi();
            }

        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void callUpdateMetaDataApi() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("metaId", model.getId());
            jsonObject.put("metaData", model.getData());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        service.updateProfile(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if(view != null)
                                    view.hideProgress();
                                    if (response != null && !response.isEmpty()) {
                                        if(view != null)
                                        view.sendBackData();
                                    } else {
                                        if (view != null)
                                            view.showError();
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    if(view != null)
                                    view.hideProgress();
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            if(view != null)
                                                view.hideProgress();
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            callUpdateMetaDataApi();
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            if(view != null)
                                            {
                                                view.hideProgress();
                                            }
                                            getInstance().toast(activity.getResources().getString(R.string.serverError));
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view !=null)
                                                view.hideProgress();
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, activity);
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    if(view != null)
                                    view.hideProgress();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, activity);
                                    break;
                                default:
                                    if(view != null)
                                    view.hideProgress();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            if(view != null) {
                                view.hideProgress();
                                view.showError();
                            }
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.hideProgress();
                            //view.stopProgressBar();
                            view.showError();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
