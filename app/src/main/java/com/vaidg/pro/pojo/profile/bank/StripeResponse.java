package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StripeResponse implements Serializable {

	@SerializedName("data")
	@Expose
	private StripeData data;

	@SerializedName("message")
	@Expose
	private String message;

	StripeResponse(){

	}

	public StripeData getData(){
		return data;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"StripResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}