package com.vaidg.pro.pojo.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 08-Nov-17.
 */

public class CancelPojo implements Serializable {
    @SerializedName("data")
    @Expose
    ArrayList<CancelData> data;
    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CancelData> getData() {
        return data;
    }

    public void setData(ArrayList<CancelData> data) {
        this.data = data;
    }

}
