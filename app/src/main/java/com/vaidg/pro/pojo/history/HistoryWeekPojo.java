package com.vaidg.pro.pojo.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 08-Dec-17.
 */

public class HistoryWeekPojo implements Serializable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<HistoryWeekData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<HistoryWeekData> getData() {
        return data;
    }

    public void setData(ArrayList<HistoryWeekData> data) {
        this.data = data;
    }


}
