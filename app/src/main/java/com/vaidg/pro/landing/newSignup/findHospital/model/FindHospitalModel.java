package com.vaidg.pro.landing.newSignup.findHospital.model;


import com.vaidg.pro.BaseModel;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import javax.inject.Inject;

public class FindHospitalModel extends BaseModel {

    @Inject
    ArrayList<HospitalDataPojo> arrayList;

    @Inject
    Utility utility;

    @Inject
    public FindHospitalModel() {
    }

    public void parseData(HospitalPojo response) {
        arrayList.clear();
        arrayList.addAll(response.getData());
    }

    public HospitalDataPojo getSelectedHospital(int position) {
        return arrayList.get(position);
    }
}
