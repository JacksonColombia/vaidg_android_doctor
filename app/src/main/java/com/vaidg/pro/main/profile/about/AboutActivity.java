package com.vaidg.pro.main.profile.about;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityAboutBinding;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>AboutActivity</h1>
 * AboutActivity for showing the company Details
 */

public class AboutActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityAboutBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAboutBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
    }

    /**
     * init the views
     */
    private void initViews() {
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        setSupportActionBar(binding.includeToolbar.toolbar);
        binding.includeToolbar.toolbar.setElevation(0f);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        binding.includeToolbar.tvTitle.setText(getString(R.string.app_name_short));
        binding.includeToolbar.tvTitle.setTypeface(fontBold);

        binding.tvGoolePlay.setTypeface(fontMedium);
        binding.tvFacebookLike.setTypeface(fontMedium);
        binding.tvLegal.setTypeface(fontMedium);
        binding.tvAboutMsg.setTypeface(fontMedium);
        binding.tvWebsite.setTypeface(fontMedium);
        binding.tvAppVersion.setTypeface(fontRegular);

        binding.tvGoolePlay.setOnClickListener(this);
        binding.tvFacebookLike.setOnClickListener(this);
        binding.tvLegal.setOnClickListener(this);
        binding.tvWebsite.setOnClickListener(this);

        binding.tvAppVersion.setText(new StringBuilder().append(getString(R.string.appVersion)).append(" ").append(VariableConstant.APP_VERSION));
        binding.tvWebsite.setText(VariableConstant.WEB_SITE_LINK_SHOWING);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        finishAfterTransition();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvGoolePlay:
                openGooglePlay();
                break;

            case R.id.tvFacebookLike:
                openFaceBook();
                break;

            case R.id.tvWebsite:
                openWebSite();
                break;

            case R.id.tvLegal:
                openWebView();
                break;

        }
    }

    /**
     * method for opening website of the app
     */
    private void openWebSite() {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(VariableConstant.WEB_SITE_LINK));
            startActivity(i);
        } catch (Exception e) {
            Toast.makeText(this, getString(R.string.unableToOpenBrowser), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


    }

    /**
     * method for opening current app in google play store
     */
    private void openGooglePlay() {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(VariableConstant.PLAY_STORE_LINK));
            startActivity(i);
        } catch (Exception e) {
            Toast.makeText(this, getString(R.string.unableToOpenPlayStore), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    /**
     * method for opening facebook page of the app
     */
    private void openFaceBook() {
        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        String facebookUrl = getFacebookPageURL();
        facebookIntent.setData(Uri.parse(VariableConstant.FACEBOOK_LINK));
        startActivity(facebookIntent);
    }

    /**
     * method for opening the Webview of the Legal
     */
    private void openWebView() {
        Intent intent;
        try {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setPackage("com.google.android.youtube");
            intent.setData(Uri.parse(VariableConstant.YOUTUBE_PAGE));
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(VariableConstant.YOUTUBE_PAGE));
            startActivity(intent);
        }
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }

    /**
     * mehtod for checking the facebook app installed or not , if installed then return valid page url accoring to the facebook app
     *
     * @return url of the Facebook page of the app
     */
    public String getFacebookPageURL() {
        PackageManager packageManager = getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + VariableConstant.FACEBOOK_LINK;
            } else { //older versions of fb app
                return "fb://page/" + VariableConstant.FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return VariableConstant.FACEBOOK_LINK; //normal web url
        }
    }
}
