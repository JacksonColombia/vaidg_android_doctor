package com.vaidg.pro.main.profile.bank.banknewaccount;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Context;
import android.util.Patterns;
import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.Objects;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 26-Aug-17.
 * <h1>BankNewAccountPresenter</h1>
 * BankNewAccountPresenter presenter for BankNewAccountActivity
 *
 * @see BankNewAccountActivity
 */

public class BankNewAccountPresenter implements BankNewAccountContract.Presenter {

    @Inject
    Context context;
    @Inject
    BankNewAccountContract.View view;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;

    @Inject
    BankNewAccountPresenter() {
    }


    /**
     * method for passing values from view to model
     *
     * @param jsonObject required field in json object
     */
    @Override
    public void addBankDetails(JSONObject jsonObject) {
        if (view != null)
            view.startProgressBar();
        try {
            if (view != null) {
                if (jsonObject.getString("account_holder_name").equals("")) {
                    view.onNameError();
                    return;
                } else if (!Patterns.EMAIL_ADDRESS.matcher(jsonObject.getString("email")).matches()) {
                    view.onErrorEmail();
                    return;
                } else if (jsonObject.getString("account_number").equals("")) {
                    view.onAccountNumberError();
                    return;
                } else if (jsonObject.getString("routing_number").equals("")) {
                    view.onRoutingNumberError();
                    return;
                } else if (jsonObject.getString("country").equals("")) {
                    view.onCountryError();
                    return;
                }
                view.onNoError();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        service.addExternalAccount(Utility.getAuth(sessionManager.getEmail()),DEFAULT_LANGUAGE, PLATFORM,jsonObject.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if (view != null)
                                    view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    if (view != null)
                                        view.onSuccess(Utility.getMessage(response));
                                } else {
                                    if (view != null)
                                        view.onFailure();
                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken)  {
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        if (view != null)
                                            view.stopProgressBar();
                                        addBankDetails(jsonObject);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        if (view != null) {
                                            view.stopProgressBar();
                                            view.onFailure();
                                        }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if (view != null)
                                            view.stopProgressBar();
                                            getInstance().toast(Utility.getMessage(msg));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                                if (view != null)
                                    view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                Utility.logoutSessionExiperd(sessionManager, context);
                                break;
                            default:
                                if (view != null) {
                                    view.stopProgressBar();
                                    view.onFailure(Utility.getMessage(errorBody));
                                }
                                getInstance().toast(Utility.getMessage(errorBody));
                                break;
                        }
                    } catch (Exception e) {
                        if (view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if (view != null) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                }

                @Override
                public void onComplete() {
                }
            });

        //  bankNewAccountModel.addBankAccount(token, jsonObject);
    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    /**
     * BankNewAccountPresenterImple interface for view implementation
     */
    interface BankNewAccountPresenterImple {
        void startProgressBar();

        void stopProgressBar();

        void onSuccess(String msg);

        void onFailure(String msg);

        void onFailure();

        void onNameError();

        void onErrorEmail();

        void onAccountNumberError();

        void onRoutingNumberError();

        void onCountryError();

        void onNoError();
    }
}
