package com.vaidg.pro.main.earning;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityEarningWebBinding;
import com.vaidg.pro.utility.ServiceUrl;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>EarningWebFragment</h1>
 * EarningWebFragment for showing Earning Details
 */

public class EarningWebActivity extends BaseDaggerActivity {

    @Inject
    EarningWebContract.Presenter presenter;

    private ActivityEarningWebBinding binding;

    public final String TAG = this.getClass().getSimpleName();
    private ProgressBar pgEarning;
    private WebView webViewEarning;

    private ArrayList<String> urls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEarningWebBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
    }

    /**
     * init  the views
     */
    @SuppressLint("SetJavaScriptEnabled")
    private void initViews() {

        setSupportActionBar(binding.includeToolbar.toolbar);
        binding.includeToolbar.toolbar.setElevation(0f);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        binding.includeToolbar.tvTitle.setText(getString(R.string.earnings));

        urls = new ArrayList<>();

        pgEarning = findViewById(R.id.pgEarning);

        webViewEarning = findViewById(R.id.webViewEarning);
        webViewEarning.setHorizontalScrollBarEnabled(false);
        webViewEarning.getSettings().setJavaScriptEnabled(true);
        webViewEarning.getSettings().setUseWideViewPort(false);
        webViewEarning.getSettings().setDomStorageEnabled(true);

        webViewEarning.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                Log.d(TAG, "shouldOverrideUrlLoading: " + url);
                if (!urls.contains(url)) {
                    urls.add(url);
                }
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pgEarning.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                pgEarning.setVisibility(View.GONE);
            }
        });

        Log.d(TAG, "init: " + ServiceUrl.EARNING_URL + presenter.getProviderId());

        webViewEarning.loadUrl(ServiceUrl.EARNING_URL + presenter.getProviderId());
        urls.add(ServiceUrl.EARNING_URL + presenter.getProviderId());
    }

    private void canGoBack() {
        if (urls.size() == 1) {
            closeActivity();
        } else {
            webViewEarning.loadUrl(urls.get(urls.size() - 2));
            urls.remove(urls.size() - 1);
        }
    }

    @Override
    public void onBackPressed() {
        canGoBack();
    }

    private void closeActivity() {
        finishAfterTransition();
    }

}
