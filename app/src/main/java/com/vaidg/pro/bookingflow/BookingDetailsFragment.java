package com.vaidg.pro.bookingflow;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.QuestionSymptomListAdapterGrid;
import com.vaidg.pro.bookingflow.bid.MyBidActivity;
import com.vaidg.pro.main.chats.ChattingActivity;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.ServiceFactory;
import com.vaidg.pro.pojo.JitsiMeet;
import com.vaidg.pro.pojo.appconfig.AppConfigData;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.pojo.booking.ServiceItem;
import com.vaidg.pro.pojo.booking.SymptomQuestionAnswer;
import com.vaidg.pro.pojo.history.Accounting;
import com.vaidg.pro.telecall.UtilityVideoCall;
import com.vaidg.pro.utility.CircleTransform;
import com.vaidg.pro.utility.DialogHelper;
import com.vaidg.pro.utility.RefreshToken;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.encryptiondecryption.CryptographyExample;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilPrivateKey;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;
import eu.janmuller.android.simplecropimage.Util;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import okhttp3.ResponseBody;
import org.jitsi.meet.sdk.JitsiMeetActivity;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.json.JSONException;
import org.json.JSONObject;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.*;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.PLATFORM;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.Utility.getDayNumberSuffix;
import static com.vaidg.pro.utility.VariableConstant.AES_MODE;
import static com.vaidg.pro.utility.VariableConstant.ANDROID_KEYSTORE;
import static com.vaidg.pro.utility.VariableConstant.FIXED_IV;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_DD;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_SPACE_COMMA_MMM_DD_YYYY;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_SPACE_DD_MMMM_YYYY_TIME_0_12_HRS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_TIME_1_12_HRS;
import static com.vaidg.pro.utility.VariableConstant.IV_PARAMETER_SPEC;
import static com.vaidg.pro.utility.VariableConstant.JOB_TIMER_COMPLETED;
import static com.vaidg.pro.utility.VariableConstant.JOB_TIMER_STARTED;
import static com.vaidg.pro.utility.VariableConstant.KEY_PRIVATEALIAS;
import static com.vaidg.pro.utility.VariableConstant.KEY_PUBLICALIAS;
import static com.vaidg.pro.utility.VariableConstant.RSA_MODE;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;
import static org.jitsi.meet.sdk.JitsiMeetActivity.*;
import static org.webrtc.ContextUtils.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link BookingDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookingDetailsFragment extends Fragment implements EasyPermissions.PermissionCallbacks, Serializable {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    public transient LinearLayout llAddress;
    private transient Booking booking;
    private boolean isAcceptReject, inCall, teleCall;
    private transient Handler handler;
    private transient Runnable runnable;
    private transient Context mContext;
    private long totalSecs, hours, minutes, seconds;
    private transient SessionManager sessionManager;
    private transient KeyStore keyStore;
    private final int CALL_REQUEST_CODE = 109;
    private String callType = "0";

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment BookingDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BookingDetailsFragment newInstance(Booking param1, boolean inCall, boolean teleCall, boolean isAcceptReject) {
        BookingDetailsFragment fragment = new BookingDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, param1);
        args.putBoolean(ARG_PARAM3, inCall);
        args.putBoolean(ARG_PARAM4, teleCall);
        args.putBoolean(ARG_PARAM2, isAcceptReject);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            booking = (Booking) getArguments().getSerializable(ARG_PARAM1);
            isAcceptReject = getArguments().getBoolean(ARG_PARAM2);
            inCall = getArguments().getBoolean(ARG_PARAM3);
            teleCall = getArguments().getBoolean(ARG_PARAM4);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        this.mContext = null;
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_booking_details, container, false);
        checkForKeystore();
        init(rootView);
        return rootView;
    }

    @SuppressLint("SetTextI18n")
    private void init(View rootView) {
        Typeface fontMedium = Utility.getFontMedium(mContext);
        Typeface fontRegular = Utility.getFontRegular(mContext);
        Typeface fontBold = Utility.getFontBold(mContext);
        sessionManager = SessionManager.getSessionManager(mContext);
        TextView tvAddress = rootView.findViewById(R.id.tvAddress);
        TextView tvDistance = rootView.findViewById(R.id.tvDistance);
        TextView tvCustomerName = rootView.findViewById(R.id.tvCustomerName);
        TextView tvBookForDep = rootView.findViewById(R.id.tvBookForDep);
        ImageView ivCustomerImage = rootView.findViewById(R.id.ivCustomerImage);
        TextView tvCategory = rootView.findViewById(R.id.tvCategory);
        TextView tvBookingId = rootView.findViewById(R.id.tvBookingId);
        TextView tvRemainingTime = rootView.findViewById(R.id.tvRemainingTime);
        TextView tvTotalBillAmountLabel = rootView.findViewById(R.id.tvTotalBillAmountLabel);
        TextView tvTotalBillAmount = rootView.findViewById(R.id.tvTotalBillAmount);
        TextView tvDate = rootView.findViewById(R.id.tvDate);
        TextView tvTime = rootView.findViewById(R.id.tvTime);
        TextView tvslotTime = rootView.findViewById(R.id.tvslotTime);
        TextView tvSelectedServiceLabel = rootView.findViewById(R.id.tvSelectedServiceLabel);
        TextView tvPaymentMethodLabel = rootView.findViewById(R.id.tvPaymentMethodLabel);
        TextView tvPaymentMethod = rootView.findViewById(R.id.tvPaymentMethod);
        TextView tvSymptomLabel = rootView.findViewById(R.id.tvSymptomLabel);
        TextView tvJobDescriptionLabel = rootView.findViewById(R.id.tvJobDescriptionLabel);
        TextView tvJobDescription = rootView.findViewById(R.id.tvJobDescription);
        TextView tvJobPhotosLabel = rootView.findViewById(R.id.tvJobPhotosLabel);
        TextView tvScheduleStartTime = rootView.findViewById(R.id.tvScheduleStartTime);
        TextView tvScheduleStartDate = rootView.findViewById(R.id.tvScheduleStartDate);
        TextView tvScheduleEndTime = rootView.findViewById(R.id.tvScheduleEndTime);
        TextView tvScheduleEndDate = rootView.findViewById(R.id.tvScheduleEndDate);
        TextView tvTo = rootView.findViewById(R.id.tvTo);
        TextView tvTotalNoOfShift = rootView.findViewById(R.id.tvTotalNoOfShift);
        TextView tvShift = rootView.findViewById(R.id.tvShift);
        TextView tvNoSymptom = rootView.findViewById(R.id.tvNoSymptom);
        LinearLayout lvButton = rootView.findViewById(R.id.lvButton);
        LinearLayout lvVoiceCall = rootView.findViewById(R.id.lvVoiceCall);
        LinearLayout lvChat = rootView.findViewById(R.id.lvChat);
        LinearLayout lvVideoCall = rootView.findViewById(R.id.lvVideoCall);
        llAddress = rootView.findViewById(R.id.llAddress);
        RecyclerView rvJobPhotos = rootView.findViewById(R.id.rvJobPhotos);
        rvJobPhotos.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        //rvJobPhotos.setAdapter(new JobImageListAdapter(getActivity(),null));
        tvCustomerName.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_18), mContext));
        tvBookForDep.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvCategory.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvTotalBillAmountLabel.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvTotalBillAmount.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_35), mContext));
        tvDate.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvTime.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvslotTime.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvSelectedServiceLabel.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvPaymentMethodLabel.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvJobDescriptionLabel.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvSymptomLabel.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16), mContext));
        tvPaymentMethod.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        tvJobDescription.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        tvNoSymptom.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        tvAddress.setTypeface(fontRegular);
        tvDistance.setTypeface(fontRegular);
        tvCustomerName.setTypeface(fontBold);
        tvBookForDep.setTypeface(fontMedium);
        tvCategory.setTypeface(fontRegular);
        tvBookingId.setTypeface(fontRegular);
        tvRemainingTime.setTypeface(fontRegular);
        tvTotalBillAmountLabel.setTypeface(fontMedium);
        tvSelectedServiceLabel.setTypeface(fontMedium);
        tvTotalBillAmount.setTypeface(fontMedium);
        tvDate.setTypeface(fontRegular);
        tvTime.setTypeface(fontRegular);
        tvslotTime.setTypeface(fontRegular);
        tvPaymentMethodLabel.setTypeface(fontMedium);
        tvJobDescriptionLabel.setTypeface(fontMedium);
        tvSymptomLabel.setTypeface(fontMedium);
        tvPaymentMethod.setTypeface(fontRegular);
        tvJobDescription.setTypeface(fontRegular);
        tvNoSymptom.setTypeface(fontRegular);
        tvJobPhotosLabel.setTypeface(fontMedium);
        tvScheduleStartTime.setTypeface(fontRegular);
        tvScheduleStartDate.setTypeface(fontMedium);
        tvScheduleEndTime.setTypeface(fontRegular);
        tvScheduleEndDate.setTypeface(fontMedium);
        tvTo.setTypeface(fontRegular);
        tvTotalNoOfShift.setTypeface(fontMedium);
        tvShift.setTypeface(fontMedium);
        lvChat.setOnClickListener(v -> {
            sessionManager.setChatBookingID(booking.getBookingId());
            sessionManager.setChatCustomerName(booking.getFirstName() + " " + booking.getLastName());
            sessionManager.setChatCustomerPic(booking.getProfilePic());
            sessionManager.setChatCustomerID(booking.getCustomerId());
            Intent chatIntent = new Intent(mContext, ChattingActivity.class);
            startActivity(chatIntent);
        });
        lvVideoCall.setOnClickListener(v -> {
            if (teleCall) {
              /*  if ((TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())) < ((Long.parseLong(booking.getEventStartTime())) - (Long.parseLong(booking.getCallButtonEnableForProvider())))) {
                    DialogHelper.customAlertDialogServiceDetails(mContext, "Alert...!", "Your tele call will be enabled on and after " +
                        Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(String.valueOf(Integer.parseInt(booking.getEventStartTime()) - (Long.parseLong(booking.getCallButtonEnableForProvider())))), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_DD_MMMM_YYYY_TIME_0_12_HRS));
                } else if ((TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())) > ((Long.parseLong(booking.getBookingEndtime())) *//*+ (Long.parseLong(booking.getCallButtonEnableForProvider()))*//*)) {
                    DialogHelper.customAlertDialogServiceDetails(mContext, "Alert...!", "Sorry your call slot has expired,You cannot call after " +
                        Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(booking.getBookingEndtime()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_DD_MMMM_YYYY_TIME_0_12_HRS));
                } else {
                    //progressDialog.setMessage(getString(R.string.calling));
                    //enableDisableCommunication(false);
                    callType = "1";
                    openCall(callType);
                }*/
                callType = "1";
                openCall(callType);
            }

        });


        lvVoiceCall.setOnClickListener(v -> {
            if (teleCall) {
              /*  if ((TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())) < ((Long.parseLong(booking.getEventStartTime())) - (Long.parseLong(booking.getCallButtonEnableForProvider())))) {
                    DialogHelper.customAlertDialogServiceDetails(mContext, "Alert...!", "Your tele call will be enabled on and after " +
                            Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(String.valueOf(Integer.parseInt(booking.getEventStartTime()) - (Long.parseLong(booking.getCallButtonEnableForProvider())))), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_DD_MMMM_YYYY_TIME_0_12_HRS));
                } else if ((TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())) > ((Long.parseLong(booking.getBookingEndtime())) *//*+ (Long.parseLong(booking.getCallButtonEnableForProvider()))*//*)) {
                    DialogHelper.customAlertDialogServiceDetails(mContext, "Alert...!", "Sorry your call slot has expired,You cannot call after " +
                            Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(booking.getBookingEndtime()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_DD_MMMM_YYYY_TIME_0_12_HRS));
                } else {
                    //progressDialog.setMessage(getString(R.string.calling));
                    //enableDisableCommunication(false);
                    callType = "0";
                    openCall(callType);
                }*/
                callType = "0";
                openCall(callType);
            } else {
                String uri = "tel:" + booking.getPhone();
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse(uri));
                startActivity(callIntent);
            }
        });

        if (booking.getCallType().equals("3")) {
            lvButton.setVisibility(View.VISIBLE);
            lvVideoCall.setVisibility(View.VISIBLE);
        } else if (booking.getCallType().equals("1")) {
            lvButton.setVisibility(View.VISIBLE);
        }
        if (booking.getDependentData() != null && booking.getDependentData().getDependentId() != null && !booking.getDependentData().getDependentId().isEmpty() &&!booking.getDependentData().getDependentId().equals("1")) {
            tvBookForDep.setVisibility(View.VISIBLE);
            tvBookForDep.setText(mContext.getResources().getString(R.string.bookForDependent) + " " + booking.getFirstName() + " " + booking.getLastName());
            tvCustomerName.setText(booking.getDependentData().getFirstName() + " " + booking.getDependentData().getLastName());
            tvCategory.setText(Utility.getAge(booking.getDependentData().getDateOfBirth())+ " " + mContext.getResources().getString(R.string.years) + " " + mContext.getResources().getString(R.string.comma) + " " + Utility.gender(String.valueOf(booking.getDependentData().getGender()))  + " " + mContext.getResources().getString(R.string.comma) + " " + booking.getDependentData().getRelationship());
            if (booking.getDependentData().getProfilePic() != null && !booking.getDependentData().getProfilePic().isEmpty()) {
                Glide.with(mContext).setDefaultRequestOptions(new RequestOptions().error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(mContext))
                    .placeholder(R.drawable.profile_default_image))
                    .load(booking.getDependentData().getProfilePic())
                    .into(ivCustomerImage);
            }
        } else {
            tvCustomerName.setText(booking.getFirstName() + " " + sessionManager.getLastName());
            tvCategory.setText(booking.getAgeString() + " " + mContext.getResources().getString(R.string.comma) + " " + Utility.gender(booking.getGender()));
            if (booking.getProfilePic() != null && !booking.getProfilePic().isEmpty()) {
                Glide.with(mContext).setDefaultRequestOptions(new RequestOptions().error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(mContext))
                    .placeholder(R.drawable.profile_default_image))
                    .load(booking.getProfilePic())
                    .into(ivCustomerImage);
            }
        }
        Accounting accounting = booking.getAccounting();
        ImageView ivbidedit = rootView.findViewById(R.id.ivbidedit);
        ivbidedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //go to mybid activity to edit your bid
                Intent bidIntent = new Intent(getActivity(), MyBidActivity.class);
                Bundle bidBundle = new Bundle();
                bidBundle.putSerializable("booking", booking);
                bidBundle.putBoolean("isUpdateBidding", true);
                bidIntent.putExtras(bidBundle);
                startActivity(bidIntent);
//                startActivityForResult(bidIntent, REQUEST_CODE_FOR_BID);
                getActivity().overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                getActivity().finish();
            }
        });
        tvBookingId.setText(getString(R.string.bookingID) + booking.getBookingId());
            tvBookingId.setVisibility(View.GONE);
        if (!booking.getCallType().equals("3")) {
            llAddress.setVisibility(View.VISIBLE);
            tvAddress.setText(booking.getAddLine1() + " " + booking.getAddLine2());
            tvDistance.setText("" + Utility.getFormattedPrice(String.valueOf((Double.parseDouble(booking.getDistance()) * sessionManager.getDistanceUnitConverter()))) + " " +
                    sessionManager.getDistanceUnit() + "  " + getString(R.string.away));
        } else {
            llAddress.setVisibility(View.GONE);
        }
        tvTotalBillAmount.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        if (booking.getBookingModel().equalsIgnoreCase("3")) {
            ivbidedit.setVisibility(View.VISIBLE);
        } else {
            ivbidedit.setVisibility(View.GONE);
        }
        if (accounting.getPaymentMethod().equals("1")) {
            tvPaymentMethod.setText(getString(R.string.cash));
            tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.vector_cash, 0, 0, 0);
        } else {
           // tvPaymentMethod.setText(getString(R.string.cardFirst16Digit) + "  " + accounting.getLast4());
            tvPaymentMethod.setText(getString(R.string.card));
           // tvPaymentMethod.setText(accounting.getRazorPayPaymentType());
        }
        if (accounting.getPaidByWallet() != null && accounting.getPaidByWallet().equals("1")) {
           // tvPaymentMethod.setText(tvPaymentMethod.getText().toString() + " + " + getString(R.string.wallet));
            tvPaymentMethod.setText(getString(R.string.wallet));
        }
        if (booking.getJobDescription() != null && !booking.getJobDescription().equals("")) {
            rootView.findViewById(R.id.llJobDescription).setVisibility(View.VISIBLE);
            tvJobDescription.setText(booking.getJobDescription());
        } else {
            rootView.findViewById(R.id.llJobDescription).setVisibility(View.GONE);
        }
        try {
            int day = Integer.parseInt(Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(booking.getEventStartTime()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_DD));
//            int day = Integer.parseInt(getDateOnly.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
            String dayNumberSuffix = getDayNumberSuffix(day);
            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
            serverFormat.setTimeZone(getInstance().getTimeZone());
            tvDate.setText(getString(R.string.date) + " : " + Utility.getFormattedDateOnly(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
            tvTime.setText(getString(R.string.time) + " : " + Utility.getFormattedTime(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
            // tvDate.setText(/*getString(R.string.date) + " : " +*/ day + dayNumberSuffix + displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
            /* tvTime.setText(getString(R.string.time) + " : "*/
            //" | "+ displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));*/
/*            if (booking.getBookingEndtime().trim().isEmpty() || booking.getEventStartTime().trim().isEmpty()) {
                tvslotTime.setText(Utility.converSecondToHourMinute(booking.getScheduleTime()));
            } else {
                tvslotTime.setText(Utility.converSecondToHourMinute(String.valueOf((Integer.valueOf(booking.getBookingEndtime()) - Integer.valueOf(booking.getEventStartTime())) / 60)));
            }*/
            if (getActivity().getIntent().getBooleanExtra("isFromChat", false)) {
                tvCustomerName.setText(booking.getCustomerData().getFirstName() + " " + booking.getCustomerData().getLastName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        TextView tvDiscountLabel = rootView.findViewById(R.id.tvDiscountLabel);
        TextView tvDiscount = rootView.findViewById(R.id.tvDiscount);
        TextView tvTotalLabel = rootView.findViewById(R.id.tvTotalLabel);
        TextView tvTotal = rootView.findViewById(R.id.tvTotal);
        TextView tvVisitFeeLabel = rootView.findViewById(R.id.tvVisitFeeLabel);
        TextView tvVisitFee = rootView.findViewById(R.id.tvVisitFee);
        TextView tvLastDueLabel = rootView.findViewById(R.id.tvLastDueLabel);
        TextView tvLastDue = rootView.findViewById(R.id.tvLastDue);
        tvDiscountLabel.setTypeface(fontRegular);
        tvDiscount.setTypeface(fontRegular);
        tvDiscountLabel.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        tvDiscount.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        tvTotalLabel.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        tvTotal.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
        tvTotalLabel.setTypeface(fontBold);
        tvTotal.setTypeface(fontBold);
        tvVisitFeeLabel.setTypeface(fontRegular);
        tvVisitFee.setTypeface(fontRegular);
        tvLastDueLabel.setTypeface(fontRegular);
        tvLastDue.setTypeface(fontRegular);
        if (accounting.getVisitFee() != null && !accounting.getVisitFee().equals("0") && !accounting.getVisitFee().equals("0.00")) {
            rootView.findViewById(R.id.rlVisitFee).setVisibility(View.VISIBLE);
            tvVisitFee.setText(Utility.getPrice(accounting.getVisitFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if (accounting.getLastDues() != null && !accounting.getLastDues().equals("0") && !accounting.getLastDues().equals("0.00")) {
            rootView.findViewById(R.id.rlLastDue).setVisibility(View.VISIBLE);
            tvLastDue.setText(Utility.getPrice(accounting.getLastDues(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        tvDiscount.setText(Utility.getPrice(accounting.getDiscount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvTotal.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        if (!booking.getBookingModel().equals("3")) {
            rootView.findViewById(R.id.llServiceParent).setVisibility(View.VISIBLE);
            LinearLayout llService = rootView.findViewById(R.id.llService);
            if (booking.getServiceType().equals("1")) {
                for (ServiceItem serviceItem : booking.getCartData()) {
                    View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
                    TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                    TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                    tvServiceFeeLabel.setTypeface(fontRegular);
                    tvServiceFee.setTypeface(fontRegular);
                    tvServiceFeeLabel.setText(serviceItem.getServiceName() + " X " + serviceItem.getQuntity());
                    tvServiceFee.setText(Utility.getPrice(serviceItem.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                    llService.addView(serviceView);
                }
            } /*else if (!booking.getCallType().equals("1") && !booking.getCallType().equals("3")) {
        View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
        TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
        TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
        tvServiceFeeLabel.setTypeface(fontRegular);
        tvServiceFee.setTypeface(fontRegular);
        tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
        tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        llService.addView(serviceView);
      }*/ else {
                /*
                 * Bug id : Trello 188
                 * Bug Description : Incall booking | For consultaning fee | it's showing service as 1 hr on going booking flow screen
                 * Fixed Description : Checked the call type if Call type is InCall then showing Consulting Fee
                 * Dev : Murashid
                 * Date : 29-11-2018
                 * */
                /*
                 *
                 * Bug Description : TeleCall booking | For consultaning fee | it's showing service as 1 hr on going booking flow screen
                 * Fixed Description : Checked the call type if Call type is InCall then showing Consulting Fee
                 * Dev : Yogesh
                 * Date : 15-05-2019
                 * */
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
                tvServiceFee.setTextSize(Utility.pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));
                tvServiceFeeLabel.setText(getString(R.string.consultingfee));
                tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            }
        }
        if (booking.getQuestionAndAnswer() != null && !booking.getQuestionAndAnswer().isEmpty()) {
            String messageFromDecrypt = "";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decryptPrivateKeyPostLollipop();
                decryptPublicKeyPostLollipop();
                messageFromDecrypt = encryptData(decryptPrivateKeyPostLollipop(), decryptPublicKeyPostLollipop(), messageFromDecrypt, booking.getQuestionAndAnswer());
            } else {
                decryptPrivateKeyPreMarshMallow();
                decryptPublicKeyPreMarshMallow();
                messageFromDecrypt = encryptData(decryptPrivateKeyPreMarshMallow(), decryptPublicKeyPreMarshMallow(), messageFromDecrypt, booking.getQuestionAndAnswer());
            }
            if (messageFromDecrypt != null && !messageFromDecrypt.isEmpty()) {
                Type collectionType = new TypeToken<ArrayList<SymptomQuestionAnswer>>() {
                }.getType();
                ArrayList<SymptomQuestionAnswer> questionAndAnswer = new Gson().fromJson(messageFromDecrypt, collectionType);
                RecyclerView rvQuestions = rootView.findViewById(R.id.rvQuestions);
                rvQuestions.setVisibility(View.VISIBLE);
                rvQuestions.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                rvQuestions.setAdapter(new QuestionSymptomListAdapterGrid(getActivity(), questionAndAnswer));
            } else {
                tvNoSymptom.setVisibility(View.VISIBLE);
            }
        }
/*
        if (booking.getQuestionAndAnswer() != null && booking.getQuestionAndAnswer().size() > 0) {
            LinearLayout llQuestionAnswer = rootView.findViewById(R.id.llQuestionAnswer);
            llQuestionAnswer.setVisibility(View.VISIBLE);

            for (QuestionAnswer questionAnswer : booking.getQuestionAndAnswer()) {
                View serviceView = inflater.inflate(R.layout.single_row_question_answer, null);
                TextView tvQuestion = serviceView.findViewById(R.id.tvQuestion);
                TextView tvAnswer = serviceView.findViewById(R.id.tvAnswer);
                tvQuestion.setTypeface(fontMedium);
                tvAnswer.setTypeface(fontRegular);
                tvQuestion.setText("Q. " + questionAnswer.getName());

                if (!questionAnswer.getQuestionType().equals("10")) {
                    tvAnswer.setText(questionAnswer.getAnswer());
                } else {
                    tvAnswer.setVisibility(View.GONE);
                    RecyclerView rvJobPhotosQuestion = serviceView.findViewById(R.id.rvJobPhotosQuestion);
                    rvJobPhotosQuestion.setVisibility(View.VISIBLE);
                    rvJobPhotosQuestion.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                    ArrayList<String> imageUrls = new ArrayList<>();
                    imageUrls.addAll(Arrays.asList(questionAnswer.getAnswer().split(",")));
                    rvJobPhotosQuestion.setAdapter(new JobImageListAdapter(getActivity(), imageUrls));
                }
                llQuestionAnswer.addView(serviceView);
            }
        }
*/
        if (isAcceptReject) {
            setTimer(tvRemainingTime, booking.getBookingRequestedForProvider(), booking.getBookingExpireForProvider(), booking.getCurrentTime());
            if (booking.getBookingType().equals("3")) {
                rootView.findViewById(R.id.llSchedule).setVisibility(View.VISIBLE);
                LinearLayout llSheduleDays = rootView.findViewById(R.id.llSheduleDays);
                tvDate.setVisibility(View.GONE);
                //tvTime.setVisibility(View.GONE);
                tvTotalBillAmountLabel.setVisibility(View.GONE);
                tvTotalBillAmount.setVisibility(View.GONE);
                TextView tvHoursPerShiftLabel = rootView.findViewById(R.id.tvHoursPerShiftLabel);
                TextView tvHoursPerShift = rootView.findViewById(R.id.tvHoursPerShift);
                TextView tvCostPerShiftLabel = rootView.findViewById(R.id.tvCostPerShiftLabel);
                TextView tvCostPerShift = rootView.findViewById(R.id.tvCostPerShift);
                TextView tvTotalShiftLabel = rootView.findViewById(R.id.tvTotalShiftLabel);
                TextView tvTotalShift = rootView.findViewById(R.id.tvTotalShift);
                tvHoursPerShiftLabel.setTypeface(fontRegular);
                tvHoursPerShift.setTypeface(fontMedium);
                tvCostPerShiftLabel.setTypeface(fontRegular);
                tvCostPerShift.setTypeface(fontMedium);
                tvTotalShiftLabel.setTypeface(fontRegular);
                tvTotalShift.setTypeface(fontMedium);
                try {
                    tvScheduleStartTime.setText(Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(booking.getEventStartTime()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_TIME_1_12_HRS));
                    tvScheduleStartDate.setText(Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(booking.getEventStartTime()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_COMMA_MMM_DD_YYYY));
                    tvScheduleEndTime.setText(Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(booking.getBookingEndtime()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_TIME_1_12_HRS));
                    tvScheduleEndDate.setText(Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(booking.getBookingEndtime()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_COMMA_MMM_DD_YYYY));
                    tvTotalNoOfShift.setText(booking.getAccounting().getTotalShiftBooking());
                    for (String days : booking.getDaysArr()) {
                        TextView tvDay = (TextView) inflater.inflate(R.layout.single_row_text_schedule_day, null);
                        tvDay.setText(days.substring(0, 1));
                        llSheduleDays.addView(tvDay);
                    }
                    double perShiftAmount, totalShiftAmount, perShiftHour;
                    int totalNoOfShift;
                    if (!booking.getBookingModel().equals("3")) {
                        perShiftAmount = Double.parseDouble(booking.getAccounting().getTotal());
                        perShiftHour = (Double.parseDouble(booking.getAccounting().getTotalActualJobTimeMinutes()) / 60);
                    } else {
                        perShiftAmount = Double.parseDouble(booking.getAccounting().getBidPrice());
                        perShiftHour = (Double.parseDouble(booking.getScheduleTime()) / 60);
                    }
                    totalNoOfShift = (int) Double.parseDouble(booking.getAccounting().getTotalShiftBooking());
                    totalShiftAmount = totalNoOfShift * perShiftAmount;
                    tvTotalShift.setText(Utility.getPrice(String.valueOf(totalShiftAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                    tvHoursPerShift.setText(perShiftHour + " " + getString(R.string.hrsCaps));
                    tvCostPerShift.setText(Utility.getPrice(String.valueOf(perShiftAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            tvRemainingTime.setVisibility(View.GONE);
        }
       /* if (booking.getStatus().equals(JOB_TIMER_STARTED)) {
            llAddress.setVisibility(View.GONE);
        }*/
        if (booking.getStatus().equals(JOB_TIMER_COMPLETED)) {
            llAddress.setVisibility(View.GONE);
//            rootView.findViewById(R.id.payment_ll).setVisibility(View.GONE);
            rootView.findViewById(R.id.llJobDescription).setVisibility(View.GONE);
        }
    }


    private void openCall(String callType) {
        sessionManager.setChatBookingID(booking.getBookingId());
        sessionManager.setChatCustomerName(booking.getFirstName() + " " + booking.getLastName());
        sessionManager.setChatCustomerPic(booking.getProfilePic());
        sessionManager.setChatCustomerID(booking.getCustomerId());

        getCaalingid(Long.parseLong(booking.getBookingId()));


        /*if (isPermissionGranted()) {
            if (callType.equals("0")) {
                if (!UtilityVideoCall.getInstance().isActiveOnACall()) {
                    UtilityVideoCall.getInstance().setActiveOnACall(true, true);
                    Intent intent = new Intent(mContext, CallingActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(USER_NAME, booking.getFirstName() + " " + booking.getLastName());
                    intent.putExtra(USER_IMAGE, booking.getProfilePic());
                    intent.putExtra(USER_ID,*//*"5e97f858e5d129adf16afd7a"*//*booking.getCustomerId());
                    intent.putExtra(CALL_STATUS, CallStatus.CALLING);
                    intent.putExtra(CALL_TYPE, AUDIO);
                    intent.putExtra(CALL_ID, "");
                    intent.putExtra(ROOM_ID, "0");
                    intent.putExtra(BOOKING_ID, booking.getBookingId());
                    intent.putExtra(AUTH, Utility.getAuth(sessionManager.getEmail()));
                    intent.putExtra(LAN, DEFAULT_LANGUAGE);
                    intent.putExtra(USERID, sessionManager.getProviderId());
                    startActivity(intent);
                    startActivity(intent);
                }
            } else {
                if (!UtilityVideoCall.getInstance().isActiveOnACall()) {
                    UtilityVideoCall.getInstance().setActiveOnACall(true, true);
                    Intent intent = new Intent(mContext, CallingActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(USER_NAME, booking.getFirstName() + " " + booking.getLastName());
                    intent.putExtra(USER_IMAGE, booking.getProfilePic());
                    intent.putExtra(USER_ID,*//*"5e97f858e5d129adf16afd7a"*//*booking.getCustomerId());
                    intent.putExtra(CALL_STATUS, CallStatus.CALLING);
                    intent.putExtra(CALL_TYPE, VIDEO);
                    intent.putExtra(CALL_ID, "");
                    intent.putExtra(ROOM_ID, "0");
                    intent.putExtra(BOOKING_ID, booking.getBookingId());
                    intent.putExtra(AUTH, Utility.getAuth(sessionManager.getEmail()));
                    intent.putExtra(LAN, DEFAULT_LANGUAGE);
                    intent.putExtra(USERID, sessionManager.getProviderId());
                    startActivity(intent);
                }
            }
        }*/
    }
    private transient OnEventCallBack  eventCallBack = new OnEventCallBack() {
        @Override
        public void onChatClick(Map<String, Object> map) {
            if(map != null && map.containsValue("true"))
            {
                // Log.w(TAG, "onConferenceJoined: 1");
                Intent intent = new Intent(JitsiMeetActivity.mContext,ChattingActivity.class);
                intent.putExtra("isFromCall",true);
                //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                //}
                JitsiMeetActivity.mContext.startActivity(intent);
            }
        }

        @Override
        public void onConferenceJoined(Map<String, Object> map) {
        }

        @Override
        public void onConferenceWillJoin(Map<String, Object> map) {
        }

        @Override
        public void onConferenceTerminated(Map<String, Object> map) {
        }
    };

    private void getCaalingid(long bid) {
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("bookingId", String.valueOf(bid));

            NetworkService service = ServiceFactory.getClient(NetworkService.class,mContext);
            Observable<Response<ResponseBody>> obsResponseBody = service.calling(Utility.getAuth(sessionManager.getEmail()),
                DEFAULT_LANGUAGE, PLATFORM, jsonParams.toString());
            obsResponseBody.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        JSONObject jsonObject;
                        try {
                            String response =
                                responseBodyResponse.body() != null ? responseBodyResponse.body().string()
                                    : null;
                            String errorBody =
                                responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string()
                                    : null;
                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    JitsiMeet jitsiMeet = new Gson().fromJson(response,JitsiMeet.class);
                                    JitsiMeet.Data data = jitsiMeet.getData();
                                    // Initialize default options for Jitsi Meet conferences.
                                    JitsiMeetConferenceOptions options
                                        = new JitsiMeetConferenceOptions.Builder()
                                        .setToken(data.getToken())
                                        .setServerURL(new URL("https://doctor.vaidg.com"))
                                        .setRoom(String.valueOf(bid))
                                        .setWelcomePageEnabled(false)
                                        .build();

                                    // Launch the new activity with the given options. The launch() method takes care
                                    // of creating the required Intent and passing the options.
                                   // JitsiMeetActivity.launch(getContext(), options,eventCallBack);
                                    JitsiMeetActivity.mContext = mContext;
                                    Intent intent = new Intent(mContext, JitsiMeetActivity.class);
                                    intent.setAction("org.jitsi.meet.CONFERENCE");
                                    intent.putExtra("JitsiMeetConferenceOptions", options);
                                    intent.putExtra("INTERFACE", eventCallBack);
                                    if (Build.VERSION.SDK_INT >= 28) {
                                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                    }

                                    startActivity(intent);
                                    break;
                                case SESSION_EXPIRED:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                getCaalingid(bid);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {

                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, getContext());
                                            }
                                        });

                                    }
                                    break;
                                default:
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private boolean isPermissionGranted() {

        if (EasyPermissions.hasPermissions(mContext, VariableConstant.CALL_PERMISSIONS)) {
            return true;
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
                    CALL_REQUEST_CODE, VariableConstant.CALL_PERMISSIONS);
            return false;
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
    if (EasyPermissions.hasPermissions(mContext, VariableConstant.CALL_PERMISSIONS)) {
        if (teleCall) {
           /* if ((TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())) < ((Long.parseLong(booking.getEventStartTime())) - (Long.parseLong(booking.getCallButtonEnableForProvider())))) {
                DialogHelper.customAlertDialogServiceDetails(mContext, "Alert...!", "Your tele call will be enabled on and after " +
                    Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(String.valueOf(Integer.parseInt(booking.getEventStartTime()) - (Long.parseLong(booking.getCallButtonEnableForProvider())))), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_DD_MMMM_YYYY_TIME_0_12_HRS));
            } else if ((TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())) > ((Long.parseLong(booking.getBookingEndtime())) *//*+ (Long.parseLong(booking.getCallButtonEnableForProvider()))*//*)) {
                DialogHelper.customAlertDialogServiceDetails(mContext, "Alert...!", "Sorry your call slot has expired,You cannot call after " +
                    Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(booking.getBookingEndtime()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_DD_MMMM_YYYY_TIME_0_12_HRS));
            } else {
                //progressDialog.setMessage(getString(R.string.calling));
                //enableDisableCommunication(false);
                callType = "1";
                openCall(callType);
            }*/
            callType = "1";
            openCall(callType);
        }

    }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    private void setTimer(final TextView tvRemainingTime, String bookingStartTime, String bookingEndTime, long current) {
        long start = Utility.convertUTCToTimeStamp(bookingStartTime);
        long end = Utility.convertUTCToTimeStamp(bookingEndTime);
        totalSecs = (end - start) / 1000;
        //totalSecs[0] for Maximum Progress
        totalSecs = (end - current) / 1000;
        hours = TimeUnit.SECONDS.toHours(totalSecs);
        minutes = TimeUnit.SECONDS.toMinutes(totalSecs) % 60;
        seconds = totalSecs % 60;
        tvRemainingTime.setText(String.format("%02d", hours) + " H : " + String.format("%02d", minutes) + " M : " + String.format("%02d", seconds) + " S");
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                totalSecs = totalSecs - 1;
                hours = TimeUnit.SECONDS.toHours(totalSecs);
                minutes = TimeUnit.SECONDS.toMinutes(totalSecs) % 60;
                seconds = totalSecs % 60;
                tvRemainingTime.setText(String.format("%02d", hours) + " H : " + String.format("%02d", minutes) + " M : " + String.format("%02d", seconds) + " S");
                handler.postDelayed(this, 1000);
            }
        };
        handler.post(runnable);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
    }


    public String decryptPrivateKeyPostLollipop() {
        String str = "";
        Log.i("TEST", "decryptData marsh melloa ");
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE);
            c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
            byte[] encryptedBytes = Base64.decode(sessionManager.getVirgilPrivateKey(), Base64.DEFAULT);

            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str;
    }

    public String decryptPublicKeyPostLollipop() {
        String str = "";
        Log.i("TEST", "decryptData marsh melloa ");
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE);
            c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
            byte[] encryptedBytes = Base64.decode(sessionManager.getVirgilPublicKey(), Base64.DEFAULT);

            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * <h2>getSecretPrivateKeyPreMarshMallow</h2>
     * This method is used to get the secret key for pre marsh mallow device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPrivateKeyPreMarshMallow() throws Exception {
        String enryptedKeyB64 = sessionManager.getEncryptionPrivateKey();
        Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
        // need to check null, omitted here
        byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaPrivateKeyDecrypt(encryptedKey);
        Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
        return new SecretKeySpec(key, "AES");
    }

    /**
     * <h2>getSecretPublicKeyPreMarshMallow</h2>
     * This method is used to get the secret key for pre marsh mallow device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPublicKeyPreMarshMallow() throws Exception {
        String enryptedKeyB64 = sessionManager.getEncryptionPublicKey();
        Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
        // need to check null, omitted here
        byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaPublicKeyDecrypt(encryptedKey);
        Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
        return new SecretKeySpec(key, "AES");
    }


    private byte[] rsaPrivateKeyDecrypt(byte[] encrypted) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
        CipherInputStream cipherInputStream = new CipherInputStream(
                new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }
        return bytes;
    }

    private byte[] rsaPublicKeyDecrypt(byte[] encrypted) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
        CipherInputStream cipherInputStream = new CipherInputStream(
                new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }
        return bytes;
    }


    public String decryptPrivateKeyPreMarshMallow() {
        Log.i("TEST", "decryptData else  ");
        Cipher c;
        String str = "";
        try {
            c = Cipher.getInstance(AES_MODE, "BC");
            c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPreMarshMallow(), IV_PARAMETER_SPEC);
            byte[] encryptedBytes = Base64.decode(sessionManager.getEncryptionPrivateKey(), Base64.DEFAULT);
            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);


        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }


    public String decryptPublicKeyPreMarshMallow() {
        Log.i("TEST", "decryptData else  ");
        Cipher c;
        String str = "";
        try {
            c = Cipher.getInstance(AES_MODE, "BC");
            c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPreMarshMallow(), IV_PARAMETER_SPEC);
            byte[] encryptedBytes = Base64.decode(sessionManager.getEncryptionPublicKey(), Base64.DEFAULT);
            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }


    /**
     * <h2>getSecretPrivateKeyPostLollipop</h2>
     * This method is used to get the secret key for post lollipop device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPrivateKeyPostLollipop() throws Exception {
        Key s = keyStore.getKey(KEY_PRIVATEALIAS, null);
        Log.i("TEST", "key " + s.toString());
        return keyStore.getKey(KEY_PRIVATEALIAS, null);
    }

    /**
     * <h2>getSecretPublicKeyPostLollipop</h2>
     * This method is used to get the secret key for post lollipop device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPublicKeyPostLollipop() throws Exception {
        Key s = keyStore.getKey(KEY_PUBLICALIAS, null);
        Log.i("TEST", "key " + s.toString());
        return keyStore.getKey(KEY_PUBLICALIAS, null);
    }


    public void checkForKeystore() {
        try {
            keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
            keyStore.load(null);
        } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
        }
        try {
            if (!keyStore.containsAlias(KEY_PRIVATEALIAS)) {
                //mSignUpView.checkForVersion();
            }
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }

    private String encryptData(String privateKey, String publicKey, String messageFromDecrypt, String messageToDecrypt) {

        try {
            CryptographyExample cryptographyExample = new CryptographyExample();
            VirgilCrypto crypto = new VirgilCrypto();
            // Import private key
            VirgilPrivateKey importedPrivateKey = cryptographyExample.importPrivateKey(privateKey, crypto);
            // Decrypt data
            //  byte[] encryptedData = Base64.decode(messageToDecrypt, Base64.DEFAULT);/*Base64.getMimeDecoder().decode(chatData.get(position).getContent());*/
            messageFromDecrypt = cryptographyExample.dataDecryption(messageToDecrypt, importedPrivateKey);
        } catch (CryptoException e) {
            e.printStackTrace();
        }
        return messageFromDecrypt;
    }


}
