package com.vaidg.pro.main.booking;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.vaidg.pro.R;
import com.vaidg.pro.adapters.BidBookingListAdapter;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;


public class BookingListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "MyEventRequest";

    private static final String ARG_PARAM1 = "param1";
    private String type;
    private BidBookingListAdapter bookingListAdapter;
    private ArrayList<Booking> bookings;
    private TextView tvNoBooking;

    private SwipeRefreshLayout srlBooking;
    private RecyclerView rvBooking;
    private BookingListFragmentInteraction bookingListFragmentInteraction;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param type Parameter 1.
     * @return A new instance of fragment BookingListFragment.
     */
    public static BookingListFragment newInstance(String type) {

        BookingListFragment fragment = new BookingListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_booking_list, container, false);
        init(rootView);
        return rootView;
    }

    /**
     * init  the views
     *
     * @param rootView parent View
     */
    private void init(View rootView) {
        tvNoBooking = rootView.findViewById(R.id.tvNoBooking);
        tvNoBooking.setTypeface(Utility.getFontMedium(getActivity()));
        bookings = new ArrayList<>();
        bookingListAdapter = new BidBookingListAdapter(getActivity(),bookings, type);
        rvBooking = rootView.findViewById(R.id.rvBooking);
        rvBooking.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvBooking.setAdapter(bookingListAdapter);
        bookingListAdapter.notifyDataSetChanged();

        srlBooking = rootView.findViewById(R.id.srlBooking);
        srlBooking.setOnRefreshListener(this);
    }

    void notifyiDataChanged(ArrayList<Booking> bookings) {
        if (bookings != null) {
            tvNoBooking.setVisibility(View.GONE);
            srlBooking.setRefreshing(false);
            bookingListAdapter.clearTimer();
            this.bookings.clear();
            this.bookings.addAll(bookings);
            bookingListAdapter.notifyDataSetChanged();
            if (bookings.size() == 0) {
                tvNoBooking.setVisibility(View.VISIBLE);
            }
        } else {
            tvNoBooking.setVisibility(View.VISIBLE);
        }
    }

    void stopRefreshing() {
        try {
            srlBooking.setRefreshing(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setBookingListFragmentInteraction(BookingListFragmentInteraction bookingListFragmentInteraction) {
        this.bookingListFragmentInteraction = bookingListFragmentInteraction;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onRefresh() {
        if (bookingListFragmentInteraction != null)
            bookingListFragmentInteraction.onRefresh();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (bookingListAdapter != null) {
            bookingListAdapter.clearHandlers();
            bookingListAdapter.clearTimer();
        }
    }

    interface BookingListFragmentInteraction {
        void onRefresh();
    }
}
