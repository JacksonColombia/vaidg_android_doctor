package com.vaidg.pro.landing.newSignup.selectCity;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>SelectCityDaggerModule</h1>
 * <p>This class is collection of object that bind with dagger.</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 12/06/2020
 **/
@Module
public abstract class SelectCityDaggerModule {

    @ActivityScoped
    @Binds
    abstract SelectCityContract.Presenter providePresenter(SelectCityPresenter selectCityPresenter);

    @ActivityScoped
    @Binds
    abstract SelectCityContract.View provideView(SelectCityActivity selectCityActivity);

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(SelectCityActivity activity);
}
