package com.vaidg.pro.bookingflow.addmedication;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Context;
import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.addMedication.AddMedicationPojo;
import com.vaidg.pro.pojo.medication.MedicationResponse;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import retrofit2.Response;

public class AddMedicationPresenterImple implements AddMedicationContract.Presenter {
    @Inject
    AddMedicationContract.View view;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;
    @Inject
    Context context;
    @Inject
    NetworkService service;
    @Inject
    NetworkStateHolder networkStateHolder;

    @Inject
    public AddMedicationPresenterImple() {
    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    @Override
    public void getMedication() {
        //view.startProgressBar();
        service.getPrescription(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    // view.stopProgressBar();
                                    if (response != null && !response.isEmpty()) {
                                        AddMedicationPojo medicationResponse = gson.fromJson(response, AddMedicationPojo.class);
                                        view.onSuccessResonse(medicationResponse.getData());
                                    } else
                                        view.onFailure();
                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            getMedication();
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, context);
                                        }
                                    });
                                    //view.stopProgressBar();
                                    break;
                                case SESSION_LOGOUT:
                                    //view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, context);
                                    break;
                                default:
                                    //view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            //   view.stopProgressBar();
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        //view.stopProgressBar();
                        view.onFailure();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void getEditMedication(String bookidId, String medicationId) {
        if(view != null)
        view.startProgressBar();
        service.getMedication(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, bookidId, medicationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if(view != null)
                                    view.stopProgressBar();
                                    if (response != null && !response.isEmpty()) {
                                        MedicationResponse medicationResponse = gson.fromJson(response, MedicationResponse.class);
                                        if(view != null)
                                        view.onSuccessEditResonse(medicationResponse.getData());
                                    } else {
                                        if (view != null)
                                            view.onFailure();
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            if(view != null)
                                                view.hideProgress();
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            getEditMedication(bookidId, medicationId);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            if(view != null){
                                                view.hideProgress();
                                                view.onFailure();
                                            }
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null)
                                                view.hideProgress();
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, context);
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    if(view != null)
                                    view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, context);
                                    break;
                                default:
                                    if(view != null)
                                    view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            if(view != null) {
                                view.stopProgressBar();
                                view.onFailure();
                            }
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void addMedication(JSONObject body) {
        if(view != null)
        view.startProgressBar();
        service.addMedication(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, body.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if(view != null)
                                    view.stopProgressBar();
                                    if (response != null && !response.isEmpty()) {
                                        if(view != null)
                                        view.onSuccessAddResponse();
                                    } else {
                                        if(view != null)
                                        view.onFailure();
                                    }break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            if(view != null)
                                                view.hideProgress();
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            addMedication(body);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            if(view != null) {
                                                view.stopProgressBar();
                                                view.onFailure();
                                            }
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null)
                                                view.hideProgress();
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, context);
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    if(view != null)
                                    view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, context);
                                    break;
                                default:
                                    if(view != null)
                                    view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            if(view != null) {
                                view.stopProgressBar();
                                view.onFailure();
                            }
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void editMedication(JSONObject body) {
        if(view != null)
        view.startProgressBar();
        service.editMedication(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, body.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if(view != null)
                                    view.stopProgressBar();
                                    if (response != null && !response.isEmpty()) {
                                        if(view != null)
                                        view.onSuccessAddResponse();
                                    } else {
                                        if (view != null)
                                            view.onFailure();
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            if(view != null)
                                                view.hideProgress();
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            addMedication(body);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            if(view != null)
                                            {
                                                view.hideProgress();
                                                view.onFailure();
                                            }
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null)
                                                view.hideProgress();
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, context);
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    if(view != null)
                                    view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, context);
                                    break;
                                default:
                                    if(view != null)
                                    view.stopProgressBar();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            if(view != null) {
                                view.stopProgressBar();
                                view.onFailure();
                            }
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }
}
