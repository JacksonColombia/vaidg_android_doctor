package com.vaidg.pro.main.profile.share;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityShareBinding;
import com.vaidg.pro.utility.DialogHelper;
import com.vaidg.pro.utility.MixpanelEvents;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import java.util.List;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>ShareActivity</h1>
 * ShareActivity for showing the share option
 */

public class ShareActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityShareBinding binding;
//    private String referalCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityShareBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
    }

    /**
     * init the views
     */
    private void initViews() {
//        SessionManager sessionManager = SessionManager.getSessionManager(this);
//        referalCode = sessionManager.getReferalCode();

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);


        setSupportActionBar(binding.includeToolbar.toolbar);
        binding.includeToolbar.toolbar.setElevation(0f);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        binding.includeToolbar.tvTitle.setText(getString(R.string.app_name));
        Utility.setTextColor(this, binding.includeToolbar.btnDone, R.color.colorPrimary);
        binding.includeToolbar.tvTitle.setTypeface(fontBold);

        binding.includeToolbar.btnDone.setText(R.string.share);
        binding.includeToolbar.btnDone.setOnClickListener(this);
        binding.tvShareMsa1.setTypeface(fontRegular);
//        binding.tvShareCodeLabel.setTypeface(fontRegular);
//        binding.tvShareCode.setTypeface(fontRegular);
        binding.tvShareMsa2.setTypeface(fontRegular);
        binding.tvFacebook.setTypeface(fontRegular);
        binding.tvInstagram.setTypeface(fontRegular);
        binding.tvEmail.setTypeface(fontRegular);
        binding.tvMessage.setTypeface(fontRegular);
        binding.tvWatsapp.setTypeface(fontRegular);
        binding.tvSnapChat.setTypeface(fontRegular);
        binding.tvTwitter.setTypeface(fontRegular);

        binding.tvFacebook.setOnClickListener(this);
        binding.tvInstagram.setOnClickListener(this);
        binding.tvEmail.setOnClickListener(this);
        binding.tvMessage.setOnClickListener(this);
        binding.tvWatsapp.setOnClickListener(this);
        binding.tvSnapChat.setOnClickListener(this);
        binding.tvTwitter.setOnClickListener(this);

//        tvShareCode.setText(referalCode);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        finishAfterTransition();
    }


    @Override
    public void onClick(View v) {

        AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.SentReferal.value);

        switch (v.getId()) {
            case R.id.btnDone:
                shareApp();
                break;
            case R.id.tvFacebook:
                openFaceBook();
                break;

            case R.id.tvInstagram:
                openInstagram();
                break;

            case R.id.tvEmail:
                openEmail();
                break;

            case R.id.tvMessage:
                openMessage();
                break;

            case R.id.tvWatsapp:
                openWatsapp();
                break;

            case R.id.tvSnapChat:
                openSnapChat();
                break;

            case R.id.tvTwitter:
                openTwitter();
                break;
        }
    }

    /**
     * method for sharing app in common way
     */
    private void shareApp() {
        Intent i = new Intent(android.content.Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TITLE, getString(R.string.subjectForEmailShare));
        i.putExtra(Intent.EXTRA_TEXT, getString(R.string.messageContentForSharingAppInOtherApp)/* + " " +
                getString(R.string.hereMyCode) + " " +
                referalCode + ".\n"*/ +
                VariableConstant.PLAY_STORE_LINK);
        startActivity(Intent.createChooser(i, getString(R.string.share_app)));
    }

    /**
     * method for opening the Facebook for sharing the app
     */
    private void openFaceBook() {
        try {

            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, VariableConstant.PLAY_STORE_LINK);
            boolean facebookAppFound = false;
            List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
            for (ResolveInfo info : matches) {
                if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook")) {
                    intent.setPackage(info.activityInfo.packageName);
                    facebookAppFound = true;
                    break;
                }
            }

            if (facebookAppFound) {
                startActivity(intent);
            } else {
                String url = "https://facebook.com/sharer.php?u=" + VariableConstant.PLAY_STORE_LINK;
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse(url));
                // Verify that the intent will resolve to an activity
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(sendIntent);
                } else {
                    DialogHelper.showWaringMessage(ShareActivity.this, "No app supports this action.");
                }

            }

        } catch (Exception e) {

            e.printStackTrace();
            Toast.makeText(this, getString(R.string.facebookNotInstalled), Toast.LENGTH_SHORT).show();

        }
    }

    /**
     * method for opening the Instagram for sharing the app
     */
    private void openInstagram() {

        try {
            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TITLE, getString(R.string.subjectForEmailShare));
            shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.messageContentForSharingAppInOtherApp)/* + " " +
                    getString(R.string.hereMyCode) + " " +
                    referalCode + ".\n"*/ +
                    VariableConstant.PLAY_STORE_LINK);
            shareIntent.setPackage("com.instagram.android");
            startActivity(shareIntent);
        } catch (Exception e) {
            Toast.makeText(this, getString(R.string.instagramNotInstalled), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * method for opening the Emai for sharing the app
     */
    @SuppressLint("IntentReset")
    private void openEmail() {
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
        i.setType("message/rfc822");
        i.setData(Uri.parse("mailto:" + ""));
        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.subjectForEmailShare));
        i.putExtra(Intent.EXTRA_TEXT, getString(R.string.messageContentForSharingAppInOtherApp)/* + " " +
                getString(R.string.hereMyCode) + " " +
                referalCode + ".\n"*/ +
                VariableConstant.PLAY_STORE_LINK);
        startActivity(Intent.createChooser(i, "Send email"));
    }

    /**
     * method for opening the Message for sharing the app
     */
    private void openMessage() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("sms:"));
        intent.putExtra("sms_body", getString(R.string.messageContentForSharingAppInOtherApp)/* + " " +
                getString(R.string.hereMyCode) + " " +
                referalCode + ".\n"*/ +
                VariableConstant.PLAY_STORE_LINK);
        startActivity(intent);

    }

    /**
     * method for opening the Watsapp for sharing the app
     */
    private void openWatsapp() {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.messageContentForSharingAppInOtherApp)/* + " " +
                getString(R.string.hereMyCode) + " " +
                referalCode + ".\n"*/ +
                VariableConstant.PLAY_STORE_LINK);
        try {
            startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, getString(R.string.watsappNotInstalled), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * method for opening the SnapChat for sharing the app
     */
    private void openSnapChat() {
        try {
            Intent snapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://snapchat.com"));
            snapIntent.setType("text/plain");
            snapIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.messageContentForSharingAppInOtherApp)/* + " " +
                    getString(R.string.hereMyCode) + " " +
                    referalCode + ".\n"*/ +
                    VariableConstant.PLAY_STORE_LINK);
            snapIntent.setPackage("com.snapchat.android");
            startActivity(snapIntent);
        } catch (Exception e) {
            Toast.makeText(this, getString(R.string.snapchatNotInstalled), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * method for opening the Twitter for sharing the app
     */
    private void openTwitter() {
        try {
            String url = "http://www.twitter.com/intent/tweet?url=" + VariableConstant.PLAY_STORE_LINK + "&text=" + getString(R.string.messageContentForSharingAppInOtherApp)/* + " " +
                    getString(R.string.hereMyCode) + " " +
                    referalCode + "."*/;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } catch (Exception e) {
            //Toast.makeText(this,getString(R.string.twitterNotInstalled),Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}
