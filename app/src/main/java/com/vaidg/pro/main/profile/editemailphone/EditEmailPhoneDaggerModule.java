package com.vaidg.pro.main.profile.editemailphone;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>EditEmailPhoneDaggerModule</h1>
 * <p>This class is collection of object that bind with dagger.</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 01/07/2020
 **/
@Module
public abstract class EditEmailPhoneDaggerModule {

    @ActivityScoped
    @Binds
    abstract EditEmailPhoneContract.Presenter providePresenter(EditEmailPhonePresenterImple editEmailPhonePresenterImple);

    @ActivityScoped
    @Binds
    abstract EditEmailPhoneContract.View provideView(EditEmailPhoneActivity editEmailPhoneActivity);

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(EditEmailPhoneActivity activity);

}
