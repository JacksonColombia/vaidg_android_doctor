package com.vaidg.pro.otpverify;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>OTPVerifyDaggerModule</h1>
 * <p>This class is collection of object that bind with dagger.</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 24/06/2020
 **/
@Module
public abstract class OTPVerifyDaggerModule {

    @ActivityScoped
    @Binds
    abstract OTPVerifyContract.Presenter providerPresenter(OTPVerifyPresenterImple otpVerifyPresenterImple);

    @ActivityScoped
    @Binds
    abstract OTPVerifyContract.View provideView(OTPVerifyActivity otpVerifyActivity);

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(OTPVerifyActivity activity);

}
