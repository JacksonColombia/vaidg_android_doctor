package com.vaidg.pro.landing.newSignup.consultaionFee;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.ConsultationFeeAdapter;
import com.vaidg.pro.databinding.ActivityConsultationFeeBinding;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;


/**
 * <h1>@ConsultationFeeActivity</h1>
 * <p>  ConsultationFeeActivity activity for setup ConsultationFee Details For Doctor</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 28/04/2020.
 **/
public class ConsultationFeeActivity extends BaseDaggerActivity implements ConsultationFeeContract.View {

    @Inject
    ConsultationFeeAdapter mConsultationFeeAdapter;

    @Inject
    ConsultationFeeContract.Presenter presenter;

    private ActivityConsultationFeeBinding binding;

    private ProgressDialog progressDialog;

    private boolean isFromEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityConsultationFeeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        init();
        getData();
        initOnclickLister();
        consultationDataSetUp();
    }

    private void getData() {
        if (getIntent() != null && getIntent().hasExtra("isFromEdit")) {
            isFromEdit = getIntent().getBooleanExtra("isFromEdit", false);
        }

        if (isFromEdit) {
            presenter.getCallTypeSetting();
        } else {
            setFeesData();
        }

    }

    private void initOnclickLister() {
        binding.includeToolbar.btnDone.setOnClickListener(this::onClick);
    }

    /*
     * this is the method for initializing views
     * */
    private void init() {
        binding.includeToolbar.tvTitle.setText(getString(R.string.selectYourFee));
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        binding.includeToolbar.btnDone.setText(getString(R.string.save));

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.msgGettingFees));

    }

    /**
     * method for start the SelectCity for select the City Adapter
     */
    private void consultationDataSetUp() {
        binding.rvConsulationFee.setAdapter(mConsultationFeeAdapter);
    }

    @Override
    public void onBackPressed() {
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();
    }

    public void onClick(View view) {
        if (view.getId() == R.id.btnDone) {
            if (!isFromEdit) {
                presenter.sendBackData();
            } else {
                progressDialog.setMessage(getString(R.string.msgUpdatingFees));
                presenter.updateCallTypeSetting();
            }
        }
    }

    @Override
    public void notifyData() {
        mConsultationFeeAdapter.notifyDataSetChanged();
    }

    @Override
    public void finish(Intent intent) {
        Utility.progressDialogCancel(this, progressDialog);
        if (!isFromEdit)
            setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void showError() {
        showError(getString(R.string.serverError));
    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.rlParent, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void setFeesData() {
        String currencySymbol = getIntent().getStringExtra("currencySymbol");
        String currencyAbrr = getIntent().getStringExtra("currencyAbbr");
        String minFee = getIntent().getStringExtra("minFee");
        String maxFee = getIntent().getStringExtra("maxFee");
        presenter.setData(currencyAbrr,currencySymbol, Utility.isTextEmpty(minFee) ? 0 : Integer.parseInt(minFee), Utility.isTextEmpty(maxFee) ? 0 : Integer.parseInt(maxFee),
                ((getIntent() != null) && getIntent().hasExtra("fees")) ? new Gson().fromJson(getIntent().getStringExtra("fees"), new TypeToken<List<String>>() {
                }.getType()) : null);
    }

    @Override
    public void onSuccessUpdateProfile(Intent intent, String msg) {
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void showProgress() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void hideProgress() {
        Utility.progressDialogDismiss(this, progressDialog);
    }
}
