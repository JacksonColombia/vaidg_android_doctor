package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ErrorsItem implements Serializable {

	@SerializedName("reason")
	@Expose
	private String reason;

	@SerializedName("code")
	@Expose
	private String code;

	@SerializedName("requirement")
	@Expose
	private String requirement;


	public String getReason(){
		return reason;
	}

	public String getCode(){
		return code;
	}

	public String getRequirement(){
		return requirement;
	}

	@Override
	public String toString(){
		return
				"ErrorsItem{" +
						"reason = '" + reason + '\'' +
						",code = '" + code + '\'' +
						",requirement = '" + requirement + '\'' +
						"}";
	}
}