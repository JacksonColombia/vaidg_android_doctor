package com.vaidg.pro.main.profile.support;


import android.app.Activity;

import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.profile.support.SupportPojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;
import static com.vaidg.pro.utility.VariableConstant.USER_TYPE;

/**
 * <h1>SupportPresenterImple</h1>
 * <p>This presenter is define to handle activity interactions done by user.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 04/07/2020
 **/
public class SupportPresenterImple implements SupportContract.Presenter {

    @Inject
    Activity activity;
    @Inject
    SupportContract.View view;
    @Inject
    NetworkService service;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    SessionManager sessionManager;
    @Inject
    Gson gson;

    @Inject
    public SupportPresenterImple() {
    }

    @Override
    public void getSupport() {
        if (networkStateHolder.isConnected()) {
            if (view != null)
            view.showProgress();
            service.support(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, USER_TYPE)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        if (view != null)
                                        view.hideProgress();
                                        if (response != null && !response.isEmpty()) {
                                            SupportPojo supportPojo = gson.fromJson(response, SupportPojo.class);
                                            if (view != null)
                                            view.onSuccess(supportPojo.getData());
                                        } else {
                                            if (view != null)
                                                view.showError();
                                        }
                                        break;
                                    case SESSION_EXPIRED:
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                getSupport();
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                if (view != null) {
                                                    view.hideProgress();
                                                    view.showError();
                                                }
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, activity);
                                            }
                                        });
                                        break;
                                    case SESSION_LOGOUT:
                                        if (view != null)
                                        view.hideProgress();
                                        Utility.logoutSessionExiperd(sessionManager, activity);
                                        break;
                                    default:
                                        if (view != null) {
                                            view.hideProgress();
                                            view.showError(Utility.getMessage(errorBody));
                                        }
                                        break;
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.showError();
                            }
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }
}
