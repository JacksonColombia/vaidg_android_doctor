package com.vaidg.pro.landing.newLogin;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.PLATFORM;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.AES_MODE;
import static com.vaidg.pro.utility.VariableConstant.ANDROID_KEYSTORE;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SEVEN;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;
import static com.vaidg.pro.utility.VariableConstant.FIXED_IV;
import static com.vaidg.pro.utility.VariableConstant.IV_PARAMETER_SPEC;
import static com.vaidg.pro.utility.VariableConstant.KEY_PRIVATEALIAS;
import static com.vaidg.pro.utility.VariableConstant.KEY_PUBLICALIAS;
import static com.vaidg.pro.utility.VariableConstant.RSA_MODE;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;

import com.google.firebase.messaging.FirebaseMessaging;
import com.vaidg.pro.R;
import com.vaidg.pro.landing.newLogin.model.LogInModel;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.utility.MixpanelEvents;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.encryptiondecryption.CryptographyExample;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilKeyPair;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Calendar;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import javax.security.auth.x500.X500Principal;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h1>LogInPresenterImpl</h1>
 * <p>This presenter is define to handle activity interactions done by user.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 08/06/2020
 **/
public class LogInPresenter implements LogInContract.Presenter {

    @Inject
    LogInContract.View view;
    @Inject
    Activity activity;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    LogInModel model;
    @Inject
    NetworkService service;
    @Inject
    SessionManager sessionManager;

    private KeyStore keyStore;
    private String exportedPrivateKey = "", exportedPublicKey = "";
    @Inject
    public LogInPresenter() {
    }

    @Override
    public boolean verifyEmailPhone(String emailPhone) {
        if (TextUtils.isDigitsOnly(emailPhone)) {
            if (emailPhone.length() < SEVEN) {
                view.onEmailPhoneInCorrect();
                return false;
            } else {
                view.onEmailPhoneCorrect();
                return true;
            }
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(emailPhone).matches()) {
                view.onEmailPhoneInCorrect();
                return false;
            } else {
                view.onEmailPhoneCorrect();
                return true;
            }
        }
    }

    @Override
    public boolean verifyPassword(String password) {
        if (!model.isValidPassword(password)) {
            view.onPasswordInCorrect();
            return false;
        } else {
            view.onPasswordCorrect();
            return true;
        }
    }

    @Override
    public void logIn(String emailPhone, String password) {
        Utility.hideKeyboad(activity);

        if (!verifyEmailPhone(emailPhone) || !verifyPassword(password)) {
            return;
        }

        if (networkStateHolder.isConnected()) {
            if(view != null)
            view.showProgress();
            Utility.basicAuth(service).signIn(model.getLanguage(), model.getPlatform(),
                    model.getLoginPayload(emailPhone, password).toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;

                                if (statusCode == SUCCESS_RESPONSE) {
                                    if(view != null)
                                    view.hideProgress();
                                    model.parseLogInData(response);
                                    sessionManager.setIsRemembered(view.isRememberMe());
                                    sessionManager.setUserName(emailPhone);
                                    sessionManager.setPassword(password);
                                    sessionManager.setProviderId(model.getLogInPojo().getData().getId());
                                    sessionManager.setEmail(model.getLogInPojo().getData().getEmail());
                                    sessionManager.setFirstName(model.getLogInPojo().getData().getFirstName());
                                    sessionManager.setLastName(model.getLogInPojo().getData().getLastName());
                                    sessionManager.setProfilePic(model.getLogInPojo().getData().getProfilePic());
                                    sessionManager.setPhoneNumber(model.getLogInPojo().getData().getMobile());
                                    sessionManager.setCountryCode(model.getLogInPojo().getData().getCountryCode());
                                    sessionManager.setReferalCode(model.getLogInPojo().getData().getReferralCode());
                                    sessionManager.setIsDriverLogin(true);
                                    sessionManager.setDeviceId(Utility.getDeviceId(activity));
                                    sessionManager.setFCMTopic(model.getLogInPojo().getData().getFcmTopic());
                                    sessionManager.setZendeskRequesterId(model.getLogInPojo().getData().getRequester_id());
                                    sessionManager.setIsBidBooking(model.getLogInPojo().getData().getBid());
                                    sessionManager.setCallToken(model.getLogInPojo().getData().getCall().getAuthToken());
                                    sessionManager.setCallWillTopic(model.getLogInPojo().getData().getCall().getWillTopic());
                                    sessionManager.setVirgilPrivateKey(model.getLogInPojo().getData().getPrivateKey());
                                    sessionManager.setVirgilPublicKey(model.getLogInPojo().getData().getPublickKey());
                                    getInstance().getAccountManagerHelper().setAuthToken(model.getLogInPojo().getData().getEmail(),
                                            password, model.getLogInPojo().getData().getToken().getAccessToken());
                                    FirebaseMessaging.getInstance().subscribeToTopic(sessionManager.getFCMTopic());
                                    getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.Login.value);
                                    checkForKeystore();
                                    if(view != null)
                                    view.onSuccess();
                                } else {
                                    if(view != null) {
                                        view.hideProgress();
                                        view.showError(Utility.getMessage(errorBody));
                                    }
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError(activity.getString(R.string.serverError));
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.showError(activity.getString(R.string.serverError));
                            }
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    @Override
    public void checkForKeystore() {
        try {
            keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
            keyStore.load(null);
            view.checkForVersion();
        } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
        }
        // if (!keyStore.containsAlias(KEY_PRIVATEALIAS)) {

        //}
    }

    public void callUpdateProfileApi(String exportedPrivateKey, String exportedPublicKey) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("privateKey", exportedPrivateKey);
            jsonObject.put("publickKey", exportedPublicKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        service.updateProfile(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                    } else {
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            callUpdateProfileApi(exportedPrivateKey, exportedPublicKey);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            getInstance().toast(activity.getResources().getString(R.string.serverError));
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, activity);
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, activity);
                                    break;
                                default:
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void checkForVersion(CryptographyExample cryptographyExample, VirgilCrypto crypto, VirgilKeyPair keyPair) {
        try {
            if (TextUtils.isEmpty(sessionManager.getVirgilPrivateKey())) {
                // Export private key
                exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
            } else {
                // Export private key
                exportedPrivateKey = sessionManager.getVirgilPrivateKey();
            }
            if (TextUtils.isEmpty(sessionManager.getVirgilPublicKey())) {
                // Export public key
                exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);
            } else {
                // Export public key
                exportedPublicKey = sessionManager.getVirgilPublicKey();
            }
        } catch (CryptoException e) {
            e.printStackTrace();
        }

        if (TextUtils.isEmpty(sessionManager.getVirgilPrivateKey()) || TextUtils.isEmpty(sessionManager.getVirgilPublicKey())) {
            callUpdateProfileApi(exportedPrivateKey, exportedPublicKey);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            generateEncryptionPrivateKeyPostLollipop(exportedPrivateKey);
            generateEncryptionPublicKeyPostLollipop(exportedPublicKey);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            generateEncryptionPrivateKeyPreMarshMallow(exportedPrivateKey);
            generateEncryptionPublicKeyPreMarshMallow(exportedPublicKey);
        }
    }


    /**
     * <hg2>generateAndStorePublicKeyAES</hg2>
     * This method is useed to generate the key for pre marsh mallow and store
     */
    private void generateAndStorePublicKeyAES(String exportedPublicKey) {
        String encryptedKeyB64 = exportedPublicKey;//sessionManager.getEncryptionPublicKey();
        if (encryptedKeyB64 == null) {
            byte[] key = new byte[16];
            SecureRandom secureRandom = new SecureRandom();
            secureRandom.nextBytes(key);
            byte[] encryptedKey;
            try {
                encryptedKey = rsaPublicKeyEncrypt(key);
                encryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT);
                sessionManager.storeEncryptionPublicKey(encryptedKeyB64);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * <hg2>generateAndStorePrivateKeyAES</hg2>
     * This method is useed to generate the key for pre marsh mallow and store
     */
    private void generateAndStorePrivateKeyAES(String exportedPrivateKey) {
        String encryptedKeyB64 = exportedPrivateKey;//sessionManager.getEncryptionPrivateKey();
        if (encryptedKeyB64 == null) {
            byte[] key = new byte[16];
            SecureRandom secureRandom = new SecureRandom();
            secureRandom.nextBytes(key);
            byte[] encryptedKey;
            try {
                encryptedKey = rsaPrivateKeyEncrypt(key);
                encryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT);
                sessionManager.storeEncryptionPrivateKey(encryptedKeyB64);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * <h2>rsaPublicKeyEncrypt</h2>
     * This method is used to do the rsa encrypt
     *
     * @param secret takes secret bytes as input
     * @return returns the RSA encrypt byts
     * @throws Exception throws an exception
     */
    private byte[] rsaPublicKeyEncrypt(byte[] secret) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
        // Encrypt the text
        Cipher inputCipher = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        inputCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inputCipher);
        cipherOutputStream.write(secret);
        cipherOutputStream.close();
        return outputStream.toByteArray();
    }

    /**
     * <h2>rsaPrivateKeyEncrypt</h2>
     * This method is used to do the rsa encrypt
     *
     * @param secret takes secret bytes as input
     * @return returns the RSA encrypt byts
     * @throws Exception throws an exception
     */
    private byte[] rsaPrivateKeyEncrypt(byte[] secret) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
        // Encrypt the text
        Cipher inputCipher = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        inputCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inputCipher);
        cipherOutputStream.write(secret);
        cipherOutputStream.close();
        return outputStream.toByteArray();
    }


    /**
     * <h2>getSecretPrivateKeyPostLollipop</h2>
     * This method is used to get the secret key for post lollipop device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPrivateKeyPostLollipop() throws Exception {
        Key s = keyStore.getKey(KEY_PRIVATEALIAS, null);
        Log.i("TEST", "key " + s.toString());
        return keyStore.getKey(KEY_PRIVATEALIAS, null);
    }

    /**
     * <h2>getSecretPublicKeyPostLollipop</h2>
     * This method is used to get the secret key for post lollipop device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPublicKeyPostLollipop() throws Exception {
        Key s = keyStore.getKey(KEY_PUBLICALIAS, null);
        Log.i("TEST", "key " + s.toString());
        return keyStore.getKey(KEY_PUBLICALIAS, null);
    }

    /**
     * <h2>getSecretPrivateKeyPreMarshMallow</h2>
     * This method is used to get the secret key for pre marsh mallow device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPrivateKeyPreMarshMallow(String exportedPrivateKey) throws Exception {
        String enryptedKeyB64 = exportedPrivateKey;//sessionManager.getEncryptionPrivateKey();
        Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
        // need to check null, omitted here
        byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaPrivateKeyDecrypt(encryptedKey);
        Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
        return new SecretKeySpec(key, "AES");
    }

    /**
     * <h2>getSecretPublicKeyPreMarshMallow</h2>
     * This method is used to get the secret key for pre marsh mallow device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPublicKeyPreMarshMallow(String exportedPublicKey) throws Exception {
        String enryptedKeyB64 = exportedPublicKey;//sessionManager.getEncryptionPublicKey();
        Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
        // need to check null, omitted here
        byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaPublicKeyDecrypt(encryptedKey);
        Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
        return new SecretKeySpec(key, "AES");
    }


    private byte[] rsaPrivateKeyDecrypt(byte[] encrypted) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
        CipherInputStream cipherInputStream = new CipherInputStream(
            new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }
        return bytes;
    }

    private byte[] rsaPublicKeyDecrypt(byte[] encrypted) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
        CipherInputStream cipherInputStream = new CipherInputStream(
            new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }
        return bytes;
    }

    @SuppressLint("NewApi")
    @Override
    public void generateEncryptionPrivateKeyPostLollipop(String exportedPrivateKey) {
        KeyGenerator keyGenerator = null;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEYSTORE);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        try {
            assert keyGenerator != null;
            keyGenerator.init
                (new KeyGenParameterSpec.Builder(KEY_PRIVATEALIAS,
                    KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                    .setRandomizedEncryptionRequired(false)
                    .build());
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        keyGenerator.generateKey();
        createVirgilPrivateKey(exportedPrivateKey);
    }


    @SuppressLint("NewApi")
    @Override
    public void generateEncryptionPublicKeyPostLollipop(String exportedPublicKey) {
        KeyGenerator keyGenerator = null;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEYSTORE);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        try {
            assert keyGenerator != null;
            keyGenerator.init
                (new KeyGenParameterSpec.Builder(KEY_PUBLICALIAS,
                    KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                    .setRandomizedEncryptionRequired(false)
                    .build());
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        keyGenerator.generateKey();
        createVirgilPublicKey(exportedPublicKey);
    }

    @SuppressLint("NewApi")
    @Override
    public void generateEncryptionPrivateKeyPreMarshMallow(String exportedPrivateKey) {
        // Generate a key pair for encryption
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        end.add(Calendar.YEAR, 30);
        KeyPairGeneratorSpec spec;
        spec = new KeyPairGeneratorSpec.Builder(activity)
            .setAlias(KEY_PRIVATEALIAS)
            .setSubject(new X500Principal("CN=" + KEY_PRIVATEALIAS))
            .setSerialNumber(BigInteger.TEN)
            .setStartDate(start.getTime())
            .setEndDate(end.getTime())
            .build();

        KeyPairGenerator kpg = null;
        try {
            kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEYSTORE);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        try {
            assert kpg != null;
            kpg.initialize(spec);
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        kpg.generateKeyPair();

        generateAndStorePrivateKeyAES(exportedPrivateKey);


        createVirgilPrivateKey(exportedPrivateKey);
    }

    @SuppressLint("NewApi")
    @Override
    public void generateEncryptionPublicKeyPreMarshMallow(String exportedPublicKey) {
        // Generate a key pair for encryption
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        end.add(Calendar.YEAR, 30);
        KeyPairGeneratorSpec spec;
        spec = new KeyPairGeneratorSpec.Builder(activity)
            .setAlias(KEY_PUBLICALIAS)
            .setSubject(new X500Principal("CN=" + KEY_PUBLICALIAS))
            .setSerialNumber(BigInteger.TEN)
            .setStartDate(start.getTime())
            .setEndDate(end.getTime())
            .build();

        KeyPairGenerator kpg = null;
        try {
            kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEYSTORE);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        try {
            assert kpg != null;
            kpg.initialize(spec);
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        kpg.generateKeyPair();

        generateAndStorePublicKeyAES(exportedPublicKey);

        createVirgilPublicKey(exportedPublicKey);
    }

    private void createVirgilPrivateKey(String exportedPrivateKey) {
/*
    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      // Generate keys
      VirgilCrypto crypto = new VirgilCrypto();
      VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
      // Export private key
      exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
      // Export public key
      exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            encryptPrivateKeyPostLollipop(exportedPrivateKey);
        } else {
            encryptPrivateKeyPreMarshMallow(exportedPrivateKey);
        }
    }

    private void createVirgilPublicKey(String exportedPublicKey) {
/*
    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      // Generate keys
      VirgilCrypto crypto = new VirgilCrypto();
      VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
      // Export private key
      exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
      // Export public key
      exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            encryptPublicKeyPostLollipop(exportedPublicKey);
        } else {
            encryptPublicKeyPreMarshMallow(exportedPublicKey);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void encryptPrivateKeyPostLollipop(String input) {
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE);
            c.init(Cipher.ENCRYPT_MODE, getSecretPrivateKeyPostLollipop(), new GCMParameterSpec(128,
                FIXED_IV.getBytes()));
            byte[] encodedBytes = c.doFinal(input.getBytes(StandardCharsets.US_ASCII));

            String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
            Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
            sessionManager.setVirgilPrivateKey(encryptedBase64Encoded);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void encryptPublicKeyPostLollipop(String input) {
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE);
            c.init(Cipher.ENCRYPT_MODE, getSecretPublicKeyPostLollipop(), new GCMParameterSpec(128,
                FIXED_IV.getBytes()));
            byte[] encodedBytes = c.doFinal(input.getBytes(StandardCharsets.US_ASCII));

            String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
            Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
            sessionManager.setVirgilPublicKey(encryptedBase64Encoded);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void encryptPrivateKeyPreMarshMallow(String exportedPrivateKey) {
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE, "BC");
            c.init(Cipher.ENCRYPT_MODE, getSecretPrivateKeyPreMarshMallow(exportedPrivateKey), IV_PARAMETER_SPEC);
            byte[] encodedBytes = c.doFinal(exportedPrivateKey.getBytes());
            String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
            Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
            sessionManager.setVirgilPrivateKey(encryptedBase64Encoded);
            sessionManager.setVirgilPrivateKeyT(encryptedBase64Encoded);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void encryptPublicKeyPreMarshMallow(String exportedPublicKey) {
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE, "BC");
            c.init(Cipher.ENCRYPT_MODE, getSecretPublicKeyPreMarshMallow(exportedPublicKey), IV_PARAMETER_SPEC);
            byte[] encodedBytes = c.doFinal(exportedPublicKey.getBytes());
            String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
            Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
            sessionManager.setVirgilPublicKey(encryptedBase64Encoded);
            sessionManager.setVirgilPublicKeyT(encryptedBase64Encoded);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean getIsRemembered() {
        return sessionManager.getIsRemembered();
    }

    @Override
    public String getUserName() {
        return sessionManager.getUserName();
    }

    @Override
    public String getPassword() {
        return sessionManager.getPassword();
    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }
}
