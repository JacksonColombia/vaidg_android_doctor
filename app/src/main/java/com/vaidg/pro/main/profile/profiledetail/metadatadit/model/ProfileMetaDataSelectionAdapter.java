package com.vaidg.pro.main.profile.profiledetail.metadatadit.model;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ListSelectionItemBinding;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.utility.Utility;

import java.util.List;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

public class ProfileMetaDataSelectionAdapter extends RecyclerView.Adapter<ProfileMetaDataSelectionAdapter.ListSelectionViewHolder> {

    private List<PreDefined> preDefined;
    private Context context;
    private boolean isSingleSelection;
    private boolean isEditMode;
    private long mLastClickTime = System.currentTimeMillis();
    private static final long CLICK_TIME_INTERVAL = 300;
    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public ProfileMetaDataSelectionAdapter(List<PreDefined> preDefined, boolean isSingleSelection) {
        this.preDefined = preDefined;
        this.isSingleSelection = isSingleSelection;
    }

    public void setEditMode(boolean editMode) {
        isEditMode = editMode;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ListSelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListSelectionItemBinding binding = ListSelectionItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        context = parent.getContext();
        return new ListSelectionViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ListSelectionViewHolder holder, int position) {
        holder.binding.ivTick.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        holder.binding.clMain.setClickable(isEditMode);
        holder.binding.tvListSelection.setText(preDefined.get(position).getName());
        if (preDefined.get(position).isSelected()) {
            holder.binding.ivTick.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_checked));
            Utility.setTextColor(context, holder.binding.tvListSelection, R.color.lighter_blue);
        } else {
            holder.binding.ivTick.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_unselected));
            Utility.setTextColor(context, holder.binding.tvListSelection, R.color.darkTextColor);
        }
    }

    @Override
    public int getItemCount() {
        return preDefined != null ? preDefined.size() : ZERO;
    }

    class ListSelectionViewHolder extends RecyclerView.ViewHolder {

        ListSelectionItemBinding binding;

        ListSelectionViewHolder(@NonNull ListSelectionItemBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
            binding.clMain.setOnClickListener(this::onClick);
        }

        public void onClick(View v) {
            if (onItemClickListener != null) {
                long now = System.currentTimeMillis();
                if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
                    return;
                }
                mLastClickTime = now;
                new Handler().postDelayed(() -> {
                    if (isSingleSelection) {
                        for (PreDefined preDefined : preDefined) {
                            preDefined.setSelected(false);
                        }
                    }

                    if (preDefined.get(getAdapterPosition()).isSelected())
                        preDefined.get(getAdapterPosition()).setSelected(false);
                    else
                        preDefined.get(getAdapterPosition()).setSelected(true);

                    onItemClickListener.onItemSelected(getAdapterPosition(), preDefined.get(getAdapterPosition()));
                    notifyDataSetChanged();
                }, 200);
            }
        }
    }

    public void addData(List<PreDefined> data) {
        preDefined.clear();
        preDefined.addAll(data);
    }

    interface OnItemClickListener {
        void onItemSelected(int position, PreDefined preDefined);
    }
}

