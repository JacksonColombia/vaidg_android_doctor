package com.vaidg.pro.bookingflow.medication;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Context;
import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.pojo.medication.MedicationResponse;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import retrofit2.Response;

public class MedicationPresenterImple implements MedicationContract.Presenter {
  @Inject
  MedicationContract.View view;
  @Inject
  Gson gson;
  @Inject
  SessionManager sessionManager;
  @Inject
  Context context;
  @Inject
  NetworkService service;

  @Inject
  public MedicationPresenterImple() {
  }

  @Override
  public void attachView(Object view) {
  }

  @Override
  public void detachView() {
  }

  @Override
  public void updateStaus(JSONObject jsonObject, boolean isTelecallBooking) {
    service.getBookingStatus(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  view.stopProgressBar();
                  if (response != null && !response.isEmpty()) {
                    if (isTelecallBooking) {
                      view.onSuccess(response, false);
                    } else {
                      view.onSuccess(Utility.getMessage(response), false);
                    }
                  } else
                    view.onFailure();
                  break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      updateStaus(jsonObject, isTelecallBooking);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      getInstance().toast(Utility.getMessage(msg));
                      Utility.logoutSessionExiperd(sessionManager, context);
                    }
                  });
                  view.stopProgressBar();
                  break;
                case SESSION_LOGOUT:
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  Utility.logoutSessionExiperd(sessionManager, context);
                  break;
                default:
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  break;
              }
            } catch (Exception e) {
              view.stopProgressBar();
              view.onFailure();
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            view.stopProgressBar();
            view.onFailure();
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public String getBookingStr(Booking booking, String oldStr, String updatedStatus) {
    return oldStr.replace(booking.getBookingId() + "|" + booking.getStatus(), booking.getBookingId() + "|" + updatedStatus);
  }

  @Override
  public void getMedication(String bookidId, String medicationId) {
    view.startProgressBar();
    service.getMedication(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, bookidId, medicationId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  view.stopProgressBar();
                  if (response != null && !response.isEmpty()) {
                    MedicationResponse medicationResponse = gson.fromJson(response, MedicationResponse.class);
                    view.onSuccessResonse(medicationResponse.getData());
                  } else
                    view.onFailure();
                  break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      getMedication(bookidId, medicationId);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      getInstance().toast(Utility.getMessage(msg));
                      Utility.logoutSessionExiperd(sessionManager, context);
                    }
                  });
                  view.stopProgressBar();
                  break;
                case SESSION_LOGOUT:
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  Utility.logoutSessionExiperd(sessionManager, context);
                  break;
                default:
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  break;
              }
            } catch (Exception e) {
              view.stopProgressBar();
              view.onFailure();
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            view.stopProgressBar();
            view.onFailure();
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void delete(String bookidId, String medicationId) {
   // view.startProgressBar();
    service.deleteMedication(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, bookidId, medicationId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  if(view != null)
                  view.stopProgressBar();
                  if (response != null && !response.isEmpty()) {
                    getMedication(bookidId,"0");
                  } else {
                    if (view != null)
                      view.onFailure();
                  }
                  break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if(view != null)
                        view.stopProgressBar();
                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      getMedication(bookidId, medicationId);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                      if(view != null)
                      {
                        view.stopProgressBar();
                        view.onFailure();
                      }
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if(view != null)
                        view.stopProgressBar();
                      getInstance().toast(Utility.getMessage(msg));
                      Utility.logoutSessionExiperd(sessionManager, context);
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  Utility.logoutSessionExiperd(sessionManager, context);
                  break;
                default:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  break;
              }
            } catch (Exception e) {
              if(view != null) {
                view.stopProgressBar();
                view.onFailure();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.stopProgressBar();
              view.onFailure();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }
}
