package com.vaidg.pro.main.chats;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface ChatCustomerContract {

    interface View extends BaseView {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String failureMsg);

        void onFailure();

        void onSuccess(String chatPojo, boolean isFromApi);

        void onNewToken(String newToken);

        void sessionExpired(String msg);

    }

    interface Presenter extends BasePresenter<View> {
        /**
         * <h2>getWalletDetails</h2>
         * <p>
         * api call to get wallet setting details
         * method for calling api for geting the Category details
         * </p>
         */
        void getCustomerList(final boolean isBackground);
    }
}