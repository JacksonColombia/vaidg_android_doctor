package com.vaidg.pro.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Created by Ali on 2/7/2018.
 */

public class JitsiMeet implements Serializable {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data  data;

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data  implements Serializable{
        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("expiryTime")
        @Expose
        private String expiryTime;
        @SerializedName("expiryTimeInString")
        @Expose
        private String expiryTimeInString;

        public String getToken() {
            return token;
        }

        public String getExpiryTime() {
            return expiryTime;
        }

        public String getExpiryTimeInString() {
            return expiryTimeInString;
        }
    }
}
