package com.vaidg.pro.pojo.signup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 31-Mar-18.
 */

public class SubCategory implements Serializable {
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("city_id")
    @Expose
    private String city_id;
    @SerializedName("cat_id")
    @Expose
    private String cat_id;
    @SerializedName("sub_cat_name")
    @Expose
    private String sub_cat_name;
    @SerializedName("sub_cat_dec")
    @Expose
    private String sub_cat_desc;
    @SerializedName("num")
    @Expose
    private String num;
    @SerializedName("alreadySelected")
    @Expose
    private boolean alreadySelected = false;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSub_cat_name() {
        return sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public String getSub_cat_desc() {
        return sub_cat_desc;
    }

    public void setSub_cat_desc(String sub_cat_desc) {
        this.sub_cat_desc = sub_cat_desc;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public boolean isAlreadySelected() {
        return alreadySelected;
    }

    public void setAlreadySelected(boolean alreadySelected) {
        this.alreadySelected = alreadySelected;
    }
}
