package com.vaidg.pro.landing.newSignup.addClinic;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>AddClinicDaggerModule</h1>
 * <p>providing to dagger</p>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 25/04/2020.
 **/

@Module
public abstract class AddClinicDaggerModule {

    @ActivityScoped
    @Binds
    abstract AddClinicContract.Presenter providePresenter(AddClinicPresenter presenter);

    @ActivityScoped
    @Binds
    abstract AddClinicContract.View provideView(AddClinicActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity provideAddClinic(AddClinicActivity activity);

}
