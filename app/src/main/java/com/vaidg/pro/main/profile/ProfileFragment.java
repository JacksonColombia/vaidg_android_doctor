package com.vaidg.pro.main.profile;


import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.livechatinc.inappchat.ChatWindowActivity;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.CancelListAdapter;
import com.vaidg.pro.adapters.LanguagelListAdapter;
import com.vaidg.pro.databinding.FragmentProfileBinding;
import com.vaidg.pro.landing.newSignup.consultaionFee.ConsultationFeeActivity;
import com.vaidg.pro.main.MainActivity;
import com.vaidg.pro.main.earning.EarningWebActivity;
import com.vaidg.pro.main.history.HistoryGraphActivity;
import com.vaidg.pro.main.profile.about.AboutActivity;
import com.vaidg.pro.main.profile.bank.bankdetails.BankDetailsActivity;
import com.vaidg.pro.main.profile.helpcenter.HelpCenterActivity;
import com.vaidg.pro.main.profile.profiledetail.ProfileDetailActivity;
import com.vaidg.pro.main.profile.share.ShareActivity;
import com.vaidg.pro.main.profile.support.SupportActivity;
import com.vaidg.pro.main.profile.wallet.WalletActivity;
import com.vaidg.pro.pojo.appconfig.AppConfigData;
import com.vaidg.pro.pojo.booking.CancelPojo;
import com.vaidg.pro.pojo.language.LanguageData;
import com.vaidg.pro.pojo.profile.ProfileData;
import com.vaidg.pro.utility.LocaleUtil;
import com.vaidg.pro.utility.OkHttp3ConnectionStatusCode;
import com.vaidg.pro.utility.ServiceUrl;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

import static android.view.View.VISIBLE;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_DISPLAYLANGUAGE;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.Utility.isFromIndia;
import static com.vaidg.pro.utility.VariableConstant.REQUEST_CODE_UPDATE_FEES;

/**
 * Created by murashid on 10-Sep-17.
 * <h1>ProfileFragment</h1>
 * ProfileEditActivity activity for showing Profile details
 */
public class ProfileFragment extends DaggerFragment implements OnClickListener, ProfileContract.View, LanguagelListAdapter.CancelSelected {

    private static final String TAG = "ProfileFragment";

    @Inject
    Activity activity;

    @Inject
    ProfileContract.Presenter presenter;

    @Inject
    SessionManager sessionManager;

    private FragmentProfileBinding binding;

    private String cancelId = "";
    private ProgressDialog progressDialog;
    private int currentLanguage = 2;
    private ProfileData profileData;
    private Typeface fontMedium;
    private Typeface fontBold;
    private LayoutInflater inflater;
    private boolean isFromLiveChat = false;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentProfileBinding.inflate(inflater);
        this.inflater = inflater;
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        initViewOnClickListeners();
    }

    private void initViews() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        fontMedium = Utility.getFontMedium(activity);
        fontBold = Utility.getFontBold(activity);
        if (!sessionManager.getProfilePic().equals("")) {
            binding.ivProfile.setImageURI(sessionManager.getProfilePic());
        }

        presenter.getProfile();

        if (sessionManager.getWalletEnable().equals("false")) {
            binding.tvWallet.setVisibility(View.GONE);
            binding.viewWallet.setVisibility(View.GONE);
        }
/*
        binding.tvPaymentMethod.setVisibility((isFromIndia(activity)) ? View.GONE : VISIBLE);
        binding.viewPaymentMethod.setVisibility((isFromIndia(activity)) ? View.GONE : VISIBLE);
*/
    }

    private void initViewOnClickListeners() {
        binding.ivProfile.setOnClickListener(this);
        binding.tvName.setOnClickListener(this);
        binding.tvViewProfile.setOnClickListener(this);
        binding.tvPaymentMethod.setOnClickListener(this);
        binding.tvMyRateCard.setOnClickListener(this);
        binding.tvWallet.setOnClickListener(this);
        binding.tvHistory.setOnClickListener(this);
        binding.tvFaq.setOnClickListener(this);
        binding.tvHelpCenter.setOnClickListener(this);
        binding.tvLiveChat.setOnClickListener(this);
        binding.tvShare.setOnClickListener(this);
        binding.tvVaidG.setOnClickListener(this);
        binding.tvEarnings.setOnClickListener(this);
        binding.tvLanguage.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    private void updateUI() {
        binding.tvName.setText(sessionManager.getTitle().concat(". ").concat(sessionManager.getFirstName().concat(Utility.isTextEmpty(sessionManager.getLastName()) ? "" : " ".concat(sessionManager.getLastName()))));
        if (VariableConstant.IS_PROFILE_PHOTO_UPDATED) {
            VariableConstant.IS_PROFILE_PHOTO_UPDATED = false;
            if (!sessionManager.getProfilePic().equals("")) {
                binding.ivProfile.setImageURI(sessionManager.getProfilePic());
            }
        }
//        binding.tvWallet.setText(getString(R.string.wallet) "(" + Utility.getPrice(sessionManager.getWalletAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()) + ")");
        if(isFromLiveChat)
        {
            isFromLiveChat = false;
            Configuration newConfig = new Configuration();
            newConfig.locale = Locale.forLanguageTag(sessionManager.getLanguageCode());
            onConfigurationChanged(newConfig);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivProfile:
            case R.id.tvName:
            case R.id.tvViewProfile:
                launchViewProfile();
                break;
            case R.id.tvMyRateCard:
                launchMyRateCard();
                break;
            case R.id.tvPaymentMethod:
                launchPaymentMethod();
                break;
            case R.id.tvWallet:
                launchWallet();
                break;
            case R.id.tvHistory:
                launchHistory();
                break;
            case R.id.tvFaq:
                launchFaq();
                break;
            case R.id.tvHelpCenter:
                launchHelpCenter();
                break;
            case R.id.tvLiveChat:
                launchLiveChat();
                break;
            case R.id.tvShare:
                launchShare();
                break;
            case R.id.tvVaidG:
                launchVaidG();
                break;
            case R.id.tvEarnings:
                launchEarnings();
                break;
    case R.id.tvLanguage:
        launchLanguage();
        break;
        }
    }

    private void launchLanguage() {
    presenter.getLanguage();
    }

    private void launchViewProfile() {
        Intent profileEditIntent = new Intent(getActivity(), ProfileDetailActivity.class);
        profileEditIntent.putExtra("data", profileData);
        startActivity(profileEditIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());

    }


    private void launchPaymentMethod() {
        Intent bankIntent = new Intent(getActivity(), BankDetailsActivity.class);
        startActivity(bankIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
    }

    private void launchMyRateCard() {
        Intent myRateCardIntent = new Intent(getActivity(), ConsultationFeeActivity.class);
        myRateCardIntent.putExtra("isFromEdit", true);
        myRateCardIntent.putExtra("currencySymbol", profileData.getCurrencySymbol());
        myRateCardIntent.putExtra("currencyAbbr", profileData.getCurrency());
        myRateCardIntent.putExtra("minFee", profileData.getMinimumFeesForConsultancy());
        myRateCardIntent.putExtra("maxFee", profileData.getMaximumFeesForConsultancy());
        myRateCardIntent.putExtra("fees", new Gson().toJson(Arrays.asList(profileData.getOutCallFee(), profileData.getInCallFee(), profileData.getTeleCallFee())));
        startActivityForResult(myRateCardIntent, REQUEST_CODE_UPDATE_FEES, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
    }

    private void launchWallet() {
        Intent walletIntent = new Intent(getActivity(), WalletActivity.class);
        startActivity(walletIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
    }

    private void launchHistory() {
        Intent historyIntent = new Intent(getActivity(), HistoryGraphActivity.class);
        startActivity(historyIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
    }

    private void launchFaq() {
        Intent faqIntent = new Intent(getActivity(), SupportActivity.class);
        startActivity(faqIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
    }

    private void launchHelpCenter() {
        Intent helpCenterIntent = new Intent(getActivity(), HelpCenterActivity.class);
        startActivity(helpCenterIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
    }

    private void launchLiveChat() {
        isFromLiveChat = true;
        Intent intent = new Intent(getActivity(), ChatWindowActivity.class);
        intent.putExtra(ChatWindowActivity.KEY_GROUP_ID, activity.getResources().getString(R.string.app_name));
        intent.putExtra(ChatWindowActivity.KEY_LICENCE_NUMBER, "4711811");
        intent.putExtra(ChatWindowActivity.KEY_VISITOR_NAME,sessionManager.getFirstName() +" "+sessionManager.getLastName());
        intent.putExtra(ChatWindowActivity.KEY_VISITOR_EMAIL, sessionManager.getEmail());
        startActivity(intent);
    }

    private void launchShare() {
        Intent shareIntent = new Intent(getActivity(), ShareActivity.class);
        startActivity(shareIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
    }

    private void launchVaidG() {
        Intent aboutIntent = new Intent(getActivity(), AboutActivity.class);
        startActivity(aboutIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
    }

    private void launchEarnings() {
        Intent earningIntent = new Intent(getActivity(), EarningWebActivity.class);
        startActivity(earningIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccessProfile(ProfileData profileData) {
        this.profileData = profileData;
        updateUI();
    }

    @Override
    public void onSuccess(String msg) {
    }

  @Override
  public void onLangSuccess(ArrayList<LanguageData> result) {
    if(result != null && result.size() > 0) {
      AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
      final View view = LayoutInflater.from(activity).inflate(R.layout.alert_dialog_cancel, null);
      alertDialogBuilder.setView(view);
      TextView tvTitle = view.findViewById(R.id.tvTitle);
      TextView tvSubmit = view.findViewById(R.id.tvSubmit);
      ImageView ivClose = view.findViewById(R.id.ivClose);
      tvTitle.setTypeface(fontMedium);
      tvSubmit.setTypeface(fontBold);
      RecyclerView rvCancel = view.findViewById(R.id.rvCancel);
      rvCancel.setLayoutManager(new LinearLayoutManager(activity));
      LanguagelListAdapter cancelListAdapter = new LanguagelListAdapter(activity, result,this);
      rvCancel.setAdapter(cancelListAdapter);
      final AlertDialog alertDialog = alertDialogBuilder.create();
      alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
      tvTitle.setText(activity.getResources().getString(R.string.language));
      tvSubmit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (!cancelId.equals("")) {
            try {
              alertDialog.dismiss();
              sessionManager.setLoadProfileFrag(true);
              Utility.changeLanguageConfig(DEFAULT_LANGUAGE,activity);
              activity.recreate();
             /* MainActivity mainActivity = (MainActivity) activity;
              mainActivity.refreshBookingFragment(1);*/
            } catch (Exception e) {
              e.printStackTrace();
            }
          } else {
            Toast.makeText(activity, getString(R.string.plsSelectCancel), Toast.LENGTH_SHORT).show();
          }
        }
      });
      ivClose.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          alertDialog.dismiss();
        }
      });
      alertDialog.setCancelable(false);
      alertDialog.show();
    }

  }

  @Override
  public void onCancelSelected(String code, String name) {
    this.cancelId = code;
    DEFAULT_LANGUAGE = code;
    DEFAULT_DISPLAYLANGUAGE = name;
  }

  @Override
  public void showError(String error) {
    Snackbar snackbar = Snackbar.make(binding.clFragmentProfile, error, Snackbar.LENGTH_LONG);
    View snackBarView = snackbar.getView();
    Utility.setBackgroundColor(activity, snackBarView, R.color.colorAccent);
    TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
    Utility.setTextColor(activity, textView, R.color.white);
    snackbar.show();
  }


    @Override
    public void showProgress() {
        Utility.progressDialogShow(activity, progressDialog);

    }

    @Override
    public void hideProgress() {
        Utility.progressDialogDismiss(activity, progressDialog);
    }

    @Override
    public void onDetach() {
        Utility.progressDialogCancel(activity, progressDialog);
        super.onDetach();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_UPDATE_FEES && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                profileData.setInCallFee(data.getStringExtra("inCallFee"));
                profileData.setOutCallFee(data.getStringExtra("outCallFee"));
                profileData.setTeleCallFee(data.getStringExtra("teleCallFee"));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if(TextUtils.isEmpty(sessionManager.getLanguageCode()))
        {
            DEFAULT_DISPLAYLANGUAGE  = "English";
            DEFAULT_LANGUAGE = "en";
        }else{
            if(sessionManager.getLanguageCode().equals("en"))
            {
                DEFAULT_DISPLAYLANGUAGE  = "English";
                DEFAULT_LANGUAGE = "en";
            }else
            {
                DEFAULT_DISPLAYLANGUAGE  = "Portuguese";
                DEFAULT_LANGUAGE = "pt";
            }
        }
        sessionManager.setLanguageCode(DEFAULT_LANGUAGE);
        Utility.changeLanguageConfig(DEFAULT_LANGUAGE, activity);
       // binding = FragmentProfileBinding.inflate(inflater);
    }
}
