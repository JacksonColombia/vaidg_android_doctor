package com.vaidg.pro.landing.newSignup;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivitySignupBinding;
import com.vaidg.pro.landing.newSignup.addClinic.AddClinicActivity;
import com.vaidg.pro.landing.newSignup.consultaionFee.ConsultationFeeActivity;
import com.vaidg.pro.landing.newSignup.consultaionFee.model.ConsultationData;
import com.vaidg.pro.landing.newSignup.educationBackground.EducationDetailsActivity;
import com.vaidg.pro.landing.newSignup.findHospital.model.HospitalDataPojo;
import com.vaidg.pro.landing.newSignup.selectCity.SelectCityActivity;
import com.vaidg.pro.landing.newSignup.selectSpecialization.SelectSpecializationActivity;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.landing.newSignup.yearOfExp.YearOfExpActivity;
import com.vaidg.pro.otpverify.OTPVerifyActivity;
import com.vaidg.pro.pojo.signup.CityData;
import com.vaidg.pro.pojo.signup.SignUpData;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.DatePickerCommon;
import com.vaidg.pro.utility.FileUtils;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;
import com.vaidg.pro.utility.encryptiondecryption.CryptographyExample;
import com.vaidg.pro.webview.CommonWebViewActivity;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilKeyPair;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import javax.inject.Inject;

import eu.janmuller.android.simplecropimage.CropImage;

import static com.vaidg.pro.utility.VariableConstant.DATA;

/**
 * <h1>@SignUpActivity</h1>
 * <p>  this is main  activity for sign up</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public class SignUpActivity extends BaseDaggerActivity implements SignUpContract.View, DatePickerCommon.DateSelected, View.OnFocusChangeListener {

    private static final String TAG = SignUpPresenter.class.getSimpleName();

    @Inject
    SignUpContract.Presenter presenter;

    @Inject
    App_permission app_permission;

    @Inject
    ProgressDialog progressDialog;

    @Inject
    DatePickerCommon datePickerFragment;

    @Inject
    Utility utility;

    private ActivitySignupBinding binding;

    private CityData cityData;     //Data

    private String sendingDob;

    private String exportedPrivateKey = "", exportedPublicKey = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySignupBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
        initViewOnClickListeners();
        initFocusOnViewListener();
    }

    private void initFocusOnViewListener() {
        binding.etTitle.setOnFocusChangeListener(this);
        binding.etEmail.setOnFocusChangeListener(this);
        binding.etPhone.setOnFocusChangeListener(this);
        binding.etFirstName.setOnFocusChangeListener(this);
        binding.etLastName.setOnFocusChangeListener(this);
        binding.etSelectCity.setOnFocusChangeListener(this);
        binding.etPassword.setOnFocusChangeListener(this);
        binding.etDob.setOnFocusChangeListener(this);
    }

    private void initViewOnClickListeners() {
        binding.etDob.setOnClickListener(this::onClick);
        binding.tlEducationBackground.setOnClickListener(this::onClick);
        binding.etEducationBackground.setOnClickListener(this::onClick);
        binding.tlClinicDetails.setOnClickListener(this::onClick);
        binding.etClinicDetails.setOnClickListener(this::onClick);
        binding.tlSelectCity.setOnClickListener(this::onClick);
        binding.etSelectCity.setOnClickListener(this::onClick);
        binding.tlSelectSpecialization.setOnClickListener(this::onClick);
        binding.etSelectSpecialization.setOnClickListener(this::onClick);
        binding.ivAddProfile.setOnClickListener(this::onClick);
        binding.tlYOE.setOnClickListener(this::onClick);
        binding.etYOE.setOnClickListener(this::onClick);
        binding.tlConsultationFee.setOnClickListener(this::onClick);
        binding.etConsultationFee.setOnClickListener(this::onClick);
        binding.btSignUp.setOnClickListener(this::onClick);
        binding.rbMale.setOnClickListener(new RadioButtonClickListener());
        binding.rbFemale.setOnClickListener(new RadioButtonClickListener());
        binding.rbOthers.setOnClickListener(new RadioButtonClickListener());
    }


    @Override
    public void initViews() {
        presenter.checkForKeystore();
        binding.include.tvTitle.setText(getString(R.string.signup));
        setSupportActionBar(binding.include.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.include.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        datePickerFragment.setDatePickerType(2);
        datePickerFragment.setCallBack(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);

        final SpannableString spannableString = new SpannableString(getString(R.string.signUpTermsConditions));
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.black)), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new UnderlineSpan(), 0, spannableString.length(), 0);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                launchTermAndCondition();
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(ContextCompat.getColor(SignUpActivity.this, R.color.black));
            }
        }, 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        final SpannableString spannablePrivacyString = new SpannableString(getString(R.string.signUpPrivacyConditions));
        spannablePrivacyString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.black)), 0, spannablePrivacyString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannablePrivacyString.setSpan(new UnderlineSpan(), 0, spannablePrivacyString.length(), 0);
        spannablePrivacyString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                launchTermAndCondition();
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(ContextCompat.getColor(SignUpActivity.this, R.color.black));
            }
        }, 0, spannablePrivacyString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        CharSequence txtTermAndConditions = TextUtils.concat(getString(R.string.signUpAgreeMessage), spannableString,getString(R.string.signUpAnd),spannablePrivacyString,getString(R.string.signUpPlatform));
        binding.tvSignUpTermConditions.setText(txtTermAndConditions);
        binding.tvSignUpTermConditions.setMovementMethod(LinkMovementMethod.getInstance());
        binding.ccp.registerCarrierNumberEditText(binding.etPhone);

    }

    class RadioButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Utility.hideKeyboad(SignUpActivity.this);
            if (getCurrentFocus() != null)
                getCurrentFocus().clearFocus();
        }
    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboad(this);
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();
        overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ivAddProfile:
                presenter.openChooser();
                break;

            case R.id.etDob:
                Utility.hideKeyboad(this);
                if (getCurrentFocus() != null)
                    getCurrentFocus().clearFocus();
                if (!datePickerFragment.isResumed())
                    datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                break;

            case R.id.tlEducationBackground:
            case R.id.etEducationBackground:
                selectDegree();
                break;

            case R.id.tlClinicDetails:
            case R.id.etClinicDetails:
                if (!Utility.isTextEmpty(binding.etSelectCity.getText() == null ? "" : binding.etSelectCity.getText().toString().trim()) && cityData != null)
                    selectClinicalDetails();
                else
                    binding.tlSelectCity.setError(getString(R.string.selectCity));
                break;

            case R.id.tlSelectCity:
            case R.id.etSelectCity:
                binding.tlSelectCity.setError(null);
                selectCity();
                break;

            case R.id.tlSelectSpecialization:
            case R.id.etSelectSpecialization:

                if (!Utility.isTextEmpty(binding.etSelectCity.getText() == null ? "" : binding.etSelectCity.getText().toString().trim()) && cityData != null)
                    selectSpecialization();
                else
                    binding.tlSelectCity.setError(getString(R.string.selectCity));
                break;

            case R.id.tlYOE:
            case R.id.etYOE:
                launchYearOfExperience();
                break;

            case R.id.tlConsultationFee:
            case R.id.etConsultationFee:
                if (!Utility.isTextEmpty(binding.etSelectSpecialization.getText() == null ? "" : binding.etSelectSpecialization.getText().toString().trim()))
                    presenter.consultationFee();
                else
                    binding.tlSelectSpecialization.setError("Select specialization");
                break;
            case R.id.btSignUp:
                progressDialog.setMessage(getString(R.string.registering));
                presenter.signUp(
                        "Dr",
                        binding.etFirstName.getText() == null ? "" : binding.etFirstName.getText().toString().trim(),
                        binding.etLastName.getText() == null ? "" : binding.etLastName.getText().toString().trim(),
                        sendingDob,
                        getGender(),
//                        binding.etReferralCode.getText().toString(),
                        "",
                        binding.etEmail.getText() == null ? "" : binding.etEmail.getText().toString().trim(),
                        binding.ccp.getSelectedCountryCodeWithPlus(),
                        binding.etPhone.getText() == null ? "" : binding.etPhone.getText().toString().trim().replaceAll("\\W", ""),
                        binding.etPassword.getText() == null ? "" : binding.etPassword.getText().toString().trim());
        }
    }

    private int getGender() {
        int selectedId = binding.rgGender.getCheckedRadioButtonId();
        RadioButton radioButton = findViewById(selectedId);
        return radioButton == null ? 0 : radioButton.getText().toString().equalsIgnoreCase("male") ? 1 : 2;
    }

    private void launchTermAndCondition() {
        Intent webIntent = new Intent(this, CommonWebViewActivity.class);
        webIntent.putExtra("title", getString(R.string.termsCondition));
        webIntent.putExtra("url", VariableConstant.TERMS_OF_SERVICE);
        webIntent.putExtra("isFromSupport", true);
        startActivity(webIntent);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }
    private void launchPrivacyPolicy() {
        Intent webIntent = new Intent(this, CommonWebViewActivity.class);
        webIntent.putExtra("title", getString(R.string.termsCondition));
        webIntent.putExtra("url", VariableConstant.TERMS_OF_SERVICE);
        webIntent.putExtra("isFromSupport", true);
        startActivity(webIntent);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }


    private void launchYearOfExperience() {
        Intent intent = new Intent(this, YearOfExpActivity.class);
        intent.putExtra(DATA, presenter.getYearOfExp());
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_YOE, ActivityOptions
                .makeSceneTransitionAnimation(this).toBundle());
    }

    private void selectSpecialization() {
        Intent intent = new Intent(this, SelectSpecializationActivity.class);
        intent.putExtra("cityId", cityData.getId());
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_SPECIALIZATION, ActivityOptions
                .makeSceneTransitionAnimation(this).toBundle());
    }

    @Override
    public void launchConsultationFee(SelectSpecializationData specializationData) {
        Intent intent = new Intent(this, ConsultationFeeActivity.class);
        intent.putExtra("currencySymbol", specializationData.getCurrencySymbol());
        intent.putExtra("currencyAbbr", specializationData.getCurrency());
        intent.putExtra("minFee", specializationData.getMinimumFeesForConsultancy());
        intent.putExtra("maxFee", specializationData.getMaximumFeesForConsultancy());
        if (presenter.getConsultationData() != null && presenter.getConsultationData().size() > 0) {
            intent.putExtra("fees", new Gson().toJson(Arrays.asList(
                    presenter.getConsultationData().get(0).getFee(),
                    presenter.getConsultationData().get(1).getFee(),
                    presenter.getConsultationData().get(2).getFee())));
        }
        startActivityForResult(intent,
                VariableConstant.REQUEST_CODE_CONSULTATION, ActivityOptions
                        .makeSceneTransitionAnimation(this).toBundle());
    }

    @Override
    public void selectCity() {
        Intent clinicDetails = new Intent(this, SelectCityActivity.class);
        startActivityForResult(clinicDetails, VariableConstant.REQUEST_CODE_CITY, ActivityOptions
                .makeSceneTransitionAnimation(this).toBundle());
    }

    @Override
    public void selectClinicalDetails() {
        Intent intent = new Intent(this, AddClinicActivity.class);
        intent.putExtra("cityId", cityData.getId());
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_OWN_CLINIC, ActivityOptions
                .makeSceneTransitionAnimation(this).toBundle());
    }

    @Override
    public void selectDegree() {
        Intent intentDegree = new Intent(this, EducationDetailsActivity.class);
        if (presenter.getEducationDetails() != null) {
            intentDegree.putExtra(DATA, new Gson().toJson(presenter.getEducationDetails()));
        }
        startActivityForResult(intentDegree, VariableConstant.REQUEST_CODE_EDUCATION, ActivityOptions
                .makeSceneTransitionAnimation(this).toBundle());
    }

    @Override
    public void showProgress() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void hideProgress() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onDateSelected(String sendingFormat, String displayFormat) {
        sendingDob = sendingFormat;
        binding.etDob.setText(displayFormat);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {
            if (resultCode != RESULT_OK)
                return;
            switch (requestCode) {

                case VariableConstant.REQUEST_CODE_TAKE_PICTURE:
                    presenter.upDateToGallery();
                    startCropImage(presenter.getRecentTemp());
                    break;

                case VariableConstant.REQUEST_CODE_GALLERY:
                    Uri uri = data.getData();
                    String file_path = FileUtils.getPath(this, uri);
                  //  binding.sdvProfile.setImageURI(uri);
//                    binding.sdvProfile.setImageBitmap(Utility.getCircleCroppedBitmap(BitmapFactory.decodeFile(file_path)));
                    //presenter.setImageProfile(file_path);
                    startCropImage(file_path);
                    break;

                case VariableConstant.REQUEST_CODE_CROP_IMAGE:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {
                        binding.ivSelfOwnedClinicLogoCam.setVisibility(View.VISIBLE);
                        binding.tvSelfOwnedClinicUploadLogo.setVisibility(View.VISIBLE);
                        return;
                    }
                    //binding.sdvProfile.setImageURI(path);
                        binding.ivSelfOwnedClinicLogoCam.setVisibility(View.GONE);
                        binding.tvSelfOwnedClinicUploadLogo.setVisibility(View.GONE);
                        binding.sdvProfile.setImageBitmap(Utility.getCircleCroppedBitmap(BitmapFactory.decodeFile(path)));
                    presenter.setImageProfile(path);
//                    presenter.upload(new File(path));
                    break;
            }
            presenter.parseOnActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void startCropImage(String recentTemp) {
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, recentTemp);
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        intent.putExtra("locale", Locale.US.getDisplayLanguage());
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_CROP_IMAGE);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            switch (v.getId()) {

                case R.id.etTitle:
                    presenter.verifyTitle(binding.etTitle.getText() == null ? "" : binding.etTitle.getText().toString());
                case R.id.etEmail:
                    try {
                        progressDialog.setMessage(getString(R.string.validatingEmail));
                        presenter.verifyEmailApi(binding.etEmail.getText() == null ? "" : binding.etEmail.getText().toString().trim());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.etPhone:
                    progressDialog.setMessage(getString(R.string.validatingPhone));
                    presenter.verifyPhoneApi(binding.etPhone.getText() == null ? "" : binding.etPhone.getText().toString().trim().replaceAll("\\W", ""),
                            binding.ccp.getSelectedCountryCodeWithPlus());
                    break;

                case R.id.etFirstName:
                    presenter.verifyFirstName(binding.etFirstName.getText() == null ? "" : binding.etFirstName.getText().toString());
                    break;

                case R.id.etLastName:
                    presenter.verifyLastName(binding.etLastName.getText() == null ? "" : binding.etLastName.getText().toString());
                    break;


                case R.id.etPassword:
                    presenter.verifyPassword(binding.etPassword.getText() == null ? "" : binding.etPassword.getText().toString());
                    break;
            }
        } else {
            if (v.getId() == R.id.etDob) {
                if (!datePickerFragment.isResumed())
                    datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
            }
        }
    }

    @Override
    public void onUserNameInCorrect() {
        setTilError(binding.tlFirstName, getString(R.string.enterName));
    }

    @Override
    public void onUserNameCorrect() {
        binding.tlFirstName.setErrorEnabled(false);
    }

    @Override
    public void onTitleInCorrect() {
        setTilError(binding.tlTitle, getString(R.string.enterName));
    }

    @Override
    public void onTitleCorrect() {
        binding.tlTitle.setErrorEnabled(false);
    }

    @Override
    public void onUserLastNameInCorrect() {
        setTilError(binding.tlLastName, getString(R.string.enterlastname));
    }

    @Override
    public void onUserLastNameCorrect() {
        binding.tlLastName.setErrorEnabled(false);
    }

    @Override
    public void onDobInCorrect() {
        setTilError(binding.tilDob, getString(R.string.selectDob));
    }

    @Override
    public void onDobCorrect() {
        binding.tilDob.setErrorEnabled(false);
    }

    @Override
    public void onEmailInCorrect() {
        setTilError(binding.tlEmail, getString(R.string.enterValidEmail));
    }

    @Override
    public void onEmailInCorrect(String errorMsg) {
        binding.tlPhone.setPasswordVisibilityToggleEnabled(false);
        setTilError(binding.tlEmail, errorMsg);
    }

    @Override
    public void onEmailCorrect() {
        binding.tlEmail.setErrorEnabled(false);
        binding.tlEmail.setPasswordVisibilityToggleEnabled(true);
        binding.tlEmail.setPasswordVisibilityToggleTintList(AppCompatResources.getColorStateList(this, R.color.colorAccent));
        binding.tlEmail.setPasswordVisibilityToggleDrawable(R.drawable.ic_checked);
    }

    @Override
    public boolean isPhoneNumberCorrect() {
        return binding.ccp.isValidFullNumber();
    }

    @Override
    public void onPhoneNumberInCorrect(int resId) {
       Toast.makeText(this, getString(resId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPhoneNumberCorrect() {
//       binding.tlName.setErrorEnabled(false);
        binding.tlPhone.setPasswordVisibilityToggleEnabled(true);
        binding.tlPhone.setPasswordVisibilityToggleTintList(AppCompatResources.getColorStateList(this, R.color.colorAccent));
        binding.tlPhone.setPasswordVisibilityToggleDrawable(R.drawable.ic_checked);
    }

    @Override
    public void clearPhoneNumber() {
        binding.tlPhone.setPasswordVisibilityToggleEnabled(false);
        binding.etPhone.setText("");
    }

    @Override
    public void onPasswordInCorrect() {
        binding.etPassword.setText("");
        setTilError(binding.tlPassword, getString(R.string.enterValidPassword));
    }

    @Override
    public void onPasswordCorrect() {
        binding.tlPassword.setErrorEnabled(false);
    }

    private void setTilError(TextInputLayout textInputLayout, String err) {
        try {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(err);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showError(String error) {

        Snackbar snackbar = Snackbar.make(binding.clSignUpActivity, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void displaySelectedCity(CityData cityData) {
        this.cityData = cityData;
        binding.etSelectCity.setText(cityData.getCity());
        binding.tlSelectCity.setErrorEnabled(false);
    }

    @Override
    public void openGallery() {
        String chooseTitle;
        Intent intent = new Intent();
        intent.setType("image/*");
        chooseTitle = "select Image";
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, chooseTitle), VariableConstant.REQUEST_CODE_GALLERY);
    }

    @Override
    public void openCamera(Uri uri) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_TAKE_PICTURE);
    }

    @Override
    public void showSpecializationData(String catName) {
        binding.tlSelectSpecialization.setErrorEnabled(false);
        binding.etSelectSpecialization.setText(catName);
    }

    @Override
    public void showEducationalData(String degree) {
        binding.tlEducationBackground.setErrorEnabled(false);
        binding.etEducationBackground.setText(degree);
    }

    @Override
    public void showYOE(String year) {
        binding.etYOE.setText(year);
        binding.tlYOE.setErrorEnabled(false);
    }

    @Override
    public void showConsultationFee(ArrayList<ConsultationData> consultationData) {
        binding.tlConsultationFee.setErrorEnabled(false);
        StringBuilder feeBuilder = new StringBuilder();
        String prefix = "";
        for (int i = 0; i < consultationData.size(); i++) {
            if (!Utility.isTextEmpty(consultationData.get(i).getFee())) {
                feeBuilder.append(prefix);
                prefix = ", ";
                feeBuilder.append(consultationData.get(i).getCurrency());
                feeBuilder.append(" ");
                feeBuilder.append(consultationData.get(i).getFee());
            }
        }
        binding.etConsultationFee.setText(feeBuilder.toString());
    }

    @Override
    public void showClinicData(HospitalDataPojo clinic) {
        if (clinic.getType() == 1)
            binding.etClinicDetails.setText(clinic.getClinicName());
        else
            binding.etClinicDetails.setText(clinic.getBusinessName());
    }

    @Override
    public void success(SignUpData signUpData) {
        Intent intent = new Intent(this, OTPVerifyActivity.class);
        intent.putExtra("phone",
                binding.etPhone.getText() == null ? "" : binding.etPhone.getText().toString().trim().replaceAll("\\W", ""));
        intent.putExtra("countryCode", binding.ccp.getSelectedCountryCodeWithPlus());
        intent.putExtra("userId", signUpData.getProviderId());
        intent.putExtra("expireOtp", signUpData.getExpireOtp());
        intent.putExtra("otpOption", 1);
       // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void checkForVersion() {
        try {
            CryptographyExample cryptographyExample = new CryptographyExample();
            // Generate keys
            VirgilCrypto crypto = new VirgilCrypto();
            VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
            // Export private key
            exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
            // Export public key
            exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);

            presenter.setVirgilPublicKeyT(exportedPublicKey);
            presenter.setVirgilPrivateKeyT(exportedPrivateKey);
        } catch (CryptoException e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            presenter.generateEncryptionPrivateKeyPostLollipop(exportedPrivateKey);
            presenter.generateEncryptionPublicKeyPostLollipop(exportedPublicKey);
        } else {
            presenter.generateEncryptionPrivateKeyPreMarshMallow(exportedPrivateKey);
            presenter.generateEncryptionPublicKeyPreMarshMallow(exportedPublicKey);
        }
    }

}
