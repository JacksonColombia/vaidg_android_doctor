package com.vaidg.pro.landing.introsilider2.model;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.databinding.ItemIntroSliderBinding;
import com.vaidg.pro.pojo.introslider.SliderData;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * <h1>IntroSliderAdapter</h1>
 * <p>This adapter is used to load list of images in slidingViewPager at IntroSliderNewActivity</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @see com.vaidg.pro.landing.introsilider2.IntroSliderNewActivity
 * @since 06/06/2020
 **/
public class IntroSliderAdapter extends RecyclerView.Adapter<IntroSliderAdapter.IntroSliderViewHolder> {

    private ArrayList<SliderData> arrayList;
    private ItemIntroSliderBinding binding;

    public IntroSliderAdapter(ArrayList<SliderData> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public IntroSliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = ItemIntroSliderBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new IntroSliderViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull IntroSliderViewHolder holder, int position) {
        holder.ivSliderImage.setImageResource(arrayList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return arrayList != null ? arrayList.size() : ZERO;
    }

    class IntroSliderViewHolder extends RecyclerView.ViewHolder {

        AppCompatImageView ivSliderImage;

        IntroSliderViewHolder(@NonNull ItemIntroSliderBinding itemView) {
            super(itemView.getRoot());
            ivSliderImage = binding.ivSliderImage;
        }
    }
}
