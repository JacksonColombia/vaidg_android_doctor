package com.vaidg.pro.landing.newSignup.selectSpecialization;

import android.content.Intent;

import com.vaidg.pro.BaseView;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;

/**
 * <h1>@SelectSpecializationContract</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface SelectSpecializationContract {

    interface View extends BaseView {

        void notifyAdapter();

        void showError();

        void showError(String error);

        void finishActivity(Intent intent);

        void nextCategoryActivity(SelectSpecializationData selectedCategory);
    }

    interface Presenter {

        void getSpecializationCategory(String cityId);

        void getSelectedDetails();

        void isCheck();
    }
}
