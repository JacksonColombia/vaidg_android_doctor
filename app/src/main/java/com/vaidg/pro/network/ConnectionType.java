package com.vaidg.pro.network;

/**
 * @author 3Embed.
 * @version 1.0.
 * @since 12/4/2017.
 */
public enum ConnectionType {
    WIFI, MOBILE, NOT_CONNECTED, GOOD, BAD;
}
