package com.vaidg.pro.dagger;


import android.app.Application;
import android.content.Context;
import dagger.Binds;
import dagger.Module;

import javax.inject.Singleton;


/**
 * <h1>AppModule</h1>
 * Used to inject the dependency
 *
 * @author 3Embed
 * @since 03-Nov-17
 */

@Module
abstract class AppModule {
    //expose Application as an injectable context
    @Singleton
    @Binds
    abstract Context bindContext(Application application);

}
