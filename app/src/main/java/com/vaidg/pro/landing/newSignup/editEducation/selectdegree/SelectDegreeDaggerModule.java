package com.vaidg.pro.landing.newSignup.editEducation.selectdegree;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class SelectDegreeDaggerModule {

    @ActivityScoped
    @Binds
    abstract SelectDegreeContract.Presenter providePresenter(SelectDegreePresenter presenter);

    @ActivityScoped
    @Binds
    abstract SelectDegreeContract.View provideView(SelectDegreeActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(SelectDegreeActivity activity);

}
