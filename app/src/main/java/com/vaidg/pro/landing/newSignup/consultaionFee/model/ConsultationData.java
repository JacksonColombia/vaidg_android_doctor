package com.vaidg.pro.landing.newSignup.consultaionFee.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ConsultationData implements Serializable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("currencyAbbr")
    @Expose
    private String currencyAbbr;
    @SerializedName("Fee")
    @Expose
    private String Fee;
    @SerializedName("isChecked")
    @Expose
    private boolean isChecked;
    @SerializedName("minFee")
    @Expose
    private double minFee;
    @SerializedName("maxFee")
    @Expose
    private double maxFee;

    public ConsultationData(String title) {
        this.title = title;
    }

    public double getMaxFee() {
        return maxFee;
    }

    public void setMaxFee(double maxFee) {
        this.maxFee = maxFee;
    }

    public double getMinFee() {
        return minFee;
    }

    public void setMinFee(double minFee) {
        this.minFee = minFee;
    }

    public String getFee() {
        return Fee;
    }

    public void setFee(String fee) {
        Fee = fee;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getCurrencyAbbr() {
        return currencyAbbr;
    }

    public void setCurrencyAbbr(String currencyAbbr) {
        this.currencyAbbr = currencyAbbr;
    }
}
