package com.vaidg.pro.landing.newSignup.selectSpecialization;

import android.app.Activity;

import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.landing.newSignup.selectCity.model.CitySelectionListener;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.landing.newSignup.selectSpecialization.model.SelectSpecializationModel;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationPojo;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.utility.Utility;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

/**
 * <h1>SelectSpecializationPresenter</h1>
 * <p>presenter for bussiness logic</p>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public class SelectSpecializationPresenter implements SelectSpecializationContract.Presenter, CitySelectionListener {

    @Inject
    SelectSpecializationContract.View view;

    @Inject
    SelectSpecializationModel model;

    @Inject
    NetworkService service;

    @Inject
    NetworkStateHolder networkStateHolder;

    @Inject
    Activity activity;

    @Inject
    Gson gson;

    private int position;

    @Inject
    public SelectSpecializationPresenter() {
    }

    @Override
    public void onItemSelected(int position) {
        this.position = position;
        model.selectCategory(position);
        if (view != null)
            view.notifyAdapter();
    }

    @Override
    public void onItemSelected(int position, String name) {
        model.selectCategory(position,name);
        if (view != null)
            view.notifyAdapter();
    }

    @Override
    public void getSpecializationCategory(String cityId) {
        if (networkStateHolder.isConnected()) {
            if (view != null)
                view.showProgress();
            Utility.basicAuth(service).getSpecializationCategory(model.getLanguage(), model.getPlatform(), cityId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            int statusCode = value.code();
                            try {
                                String response = value.body() != null
                                        ? value.body().string() : null;
                                String errorBody = value.errorBody() != null
                                        ? value.errorBody().string() : null;
                                if (statusCode == SUCCESS_RESPONSE) {
                                    SelectSpecializationPojo selectSpecializationPojo = gson.fromJson(response, SelectSpecializationPojo.class);
                                    model.parseSpecializationData(selectSpecializationPojo);
                                    if (view != null) {
                                        view.hideProgress();
                                        view.notifyAdapter();
                                    }
                                } else {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.showError(Utility.getMessage(errorBody));
                                    }
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null)
                                view.hideProgress();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void getSelectedDetails() {
        position = model.getPosition();
        SelectSpecializationData selectedCategory = model.getSelectedCategory(position);
        view.nextCategoryActivity(selectedCategory);
    }

    @Override
    public void isCheck() {
        if (model.isCheck())
            getSelectedDetails();
        else
            view.showError(activity.getString(R.string.specialization_selection_error));

    }

}