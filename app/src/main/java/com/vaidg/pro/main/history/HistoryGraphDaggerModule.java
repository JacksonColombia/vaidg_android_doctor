package com.vaidg.pro.main.history;

import android.app.Activity;

import com.vaidg.pro.bookingflow.addmedication.AddMedicationActivity;
import com.vaidg.pro.bookingflow.addmedication.AddMedicationContract;
import com.vaidg.pro.bookingflow.addmedication.AddMedicationPresenterImple;
import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>HistoryGraphDaggerModule</h1>
 * <p>This dagger module created for HistoryGraphActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/07/2020
 **/
@Module
public abstract class HistoryGraphDaggerModule {
    @ActivityScoped
    @Binds
    abstract Activity getActivity(HistoryGraphActivity activity);

    @ActivityScoped
    @Binds
    abstract HistoryGraphContract.View getView(HistoryGraphActivity activity);

    @ActivityScoped
    @Binds
    abstract HistoryGraphContract.Presenter getPresenter(HistoryGraphPresenterImple presenterImple);
}
