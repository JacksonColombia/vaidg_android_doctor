package com.vaidg.pro.network;

import android.content.Context;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.vaidg.pro.BuildConfig;
import com.vaidg.pro.utility.SSLCertificate;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.vaidg.pro.utility.ServiceUrl.HOST_URL;

/**
 * Created by Pramod on 15/12/17.
 */
public class ServiceFactory {

  private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
      .readTimeout(50, TimeUnit.SECONDS)
      .writeTimeout(30, TimeUnit.SECONDS)
      .connectTimeout(80, TimeUnit.SECONDS)
      .retryOnConnectionFailure(true)
      .addInterceptor(getLoggingInterceptor())
      .build();

  /**
   * Creates a retrofit service from an arbitrary class (clazz)
   *
   * @return retrofit service with defined endpoint
   */
  public static <T> T  getClient(final Class<T> clazz,Context context) {
    okHttpClient = //SSLCertificate.getSSLCertificate(context).newBuilder()
        new OkHttpClient().newBuilder()
        .readTimeout(50, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)
        .connectTimeout(80, TimeUnit.SECONDS)
        .retryOnConnectionFailure(false)
        .addInterceptor(getLoggingInterceptor())
        .build();
    return new Retrofit.Builder()
        .baseUrl(HOST_URL)
        .client(okHttpClient)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(clazz);
  }


  public static HttpLoggingInterceptor getLoggingInterceptor() {
    HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
    httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    return httpLoggingInterceptor;
  }
}
