package com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.dagger.FragmentScoped;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.model.DocumentSelectionAdapter;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.utility.DatePickerCommon;
import com.vaidg.pro.utility.MediaBottomSelector;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;

/**
 * <h1>DocumentSelectionUtil</h1>
 * <p>This dagger util is used to provide object reference to injected objects when declare in activity.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
@Module
public class DocumentSelectionUtil {

    @FragmentScoped
    @Provides
    List<PreDefined> getPreDefineds() {
        return new ArrayList<>();
    }

    @FragmentScoped
    @Provides
    DocumentSelectionAdapter getDocumentSelectionAdapter(List<PreDefined> arrayList) {
        return new DocumentSelectionAdapter(arrayList);
    }

    @FragmentScoped
    @Provides
    DatePickerCommon provideDatePickerCommon() {
        return new DatePickerCommon();
    }
}
