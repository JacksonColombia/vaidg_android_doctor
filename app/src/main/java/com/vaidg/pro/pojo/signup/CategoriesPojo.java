package com.vaidg.pro.pojo.signup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 */

public class CategoriesPojo implements Serializable {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<CategoriesData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CategoriesData> getData() {
        return data;
    }

    public void setData(ArrayList<CategoriesData> data) {
        this.data = data;
    }

}
