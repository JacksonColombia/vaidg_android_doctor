package com.vaidg.pro.main.schedule.bookingschedule;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.booking.CancelPojo;
import com.vaidg.pro.pojo.shedule.ScheduleBookngPojo;
import org.json.JSONObject;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface BookingScheculeContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onFailure(String msg);

    void onFailure();

    void onSuccess(ScheduleBookngPojo scheduleBookngPojo);

    void onSuccessCancelReason(CancelPojo cancelPojo);

    void onCancelBooking(String msg);
  }

  interface Presenter extends BasePresenter {
    /**
     * Method for calling api for getting the cancel reasion
     */
    void getCancelReason(String bookingId);
    /**
     * method for passing value from view to model
     *
     * @param bookingId    booking id
     */
    void getBooking(String bookingId);
    /**
     * Method for calling api for update the job status
     *
     * @param jsonObject
     */
    void cancelBooking(JSONObject jsonObject);
  }
}