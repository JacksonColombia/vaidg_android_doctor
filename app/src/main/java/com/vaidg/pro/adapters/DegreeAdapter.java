package com.vaidg.pro.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.utility.VariableConstant;

/**
 * DegreeAdapter adapter to list out the degrees of Doctor
 */
public class DegreeAdapter extends RecyclerView.Adapter<DegreeAdapter.MyViewHolder> {
    @NonNull
    @Override
    public DegreeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_specialization_selection, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull DegreeAdapter.MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return VariableConstant.ZERO;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
