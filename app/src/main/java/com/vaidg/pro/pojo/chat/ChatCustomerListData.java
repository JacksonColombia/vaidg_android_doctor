package com.vaidg.pro.pojo.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 10-Apr-18.
 */

public class ChatCustomerListData implements Serializable {
    @SerializedName("past")
    @Expose
    private ArrayList<ChatCutomerList> past;
    @SerializedName("accepted")
    @Expose
    private ArrayList<ChatCutomerList> accepted;

    public ArrayList<ChatCutomerList> getPast() {
        return past;
    }

    public void setPast(ArrayList<ChatCutomerList> past) {
        this.past = past;
    }

    public ArrayList<ChatCutomerList> getAccepted() {
        return accepted;
    }

    public void setAccepted(ArrayList<ChatCutomerList> accepted) {
        this.accepted = accepted;
    }
}
