package com.vaidg.pro.main.schedule;

import static com.vaidg.pro.AppController.getContext;
import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.format.DateFormat;
import android.util.Log;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.main.schedule.addschedule.ScheculeAddContract;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.shedule.Booked;
import com.vaidg.pro.pojo.shedule.Schedule;
import com.vaidg.pro.pojo.shedule.ScheduleMonthData;
import com.vaidg.pro.pojo.shedule.ScheduleMonthPojo;
import com.vaidg.pro.pojo.shedule.Slot;
import com.vaidg.pro.utility.OkHttp3ConnectionStatusCode;
import com.vaidg.pro.utility.RefreshToken;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.ServiceUrl;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 23-Oct-17.
 * <h1>ScheduleFragmentPresenter</h1>
 * ScheduleFragmentPresenter presenter for ScheduleFragment
 *
 * @see ScheduleFragment
 */

public class ScheduleFragmentPresenter implements ScheculeFragmentContract.Presenter {

    private SimpleDateFormat serverFormat;
    private SimpleDateFormat  displayHourFormat;
    private SimpleDateFormat displayHourFormatInBooked;
    private SimpleDateFormat displayPeriodFormat;
    private boolean  isCurrentMonth = false;
    private String scheduleData = "";
    @Inject
    Context context;
    private ScheculeFragmentContract.View view;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;

    @Inject
    public ScheduleFragmentPresenter() {
    }


    /**
     * methof for passing value from view to model
     *
     * @param date         date of selected month
     */
    @Override
    public void getSchedule(String date, boolean isCurrentMonth) {
       // this.isCurrentMonth = isCurrentMonth;
        if(view!=null)
            view.stopProgressBar();

        /*if (!isCurrentMonth || sessionManager.getScheduleData().equals("")) {
            if(view!=null)
                view.startProgressBar();
        } else {
           // sessionManager.getScheduleData();
            if(view!=null)
                view.stopProgressBar();

            if (scheduleData.equals(sessionManager.getScheduleData())) {
                return;
            }
            scheduleData = sessionManager.getScheduleData();
            if (this.isCurrentMonth) {
                sessionManager.setScheduleData(sessionManager.getScheduleData());
            }
            ScheduleMonthPojo scheduleMonthPojo = gson.fromJson(sessionManager.getScheduleData(), ScheduleMonthPojo.class);
                view.onSuccessGetSchedule(scheduleMonthPojo);
                //  model.getShedule(sessionToken, date);
        }
*/
        service.getScheduleMonth(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, date)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if(view!=null)
                                    view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    ScheduleMonthPojo scheduleMonthPojo = gson.fromJson(response, ScheduleMonthPojo.class);
                                    if(view!=null)
                                        view.onSuccessGetSchedule(scheduleMonthPojo);
                                } else {
                                    if(view!=null)
                                        view.onFailure();
                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        if(view!=null)
                                            view.stopProgressBar();
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        getSchedule(date, isCurrentMonth);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        view.onFailure();
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        getInstance().toast(Utility.getMessage(msg));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                                if(view!=null)
                                view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                Utility.logoutSessionExiperd(sessionManager, context);
                                break;
                            default:
                                if(view!=null)
                                    view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                view.onFailure(Utility.getMessage(errorBody));
                                break;
                        }
                    } catch (Exception e) {
                        if(view!=null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if(view!=null) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                }

                @Override
                public void onComplete() {
                }
            });


    }
    /**
     * Returing the created events based on the sheduleMontDatas
     * create blue color for schedule day and differrent color for booked color
     *
     * @param scheduleMonthDatas list of scheduleMonthDatas
     * @param calendarEvents     List of calendar events
     * @return calendarEvents
     */
    @Override
   public ArrayList<Event> getEvents(ArrayList<ScheduleMonthData> scheduleMonthDatas, ArrayList<Event> calendarEvents) {
        for (ScheduleMonthData scheduleMonthData : scheduleMonthDatas) {
            for (Schedule schedule : scheduleMonthData.getSchedule()) {
                Event event;
                if (schedule.getBooked().size() == 0) {
                    //color primary for created schedule
                    event = new Event(Color.parseColor("#01B5F6"), Utility.convertUTCToTimeStamp(scheduleMonthData.getDate()), schedule);
                } else {
                    if (!schedule.getBooked().get(schedule.getBooked().size() - 1).getStatus().equals(VariableConstant.JOB_COMPLETED_RAISE_INVOICE)) {
                        //color green for upcoming and ongoing shedule booking,
                        event = new Event(Color.parseColor("#5BC24F"), Utility.convertUTCToTimeStamp(scheduleMonthData.getDate()), schedule);
                    } else {
                        //color red for completed booking
                        event = new Event(Color.parseColor("#EB4942"), Utility.convertUTCToTimeStamp(scheduleMonthData.getDate()), schedule);
                    }
                }
                calendarEvents.add(event);
            }
        }
        return calendarEvents;
    }

    /**
     * Create slot for Each based on booked and arrange according to the time
     *
     * @param eventList calendar event list
     * @return list of Slot
     */

    @Override
    public ArrayList<Slot> createSlotFromSchedules(List<Event> eventList) {
        ArrayList<Slot> slots = new ArrayList<>();
        serverFormat= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        //displayHourFormat = new SimpleDateFormat(DateFormat.is24HourFormat(getContext()) ? "HH:mm" : "hh:mm", Locale.US);
        displayHourFormat = new SimpleDateFormat("hh:mm", Locale.US);
         displayHourFormatInBooked= new SimpleDateFormat("hh:mm a", Locale.US);
         displayPeriodFormat= new SimpleDateFormat("a", Locale.US);

        serverFormat.setTimeZone(getInstance().getTimeZone());
        displayHourFormat.setTimeZone(getInstance().getTimeZone());
        displayHourFormatInBooked.setTimeZone(getInstance().getTimeZone());
        displayPeriodFormat.setTimeZone(getInstance().getTimeZone());
        ArrayList<Schedule> schedules = new ArrayList<>();
        for (Event event : eventList) {
            schedules.add((Schedule) event.getData());
        }

        try {
            for (Schedule schedule : schedules) {
                Slot slotStartTime = new Slot();
                slotStartTime.setSlotHour(displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(schedule.getStartTime()))));
                slotStartTime.setSlotPeriod(displayPeriodFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(schedule.getStartTime()))));
                slotStartTime.setStartTimeStamp(schedule.getStartTime());
                slots.add(slotStartTime);
                if (schedule.getBooked().size() > 0) {
                    for (Booked booked : schedule.getBooked()) {
                        Slot bookedSlot = new Slot();
                        bookedSlot.setStatus(booked.getStatus());
                        bookedSlot.setBookingId(booked.getBookingId());
                        bookedSlot.setCustomerId(booked.getCustomerId());
                        bookedSlot.setSlotHour(displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booked.getStart()))));
                        bookedSlot.setSlotPeriod(displayPeriodFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booked.getStart()))));
                        bookedSlot.setSlotEndHourBooking(displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booked.getEnd()))));
                        bookedSlot.setSlodp_10eriodBooking(displayPeriodFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booked.getEnd()))));
                        bookedSlot.setBookedStartHour(displayHourFormatInBooked.format(serverFormat.parse(Utility.convertUTCToServerFormat(booked.getStart()))));
                        bookedSlot.setBookedEndHour(displayHourFormatInBooked.format(serverFormat.parse(Utility.convertUTCToServerFormat(booked.getEnd()))));
                        bookedSlot.setEvent(booked.getEvent());
                        bookedSlot.setCutomerName(booked.getFirstName() + " " + booked.getLastName());
                        bookedSlot.setStartTimeStamp(booked.getStart());
                        slots.add(bookedSlot);
                    }
                }
                Slot slotEndTime = new Slot();
                slotEndTime.setSlotHour(displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(schedule.getEndTime()))));
                slotEndTime.setSlotPeriod(displayPeriodFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(schedule.getEndTime()))));
                slotEndTime.setStartTimeStamp(schedule.getEndTime());
                slots.add(slotEndTime);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        Collections.sort(slots, new Comparator<Slot>() {
            @Override
            public int compare(Slot o1, Slot o2) {
                return o1.getStartTimeStamp().compareTo(o2.getStartTimeStamp());
            }
        });

        //Remove repeated Slot time (i.e) if 5 to 6 and 6 to 8 then show only 5 6 8 no need to show 6 again
       /*

       ArrayList<Integer> removablePosition = new ArrayList<>();
        int j = 0;
        for( int i=0 ; i < slots.size()-1 ; i++)
        {
            if(slots.get(i).getSlotHour().equals(slots.get(i+1).getSlotHour()))
            {
                removablePosition.add(i);
            }
        }

        for(int i = 0 ; i < removablePosition.size() ; i++)
        {
            slots.remove((int)removablePosition.get(i) - j);
            j++;
        }*/

        return slots;
    }

    @Override
    public void attachView(ScheculeFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        view = null;
    }
}
