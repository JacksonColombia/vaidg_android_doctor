package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AccountRazorPayResponse implements Parcelable
{

	@SerializedName("message")
	@Expose
	private String message;
	@SerializedName("data")
	@Expose
	private ArrayList<AccountRazorPayData> data = null;
	public final static Creator<AccountRazorPayResponse> CREATOR = new Creator<AccountRazorPayResponse>() {


		@SuppressWarnings({
				"unchecked"
		})
		public AccountRazorPayResponse createFromParcel(Parcel in) {
			return new AccountRazorPayResponse(in);
		}

		public AccountRazorPayResponse[] newArray(int size) {
			return (new AccountRazorPayResponse[size]);
		}

	}
			;

	protected AccountRazorPayResponse(Parcel in) {
		this.message = ((String) in.readValue((String.class.getClassLoader())));
		in.readList(this.data, (AccountRazorPayData.class.getClassLoader()));
	}

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public AccountRazorPayResponse() {
	}

	/**
	 *
	 * @param data
	 * @param message
	 */
	public AccountRazorPayResponse(String message, ArrayList<AccountRazorPayData> data) {
		super();
		this.message = message;
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArrayList<AccountRazorPayData> getData() {
		return data;
	}

	public void setData(ArrayList<AccountRazorPayData> data) {
		this.data = data;
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(message);
		dest.writeList(data);
	}

	public int describeContents() {
		return 0;
	}

}