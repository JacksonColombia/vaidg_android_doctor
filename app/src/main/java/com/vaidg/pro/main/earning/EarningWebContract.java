package com.vaidg.pro.main.earning;

import com.vaidg.pro.BasePresenter;

public interface EarningWebContract {

    interface Presenter extends BasePresenter {

        /**
         * <p>This method is get provide id from the session manager and return it here.</p>
         * @return the provider id
         */
        String getProviderId();
    }
}
