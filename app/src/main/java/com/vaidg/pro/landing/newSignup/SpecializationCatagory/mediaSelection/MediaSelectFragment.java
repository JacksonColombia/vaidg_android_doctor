package com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.google.gson.Gson;
import com.vaidg.pro.BuildConfig;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.FragmentMediaSelectBinding;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.SpecializationCategoryContract;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection.model.MediaSelectAdapter;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection.model.MediaSelectItemCallBack;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.DialogHelper;
import com.vaidg.pro.utility.FileUtils;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.android.support.DaggerFragment;

import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_PICTURE_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_VIDEO_UPLOAD;

/**
 * <h1>@MediaSelectFragment</h1>
 * <p>this fragment is used to display picture , video and document upload
 * this fragment is reused for type 12,13,14
 * </P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class MediaSelectFragment extends DaggerFragment implements MediaSelectContract.View {

    private static final String TAG = MediaSelectFragment.class.getSimpleName();

    @Inject
    Activity activity;
    @Named("filePath")
    @Inject
    ArrayList<String> arrayList;
    @Inject
    SpecializationCategoryContract.Presenter mainPresenter;
    @Inject
    MediaSelectContract.Presenter presenter;
    @Inject
    App_permission app_permission;
    @Inject
    Utility utility;

    private Attribute currentItem;
    private int list_number;
    private FragmentMediaSelectBinding binding;
    private MediaSelectAdapter adapter;
    private int type;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        assert bundle != null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentMediaSelectBinding.inflate(inflater);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRv();
        getData();
        initViews();
        setProgressBar();
        initOnClickListener();
    }

    private void getData() {
    }

    private void initRv() {
        adapter = new MediaSelectAdapter(currentItem.getType(), arrayList);
        adapter.provideCallBack((MediaSelectItemCallBack) presenter);
        binding.rvMediaSelect.setAdapter(adapter);
    }

    private void initOnClickListener() {
        binding.includeToolbar.btnDone.setOnClickListener(this::onclick);
        binding.btnMediaSelectUpload.setOnClickListener(this::onclick);
    }

    private void onclick(View view) {
        switch (view.getId()) {
            case R.id.btnDone:
                presenter.isUpload();
                break;
            case R.id.btnMediaSelectUpload:
                int count = 0;
                if(currentItem.getMaximum() != null && !currentItem.getMaximum().isEmpty() && TextUtils.isDigitsOnly(currentItem.getMaximum()))
                {
                    count = Integer.parseInt(currentItem.getMaximum());
                }
                if(adapter.getItemCount() == count) {
                    DialogHelper.showWaringMessage(getContext(), getContext().getResources().getString(R.string.maxUploadLimit));
                }else{
                    switch (type) {
                        case SPECIALIZATION_VIEW_PICTURE_UPLOAD:
                        case SPECIALIZATION_VIEW_VIDEO_UPLOAD:
                            presenter.openChooser(type);
                            break;
                    }
                }
                break;
        }
    }

    private void setProgressBar() {
        binding.progressBar.setMax(currentItem.getMax_count());
        binding.progressBar.setProgress(currentItem.getPosition() + 1);
    }

    /**
     * <h2>Setting the current preference.</h2>
     *
     * @param item
     */
    public void setPreferenceItem(Attribute item) {
        currentItem = item;
        list_number = currentItem.getList_no();
        type = currentItem.getType();
    }

    public void initViews() {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        binding.includeToolbar.tvTitle.setText(getString(R.string.complete_your_profile));
        binding.tvMediaSelectTitle.setText(currentItem.getQueForProviderSignup());
        binding.includeToolbar.btnDone.setText(getString(R.string.next));
        if (activity != null) {
            activity.setSupportActionBar(binding.includeToolbar.toolbar);
            if (activity.getSupportActionBar() != null) {
                activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
                activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
                binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> activity.onBackPressed());
            }
        }
    }

    @Override
    public void notifyAdapter() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.attachView(this);
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public void selectDoc(Uri uri_path) {
        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
        chooseFile.setType("application/pdf");
        startActivityForResult(chooseFile, VariableConstant.PICKFILE_RESULT_CODE);
    }

    @Override
    public boolean isMandatoryForProvider() {
        return currentItem.getMandatoryForProvider();
    }

    @Override
    public void moveNextFrag() {
        presenter.saveData(currentItem);
        mainPresenter.openNextFrag(list_number, currentItem.getPosition() + 1, currentItem);
    }

    @Override
    public void openGallery(int type) {
        String chooseTitle = "";
        Intent intent = new Intent();
        if (type == SPECIALIZATION_VIEW_VIDEO_UPLOAD) {
            intent.setType("video/*");
            chooseTitle = "Select Video";
        } else {
            intent.setType("image/*");
            chooseTitle = "select Image";
        }
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, chooseTitle), VariableConstant.GALLERY_CODE);
    }

    @Override
    public void openCamera(Uri uri, int type) {
        Intent intent;
        if (type == SPECIALIZATION_VIEW_VIDEO_UPLOAD)
            intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        else
            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, VariableConstant.CAMERA_CODE);
    }

    @Override
    public void showError(String message) {
        mainPresenter.showError(message);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VariableConstant.CAMERA_CODE && resultCode == Activity.RESULT_OK) {
            if (presenter.isValidMediaSize()) {
                presenter.upDateToGallery();
                presenter.addMedia(new File(presenter.getRecentTemp()));
            }

        } else if (requestCode == VariableConstant.GALLERY_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null && data.getData() != null) {
                Uri uri = data.getData();
                String file_path = FileUtils.getPath(activity, uri);
                if (!Utility.isTextEmpty(file_path)) {
                    if (presenter.isValidMediaSize(file_path)) {
                        presenter.addMedia(new File(file_path));
                    }
                } else {
                    showError("file_path_error");
                }
            } else {
                showError("file_path_error");
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void launchViewer(String filePath) {
        MimeTypeMap myMime = MimeTypeMap.getSingleton();
        Intent newIntent = new Intent(Intent.ACTION_VIEW);
        newIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        String mimeType = myMime.getMimeTypeFromExtension(fileExt(filePath).substring(1));
        newIntent.setDataAndType(FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", new File(filePath)), mimeType);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            startActivity(newIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getContext(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }
}

