package com.vaidg.pro.landing.newSignup.findHospital;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

@Module
public interface FindHospitalDaggerModule {

    @ActivityScoped
    @Binds
    FindHospitalContract.Presenter providePresenter(FindHospitalPresenter presenter);

    @ActivityScoped
    @Binds
    FindHospitalContract.View provideView(FindHospitalActivity activity);

    @ActivityScoped
    @Binds
    Activity provideActivity(FindHospitalActivity activity);
}
