package com.vaidg.pro.pojo.signup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 14-Nov-17.
 */

public class CategoryDocument implements Serializable {
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("Service_category_id")
    @Expose
    private String Service_category_id;
    @SerializedName("docName")
    @Expose
    private String docName;
    @SerializedName("isManadatory")
    @Expose
    private String isManadatory;
    @SerializedName("field")
    @Expose
    private ArrayList<CategoryDocumentField> field;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getService_category_id() {
        return Service_category_id;
    }

    public void setService_category_id(String service_category_id) {
        Service_category_id = service_category_id;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getIsManadatory() {
        return isManadatory;
    }

    public void setIsManadatory(String isManadatory) {
        this.isManadatory = isManadatory;
    }

    public ArrayList<CategoryDocumentField> getField() {
        return field;
    }

    public void setField(ArrayList<CategoryDocumentField> field) {
        this.field = field;
    }
}
