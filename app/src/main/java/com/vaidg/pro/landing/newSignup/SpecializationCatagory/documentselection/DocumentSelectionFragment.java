package com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.vaidg.pro.BuildConfig;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.FragmentDocumentSelectionBinding;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.SpecializationCategoryContract;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.model.DocumentSelectionAdapter;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.model.DocumentSelectionModel;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.model.OnDocumentSelectionItemClickListener;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.DatePickerCommon;
import com.vaidg.pro.utility.FileUtils;
import com.vaidg.pro.utility.MediaBottomSelector;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.fileUtil.AppFileManger;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

import static com.vaidg.pro.utility.VariableConstant.CAMERA_CODE;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_VIDEO_UPLOAD;

public class DocumentSelectionFragment extends DaggerFragment implements DocumentSelectionContract.View, OnDocumentSelectionItemClickListener, App_permission.Permission_Callback, DatePickerCommon.DateSelected, MediaBottomSelector.Callback {

    private static final String TAG = DocumentSelectionFragment.class.getSimpleName();

    private static final String DOC_PICK = "doc_pick";
    private static final String CAMERA = "camera";

    @Inject
    Activity activity;
    @Inject
    SpecializationCategoryContract.Presenter mainPresenter;
    @Inject
    DocumentSelectionContract.Presenter presenter;
    @Inject
    DocumentSelectionAdapter documentSelectionAdapter;
    @Inject
    App_permission app_permission;
    @Inject
    Utility utility;
    @Inject
    DatePickerCommon datePickerFragment;
    @Inject
    MediaBottomSelector mediaBottomSelector;

    private Attribute currentItem;
    private int list_number;
    private FragmentDocumentSelectionBinding binding;

    private int clickedPosition;
    private File temp_file = null;
    @Inject
    AppFileManger appFileManger;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        assert bundle != null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDocumentSelectionBinding.inflate(inflater);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setProgressBar();
        initOnClickListener();
    }

    public void initViews() {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        binding.includeToolbar.tvTitle.setText(getString(R.string.complete_your_profile));
        binding.tvMediaSelectTitle.setText(currentItem.getQueForProviderSignup());
        binding.includeToolbar.btnDone.setText(getString(R.string.next));
        if (activity != null) {
            activity.setSupportActionBar(binding.includeToolbar.toolbar);
            if (activity.getSupportActionBar() != null) {
                activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
                activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
                binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> {
                    presenter.clearData();
                    activity.onBackPressed();
                });
            }
        }
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this.activity, LinearLayoutManager.VERTICAL);
        itemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_light, null));
        binding.rvDocumentSelection.addItemDecoration(itemDecoration);
        documentSelectionAdapter.setOnDocumentSelectionItemClickListener(this);
        binding.rvDocumentSelection.setAdapter(documentSelectionAdapter);
        presenter.addPreDefinedData(currentItem.getPreDefined());

        datePickerFragment.setCallBack(this);

    }

    private void initOnClickListener() {
        binding.includeToolbar.btnDone.setOnClickListener(this::onclick);
    }

    private void onclick(View view) {
        if (view.getId() == R.id.btnDone) {
            if (presenter.isMandatoryFilled()) {
                moveNextFrag();
            }
        }
    }

    @Override
    public void setPreferenceItem(Attribute item) {
        currentItem = item;
        list_number = currentItem.getList_no();
    }

    @Override
    public boolean isMandatoryForProvider() {
        return currentItem.getMandatoryForProvider();
    }

    @Override
    public void notifyAdapter() {
        documentSelectionAdapter.notifyDataSetChanged();
    }

    @Override
    public void notifyAdapter(int position, int listSize) {
        documentSelectionAdapter.notifyItemChanged(position, listSize);
    }


    @Override
    public void showError(String error) {
        mainPresenter.showError(error);
    }

    @Override
    public void launchViewer(String filePath) {
        File file = new File(filePath);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        MimeTypeMap myMime = MimeTypeMap.getSingleton();
        String mimeType = myMime.getMimeTypeFromExtension(FileUtils.getExtension(filePath));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri apkURI = FileProvider.getUriForFile(activity.getApplicationContext(), BuildConfig.APPLICATION_ID + ".provider", file);
            intent.setDataAndType(apkURI, mimeType);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            intent.setDataAndType(Uri.fromFile(file), mimeType);
        }

        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            showError("No handler for this type of file.");
        }
    }


    private void setProgressBar() {
        binding.progressBar.setMax(currentItem.getMax_count());
        binding.progressBar.setProgress(currentItem.getPosition() + 1);
    }


    @Override
    public void openDocPicker() {
        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
        String[] supportedTypes = {"application/msword",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "application/pdf",
                "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "image/*"};
        chooseFile.setType("*/*");
        chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, supportedTypes);
        startActivityForResult(chooseFile, VariableConstant.PICKFILE_RESULT_CODE);
    }

    @Override
    public void openDatePicker(int type) {
        if (!datePickerFragment.isResumed()) {
            datePickerFragment.setDatePickerType(type == DocumentSelectionModel
                    .DATE_TIME_CURRENT_TO_FUTURE ? 3 : 4);
            datePickerFragment.show(getChildFragmentManager(), "dataPicker");
        }
//        final Calendar c = Calendar.getInstance();
//        long currentTimeStamp = new GregorianCalendar(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH))
//                .getTimeInMillis();
//        DatePickerDialog datePickerDialog = new DatePickerDialog(Objects.requireNonNull(getContext()),
//                (view, year, monthOfYear, dayOfMonth) -> presenter.setData(clickedPosition, String.format(Locale.US, "%02d-%02d-%02d", year, (monthOfYear + 1), dayOfMonth)), c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
//        datePickerDialog.show();
//        DatePicker dp = datePickerDialog.getDatePicker();
//        switch (type) {
//            case DocumentSelectionModel
//                    .DATE_TIME_CURRENT_TO_FUTURE:
//                dp.setMinDate(currentTimeStamp);
//                break;
//            case DocumentSelectionModel
//                    .DATE_TIME_PAST_TO_CURRENT:
//                dp.setMaxDate(currentTimeStamp);
//                break;
//        }
    }

    @Override
    public void onDateSelected(String sendingFormat, String displayFormat) {
        presenter.setData(clickedPosition, sendingFormat);
    }

    @Override
    public void moveNextFrag() {
        presenter.saveData(currentItem);
        mainPresenter.openNextFrag(list_number, currentItem.getPosition() + 1, currentItem);
    }

    @Override
    public void onItemClick(int position, PreDefined preDefined) {
        clickedPosition = position;
        switch (preDefined.getType()) {
            case DocumentSelectionModel.UPLOAD_DOCUMENT:
                if (preDefined.getData() != null && !preDefined.getData().isEmpty()) {
                    presenter.deleteDocument(position);
                } else {
                    temp_file = null;
                    mediaBottomSelector.showBottomSheet(this);
                  /*  ArrayList<App_permission.Permission> permissions = new ArrayList<>();
                    permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
                    permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
                    app_permission.getPermission_for_Sup_v4Fragment(DOC_PICK, permissions, this, this);*/
                }
                break;
            case DocumentSelectionModel.DATE_TIME_CURRENT_TO_FUTURE:
            case DocumentSelectionModel.DATE_TIME_PAST_TO_CURRENT:
                Utility.hideKeyboad(activity);
                openDatePicker(preDefined.getType());
                break;
        }
    }

    @Override
    public void onDocumentClick(int position, PreDefined preDefined) {
        launchViewer(preDefined.getData());
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        if (isAllGranted && tag.equals(DOC_PICK)) {
            openDocPicker();
        }else if (isAllGranted && tag.equals(CAMERA)) {
                try {
                    temp_file = appFileManger.getImageFile();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    }
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, utility.getUri_Path(temp_file));
                    startActivityForResult(intent,CAMERA_CODE);
                } catch (Exception e) {
                    showError(e.getMessage());
                }
        }
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        if (tag.equals(DOC_PICK)) {
            app_permission.show_Alert_Permission(getActivity().getString(R.string.storage_access_denied), getActivity().getString(R.string.storage_denied_subtitle),
                    getActivity().getString(R.string.gallery_acess_message), stringArray);
        }else if (tag.equals(CAMERA)) {
            app_permission.show_Alert_Permission(getActivity().getString(R.string.camera_access_text), getActivity().getString(R.string.camera_acess_subtitle),
                    getActivity().getString(R.string.camera_acess_message), stringArray);
        }
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean permanent) {
        if (permanent) {
            if (tag.equals(DOC_PICK)) {
                app_permission.showAlertDeniedPermission(getActivity().getString(R.string.storage_access_denied), getActivity().getString(R.string.storage_denied_subtitle),
                        getActivity().getString(R.string.gallery_denied_message));
            }else if (tag.equals(CAMERA)) {
                app_permission.showAlertDeniedPermission(getActivity().getString(R.string.camera_denied_text), getActivity().getString(R.string.camera_denied_subtitle),
                        getActivity().getString(R.string.camera_denied_message));
            }
        }

    }
    public void upDateToGallery() {
        if (temp_file == null)
            return;
      /*  Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(temp_file.getPath());
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);*/
        //Log.d(TAG, "upDateToGallery: 2"+temp_file.getPath()+ "=====>\n"+temp_file.getAbsolutePath());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            final Uri contentUri = Uri.fromFile(temp_file);
            scanIntent.setData(contentUri);
            getActivity().sendBroadcast(scanIntent);
        } else {
            getActivity().sendBroadcast(
                    new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(temp_file.getAbsolutePath())));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == VariableConstant.PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null && data.getData() != null) {
                Uri uri = data.getData();
                String file_path = FileUtils.getPath(activity, uri);
                if (!Utility.isTextEmpty(file_path)) {
                    if (presenter.isValidDocumentSize(file_path)) {
                        presenter.setData(clickedPosition, file_path);
                    }
                } else {
                    showError(getString(R.string.errorDocumentPicker));
                }
            } else {
                showError(getString(R.string.errorDocumentPicker));
            }
        }else if (requestCode == VariableConstant.CAMERA_CODE && resultCode == Activity.RESULT_OK) {
            if (presenter.isValidDocumentSize(temp_file.getPath())) {
                upDateToGallery();
                presenter.setData(clickedPosition, temp_file.getPath());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.attachView(this);
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public void onCamera() {
        ArrayList<App_permission.Permission> permissions = new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.CAMERA);
        app_permission.getPermission_for_Sup_v4Fragment(CAMERA, permissions, this, this);
    }

    @Override
    public void onGallery() {
        ArrayList<App_permission.Permission> permissions = new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
        app_permission.getPermission_for_Sup_v4Fragment(DOC_PICK, permissions, this, this);
    }
}