package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.pojo.addMedication.Direction;
import com.vaidg.pro.pojo.addMedication.DrugRoute;
import com.vaidg.pro.pojo.addMedication.Frequency;
import com.vaidg.pro.pojo.addMedication.Unit;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import org.jetbrains.annotations.NotNull;

import static androidx.core.content.ContextCompat.*;
import static com.vaidg.pro.utility.Utility.getFontBold;
import static com.vaidg.pro.utility.Utility.getFontMedium;
import static com.vaidg.pro.utility.Utility.getFontRegular;
import static com.vaidg.pro.utility.Utility.pixelsToSp;

/**
 * Created by murashid on 29-Sep-17.
 * <h1>AddressListAdapter</h1>
 * Recycler View Adapter for Address List for selecting address during schedule
 *
 * @see com.vaidg.pro.address.AddressListActivity
 */
public class SelectMedicationListAdapter extends RecyclerView.Adapter<SelectMedicationListAdapter.ViewHolder> {
  private Context mContext;
  private ArrayList<Unit> unitArrayList = new ArrayList<>();
  private boolean isFromFrequency = false,isFromUnit = false,isFromDirection = false,isFromSelectDate = false,isFromDrugRoute = false,isFromDuration = false,isFromInstruction = false;
  private ArrayList<Frequency> frequencyArrayList = new ArrayList<>();
  private ArrayList<Direction> directionArrayList = new ArrayList<>();
  private ArrayList<DrugRoute> drugRouteArrayList = new ArrayList<>();
  private OnItemClickListener listener;
  private String mDrugRouteName = "", mDirectionName = "", mFrequencyName = "", mUnitName = "",mDurationName = "",mInstruction ="";


  public SelectMedicationListAdapter(ArrayList<Frequency> frequencyArrayList, ArrayList<Unit> unitArrayList, ArrayList<DrugRoute> drugRouteArrayList, ArrayList<Direction> directionArrayList, String mInstruction, boolean isFromInstruction, boolean isFromDirection, boolean isFromDrugRoute, boolean isFromUnit, boolean isFromDuration, boolean isFromFrequency, String mDirectionName, String mDurationName, String mDrugRouteName, String mFrequencyName, String mUnitName, boolean isFromSelectDate, OnItemClickListener listener) {
    this.unitArrayList = unitArrayList;
    this.frequencyArrayList = frequencyArrayList;
    this.directionArrayList = directionArrayList;
    this.drugRouteArrayList = drugRouteArrayList;
    this.isFromFrequency = isFromFrequency;
    this.isFromUnit = isFromUnit;
    this.isFromDirection = isFromDirection;
    this.isFromDrugRoute = isFromDrugRoute;
    this.isFromDuration = isFromDuration;
    this.isFromInstruction = isFromInstruction;
    this.mDrugRouteName = mDrugRouteName;
    this.mDirectionName = mDirectionName;
    this.mFrequencyName = mFrequencyName;
    this.mUnitName = mUnitName;
    this.mDurationName = mDurationName;
    this.mInstruction = mInstruction;
    this.isFromSelectDate = isFromSelectDate;
    this.listener = listener;
  }

  @NotNull
  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_selectmedication, parent, false);
    mContext = parent.getContext();
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NotNull ViewHolder holder, int position) {
    if(isFromUnit) {
      holder.tvCompoundName.setText(unitArrayList.get(position).getName());
      if(unitArrayList.get(position).isChecked())
      {
        check(holder);
      }else{
        uncheck(holder);
      }
    } else if(isFromDirection) {
      holder.tvCompoundName.setText(directionArrayList.get(position).getName());
      if(directionArrayList.get(position).isChecked())
      {
        check(holder);
      }else{
        uncheck(holder);
      }
    } else if(isFromDrugRoute) {
      holder.tvCompoundName.setText(drugRouteArrayList.get(position).getName());
      if(drugRouteArrayList.get(position).isChecked())
      {
        check(holder);
      }else{
        uncheck(holder);
      }
    } else if(isFromFrequency) {
      holder.tvCompoundName.setText(frequencyArrayList.get(position).getName());
      if(frequencyArrayList.get(position).isChecked())
      {
        check(holder);
      }else{
        uncheck(holder);
      }
    }else if(isFromInstruction) {
      holder.etInstruction.setVisibility(View.VISIBLE);
      holder.tvCompoundName.setTypeface(holder.fontBold);
      holder.tvCompoundName.setText("Add Instruction");
      if(mInstruction != null && !mInstruction.isEmpty()) {
        holder.etInstruction.setText(mInstruction);
      }
      holder.etInstruction.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
          if (listener != null) {
            listener.onInstructionItemClick(s.toString());
          }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
      });

    }else if(isFromDuration)
    {
      holder.lvDuration.setVisibility(View.VISIBLE);
      holder.tvCompoundName.setTypeface(holder.fontBold);
      holder.tvCompoundName.setText("Add Duration");
      if(mDurationName != null && !mDurationName.isEmpty()) {
        String[] separated = mDurationName.split(" ");
        holder.etDurationName.setText(separated[0]);
      }

        holder.etDurationName.addTextChangedListener(new TextWatcher() {
          @Override
          public void beforeTextChanged(CharSequence s, int start, int count, int after) {
          }

          @Override
          public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (listener != null) {
              listener.onDurationItemClick(s.toString());
            }
          }

          @Override
          public void afterTextChanged(Editable s) {
          }
        });
    } else if(isFromSelectDate)
    {
      holder.lvDuration.setVisibility(View.VISIBLE);
      holder.tvAppointmentName.setVisibility(View.VISIBLE);
      holder.tvCompoundName.setTypeface(holder.fontBold);
      holder.etInstruction.setVisibility(View.VISIBLE);
      holder.tvCompoundName.setText("Add recommended visit date after");
      holder.etInstruction.setHint("Add general comments");
      if(mDurationName != null && !mDurationName.isEmpty()) {
        String[] separated = mDurationName.split(" ");
        holder.etDurationName.setText(separated[0]);
      }
      if(mInstruction != null && !mInstruction.isEmpty()) {
        holder.etInstruction.setText(mInstruction);
      }

        holder.etDurationName.addTextChangedListener(new TextWatcher() {
          @Override
          public void beforeTextChanged(CharSequence s, int start, int count, int after) {
          }

          @Override
          public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (listener != null) {
              listener.onDurationItemClick(s.toString());
            }
          }

          @Override
          public void afterTextChanged(Editable s) {
          }
        });

      holder.etInstruction.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
          if (listener != null) {
            listener.onInstructionItemClick(s.toString());
          }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
      });
    }

    holder.itemView.setOnClickListener(v -> {
        if(isFromDrugRoute && listener != null)
            listener.onDrugRouteItemClick(position,drugRouteArrayList.get(position).getName(),drugRouteArrayList.get(position).getId());
        if(isFromFrequency && listener != null)
            listener.onFrequencyItemClick(position,frequencyArrayList.get(position).getName(),frequencyArrayList.get(position).getId());
        if(isFromDirection && listener != null)
            listener.onDirectionItemClick(position,directionArrayList.get(position).getName(),directionArrayList.get(position).getId());
    });

  }

  private void uncheck(ViewHolder holder) {
    holder.tvCompoundName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.checkbox_unselected,0,0,0);
    Utility.setTextColor(mContext, holder.tvCompoundName, R.color.normalTextColor);
    holder.tvCompoundName.setTypeface(holder.fontMedium);
  }

  private void check(ViewHolder holder) {
    holder.tvCompoundName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.checkbox_selected,0,0,0);
    Utility.setTextColor(mContext, holder.tvCompoundName, R.color.colorAccent);
    holder.tvCompoundName.setTypeface(holder.fontBold);
  }

  public interface OnItemClickListener {
    void onDrugRouteItemClick(int position, String unitName,String id);

    void onFrequencyItemClick(int position, String frequencyName,String id);

    void onDirectionItemClick(int position, String directioName,String id);

    void onDurationItemClick(String durationName);

    void onInstructionItemClick(String instructionName);
  }


  @Override
  public int getItemCount() {
    if(isFromUnit)
     return unitArrayList == null ? 0 : unitArrayList.size();
    else if(isFromDirection)
      return directionArrayList == null ? 0 : directionArrayList.size();
    else if(isFromDrugRoute)
      return drugRouteArrayList == null ? 0 : drugRouteArrayList.size();
    else if(isFromFrequency)
      return frequencyArrayList == null ? 0 : frequencyArrayList.size();

    return 1;
  }

  class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvCompoundName)
    TextView tvCompoundName;
    @BindView(R.id.tvAppointmentName)
    TextView tvAppointmentName;
    @BindView(R.id.lvDuration)
    LinearLayout lvDuration;
    @BindView(R.id.etDurationName)
    EditText etDurationName;
    @BindView(R.id.etDurationDay)
    EditText etDurationDay;
    @BindView(R.id.etInstruction)
    EditText etInstruction;
    private Typeface fontRegular, fontMedium,fontBold;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      fontMedium = getFontMedium(mContext);
      fontRegular = getFontRegular(mContext);
      fontBold = getFontBold(mContext);
      tvCompoundName.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14),mContext));
      tvAppointmentName.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14),mContext));
      etDurationName.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16),mContext));
      etDurationDay.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16),mContext));
      etInstruction.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_16),mContext));
      etDurationDay.setTypeface(fontMedium);
      etDurationName.setTypeface(fontMedium);
      etInstruction.setTypeface(fontMedium);
      tvAppointmentName.setTypeface(fontBold);
    }
  }
}
