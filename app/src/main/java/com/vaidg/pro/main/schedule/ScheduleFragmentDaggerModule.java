package com.vaidg.pro.main.schedule;

import com.vaidg.pro.dagger.FragmentScoped;
import com.vaidg.pro.main.profile.ProfileContract;
import com.vaidg.pro.main.profile.ProfileFragment;
import com.vaidg.pro.main.profile.ProfilePresenter;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>ProfileDaggerModule</h1>
 * <p>This dagger module created for ProfileFragment to bind injected objects</p>
 *
 * @author 3embed.
 * @version 1.0.20.
 * @since 19/06/2020.
 **/
@Module
public abstract class ScheduleFragmentDaggerModule {

    @FragmentScoped
    @Binds
    abstract ScheculeFragmentContract.Presenter providePresenter(ScheduleFragmentPresenter presenter);

   /* @FragmentScoped
    @Binds
    abstract ScheculeFragmentContract.View provideView(ScheduleFragment fragment);*/
}
