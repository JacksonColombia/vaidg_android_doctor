package com.vaidg.pro.landing.newSignup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.landing.newSignup.consultaionFee.model.ConsultationData;
import com.vaidg.pro.landing.newSignup.findHospital.model.HospitalDataPojo;
import com.vaidg.pro.landing.newSignup.model.OnFileUploadListener;
import com.vaidg.pro.landing.newSignup.model.SignUpModel;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.network.APIHelper;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.pojo.imageupload.ImageUploadPojo;
import com.vaidg.pro.pojo.profile.DegreeDetailsData;
import com.vaidg.pro.pojo.signup.CityData;
import com.vaidg.pro.pojo.signup.SignUpPojo;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.MediaBottomSelector;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.encryptiondecryption.CryptographyExample;
import com.vaidg.pro.utility.fileUtil.AppFileManger;
import com.videocompressor.com.CompressImage;
import com.videocompressor.com.DataModel.CompressedData;
import com.videocompressor.com.RxCompressObservable;
import com.videocompressor.com.VideoCompressor;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilKeyPair;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import javax.security.auth.x500.X500Principal;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.VariableConstant.*;
import static com.vaidg.pro.utility.VariableConstant.AES_MODE;
import static com.vaidg.pro.utility.VariableConstant.ANDROID_KEYSTORE;
import static com.vaidg.pro.utility.VariableConstant.CLINIC_LOGO;
import static com.vaidg.pro.utility.VariableConstant.CLINIC_PHOTOS;
import static com.vaidg.pro.utility.VariableConstant.HTTP;
import static com.vaidg.pro.utility.VariableConstant.KEY_PRIVATEALIAS;
import static com.vaidg.pro.utility.VariableConstant.KEY_PUBLICALIAS;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.PROFILE_PIC;
import static com.vaidg.pro.utility.VariableConstant.RSA_MODE;
import static com.vaidg.pro.utility.VariableConstant.SIGNUP_PROFILE_PIC;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_DOCUMENT_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_PIC_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIDEO_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

/**
 * <h1>@SignUpPresenter</h1>
 * <p> this is presenter class which provides bussiness logics</p>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public class SignUpPresenter implements SignUpContract.Presenter, App_permission.Permission_Callback, MediaBottomSelector.Callback, OnFileUploadListener {

    private static final String TAG = SignUpPresenter.class.getSimpleName();
    private final String GALLERY = "gallery";
    private final String CAMERA = "camera";

    @Inject
    Activity context;
    @Inject
    SignUpContract.View view;
    @Inject
    Gson gson;
    @Inject
    MediaBottomSelector mediaBottomSelector;
    @Inject
    App_permission app_permission;
    @Inject
    Activity activity;
    @Inject
    Utility utility;
    @Inject
    AppFileManger appFileManger;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    CompressImage compressImage;
    @Inject
    SignUpModel model;
    @Inject
    NetworkService service;
    @Inject
    VideoCompressor videoCompressor;
    @Inject
    SessionManager sessionManager;

    private File temp_file = null;
    private KeyStore keyStore;
    private String exportedPrivateKey = "", exportedPublicKey = "";

    @Inject
    SignUpPresenter() {
    }

    @Override
    public void setImageProfile(String filePath) {
        model.setProfile(filePath);
    }

    @Override
    public boolean verifyPassword(String pass) {
        if (!model.isValidPassword(pass)) {
            view.onPasswordInCorrect();
            return false;
        } else {
            view.onPasswordCorrect();
            return true;
        }
    }

    @Override
    public boolean verifyFirstName(String val) {
        if (Utility.isTextEmpty(val)) {
            view.onUserNameInCorrect();
            return false;
        } else {
            view.onUserNameCorrect();
            return true;
        }
    }

    @Override
    public boolean verifyLastName(String val) {
        if (Utility.isTextEmpty(val)) {
            view.onUserLastNameInCorrect();
            return false;
        } else {
            view.onUserLastNameCorrect();
            return true;
        }
    }

    @Override
    public boolean verifyDob(String dob) {
        if (Utility.isTextEmpty(dob)) {
            view.onDobInCorrect();
            return false;
        } else {
            view.onDobCorrect();
            return true;
        }
    }

    @Override
    public boolean verifyEmail(String email) {
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            view.onEmailInCorrect();
            return true;
        } else {
            verifyEmailApi(email);
            return false;
        }
    }

    @Override
    public void verifyEmailApi(String email) {

/*
        if (!verifyEmail(email)) {
            return;
        }
*/

        if (networkStateHolder.isConnected()) {
            if (view != null)
                view.showProgress();
            Utility.basicAuth(service).validateEmail(model.getLanguage(), model.getPlatform(), model.sendEmailValidate(email))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                if (statusCode == SUCCESS_RESPONSE) {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.onEmailCorrect();
                                    }
                                } else {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.onEmailInCorrect(Utility.getMessage(errorBody));
                                    }
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.onEmailInCorrect(activity.getString(R.string.serverError));
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.onEmailInCorrect(activity.getString(R.string.serverError));
                            }
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public boolean verifyPhone(String phone, String countryCode) {
        if (Utility.isTextEmpty(phone)/*!view.isPhoneNumberCorrect()*/) {
            view.onPhoneNumberInCorrect(/*Utility.isTextEmpty(phone) ? */R.string.enterPhone /*: R.string.invalidPhoneNumber*/);
            return true;
        } else {
          //  view.onPhoneNumberCorrect();
            verifyPhoneApi(phone,countryCode);
            return false;
        }
    }

    @Override
    public void verifyPhoneApi(String phone, String countryCode) {

/*
        if (verifyPhone(phone, countryCode)) {
            return;
        }
*/

        if (networkStateHolder.isConnected()) {
            view.showProgress();
            Utility.basicAuth(service).validatePhoneNumber(model.getLanguage(), model.getPlatform(), model.sendPhoneNumberValidate(countryCode, phone))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                if (statusCode == SUCCESS_RESPONSE) {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.onPhoneNumberCorrect();
                                    }
                                } else {
                                    if (view != null) {
                                        view.clearPhoneNumber();
                                        view.hideProgress();
                                        view.showError(Utility.getMessage(errorBody));
                                        view.onPhoneNumberInCorrect(R.string.invalidPhoneNumber);
                                        Toast.makeText(context,Utility.getMessage(errorBody), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError(activity.getString(R.string.serverError));
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null)
                                view.hideProgress();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void parseOnActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_CITY:
                    CityData cityData = (CityData) data.getSerializableExtra("city");
                    model.setCityData(cityData);
                    view.displaySelectedCity(cityData);
                    break;

                case REQUEST_CODE_SPECIALIZATION:
                    SelectSpecializationData specializationData = data.getParcelableExtra("specialization");
                    model.setSpecializationData(specializationData);
                    view.showSpecializationData(specializationData != null ? specializationData.getCatName() : "");
                    break;

                case REQUEST_CODE_EDUCATION:
                    ArrayList<DegreeDetailsData> educationDetails = (ArrayList<DegreeDetailsData>) data.getSerializableExtra("Education");
                    model.setEducationDetails(educationDetails);
                    if (educationDetails != null && educationDetails.size() > 0)
                        view.showEducationalData(educationDetails.get(0).getName());
                    break;

                case REQUEST_CODE_YOE:
                    String year = data.getStringExtra("year");
                    model.setYearOfExp(year);
                    view.showYOE(year);
                    break;
                case REQUEST_CODE_CONSULTATION:
                    ArrayList<ConsultationData> consultationData = (ArrayList<ConsultationData>) data.getSerializableExtra("fee");
                    model.setConsultationData(consultationData);
                    view.showConsultationFee(consultationData);
                    break;

                case REQUEST_CODE_OWN_CLINIC:
                    HospitalDataPojo clinic = data.getParcelableExtra("clinic");
                    model.setClinic(clinic);
                    view.showClinicData(clinic);
                    break;
            }
        }
    }

    @Override
    public void consultationFee() {
        view.launchConsultationFee(model.getSpecializationData());
    }

    @Override
    public void openChooser() {
        temp_file = null;
        mediaBottomSelector.showBottomSheet(this);
    }

    @Override
    public void onCamera() {
        ArrayList<App_permission.Permission> permissions = new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.CAMERA);
        app_permission.getPermission_for_Sup_v4Fragment(CAMERA, permissions, null, this);

    }

    @Override
    public void onGallery() {
        ArrayList<App_permission.Permission> permissions = new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
        app_permission.getPermission_for_Sup_v4Fragment(GALLERY, permissions, null, this);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        if (isAllGranted && tag.equals(CAMERA)) {
            if (view != null) {
                try {

                    temp_file = appFileManger.getImageFile();

                    view.openCamera(utility.getUri_Path(temp_file));
                } catch (Exception e) {
                    view.showError(e.getMessage());
                }
            }
        } else if (isAllGranted && tag.equals(GALLERY)) {
            if (view != null)
                view.openGallery();
        }
    }

    @Override
    public void upDateToGallery() {
        if (temp_file == null)
            return;
      /*  Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(temp_file.getPath());
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);*/
       // Log.d(TAG, "upDateToGallery: 1"+temp_file.getPath()+ "=====>\n"+temp_file.getAbsolutePath());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            final Uri contentUri = Uri.fromFile(temp_file);
            scanIntent.setData(contentUri);
            activity.sendBroadcast(scanIntent);
        } else {
            activity.sendBroadcast(
                    new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(temp_file.getAbsolutePath())));
        }
    }


    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        if (tag.equals(GALLERY)) {
            app_permission.show_Alert_Permission(activity.getString(R.string.photo_access_text), activity.getString(R.string.gallery_acess_subtitle),
                    activity.getString(R.string.gallery_acess_message), stringArray);
        } else if (tag.equals(CAMERA)) {
            app_permission.show_Alert_Permission(activity.getString(R.string.camera_access_text), activity.getString(R.string.camera_acess_subtitle),
                    activity.getString(R.string.camera_acess_message), stringArray);
        }
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent) {
        if (parmanent) {
            if (tag.equals(GALLERY)) {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.photo_denied_text), activity.getString(R.string.gallery_denied_subtitle),
                        activity.getString(R.string.gallery_denied_message));
            } else if (tag.equals(CAMERA)) {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.camera_denied_text), activity.getString(R.string.camera_denied_subtitle),
                        activity.getString(R.string.camera_denied_message));
            }
        }
    }

    @Override
    public void compressImage(String from, String filePath, OnFileUploadListener onFileUploadListener) {
        Observer<CompressedData> observer = new Observer<CompressedData>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(CompressedData value) {
                if (value != null) {
                    upload(from, new File(value.getPath()), onFileUploadListener);
                } else {
                    view.hideProgress();
                    view.showError(activity.getString(R.string.somethingWentWrong));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                if (view != null) {
                    view.hideProgress();
                    view.showError("Failed to collect!");
                }
            }

            @Override
            public void onComplete() {
            }
        };
        RxCompressObservable observable = compressImage.compressImage(activity, filePath);
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }

    @Override
    public void compressedVideo(String from, String file_path, OnFileUploadListener onFileUploadListener) {
        Observer<CompressedData> observer = new Observer<CompressedData>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(CompressedData value) {
                try {
                    if (value != null) {
                        upload(from, new File(value.getPath()), onFileUploadListener);
                    } else {
                        view.hideProgress();
                        view.showError(activity.getString(R.string.somethingWentWrong));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (view != null) {
                        view.hideProgress();
                        view.showError(e.getMessage());
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                if (view != null) {
                    view.hideProgress();
                    view.showError(e.getMessage());
                }
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
            }
        };
        RxCompressObservable observable = videoCompressor.compressVideo(file_path);
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Override
    public void upload(String from, File mFileTemp, OnFileUploadListener onFileUploadListener) {
        if (networkStateHolder.isConnected()) {
            Map<String, RequestBody> params = new HashMap<>();
            params.put("uploadTo", APIHelper.createPartFromString("1"));
            params.put("folder", APIHelper.createPartFromString(PROFILE_PIC));
            params.put("fileName", APIHelper.createPartFromString(""));
            MultipartBody.Part filePart = APIHelper.prepareFilePart(activity, "file", mFileTemp);
            service.imageUpload(DEFAULT_LANGUAGE, PLATFORM, params, filePart)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                Utility.printLog(TAG, "Status code : " + statusCode + ", Response : " + response + ", ErrorBody : " + errorBody);
                                if (statusCode == SUCCESS_RESPONSE) {
                                    ImageUploadPojo imageUploadPojo = gson.fromJson(response, ImageUploadPojo.class);
                                    Utility.printLog(TAG, "Uploaded Image S3 Path : " + imageUploadPojo.getData().getImageUrl());
                                    onFileUploadListener.onFileUploaded(from, imageUploadPojo.getData().getImageUrl());
                                } else {
                                    onFileUploadListener.onFileUploaded(from, Utility.getMessage(errorBody));
                                }
                            } catch (Exception e) {
/*
                                if (view != null)
                                    view.hideProgress();
*/

                                onFileUploadListener.onFileUploaded(from, activity.getString(R.string.serverError));
                                e.printStackTrace();
                            }
/*
                            if (view != null)
                                view.hideProgress();
*/

                        }

                        @Override
                        public void onError(Throwable e) {
/*
                            if (view != null)
                                view.hideProgress();
*/
                            onFileUploadListener.onFileUploaded(from, activity.getString(R.string.serverError));
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public String getRecentTemp() {
        return temp_file.getPath();
    }

    @Override
    public void signUp(String title, String firstName, String lastName, String DOB, int gender,
                       String referralCode, String email, String ccp, String phoneNumber, String Password) {

        Utility.hideKeyboad(activity);

        if (model.getProfile() == null || model.getProfile().isEmpty()) {
            view.showError(activity.getString(R.string.errorSelectProfilePic));
            return;
        }

        if (model.getCityData() == null) {
            view.showError(activity.getString(R.string.city_selection_error));
            return;
        }

        if (model.getSpecializationData() == null) {
            view.showError(activity.getString(R.string.specialization_selection_error));
            return;
        }

        if (model.getClinic() == null) {
            view.showError(activity.getString(R.string.clinic_hospital_selection_error));
            return;
        }

        if (model.getEducationDetails() == null) {
            view.showError(activity.getString(R.string.education_detail_error));
            return;
        }

        if (model.getYearOfExp() == null || model.getYearOfExp().isEmpty()) {
            view.showError(activity.getString(R.string.enter_yoe));
            return;
        }

        if (model.getConsultationData() == null) {
            view.showError(activity.getString(R.string.consultation_fee_selection_error));
            return;
        }

        if (!verifyTitle(title) || !verifyFirstName(firstName) || !verifyLastName(lastName) || !verifyDob(DOB) || verifyEmail(email) || verifyPhone(phoneNumber,ccp) || !verifyPassword(Password)) {
            return;
        }

        try {
            model.setProfileData(title, firstName, lastName, DOB, gender, referralCode, email, ccp, phoneNumber, Password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        allMediaUploaded();

    }

    @Override
    public void callSignUpAPI() {
        if (networkStateHolder.isConnected()) {
            if (view != null)
                view.showProgress();
            JSONObject params = model.getProfileData();
            try {
                params = model.sendSendData(params);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String payload = params.toString();

            Utility.basicAuth(service).signUp(model.getAuthorization(), model.getLanguage(), model.getPlatform(),
                    payload)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                if (statusCode == SUCCESS_RESPONSE) {
                                    if (sessionManager.getVirgilPrivateKeyT() != null && !sessionManager.getVirgilPrivateKeyT().isEmpty() && sessionManager.getVirgilPublicKeyT() != null && !sessionManager.getVirgilPublicKeyT().isEmpty())
                                        sessionManager.clearVirgilPrivatePublicKeyT();
                                    if (view != null)
                                        view.hideProgress();
                                    SignUpPojo signUpPojo = gson.fromJson(response, SignUpPojo.class);
                                    if (view != null)
                                        view.success(signUpPojo.getData());
                                } else {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.showError(Utility.getMessage(errorBody));
                                    }
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError(activity.getString(R.string.serverError));
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.showError(activity.getString(R.string.serverError));
                            }
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public boolean allMediaUploaded() {
        String from = null;
        String uploadPath = null;
      //  view.showProgress();

        if (!isRemoteUrl(model.getProfile())) {
            from = SIGNUP_PROFILE_PIC;
            uploadPath = model.getProfile();
        }

        if (uploadPath == null && model.getClinic().getClinicLogoApp() != null && !model.getClinic().getClinicLogoApp().isEmpty() && !isRemoteUrl(model.getClinic().getClinicLogoApp())) {
            from = CLINIC_LOGO;
            uploadPath = model.getClinic().getClinicLogoApp();
        }

        if (uploadPath == null && model.getClinic().getClinicLogoWeb() != null && !model.getClinic().getClinicLogoWeb().isEmpty()) {
            String[] images = model.getClinic().getClinicLogoWeb().contains(",") ? model.getClinic().getClinicLogoWeb().split(",") : new String[]{model.getClinic().getClinicLogoWeb()};
            for (int i = 0; i < images.length; i++) {
                if (!isRemoteUrl(images[i])) {
                    from = CLINIC_PHOTOS.concat("_").concat(String.valueOf(i));
                    uploadPath = images[i];
                    break;
                }
            }
        }

        if (uploadPath == null && model.getSpecializationData().getAttributes() != null && model.getSpecializationData().getAttributes().size() > 0) {
            for (Attribute attribute : model.getSpecializationData().getAttributes()) {
                switch (attribute.getType()) {
                    case 12:
                    case 13:
                        String type = attribute.getType() == 12 ? SPECIALIZATION_PIC_UPLOAD : SPECIALIZATION_VIDEO_UPLOAD;
                        if (attribute.getData() != null && !attribute.getData().trim().isEmpty()) {
                            String[] images = attribute.getData().contains(",") ? attribute.getData().split(",") : new String[]{attribute.getData()};
                            for (int i = 0; i < images.length; i++) {
                                if (!isRemoteUrl(images[i])) {
                                    from = type.concat("_").concat(String.valueOf(i));
                                    uploadPath = images[i];
                                    break;
                                }
                            }
                        }
                        break;
                    case 14:
                        if (attribute.getPreDefined() != null && attribute.getPreDefined().size() > 0) {
                            for (int i = 0; i < attribute.getPreDefined().size(); i++) {
                                PreDefined preDefined = attribute.getPreDefined().get(i);
                                if (preDefined.getType() == 3 && !Utility.isTextEmpty(preDefined.getData()) && !isRemoteUrl(preDefined.getData())) {
                                    from = SPECIALIZATION_DOCUMENT_UPLOAD.concat("_").concat(String.valueOf(i));
                                    uploadPath = preDefined.getData();
                                    break;
                                }
                            }
                        }
                        break;
                }
            }
        }

        if (uploadPath == null) {
            callSignUpAPI();
            return true;
        }

        String mimeType = URLConnection.guessContentTypeFromName(uploadPath);
        if (mimeType != null && mimeType.startsWith("image")) {
           compressImage(from, uploadPath, this);
           // upload(from, new File(uploadPath), this);
        } else if (mimeType != null && mimeType.startsWith("video")) {
            compressedVideo(from, uploadPath, this);
          //  upload(from, new File(uploadPath), this);
        } else {
            upload(from, new File(uploadPath), this);
        }
        return false;
    }

    @SuppressWarnings("DuplicateExpressions")
    @Override
    public void onFileUploaded(String from, String remoteUrl) {
        if (from.equals(SIGNUP_PROFILE_PIC)) {
            model.setProfile(remoteUrl);
        } else if (from.equals(CLINIC_LOGO)) {
            model.getClinic().setClinicLogoApp(remoteUrl);
        } else if (from.startsWith(CLINIC_PHOTOS)) {
            if (from.contains("_")) {
                int position = Integer.parseInt(from.substring(from.indexOf("_") + 1));
                String[] images = model.getClinic().getClinicLogoWeb().split(",");
                images[position] = remoteUrl;
                model.getClinic().setClinicLogoWeb(TextUtils.join(",", images));
            } else {
                model.getClinic().setClinicLogoWeb(remoteUrl);
            }
        } else if (from.startsWith(SPECIALIZATION_PIC_UPLOAD)) {
            for (int i = 0; i < model.getSpecializationData().getAttributes().size(); i++) {
                Attribute attribute = model.getSpecializationData().getAttributes().get(i);
                if (attribute.getType() == 12) {
                    if (from.contains("_")) {
                        int position = Integer.parseInt(from.substring(from.indexOf("_") + 1));
                        String[] images = attribute.getData().split(",");
                        images[position] = remoteUrl;
                        attribute.setData(TextUtils.join(",", images));
                    } else {
                        attribute.setData(remoteUrl);
                    }
                    model.getSpecializationData().getAttributes().set(i, attribute);
                }
            }
        } else if (from.startsWith(SPECIALIZATION_VIDEO_UPLOAD)) {
            for (int i = 0; i < model.getSpecializationData().getAttributes().size(); i++) {
                Attribute attribute = model.getSpecializationData().getAttributes().get(i);
                if (attribute.getType() == 13) {
                    if (from.contains("_")) {
                        int position = Integer.parseInt(from.substring(from.indexOf("_") + 1));
                        String[] images = attribute.getData().split(",");
                        images[position] = remoteUrl;
                        attribute.setData(TextUtils.join(",", images));
                    } else {
                        attribute.setData(remoteUrl);
                    }
                    model.getSpecializationData().getAttributes().set(i, attribute);
                }
            }
        } else if (from.startsWith(SPECIALIZATION_DOCUMENT_UPLOAD)) {
            for (int i = 0; i < model.getSpecializationData().getAttributes().size(); i++) {
                Attribute attribute = model.getSpecializationData().getAttributes().get(i);
                if (attribute.getType() == 14) {
                    if (attribute.getPreDefined() != null && attribute.getPreDefined().size() > 0) {
                        int position = Integer.parseInt(from.substring(from.indexOf("_") + 1));
                        attribute.getPreDefined().get(position).setData(remoteUrl);
                        model.getSpecializationData().getAttributes().set(i, attribute);
                    }
                }
            }
        }

        allMediaUploaded();

    }

    @Override
    public void onFileUploadFailed(String from, String errorMessage) {
        view.hideProgress();
        view.showError(errorMessage);
    }

    @Override
    public boolean isRemoteUrl(String url) {
        return url.startsWith(HTTP);
    }

    @Override
    public boolean verifyTitle(String val) {
        if (Utility.isTextEmpty(val)) {
            view.onTitleInCorrect();
            return false;
        } else {
            view.onTitleCorrect();
            return true;
        }
    }

    @Override
    public ArrayList<DegreeDetailsData> getEducationDetails() {
        return model.getEducationDetails();
    }

    @Override
    public String getYearOfExp() {
        return model.getYearOfExp();
    }

    @Override
    public ArrayList<ConsultationData> getConsultationData() {
        return model.getConsultationData();
    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    @Override
    public void checkForKeystore() {
        try {
            keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
            keyStore.load(null);
            view.checkForVersion();
        } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
        }
        // if (!keyStore.containsAlias(KEY_PRIVATEALIAS)) {

        //}
    }

    private void createVirgilPublicPrivateKey() {
        try {
            CryptographyExample cryptographyExample = new CryptographyExample();
            // Generate keys
            VirgilCrypto crypto = new VirgilCrypto();
            VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
            // Export private key
            exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
            // Export public key
            exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);

        } catch (CryptoException e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            encryptPrivateKeyPostLollipop(exportedPrivateKey);
            encryptPublicKeyPostLollipop(exportedPublicKey);
        } else {
            encryptPrivateKeyPreMarshMallow(exportedPrivateKey);
            encryptPublicKeyPreMarshMallow(exportedPublicKey);
        }
    }

    /**
     * <hg2>generateAndStorePublicKeyAES</hg2>
     * This method is useed to generate the key for pre marsh mallow and store
     */
    private void generateAndStorePublicKeyAES(String exportedPublicKey) {
        String encryptedKeyB64 = exportedPublicKey;//sessionManager.getEncryptionPublicKey();
        if (encryptedKeyB64 == null) {
            byte[] key = new byte[16];
            SecureRandom secureRandom = new SecureRandom();
            secureRandom.nextBytes(key);
            byte[] encryptedKey;
            try {
                encryptedKey = rsaPublicKeyEncrypt(key);
                encryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT);
                sessionManager.storeEncryptionPublicKey(encryptedKeyB64);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * <hg2>generateAndStorePrivateKeyAES</hg2>
     * This method is useed to generate the key for pre marsh mallow and store
     */
    private void generateAndStorePrivateKeyAES(String exportedPrivateKey) {
        String encryptedKeyB64 = exportedPrivateKey;//sessionManager.getEncryptionPrivateKey();
        if (encryptedKeyB64 == null) {
            byte[] key = new byte[16];
            SecureRandom secureRandom = new SecureRandom();
            secureRandom.nextBytes(key);
            byte[] encryptedKey;
            try {
                encryptedKey = rsaPrivateKeyEncrypt(key);
                encryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT);
                sessionManager.storeEncryptionPrivateKey(encryptedKeyB64);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * <h2>rsaPublicKeyEncrypt</h2>
     * This method is used to do the rsa encrypt
     *
     * @param secret takes secret bytes as input
     * @return returns the RSA encrypt byts
     * @throws Exception throws an exception
     */
    private byte[] rsaPublicKeyEncrypt(byte[] secret) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
        // Encrypt the text
        Cipher inputCipher = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        inputCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inputCipher);
        cipherOutputStream.write(secret);
        cipherOutputStream.close();
        return outputStream.toByteArray();
    }

    /**
     * <h2>rsaPrivateKeyEncrypt</h2>
     * This method is used to do the rsa encrypt
     *
     * @param secret takes secret bytes as input
     * @return returns the RSA encrypt byts
     * @throws Exception throws an exception
     */
    private byte[] rsaPrivateKeyEncrypt(byte[] secret) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
        // Encrypt the text
        Cipher inputCipher = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        inputCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inputCipher);
        cipherOutputStream.write(secret);
        cipherOutputStream.close();
        return outputStream.toByteArray();
    }


    /**
     * <h2>getSecretPrivateKeyPostLollipop</h2>
     * This method is used to get the secret key for post lollipop device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private Key getSecretPrivateKeyPostLollipop() throws Exception {
        Key s = keyStore.getKey(KEY_PRIVATEALIAS, null);
        Log.i("TEST", "key " + s.toString());
        return keyStore.getKey(KEY_PRIVATEALIAS, null);
    }

    /**
     * <h2>getSecretPublicKeyPostLollipop</h2>
     * This method is used to get the secret key for post lollipop device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private Key getSecretPublicKeyPostLollipop() throws Exception {
        Key s = keyStore.getKey(KEY_PUBLICALIAS, null);
        Log.i("TEST", "key " + s.toString());
        return keyStore.getKey(KEY_PUBLICALIAS, null);
    }

    /**
     * <h2>getSecretPrivateKeyPreMarshMallow</h2>
     * This method is used to get the secret key for pre marsh mallow device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private Key getSecretPrivateKeyPreMarshMallow(String exportedPrivateKey) throws Exception {
        String enryptedKeyB64 = exportedPrivateKey;//sessionManager.getEncryptionPrivateKey();
        Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
        // need to check null, omitted here
        byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaPrivateKeyDecrypt(encryptedKey);
        Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
        return new SecretKeySpec(key, "AES");
    }

    /**
     * <h2>getSecretPublicKeyPreMarshMallow</h2>
     * This method is used to get the secret key for pre marsh mallow device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private Key getSecretPublicKeyPreMarshMallow(String exportedPublicKey) throws Exception {
        String enryptedKeyB64 = exportedPublicKey;//sessionManager.getEncryptionPublicKey();
        Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
        // need to check null, omitted here
        byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaPublicKeyDecrypt(encryptedKey);
        Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
        return new SecretKeySpec(key, "AES");
    }


    private byte[] rsaPrivateKeyDecrypt(byte[] encrypted) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
        CipherInputStream cipherInputStream = new CipherInputStream(
                new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }
        return bytes;
    }

    private byte[] rsaPublicKeyDecrypt(byte[] encrypted) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
        CipherInputStream cipherInputStream = new CipherInputStream(
                new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }
        return bytes;
    }

    @SuppressLint("NewApi")
    @Override
    public void generateEncryptionPrivateKeyPostLollipop(String exportedPrivateKey) {
        KeyGenerator keyGenerator = null;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEYSTORE);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        try {
            assert keyGenerator != null;
            keyGenerator.init
                    (new KeyGenParameterSpec.Builder(KEY_PRIVATEALIAS,
                            KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                            .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                            .setRandomizedEncryptionRequired(false)
                            .build());
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        keyGenerator.generateKey();
        createVirgilPrivateKey(exportedPrivateKey);
    }


    @SuppressLint("NewApi")
    @Override
    public void generateEncryptionPublicKeyPostLollipop(String exportedPublicKey) {
        KeyGenerator keyGenerator = null;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEYSTORE);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        try {
            assert keyGenerator != null;
            keyGenerator.init
                    (new KeyGenParameterSpec.Builder(KEY_PUBLICALIAS,
                            KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                            .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                            .setRandomizedEncryptionRequired(false)
                            .build());
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        keyGenerator.generateKey();
        createVirgilPublicKey(exportedPublicKey);
    }

    @SuppressLint("NewApi")
    @Override
    public void generateEncryptionPrivateKeyPreMarshMallow(String exportedPrivateKey) {
        // Generate a key pair for encryption
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        end.add(Calendar.YEAR, 30);
        KeyPairGeneratorSpec spec;
        spec = new KeyPairGeneratorSpec.Builder(activity)
                .setAlias(KEY_PRIVATEALIAS)
                .setSubject(new X500Principal("CN=" + KEY_PRIVATEALIAS))
                .setSerialNumber(BigInteger.TEN)
                .setStartDate(start.getTime())
                .setEndDate(end.getTime())
                .build();

        KeyPairGenerator kpg = null;
        try {
            kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEYSTORE);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        try {
            assert kpg != null;
            kpg.initialize(spec);
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        kpg.generateKeyPair();

        generateAndStorePrivateKeyAES(exportedPrivateKey);


        createVirgilPrivateKey(exportedPrivateKey);
    }

    @SuppressLint("NewApi")
    @Override
    public void generateEncryptionPublicKeyPreMarshMallow(String exportedPublicKey) {
        // Generate a key pair for encryption
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        end.add(Calendar.YEAR, 30);
        KeyPairGeneratorSpec spec;
        spec = new KeyPairGeneratorSpec.Builder(activity)
                .setAlias(KEY_PUBLICALIAS)
                .setSubject(new X500Principal("CN=" + KEY_PUBLICALIAS))
                .setSerialNumber(BigInteger.TEN)
                .setStartDate(start.getTime())
                .setEndDate(end.getTime())
                .build();

        KeyPairGenerator kpg = null;
        try {
            kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEYSTORE);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        try {
            assert kpg != null;
            kpg.initialize(spec);
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        kpg.generateKeyPair();

        generateAndStorePublicKeyAES(exportedPublicKey);

        createVirgilPublicKey(exportedPublicKey);
    }

    private void createVirgilPrivateKey(String exportedPrivateKey) {
/*
    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      // Generate keys
      VirgilCrypto crypto = new VirgilCrypto();
      VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
      // Export private key
      exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
      // Export public key
      exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            encryptPrivateKeyPostLollipop(exportedPrivateKey);
        } else {
            encryptPrivateKeyPreMarshMallow(exportedPrivateKey);
        }
    }

    private void createVirgilPublicKey(String exportedPublicKey) {
/*
    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      // Generate keys
      VirgilCrypto crypto = new VirgilCrypto();
      VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
      // Export private key
      exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
      // Export public key
      exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            encryptPublicKeyPostLollipop(exportedPublicKey);
        } else {
            encryptPublicKeyPreMarshMallow(exportedPublicKey);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void encryptPrivateKeyPostLollipop(String exportedPrivateKey) {
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE);
            c.init(Cipher.ENCRYPT_MODE, getSecretPrivateKeyPostLollipop(), new GCMParameterSpec(128,
                    FIXED_IV.getBytes()));
            byte[] encodedBytes = c.doFinal(exportedPrivateKey.getBytes(StandardCharsets.US_ASCII));

            String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
            Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
            sessionManager.setVirgilPrivateKey(encryptedBase64Encoded);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void encryptPublicKeyPostLollipop(String exportedPublicKey) {
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE);
            c.init(Cipher.ENCRYPT_MODE, getSecretPublicKeyPostLollipop(), new GCMParameterSpec(128,
                    FIXED_IV.getBytes()));
            byte[] encodedBytes = c.doFinal(exportedPublicKey.getBytes(StandardCharsets.US_ASCII));

            String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
            Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
            sessionManager.setVirgilPublicKey(encryptedBase64Encoded);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void encryptPrivateKeyPreMarshMallow(String exportedPrivateKey) {
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE, "BC");
            c.init(Cipher.ENCRYPT_MODE, getSecretPrivateKeyPreMarshMallow(exportedPrivateKey), IV_PARAMETER_SPEC);
            byte[] encodedBytes = c.doFinal(exportedPrivateKey.getBytes());
            String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
            Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
            sessionManager.setVirgilPrivateKey(encryptedBase64Encoded);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void encryptPublicKeyPreMarshMallow(String exportedPublicKey) {
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE, "BC");
            c.init(Cipher.ENCRYPT_MODE, getSecretPublicKeyPreMarshMallow(exportedPublicKey), IV_PARAMETER_SPEC);
            byte[] encodedBytes = c.doFinal(exportedPublicKey.getBytes());
            String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
            Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
            sessionManager.setVirgilPublicKey(encryptedBase64Encoded);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setVirgilPublicKeyT(String exportedPublicKey) {
        sessionManager.setVirgilPublicKeyT(exportedPublicKey);
    }

    @Override
    public void setVirgilPrivateKeyT(String exportedPrivateKey) {
        sessionManager.setVirgilPrivateKeyT(exportedPrivateKey);
    }
}
