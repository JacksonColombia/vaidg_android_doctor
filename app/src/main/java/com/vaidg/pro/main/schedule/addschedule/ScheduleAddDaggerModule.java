package com.vaidg.pro.main.schedule.addschedule;

import android.app.Activity;
import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>ProfileDaggerModule</h1>
 * <p>This dagger module created for ProfileFragment to bind injected objects</p>
 *
 * @author 3embed.
 * @version 1.0.20.
 * @since 19/06/2020.
 **/
@Module
public abstract class ScheduleAddDaggerModule {

    @ActivityScoped
    @Binds
    abstract Activity getActivity(ScheduleAddActivity mainActivity);


    @ActivityScoped
    @Binds
    abstract ScheculeAddContract.Presenter providePresenter(ScheduleAddPresenter presenter);

    @ActivityScoped
    @Binds
    abstract ScheculeAddContract.View provideView(ScheduleAddActivity activity);

}
