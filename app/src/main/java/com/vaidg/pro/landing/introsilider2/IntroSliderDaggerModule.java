package com.vaidg.pro.landing.introsilider2;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>IntroSliderDaggerModule</h1>
 * <p>This dagger module created for IntroSliderNewActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @see com.vaidg.pro.dagger.ActivityBindingModule
 * @since 02/06/2020
 **/
@Module
public abstract class IntroSliderDaggerModule {

    @ActivityScoped
    @Binds
    abstract Activity getActivity(IntroSliderNewActivity introSliderNewActivity);

    @ActivityScoped
    @Binds
    abstract IntroSliderContract.View getView(IntroSliderNewActivity introSliderNewActivity);

    @ActivityScoped
    @Binds
    abstract IntroSliderContract.Presenter getPresenter(IntroSliderPresenter introSliderPresenter);

}
