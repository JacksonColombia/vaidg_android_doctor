package com.vaidg.pro.main.profile;

import android.app.Activity;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.language.LanguagePojo;
import com.vaidg.pro.pojo.profile.ProfilePojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

/**
 * <h1>ProfilePresenter</h1>
 * <p>This presenter is define to handle activity interactions done by user.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 19/06/2020
 **/
public class ProfilePresenter implements ProfileContract.Presenter {

    private static final String TAG = "ProfilePresenter";

    @Inject
    Activity activity;
    @Inject
    ProfileContract.View view;
    @Inject
    NetworkService service;
    @Inject
    SessionManager sessionManager;
    @Inject
    Gson gson;

    @Inject
    ProfilePresenter() {

    }


    @Override
    public void getProfile() {
        if (view != null)
        view.showProgress();
        Utility.printLog(TAG, "User Id : " + sessionManager.getProviderId());
        service.getProfile(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if (view != null)
                                    view.hideProgress();
                                    if (response != null && !response.isEmpty()) {
                                        ProfilePojo profilePojo = gson.fromJson(response, ProfilePojo.class);
                                        sessionManager.setTitle(profilePojo.getData().getTitle());
                                        sessionManager.setFirstName(profilePojo.getData().getFirstName());
                                        sessionManager.setFirstName(profilePojo.getData().getFirstName());
                                        sessionManager.setLastName(profilePojo.getData().getLastName());
                                        sessionManager.setEmail(profilePojo.getData().getEmail());
                                        sessionManager.setPhoneNumber(profilePojo.getData().getMobile());
                                        sessionManager.setCountryCode(profilePojo.getData().getCountryCode());
                                        sessionManager.setProfilePic(profilePojo.getData().getProfilePic());
                                        sessionManager.setGender(profilePojo.getData().getGender());
                                        sessionManager.setDob(profilePojo.getData().getDob());
                                        if (view != null)
                                        view.onSuccessProfile(profilePojo.getData());
                                    } else {
                                        if (view != null)
                                            view.onFailure();
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            if (view != null)
                                                view.hideProgress();
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            getProfile();
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            if (view != null)
                                                view.hideProgress();
                                            getInstance().toast(Utility.getMessage(errorBody));
                                            Utility.logoutSessionExiperd(sessionManager, activity);
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if (view != null)
                                            view.hideProgress();
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, activity);
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    if (view != null)
                                    view.hideProgress();
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, activity);
                                    break;
                                default:
                                    if (view != null) {
                                        view.hideProgress();
                                        view.showError(Utility.getMessage(errorBody));
                                    }
                                    break;
                            }
                        } catch (Exception e) {
                            if (view != null) {
                                view.hideProgress();
                                view.onFailure();
                            }
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.hideProgress();
                            view.onFailure();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void getLanguage() {
       // sessionManager.setLanguageCode(DEFAULT_LANGUAGE);
            Utility.basicAuth(service).getLanguage(DEFAULT_LANGUAGE,PLATFORM)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> value) {
                        int statusCode = value.code();
                        try {
                            String response = value.body() != null
                                ? value.body().string() : null;
                            String errorBody = value.errorBody() != null
                                ? value.errorBody().string() : null;
                            if (statusCode == SUCCESS_RESPONSE) {
                                LanguagePojo languagePojo = gson.fromJson(response, LanguagePojo.class);
                                sessionManager.setLanguageList(response);
                                view.onLangSuccess(languagePojo.getData());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    @Override
    public void detach() {

    }

    @Override
    public void attachView(ProfileContract.View view) {

    }

    @Override
    public void detachView() {

    }
}
