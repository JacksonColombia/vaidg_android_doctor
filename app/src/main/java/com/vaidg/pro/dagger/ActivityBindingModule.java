package com.vaidg.pro.dagger;



import com.vaidg.pro.bookingflow.acceptReject.AcceptRejectActivity;
import com.vaidg.pro.bookingflow.acceptReject.AcceptRejectDaggerModule;
import com.vaidg.pro.bookingflow.acceptReject.model.AcceptRejectActivityUtil;
import com.vaidg.pro.bookingflow.addmedication.AddMedicationActivity;
import com.vaidg.pro.bookingflow.addmedication.AddMedicationDaggerModule;
import com.vaidg.pro.bookingflow.addmedication.model.AddMedicationUtil;
import com.vaidg.pro.bookingflow.arrived.ArrivedActivity;
import com.vaidg.pro.bookingflow.arrived.ArrivedDaggerModule;
import com.vaidg.pro.bookingflow.arrived.model.ArrivedActivityUtil;
import com.vaidg.pro.bookingflow.bookingdetails.EventStartedCompletedActivity;
import com.vaidg.pro.bookingflow.bookingdetails.EventStartedCompletedDaggerModule;
import com.vaidg.pro.bookingflow.bookingdetails.model.EventStartedCompletedActivityUtil;
import com.vaidg.pro.bookingflow.incall.InCallActivity;
import com.vaidg.pro.bookingflow.incall.InCallDaggerModule;
import com.vaidg.pro.bookingflow.incall.model.InCallActivityUtil;
import com.vaidg.pro.bookingflow.invoice.InvoiceActivity;
import com.vaidg.pro.bookingflow.invoice.InvoiceDaggerModule;
import com.vaidg.pro.bookingflow.invoice.model.InvoiceActivityUtil;
import com.vaidg.pro.bookingflow.medication.MedicationActivity;
import com.vaidg.pro.bookingflow.medication.MedicationDaggerModule;
import com.vaidg.pro.bookingflow.medication.model.MedicationUtil;
import com.vaidg.pro.bookingflow.ontheway.OnTheWayActivity;
import com.vaidg.pro.bookingflow.ontheway.OnTheWayDaggerModule;
import com.vaidg.pro.bookingflow.ontheway.model.OnTheWayActivityUtil;
import com.vaidg.pro.bookingflow.review.RateCustomerActivity;
import com.vaidg.pro.bookingflow.review.RateCustomerDaggerModule;
import com.vaidg.pro.bookingflow.review.model.RateCustomerUtil;
import com.vaidg.pro.bookingflow.telecall.TeleCallActivity;
import com.vaidg.pro.bookingflow.telecall.TeleCallActivityDaggerModule;
import com.vaidg.pro.bookingflow.telecall.model.TeleCallActivityUtil;
import com.vaidg.pro.changepassword.ChangePasswordActivity;
import com.vaidg.pro.changepassword.ChangePasswordDaggerModule;
import com.vaidg.pro.changepassword.model.ChangePasswordUtil;
import com.vaidg.pro.helpwithpassword.HelpWithPasswordActivity;
import com.vaidg.pro.helpwithpassword.HelpWithPasswordDaggerModule;
import com.vaidg.pro.helpwithpassword.model.HelpWithPasswordUtil;
import com.vaidg.pro.landing.introsilider2.IntroSliderDaggerModule;
import com.vaidg.pro.landing.introsilider2.IntroSliderNewActivity;
import com.vaidg.pro.landing.introsilider2.IntroSliderUtil;
import com.vaidg.pro.landing.newLogin.LogInDaggerModule;
import com.vaidg.pro.landing.newLogin.LogInUtil;
import com.vaidg.pro.landing.newLogin.NewLogInActivity;
import com.vaidg.pro.landing.newSignup.SignUpActivity;
import com.vaidg.pro.landing.newSignup.SignUpDaggerModule;
import com.vaidg.pro.landing.newSignup.SignUpUtil;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.SpecializationCategoryActivity;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.SpecializationCategoryDaggerModule;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.SpecializationCategoryUtil;
import com.vaidg.pro.landing.newSignup.addClinic.AddClinicActivity;
import com.vaidg.pro.landing.newSignup.addClinic.AddClinicDaggerModule;
import com.vaidg.pro.landing.newSignup.consultaionFee.ConsultationFeeActivity;
import com.vaidg.pro.landing.newSignup.consultaionFee.ConsultationFeeDaggerModule;
import com.vaidg.pro.landing.newSignup.consultaionFee.ConsultationFeeUtil;
import com.vaidg.pro.landing.newSignup.editEducation.AddEditEducationActivity;
import com.vaidg.pro.landing.newSignup.editEducation.AddEditEducationDaggerModule;
import com.vaidg.pro.landing.newSignup.editEducation.selectdegree.SelectDegreeActivity;
import com.vaidg.pro.landing.newSignup.editEducation.selectdegree.SelectDegreeDaggerModule;
import com.vaidg.pro.landing.newSignup.editEducation.selectdegree.SelectDegreeUtil;
import com.vaidg.pro.landing.newSignup.educationBackground.EducationDetailsActivity;
import com.vaidg.pro.landing.newSignup.educationBackground.EducationDetailsModule;
import com.vaidg.pro.landing.newSignup.educationBackground.EducationDetailsUtil;
import com.vaidg.pro.landing.newSignup.findHospital.FindHospitalActivity;
import com.vaidg.pro.landing.newSignup.findHospital.FindHospitalDaggerModule;
import com.vaidg.pro.landing.newSignup.findHospital.FindHospitalUtil;
import com.vaidg.pro.landing.newSignup.selectCity.SelectCityActivity;
import com.vaidg.pro.landing.newSignup.selectCity.SelectCityDaggerModule;
import com.vaidg.pro.landing.newSignup.selectCity.SelectCityUtil;
import com.vaidg.pro.landing.newSignup.selectSpecialization.SelectSpecializationActivity;
import com.vaidg.pro.landing.newSignup.selectSpecialization.SelectSpecializationDaggerModule;
import com.vaidg.pro.landing.newSignup.selectSpecialization.SelectSpecializationUtil;
import com.vaidg.pro.landing.newSignup.selfOwnedClinic.SelfOwnedClinicActivity;
import com.vaidg.pro.landing.newSignup.selfOwnedClinic.SelfOwnedClinicDaggerModule;
import com.vaidg.pro.landing.newSignup.selfOwnedClinic.SelfOwnedClinicUtil;
import com.vaidg.pro.landing.newSignup.yearOfExp.YearOfExpActivity;
import com.vaidg.pro.landing.newSignup.yearOfExp.YearOfExpDaggerModule;
import com.vaidg.pro.location.LocationActivity;
import com.vaidg.pro.main.MainActivity;
import com.vaidg.pro.main.MainActivityDaggerModule;
import com.vaidg.pro.main.earning.EarningWebActivity;
import com.vaidg.pro.main.earning.EarningWebDaggerModule;
import com.vaidg.pro.main.earning.model.EarningWebUtil;
import com.vaidg.pro.main.history.HistoryGraphActivity;
import com.vaidg.pro.main.history.HistoryGraphDaggerModule;
import com.vaidg.pro.main.history.historyinvoice.HistoryInvoiceActivity;
import com.vaidg.pro.main.history.historyinvoice.HistoryInvoiceDaggerModule;
import com.vaidg.pro.main.history.model.HistoryGraphUtil;
import com.vaidg.pro.main.model.MainActivityUtil;
import com.vaidg.pro.main.notification.NotificationActivity;
import com.vaidg.pro.main.notification.NotificationDaggerModule;
import com.vaidg.pro.main.notification.model.NotificationActivityUtil;
import com.vaidg.pro.main.profile.bank.backrazorpayaccount.BankRazorPayAccountActivity;
import com.vaidg.pro.main.profile.bank.backrazorpayaccount.BankRazorPayAccountDaggerModule;
import com.vaidg.pro.main.profile.bank.bankdetails.BankDetailsActivity;
import com.vaidg.pro.main.profile.bank.bankdetails.BankDetailsDaggerModule;
import com.vaidg.pro.main.profile.bank.banknewaccount.BankNewAccountActivity;
import com.vaidg.pro.main.profile.bank.banknewaccount.BankNewAccountDaggerModule;
import com.vaidg.pro.main.profile.bank.banknewstripe.BankNewStripeActivity;
import com.vaidg.pro.main.profile.bank.banknewstripe.BankNewStripeDaggerModule;
import com.vaidg.pro.main.profile.bank.bankrazorpay.BankRazorPayActivity;
import com.vaidg.pro.main.profile.bank.bankrazorpay.BankRazorPayDaggerModule;
import com.vaidg.pro.main.profile.bank.model.BankDetailsUtil;
import com.vaidg.pro.main.profile.bank.model.BankNewStripeUtil;
import com.vaidg.pro.main.profile.bank.model.BankNewAccountUtil;
import com.vaidg.pro.main.profile.bank.model.BankRazorPayAccountUtil;
import com.vaidg.pro.main.profile.bank.model.BankRazorPayUtil;
import com.vaidg.pro.main.profile.helpcenter.HelpCenterActivity;
import com.vaidg.pro.main.profile.helpcenter.HelpCenterContract;
import com.vaidg.pro.main.profile.helpcenter.HelpCenterDaggerModule;
import com.vaidg.pro.main.profile.helpcenter.HelpCenterUtil;
import com.vaidg.pro.main.profile.helpcenter.NewTicketActivity;
import com.vaidg.pro.main.profile.helpcenter.NewTicketDaggerModule;
import com.vaidg.pro.main.profile.helpcenter.NewTicketUtil;
import com.vaidg.pro.main.profile.profiledetail.ProfileDetailActivity;
import com.vaidg.pro.main.profile.profiledetail.ProfileDetailDaggerModule;
import com.vaidg.pro.main.profile.profiledetail.model.ProfileDetailUtil;
import com.vaidg.pro.main.profile.editemailphone.EditEmailPhoneActivity;
import com.vaidg.pro.main.profile.editemailphone.EditEmailPhoneDaggerModule;
import com.vaidg.pro.main.profile.editemailphone.model.EditEmailPhoneUtil;
import com.vaidg.pro.main.profile.profiledetail.metadatadit.ProfileMetaDataEditActivity;
import com.vaidg.pro.main.profile.profiledetail.metadatadit.ProfileMetaDataEditDaggerModule;
import com.vaidg.pro.main.profile.profiledetail.metadatadit.model.ProfileMetaDataEditUtil;
import com.vaidg.pro.main.profile.support.SupportActivity;
import com.vaidg.pro.main.profile.support.SupportDaggerModule;
import com.vaidg.pro.main.profile.support.model.SupportUtil;
import com.vaidg.pro.main.profile.wallet.WalletActivity;
import com.vaidg.pro.main.profile.wallet.WalletActivityDaggerModule;
import com.vaidg.pro.main.profile.wallet.cardlist.CardListActivity;
import com.vaidg.pro.main.profile.wallet.cardlist.CardListDaggerModule;
import com.vaidg.pro.main.profile.wallet.model.CardListUtil;
import com.vaidg.pro.main.profile.wallet.model.WalletActivityUtil;
import com.vaidg.pro.main.profile.wallet.model.WalletTansactionUtil;
import com.vaidg.pro.main.profile.wallet.walletransaction.WalletTransactionActivity;
import com.vaidg.pro.main.profile.wallet.walletransaction.WalletTransactionDaggerModule;
import com.vaidg.pro.main.schedule.addschedule.ScheduleAddActivity;
import com.vaidg.pro.main.schedule.addschedule.ScheduleAddDaggerModule;
import com.vaidg.pro.main.schedule.addschedule.model.ScheduleAdUtil;
import com.vaidg.pro.main.schedule.bookingschedule.BookingScheduleActivity;
import com.vaidg.pro.main.schedule.bookingschedule.BookingScheduleDaggerModule;
import com.vaidg.pro.main.schedule.bookingschedule.model.BookingScheduleUtil;
import com.vaidg.pro.main.schedule.scheduleselection.ScheduleSelectionActivity;
import com.vaidg.pro.main.schedule.scheduleselection.ScheduleSelectionDaggerModule;
import com.vaidg.pro.main.schedule.scheduleselection.model.ScheduleSelectionUtil;
import com.vaidg.pro.main.schedule.viewschedule.ScheduleDaggerModule;
import com.vaidg.pro.main.schedule.viewschedule.ScheduleViewActivity;
import com.vaidg.pro.main.schedule.viewschedule.model.ScheduleViewUtil;
import com.vaidg.pro.otpverify.OTPVerifyActivity;
import com.vaidg.pro.otpverify.OTPVerifyDaggerModule;
import com.vaidg.pro.otpverify.model.OTPVerifyUtil;
import com.vaidg.pro.photoVidPreview.PhotoVidActivity;
import com.vaidg.pro.photoVidPreview.PhotoVidModule;
import com.vaidg.pro.photoVidPreview.PhotoVidUtilModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = {IntroSliderDaggerModule.class, IntroSliderUtil.class})
    abstract IntroSliderNewActivity introSliderNewActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {MainActivityDaggerModule.class, MainActivityUtil.class})
    abstract MainActivity mainActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {NotificationDaggerModule.class, NotificationActivityUtil.class})
    abstract NotificationActivity notificationActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {LogInDaggerModule.class, LogInUtil.class})
    abstract NewLogInActivity newLogInActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SignUpDaggerModule.class, SignUpUtil.class})
    abstract SignUpActivity signUpActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ScheduleDaggerModule.class, ScheduleViewUtil.class})
    abstract ScheduleViewActivity scheduleViewActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ScheduleAddDaggerModule.class, ScheduleAdUtil.class})
    abstract ScheduleAddActivity scheduleAddActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {AcceptRejectDaggerModule.class, AcceptRejectActivityUtil.class})
    abstract AcceptRejectActivity acceptRejectActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ArrivedDaggerModule.class, ArrivedActivityUtil.class})
    abstract ArrivedActivity arrivedActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EventStartedCompletedDaggerModule.class, EventStartedCompletedActivityUtil.class})
    abstract EventStartedCompletedActivity eventStartedCompletedActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {InCallDaggerModule.class, InCallActivityUtil.class})
    abstract InCallActivity inCallActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {BookingScheduleDaggerModule.class, BookingScheduleUtil.class})
    abstract BookingScheduleActivity bookingScheduleActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {CardListDaggerModule.class, CardListUtil.class})
    abstract CardListActivity cardListActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {WalletTransactionDaggerModule.class, WalletTansactionUtil.class})
    abstract WalletTransactionActivity walletTransactionActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {BankNewStripeDaggerModule.class, BankNewStripeUtil.class})
    abstract BankNewStripeActivity bankNewStripeActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {BankRazorPayDaggerModule.class, BankRazorPayUtil.class})
    abstract BankRazorPayActivity bankRazorPayActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {BankNewAccountDaggerModule.class, BankNewAccountUtil.class})
    abstract BankNewAccountActivity bankNewAccountActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {BankRazorPayAccountDaggerModule.class, BankRazorPayAccountUtil.class})
    abstract BankRazorPayAccountActivity bankRazorPayAccountActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {BankDetailsDaggerModule.class, BankDetailsUtil.class})
    abstract BankDetailsActivity bankDetailsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {HistoryInvoiceDaggerModule.class, BookingScheduleUtil.class})
    abstract HistoryInvoiceActivity historyInvoiceActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {WalletActivityDaggerModule.class, WalletActivityUtil.class})
    abstract WalletActivity walletActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ScheduleSelectionDaggerModule.class, ScheduleSelectionUtil.class})
    abstract ScheduleSelectionActivity scheduleSelectionActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {InvoiceDaggerModule.class, InvoiceActivityUtil.class})
    abstract InvoiceActivity invoiceActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {MedicationDaggerModule.class, MedicationUtil.class})
    abstract MedicationActivity medicationActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {AddMedicationDaggerModule.class, AddMedicationUtil.class})
    abstract AddMedicationActivity addMedicationActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {OnTheWayDaggerModule.class, OnTheWayActivityUtil.class})
    abstract OnTheWayActivity onTheWayActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {RateCustomerDaggerModule.class, RateCustomerUtil.class})
    abstract RateCustomerActivity rateCustomerActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {TeleCallActivityDaggerModule.class, TeleCallActivityUtil.class})
    abstract TeleCallActivity teleCallActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SelectCityDaggerModule.class, SelectCityUtil.class})
    abstract SelectCityActivity getSelectCityActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SelectSpecializationDaggerModule.class, SelectSpecializationUtil.class})
    abstract SelectSpecializationActivity getSelectSpecializationActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {AddClinicDaggerModule.class})
    abstract AddClinicActivity getAddClinicActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {FindHospitalDaggerModule.class, FindHospitalUtil.class})
    abstract FindHospitalActivity getFindHospitalActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SelfOwnedClinicDaggerModule.class, SelfOwnedClinicUtil.class})
    abstract SelfOwnedClinicActivity getSelfOwnedClinicActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EducationDetailsModule.class, EducationDetailsUtil.class})
    abstract EducationDetailsActivity getEducationDetailsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {AddEditEducationDaggerModule.class})
    abstract AddEditEducationActivity getAddEditEducationActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SelectDegreeDaggerModule.class, SelectDegreeUtil.class})
    abstract SelectDegreeActivity getSelectDegreeActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {YearOfExpDaggerModule.class})
    abstract YearOfExpActivity getYearOfExpActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ConsultationFeeDaggerModule.class, ConsultationFeeUtil.class})
    abstract ConsultationFeeActivity getConsultationFeeActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SpecializationCategoryDaggerModule.class, SpecializationCategoryUtil.class})
    abstract SpecializationCategoryActivity getSpecializationCategoryActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {PhotoVidModule.class, PhotoVidUtilModule.class})
    abstract PhotoVidActivity photoVidActivity();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract LocationActivity getLocationActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ProfileDetailDaggerModule.class, ProfileDetailUtil.class})
    abstract ProfileDetailActivity getProfileDetailActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {HelpWithPasswordDaggerModule.class, HelpWithPasswordUtil.class})
    abstract HelpWithPasswordActivity getHelpWithPasswordActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {OTPVerifyDaggerModule.class, OTPVerifyUtil.class})
    abstract OTPVerifyActivity getOtpVerifyActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ChangePasswordDaggerModule.class, ChangePasswordUtil.class})
    abstract ChangePasswordActivity getChangePasswordActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditEmailPhoneDaggerModule.class, EditEmailPhoneUtil.class})
    abstract EditEmailPhoneActivity getEditEmailPhoneActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {HistoryGraphDaggerModule.class, HistoryGraphUtil.class})
    abstract HistoryGraphActivity getHistoryGraphActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SupportDaggerModule.class, SupportUtil.class})
    abstract SupportActivity getSupportActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EarningWebDaggerModule.class, EarningWebUtil.class})
    abstract EarningWebActivity getEarningWebActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ProfileMetaDataEditDaggerModule.class, ProfileMetaDataEditUtil.class})
    abstract ProfileMetaDataEditActivity getProfileMetaDataEditActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {HelpCenterDaggerModule.class, HelpCenterUtil.class})
    abstract HelpCenterActivity getHelpCenterActivity();

     @ActivityScoped
    @ContributesAndroidInjector(modules = {NewTicketDaggerModule.class, NewTicketUtil.class})
    abstract NewTicketActivity getNewTicketActivity();
}
