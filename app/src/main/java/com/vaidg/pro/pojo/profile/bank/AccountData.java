package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AccountData implements Serializable {

  private boolean isDefault = false;
  private boolean isSelected;
  @SerializedName("country")
  @Expose
  private String country;
  @SerializedName("last4")
  @Expose
  private String last4;
  @SerializedName("metadata")
  @Expose
  private Metadata metadata;
  @SerializedName("account_holder_name")
  @Expose
  private String accountHolderName;
  @SerializedName("routing_number")
  @Expose
  private String routingNumber;
  @SerializedName("account_holder_type")
  @Expose
  private String accountHolderType;
  @SerializedName("bank_name")
  @Expose
  private String bankName;
  @SerializedName("fingerprint")
  @Expose
  private String fingerprint;
  @SerializedName("currency")
  @Expose
  private String currency;
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("default_for_currency")
  @Expose
  private boolean defaultForCurrency;
  @SerializedName("account")
  @Expose
  private String account;
  @SerializedName("object")
  @Expose
  private String object;
  @SerializedName("status")
  @Expose
  private String status;

  public String getCountry() {
    return country;
  }

  public String getLast4() {
    return last4;
  }

  public Metadata getMetadata() {
    return metadata;
  }

  public String getAccountHolderName() {
    return accountHolderName;
  }

  public String getRoutingNumber() {
    return routingNumber;
  }

  public String getAccountHolderType() {
    return accountHolderType;
  }

  public String getBankName() {
    return bankName;
  }

  public String getFingerprint() {
    return fingerprint;
  }

  public String getCurrency() {
    return currency;
  }

  public String getId() {
    return id;
  }

  public boolean isDefaultForCurrency() {
    return defaultForCurrency;
  }

  public String getAccount() {
    return account;
  }

  public String getObject() {
    return object;
  }

  public String getStatus() {
    return status;
  }

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}

	@Override
  public String toString() {
    return "AccountData{" + "country = '" + country + '\'' + ",last4 = '" + last4 + '\'' + "," +
				"metadata = '" + metadata + '\'' + ",account_holder_name = '" + accountHolderName + '\'' + ",routing_number = '" + routingNumber + '\'' + ",account_holder_type = '" + accountHolderType + '\'' + ",bank_name = '" + bankName + '\'' + ",fingerprint = '" + fingerprint + '\'' + ",currency = '" + currency + '\'' + ",id = '" + id + '\'' + ",default_for_currency = '" + defaultForCurrency + '\'' + ",account = '" + account + '\'' + ",object = '" + object + '\'' + ",status = '" + status + '\'' + "}";
  }

  public boolean isDefault() {
    return isDefault;
  }

  public void setDefault(boolean aDefault) {
    this.isDefault = aDefault;
  }
}