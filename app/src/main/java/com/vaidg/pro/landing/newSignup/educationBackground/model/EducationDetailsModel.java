package com.vaidg.pro.landing.newSignup.educationBackground.model;

import com.vaidg.pro.BaseModel;
import com.vaidg.pro.pojo.profile.DegreeDetailsData;

import java.util.ArrayList;

import javax.inject.Inject;

public class EducationDetailsModel extends BaseModel {

    @Inject
    ArrayList<DegreeDetailsData> arrayList;

    @Inject
    public EducationDetailsModel() {
    }

    public void addAllDataToList(ArrayList<DegreeDetailsData> data) {
        arrayList.addAll(data);
    }

    public void addDataToList(DegreeDetailsData pojo) {
        arrayList.add(pojo);
    }

    public DegreeDetailsData getPositionData(int position) {
        return arrayList.get(position);
    }

    public boolean isEditedData(DegreeDetailsData pojo) {
        return pojo.isEdited();
    }

    public void addEditData(int position, DegreeDetailsData pojo) {
        arrayList.set(position, pojo);
    }

    public ArrayList<DegreeDetailsData> getdata() {
        return arrayList;
    }
}
