package com.vaidg.pro.pojo.profile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vaidg.pro.pojo.profile.metaData.MetaDataArr;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by murashid on 10-Sep-17.
 */

public class ProfileData implements Parcelable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("profileStatus")
    @Expose
    private String profileStatus;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("radius")
    @Expose
    private String radius;
    @SerializedName("minRadius")
    @Expose
    private String minRadius;
    @SerializedName("maxRadius")
    @Expose
    private String maxRadius;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("currencySymbol")
    @Expose
    private String currencySymbol;
    @SerializedName("metaDataArr")
    @Expose
    private List<MetaDataArr> metaDataArr = new ArrayList<>();
    @SerializedName("doctorType")
    @Expose
    private String doctorType;
    @SerializedName("hospitalId")
    @Expose
    private String hospitalId;
    @SerializedName("hospitalName")
    @Expose
    private String hospitalName;
    @SerializedName("clinicName")
    @Expose
    private String clinicName;
    @SerializedName("clinicLogoWeb")
    @Expose
    private String clinicLogoWeb;
    @SerializedName("clinicLogoApp")
    @Expose
    private String clinicLogoApp;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("yearOfExp")
    @Expose
    private String yearOfExp;
    @SerializedName("degree")
    @Expose
    private List<DegreeDetailsData> degree = new ArrayList<>();
    @SerializedName("inCallFee")
    @Expose
    private String inCallFee;
    @SerializedName("outCallFee")
    @Expose
    private String outCallFee;
    @SerializedName("teleCallFee")
    @Expose
    private String teleCallFee;
    @SerializedName("cityName")
    @Expose
    private String cityName;
    @SerializedName("minimumFeesForConsultancy")
    @Expose
    private String minimumFeesForConsultancy;
    @SerializedName("maximumFeesForConsultancy")
    @Expose
    private String maximumFeesForConsultancy;
    @SerializedName("catName")
    @Expose
    private String catName;
    @SerializedName("catNameArr")
    @Expose
    private List<String> catNameArr = new ArrayList<>();
    public final static Parcelable.Creator<ProfileData> CREATOR = new Creator<ProfileData>() {

        public ProfileData createFromParcel(Parcel in) {
            return new ProfileData(in);
        }

        public ProfileData[] newArray(int size) {
            return (new ProfileData[size]);
        }

    };

    protected ProfileData(Parcel in) {
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.firstName = ((String) in.readValue((String.class.getClassLoader())));
        this.lastName = ((String) in.readValue((String.class.getClassLoader())));
        this.profilePic = ((String) in.readValue((String.class.getClassLoader())));
        this.mobile = ((String) in.readValue((String.class.getClassLoader())));
        this.countryCode = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.profileStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.radius = ((String) in.readValue((String.class.getClassLoader())));
        this.minRadius = ((String) in.readValue((String.class.getClassLoader())));
        this.maxRadius = ((String) in.readValue((String.class.getClassLoader())));
        this.currency = ((String) in.readValue((String.class.getClassLoader())));
        this.currencySymbol = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.metaDataArr, (MetaDataArr.class.getClassLoader()));
        this.doctorType = ((String) in.readValue((String.class.getClassLoader())));
        this.hospitalId = ((String) in.readValue((String.class.getClassLoader())));
        this.hospitalName = ((String) in.readValue((String.class.getClassLoader())));
        this.clinicName = ((String) in.readValue((String.class.getClassLoader())));
        this.clinicLogoWeb = ((String) in.readValue((String.class.getClassLoader())));
        this.clinicLogoApp = ((String) in.readValue((String.class.getClassLoader())));
        this.gender = ((String) in.readValue((String.class.getClassLoader())));
        this.dob = ((String) in.readValue((String.class.getClassLoader())));
        this.yearOfExp = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.degree, (DegreeDetailsData.class.getClassLoader()));
        this.inCallFee = ((String) in.readValue((String.class.getClassLoader())));
        this.outCallFee = ((String) in.readValue((String.class.getClassLoader())));
        this.teleCallFee = ((String) in.readValue((String.class.getClassLoader())));
        this.cityName = ((String) in.readValue((String.class.getClassLoader())));
        this.minimumFeesForConsultancy = ((String) in.readValue((String.class.getClassLoader())));
        this.maximumFeesForConsultancy = ((String) in.readValue((String.class.getClassLoader())));
        this.catName = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.catNameArr, (java.lang.String.class.getClassLoader()));
    }

    public ProfileData() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getMinRadius() {
        return minRadius;
    }

    public void setMinRadius(String minRadius) {
        this.minRadius = minRadius;
    }

    public String getMaxRadius() {
        return maxRadius;
    }

    public void setMaxRadius(String maxRadius) {
        this.maxRadius = maxRadius;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public List<MetaDataArr> getMetaDataArr() {
        return metaDataArr;
    }

    public void setMetaDataArr(List<MetaDataArr> metaDataArr) {
        this.metaDataArr = metaDataArr;
    }

    public String getDoctorType() {
        return doctorType;
    }

    public void setDoctorType(String doctorType) {
        this.doctorType = doctorType;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getClinicName() {
        return clinicName;
    }

    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }

    public String getClinicLogoWeb() {
        return clinicLogoWeb;
    }

    public void setClinicLogoWeb(String clinicLogoWeb) {
        this.clinicLogoWeb = clinicLogoWeb;
    }

    public String getClinicLogoApp() {
        return clinicLogoApp;
    }

    public void setClinicLogoApp(String clinicLogoApp) {
        this.clinicLogoApp = clinicLogoApp;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getYearOfExp() {
        return yearOfExp;
    }

    public void setYearOfExp(String yearOfExp) {
        this.yearOfExp = yearOfExp;
    }

    public List<DegreeDetailsData> getDegree() {
        return degree;
    }

    public void setDegree(List<DegreeDetailsData> degree) {
        this.degree = degree;
    }

    public String getInCallFee() {
        return inCallFee;
    }

    public void setInCallFee(String inCallFee) {
        this.inCallFee = inCallFee;
    }

    public String getOutCallFee() {
        return outCallFee;
    }

    public void setOutCallFee(String outCallFee) {
        this.outCallFee = outCallFee;
    }

    public String getTeleCallFee() {
        return teleCallFee;
    }

    public void setTeleCallFee(String teleCallFee) {
        this.teleCallFee = teleCallFee;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getMinimumFeesForConsultancy() {
        return minimumFeesForConsultancy;
    }

    public void setMinimumFeesForConsultancy(String minimumFeesForConsultancy) {
        this.minimumFeesForConsultancy = minimumFeesForConsultancy;
    }

    public String getMaximumFeesForConsultancy() {
        return maximumFeesForConsultancy;
    }

    public void setMaximumFeesForConsultancy(String maximumFeesForConsultancy) {
        this.maximumFeesForConsultancy = maximumFeesForConsultancy;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public List<String> getCatNameArr() {
        return catNameArr;
    }

    public void setCatNameArr(List<String> catNameArr) {
        this.catNameArr = catNameArr;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(title);
        dest.writeValue(firstName);
        dest.writeValue(lastName);
        dest.writeValue(profilePic);
        dest.writeValue(mobile);
        dest.writeValue(countryCode);
        dest.writeValue(email);
        dest.writeValue(profileStatus);
        dest.writeValue(address);
        dest.writeValue(radius);
        dest.writeValue(minRadius);
        dest.writeValue(maxRadius);
        dest.writeValue(currency);
        dest.writeValue(currencySymbol);
        dest.writeList(metaDataArr);
        dest.writeValue(doctorType);
        dest.writeValue(hospitalId);
        dest.writeValue(hospitalName);
        dest.writeValue(clinicName);
        dest.writeValue(clinicLogoWeb);
        dest.writeValue(clinicLogoApp);
        dest.writeValue(gender);
        dest.writeValue(dob);
        dest.writeValue(yearOfExp);
        dest.writeList(degree);
        dest.writeValue(inCallFee);
        dest.writeValue(outCallFee);
        dest.writeValue(teleCallFee);
        dest.writeValue(cityName);
        dest.writeValue(minimumFeesForConsultancy);
        dest.writeValue(maximumFeesForConsultancy);
        dest.writeValue(catName);
        dest.writeList(catNameArr);
    }

    public int describeContents() {
        return 0;
    }
}
