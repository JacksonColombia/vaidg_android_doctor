package com.vaidg.pro.main.profile.helpcenter;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.main.profile.editemailphone.EditEmailPhoneActivity;
import com.vaidg.pro.main.profile.editemailphone.EditEmailPhonePresenterImple;
import com.vaidg.pro.pojo.profile.helpcenter.Ticket;
import com.vaidg.pro.pojo.profile.helpcenter.TicketData;
import java.util.ArrayList;
import org.json.JSONObject;

/**
 * <h1>EditEmailPhoneContract</h1>
 * <p>This contract define interface with required methods for view and presenter</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @see EditEmailPhoneActivity
 * @see EditEmailPhonePresenterImple
 * @since 01/07/2020
 **/
public interface NewTicketContract {

    interface View extends BaseView {

        void startProgressBar();

        void stopProgressBar();

        void onFailure(String msg);

        void onFailure();

        void onSuccessRaiseTicket(String msg);

        void onSuccessComment();

        void onSuccessTicket(TicketData ticket);

        void onErrorSubject();

        void onErrorPriority();

        void onErrorComment();
    }

    interface Presenter extends BasePresenter {

        void getTicketDetails(String sessionToken, String ticketId);

        void sendComment(String sessionToken, JSONObject jsonObject);

        void createTicket(String sessionToken, JSONObject jsonObject);}
}
