package com.vaidg.pro.changepassword;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;

/**
 * <h1>ChangePasswordContract</h1>
 * <p>This contract define interface with required methods for view and presenter</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @see com.vaidg.pro.changepassword.ChangePasswordActivity
 * @see ChangePasswordPresenterImple
 * @since 24/06/2020
 **/
public interface ChangePasswordContract {

    interface View extends BaseView {

        /**
         * <p>This method is called when password change successfully.</p>
         *
         * @param msg the success message come in response
         */
        void onSuccess(String msg);

        /**
         * <p>This method is called when entered old password is wrong.</p>
         */
        void onOldPasswordError();

        /**
         * <p>This method is called when entered new password is invalid.</p>
         */
        void onNewPasswordError();

        /**
         * <p>This method is called when re entered password in invalid.</p>
         */
        void onConfirmPasswordError();

        /**
         * <p>This method is called when new password and re entered password is didn't matched.</p>
         */
        void onPasswordMismatch();

        /**
         * <p>This method is display passed error message in snack bar.</p>
         *
         * @param error the error message
         */
        void showError(String error);
    }

    interface Presenter extends BasePresenter {

        /**
         * <p>This method is check entered password is valid as per defined password standard.</p>
         *
         * @param password the entered password
         * @return true if password is correct
         */
        boolean isValidPassword(String password);

        /**
         * <p>This method is check entered password and confirm password are valid then call API for update password.</p>
         *
         * @param userId          the user id
         * @param password        the entered password
         * @param confirmPassword the re-entered password
         */
        void forgotPassword(String userId, String password, String confirmPassword);


        /**
         * <p>This method is check entered old password, new password and re-entered password are valid then
         * call API for update new password.</p>
         *
         * @param oldPassword     the old password
         * @param newPassword     the new password
         * @param confirmPassword the re-entered new password
         */
        void changePassword(String oldPassword, String newPassword, String confirmPassword);
    }
}
