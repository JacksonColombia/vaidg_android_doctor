package com.vaidg.pro.main.history;

import android.content.Context;

import com.github.mikephil.charting.data.BarEntry;
import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.history.HistoryPojo;
import com.vaidg.pro.pojo.history.HistoryWeekData;
import com.vaidg.pro.pojo.history.HistoryWeekPojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

/**
 * <h1>HistoryGraphPresenterImple</h1>
 * <p>This presenter is define to handle activity interactions done by user.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/07/2020
 **/
public class HistoryGraphPresenterImple implements HistoryGraphContract.Presenter {

    @Inject
    HistoryGraphContract.View view;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;
    @Inject
    Context context;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    NetworkService service;

    private boolean isCurrentWeek = false;

    @Inject
    public HistoryGraphPresenterImple() {
    }

    @Override
    public void getBookingHistoryByWeek() {

/*
        if (sessionManager.getHistoryWeekData() != null && !sessionManager.getHistoryWeekData().isEmpty()) {
            prepareChartData(sessionManager.getHistoryWeekData());
            return;
        }
*/

        if (networkStateHolder.isConnected()) {
            if (view != null)
            view.showProgress();
            service.bookingHistoryByWeek(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        if (view != null)
                                            view.hideProgress();
                                        if (response != null && !response.isEmpty()) {
                                            prepareChartData(response);
                                        } else {
                                            if (view != null) {
                                                view.onFailure();
                                            }
                                        }
                                        break;
                                    case SESSION_EXPIRED:
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                getBookingHistoryByWeek();
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                if (view != null) {
                                                    view.hideProgress();
                                                    view.onFailure();
                                                }
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                if (view != null)
                                                view.hideProgress();
                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, context);
                                            }
                                        });
                                        break;
                                    case SESSION_LOGOUT:
                                        if (view != null)
                                        view.hideProgress();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                        break;
                                    default:
                                        if (view != null) {
                                            view.hideProgress();
                                            view.showError(Utility.getMessage(errorBody));
                                        }
                                        break;
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.onFailure();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.onFailure();
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(context.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void prepareChartData(String data) {

        sessionManager.setHistoryWeekData(data);

        HistoryWeekPojo historyWeekPojo = gson.fromJson(data, HistoryWeekPojo.class);

        SimpleDateFormat xAxisFormat = new SimpleDateFormat("EEE", Locale.US);
        xAxisFormat.setTimeZone(getInstance().getTimeZone());
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTimeZone(getInstance().getTimeZone());
        c.setTime(date);

        ArrayList<ArrayList<BarEntry>> wholeBarEntries = new ArrayList<>();
        ArrayList<Integer> highestPosition = new ArrayList<>();
        ArrayList<String> apiFormatDates = new ArrayList<>();
        ArrayList<String> tabFormatDates = new ArrayList<>();

        SimpleDateFormat tabDateFormat = new SimpleDateFormat("MMM dd", Locale.US);
        tabDateFormat.setTimeZone(getInstance().getTimeZone());
        if (historyWeekPojo.getData().size() > 0) {
            for (HistoryWeekData historyWeekData : historyWeekPojo.getData()) {
                c.setTimeInMillis(Utility.convertUTCToTimeStamp(historyWeekData.getEndDate()));

                apiFormatDates.add(historyWeekData.getsDate());
                tabFormatDates.add(tabDateFormat.format(Utility.convertUTCToTimeStamp(historyWeekData.getStartDate())) + "-" + tabDateFormat.format(Utility.convertUTCToTimeStamp(historyWeekData.getEndDate())));

                ArrayList<BarEntry> barEntries = new ArrayList<>();
                int highestVal = 0, position = 0;
                for (int i = 0; i < historyWeekData.getCount().size(); i++) {
                    barEntries.add(new BarEntry(i, historyWeekData.getCount().get(i)));
                    if (highestVal < historyWeekData.getCount().get(i)) {
                        highestVal = historyWeekData.getCount().get(i);
                        position = i;
                    }
                }
                wholeBarEntries.add(barEntries);
                highestPosition.add(position);
            }

            view.hideProgress();

            ArrayList<String> days = new ArrayList<>();

            c.add(Calendar.DATE, -6);
            days.add(xAxisFormat.format(c.getTime()).toUpperCase());
            c.add(Calendar.DATE, +1);
            days.add(xAxisFormat.format(c.getTime()).toUpperCase());
            c.add(Calendar.DATE, +1);
            days.add(xAxisFormat.format(c.getTime()).toUpperCase());
            c.add(Calendar.DATE, +1);
            days.add(xAxisFormat.format(c.getTime()).toUpperCase());
            c.add(Calendar.DATE, +1);
            days.add(xAxisFormat.format(c.getTime()).toUpperCase());
            c.add(Calendar.DATE, +1);
            days.add(xAxisFormat.format(c.getTime()).toUpperCase());
            c.add(Calendar.DATE, +1);
            days.add(xAxisFormat.format(c.getTime()).toUpperCase());

            view.setTabBarChart(wholeBarEntries, highestPosition, apiFormatDates, tabFormatDates, days);
        }
    }

    @Override
    public void getHistory(String selectedDate, boolean isCurrentWeek) {
        this.isCurrentWeek = isCurrentWeek;

       /* if (!isCurrentWeek && sessionManager.getHistoryData() != null && !sessionManager.getHistoryData().isEmpty()) {
            prepareBookingHistoryData(sessionManager.getHistoryData());
            return;
        }
*/
        if (networkStateHolder.isConnected()) {
            if (view != null)
            view.showProgress();
            service.bookingHistory(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, selectedDate)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        if (view != null)
                                            view.hideProgress();
                                        if (response != null && !response.isEmpty()) {
                                            prepareBookingHistoryData(response);
                                        } else {
                                            if (view != null) {
                                                view.onFailure();
                                            }
                                        }
                                        break;
                                    case SESSION_EXPIRED:
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                getBookingHistoryByWeek();
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                if (view != null) {
                                                    view.hideProgress();
                                                    view.onFailure();
                                                }
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                if (view != null)
                                                view.hideProgress();
                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, context);
                                            }
                                        });
                                        break;
                                    case SESSION_LOGOUT:
                                        if (view != null)
                                        view.hideProgress();
                                        getInstance().toast(Utility.getMessage(errorBody));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                        break;
                                    default:
                                        if (view != null) {
                                            view.hideProgress();
                                            view.showError(Utility.getMessage(errorBody));
                                        }
                                        break;
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.onFailure();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.onFailure();
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(context.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void prepareBookingHistoryData(String data) {

        if (isCurrentWeek) {
            sessionManager.setHistoryData(data);
        }

        HistoryPojo historyPojo = gson.fromJson(data, HistoryPojo.class);
        double amountEarned = 0;
        for (int i = 0; i < historyPojo.getData().size(); i++) {
            amountEarned += Double.parseDouble(historyPojo.getData().get(i).getAccounting().getTotal());
        }

        view.hideProgress();
        view.onSuccessBookingHistory(historyPojo.getData(), String.valueOf(amountEarned));

    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }
}
