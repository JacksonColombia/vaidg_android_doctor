package com.vaidg.pro.main.profile.bank;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Context;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.ServiceFactory;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 29-Aug-17.
 * <h1>BankBottomSheetModel</h1>
 * BankBottomSheetModel model for BankBottomSheetFragment
 *
 * @see BankBottomSheetFragment
 */

public class BankBottomSheetModel {

    private static final String TAG = "BankBottomSheet";

    private BankBottomSheetModelImplem bankBottomSheetModelImplem;

    private NetworkService service;

    private Gson gson;

    private Context mContext;

    private SessionManager sessionManager;

    BankBottomSheetModel(BankBottomSheetModelImplem bankBottomSheetModelImplem, Context context) {
        this.bankBottomSheetModelImplem = bankBottomSheetModelImplem;
        this.mContext = context;
        service = ServiceFactory.getClient(NetworkService.class,AppController.getContext());
        gson = new Gson();
        sessionManager = SessionManager.getSessionManager(context);
    }

    /**
     * method for calling api for set the default bank account
     *
     * @param jsonObject required fiels in json Object
     */
    void setDefaultAccount(JSONObject jsonObject) {

        service.editExternalAccount(Utility.getAuth(sessionManager.getEmail()),DEFAULT_LANGUAGE, PLATFORM,jsonObject.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if (response != null && !response.isEmpty()) {
                                    bankBottomSheetModelImplem.onSuccess(response);
                                } else {
                                    bankBottomSheetModelImplem.onFailure();
                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        setDefaultAccount(jsonObject);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        bankBottomSheetModelImplem.onFailure();
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        getInstance().toast(Utility.getMessage(msg));
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                                getInstance().toast(Utility.getMessage(errorBody));
                                break;
                            default:
                                getInstance().toast(Utility.getMessage(errorBody));
                                bankBottomSheetModelImplem.onFailure(Utility.getMessage(errorBody));
                                break;
                        }
                    } catch (Exception e) {
                        bankBottomSheetModelImplem.onFailure();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    bankBottomSheetModelImplem.onFailure();
                }

                @Override
                public void onComplete() {
                }
            });

    }

    /**
     * method for calinng the api for deleting the bank account
     *
     * @param jsonObject required field in Json Object
     */
    void deleteBankAccount(JSONObject jsonObject) {
        service.deleteExternalAccount(Utility.getAuth(sessionManager.getEmail()),DEFAULT_LANGUAGE, PLATFORM,jsonObject.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if (response != null && !response.isEmpty()) {
                                    bankBottomSheetModelImplem.onSuccess(response);
                                } else {
                                    bankBottomSheetModelImplem.onFailure();
                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        deleteBankAccount(jsonObject);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        bankBottomSheetModelImplem.onFailure();
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        getInstance().toast(Utility.getMessage(msg));
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                                getInstance().toast(Utility.getMessage(errorBody));
                                break;
                            default:
                                getInstance().toast(Utility.getMessage(errorBody));
                                bankBottomSheetModelImplem.onFailure(Utility.getMessage(errorBody));
                                break;
                        }
                    } catch (Exception e) {
                        bankBottomSheetModelImplem.onFailure();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    bankBottomSheetModelImplem.onFailure();
                }

                @Override
                public void onComplete() {
                }
            });
    }

    /**
     * BankBottomSheetModelImplem interface for Presenter implementation
     */
    interface BankBottomSheetModelImplem {
        void onFailure(String msg);

        void onFailure();

        void onSuccess(String msg);
    }
}
