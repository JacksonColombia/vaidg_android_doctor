package com.vaidg.pro.bookingflow;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.AMAZON_BASE_URL;
import static com.vaidg.pro.utility.VariableConstant.BUCKET_NAME;
import static com.vaidg.pro.utility.VariableConstant.NOT_FOUND;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.vaidg.pro.bookingflow.acceptReject.AcceptRejectActivity;
import com.vaidg.pro.bookingflow.telecall.TeleCallActivity;
import com.vaidg.pro.mqtt.MqttHelper;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.pojo.booking.CancelPojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.File;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 31-Oct-17.
 * <h1>UpdateStatusPresenter</h1>
 * UpdateStatusPresenter presenter for update status in AcceptRejectAcitivty
 *
 * @see AcceptRejectActivity
 */
public class UpdateStatusPresenterImple implements UpdateStatusContract.Presenter {
  @Inject
  UpdateStatusContract.View view;
  @Inject
  SessionManager sessionManager;
  @Inject
  Gson gson;
  @Inject
  NetworkService service;
  @Inject
  Context mContext;
  private String TAG = UpdateStatusPresenterImple.class.getSimpleName();
  private MqttHelper mqttHelper;

  @Inject
  UpdateStatusPresenterImple() {
  }

  @Override
  public void getBookingById(String bookingId) {
    if(view != null)
    view.startProgressBar();
    service.getBooking(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  if(view != null)
                  view.stopProgressBar();
                  if (response != null && !response.isEmpty()) {
                    if(view != null)
                    view.onSuccess(Utility.getData(response), true);
                  } else {
                    if (view != null)
                      view.onFailure();
                  }
                  break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      getBookingById(bookingId);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                      if(view != null) {
                        view.stopProgressBar();
                        view.onFailure();
                      }
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if (view != null) {
                        view.stopProgressBar();
                        view.sessionExpired(Utility.getMessage(errorBody));
                      }
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  if(view != null) {
                    view.stopProgressBar();
                    view.sessionExpired(Utility.getMessage(errorBody));
                  }
                  break;
                default:
                  if(view != null) {
                    view.stopProgressBar();
                    view.onFailure(Utility.getMessage(errorBody));
                  }
                  break;
              }
            } catch (Exception e) {
              if(view != null) {
                view.stopProgressBar();
                view.onFailure();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.stopProgressBar();
              view.onFailure();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void acceptRejectJob(JSONObject jsonObject) {
    service.getBookingResponse(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  if(view != null)
                  view.stopProgressBar();
                  if (response != null && !response.isEmpty()) {
                    if(view != null)
                    view.onSuccess(Utility.getMessage(response), false);
                  } else {
                    if(view != null)
                    view.onFailure();
                  }
                  break;
                case NOT_FOUND:
                  if(view != null)
                  view.stopProgressBar();
                  if (errorBody != null && !errorBody.isEmpty()) {
                    if(view != null)
                    view.onCancelBooking(Utility.getMessage(errorBody));
                  } else {
                    if (view != null)
                      view.onFailure();
                  }
                  break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      acceptRejectJob(jsonObject);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().toast(Utility.getMessage(msg));
                      Utility.logoutSessionExiperd(sessionManager, mContext);
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  Utility.logoutSessionExiperd(sessionManager, mContext);
                  break;
                default:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  break;
              }
            } catch (Exception e) {
              if(view != null) {
                view.stopProgressBar();
                view.onFailure();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.stopProgressBar();
              view.onFailure();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void updateStaus(JSONObject jsonObject, boolean isTelecallBooking) {
    service.getBookingStatus(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  if(view != null)
                  view.stopProgressBar();
                  if (response != null && !response.isEmpty()) {
                    if (isTelecallBooking) {
                      if(view != null)
                      view.onSuccess(response, false);
                    } else {
                      if(view != null)
                      view.onSuccess(response, false);
                    }
                  } else {
                    if (view != null)
                      view.onFailure();
                  }
                  break;
                case NOT_FOUND:
                  if(view != null)
                  view.stopProgressBar();
                  if (errorBody != null && !errorBody.isEmpty()) {
                    if(view != null)
                    view.onCancelBooking(Utility.getMessage(errorBody));
                  } else {
                    if(view != null)
                    view.onFailure();
                  }break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      updateStaus(jsonObject, isTelecallBooking);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().toast(Utility.getMessage(msg));
                      Utility.logoutSessionExiperd(sessionManager, mContext);
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  Utility.logoutSessionExiperd(sessionManager, mContext);
                  break;
                default:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  break;
              }
            } catch (Exception e) {
              if(view != null) {
                view.stopProgressBar();
                view.onFailure();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.stopProgressBar();
              view.onFailure();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void updateStaus(JSONObject jsonObject, JSONObject jsonObjectTimer, boolean isFromUpdateStatus) {
    service.getBookingStatus(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  if(view != null)
                  view.stopProgressBar();
                  if (response != null && !response.isEmpty()) {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    if (jsonObject.get("status").equals(VariableConstant.JOB_TIMER_STARTED)) {
                      updateTimer(jsonObjectTimer, isFromUpdateStatus);
                    } else {
                      if(view != null)
                      view.onSuccess(response, false);
                    }
                  } else {
                    if (view != null)
                      view.onFailure();
                  }                  break;
                case NOT_FOUND:
                  if(view != null)
                  view.stopProgressBar();
                  if (errorBody != null && !errorBody.isEmpty()) {
                    if(view != null)
                    view.onCancelBooking(Utility.getMessage(errorBody));
                  } else {
                    if (view != null)
                      view.onFailure();
                  }
                  break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      updateStaus(jsonObject, jsonObjectTimer, isFromUpdateStatus);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().toast(Utility.getMessage(msg));
                      Utility.logoutSessionExiperd(sessionManager, mContext);
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  Utility.logoutSessionExiperd(sessionManager, mContext);
                  break;
                default:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  break;
              }
            } catch (Exception e) {
              if(view != null) {
                view.stopProgressBar();
                view.onFailure();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.stopProgressBar();
              view.onFailure();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void updateStaus(JSONObject jsonObject, UploadFileAmazonS3 amazonS3, File mFileTemp) {
    service.getBookingStatus(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  if(view != null)
                  view.stopProgressBar();
                  if (response != null && !response.isEmpty()) {
                    if(view != null)
                    view.onSuccess(Utility.getMessage(response), false);
                   // amazonUpload(amazonS3, mFileTemp);
                  } else {
                    if(view != null)
                    view.onFailure();
                  }  break;
                case NOT_FOUND:
                  if(view != null)
                  view.stopProgressBar();
                  if (errorBody != null && !errorBody.isEmpty()) {
                    if(view != null)
                    view.onCancelBooking(Utility.getMessage(errorBody));
                  } else {
                    if (view != null)
                      view.onFailure();
                  }
                  break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      updateStaus(jsonObject, amazonS3, mFileTemp);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().toast(Utility.getMessage(msg));
                      Utility.logoutSessionExiperd(sessionManager, mContext);
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  Utility.logoutSessionExiperd(sessionManager, mContext);
                  break;
                default:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  break;
              }
            } catch (Exception e) {
              if(view != null) {
                view.stopProgressBar();
                view.onFailure();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.stopProgressBar();
              view.onFailure();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void updateTimer(JSONObject jsonObject, boolean isFromUpdateStatus) {
    service.getBookingTimer(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  if(view != null)
                  view.stopProgressBar();
                  if (response != null && !response.isEmpty()) {
                    if (isFromUpdateStatus) {
                      if(view != null)
                      view.onSuccess(Utility.getMessage(response), false);
                    } else {
                      if(view != null)
                      view.stopProgressBar();
                    }
                  } else {
                    if(view != null)
                    view.onFailure();
                  }
                  break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      updateTimer(jsonObject, isFromUpdateStatus);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().toast(Utility.getMessage(msg));
                      Utility.logoutSessionExiperd(sessionManager, mContext);
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  Utility.logoutSessionExiperd(sessionManager, mContext);
                  break;
                default:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  break;
              }
            } catch (Exception e) {
              if(view != null) {
                view.stopProgressBar();
                view.onFailure();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.stopProgressBar();
              view.onFailure();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void getCancelReason(String bookingId) {
    service.getCancelReasons(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, bookingId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  if(view != null)
                  view.stopProgressBar();
                  if (response != null && !response.isEmpty()) {
                    CancelPojo cancelPojo = gson.fromJson(response, CancelPojo.class);
                    if(view != null)
                    view.onSuccessCancelReason(cancelPojo);
                  } else {
                    if(view != null)
                    view.onFailure();
                  } break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      getCancelReason(bookingId);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().toast(Utility.getMessage(msg));
                      Utility.logoutSessionExiperd(sessionManager, mContext);
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  Utility.logoutSessionExiperd(sessionManager, mContext);
                  break;
                default:
                  if(view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  break;
              }
            } catch (Exception e) {
              if(view != null) {
                view.stopProgressBar();
                view.onFailure();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.stopProgressBar();
              view.onFailure();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void initCallApi(String calltoken, String currentRoomId, String customerId, String callType, String bookingId) {
//        presenterImple.startProgressBar();
    JSONObject jsonObject = new JSONObject();
    try {
      jsonObject.put("room", currentRoomId);
      jsonObject.put("to", customerId);
      jsonObject.put("type", callType);
      jsonObject.put("bookingId", bookingId);
    } catch (JSONException e) {
      view.stopProgressBar();
      Toast.makeText(((TeleCallActivity) view).getApplicationContext(), "Some things went wrong", Toast.LENGTH_LONG).show();
      e.printStackTrace();
    }


/*
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(calltoken, ServiceUrl.CALL, OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
//                        presenterImple.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        String callId = "";
                                        try {
                                            callId = callingModel.parseCallerId(result);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        if (!Utility.isTextEmpty(callId)) {
                                            if (mqttHelper.isMqttConnected()) {
                                                if (presenterImple != null)
                                                    presenterImple.launchCallsScreen(currentRoomId, callId, callType);
                                            } else {
                                                Log.d(TAG, "onNext: can not subscribe to callId mqtt is not connected");
                                            }
                                        }
                                        break;
                                    case RESPONSE_CODE_ALREADY_IN_CALL:
                                        presenterImple.onFailure(jsonObject.getString("message"));
                                        break;
                                    default:
                                        Log.d(TAG, "ondefault: " + result);
                                        break;
                                }
                            } else {
                                Log.d(TAG, "ondefault: " + result);
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "ondefault: " + result);
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        presenterImple.stopProgressBar();
                        Toast.makeText(((TeleCallActivity) presenterImple).getApplicationContext(), "Some things went wrong", Toast.LENGTH_LONG).show();
                        Log.d(TAG, "onErroir: " + error);
                    }
                });
*/
  }

  @Override
  public void amazonUpload(UploadFileAmazonS3 amazonS3, File mFileTemp) {
    String BUCKETSUBFOLDER = VariableConstant.SIGNATURE_UPLOAD;
    final String imageUrl = "https://" + BUCKET_NAME + "." + AMAZON_BASE_URL
            + BUCKETSUBFOLDER + "/"
            + mFileTemp.getName();
    Log.d(TAG, "amzonUpload: " + imageUrl);
    amazonS3.Upload_data(BUCKETSUBFOLDER , mFileTemp.getName(), mFileTemp, new UploadFileAmazonS3.UploadCallBack() {
      @Override
      public void sucess(String success) {
        Log.d(TAG, "sucess: " + success);
        mFileTemp.delete();
      }

      @Override
      public void error(String errormsg) {
        Log.d(TAG, "error: " + errormsg);
      }
    });
  }

  @Override
  public void cancelBooking(JSONObject jsonObject) {
    service.getCancelBooking(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  if (view != null)
                  view.stopProgressBar();
                  if (response != null && !response.isEmpty()) {
                    if (view != null)
                    view.onCancelBooking(Utility.getMessage(response));
                  } else {
                    if (view != null)
                      view.onFailure();
                  }
                  break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if (view != null)
                        view.stopProgressBar();
                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      cancelBooking(jsonObject);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                      if (view != null){
                        view.stopProgressBar();
                        view.onFailure();
                      }
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if (view != null)
                        view.stopProgressBar();
                      getInstance().toast(Utility.getMessage(msg));
                      Utility.logoutSessionExiperd(sessionManager, mContext);
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  if (view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  Utility.logoutSessionExiperd(sessionManager, mContext);
                  break;
                default:
                  if (view != null)
                  view.stopProgressBar();
                  getInstance().toast(Utility.getMessage(errorBody));
                  break;
              }
            } catch (Exception e) {
              if (view != null) {
                view.stopProgressBar();
                view.onFailure();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if (view != null) {
              view.stopProgressBar();
              view.onFailure();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void updateBidding(JSONObject jsonObject) {
/*
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.BID_UPDATE, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode, String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                        Gson gson = new Gson();
                        JSONObject jsonObjectResponse = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                            imple.onSuccess(jsonObjectResponse.getString("message"), false);
                        } else if (statusCode.equals(VariableConstant.RESPONSE_CODE_NOT_FOUND)) {
                            imple.onCancelBooking(jsonObjectResponse.getString("message"));
                        } else if (statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE)) {
                            RefreshToken.onRefreshToken(jsonObjectResponse.getString("data"), new RefreshToken.RefreshTokenImple() {
                                @Override
                                public void onSuccessRefreshToken(String newToken) {
                                    acceptRejectJob(newToken, jsonObject);
                                    imple.onNewToken(newToken);
                                }

                                @Override
                                public void onFailureRefreshToken() {
                                    imple.onFailure();
                                }

                                @Override
                                public void sessionExpired(String msg) {
                                    imple.sessionExpired(msg);
                                }
                            });
                        } else if (statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN)) {
                            imple.sessionExpired(jsonObjectResponse.getString("message"));
                        } else {
                            imple.onFailure(jsonObjectResponse.getString("message"));
                        }
                    } else {
                        imple.onFailure();
                    }
                } catch (Exception e) {
                    imple.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                imple.onFailure();
            }
        });
*/
  }

  @Override
  public String getBookingStr(Booking booking, String oldStr, String updatedStatus) {
    return oldStr.replace(booking.getBookingId() + "|" + booking.getStatus(), booking.getBookingId() + "|" + updatedStatus);
  }

  @Override
  public void attachView(Object view) {
  }

  @Override
  public void detachView() {
  }

  /**
   * method for returning hours and minutes from seconds
   *
   * @param seconds second
   * @return hours and minutes
   */
  @Override
  public String getDurationString(long seconds) {
    int day = (int) TimeUnit.SECONDS.toDays(seconds);
    int hours = (int) (TimeUnit.SECONDS.toHours(seconds) - (day * 24));
    int minute = (int) (TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60));
    int second = (int) (TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60));
    String timer = String.format("%02d", hours) + " : " + String.format("%02d", minute) + " : " + String.format("%02d", second);
    return timer;
  }

  @Override
  public long getTimeWhileInBackground(SessionManager sessionManager) {
    return sessionManager.getElapsedTime() + (long) (Math.round((System.currentTimeMillis() - sessionManager.getTimeWhilePaused()) / 1000f));
  }
}