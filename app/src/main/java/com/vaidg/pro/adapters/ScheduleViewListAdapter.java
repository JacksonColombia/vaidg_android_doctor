package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.pojo.shedule.ScheduleViewData;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import static com.vaidg.pro.utility.Utility.convertUTCToServerFormat;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_SPACE_DD_MM_YYYY;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_TIME_1_12_HRS;
import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * Created by murashid on 04-Jul-17.
 * <h1>ScheduleViewListAdapter</h1>
 * ScheduleViewData View Recycler adapter for displaying list of schedule in ScheduleViewActivity
 *
 * @See ScheduleViewActivity
 */

public class ScheduleViewListAdapter extends RecyclerView.Adapter<ScheduleViewListAdapter.ViewHolder> implements View.OnClickListener {

    private static final String TAG = "ScheduleViewListAdapter";
    private Context context;
    private DeleteSchdule deleteSchdule;
    private ArrayList<ScheduleViewData> scheduleViewDatas;
    private Typeface fontRegular, fontMedium, fontBold;

    public ScheduleViewListAdapter(Context context, ArrayList<ScheduleViewData> scheduleViewDatas, DeleteSchdule deleteSchdule) {
        this.context = context;
        this.scheduleViewDatas = scheduleViewDatas;
        this.deleteSchdule = deleteSchdule;
        fontRegular = Utility.getFontRegular(context);
        fontMedium = Utility.getFontMedium(context);
        fontBold = Utility.getFontBold(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ScheduleViewListAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_view_slot, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.ivDelete.setOnClickListener(this);
        holder.ivDelete.setTag(holder);

        try {
            holder.tvScheduleTime.setTextSize(Utility.pixelsToSp(context.getResources().getDimension(R.dimen.sp_16), context));
            holder.tvStartDateLabel.setTextSize(Utility.pixelsToSp(context.getResources().getDimension(R.dimen.sp_14), context));
            holder.tvEndDateLabel.setTextSize(Utility.pixelsToSp(context.getResources().getDimension(R.dimen.sp_14), context));
            holder.tvRepeatLabel.setTextSize(Utility.pixelsToSp(context.getResources().getDimension(R.dimen.sp_14), context));
            holder.tvCallLabel.setTextSize(Utility.pixelsToSp(context.getResources().getDimension(R.dimen.sp_14), context));
            holder.tvStartDate.setTextSize(Utility.pixelsToSp(context.getResources().getDimension(R.dimen.sp_16), context));
            holder.tvEndDate.setTextSize(Utility.pixelsToSp(context.getResources().getDimension(R.dimen.sp_16), context));
            holder.tvRepeatDays.setTextSize(Utility.pixelsToSp(context.getResources().getDimension(R.dimen.sp_16), context));
            holder.tvCallTypeValue.setTextSize(Utility.pixelsToSp(context.getResources().getDimension(R.dimen.sp_16), context));

            holder.tvScheduleTime.setText(new StringBuilder()
                    .append(Utility.changeDateTimeFormat(convertUTCToServerFormat(scheduleViewDatas.get(position).getStartDate()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_TIME_1_12_HRS))
                    .append("  ")
                    .append(context.getString(R.string.to))
                    .append("  ")
                    .append(Utility.changeDateTimeFormat(convertUTCToServerFormat(scheduleViewDatas.get(position).getEndDate()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_TIME_1_12_HRS))
            );

            String startDate = Utility.changeDateTimeFormat(convertUTCToServerFormat(scheduleViewDatas.get(position).getStartDate()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_DD_MM_YYYY);
            String[] startDatearr = startDate.split(" ");
            holder.tvStartDate.setText(Utility.getDayOfMonthSuffix(Integer.parseInt(startDatearr[0]), startDatearr[1] + " " + startDatearr[2]));

            String endDate = Utility.changeDateTimeFormat(convertUTCToServerFormat(scheduleViewDatas.get(position).getEndDate()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_DD_MM_YYYY);
            String[] endDatearr = endDate.split(" ");
            holder.tvEndDate.setText(Utility.getDayOfMonthSuffix(Integer.parseInt(endDatearr[0]), endDatearr[1] + " " + endDatearr[2]));
            holder.tvRepeatDays.setText(getDays(scheduleViewDatas.get(position).getDays()));

            StringBuffer callTypeBuffer = new StringBuffer("");
            if (scheduleViewDatas.get(position).getInCall().equals("1")) {
                callTypeBuffer.append(context.getString(R.string.inCall));
                if (scheduleViewDatas.get(position).getOutCall().equals("1")) {
                    callTypeBuffer.append("/" + context.getString(R.string.outCall));
                    if (scheduleViewDatas.get(position).getTeleCall().equals("1")) {
                        callTypeBuffer.append("/" + context.getString(R.string.teleCall));
                    }
                } else if (scheduleViewDatas.get(position).getTeleCall().equals("1")) {
                    callTypeBuffer.append("/" + context.getString(R.string.teleCall));
                }
            } else if (scheduleViewDatas.get(position).getOutCall().equals("1")) {
                callTypeBuffer.append(context.getString(R.string.outCall));
                if (scheduleViewDatas.get(position).getTeleCall().equals("1")) {
                    callTypeBuffer.append("/" + context.getString(R.string.teleCall));
                }
            } else if (scheduleViewDatas.get(position).getTeleCall().equals("1")) {
                callTypeBuffer.append(context.getString(R.string.teleCall));
            }

            holder.tvCallTypeValue.setText(callTypeBuffer.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return scheduleViewDatas != null ? scheduleViewDatas.size() : ZERO;
    }

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position = viewHolder.getAdapterPosition();
        if (v.getId() == R.id.ivDelete) {
            deleteSchdule.onDelete(position, scheduleViewDatas.get(position).get_id());
        }
    }

    private String getDayOfMonthSuffix(final int n) {
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    /*public String convertUTCToServerFormat(String time) {
        long timestamp = Long.parseLong(time);
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTimeInMillis(timestamp);
        return serverFormat.format(cal);
    }*/

    /**
     * <h1>getDays</h1>
     *
     * @param days days in MMM format
     * @return days in MMMMM Format
     */
    private String getDays(ArrayList<String> days) {

        StringBuilder stringBuilder = new StringBuilder("");
        String prefix = "";

        for (String day : days) {
            stringBuilder.append(prefix);
            prefix = ", ";

            switch (day) {
                case "Mon":
                    stringBuilder.append(context.getString(R.string.monday));
                    break;

                case "Tue":
                    stringBuilder.append(context.getString(R.string.tuesday));
                    break;

                case "Wed":
                    stringBuilder.append(context.getString(R.string.wednesday));
                    break;

                case "Thu":
                    stringBuilder.append(context.getString(R.string.thursday));
                    break;

                case "Fri":
                    stringBuilder.append(context.getString(R.string.friday));
                    break;

                case "Sat":
                    stringBuilder.append(context.getString(R.string.saturday));
                    break;

                case "Sun":
                    stringBuilder.append(context.getString(R.string.sunday));
                    break;
            }
        }
        return stringBuilder.toString();
    }

    /**
     * <h1>DeleteSchdule</h1>
     * Interface for delete schedule callback
     */

    public interface DeleteSchdule {
        void onDelete(int position, String slotId);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvStartDateLabel, tvEndDateLabel, tvRepeatLabel, tvCallLabel;
        TextView tvScheduleTime, tvSlotRate, tvStartDate, tvEndDate, tvRepeatDays, tvCallTypeValue;
        ImageView ivDelete;

        ViewHolder(View itemView) {
            super(itemView);
            tvStartDateLabel = itemView.findViewById(R.id.tvStartDateLabel);
            tvEndDateLabel = itemView.findViewById(R.id.tvEndDateLabel);
            tvRepeatLabel = itemView.findViewById(R.id.tvRepeatLabel);
            tvCallLabel = itemView.findViewById(R.id.tvCallLabel);
            tvScheduleTime = itemView.findViewById(R.id.tvScheduleTime);
            tvSlotRate = itemView.findViewById(R.id.tvSlotRate);
            tvStartDate = itemView.findViewById(R.id.tvStartDate);
            tvEndDate = itemView.findViewById(R.id.tvEndDate);
            tvRepeatDays = itemView.findViewById(R.id.tvRepeatDays);
            tvCallTypeValue = itemView.findViewById(R.id.tvCallTypeValue);
            ivDelete = itemView.findViewById(R.id.ivDelete);

            tvStartDateLabel.setTypeface(fontRegular);
            tvEndDateLabel.setTypeface(fontRegular);
            tvRepeatLabel.setTypeface(fontRegular);
            tvCallLabel.setTypeface(fontRegular);
            tvScheduleTime.setTypeface(fontBold);
            tvSlotRate.setTypeface(fontMedium);
            tvStartDate.setTypeface(fontMedium);
            tvEndDate.setTypeface(fontMedium);
            tvRepeatDays.setTypeface(fontMedium);
            tvCallTypeValue.setTypeface(fontMedium);

        }
    }
}
