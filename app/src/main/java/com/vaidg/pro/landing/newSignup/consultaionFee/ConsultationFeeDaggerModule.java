package com.vaidg.pro.landing.newSignup.consultaionFee;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>@ConsultationFeeDaggerModule</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 28/04/2020.
 **/
@Module
public abstract class ConsultationFeeDaggerModule {

    @ActivityScoped
    @Binds
    abstract ConsultationFeeContract.Presenter providePresenter(ConsultationFeePresenter consultationFeePresenter);

    @ActivityScoped
    @Binds
    abstract ConsultationFeeContract.View provideView(ConsultationFeeActivity consultationFeeActivity);

    @ActivityScoped
    @Binds
    abstract Activity provide(ConsultationFeeActivity activity);
}
