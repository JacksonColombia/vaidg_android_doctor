package com.vaidg.pro.pojo.signup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 07-Oct-17.
 */

public class SignUpData implements Serializable {

    @SerializedName("providerId")
    @Expose
    private String providerId;
    @SerializedName("expireOtp")
    @Expose
    private Integer expireOtp;

    public Integer getExpireOtp() {
        return expireOtp;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }
}
