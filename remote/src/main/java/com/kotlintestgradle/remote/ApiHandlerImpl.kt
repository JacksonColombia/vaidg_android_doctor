package com.kotlintestgradle.remote

import com.kotlintestgradle.remote.model.request.LoginRequest
import com.kotlintestgradle.remote.model.request.PostCallRequest
import com.kotlintestgradle.remote.model.response.calling.CallingResponse
import com.kotlintestgradle.remote.model.response.dashboard.ParticipantsListResponse
import com.kotlintestgradle.remote.model.response.dashboard.UsersListResponse
import com.kotlintestgradle.remote.model.response.login.LoginResponse
import io.reactivex.Single

/**
 * @author 3Embed
 *used for retrofit
 * @since 1.0 (23-Aug-2019)
 */
class ApiHandlerImpl(
    private var restApi: RestApi,
    private val deviceId: String
) : ApiHandler {
    override fun getLogin(lan: String?, request: LoginRequest): Single<LoginResponse> {
        return restApi.loginAPI(lan, request)
    }

    override fun getUsersList(lan: String?, authToken: String?): Single<UsersListResponse> {
        return restApi.getUsersList(lan, authToken)
    }

    override fun getParticipantsInCall(
        lan: String?,
        authToken: String?,
        callId: String
    ): Single<ParticipantsListResponse> {
        return restApi.getParticipantsList(lan, authToken, callId)
    }

    override fun postCall(
        lan: String?,
        authToken: String?,
        callingRequest: PostCallRequest
    ): Single<CallingResponse> {
        return restApi.postCall(lan, authToken, callingRequest)
    }

    override fun answerCall(
        lan: String?,
        authToken: String?,
        callId: String,recording: String,sessionId: String,width: Int,height: Int
    ): Single<CallingResponse> {
        return restApi.answerCall(lan, authToken, callId,recording,sessionId,width,height)
    }

    override fun inviteMembers(
        lan: String?,
        authToken: String?,
        callId: String,
        inviteMember: String
    ): Single<CallingResponse> {
        return restApi.inviteMembers(lan, authToken, callId, inviteMember)
    }

    override fun disconnectCall(
        lan: String?,
        authToken: String?,
        callId: String,
        callFrom: String,
        second: Long
    ): Single<CallingResponse> {
        return restApi.disconnectCall(lan, authToken, callId, callFrom,second)
    }
}