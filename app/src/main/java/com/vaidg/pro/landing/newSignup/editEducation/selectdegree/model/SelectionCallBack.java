package com.vaidg.pro.landing.newSignup.editEducation.selectdegree.model;

public interface SelectionCallBack {

    /**
     * on selecting city
     *
     * @param position
     */
    void onItemSelected(int position);
}
