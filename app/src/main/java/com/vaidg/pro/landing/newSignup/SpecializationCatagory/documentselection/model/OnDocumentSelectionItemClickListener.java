package com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.model;

import com.vaidg.pro.landing.newLogin.LogInPresenter;
import com.vaidg.pro.landing.newLogin.NewLogInActivity;
import com.vaidg.pro.pojo.PreDefined;

/**
 * <h1>OnDocumentSelectionItemClickListener</h1>
 * <p>This listener is notify registered user interaction of {@link DocumentSelectionAdapter} so, based on that perform various action. </p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 27/06/2020
 **/
public interface OnDocumentSelectionItemClickListener {

    /**
     * <p>This method provide click event of list item.</p>
     * @param position the position of clicked item
     * @param preDefined the data object of clicked item
     */
    void onItemClick(int position, PreDefined preDefined);

    /**
     * <p>This method provide click event of selected document to preview it.</p>
     * @param position the position of clicked item
     */
    void onDocumentClick(int position, PreDefined preDefined);
}
