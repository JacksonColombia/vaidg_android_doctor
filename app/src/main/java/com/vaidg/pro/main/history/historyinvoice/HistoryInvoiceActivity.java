package com.vaidg.pro.main.history.historyinvoice;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityHistoryInvoiceBinding;
import com.vaidg.pro.main.schedule.bookingschedule.BookingScheculeContract;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.pojo.booking.CancelPojo;
import com.vaidg.pro.pojo.history.Accounting;
import com.vaidg.pro.pojo.history.AdditionalService;
import com.vaidg.pro.pojo.history.CustomerData;
import com.vaidg.pro.pojo.history.ReviewByProvider;
import com.vaidg.pro.pojo.shedule.ScheduleBookngPojo;
import com.vaidg.pro.utility.CircleTransform;
import com.vaidg.pro.utility.DialogHelper;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import javax.inject.Inject;

import static com.vaidg.pro.utility.VariableConstant.FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_SPACE_COLON_DD_MMMM_YYYY_TIME_0_12_HRS;


public class HistoryInvoiceActivity extends BaseDaggerActivity implements BookingScheculeContract.View {

    private SessionManager sessionManager;
    @Inject
    BookingScheculeContract.Presenter presenter;

    private ActivityHistoryInvoiceBinding binding;

    private ProgressDialog progressDialog;

    private boolean isFromSchedule;
    private Booking booking;
    private ReviewByProvider reviewByProvider;
    private Accounting accounting;

    private Typeface fontRegular;
    private LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHistoryInvoiceBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
    }

    private void initViews() {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingDetails));

        inflater = getLayoutInflater();

        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        fontRegular = Utility.getFontRegular(this);

        binding.tvStatus.setTypeface(fontMedium);
        binding.tvPrice.setTypeface(fontBold);
        binding.tvCustomerName.setTypeface(fontMedium);
        binding.tvYouRatedLabel.setTypeface(fontRegular);
        binding.tvYouRated.setTypeface(fontMedium);
        binding.tvJobLocationLabel.setTypeface(fontMedium);
        binding.tvJobLocation.setTypeface(fontRegular);
        binding.tvTotalLabel.setTypeface(fontMedium);
        binding.tvTotal.setTypeface(fontMedium);
        binding.tvPaymentMethodLabel.setTypeface(fontMedium);
        binding.tvPaymentMethod.setTypeface(fontMedium);
        binding.tvPaymentCardCash.setTypeface(fontMedium);
        binding.tvPaymentMethodWallet.setTypeface(fontMedium);
        binding.tvPaymentWallet.setTypeface(fontMedium);
        binding.tvSignatureHeader.setTypeface(fontMedium);

        binding.tvTravelFeeLabel.setTypeface(fontRegular);
        binding.tvTravelFee.setTypeface(fontRegular);
        binding.tvVisitFeeLabel.setTypeface(fontRegular);
        binding.tvVisitFee.setTypeface(fontRegular);
        binding.tvLastDueLabel.setTypeface(fontRegular);
        binding.tvLastDue.setTypeface(fontRegular);

        binding.tvYourEarningsLabel.setTypeface(fontMedium);
        binding.tvEarnedAmountLabel.setTypeface(fontRegular);
        binding.tvEarnedAmount.setTypeface(fontRegular);
        binding.tvAppCommissionLabel.setTypeface(fontRegular);
        binding.tvAppCommission.setTypeface(fontRegular);
        binding.tvCancelReason.setTypeface(fontRegular);
        binding.tvPgCommissionLabel.setTypeface(fontRegular);
        binding.tvpgComission.setTypeface(fontRegular);

        binding.tvPaymentBreakDownLabel.setTypeface(fontMedium);
        binding.tvDiscountLabel.setTypeface(fontRegular);
        binding.tvDiscount.setTypeface(fontRegular);
        binding.tvCancelFeeLabel.setTypeface(fontRegular);
        binding.tvCancelFee.setTypeface(fontRegular);

        binding.tvamountPaidLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_14), this));
        binding.tvPrice.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_35), this));
        binding.tvCustomerName.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_18), this));
        binding.tvPaymentMethodLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_18), this));
        binding.tvYourEarningsLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_18), this));
        binding.tvPaymentBreakDownLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_18), this));
        binding.tvJobLocationLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_18), this));
        binding.tvStatus.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvJobLocation.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvYouRated.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_18), this));
        binding.tvYouRatedLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvPgCommissionLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvpgComission.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvDiscountLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvDiscount.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvAppCommissionLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvAppCommission.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvEarnedAmountLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvEarnedAmount.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvTotalLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvTotal.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvPaymentCardCash.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        binding.tvPaymentMethod.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));

        isFromSchedule = getIntent().getBooleanExtra("isFromSchedule", false);
        if (!isFromSchedule) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                booking = (Booking) bundle.getSerializable("booking");
            }
            if (booking != null) {
                reviewByProvider = booking.getReviewByProvider();
                accounting = booking.getAccounting();
                setValues();
            }
        } else {
            presenter.getBooking(getIntent().getStringExtra("bookingId"));
        }

        binding.btnViewPrescription.setOnClickListener(v -> launchInvoiceViewer(booking.getPdfFile()));
    }

    @Override
    public void onBackPressed() {
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();

    }

    public void launchInvoiceViewer(String filePath) {
        if (Utility.isTextEmpty(filePath)) {
            DialogHelper.customAlertDialog(this, getResources().getString(R.string.prescription), getResources().getString(R.string.msgPrescriptionNotFound), getResources().getString(R.string.oK));
            return;
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(filePath));
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            showError("No handler for this type of file.");
        }
    }

    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clParent, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    private void setValues() {
        if (booking.getCallType().equals("3")) {
//            tvJobLocationLabel.setVisibility(View.GONE);
            binding.tvJobLocationLabel.setText(R.string.timeslot);
//            tvJobLocation.setVisibility(View.GONE);
//            SimpleDateFormat displayDateFormaconsultingfeet = new SimpleDateFormat("dd MMM yyyy", Locale.US);

//                tvJobLocation.setText(displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getBookingRequestedFor()))));
            binding.tvJobLocation.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
//            findViewById(R.id.vvjoblocation).setVisibility(View.GONE);
        } else {
            binding.tvJobLocation.setText(booking.getAddLine1().concat(" ").concat(booking.getAddLine2()));
        }
        binding.tvTitle.setText(getText(R.string.jobId).toString().concat(" ").concat(booking.getBookingId()));
        binding.tvStatus.setText(booking.getStatusMsg());
        binding.tvPrice.setText(Utility.getPrice(booking.getAccounting().getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        binding.tvCustomerName.setText(Utility.toTitleCase(booking.getFirstName().concat(" ").concat(booking.getLastName())));


        binding.tvDiscount.setText(Utility.getPrice(accounting.getDiscount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        binding.tvTotal.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        binding.tvEarnedAmount.setText(Utility.getPrice(accounting.getProviderEarning(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        binding.tvAppCommission.setText(Utility.getPrice(accounting.getAppCommission(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

        if (reviewByProvider != null && reviewByProvider.getRating() != null) {
            binding.tvYouRated.setText(Utility.roundString(reviewByProvider.getRating(), 1));
        }

        if (accounting.getTravelFee() != null && !accounting.getTravelFee().equals("0") && !accounting.getTravelFee().equals("0.00")) {
            findViewById(R.id.rlTravelFee).setVisibility(View.VISIBLE);
            binding.tvTravelFee.setText(Utility.getPrice(accounting.getTravelFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if (accounting.getVisitFee() != null && !accounting.getVisitFee().equals("0") && !accounting.getVisitFee().equals("0.00")) {
            findViewById(R.id.rlVisitFee).setVisibility(View.VISIBLE);
            binding.tvVisitFee.setText(Utility.getPrice(accounting.getVisitFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if (accounting.getLastDues() != null && !accounting.getLastDues().equals("0") && !accounting.getLastDues().equals("0.00")) {
            findViewById(R.id.rlLastDue).setVisibility(View.VISIBLE);
            binding.tvLastDue.setText(Utility.getPrice(accounting.getLastDues(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if (accounting.getPaymentMethod().equals("1")) {
            binding.tvPaymentMethod.setText(getString(R.string.cash));
            binding.tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.vector_cash, 0, 0, 0);
        } else {
           // binding.tvPaymentMethod.setText(getString(R.string.cardFirst16Digit).concat("  ").concat(accounting.getLast4()));
            binding.tvPaymentMethod.setText(getString(R.string.card));
          //  binding.tvPaymentMethod.setText(accounting.getRazorPayPaymentType());
        }

        //  tvPaymentCardCash.setText(Utility.getPrice(accounting.getRemainingAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

        if (accounting.getPaidByWallet() != null && accounting.getPaidByWallet().equals("1")) {
            binding.rlWalletPaymentDetails.setVisibility(View.VISIBLE);
            // tvPaymentWallet.setText(Utility.getPrice(accounting.getCaptureAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if (!accounting.getPgCommissionProvider().equals("") && !accounting.getPgCommissionProvider().equals("0")) {
            binding.rlPgCommission.setVisibility(View.VISIBLE);
            binding.tvpgComission.setText(Utility.getPrice(accounting.getPgCommissionProvider(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if (!booking.getBookingModel().equals("3")) {
/*            if (booking.getServiceType().equals("1")) {
                if (booking.getCartData() != null) {
                    for (ServiceItem serviceItem : booking.getCartData()) {
                        View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
                        TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                        TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                        tvServiceFeeLabel.setTypeface(fontRegular);
                        tvServiceFee.setTypeface(fontRegular);
                        tvServiceFeeLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16),this));
                        tvServiceFee.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16),this));
                        tvServiceFeeLabel.setText(serviceItem.getServiceName() + " X " + serviceItem.getQuntity());
                        tvServiceFee.setText(Utility.getPrice(serviceItem.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                        llService.addView(serviceView);
                    }
                }
            } else if (!booking.getCallType().equals("1") && !booking.getCallType().equals("3")) {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16),this));
                tvServiceFee.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16),this));
                tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
                tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            } else {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16),this));
                tvServiceFee.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16),this));
                tvServiceFeeLabel.setText(getString(R.string.consultingfee));
                tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            }*/
            @SuppressLint("InflateParams") View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
            tvServiceFeeLabel.setTypeface(fontRegular);
            tvServiceFee.setTypeface(fontRegular);
            tvServiceFeeLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
            tvServiceFee.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
            tvServiceFeeLabel.setText(getString(R.string.consultingfee));
            tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            binding.llService.addView(serviceView);
        } else {
            @SuppressLint("InflateParams") View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
            tvServiceFeeLabel.setTypeface(fontRegular);
            tvServiceFee.setTypeface(fontRegular);
            tvServiceFeeLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
            tvServiceFee.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
            tvServiceFeeLabel.setText(getString(R.string.serviceFee));
            tvServiceFee.setText(Utility.getPrice(accounting.getBidPrice(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            binding.llService.addView(serviceView);
        }

/*
        if (booking.getQuestionAndAnswer() != null && booking.getQuestionAndAnswer().size() > 0) {
            LinearLayout llQuestionAnswer = findViewById(R.id.llQuestionAnswer);
            llQuestionAnswer.setVisibility(View.VISIBLE);
            findViewById(R.id.vQuestionAnswer).setVisibility(View.VISIBLE);
            for (QuestionAnswer questionAnswer : booking.getQuestionAndAnswer()) {
                View questionAnswerView = inflater.inflate(R.layout.single_row_question_answer, null);
                TextView tvQuestion = questionAnswerView.findViewById(R.id.tvQuestion);
                TextView tvAnswer = questionAnswerView.findViewById(R.id.tvAnswer);
                tvQuestion.setTypeface(fontMedium);
                tvAnswer.setTypeface(fontRegular);
                tvQuestion.setText("Q. " + questionAnswer.getName());
                if (!questionAnswer.getQuestionType().equals("10")) {
                    tvAnswer.setText(questionAnswer.getAnswer());
                } else {
                    tvAnswer.setVisibility(View.GONE);
                    RecyclerView rvJobPhotosQuestion = questionAnswerView.findViewById(R.id.rvJobPhotosQuestion);
                    rvJobPhotosQuestion.setVisibility(View.VISIBLE);
                    rvJobPhotosQuestion.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                    ArrayList<String> imageUrls = new ArrayList<>();
                    imageUrls.addAll(Arrays.asList(questionAnswer.getAnswer().split(",")));
                    rvJobPhotosQuestion.setAdapter(new JobImageListAdapter(this, imageUrls));
                }
                llQuestionAnswer.addView(questionAnswerView);
            }
        }
*/
        if (booking.getAdditionalService() != null && booking.getAdditionalService().size() > 0) {
            for (AdditionalService serviceItem : booking.getAdditionalService()) {
                @SuppressLint("InflateParams") View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
                tvServiceFee.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setText(serviceItem.getServiceName());
                tvServiceFee.setText(Utility.getPrice(serviceItem.getPrice(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                binding.llService.addView(serviceView);
            }
        }

        switch (booking.getStatus()) {
            case VariableConstant.JOB_COMPLETED_RAISE_INVOICE:
                Utility.setBackgroundColor(this, binding.tvStatus, R.color.jobStatusCompleted);
                binding.btnViewPrescription.setVisibility(View.VISIBLE);
                break;

            case VariableConstant.CANCELLED_BY_PROVIDER:
            case VariableConstant.CANCELLED_BY_CUTOMER:
                binding.btnViewPrescription.setVisibility(View.GONE);
                binding.tvCancelReason.setVisibility(View.VISIBLE);
                binding.rlCancelFee.setVisibility(View.VISIBLE);
                binding.tvCancelReason.setText(getString(R.string.cancellationReason).concat(": ").concat(booking.getCancellationReason()));
                binding.tvCancelFee.setText(Utility.getPrice(accounting.getCancellationFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                Utility.setBackgroundColor(this, binding.tvStatus, R.color.jobStatusCanceled);
                binding.llRating.setVisibility(View.GONE);
                binding.llSignature.setVisibility(View.GONE);
                break;

            case VariableConstant.BOOKING_EXPIRED:
                binding.btnViewPrescription.setVisibility(View.GONE);
                Utility.setBackgroundColor(this, binding.tvStatus, R.color.jobStatusExpired);
                binding.llRating.setVisibility(View.GONE);
                binding.llAmount.setVisibility(View.GONE);
                binding.llSignature.setVisibility(View.GONE);
                break;

            default:
                binding.btnViewPrescription.setVisibility(View.GONE);
                Utility.setBackgroundColor(this, binding.tvStatus, R.color.jobStatusDeclined);
                binding.llRating.setVisibility(View.GONE);
                binding.llAmount.setVisibility(View.GONE);
                binding.llSignature.setVisibility(View.GONE);
                break;
        }

        if (isFromSchedule) {
            CustomerData customerData = booking.getCustomerData();
            binding.tvCustomerName.setText(Utility.toTitleCase(customerData.getFirstName().concat(" ").concat(customerData.getLastName())));
            if (!customerData.getProfilePic().equals("")) {
                Glide.with(this).setDefaultRequestOptions(new RequestOptions().error(R.drawable.profile_default_image)
                        .transform(new CircleTransform(this))
                        .placeholder(R.drawable.profile_default_image))
                        .load(customerData.getProfilePic())
                        .into(binding.ivCustomer);
            }

/*            if(!customerData.getAge().isEmpty()  && !customerData.getGender().isEmpty())
            {
                tvYouRated.setText(customerData.getGender()+","+customerData.getAge());
            }else if(customerData.getAge().isEmpty()  && !customerData.getGender().isEmpty())
            {
                tvYouRated.setText(customerData.getGender());
            }else if(!customerData.getAge().isEmpty()  && customerData.getGender().isEmpty())
            {
                tvYouRated.setText(customerData.getAge());
            }*/
        } else if (!booking.getProfilePic().equals("")) {
            Glide.with(this).setDefaultRequestOptions(new RequestOptions().error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(this))
                    .placeholder(R.drawable.profile_default_image))
                    .load(booking.getProfilePic())
                    .into(binding.ivCustomer);
        }

        if (!booking.getSignatureUrl().equals("")) {
            Glide.with(this).setDefaultRequestOptions(new RequestOptions())
                    .load(booking.getSignatureUrl())
                    .into(binding.ivSignature);
        }

        try {
            binding.tvSubTitle.setText(Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(booking.getBookingRequestedFor()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_COLON_DD_MMMM_YYYY_TIME_0_12_HRS));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onSuccess(ScheduleBookngPojo scheduleBookngPojo) {
        booking = scheduleBookngPojo.getData();
        reviewByProvider = booking.getReviewByProvider();
        accounting = booking.getAccounting();
        setValues();
    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {

    }

    @Override
    public void onCancelBooking(String msg) {
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }
}

