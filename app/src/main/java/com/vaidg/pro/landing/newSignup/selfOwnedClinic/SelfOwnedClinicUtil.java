package com.vaidg.pro.landing.newSignup.selfOwnedClinic;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.landing.newSignup.selfOwnedClinic.model.SelfOwnedClinicAdapter;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.MediaBottomSelector;
import com.videocompressor.com.CompressImage;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class SelfOwnedClinicUtil {
    public static final String PATH = "filePath";
    public static final String URL = "URL";

    @Provides
    @ActivityScoped
    App_permission getApp_permission(Activity activity) {
        return new App_permission(activity);
    }

    @Provides
    @ActivityScoped
    MediaBottomSelector getMediaBottomSelector(Activity activity) {
        return new MediaBottomSelector(activity);
    }

    @ActivityScoped
    @Provides
    CompressImage compressImage() {
        return new CompressImage();
    }

    @Named(PATH)
    @ActivityScoped
    @Provides
    ArrayList<String> providepathArrayList() {
        return new ArrayList<>();
    }
//
//    @Named(URL)
//    @ActivityScoped
//    @Provides
//    ArrayList<String> provideUrlArrayList() {
//        return new ArrayList<>();
//    }

    @ActivityScoped
    @Provides
    SelfOwnedClinicAdapter proviClinicAdapter(@Named(PATH) ArrayList<String> arrayList) {
        return new SelfOwnedClinicAdapter(arrayList);
    }

}
