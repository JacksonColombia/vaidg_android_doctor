package com.vaidg.pro.main.profile.support;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.adapters.SupportListAdapter;
import com.vaidg.pro.databinding.ActivitySupportSubCategoryBinding;
import com.vaidg.pro.pojo.profile.support.SupportData;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>SupportSubCategoryActivity</h1>SupportActivity
 * SupportSubCategoryActivity for showing SubCategory Support in  SupportActivity
 *
 * @see SupportActivity
 */
public class SupportSubCategoryActivity extends AppCompatActivity {

    private ActivitySupportSubCategoryBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySupportSubCategoryBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }

    /**
     * init the views
     */
    @SuppressWarnings("unchecked")
    private void initView() {
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        Intent intent = getIntent();
        ArrayList<SupportData> supportDatas = new ArrayList<>((Collection<? extends SupportData>) intent.getSerializableExtra("data"));

        TextView tvTitle = findViewById(R.id.tvTitle);
        TextView tvSubTitle = findViewById(R.id.tvSubTitle);

        tvTitle.setTypeface(Utility.getFontBold(this));
        tvSubTitle.setTypeface(Utility.getFontMedium(this));

        tvTitle.setText(getString(R.string.support));
        tvSubTitle.setText(intent.getStringExtra("title"));


        RecyclerView rvSupportSubCategory = findViewById(R.id.rvSupportSubCateogry);
        rvSupportSubCategory.setLayoutManager(new LinearLayoutManager(this));

        SupportListAdapter supportListAdapter = new SupportListAdapter(this, supportDatas, false);
        rvSupportSubCategory.setAdapter(supportListAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }
}
