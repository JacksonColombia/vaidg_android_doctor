package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StripeData implements Serializable {

	@SerializedName("tos_acceptance")
	@Expose
	private TosAcceptance tosAcceptance;

	@SerializedName("country")
	@Expose
	private String country;

	@SerializedName("settings")
	@Expose
	private Settings settings;

	@SerializedName("metadata")
	@Expose
	private Metadata metadata;

	@SerializedName("requirements")
	@Expose
	private Requirements requirements;

	@SerializedName("capabilities")
	@Expose
	private Capabilities capabilities;

	@SerializedName("individual")
	@Expose
	private Individual individual;

	@SerializedName("created")
	@Expose
	private int created;

	@SerializedName("payouts_enabled")
	@Expose
	private boolean payoutsEnabled;

	@SerializedName("type")
	@Expose
	private String type;

	@SerializedName("business_profile")
	@Expose
	private BusinessProfile businessProfile;

	@SerializedName("charges_enabled")
	@Expose
	private boolean chargesEnabled;

	@SerializedName("details_submitted")
	@Expose
	private boolean detailsSubmitted;

	@SerializedName("business_type")
	@Expose
	private String businessType;

	@SerializedName("id")
	@Expose
	private String id;

	@SerializedName("default_currency")
	@Expose
	private String defaultCurrency;

	@SerializedName("email")
	@Expose
	private String email;

	@SerializedName("object")
	@Expose
	private String object;

	@SerializedName("external_accounts")
	@Expose
	private ExternalAccounts externalAccounts;

	public TosAcceptance getTosAcceptance(){
		return tosAcceptance;
	}

	public String getCountry(){
		return country;
	}

	public Settings getSettings(){
		return settings;
	}

	public Metadata getMetadata(){
		return metadata;
	}

	public Requirements getRequirements(){
		return requirements;
	}

	public Capabilities getCapabilities(){
		return capabilities;
	}

	public Individual getIndividual(){
		return individual;
	}

	public int getCreated(){
		return created;
	}

	public boolean isPayoutsEnabled(){
		return payoutsEnabled;
	}

	public String getType(){
		return type;
	}

	public BusinessProfile getBusinessProfile(){
		return businessProfile;
	}

	public boolean isChargesEnabled(){
		return chargesEnabled;
	}

	public boolean isDetailsSubmitted(){
		return detailsSubmitted;
	}

	public String getBusinessType(){
		return businessType;
	}

	public String getId(){
		return id;
	}

	public String getDefaultCurrency(){
		return defaultCurrency;
	}

	public String getEmail(){
		return email;
	}

	public String getObject(){
		return object;
	}

	public ExternalAccounts getExternalAccounts(){
		return externalAccounts;
	}

	@Override
	public String toString(){
		return
				"OrderData{" +
						"tos_acceptance = '" + tosAcceptance + '\'' +
						",country = '" + country + '\'' +
						",settings = '" + settings + '\'' +
						",metadata = '" + metadata + '\'' +
						",requirements = '" + requirements + '\'' +
						",capabilities = '" + capabilities + '\'' +
						",individual = '" + individual + '\'' +
						",created = '" + created + '\'' +
						",payouts_enabled = '" + payoutsEnabled + '\'' +
						",type = '" + type + '\'' +
						",business_profile = '" + businessProfile + '\'' +
						",charges_enabled = '" + chargesEnabled + '\'' +
						",details_submitted = '" + detailsSubmitted + '\'' +
						",business_type = '" + businessType + '\'' +
						",id = '" + id + '\'' +
						",default_currency = '" + defaultCurrency + '\'' +
						",email = '" + email + '\'' +
						",object = '" + object + '\'' +
						",external_accounts = '" + externalAccounts + '\'' +
						"}";
	}
}