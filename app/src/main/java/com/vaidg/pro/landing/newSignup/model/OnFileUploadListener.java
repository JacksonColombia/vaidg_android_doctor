package com.vaidg.pro.landing.newSignup.model;

import java.io.File;

public interface OnFileUploadListener {

    void onFileUploaded(String from, String remoteUrl);

    void onFileUploadFailed(String from, String errorMessage);

}
