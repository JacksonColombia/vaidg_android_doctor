package com.vaidg.pro.pojo.profile.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 14-Sep-17.
 */

public class ReviewData implements Serializable {
    @SerializedName("reviews")
    @Expose
    private ArrayList<Reviews> reviews;
    @SerializedName("averageRating")
    @Expose
    private String averageRating = "";
    @SerializedName("reviewCount")
    @Expose
    private String reviewCount = "";
    @SerializedName("about")
    @Expose
    private String about = "";

    public ArrayList<Reviews> getReviews() {
        return reviews;
    }

    public void setReviews(ArrayList<Reviews> reviews) {
        this.reviews = reviews;
    }

    public String getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(String averageRating) {
        this.averageRating = averageRating;
    }

    public String getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}
