package com.vaidg.pro.landing.newSignup.addClinic;

import android.app.Activity;
import android.content.Intent;

import com.vaidg.pro.landing.newSignup.findHospital.model.HospitalDataPojo;
import com.vaidg.pro.utility.VariableConstant;

import javax.inject.Inject;

/**
 * <h1>AddClinicPresenter</h1>
 * <p>this is class for bussiness logic</p>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 25/04/2020.
 **/
public class AddClinicPresenter implements AddClinicContract.Presenter {

    @Inject
    AddClinicContract.View view;

    @Inject
    public AddClinicPresenter() {
    }

    @Override
    public void parseOnActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case VariableConstant.REQUEST_CODE_FIND_HOSPITAL:
                case VariableConstant.REQUEST_CODE_OWN_CLINIC:
                    HospitalDataPojo dataPojo = data.getParcelableExtra("clinic");
                    Intent intent = new Intent();
                    intent.putExtra("clinic", dataPojo);
                    view.sendBackData(intent);
                    break;
            }
        }
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }
}
