package com.vaidg.pro.main;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.RefreshTokenImple;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.appconfig.AppConfigMainPojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by murashid on 25-Oct-17.
 * MainActivityPresenter presenter for MainActivity
 */
public class MainActivityPresenter implements MainActivityContract.Presenter {
  @Inject
  NetworkService service;
  @Inject
  Gson gson;
  @Inject
  MainActivityContract.View view;
  @Inject
  SessionManager sessionManager;

  @Inject
  MainActivityPresenter() {
  }

  @Override
  public void getAppConfig() {
    service.getAppConfig(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  if(view != null)
                    view.hideProgress();
                    if (response != null && !response.isEmpty()) {
                    AppConfigMainPojo pojo = gson.fromJson(response, AppConfigMainPojo.class);
                    if (view != null)
                      view.onSuccess(pojo.getData(), response);

                  }
                  break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if(view != null)
                          view.hideProgress();
                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      getAppConfig();
                    }

                    @Override
                    public void onFailureRefreshToken() {
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if (view != null)
                      {
                        view.hideProgress();
                        view.sessionExpired(Utility.getMessage(msg));
                      }
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  if (view != null) {
                    view.hideProgress();
                    view.sessionExpired(Utility.getMessage(errorBody));
                  }break;
                default:
                  if (view != null) {
                    view.hideProgress();
                    view.onFailure(Utility.getMessage(errorBody));
                  }
                  break;
              }
            } catch (Exception e) {
              if (view != null) {
                view.hideProgress();
                view.onFailure();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.hideProgress();
              view.onFailure();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

  /**
   * method for calling api for geting the Category details
   */
  @Override
  public void getBookingByDate(final String startDate, final String endDate, final boolean isBackground) {
       /* if(!isBackground)
        {
            view.startProgressBar();
        }*/
    service.getBookings(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, startDate, endDate)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  if(view != null)
                        view.hideProgress();
                    if (response != null && !response.isEmpty()) {
                    if(view != null) {
                      view.onSuccesBooking(response, true);
                    }
                  }
                  break;
                case SESSION_EXPIRED:
                  onRefreshToken(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if(view != null)
                          view.hideProgress();
                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      getBookingByDate(startDate, endDate, isBackground);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                      if(view != null) {
                        view.hideProgress();
                        view.onFailure();
                      }
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if (view != null) {
                        view.hideProgress();
                        view.sessionExpired(msg);
                      }
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  if(view != null) {
                    view.hideProgress();
                    view.sessionExpired(Utility.getMessage(errorBody));
                  }
                  break;
                default:
                  if(view != null) {
                    view.hideProgress();
                    view.onFailure(Utility.getMessage(errorBody));
                  }
                  break;
              }
            } catch (Exception e) {
              if(view != null) {
                view.hideProgress();
                view.onFailure();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.hideProgress();
              view.onFailure();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void attachView(Object view) {
  }

  @Override
  public void detachView() {
  }
}
