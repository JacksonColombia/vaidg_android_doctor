package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Dob implements Serializable {

	@SerializedName("month")
	@Expose
	private String month;

	@SerializedName("year")
	@Expose
	private String year;

	@SerializedName("day")
	@Expose
	private String day;

	public String getMonth(){
		return month;
	}

	public String getYear(){
		return year;
	}

	public String getDay(){
		return day;
	}

	@Override
 	public String toString(){
		return 
			"Dob{" + 
			"month = '" + month + '\'' + 
			",year = '" + year + '\'' + 
			",day = '" + day + '\'' + 
			"}";
		}
}