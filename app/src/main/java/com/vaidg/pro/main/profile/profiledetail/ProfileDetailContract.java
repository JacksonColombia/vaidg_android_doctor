package com.vaidg.pro.main.profile.profiledetail;

import android.content.Intent;
import android.net.Uri;

import androidx.annotation.Nullable;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.profile.ProfileData;
import com.vaidg.pro.pojo.profile.metaData.MetaDataArr;
import com.vaidg.pro.utility.UploadFileAmazonS3;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>ProfileDetailContract</h1>
 * <p>This contract define interface with required methods for view and presenter</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 20/06/2020
 **/
public interface ProfileDetailContract {

    interface View extends BaseView {

        /**
         * <p>This method is used to initialize views</p>
         */
        void initViews();

        /**
         * <p>This method is used to set click event on the views.</p>
         */
        void initViewOnClickListeners();

        /**
         * <p>This method is update UI with set data to display.</p>
         */
        void updateUI();

        /**
         * <p>This method is initialize RecyclerView and set data into that.</p>
         */
        void initRv();

        /**
         * <p>This method is provide response from profile API with {@link ProfileData} class.</p>
         *
         * @param profileData the response of profile data
         */
        void onSuccessProfile(ProfileData profileData);

        /**
         * <p>This method is indicate API is failed.</p>
         */
        void onFailure();

        /**
         * <p>This method is display passed error message in snack bar.</p>
         *
         * @param error the error message
         */
        void showError(String error);

        /**
         * <p>This method is redirect user to edit email or phone screen.</p>
         *
         * @param isPhone the true if edit phone
         */
        void launchEditEmailPhone(boolean isPhone);

        /**
         * <p>This method redirect user to change password screen.</p>
         */
        void launchChangePassword();

        /**
         * <p>This method is redirect user to display clinic or hospital details.</p>
         */
        void launchClinicDetails();

        /**
         * <p>This method redirect user to display list of degrees.</p>
         */
        void launchEducationDetails();

        /**
         * <p>this is used for notifying recycler view adapter</p>
         */
        void notifyAdapter();

        /**
         * <p>this is used for notifying recycler view item at given position</p>
         * @param position the position of item
         */
        void notifyAdapter(int position);

        /**
         * <p>This method is open gallery for user to select media from local storage.</p>
         */
        void openGallery();

        /**
         * <p>This method is open camera with set pre defined image save path.</p>
         *
         * @param uri_path the path to save captured image
         */
        void openCamera(Uri uri_path);

        /**
         * <p>This method is start screen for image cropping.</p>
         *
         * @param recentTemp the image path
         */
        void startCropImage(String recentTemp);

        /**
         * <[>This method is update profile picture with provided new image path.</[>
         * @param newPath the new path of uploaded image
         */
        void updateProfilePic(String newPath);
    }

    interface Presenter extends BasePresenter {

        /**
         * <p>This method is display bottom sheet with some options when tap on select profile.</p>
         */
        void openChooser();

        /**
         * <p>This method is compress provided image and then upload it to server.</p>
         *
         * @param filePath the local file path
         * @param callBack the listener for upload status
         */
        void compressImage(String filePath, UploadFileAmazonS3.UploadCallBack callBack);

        /**
         * method for uploading image to amazon
         *
         * @param mFileTemp file which has to been upload in amazon
         * @param callBack  the call back with status of upload
         */
        void amazonUpload(final File mFileTemp, UploadFileAmazonS3.UploadCallBack callBack);

        /**
         * <p>This method call profile API and set data to display.</p>
         */
        void getProfile();

        /**
         * <p>This method is add meta data into list and displaying it in recyclerview.</p>
         *
         * @param data the list of {@link MetaDataArr} objects
         */
        void addAdditionalDetailsData(List<MetaDataArr> data);

        /**
         * <p>This method is provide item click event with it's position and {@link MetaDataArr} object.</p>
         * @param position the position of item
         * @param metaDataArr the {@link MetaDataArr} object of clicked item
         */
        void onItemClick(int position, MetaDataArr metaDataArr);

        /**
         * <p>This method is update metadata object into list and notify adapter to update view.</p>
         * @param position the position of meta data
         * @param metaDataArr the updated meta data object
         */
        void updateMetaData(int position, MetaDataArr metaDataArr);

        /**
         * <p>This method is receive onActivityResult data to handle result data in presenter.</p>
         * @param requestCode the intent request code
         * @param resultCode the intent result code
         * @param data the intent data
         */
        void onActivityResultParse(int requestCode, int resultCode, @Nullable Intent data);

        /**
         * <p>This method is call logout API and on success redirect user to intro screen.</p>
         */
        void logout();

        /**
         * <p>This method is call api for upload profile data changes on server.</p>
         *
         * @param profilePic the updated profile pic
         */
        void callUpdateProfileApi(String profilePic);

    }
}