package com.vaidg.pro.landing.newSignup.selectCity;

import android.content.Intent;

import com.vaidg.pro.BaseView;

/**
 * <h1>SelectCityContract</h1>
 * <p>This class is define implementation method for View an Presenter class.</p>

 * @author 3embed
 * @version 1.0.20
 * @since 12/06/2020
 **/
public interface SelectCityContract {

    interface View extends BaseView {

        /**
         * <p>This method is used to initialize views</p>
         */
        void initViews();

        /**
         * <p>This method is update notify city list adapter.</p>
         */
        void notifyAdapter();


        /**
         * <p>This method is display passed error message in snack bar.</p>
         */
        void showError();

        /**
         * <p>This method is display passed error message in snack bar.</p>
         * @param error the error message
         */
        void showError(String error);

        /**
         * <p>This method is intent to specified activity and finish current activity</p>
         * @param intent the object of intent with destination activity
         */
        void finishActivity(Intent intent);
    }

    interface Presenter {

        /**
         * <p>This method is call API for get list of cities and add in list then notify to city adapter.</p>
         */
        void getCity();

    }
}
