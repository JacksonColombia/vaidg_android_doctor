package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DeclineOn implements Serializable {

  @SerializedName("avs_failure")
  @Expose
  private boolean avsFailure;
  @SerializedName("cvc_failure")
  @Expose
  private boolean cvcFailure;

  public boolean isAvsFailure() {
    return avsFailure;
  }

  public boolean isCvcFailure() {
    return cvcFailure;
  }

  @Override
  public String toString() {
    return "DeclineOn{" + "avs_failure = '" + avsFailure + '\'' + ",cvc_failure = '" + cvcFailure + '\'' + "}";
  }
}