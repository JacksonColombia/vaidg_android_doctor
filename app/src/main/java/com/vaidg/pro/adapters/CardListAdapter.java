package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.stripe.android.model.Card;
import com.vaidg.pro.R;
import com.vaidg.pro.pojo.profile.wallet.CardData;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by murashid on 28-Mar-18.
 */

public class CardListAdapter extends RecyclerView.Adapter<CardListAdapter.ViewHolder> implements View.OnClickListener {

    private static final String TAG = "BankListAdapter";
    private ArrayList<CardData> cardData;
    private Typeface fontRegular;
    private CardClickListener cardClickListener;
    private Context context;
    private SessionManager sessionManager;
    private Map<String, Integer> TEMPLATE_RESOURCE_MAP = new HashMap<>();

/*
    {
        TEMPLATE_RESOURCE_MAP.put(Card.AMERICAN_EXPRESS, com.stripe.android.R.drawable.ic_amex_template_32);
        TEMPLATE_RESOURCE_MAP.put(Card.DINERS_CLUB, com.stripe.android.R.drawable.ic_diners_template_32);
        TEMPLATE_RESOURCE_MAP.put(Card.DISCOVER, com.stripe.android.R.drawable.ic_discover_template_32);
        TEMPLATE_RESOURCE_MAP.put(Card.JCB, com.stripe.android.R.drawable.ic_jcb_template_32);
        TEMPLATE_RESOURCE_MAP.put(Card.MASTERCARD, com.stripe.android.R.drawable.ic_mastercard_template_32);
        TEMPLATE_RESOURCE_MAP.put(Card.VISA, com.stripe.android.R.drawable.ic_visa_template_32);
        TEMPLATE_RESOURCE_MAP.put(Card.UNKNOWN, com.stripe.android.R.drawable.ic_unknown);
    }
*/


    public CardListAdapter(Context context, ArrayList<CardData> cardData, SessionManager sessionManager, CardClickListener cardClickListener) {
        this.cardData = cardData;
        this.context = context;
        fontRegular = Utility.getFontRegular(context);
        this.sessionManager = sessionManager;
        this.cardClickListener = cardClickListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_card, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvCardNo.setText(context.getString(R.string.card_ending_with) + " " + cardData.get(position).getLast4());
        if (cardData.get(position).getBrand() != null && !cardData.get(position).getBrand().equals("")) {
            holder.ivCardImage.setImageBitmap(Utility.getCardBrand(cardData.get(position).getBrand(),context));
        }

        if (cardData.get(position).isDefault()) {
            holder.ivTick.setVisibility(View.VISIBLE);
            holder.ivDelete.setVisibility(View.INVISIBLE);

            sessionManager.setSelectedCardId(cardData.get(position).getId());
            sessionManager.setSelectedCardNumber(cardData.get(position).getLast4());
            sessionManager.setCardBrand(cardData.get(position).getBrand());
        } else {
            holder.ivTick.setVisibility(View.INVISIBLE);
            holder.ivDelete.setVisibility(View.VISIBLE);
        }

        holder.ivDelete.setOnClickListener(this);
        holder.tvCardNo.setOnClickListener(this);
        holder.ivDelete.setTag(holder);
        holder.tvCardNo.setTag(holder);


    }

    @Override
    public int getItemCount() {
        return cardData == null ? 0 : cardData.size();
    }

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position = viewHolder.getAdapterPosition();

        switch (v.getId()) {
            case R.id.tvCardNo:
                cardClickListener.onCardSelect(position);
                break;

            case R.id.ivDelete:
                cardClickListener.onCardDelete(position);
                break;
        }
    }

    /**
     * <h1>RefreshBankDetails</h1>
     * Interface for refresh the bank list
     */

    public interface CardClickListener {
        void onCardSelect(int position);

        void onCardDelete(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCardNo;
        ImageView ivTick, ivDelete,ivCardImage;

        public ViewHolder(View itemView) {
            super(itemView);

            tvCardNo = itemView.findViewById(R.id.tvCardNo);
            ivCardImage = itemView.findViewById(R.id.ivCardImage);
            ivTick = itemView.findViewById(R.id.ivTick);
            ivDelete = itemView.findViewById(R.id.ivDelete);

            tvCardNo.setTypeface(fontRegular);
        }

    }

}
