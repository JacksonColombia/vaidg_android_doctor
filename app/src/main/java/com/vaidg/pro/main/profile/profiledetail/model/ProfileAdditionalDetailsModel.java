package com.vaidg.pro.main.profile.profiledetail.model;

import com.google.gson.Gson;
import com.vaidg.pro.pojo.profile.metaData.MetaDataArr;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.vaidg.pro.AppController.TAG;
import static com.vaidg.pro.utility.VariableConstant.DATA;

public class ProfileAdditionalDetailsModel {

    @Inject
    List<MetaDataArr> additionalDetailsList;

    @Inject
    public ProfileAdditionalDetailsModel() {
    }

    public void addAdditionalDetailsData(List<MetaDataArr> data) {
        additionalDetailsList.clear();
        additionalDetailsList.addAll(data);
    }

    public void updateData(int position, MetaDataArr data) {
        additionalDetailsList.set(position, data);
    }
}
