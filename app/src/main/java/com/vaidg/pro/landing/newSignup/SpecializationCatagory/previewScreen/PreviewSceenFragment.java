package com.vaidg.pro.landing.newSignup.SpecializationCatagory.previewScreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.vaidg.pro.databinding.FragmentPreviewSceenBinding;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.SpecializationCategoryContract;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;


/**
 * <h1>@PreviewSceenFragment</h1>
 * <p>this fragment is used to display preview initially before category fragements gets loaded
 * </P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class PreviewSceenFragment extends DaggerFragment implements PreviewSceenContract.View {

    @Inject
    SpecializationCategoryContract.Presenter mainPresenter;
    private FragmentPreviewSceenBinding binding;

    public PreviewSceenFragment() {
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentPreviewSceenBinding.inflate(inflater);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initOnClickLister();
    }

    private void initOnClickLister() {
        binding.btnPreviewStart.setOnClickListener(this::Onclick);
    }

    private void Onclick(View view) {
        mainPresenter.hidePreview();
    }
}
