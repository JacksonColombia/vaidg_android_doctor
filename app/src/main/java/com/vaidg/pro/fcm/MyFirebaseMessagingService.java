package com.vaidg.pro.fcm;

import static com.vaidg.pro.utility.VariableConstant.AES_MODE;
import static com.vaidg.pro.utility.VariableConstant.ANDROID_KEYSTORE;
import static com.vaidg.pro.utility.VariableConstant.FIXED_IV;
import static com.vaidg.pro.utility.VariableConstant.IV_PARAMETER_SPEC;
import static com.vaidg.pro.utility.VariableConstant.KEY_PRIVATEALIAS;
import static com.vaidg.pro.utility.VariableConstant.KEY_PUBLICALIAS;
import static com.vaidg.pro.utility.VariableConstant.RSA_MODE;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Base64;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.main.MainActivity;
import com.vaidg.pro.main.chats.ChatOnMessageCallback;
import com.vaidg.pro.pojo.callpojo.CallActions;
import com.vaidg.pro.pojo.callpojo.NewCallData;
import com.vaidg.pro.pojo.chat.ChatData;
import com.vaidg.pro.service.LocationPublishService;
import com.vaidg.pro.telecall.RxCallInfo;

import com.vaidg.pro.utility.CalendarEventHelper;
import com.vaidg.pro.utility.DataBaseChat;
import com.vaidg.pro.utility.ExitActivity;
import com.vaidg.pro.utility.NotificationHelper;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.encryptiondecryption.CryptographyExample;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilPrivateKey;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONObject;

/**
 * Created by murshid on 18/9/16.
 * <h2>MyFirebaseMessagingService</h2>
 * MyFirebaseMessagingService is used for receiveng messsage from fcm
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private static ChatOnMessageCallback chatListener;
    private SessionManager sessionManager;
    private KeyStore keyStore;

    public static void setChatListener(ChatOnMessageCallback chatList) {
        chatListener = chatList;
    }

    /**
     * Called when message is jsonRemoteMessage.
     *
     * @param remoteMessage Object representing the message jsonRemoteMessage from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "onMessageReceived Called" + remoteMessage.toString());
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. AppConfigData messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. AppConfigData messages are the type
        // traditionally used with GCM. Notification messages are only jsonRemoteMessage here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        sessionManager = SessionManager.getSessionManager(this);
        DataBaseChat db = new DataBaseChat(this);

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            try {
                JSONObject jsonRemoteMessage = new JSONObject(remoteMessage.getData());
                String action = jsonRemoteMessage.getString("action");
                String message = "";
                String title = "";
                if (!(action.equals("121") || action.equals("120"))) {
                    message = jsonRemoteMessage.optString("msg", "");
                    title = jsonRemoteMessage.optString("title", "");
                }


                Gson gson = new Gson();
                if (action.equals("112")) {
                    ChatData chatData = gson.fromJson(jsonRemoteMessage.getString("data"), ChatData.class);

                    checkForKeystore();
                    String messageFromDecrypt = "";
                    if (!chatData.getContent().contains("https://")) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            decryptPrivateKeyPostLollipop();
                            decryptPublicKeyPostLollipop();
                            messageFromDecrypt =
                                encryptData(decryptPrivateKeyPostLollipop(), decryptPublicKeyPostLollipop(), messageFromDecrypt, chatData.getContent());
                        } else {
                            decryptPrivateKeyPreMarshMallow();
                            decryptPublicKeyPreMarshMallow();
                            messageFromDecrypt = encryptData(decryptPrivateKeyPreMarshMallow(), decryptPublicKeyPreMarshMallow(), messageFromDecrypt, chatData.getContent());
                        }
                    }else{
                        messageFromDecrypt = chatData.getContent();
                    }
                    if (!VariableConstant.IS_CHATTING_OPENED) {
                        db.addNewChat(String.valueOf(chatData.getBid()), messageFromDecrypt, sessionManager.getProviderId(),
                                chatData.getFromID(), String.valueOf(chatData.getTimestamp()), chatData.getType(), "1");
                    }

                    if (chatData.getTimestamp() > sessionManager.getLastTimeStampMsg()) {
                        sessionManager.setLastTimeStampMsg(chatData.getTimestamp());

                        if (chatListener != null) {
                            chatListener.onMessageReceived(chatData);
                        }

                        if (!VariableConstant.IS_CHATTING_RESUMED) {
                            sessionManager.setChatCount(String.valueOf(chatData.getBid()), sessionManager.getChatCount(String.valueOf(chatData.getBid())) + 1);
                            sessionManager.setChatBookingID(String.valueOf(chatData.getBid()));
                            sessionManager.setChatCustomerName(chatData.getName());
                            sessionManager.setChatCustomerID(chatData.getFromID());
                            if(messageFromDecrypt.contains("http"))
                            NotificationHelper.sendChatNotification(this, chatData.getName(), "You got a new message", sessionManager.getChatNotificationId());
                            else
                            NotificationHelper.sendChatNotification(this, chatData.getName(), /*messageFromDecrypt*/"You got a new message", sessionManager.getChatNotificationId());
                            sessionManager.setChatNotificationId(sessionManager.getChatNotificationId() + 1);

                            Intent intent = new Intent();
                            intent.setAction(VariableConstant.INTENT_ACTION_NEW_CHAT);
                            sendBroadcast(intent);

                        } else /*if (VariableConstant.IS_IN_CALL)*/ {
                            if(messageFromDecrypt.contains("http"))
                                NotificationHelper.sendChatNotification(this, chatData.getName(), "You got a new message", sessionManager.getChatNotificationId());
                            else
                                NotificationHelper.sendChatNotification(this, chatData.getName(), /*messageFromDecrypt*/"You got a new message", sessionManager.getChatNotificationId());
                            sessionManager.setChatNotificationId(sessionManager.getChatNotificationId() + 1);
                        }
                    }

                    return;
                }

                /*
                 * Bug id : Trello 187
                 * Bug Description : Getting chat notification 2 times
                 * Fixed Description :  Handled the action 112. Before for chat notification the action was 1 but now it was 112.
                 * Dev : Murashid
                 * Date : 29-11-2018
                 * */
                if (!action.equals("25") && !action.equals("10") && !action.equals("1") && !action.equals("12") && !action.equals("5") && !action.equals("112") && !action.equals("120") ) {
                    NotificationHelper.sendNotification(this, action, title, message);
                }

                    /*
                    1 => New BookingStatus , 18 => BookingAccepted or rejected by email
                    5 = > Expired 12 => Cancel,Unassign 22 => ban, 23 => logout , 21 => offline
                    25 = > Profile activation 26 = > Booking updated from super admin
                    10=>cuatomer done the payment for booking,rate the customer now.
                    201=>Telle-Call Booking End alert notification
                    121=>Telle-Call ,Cut the active or the ongoingcall

                    * */
                if (action.equals("1")) {
                    VariableConstant.IS_BOOKING_UPDATED = true;
                    JSONObject jsonObjectBooking = new JSONObject(jsonRemoteMessage.optString("data", ""));

                    if (!jsonObjectBooking.equals(sessionManager.getLastBooking()) && !jsonObjectBooking.equals("")) {
                        sessionManager.setLastBooking(jsonObjectBooking.toString());
                        sessionManager.setIsNewBooking(true);
                        Log.d(TAG, "Booking from Fcm");
                       /* boolean isAssignedBooking = false;
                        if (jsonObjectBooking.has("status") && jsonObjectBooking.getString("status").equals("3")) {
                            sessionManager.setIsAssignedBooking(true);
                            isAssignedBooking = true;
                        }*/
                        if (VariableConstant.IS_MYBOOKING_OPENED /*&& !isAssignedBooking*/) {
                            //justRefresh();
                            Intent intent = new Intent();
                            intent.setAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
                            sendBroadcast(intent);
                        } else if (VariableConstant.IS_ACCEPTEDBOOKING_OPENED /*&& isAssignedBooking*/) {
                            //justRefresh();
                            Intent intent = new Intent();
                            intent.setAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
                            sendBroadcast(intent);
                        } else {
                            sessionManager.setNewBookingFromMain(true);
                            VariableConstant.IS_BOOKING_UPDATED = true;
                            Intent intentOpen = new Intent(this, MainActivity.class);
                            intentOpen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                            startActivity(intentOpen);
                        }

//                            Utility.updateBookingAck(sessionManager, AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObjectBooking.getString("bookingId"), sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
                    }
                    if (jsonObjectBooking.getString("bookingType").equals("1")) {
                        sessionManager.setIsNowBooking(true);
                    } else {
                        sessionManager.setIsNowBooking(false);
                    }
                    NotificationHelper.sendNotification(this, action, title, message);
                } else if (action.equals("12") || action.equals("5") || action.equals("18")) {
                    JSONObject expireCancelJsonObject = new JSONObject(jsonRemoteMessage.optString("data", ""));
                    if (expireCancelJsonObject != null && !expireCancelJsonObject.equals("")) {
                        String bookingId = expireCancelJsonObject.getString("bookingId");
                        String header = expireCancelJsonObject.has("statusMsg") ? expireCancelJsonObject.getString("statusMsg") :
                                title;
                        String msg = expireCancelJsonObject.has("cancellationReason") ? expireCancelJsonObject.getString("cancellationReason") :
                                message;

                        if (expireCancelJsonObject.has("msg")) {
                            msg = expireCancelJsonObject.getString("msg");
                        }

                        if (!bookingId.equals(sessionManager.getLastBookingIdCancel())) {
                            sessionManager.setLastBookingIdCancel(bookingId);
                            Intent intent = new Intent();
                            intent.setAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
                            intent.putExtra("cancelid", bookingId);
                            intent.putExtra("header", header);
                            intent.putExtra("msg", msg);
                            intent.putExtra("action", action);
                            sendBroadcast(intent);

                            VariableConstant.IS_BOOKING_UPDATED = true;
                            NotificationHelper.sendNotification(this, action, header, msg);

                            Log.d(TAG, "Booking cancel from Fcm");
                        }

                        if (expireCancelJsonObject.has("reminderId")) {
                            if (!expireCancelJsonObject.getString("reminderId").equals("")) {
                                CalendarEventHelper calendarEventHelper = new CalendarEventHelper(this);
                                calendarEventHelper.deleteEvent(expireCancelJsonObject.getString("reminderId"));
                            }
                        }
                    }
                    NotificationHelper.sendNotification(this, action, title, message);
                } else if (action.equals("21")) {
                    if (Utility.isMyServiceRunning(this, LocationPublishService.class)) {
                        Intent stopIntent = new Intent(this, LocationPublishService.class);
                        stopIntent.setAction(VariableConstant.ACTION.STOPFOREGROUND_ACTION);
                        startService(stopIntent);
                        AppController.getInstance().getMqttHelper().disconnect(/*sessionManager.getPhoneNumber()*/);
                    }
                    Intent intent = new Intent();
                    intent.setAction(VariableConstant.INTENT_ACTION_MAKE_OFFLINE);
                    sendBroadcast(intent);
                } else if (action.equals("23") || action.equals("22")) {
                    Utility.logoutSessionExiperd(sessionManager, this);
                } else if (action.equals("26")) {
                    JSONObject expireCancelJsonObject = new JSONObject(jsonRemoteMessage.getString("data"));
                    String bookingId = expireCancelJsonObject.getString("bookingId");
                    String header = expireCancelJsonObject.has("statusMsg") ? expireCancelJsonObject.getString("statusMsg") :
                            title;
                    String msg = expireCancelJsonObject.has("cancellationReason") ? expireCancelJsonObject.getString("cancellationReason") :
                            message;

                    if (expireCancelJsonObject.has("msg")) {
                        msg = expireCancelJsonObject.getString("msg");
                    }

                    Intent intent = new Intent();
                    intent.setAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
                    intent.putExtra("cancelid", bookingId);
                    intent.putExtra("header", header);
                    intent.putExtra("msg", msg);
                    intent.putExtra("action", action);
                    sendBroadcast(intent);
                    VariableConstant.IS_BOOKING_UPDATED = true;
                    NotificationHelper.sendNotification(this, action, header, msg);
                    Log.d(TAG, "Booking updated from Fcm");
                } else if (action.equals("25")) {
                    sessionManager.setIsProfileAcivated(true);
                    if (VariableConstant.IS_MYBOOKING_OPENED || VariableConstant.IS_ACCEPTEDBOOKING_OPENED) {
                        Intent intent = new Intent();
                        intent.setAction(VariableConstant.INTENT_ACTION_PROFILE_ACTIVATION);
                        sendBroadcast(intent);
                    } else {
                        NotificationHelper.sendProfileActivationNotification(this, action, title, message);
                    }
                } else if (action.equals("121")) {
                    if (VariableConstant.isAppOpened) {
                        Utility.printLog("FCMlog1", jsonRemoteMessage.toString());
                        if (jsonRemoteMessage.has("action")) {
                            jsonRemoteMessage.remove("action");
                        }
                        jsonRemoteMessage.put("action", CallActions.CALL_ENDED.value);
                        RxCallInfo.getInstance().emitData(jsonRemoteMessage.toString());
                    } else {
                        ExitActivity.exitApplicationAndRemoveFromRecent(this);
                    }
//                        JSONObject jobs=new JSONObject();
//                        jobs.put("status", 1);
//                        JSONObject emitJson=new JSONObject();
//                        emitJson.put("action", CallActions.CALL_ENDED.value);
//                        RxCallInfo.getInstance().emitData(emitJson.toString());
//                        AppController.getInstance().getMqttHelper().publish(MqttEvents.CallsAvailability.value + "/" + sessionManager.getProviderId(), jobs, 0, true);//UserId

                } else if (action.equals("120")) {
                    Utility.printLog("FCMlog1", jsonRemoteMessage.toString());
                    if (jsonRemoteMessage.has("action")) {
                        jsonRemoteMessage.remove("action");
                    }
                    jsonRemoteMessage.put("action", CallActions.NEW_CALL.value);
                    handleNewCall(jsonRemoteMessage.toString());

                    /*JSONObject obj = new JSONObject();
                    if (obj != null && !obj.equals("")) {
                        sessionManager.setChatCount(obj.getString("bookingId"), sessionManager.getChatCount(obj.getString("bookingId") + 1));
                        sessionManager.setChatBookingID(obj.getString("bookingId"));
                        sessionManager.setChatCustomerName(obj.getString("callerName"));
                        sessionManager.setChatCustomerID(obj.getString("callerId"));

                        JSONObject tempObj = new JSONObject();
                        tempObj.put("status", 0);

                        Log.d(TAG, "Calling received: 2 ");

                        if (AppController.getInstance().getMqttHelper().isMqttConnected()) {
                            AppController.getInstance().getMqttHelper().publish(MqttEvents.CallsAvailability.value + "/" + AppController.getInstance().getCallHelper().getUserId(), tempObj, 0, true);
                        } else {
                            AppController.getInstance().getMqttHelper().setUserId(AppController.getInstance().getCallHelper().getUserId(), tempObj);
                            AppController.getInstance().getMqttHelper().createMQttConnection(sessionManager.getPhoneNumber(), false);
                        }
                        AppController.getInstance().getCallHelper().setActiveOnACall(true, true);
                        CallingApis.OpenIncomingCallScreen(obj, this);

                        Log.d(TAG, "Calling received: 3");
                    }*/

                } else if (action.equals("10")) {
                    try {
                        Log.d(TAG, "onMessageReceived: 10");
                        JSONObject obj = new JSONObject(jsonRemoteMessage.getString("data"));
                        final String bookingId = obj.optString("bookingId");
                        if (Utility.isApplicationSentToBackground(this)) {
                            Log.d(TAG, "onMessageReceived: appbckgrnd");
                            NotificationHelper.sendRateCustomerNotification(this, bookingId, title, message);
                        } else {
                            Log.d(TAG, "onMessageReceived: noappbckgrnd");
                            Uri notification = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                                    + "://" + getPackageName() + "/raw/incoming");
                            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                            r.play();
                            Intent intent = new Intent(this, MessageNotificationActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("rateNotfcnCustomer", true);
                            bundle.putString("bookingId", bookingId);
                            bundle.putString("RemoteMessage", message);
                            bundle.putString("RemoteTitle", title);
                            intent.putExtras(bundle);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (action.equals("201")) {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
     * handle new Call data and open incoming screen
     */
    public void handleNewCall(String callDataResponse) {
        try {
            NewCallData callData = new Gson().fromJson(callDataResponse, NewCallData.class);
//            NewCallData callData = newCallMqttResponse.getData();
            Log.d("log1", "handleNewCallfcm: " + callData.getCallId());
            //CallingApis.OpenIncomingCallScreen(callData, this);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
    @SuppressLint("NewApi")
    public String decryptPrivateKeyPostLollipop() {
        String str = "";
        Log.i("TEST", "decryptData marsh melloa ");
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE);
            c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
            byte[] encryptedBytes = Base64.decode(sessionManager.getVirgilPrivateKey(), Base64.DEFAULT);

            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str;
    }

    @SuppressLint("NewApi")

    public String decryptPublicKeyPostLollipop() {
        String str = "";
        Log.i("TEST", "decryptData marsh melloa ");
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE);
            c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
            byte[] encryptedBytes = Base64.decode(sessionManager.getVirgilPublicKey(), Base64.DEFAULT);

            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * <h2>getSecretPrivateKeyPreMarshMallow</h2>
     * This method is used to get the secret key for pre marsh mallow device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPrivateKeyPreMarshMallow() throws Exception {
        String enryptedKeyB64 = sessionManager.getEncryptionPrivateKey();
        Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
        // need to check null, omitted here
        byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaPrivateKeyDecrypt(encryptedKey);
        Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
        return new SecretKeySpec(key, "AES");
    }

    /**
     * <h2>getSecretPublicKeyPreMarshMallow</h2>
     * This method is used to get the secret key for pre marsh mallow device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPublicKeyPreMarshMallow() throws Exception {
        String enryptedKeyB64 = sessionManager.getEncryptionPublicKey();
        Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
        // need to check null, omitted here
        byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaPublicKeyDecrypt(encryptedKey);
        Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
        return new SecretKeySpec(key, "AES");
    }


    private byte[] rsaPrivateKeyDecrypt(byte[] encrypted) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
        CipherInputStream cipherInputStream = new CipherInputStream(
            new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }
        return bytes;
    }

    private byte[] rsaPublicKeyDecrypt(byte[] encrypted) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
        CipherInputStream cipherInputStream = new CipherInputStream(
            new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }
        return bytes;
    }



    public String decryptPrivateKeyPreMarshMallow() {
        Log.i("TEST", "decryptData else  ");
        Cipher c;
        String str = "";
        try {
            c = Cipher.getInstance(AES_MODE, "BC");
            c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPreMarshMallow(), IV_PARAMETER_SPEC);
            byte[] encryptedBytes = Base64.decode(sessionManager.getEncryptionPrivateKey(), Base64.DEFAULT);
            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);


        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }


    public String decryptPublicKeyPreMarshMallow() {
        Log.i("TEST", "decryptData else  ");
        Cipher c;
        String str = "";
        try {
            c = Cipher.getInstance(AES_MODE, "BC");
            c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPreMarshMallow(), IV_PARAMETER_SPEC);
            byte[] encryptedBytes = Base64.decode(sessionManager.getEncryptionPublicKey(), Base64.DEFAULT);
            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }


    /**
     * <h2>getSecretPrivateKeyPostLollipop</h2>
     * This method is used to get the secret key for post lollipop device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPrivateKeyPostLollipop() throws Exception {
        Key s = keyStore.getKey(KEY_PRIVATEALIAS, null);
        Log.i("TEST", "key " + s.toString());
        return keyStore.getKey(KEY_PRIVATEALIAS, null);
    }

    /**
     * <h2>getSecretPublicKeyPostLollipop</h2>
     * This method is used to get the secret key for post lollipop device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPublicKeyPostLollipop() throws Exception {
        Key s = keyStore.getKey(KEY_PUBLICALIAS, null);
        Log.i("TEST", "key " + s.toString());
        return keyStore.getKey(KEY_PUBLICALIAS, null);
    }


    public void checkForKeystore() {
        try {
            keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
            keyStore.load(null);
        } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
        }
        try {
            if (!keyStore.containsAlias(KEY_PRIVATEALIAS)) {
                //mSignUpView.checkForVersion();
            }
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }
    private String encryptData(String privateKey, String publicKey, String messageFromDecrypt, String messageToDecrypt) {

        try {
            CryptographyExample cryptographyExample = new CryptographyExample();
            VirgilCrypto crypto = new VirgilCrypto();
            // Import private key
            VirgilPrivateKey importedPrivateKey = cryptographyExample.importPrivateKey(privateKey, crypto);
            // Decrypt data
            // byte[] encryptedData = Base64.decode(messageToDecrypt, Base64.DEFAULT);/*Base64.getMimeDecoder().decode(chatData.get(position).getContent());*/
            messageFromDecrypt = cryptographyExample.dataDecryption(messageToDecrypt, importedPrivateKey);
        } catch (CryptoException e) {
            e.printStackTrace();
        }
        return messageFromDecrypt;
    }
}
