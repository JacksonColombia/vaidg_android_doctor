package com.vaidg.pro.main.schedule.scheduleselection;

import android.app.Activity;
import android.widget.ImageView;
import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import java.util.ArrayList;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface ScheculeSelectionContract {
  interface View extends BaseView {
    void startDay(String startDay);

    void endDay(String endDay);

    void startDayCalendar(String startDay, String displayStartDay);

    void endDayCalendar(String endDay, String displayEndDay);

    void addDays(String days);

    void removeDays(String days);
    }

  interface Presenter extends BasePresenter {
    /**
     * method for the sorting the days
     *
     * @param selectedDays list of unsorted days
     * @return list of sorted days
     */
    ArrayList<String> sortDays(ArrayList<String> selectedDays);
    /**
     * method for adding and removing the days as well as the change the image view
     *
     * @param selectedDays list of already selected days
     * @param days         current selected or unselected days
     * @param ivDay        imageview of the current selected or unselected days
     */
    void addRemoveDays(ArrayList<String> selectedDays, String days, ImageView ivDay);

    /**
     * method for showing date picker for selecting the start date and end date based on date Picker id
     *  @param datePickerVarID  id for datepicker 1 => start date 2=> endDate
     * @param lastSelectedDate last selected date
     */
    void selectDate(int datePickerVarID, String lastSelectedDate);

    /**
     * method for set minimu, time for already selected date
     *
     * @param startDate start Date
     */
    void setMinimumDateForAlreadySelectedStartDate(String startDate);
    /**
     * method for calculating the end days from adding remaining days of the month and param days
     *
     * @param days days mention how many days we want to add extra with remain days of current month
     */
    void endDay(int days);

    /**
     * method for calculating the current date
     */
    void startDay();

  }
}