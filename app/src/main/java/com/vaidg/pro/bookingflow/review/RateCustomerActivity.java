package com.vaidg.pro.bookingflow.review;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.bookingflow.ReceiptDialogFragment;
import com.vaidg.pro.main.MainActivity;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.pojo.history.Accounting;
import com.vaidg.pro.utility.CircleTransform;
import com.vaidg.pro.utility.DataBaseChat;
import com.vaidg.pro.utility.DialogHelper;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import org.json.JSONObject;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

import static com.vaidg.pro.utility.Utility.convertUTCToServerFormat;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_SPACE_DD_MM_YYYY;

public class RateCustomerActivity extends DaggerAppCompatActivity implements RateCustomerContract.View, View.OnClickListener {
    private SessionManager sessionManager;
    private ProgressDialog progressDialog;
    @Inject
    RateCustomerContract.Presenter presenter;
    private EditText etReview;
    private RatingBar ratingStar;
    private String name = "";
    private Booking booking;
    private boolean isFromNotification = true, isRefreshing = true;
    private ImageView ivCustomer;
    private TextView tvPrice, tvPriceLabel, tvSubmit, tvDate, rateTitle1, tvCustomerName, tvCustomerAge;
    private Button tvReceipt, btnViewPrescription;
    private String bookingId = "";
    private Context mContext;
    private LinearLayout llService;
    private Typeface fontMedium, fontRegular, fontBold;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_customer);
        mContext = this;
        init();
    }

    private void init() {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.submitting));
        progressDialog.setCancelable(false);
        fontMedium = Utility.getFontMedium(this);
        fontRegular = Utility.getFontRegular(this);
        fontBold = Utility.getFontBold(this);
        rateTitle1 = findViewById(R.id.rateTitle1);
        tvDate = findViewById(R.id.tvDate);
        tvPriceLabel = findViewById(R.id.tvPriceLabel);
        tvPrice = findViewById(R.id.tvPrice);
        tvCustomerName = findViewById(R.id.tvCustomerName);
        tvCustomerAge = findViewById(R.id.tvCustomerAge);
        tvSubmit = findViewById(R.id.tvSubmit);
        etReview = findViewById(R.id.etReview);
        ratingStar = findViewById(R.id.ratingStar);
        ivCustomer = findViewById(R.id.ivCustomer);
        tvReceipt = findViewById(R.id.tvReceipt);
        btnViewPrescription = findViewById(R.id.btnViewPrescription);
        llService = findViewById(R.id.llService);
        rateTitle1.setTypeface(fontBold);
        tvDate.setTypeface(fontMedium);
        tvPriceLabel.setTypeface(fontRegular);
        tvReceipt.setTypeface(fontRegular);
        btnViewPrescription.setTypeface(fontRegular);
        tvPrice.setTypeface(fontBold);
        tvCustomerName.setTypeface(fontBold);
        tvCustomerAge.setTypeface(fontBold);
        tvSubmit.setTypeface(fontBold);
        etReview.setTypeface(fontMedium);
        tvSubmit.setOnClickListener(this);
        tvReceipt.setOnClickListener(this);
        btnViewPrescription.setOnClickListener(this);
        rateTitle1.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_16), mContext));
        tvCustomerName.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_16), mContext));
        tvCustomerAge.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_14), mContext));
        tvReceipt.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_16), mContext));
        btnViewPrescription.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_16), mContext));
        tvPriceLabel.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_18), mContext));
        tvPrice.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_35), mContext));
        tvDate.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_14), mContext));

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isFromNotification = bundle.getBoolean("rateNotfcnCustomer", false);
        }
        if (isFromNotification) {
            try {
                bookingId = bundle.getString("bookingId");
            } catch (Exception e) {
                e.printStackTrace();
            }
            //call api
            presenter.getBookingById(bookingId);
        } else {
//            Toast.makeText(this,"sdjhf",Toast.LENGTH_LONG).show();
            booking = (Booking) bundle.getSerializable("booking");
            bookingId = booking.getBookingId();
            //call api
            presenter.getBookingById(bookingId);
//            onSuccess("", isFromNotification, isRefreshing);
        }
//
    }

    @Override
    public void onBackPressed() {
        Utility.progressDialogCancel(this, progressDialog);
        closeActivity();
    }

    private void closeActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("showDialog", false);
        startActivity(intent);
        overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSubmit:
                isFromNotification = false;
                isRefreshing = false;
                try {
                    Log.d("mura", "onClick: " + ratingStar.getRating());
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("bookingId", bookingId);
                    jsonObject.put("rating", "" + ratingStar.getRating());
                    jsonObject.put("review", etReview.getText().toString());
                    presenter.setCustomerRating(jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tvReceipt:
                if (booking != null) {
                    ReceiptDialogFragment.newInstance(booking).show(getSupportFragmentManager(), "");
                }
                break;

            case R.id.btnViewPrescription:
                if (booking != null) {
                    if (booking.getPdfFile() != null && !booking.getPdfFile().isEmpty()) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(booking.getPdfFile())));
                    } else {
                        DialogHelper.customAlertDialog(RateCustomerActivity.this, getResources().getString(R.string.prescription), getResources().getString(R.string.msgPrescriptionNotFound), getResources().getString(R.string.oK));
                    }
                }
                break;
        }
    }

    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    public void onSuccess(String msg, boolean isFromNotification, boolean ispageRefreshing) {
        if (isFromNotification) {
            booking = new Gson().fromJson(msg, Booking.class);
        }
        if (ispageRefreshing) {
            Accounting accounting = booking.getAccounting();
            if (!booking.getProfilePic().equals("")) {
                Glide.with(this).setDefaultRequestOptions(new RequestOptions().error(R.drawable.profile_default_image)
                        .transform(new CircleTransform(this))
                        .placeholder(R.drawable.profile_default_image))
                        .load(booking.getProfilePic())
                        .into(ivCustomer);
            }
            bookingId = booking.getBookingId();
            name = booking.getFirstName() + " " + booking.getLastName();
            tvCustomerName.setText(name);
            if (booking.getGender() != null && !booking.getGender().isEmpty() && booking.getAgeString() != null && !booking.getAgeString().isEmpty())
                tvCustomerAge.setText(booking.getAgeString()+ " " + mContext.getResources().getString(R.string.comma) + " " + Utility.gender(booking.getGender()));
            else if (booking.getGender() != null && booking.getGender().isEmpty() && booking.getAgeString() != null && !booking.getAgeString().isEmpty())
                tvCustomerAge.setText(booking.getAgeString());
            else if (booking.getGender() != null && !booking.getGender().isEmpty() && booking.getAgeString() != null && booking.getAgeString().isEmpty())
                tvCustomerAge.setText(Utility.gender(booking.getGender()));

            try {
                String endDate = Utility.changeDateTimeFormat(convertUTCToServerFormat(booking.getBookingRequestedFor()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_DD_MM_YYYY);
                String[] endDatearr = endDate.split(" ");
                tvDate.setText(Utility.getDayOfMonthSuffix(Integer.parseInt(endDatearr[0]), endDatearr[1] + " " + endDatearr[2]));
                sessionManager.clearChatCountPreference(bookingId);
                DataBaseChat db = new DataBaseChat(this);
                db.deleteData(bookingId);
                tvPrice.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            LayoutInflater inflater = getLayoutInflater();

            if (booking.getCallType().equals("3")) {
                llService.setVisibility(View.GONE);
             /*   View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontMedium);
                tvServiceFee.setTypeface(fontBold);
                tvServiceFeeLabel.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_16), mContext));
                tvServiceFee.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_16), mContext));

//                tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
                tvServiceFeeLabel.setText(getResources().getString(R.string.consultingfee));
                tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);*/
            } else {
                llService.setVisibility(View.GONE);
            }
     /* if (!booking.getBookingModel().equals("3")) {
        if (booking.getServiceType().equals("1")) {
          if (booking.getCartData() != null) {
            for (ServiceItem serviceItem : booking.getCartData()) {
              View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
              TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
              TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
              tvServiceFeeLabel.setTypeface(fontRegular);
              tvServiceFee.setTypeface(fontRegular);
              tvServiceFeeLabel.setText(serviceItem.getServiceName() + " X " + serviceItem.getQuntity());
              tvServiceFee.setText(Utility.getPrice(serviceItem.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
              llService.addView(serviceView);
            }
          }
        } else {

          View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
          TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
          TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
          tvServiceFeeLabel.setTypeface(fontMedium);
          tvServiceFee.setTypeface(fontBold);
          tvServiceFeeLabel.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_16),mContext));
          tvServiceFee.setTextSize(Utility.pixelsToSp(getResources().getDimension(R.dimen.sp_16),mContext));

//                tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
          tvServiceFeeLabel.setText(getResources().getString(R.string.consultingfee));
          tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
          llService.addView(serviceView);
        }
      } else {
        View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
        TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
        TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
        tvServiceFeeLabel.setTypeface(fontRegular);
        tvServiceFee.setTypeface(fontRegular);
        tvServiceFeeLabel.setText(getString(R.string.jobPay));
        tvServiceFee.setText(Utility.getPrice(accounting.getProviderEarning(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        llService.addView(serviceView);
      }*/
        } else {
            AppController.getInstance().getMixpanelHelper().postingReview(name, bookingId);
            closeActivity();
        }
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, this);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEmptyReview() {
        Toast.makeText(this, getString(R.string.plsEnterReview), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }
}
