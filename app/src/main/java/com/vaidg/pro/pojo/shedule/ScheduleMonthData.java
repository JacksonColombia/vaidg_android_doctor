package com.vaidg.pro.pojo.shedule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 23-Oct-17.
 */

public class ScheduleMonthData implements Serializable {
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("schedule")
    @Expose
    private ArrayList<Schedule> schedule;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<Schedule> getSchedule() {
        return schedule;
    }

    public void setSchedule(ArrayList<Schedule> schedule) {
        this.schedule = schedule;
    }
}
