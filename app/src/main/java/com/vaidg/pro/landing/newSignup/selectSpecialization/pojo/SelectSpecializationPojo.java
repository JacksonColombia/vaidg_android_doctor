package com.vaidg.pro.landing.newSignup.selectSpecialization.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;

import java.util.List;

public class SelectSpecializationPojo {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<SelectSpecializationData> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SelectSpecializationData> getData() {
        return data;
    }

    public void setData(List<SelectSpecializationData> data) {
        this.data = data;
    }
}
