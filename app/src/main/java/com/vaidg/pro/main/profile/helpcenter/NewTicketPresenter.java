package com.vaidg.pro.main.profile.helpcenter;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.app.Activity;
import android.util.Log;
import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.profile.helpcenter.HelpCenterData;
import com.vaidg.pro.pojo.profile.helpcenter.HelpCenterPojo;
import com.vaidg.pro.pojo.profile.helpcenter.Ticket;
import com.vaidg.pro.pojo.profile.helpcenter.TicketData;
import com.vaidg.pro.pojo.profile.helpcenter.TicketPojo;
import com.vaidg.pro.utility.OkHttp3ConnectionStatusCode;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.ServiceUrl;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 29-Dec-17.
 */

public class NewTicketPresenter implements NewTicketContract.Presenter {

    @Inject
    Activity activity;
    @Inject
    NewTicketContract.View view;
    @Inject
    NetworkService service;
    @Inject
    SessionManager sessionManager;
    @Inject
    Gson gson;

    @Inject
    NewTicketPresenter() {
    }
    @Override
    public void createTicket(String sessionToken, JSONObject jsonObject) {
        view.startProgressBar();
        try {
            if (jsonObject.getString("subject").equals("")) {
                view.onErrorSubject();
                return;
            } else if (jsonObject.getString("priority").equals("")) {
                view.onErrorPriority();
                return;
            } else if (jsonObject.getString("body").equals("")) {
                view.onErrorComment();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        service.postTicket(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if (view != null)
                                    view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    if (view != null)
                                    view.onSuccessRaiseTicket(jsonObject.getString("message"));
                                } else {
                                    if (view != null)
                                        view.onFailure(Utility.getMessage(errorBody));

                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        createTicket( sessionToken,  jsonObject);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        if (view != null) {
                                            view.stopProgressBar();
                                            view.onFailure(Utility.getMessage(errorBody));
                                        }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().toast(Utility.getMessage(msg));
                                        Utility.logoutSessionExiperd(sessionManager, activity);
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                                if (view != null)
                                    view.stopProgressBar();
                                Utility.logoutSessionExiperd(sessionManager, activity);
                                break;
                            default:
                                if (view != null) {
                                    view.stopProgressBar();
                                    view.onFailure(Utility.getMessage(errorBody));
                                }
                                break;
                        }
                    } catch (Exception e) {
                        if (view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if (view != null) {
                        view.stopProgressBar();
                        view.onFailure();

                    }
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {

                }
            });



    }


    @Override
    public void sendComment(String sessionToken, JSONObject jsonObject) {
        view.startProgressBar();
        try {
            if (jsonObject.getString("body").equals("")) {
                view.onErrorComment();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



        service.putTicket(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, jsonObject.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if (view != null)
                                    view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    if (view != null)
                                        view.onSuccessComment();
                                } else {
                                    if (view != null)
                                        view.onFailure(Utility.getMessage(errorBody));

                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        createTicket( sessionToken,  jsonObject);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        if (view != null) {
                                            view.stopProgressBar();
                                            view.onFailure(Utility.getMessage(errorBody));
                                        }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().toast(Utility.getMessage(msg));
                                        Utility.logoutSessionExiperd(sessionManager, activity);
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                                if (view != null)
                                    view.stopProgressBar();
                                Utility.logoutSessionExiperd(sessionManager, activity);
                                break;
                            default:
                                if (view != null) {
                                    view.stopProgressBar();
                                    view.onFailure(Utility.getMessage(errorBody));
                                }
                                break;
                        }
                    } catch (Exception e) {
                        if (view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if (view != null) {
                        view.stopProgressBar();
                        view.onFailure();

                    }
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {

                }
            });

    }


    @Override
    public void getTicketDetails(String sessionToken, String ticketId) {
        view.startProgressBar();

        service.getTicketHistory(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, ticketId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if (view != null)
                                    view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    gson = new Gson();
                                    TicketPojo ticketPojo = gson.fromJson(response, TicketPojo.class);
                                    if (view != null)
                                    view.onSuccessTicket(ticketPojo.getData());
                                } else {
                                    if (view != null)
                                        view.onFailure(Utility.getMessage(errorBody));

                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        getTicketDetails( sessionToken, ticketId);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        if (view != null) {
                                            view.stopProgressBar();
                                            view.onFailure(Utility.getMessage(errorBody));
                                        }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().toast(Utility.getMessage(msg));
                                        Utility.logoutSessionExiperd(sessionManager, activity);
                                    }
                                });
                                break;
                            case SESSION_LOGOUT:
                                if (view != null)
                                    view.stopProgressBar();
                                Utility.logoutSessionExiperd(sessionManager, activity);
                                break;
                            default:
                                if (view != null) {
                                    view.stopProgressBar();
                                    view.onFailure(Utility.getMessage(errorBody));
                                }
                                break;
                        }
                    } catch (Exception e) {
                        if (view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if (view != null) {
                        view.stopProgressBar();
                        view.onFailure();

                    }
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {

                }
            });

    }


    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    interface NewTicketPresenterImple {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String msg);

        void onFailure();

        void onSuccessRaiseTicket(String msg);

        void onSuccessComment();

        void onSuccessTicket(TicketData ticket);

        void onErrorSubject();

        void onErrorPriority();

        void onErrorComment();
    }
}
