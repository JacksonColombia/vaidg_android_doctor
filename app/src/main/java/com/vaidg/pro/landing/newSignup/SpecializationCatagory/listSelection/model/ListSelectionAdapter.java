package com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection.model;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ListSelectionItemBinding;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;
import java.util.List;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * <h1>@ListSelectionAdapter</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class ListSelectionAdapter extends RecyclerView.Adapter<ListSelectionAdapter.ListSelectionViewHolder> {

    private ListSelectItemCallBack callBack;
    private boolean isSingleSelection;
    private ArrayList<PreDefined> preDefined;
    private Context context;
    private long mLastClickTime = System.currentTimeMillis();
    private static final long CLICK_TIME_INTERVAL = 300;

    public ListSelectionAdapter(List<PreDefined> preDefined, boolean isSingleSelection) {
        this.preDefined = (ArrayList<PreDefined>) preDefined;
        this.isSingleSelection = isSingleSelection;
    }

    public void provideCallBack(ListSelectItemCallBack callBack) {
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public ListSelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListSelectionItemBinding binding = ListSelectionItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        context = parent.getContext();
        return new ListSelectionViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ListSelectionViewHolder holder, int position) {
        holder.binding.tvListSelection.setText(preDefined.get(position).getName());
        if (preDefined.get(position).isSelected()) {
            holder.binding.ivTick.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_checked));
            Utility.setTextColor(context, holder.binding.tvListSelection, R.color.lighter_blue);
        } else {
            holder.binding.ivTick.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_unselected));
            Utility.setTextColor(context, holder.binding.tvListSelection, R.color.darkTextColor);
        }
    }

    @Override
    public int getItemCount() {
        return preDefined != null ? preDefined.size() : ZERO;
    }

    class ListSelectionViewHolder extends RecyclerView.ViewHolder {

        ListSelectionItemBinding binding;

        ListSelectionViewHolder(@NonNull ListSelectionItemBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
            binding.clMain.setOnClickListener(this::onClick);
        }

        public void onClick(View v) {
            if (callBack != null) {
                long now = System.currentTimeMillis();
                if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
                    return;
                }
                mLastClickTime = now;
                new Handler().postDelayed(() -> callBack.onItemSelected(getAdapterPosition(), isSingleSelection), 200);
            }
        }
    }
}
