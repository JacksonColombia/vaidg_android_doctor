package com.vaidg.pro.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.pro.R;
import com.vaidg.pro.pojo.booking.QuestionAnswer;
import com.vaidg.pro.utility.Utility;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Ali on 7/24/2018.
 */
public class QuestionSymptomAdapterGrid extends RecyclerView.Adapter<QuestionSymptomAdapterGrid.MyViewHolder> {

  private Context mContext;
  private ArrayList<QuestionAnswer> questionAndAnswer;
  private Typeface fontMedium,fontRegular;
  
  public QuestionSymptomAdapterGrid(Context context,ArrayList<QuestionAnswer> bidQuestionAnswers) {
    this.mContext = context;
    this.questionAndAnswer = bidQuestionAnswers;
    fontMedium = Utility.getFontMedium(mContext);
    fontRegular = Utility.getFontRegular(mContext);
  }

  @NonNull
  @Override
  public QuestionSymptomAdapterGrid.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_question_answer, parent, false);
    mContext = parent.getContext();
    return new MyViewHolder(itemView);
  }

 

  @Override
  public void onBindViewHolder(@NonNull QuestionSymptomAdapterGrid.MyViewHolder holder, int position) {

      if(TextUtils.isEmpty(questionAndAnswer.get(position).getQuestion()))
          holder.tvQuestion.setVisibility(View.GONE);
      else
         holder.tvQuestion.setText("Q. " + questionAndAnswer.get(position).getQuestion());
   if(questionAndAnswer.get(position).getQuestionType().equals("12")){
      holder.tvAnswer.setVisibility(View.GONE);
      holder.rvJobPhotosQuestion.setVisibility(View.VISIBLE);
     holder.rvJobVideoQuestion.setVisibility(View.GONE);
     holder.rvJobPhotosQuestion.setLayoutManager(new LinearLayoutManager(((Activity)mContext), LinearLayoutManager.HORIZONTAL, false));
      ArrayList<String> imageUrls = new ArrayList<>();
      imageUrls.addAll(Arrays.asList(questionAndAnswer.get(position).getAnswer().split(",")));
      holder.rvJobPhotosQuestion.setAdapter(new JobImageListAdapter(((Activity)mContext), imageUrls));
    }else if(questionAndAnswer.get(position).getQuestionType().equals("13"))
    {
      holder.tvAnswer.setVisibility(View.GONE);
      holder.rvJobPhotosQuestion.setVisibility(View.GONE);
      holder.rvJobVideoQuestion.setVisibility(View.VISIBLE);
      holder.rvJobVideoQuestion.setLayoutManager(new LinearLayoutManager(((Activity)mContext), LinearLayoutManager.HORIZONTAL, false));
      ArrayList<String> imageUrls = new ArrayList<>();
      imageUrls.addAll(Arrays.asList(questionAndAnswer.get(position).getAnswer().split(",")));
      holder.rvJobVideoQuestion.setAdapter(new JobVideoListAdapter(((Activity)mContext), imageUrls));
    }else{
     holder.tvAnswer.setVisibility(View.VISIBLE);
     holder.rvJobPhotosQuestion.setVisibility(View.GONE);
     holder.rvJobVideoQuestion.setVisibility(View.GONE);
      holder.tvAnswer.setText(questionAndAnswer.get(position).getAnswer());
    }
  }

  @Override
  public int getItemCount() {
    return questionAndAnswer == null ? 0 : questionAndAnswer.size();
  }


  class MyViewHolder extends RecyclerView.ViewHolder {
    
    TextView tvQuestion,tvAnswer;
    RecyclerView rvJobPhotosQuestion,rvJobVideoQuestion;

    MyViewHolder(View itemView) {
      super(itemView);
      rvJobPhotosQuestion = itemView.findViewById(R.id.rvJobPhotosQuestion);
      rvJobVideoQuestion = itemView.findViewById(R.id.rvJobVideoQuestion);
      tvQuestion = itemView.findViewById(R.id.tvQuestion);
      tvAnswer = itemView.findViewById(R.id.tvAnswer);
      tvQuestion.setTypeface(fontMedium);
      tvAnswer.setTypeface(fontRegular);
    }
  }

}
