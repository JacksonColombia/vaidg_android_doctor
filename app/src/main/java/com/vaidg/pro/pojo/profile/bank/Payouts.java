package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Payouts implements Serializable {

	@SerializedName("debit_negative_balances")
	@Expose
	private boolean debitNegativeBalances;

	@SerializedName("statement_descriptor")
	@Expose
	private Object statementDescriptor;

	@SerializedName("schedule")
	@Expose
	private Schedule schedule;

	public boolean isDebitNegativeBalances(){
		return debitNegativeBalances;
	}

	public Object getStatementDescriptor(){
		return statementDescriptor;
	}

	public Schedule getSchedule(){
		return schedule;
	}

	@Override
 	public String toString(){
		return 
			"Payouts{" + 
			"debit_negative_balances = '" + debitNegativeBalances + '\'' + 
			",statement_descriptor = '" + statementDescriptor + '\'' + 
			",schedule = '" + schedule + '\'' + 
			"}";
		}
}