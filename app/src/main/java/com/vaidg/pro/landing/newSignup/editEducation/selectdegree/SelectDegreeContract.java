package com.vaidg.pro.landing.newSignup.editEducation.selectdegree;

import android.content.Intent;

import com.vaidg.pro.BaseView;

public interface SelectDegreeContract {

    interface View extends BaseView {

        /**
         * <p>This method is used to initialize views</p>
         */
        void initViews();

        /**
         * <p>This method is initialize adapter and bind data in recyclerview.</p>
         */
        void initRv();

        /**
         * <p>This method is update notify city list adapter.</p>
         */
        void notifyAdapter();

        /**
         * <p>This method is display passed error message in snack bar.</p>
         */
        void showError();

        /**
         * <p>This method is display passed error message in snack bar.</p>
         * @param error the error message
         */
        void showError(String error);

        /**
         * <p>This method is intent to specified activity and finish current activity</p>
         * @param intent the object of intent with destination activity
         */
        void finishActivity(Intent intent);

    }

    interface Presenter {

        void getDegree();

        /**
         * on selecting city
         *
         * @param position
         */
        void onItemSelected(int position);
    }
}
