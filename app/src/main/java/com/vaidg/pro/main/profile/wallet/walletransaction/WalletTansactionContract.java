package com.vaidg.pro.main.profile.wallet.walletransaction;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.profile.wallet.CardData;
import com.vaidg.pro.pojo.profile.wallet.WalletData;
import com.vaidg.pro.pojo.profile.wallet.WalletTransData;
import java.util.ArrayList;
import okhttp3.RequestBody;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface WalletTansactionContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onFailure(String failureMsg);

    void onFailure();

    void onSuccess(WalletTransData walletTransData);

    void onNewToken(String newToken);

    void sessionExpired(String msg);
  }

  interface Presenter extends BasePresenter {

    void getTransaction(final int index);

  }
}