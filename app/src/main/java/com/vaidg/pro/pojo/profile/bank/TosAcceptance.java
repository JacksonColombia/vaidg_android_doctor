package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TosAcceptance implements Serializable {

	@SerializedName("date")
	@Expose
	private int date;

	@SerializedName("ip")
	@Expose
	private String ip;

	@SerializedName("user_agent")
	@Expose
	private Object userAgent;

	public int getDate(){
		return date;
	}

	public String getIp(){
		return ip;
	}

	public Object getUserAgent(){
		return userAgent;
	}

	@Override
 	public String toString(){
		return 
			"TosAcceptance{" + 
			"date = '" + date + '\'' + 
			",ip = '" + ip + '\'' + 
			",user_agent = '" + userAgent + '\'' + 
			"}";
		}
}