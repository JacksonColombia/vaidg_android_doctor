package com.vaidg.pro.landing.newSignup.SpecializationCatagory.slider;

import javax.inject.Inject;

/**
 * <h1>@SliderPresenter</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class SliderPresenter implements SliderContract.Presenter {

    private SliderContract.View view;

    @Inject
    public SliderPresenter() {
    }

    @Override
    public void attachView(SliderContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
