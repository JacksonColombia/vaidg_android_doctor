package com.vaidg.pro.landing.introsilider2.model;

import com.vaidg.pro.BaseModel;
import com.vaidg.pro.pojo.introslider.SliderData;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * <h1>IntroSliderModel</h1>
 * <p>This model is contain objects that's all used at IntroSliderActivity through IntroSliderPresenter
 *
 * @author 3embed
 * @version 1.0.20
 * @see com.vaidg.pro.landing.introsilider2.IntroSliderPresenter
 * @since 06/06/2020
 **/
public class IntroSliderModel extends BaseModel {

    @Inject
    ArrayList<SliderData> sliderList;

    @Inject
    public IntroSliderModel() {
    }

    /**
     * It's used to add SliderData object into the sliderList
     *
     * @param sliderData object of SliderData
     */
    public void addSliderToAdapter(SliderData sliderData) {
        sliderList.add(sliderData);
    }

    /**
     * It's used to get SliderData object from the sliderList at particular position
     *
     * @param position int position of list
     * @return SliderData object
     */
    public SliderData getSliderData(int position) {
        return (sliderList.size() >= position) ? sliderList.get(position) : null;
    }
}
