package com.vaidg.pro.main;

import android.animation.Animator;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.google.android.gms.common.api.Status;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.main.acceptedbooking.AcceptedBookingFragment;
import com.vaidg.pro.main.booking.BookingFragment;
import com.vaidg.pro.main.chats.ChatCustomerFragment;
import com.vaidg.pro.main.profile.ProfileFragment;
import com.vaidg.pro.main.schedule.ScheduleFragment;
import com.vaidg.pro.pojo.appconfig.AppConfigData;
import com.vaidg.pro.pojo.appconfig.AppConfigMainPojo;
import com.vaidg.pro.service.LocationPublishService;
import com.vaidg.pro.service.OfflineLocationPublishService;
import com.vaidg.pro.utility.CustomTypefaceSpan;
import com.vaidg.pro.utility.DialogHelper;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import dagger.android.support.DaggerAppCompatActivity;
import java.lang.reflect.Field;
import javax.inject.Inject;

import static com.vaidg.pro.utility.VariableConstant.RAZORPAY_KEY;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>MainActivity</h1>
 * MainActivity that contain bottom Navigation view
 */
public class MainActivity extends DaggerAppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, MainActivityContract.View, BookingFragment.BookingFragmentInteraction {
  @Inject
  MainActivityContract.Presenter presenter;
  private SessionManager sessionManager;
  private FragmentManager fragmentManager;
  private Fragment fragment;
  private IntentFilter filter;
  private FrameLayout flContainer;
  private BottomNavigationView bottomNavigationView;
  private long backPressed;
  private NotificationManager notificationManager;
  private Animation fade_open;
  private boolean isResume = false;
  private AlertDialog dialogGps;
  private Status status;
  private BroadcastReceiver receiver;
  private int fragmentSelectedPosition = 0;
  private int fragmentOldPosition = -1;
  private BottomNavigationMenuView menuView;
  private Intent mainIntent;

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    Utility.checkAndShowNetworkError(this);
    if (hasFocus) {
      if (!Utility.isGpsEnabled(this)) {
        if (status != null) {
          showGpsAlert();
        }
             /*   else if(!dialogGps.isShowing())
                {
                    dialogGps.show();
                }*/
      } else if (dialogGps != null && dialogGps.isShowing()) {
        dialogGps.dismiss();
      }
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    //set Activity is Foreground for TeleCall services
   // startService(new Intent(this, OnClearFromRecentService.class));
    mainIntent = getIntent();
    sessionManager = SessionManager.getSessionManager(this);
    Log.d("init: ", sessionManager.getLastBooking());
    fragmentManager = getSupportFragmentManager();
    notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    fade_open = AnimationUtils.loadAnimation(this, R.anim.fade_open);
    Typeface fontBold = Utility.getFontBold(this);
    flContainer = findViewById(R.id.flContainer);
    bottomNavigationView = findViewById(R.id.navigation);
    bottomNavigationView.setOnNavigationItemSelectedListener(this);
    removeShiftModeAndApplyFont(bottomNavigationView, fontBold);
    presenter.getAppConfig();
   // presenter.getBookingByDate(String.valueOf((System.currentTimeMillis() / 1000)), String.valueOf((System.currentTimeMillis() / 1000)), false);
    if (sessionManager.getIsNewBooking()) {
      sessionManager.setIsNewBooking(false);
      Window window = getWindow();
      window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
      window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
      window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
      window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
      if (sessionManager.getIsNowBooking()) {
        AppController.getInstance().getMixpanelHelper().timeStartForNowBookingRespond();
      } else {
        AppController.getInstance().getMixpanelHelper().timeStartForLaterBookingRespond();
      }
      setBookingFragment();

          /*  if (sessionManager.getIsAssignedBooking()) {
                sessionManager.setIsAssignedBooking(false);
                setAcceptedBookingFragment();
            } else {
                setBookingFragment();
            }*/



           /* if(mainIntent!=null&& mainIntent.hasExtra("callType"))
            {
                String callType = mainIntent.getStringExtra("callType");
                if (callType.equals("3") || callType.equals("1")) {
                    sessionManager.setIsNewBooking(true);
                    setAcceptedBookingFragment();
                }
                else{
                    setBookingFragment();
                }

            }
            else{
                setBookingFragment();
            }*/
      // AppController.getInstance().startNewBookingRingtoneService();
    } else {
      if (sessionManager.getLoadProfileFrag()) {
        refreshBookingFragment(5);
        sessionManager.setLoadProfileFrag(false);
      } else {
        setBookingFragment();
      }
    }
    isResume = true;
    VariableConstant.IS_APPLICATION_RUNNING = true;
    dialogGps = DialogHelper.adEnableGPS(this);
    filter = new IntentFilter();
    filter.addAction(VariableConstant.INTENT_ACTION_GPS_OFF);
    receiver = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
        try {
          if (intent.getAction().equals(VariableConstant.INTENT_ACTION_GPS_OFF)) {
            status = intent.getParcelableExtra("gps");
            showGpsAlert();
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    };
    registerReceiver(receiver, filter);
    AppController.getInstance().getMixpanelHelper().timeForAppOpened();
    boolean showDialog = getIntent().getBooleanExtra("showDialog", true);
    if (showDialog) {
      sessionManager.setAppOpenTime(sessionManager.getAppOpenTime() + 1);
      if (sessionManager.getAppOpenTime() % 5 == 0 && !sessionManager.getDontShowRate()) {
        DialogHelper.rateApp(this, sessionManager);
      }
    }
  }

  private void showGpsAlert() {
    try {
      status.startResolutionForResult(MainActivity.this, VariableConstant.REQUEST_CODE_GPS);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /*positio=>1-5*/
  public void refreshBookingFragment(int position) {

      /*  fragmentOldPosition = fragmentSelectedPosition;
        fragmentSelectedPosition=position;
        switch (position) {
            case 1:
                fragment = BookingFragment.newInstance();
                break;

            case 2:
                fragment = AcceptedBookingFragment.newInstance();
                break;

            case 3:
                fragment = ChatCustomerFragment.newInstance();
                break;

            case 4:
                fragment = ScheduleFragment.newInstance();
                break;

            case 5:
                fragment = ProfileFragment.newInstance();
                break;
        }

        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.flContainer, fragment).commit();
        }

        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                Animator anim = ViewAnimationUtils.createCircularReveal(flContainer, flContainer.getWidth(), flContainer.getHeight(), 0, flContainer.getHeight());
                anim.setDuration(400);
                anim.start();
            } else {
                flContainer.startAnimation(fade_open);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
*/
    Menu m = bottomNavigationView.getMenu();
    MenuItem mi = m.getItem(position - 1);
    onNavigationItemSelected(mi);
    bottomNavigationView.setSelectedItemId(mi.getItemId());
  }

  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    fragmentOldPosition = fragmentSelectedPosition;
    switch (item.getItemId()) {
      case R.id.navigation_lead:
        fragment = BookingFragment.newInstance();
        fragmentSelectedPosition = 1;
        break;
      case R.id.navigation_booking:
        fragment = AcceptedBookingFragment.newInstance();
        fragmentSelectedPosition = 2;
        break;
      case R.id.navigation_chat:
        fragment = ChatCustomerFragment.newInstance();
        fragmentSelectedPosition = 3;
        break;
      case R.id.navigation_schedule:
        fragment = ScheduleFragment.newInstance();
        fragmentSelectedPosition = 4;
        break;
      case R.id.navigation_profile:
        fragment = ProfileFragment.newInstance();
        fragmentSelectedPosition = 5;
        break;
    }
    if (fragmentSelectedPosition == fragmentOldPosition) {
      return true;
    }
    if (fragment != null) {
      fragmentManager.beginTransaction().replace(R.id.flContainer, fragment).commitAllowingStateLoss();
    }
    try {
      if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
        Animator anim = ViewAnimationUtils.createCircularReveal(flContainer, flContainer.getWidth(), flContainer.getHeight(), 0, flContainer.getHeight());
        anim.setDuration(400);
        anim.start();
      } else {
        flContainer.startAnimation(fade_open);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return true;
  }

  @Override
  protected void onResume() {
    super.onResume();
    VariableConstant.isAppOpened = true;
    //notificationManager.cancelAll();
  }

  /**
   * customise the bottom navigation view for showing icons and texts
   *
   * @param bottomNavigationView MainActivity's bottomNavigationView
   * @param font                 typeface for bottom navigation texts
   */
  void removeShiftModeAndApplyFont(BottomNavigationView bottomNavigationView, Typeface font) {
    menuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
  /*  menuView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
    menuView.buildMenuView();*/
    try {
      Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
      shiftingMode.setAccessible(true);
      shiftingMode.setBoolean(menuView, false);
      shiftingMode.setAccessible(false);
      if (menuView != null) {
        for (int i = 0; i < menuView.getChildCount(); i++) {
          BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
          TextView smallText = item.findViewById(R.id.smallLabel);
          smallText.setVisibility(View.GONE);
          ImageView icon = item.findViewById(R.id.icon);
          FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) icon.getLayoutParams();
          params.gravity = Gravity.CENTER;
          item.setShifting(false);
          item.setPadding(0, 0, 0, 0);
          item.setChecked(item.getItemData().isChecked());
        }
        Menu m = bottomNavigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
          MenuItem mi = m.getItem(i);
          SpannableString mNewTitle = new SpannableString(mi.getTitle());
          mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
          mi.setTitle(mNewTitle);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onBackPressed() {
    if (fragment instanceof ScheduleFragment) {
      if (((ScheduleFragment) fragment).isFabOpen) {
        ((ScheduleFragment) fragment).animateFAB();
        return;
      }
    }
    if (backPressed + 2000 > System.currentTimeMillis()) {
      super.onBackPressed();
      overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    } else {
      Toast.makeText(getBaseContext(), getResources().getString(R.string.doublePressExit), Toast.LENGTH_SHORT).show();
    }
    backPressed = System.currentTimeMillis();
  }

  @Override
  public void onSuccess(AppConfigData appConfigData, String pojo) {
    try {
      AppConfigMainPojo appConfigMainPojo = new Gson().fromJson(pojo, AppConfigMainPojo.class);
      sessionManager.setLocationPublishInterval(appConfigData.getProviderFrequency().getLocationPublishInterval());
      sessionManager.setLiveTrackInterval(appConfigData.getProviderFrequency().getLiveTrackInterval());
      sessionManager.setProTimeOut(appConfigData.getProviderFrequency().getProTimeOut());
      sessionManager.setLatLongDisplacement(appConfigData.getLatLongDisplacement());
      sessionManager.setStripeKey(appConfigData.getStripeKeys());
      sessionManager.setCurrency(appConfigData.getCurrency());
      sessionManager.setCurrencySymbol(appConfigData.getCurrencySymbol());
      sessionManager.setCurrencyAbbrevation(appConfigData.getCurrencyAbbr());
      sessionManager.setDistanceUnit(appConfigData.getDistanceMatrix());
      RAZORPAY_KEY =  (appConfigData.getPaymentGateways() != null && appConfigData.getPaymentGateways().size() > 0 && appConfigData.getPaymentGateways().get(0).getName().equals("razorpay")) ? appConfigData.getRazorPayKey() : "";
      if (appConfigData.getWalletData() != null) {
        sessionManager.setWalletEnable(appConfigData.getWalletData().getEnableWallet());
      }
      boolean showDialog = getIntent().getBooleanExtra("showDialog", true);
      if (Utility.isLatestVersion(appConfigData.getAppVersion()) && isResume && showDialog) {
        if (appConfigData.getMandatory().equals("true")) {
          //show Mandatory
          DialogHelper.appUpdateMandatory(this);
        } else {
          //show nonMandatory
          DialogHelper.appUpdateNonMandatory(this);
        }
      }
      if (appConfigData.getPushTopics().getCity() != null && !appConfigData.getPushTopics().getCity().equals(""))
        FirebaseMessaging.getInstance().subscribeToTopic(appConfigData.getPushTopics().getCity());
      if (appConfigData.getPushTopics().getAllCities() != null && !appConfigData.getPushTopics().getAllCities().equals(""))
        FirebaseMessaging.getInstance().subscribeToTopic(appConfigData.getPushTopics().getAllCities());
      if (appConfigData.getPushTopics().getAllProvider() != null && !appConfigData.getPushTopics().getAllProvider().equals(""))
        FirebaseMessaging.getInstance().subscribeToTopic(appConfigData.getPushTopics().getAllProvider());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void sessionExpired(String msg) {
    if(msg != null && !msg.isEmpty()) {
      Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
      Utility.logoutSessionExiperd(sessionManager, this);
    }
  }

  @Override
  public void onFailure(String msg) {
    if(msg != null && !msg.isEmpty())
      Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onFailure() {
    //Toast.makeText(getActivity(),getActivity().getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    AppController.toast();
  }

  @Override
  public void onNewToken(String token) {
    AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
        sessionManager.getPassword(), token);
  }

  @Override
  public void onSuccesBooking(String result, boolean b) {
    Utility.printLog("onsuccessbookingmain", result);

        /*if (result != null) {
            AcceptedBookingPojo bookingPojo = new Gson().fromJson(result, AcceptedBookingPojo.class);
            ArrayList<Booking> bookingArray = bookingPojo.getData();

            for (Booking booking : bookingArray) {
                if (booking.getCallType().equals("3")) {
                    if ((System.currentTimeMillis()) < (Long.parseLong(booking.getBookingEndtime()) - (15 * 60000))) {
                        //set alarm for this booking
                        boolean alarmUp = (PendingIntent.getBroadcast(this, ALARM_FOR_END_BOOKING,
                                new Intent("com.vaidg.pro.alarmForTelecalls"),
                                PendingIntent.FLAG_NO_CREATE) != null);
                        if (alarmUp) {
                            Utility.printLog("myTag", "Alarm is already active");
                        } else {
                            Utility.printLog("myTag", "Alarm not active ,set now");
                                Utility.setAlarm(this,booking.getBookingEndtime());
                        }
                        break;
                    }
                }
            }
        }*/
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (Utility.isMyServiceRunning(this, OfflineLocationPublishService.class)) {
      Intent stopIntent = new Intent(this, OfflineLocationPublishService.class);
      stopIntent.setAction(VariableConstant.ACTION.STOPOFFLINELOCATIONSERVICE);
    /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        ContextCompat.startForegroundService(this,stopIntent);
      } else {
        startService(stopIntent);
      }*/
      startService(stopIntent);

    }
    unregisterReceiver(receiver);
    AppController.getInstance().getMixpanelHelper().timeForAppClosed();
    isResume = false;
    VariableConstant.IS_APPLICATION_RUNNING = false;
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == VariableConstant.REQUEST_CODE_GPS) {
      if (resultCode == RESULT_OK) {
        Intent startIntent = new Intent(this, LocationPublishService.class);
        startIntent.setAction(VariableConstant.ACTION.STARTFOREGROUND_ACTION);
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
          ContextCompat.startForegroundService(this,startIntent);
        } else {
          startService(startIntent);
        } */
        startService(startIntent);
        if (dialogGps.isShowing()) {
          dialogGps.dismiss();
        }
      } else if (resultCode == RESULT_CANCELED) {
        if (!dialogGps.isShowing()) {
          dialogGps.show();
        }
      }
    }
  }

  @Override
  public void setAcceptedBookingFragment() {
    bottomNavigationView.setSelectedItemId(R.id.navigation_booking);
  }

  public void setBookingFragment() {
    fragment = BookingFragment.newInstance();
    fragmentManager.beginTransaction().replace(R.id.flContainer, fragment).commit();
  }

  @Override
  public void showProgress() {
  }

  @Override
  public void hideProgress() {
  }

    /*public void OpenChatingActivity(JSONObject jsonObject) {
        Intent chatIntent = new Intent(MainActivity.this, ChattingActivity.class);
        try {
            if (jsonObject != null) {
                if (jsonObject.has("isPastChat")) {
                    chatIntent.putExtra("isPastChat", jsonObject.getBoolean("isPastChat"));
                }
                if (jsonObject.has("isActiveChat")) {
                    chatIntent.putExtra("isActiveChat", jsonObject.getBoolean("isActiveChat"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        startActivity(chatIntent);
    }*/
}
