package com.vaidg.pro.main.profile.bank.bankdetails;


import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.BAD_REQUEST;
import static com.vaidg.pro.utility.VariableConstant.NOT_FOUND;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.RAZOR_NOT_FOUND;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Context;
import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.profile.bank.AccountRazorPayResponse;
import com.vaidg.pro.pojo.profile.bank.BankList;
import com.vaidg.pro.pojo.profile.bank.LegalEntity;
import com.vaidg.pro.pojo.profile.bank.RazorPayData;
import com.vaidg.pro.pojo.profile.bank.RazorPayResponse;
import com.vaidg.pro.pojo.profile.bank.StripeDetailsPojo;
import com.vaidg.pro.pojo.profile.bank.StripeResponse;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import eu.janmuller.android.simplecropimage.Util;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>BankDetailsPresenter</h1>
 * BankDetailsPresenter presenter for BankDetailsActivity
 *
 * @see BankDetailsActivity
 */

public class BankDetailsPresenter implements BankDetailsContract.Presenter {

    @Inject
    Context context;
    @Inject
    BankDetailsContract.View view;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;
    private String imageUrl = "";

    @Inject
    BankDetailsPresenter() {
    }

    @Override
    public void getBankDetails() {
        if (view != null)
        view.startProgressBar();
      //  bankListFragModel.fetchData(token);
        service.getConnectAccount(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if (view != null)
                                    view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                        StripeResponse stripeDetailsPojo = new Gson().fromJson(response, StripeResponse.class);
                                        if (view != null)
                                            view.onSuccess(stripeDetailsPojo.getData());
                                } else {
                                    if (view != null)
                                        view.onFailure(Utility.getMessage(response));
                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        getBankDetails();
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        if (view != null) {
                                            view.stopProgressBar();
                                            view.onFailure();
                                        }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().toast(Utility.getMessage(msg));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                    }
                                });
                                break;
                            case BAD_REQUEST:
                            case NOT_FOUND:
                                if (view != null) {
                                    view.stopProgressBar();
                                    view.showAddStipe(Utility.getMessage(errorBody));
                                }
                                getInstance().toast(Utility.getMessage(errorBody));
                                break;
                            case SESSION_LOGOUT:
                                if (view != null)
                                    view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                Utility.logoutSessionExiperd(sessionManager, context);
                                break;
                            default:
                                if (view != null)
                                {           view.stopProgressBar();
                                 view.onFailure();
                        } getInstance().toast(Utility.getMessage(errorBody));

                                break;
                        }
                    } catch (Exception e) {
                        if (view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if (view != null) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                }

                @Override
                public void onComplete() {
                }
            });
    }

    @Override
    public void fetchContactAndCache() {
        if (view != null)
        view.startProgressBar();
      //  bankListFragModel.fetchData(token);
        service.getContact(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if (view != null)
                                    view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    RazorPayResponse razorPayData = new Gson().fromJson(response, RazorPayResponse.class);
                                        if (view != null)
                                            view.onRazorPaySuccess(razorPayData.getData());
                                } else {
                                    if (view != null)
                                        view.onFailure(Utility.getMessage(response));
                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        fetchContactAndCache();
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        if (view != null) {
                                            view.stopProgressBar();
                                            view.onFailure();
                                        }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().toast(Utility.getMessage(msg));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                    }
                                });
                                break;
                            case BAD_REQUEST:
                            case NOT_FOUND:
                            case RAZOR_NOT_FOUND:
                                if (view != null) {
                                    view.stopProgressBar();
                                    view.showAddRazorPay(Utility.getMessage(errorBody),true);
                                }
                            //    getInstance().toast(Utility.getMessage(errorBody));
                                break;
                            case SESSION_LOGOUT:
                                if (view != null)
                                    view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                Utility.logoutSessionExiperd(sessionManager, context);
                                break;
                            default:
                                if (view != null)
                                {           view.stopProgressBar();
                                 view.onFailure();
                        } getInstance().toast(Utility.getMessage(errorBody));

                                break;
                        }
                    } catch (Exception e) {
                        if (view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if (view != null) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                }

                @Override
                public void onComplete() {
                }
            });
    }

    @Override
    public void fetchFundAccountAndCache() {
        if (view != null)
        view.startProgressBar();
      //  bankListFragModel.fetchData(token);
        service.getFundAccount(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {
                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                    int statusCode = responseBodyResponse.code();
                    try {
                        String response = responseBodyResponse.body() != null
                            ? responseBodyResponse.body().string() : null;
                        String errorBody = responseBodyResponse.errorBody() != null
                            ? responseBodyResponse.errorBody().string() : null;
                        switch (statusCode) {
                            case SUCCESS_RESPONSE:
                                if (view != null)
                                    view.stopProgressBar();
                                if (response != null && !response.isEmpty()) {
                                    AccountRazorPayResponse accountRazorPayResponse = new Gson().fromJson(response, AccountRazorPayResponse.class);
                                        if (view != null)
                                            view.onRazorPayList(accountRazorPayResponse.getData());
                                } else {
                                    if (view != null)
                                        view.onFailure(Utility.getMessage(response));
                                }
                                break;
                            case SESSION_EXPIRED:
                                onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        fetchFundAccountAndCache();
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                        if (view != null) {
                                            view.stopProgressBar();
                                            view.onFailure();
                                        }
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if (view != null)
                                            view.stopProgressBar();
                                        getInstance().toast(Utility.getMessage(msg));
                                        Utility.logoutSessionExiperd(sessionManager, context);
                                    }
                                });
                                break;
                            case BAD_REQUEST:
                            case NOT_FOUND:
                            case RAZOR_NOT_FOUND:
                                if (view != null) {
                                    view.stopProgressBar();
                                  //  view.showAddRazorPayList(Utility.getMessage(errorBody),true);
                                }
                            //    getInstance().toast(Utility.getMessage(errorBody));
                                break;
                            case SESSION_LOGOUT:
                                if (view != null)
                                    view.stopProgressBar();
                                getInstance().toast(Utility.getMessage(errorBody));
                                Utility.logoutSessionExiperd(sessionManager, context);
                                break;
                            default:
                                if (view != null)
                                {           view.stopProgressBar();
                                 view.onFailure();
                        } getInstance().toast(Utility.getMessage(errorBody));

                                break;
                        }
                    } catch (Exception e) {
                        if (view != null) {
                            view.stopProgressBar();
                            view.onFailure();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    if (view != null) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                }

                @Override
                public void onComplete() {
                }
            });
    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    /**
     * BankDetailsPresenterImplement interface for View implementation
     */
    interface BankDetailsPresenterImplement {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String msg);

        void onFailure();

        void onSuccess(LegalEntity legalEntity, ArrayList<BankList> bankLists);

        void showAddStipe(String msg);
    }
}
