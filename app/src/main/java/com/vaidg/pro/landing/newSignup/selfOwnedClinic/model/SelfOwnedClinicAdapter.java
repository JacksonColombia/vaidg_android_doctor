package com.vaidg.pro.landing.newSignup.selfOwnedClinic.model;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.vaidg.pro.databinding.SelfOwnedClinicItemBinding;

import java.io.File;
import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

public class SelfOwnedClinicAdapter extends RecyclerView.Adapter<SelfOwnedClinicAdapter.SelfOwnedClinicViewHolder> {

    private ArrayList<String> arrayList;
    private SelfOwnedClinicItemBinding binding;

    public SelfOwnedClinicAdapter(ArrayList<String> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public SelfOwnedClinicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = SelfOwnedClinicItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new SelfOwnedClinicViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SelfOwnedClinicViewHolder holder, int position) {
        holder.sdvSelfOwnedClinic.setImageURI(arrayList.get(position).startsWith("http") ? Uri.parse(arrayList.get(position)) : Uri.fromFile(new File(arrayList.get(position))));
    }

    @Override
    public int getItemCount() {
        return arrayList != null ? arrayList.size() : ZERO;
    }

    class SelfOwnedClinicViewHolder extends RecyclerView.ViewHolder {

        SimpleDraweeView sdvSelfOwnedClinic;

        SelfOwnedClinicViewHolder(@NonNull SelfOwnedClinicItemBinding itemView) {
            super(itemView.getRoot());
            sdvSelfOwnedClinic = binding.sdvSelfOwnedClinic;
        }
    }

    public void addAll(ArrayList<String> data) {
        arrayList.clear();
        arrayList.addAll(data);
    }
}
