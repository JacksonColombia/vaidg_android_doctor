package com.vaidg.pro.landing.newSignup.editEducation.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class EducationDataModel implements Parcelable {


    public static final Creator<EducationDataModel> CREATOR = new Creator<EducationDataModel>() {
        @Override
        public EducationDataModel createFromParcel(Parcel in) {
            return new EducationDataModel(in);
        }

        @Override
        public EducationDataModel[] newArray(int size) {
            return new EducationDataModel[size];
        }
    };
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("college")
    @Expose
    private String college;

    protected EducationDataModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        year = in.readString();
        college = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(year);
        dest.writeString(college);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }
}
