package com.vaidg.pro.adapters;

import static com.vaidg.pro.utility.Utility.getFontMedium;
import static com.vaidg.pro.utility.Utility.pixelsToSp;
import static com.vaidg.pro.utility.VariableConstant.ZERO;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.pro.R;
import com.vaidg.pro.pojo.addMedication.Unit;

import java.util.ArrayList;

public class CategoryDropdownAdapter extends RecyclerView.Adapter<CategoryDropdownAdapter.CategoryViewHolder> {

  private ArrayList<Unit> units = new ArrayList<>();
  private Context mContext;
  private CategorySelectedListener categorySelectedListener;

  public CategoryDropdownAdapter(ArrayList<Unit> units){
    super();
    this.units = units;
  }


  public void setCategorySelectedListener(CategoryDropdownAdapter.CategorySelectedListener categorySelectedListener) {
    this.categorySelectedListener = categorySelectedListener;
  }

  @NonNull
  @Override
  public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View  view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
    mContext = parent.getContext();
    return new CategoryViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull CategoryViewHolder holder, final int position) {
    final Unit unit = units.get(position);
    holder.tvCategory.setText(unit.getName());

    holder.itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(categorySelectedListener != null){
          categorySelectedListener.onCategorySelected(position, unit);
        }
      }
    });
  }

  @Override
  public int getItemCount() {
    return units != null ? units.size() : ZERO;
  }

   class CategoryViewHolder extends RecyclerView.ViewHolder{
    AppCompatTextView tvCategory;
    AppCompatImageView ivIcon;
    Typeface fontMedium;

    public CategoryViewHolder(View itemView) {
      super(itemView);
      tvCategory = itemView.findViewById(R.id.tvCategory);
      ivIcon = itemView.findViewById(R.id.ivIcon);
      fontMedium = getFontMedium(mContext);
      tvCategory.setTypeface(fontMedium);
      tvCategory.setTextSize(pixelsToSp(mContext.getResources().getDimension(R.dimen.sp_14), mContext));

    }
  }

  public interface CategorySelectedListener {
    void onCategorySelected(int position, Unit category);
  }
}