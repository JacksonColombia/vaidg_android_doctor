package com.vaidg.pro.landing.newSignup.editEducation.selectdegree;

import android.app.Activity;
import android.content.Intent;

import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.landing.newSignup.editEducation.selectdegree.model.SelectDegreeModel;
import com.vaidg.pro.landing.newSignup.editEducation.selectdegree.model.SelectionCallBack;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationPojo;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.signup.DegreeData;
import com.vaidg.pro.pojo.signup.DegreePojo;
import com.vaidg.pro.utility.Utility;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

public class SelectDegreePresenter implements SelectDegreeContract.Presenter, SelectionCallBack {

    @Inject
    SelectDegreeContract.View view;

    @Inject
    SelectDegreeModel model;

    @Inject
    NetworkService service;

    @Inject
    NetworkStateHolder networkStateHolder;

    @Inject
    Activity activity;

    @Inject
    Gson gson;

    @Inject
    public SelectDegreePresenter() {
    }

    @Override
    public void onItemSelected(int position) {
        DegreeData degreeData = model.getSelectedDegree(position);
        Intent intent = new Intent();
        intent.putExtra("degree", degreeData);
        view.finishActivity(intent);
    }

    @Override
    public void getDegree() {
        if (networkStateHolder.isConnected()) {
            if (view != null)
                view.showProgress();
            Utility.basicAuth(service).getDegree(model.getLanguage(), model.getPlatform())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            int statusCode = value.code();
                            try {
                                String response = value.body() != null
                                        ? value.body().string() : null;
                                String errorBody = value.errorBody() != null
                                        ? value.errorBody().string() : null;
                                if (statusCode == SUCCESS_RESPONSE) {
                                    DegreePojo degreePojo = gson.fromJson(response, DegreePojo.class);
                                    model.parseDegreeData(degreePojo);
                                    if (view != null) {
                                        view.hideProgress();
                                        view.notifyAdapter();
                                    }
                                } else {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.showError(Utility.getMessage(errorBody));
                                    }
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null)
                                view.hideProgress();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }
}
