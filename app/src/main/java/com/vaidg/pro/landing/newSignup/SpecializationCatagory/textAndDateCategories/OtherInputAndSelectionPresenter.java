package com.vaidg.pro.landing.newSignup.SpecializationCatagory.textAndDateCategories;

import android.app.Activity;
import android.text.TextUtils;

import com.vaidg.pro.R;
import com.vaidg.pro.utility.Utility;

import javax.inject.Inject;

/**
 * <h1>@OtherInputAndSelectionPresenter</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class OtherInputAndSelectionPresenter implements OtherInputAndSelectionContract.Presenter {

    @Inject
    Activity activity;
    private OtherInputAndSelectionContract.View view;


    @Inject
    public OtherInputAndSelectionPresenter() {
    }

    @Override
    public void valid(String enteredData) {
        if (!Utility.isTextEmpty(enteredData) || !view.isMandatoryForProvider())
            view.moveNextFrag();
        else
            view.showError(activity.getString(R.string.text_input_error));

    }

    @Override
    public void attachView(OtherInputAndSelectionContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view= null;
    }
}
