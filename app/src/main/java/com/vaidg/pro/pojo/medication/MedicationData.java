package com.vaidg.pro.pojo.medication;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedicationData implements Serializable
{

  @SerializedName("compoundName")
  @Expose
  public String compoundName;
  @SerializedName("drugStrength")
  @Expose
  public String drugStrength;
  @SerializedName("drugUnit")
  @Expose
  public String drugUnit;
  @SerializedName("frequency")
  @Expose
  public String frequency;
  @SerializedName("drugRoute")
  @Expose
  public String drugRoute;
  @SerializedName("direction")
  @Expose
  public String direction;
  @SerializedName("duration")
  @Expose
  public String duration;
  @SerializedName("instruction")
  @Expose
  public String instruction;
  @SerializedName("id")
  @Expose
  public String id;

  public String getCompoundName() {
    return compoundName;
  }

  public String getDrugStrength() {
    return drugStrength;
  }

  public String getDrugUnit() {
    return drugUnit;
  }

  public String getFrequency() {
    return frequency;
  }

  public String getDrugRoute() {
    return drugRoute;
  }

  public String getDirection() {
    return direction;
  }

  public String getDuration() {
    return duration;
  }

  public String getInstruction() {
    return instruction;
  }

  public String getId() {
    return id;
  }
}