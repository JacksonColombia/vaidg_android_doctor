package com.vaidg.pro.landing.newSignup;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>@SignUpDaggerModule</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
@Module
public abstract class SignUpDaggerModule {

    @ActivityScoped
    @Binds
    abstract Activity getActivity(SignUpActivity signUpActivity);

    @ActivityScoped
    @Binds
    abstract SignUpContract.View getView(SignUpActivity signUpActivity);

    @ActivityScoped
    @Binds
    abstract SignUpContract.Presenter getPresenter(SignUpPresenter signUpPresenter);

}
