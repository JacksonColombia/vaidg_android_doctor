package com.kotlintestgradle.data.preference

/**
 * @author 3Embed
 * for local dataBase
 * @since 1.0 (23-Aug-2019)
 */
interface PreferenceManager {
    var languageCode: String?
    var authToken: String?
    var isLoggedIn: Boolean
    var userId: String?
    var mqttTopic: String?
    var willTopic: String?
    var fcmTopic: String?
    fun clearPreference()
}
