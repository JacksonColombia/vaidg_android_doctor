package com.jio.consumer.domain.interactor.user.handler

/**
 * Created by 3Embed on 29/03/19.
 */
interface UserHandler {
    fun isLogged(): Boolean
    fun getUserId(): String?
    fun getMQTTTopic(): String
    fun getWillTopic(): String?
    fun getFcmTopic(): String
    fun setAuthToken(authToken:String)
    fun setLanguageCode(lang : String)
    fun setUserId(userId : String)
}