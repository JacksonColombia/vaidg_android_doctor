package com.vaidg.pro.main.schedule.bookingschedule;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.button.MaterialButton;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.CancelListAdapter;
import com.vaidg.pro.bookingflow.review.CustomerReviewsActivity;
import com.vaidg.pro.main.chats.ChattingActivity;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.pojo.booking.CancelPojo;
import com.vaidg.pro.pojo.history.Accounting;
import com.vaidg.pro.pojo.history.CustomerData;
import com.vaidg.pro.pojo.shedule.ScheduleBookngPojo;
import com.vaidg.pro.utility.CalendarEventHelper;
import com.vaidg.pro.utility.CircleTransform;
import com.vaidg.pro.utility.DialogHelper;
import com.vaidg.pro.utility.MixpanelEvents;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import org.json.JSONObject;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

import static com.vaidg.pro.utility.VariableConstant.FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_SPACE_DD_MMMM_YYYY;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_TIME_1_12_HRS;

public class BookingScheduleActivity extends DaggerAppCompatActivity implements View.OnClickListener, BookingScheculeContract.View, CancelListAdapter.CancelSelected {

    Typeface fontBold, fontMedium, fontRegular;
    @Inject
    BookingScheculeContract.Presenter presenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private ImageView ivCustomer;
    private RatingBar ratingStar;
    private TextView tvCustomerName;
    private TextView tvMessageCount;
    private TextView tvTitle, tvAddress, tvDistance, tvTotalBillAmount, tvDate, tvTime,
            tvTravelFee, tvVisitFee, tvLastDue, tvDiscount, tvTotal, tvPaymentMethod,
            tvBookingStatus, tvQuestionAnswerLabel;
    private MaterialButton tvCancel;
    private BroadcastReceiver receiver;
    private String bookingId = "", cancelId = "";
    private Booking bookingDataHistory;

    private LayoutInflater inflater;
    private LinearLayout llService;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_schedule);
        initView();
    }

    private void initView() {
        mContext = this;
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingDetails));
        progressDialog.setCancelable(false);

        fontBold = Utility.getFontBold(this);
        fontMedium = Utility.getFontMedium(this);
        fontRegular = Utility.getFontRegular(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        tvCancel = findViewById(R.id.btnDone);
        tvCancel.setText(getString(R.string.cancel));
        tvCancel.setVisibility(View.VISIBLE);
        tvCancel.setTypeface(fontMedium);
        Utility.setTextColor(this, tvCancel, R.color.colorPrimary);
        tvCancel.setOnClickListener(this);

        ivCustomer = findViewById(R.id.ivCustomer);
        ratingStar = findViewById(R.id.ratingStar);

        tvTitle = findViewById(R.id.tvTitle);
        TextView tvAddressLabel = findViewById(R.id.tvAddressLabel);
        tvAddress = findViewById(R.id.tvAddress);
        tvDistance = findViewById(R.id.tvDistance);
        tvBookingStatus = findViewById(R.id.tvBookingStatus);
        tvCustomerName = findViewById(R.id.tvCustomerName);
        tvMessageCount = findViewById(R.id.tvMessageCount);
        tvQuestionAnswerLabel = findViewById(R.id.tvQuestionAnswerLabel);

        llService = findViewById(R.id.llService);
        TextView tvTotalBillAmountLabel = findViewById(R.id.tvTotalBillAmountLabel);
        tvTotalBillAmount = findViewById(R.id.tvTotalBillAmount);
        tvDate = findViewById(R.id.tvDate);
        tvTime = findViewById(R.id.tvTime);
        TextView tvPaymentBreakDownLabel = findViewById(R.id.tvPaymentBreakDownLabel);
        TextView tvTravelFeeLabel = findViewById(R.id.tvTravelFeeLabel);
        tvTravelFee = findViewById(R.id.tvTravelFee);
        TextView tvVisitFeeLabel = findViewById(R.id.tvVisitFeeLabel);
        tvVisitFee = findViewById(R.id.tvVisitFee);
        TextView tvLastDueLabel = findViewById(R.id.tvLastDueLabel);
        tvLastDue = findViewById(R.id.tvLastDue);
        TextView tvDiscountLabel = findViewById(R.id.tvDiscountLabel);
        tvDiscount = findViewById(R.id.tvDiscount);
        TextView tvTotalLabel = findViewById(R.id.tvTotalLabel);
        tvTotal = findViewById(R.id.tvTotal);
        TextView tvPaymentMethodLabel = findViewById(R.id.tvPaymentMethodLabel);
        tvPaymentMethod = findViewById(R.id.tvPaymentMethod);
        inflater = getLayoutInflater();

        tvAddressLabel.setTypeface(fontMedium);
        tvAddress.setTypeface(fontRegular);
        tvDistance.setTypeface(fontRegular);
        tvCustomerName.setTypeface(fontMedium);
        tvMessageCount.setTypeface(fontMedium);

        tvTotalBillAmountLabel.setTypeface(fontMedium);
        tvTotalBillAmount.setTypeface(fontBold);
        tvDate.setTypeface(fontRegular);
        tvTime.setTypeface(fontRegular);
        tvPaymentBreakDownLabel.setTypeface(fontMedium);
        tvTravelFeeLabel.setTypeface(fontRegular);
        tvTravelFee.setTypeface(fontRegular);
        tvVisitFeeLabel.setTypeface(fontRegular);
        tvVisitFee.setTypeface(fontRegular);
        tvLastDueLabel.setTypeface(fontRegular);
        tvLastDue.setTypeface(fontRegular);
        tvDiscountLabel.setTypeface(fontRegular);
        tvDiscount.setTypeface(fontRegular);
        tvTotalLabel.setTypeface(fontRegular);
        tvTotal.setTypeface(fontRegular);
        tvPaymentMethodLabel.setTypeface(fontMedium);
        tvPaymentMethod.setTypeface(fontRegular);
        tvQuestionAnswerLabel.setTypeface(fontMedium);

        tvTitle.setTypeface(fontBold);
        tvAddress.setTypeface(fontMedium);
        tvDistance.setTypeface(fontRegular);
        tvCustomerName.setTypeface(fontMedium);
        tvMessageCount.setTypeface(fontMedium);
        tvBookingStatus.setTypeface(fontMedium);

        tvTitle.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        tvCancel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_14), this));
        tvAddressLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_18), this));
        tvPaymentBreakDownLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_18), this));
        tvTotalBillAmountLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_18), this));
        tvDiscountLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_18), this));
        tvPaymentMethodLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_18), this));
        tvPaymentMethod.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        tvDiscount.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        tvAddress.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        tvDate.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        tvTime.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        tvTotalBillAmount.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_35), this));

        ImageView ivCall = findViewById(R.id.ivCall);
        RelativeLayout rlMessage = findViewById(R.id.rlMessage);
        LinearLayout llCutomerDetails = findViewById(R.id.llCutomerDetails);

        ivCall.setOnClickListener(this);
        rlMessage.setOnClickListener(this);
        //llCutomerDetails.setOnClickListener(this);


        bookingId = getIntent().getStringExtra("bookingId");

        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_NEW_CHAT);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(VariableConstant.INTENT_ACTION_NEW_CHAT)) {
                    tvMessageCount.setText("" + sessionManager.getChatCount(bookingId));
                    tvMessageCount.setVisibility(View.GONE);
                } else {
                    String cancelId = intent.getStringExtra("cancelid");
                    String msg = intent.getStringExtra("msg");
                    String header = intent.getStringExtra("header");
                    if (cancelId.equals(bookingId)) {
                        VariableConstant.IS_BOOKING_UPDATED = true;
                        DialogHelper.customAlertDialogCloseActivity(BookingScheduleActivity.this, header, msg, getString(R.string.oK));
                    }
                }

            }
        };

        registerReceiver(receiver, filter);

        presenter.getBooking(bookingId);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Utility.checkAndShowNetworkError(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sessionManager.getChatCount(bookingId) != 0) {
            tvMessageCount.setVisibility(View.GONE);
            tvMessageCount.setText("" + sessionManager.getChatCount(bookingId));
        } else {
            tvMessageCount.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();
    }

    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSuccess(ScheduleBookngPojo scheduleBookngPojo) {
        this.bookingDataHistory = scheduleBookngPojo.getData();
        if (bookingDataHistory.getStatus().equals(VariableConstant.JOB_TIMER_INCOMPLETE)
                || bookingDataHistory.getStatus().equals(VariableConstant.JOB_COMPLETED_RAISE_INVOICE)) {
            tvCancel.setVisibility(View.GONE);
        }

        CustomerData customerData = bookingDataHistory.getCustomerData();
        Accounting accounting = bookingDataHistory.getAccounting();

        if (!customerData.getProfilePic().equals("")) {
            Glide.with(this).setDefaultRequestOptions(new RequestOptions().error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(this))
                    .placeholder(R.drawable.profile_default_image))
                    .load(customerData.getProfilePic())
                    .into(ivCustomer);
        }

        tvTitle.setText(getString(R.string.bookingID) + bookingId);
        tvCustomerName.setText(customerData.getFirstName() + " " + customerData.getLastName());
        tvAddress.setText(bookingDataHistory.getAddLine1() + " " + bookingDataHistory.getAddLine2());
        tvDistance.setText("" + Utility.getFormattedPrice(String.valueOf((Double.parseDouble(bookingDataHistory.getDistance()) * sessionManager.getDistanceUnitConverter()))) + " " +
                sessionManager.getDistanceUnit() + "  " + getString(R.string.away));

        tvTotalBillAmount.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvDiscount.setText(Utility.getPrice(accounting.getDiscount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvTotal.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

        if (accounting.getTravelFee() != null && !accounting.getTravelFee().equals("0") && !accounting.getTravelFee().equals("0.00")) {
            findViewById(R.id.rlTravelFee).setVisibility(View.VISIBLE);
            tvTravelFee.setText(Utility.getPrice(accounting.getTravelFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if (accounting.getVisitFee() != null && !accounting.getVisitFee().equals("0") && !accounting.getVisitFee().equals("0.00")) {
            findViewById(R.id.rlVisitFee).setVisibility(View.VISIBLE);
            tvVisitFee.setText(Utility.getPrice(accounting.getVisitFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if (accounting.getLastDues() != null && !accounting.getLastDues().equals("0") && !accounting.getLastDues().equals("0.00")) {
            findViewById(R.id.rlLastDue).setVisibility(View.VISIBLE);
            tvLastDue.setText(Utility.getPrice(accounting.getLastDues(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if (accounting.getPaymentMethod().equals("1")) {
            tvPaymentMethod.setText(getString(R.string.cash));
            tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.vector_cash, 0, 0, 0);
        } else {
          //  tvPaymentMethod.setText(getString(R.string.cardFirst16Digit) + "  " + accounting.getLast4());
            tvPaymentMethod.setText(getString(R.string.card));
         //   tvPaymentMethod.setText(accounting.getRazorPayPaymentType());
        }

        if (accounting.getPaidByWallet() != null && accounting.getPaidByWallet().equals("1")) {
            tvPaymentMethod.setText(tvPaymentMethod.getText().toString() + " + " + getString(R.string.wallet));
        }

        View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
        TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
        TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
        tvServiceFeeLabel.setTypeface(fontRegular);
        tvServiceFee.setTypeface(fontRegular);
        tvServiceFeeLabel.setText(getString(R.string.consultingfee));
        tvServiceFee.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        tvServiceFeeLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16), this));
        tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        llService.addView(serviceView);
      /*  if (bookingDataHistory.getServiceType().equals("1")) {
            for (ServiceItem serviceItem : bookingDataHistory.getCartData()) {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setText(serviceItem.getServiceName() + " X " + serviceItem.getQuntity());
                tvServiceFee.setText(Utility.getPrice(serviceItem.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            }
        } else if (!bookingDataHistory.getCallType().equals("1")) {
            View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
            tvServiceFeeLabel.setTypeface(fontRegular);
            tvServiceFee.setTypeface(fontRegular);
            tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
            tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            llService.addView(serviceView);
        } else {
            View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
            tvServiceFeeLabel.setTypeface(fontRegular);
            tvServiceFee.setTypeface(fontRegular);
            tvServiceFeeLabel.setText(getString(R.string.consultingfee));
            tvServiceFee.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16),this));
            tvServiceFeeLabel.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_16),this));
            tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            llService.addView(serviceView);
        }*/

/*
        if (bookingDataHistory.getQuestionAndAnswer() != null && bookingDataHistory.getQuestionAndAnswer().size() > 0) {
            LinearLayout llQuestionAnswer = findViewById(R.id.llQuestionAnswer);
            llQuestionAnswer.setVisibility(View.VISIBLE);
            tvQuestionAnswerLabel.setVisibility(View.VISIBLE);

            for (QuestionAnswer questionAnswer : bookingDataHistory.getQuestionAndAnswer()) {
                View serviceView = inflater.inflate(R.layout.single_row_question_answer, null);
                TextView tvQuestion = serviceView.findViewById(R.id.tvQuestion);
                TextView tvAnswer = serviceView.findViewById(R.id.tvAnswer);
                tvQuestion.setTypeface(fontMedium);
                tvAnswer.setTypeface(fontRegular);
                tvQuestion.setText("Q. " + questionAnswer.getName());

                if (!questionAnswer.getQuestionType().equals("10")) {
                    tvAnswer.setText(questionAnswer.getAnswer());
                } else {
                    tvAnswer.setVisibility(View.GONE);
                    RecyclerView rvJobPhotosQuestion = serviceView.findViewById(R.id.rvJobPhotosQuestion);
                    rvJobPhotosQuestion.setVisibility(View.VISIBLE);
                    rvJobPhotosQuestion.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                    ArrayList<String> imageUrls = new ArrayList<>();
                    imageUrls.addAll(Arrays.asList(questionAnswer.getAnswer().split(",")));
                    rvJobPhotosQuestion.setAdapter(new JobImageListAdapter(this, imageUrls));
                }
                llQuestionAnswer.addView(serviceView);
            }
        }
*/


        try {
            tvDate.setText(getString(R.string.date) + " : " + Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(bookingDataHistory.getBookingRequestedFor()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_DD_MMMM_YYYY));
            tvTime.setText(getString(R.string.time) + " : " + Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(bookingDataHistory.getBookingRequestedFor()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_TIME_1_12_HRS));
        } catch (Exception e) {
            e.printStackTrace();
        }
        tvBookingStatus.setText(bookingDataHistory.getStatusMsg());
        if (bookingDataHistory.getStatus().equals(VariableConstant.JOB_COMPLETED_RAISE_INVOICE) ||
                bookingDataHistory.getStatus().equals(VariableConstant.JOB_TIMER_INCOMPLETE)) {
            Utility.setTextColor(this, tvBookingStatus, R.color.customRed);
        } else {
            Utility.setTextColor(this, tvBookingStatus, R.color.customGreen);
        }


        if (bookingDataHistory.getPaymentMethod().equals("1")) {
            tvPaymentMethod.setText(getString(R.string.cash));
        } else {
            tvPaymentMethod.setText(getString(R.string.card));
        }


        bookingId = bookingDataHistory.getBookingId();

        try {

            tvDate.setText(Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(bookingDataHistory.getBookingRequestedFor()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_DD_MMMM_YYYY));
            tvTime.setText(Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(bookingDataHistory.getBookingRequestedFor()), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_TIME_1_12_HRS));

            ratingStar.setRating(Float.parseFloat(customerData.getAverageRating()));
        } catch (Exception e) {
            e.printStackTrace();
        }


        switch (bookingDataHistory.getStatus()) {
            case VariableConstant.RECEIVED:
            case VariableConstant.NEW_BOOKING:
            case VariableConstant.BOOKING_EXPIRED:
            case VariableConstant.CANCELLED_BY_PROVIDER:
            case VariableConstant.CANCELLED_BY_CUTOMER:
            case VariableConstant.ARRIVED:
            case VariableConstant.JOB_TIMER_STARTED:
            case VariableConstant.JOB_TIMER_COMPLETED:
            case VariableConstant.JOB_COMPLETED_RAISE_INVOICE:
                tvCancel.setVisibility(View.GONE);
                break;
        }
    }


    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        final View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_cancel, null);
        alertDialogBuilder.setView(view);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        TextView tvSubmit = view.findViewById(R.id.tvSubmit);
        ImageView ivClose = view.findViewById(R.id.ivClose);

        tvTitle.setTypeface(fontMedium);
        tvSubmit.setTypeface(fontBold);
        RecyclerView rvCancel = view.findViewById(R.id.rvCancel);
        rvCancel.setLayoutManager(new LinearLayoutManager(this));
        CancelListAdapter cancelListAdapter = new CancelListAdapter(this, cancelPojo.getData(), this);
        rvCancel.setAdapter(cancelListAdapter);

        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cancelId.equals("")) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("bookingId", bookingId);
                        jsonObject.put("resonId", cancelId);
                        progressDialog.setMessage(getString(R.string.loading));
                        presenter.cancelBooking(jsonObject);
                        alertDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(BookingScheduleActivity.this, getString(R.string.plsSelectCancel), Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelId = "";
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }

    @Override
    public void onCancelBooking(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;
        VariableConstant.IS_SHEDULE_EDITED = true;
        DialogHelper.customAlertDialogCloseActivity(this, getString(R.string.alert), msg, getString(R.string.oK));
        AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.BookingCancelled.value, bookingId);

        try {
            if (bookingDataHistory.getReminderId() != null && !bookingDataHistory.getReminderId().equals("")) {
                CalendarEventHelper calendarEventHelper = new CalendarEventHelper(this);
                calendarEventHelper.deleteEvent(bookingDataHistory.getReminderId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {

        if (bookingDataHistory == null) {
            return;
        }
        switch (v.getId()) {
            case R.id.llCutomerDetails:
                Intent intent = new Intent(this, CustomerReviewsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("bookingDataHistory", bookingDataHistory);
                intent.putExtras(bundle);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                } else {
                    startActivity(intent);
                    overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                }
                break;

            case R.id.ivCall:
                String uri = "tel:" + bookingDataHistory.getPhone();
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse(uri));
                startActivity(callIntent);
                break;

            case R.id.rlMessage:
                /*
                Intent msgIntent = new Intent(Intent.ACTION_VIEW);
                msgIntent.setData(Uri.parse("sms:"+bookingDataHistory.getPhone()));
                startActivity(msgIntent);*/

                sessionManager.setChatBookingID(bookingId);
                sessionManager.setChatCustomerName(bookingDataHistory.getCustomerData().getFirstName() + " " + bookingDataHistory.getCustomerData().getLastName());
                sessionManager.setChatCustomerPic(bookingDataHistory.getCustomerData().getProfilePic());
                sessionManager.setChatCustomerID(bookingDataHistory.getCustomerId());

                Intent chatIntent = new Intent(this, ChattingActivity.class);
                startActivity(chatIntent);
                break;

            case R.id.btnDone:
                progressDialog.setMessage(getString(R.string.loading));
                presenter.getCancelReason(bookingDataHistory.getBookingId());
                break;
        }
    }

    @Override
    public void onCancelSelected(String id) {
        cancelId = id;
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }
}
