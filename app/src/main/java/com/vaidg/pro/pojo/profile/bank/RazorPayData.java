package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RazorPayData implements Parcelable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("entity")
    @Expose
    private String entity;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("reference_id")
    @Expose
    private String referenceId;
    @SerializedName("batch_id")
    @Expose
    private String batchId;
    @SerializedName("active")
    @Expose
    private boolean active;
    @SerializedName("notes")
    @Expose
    private List<Object> notes = null;
    @SerializedName("created_at")
    @Expose
    private int createdAt;
    public final static Creator<RazorPayData> CREATOR = new Creator<RazorPayData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RazorPayData createFromParcel(android.os.Parcel in) {
            return new RazorPayData(in);
        }

        public RazorPayData[] newArray(int size) {
            return (new RazorPayData[size]);
        }

    }
            ;

    protected RazorPayData(android.os.Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.entity = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.contact = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.referenceId = ((String) in.readValue((String.class.getClassLoader())));
        this.batchId = ((String) in.readValue((String.class.getClassLoader())));
        this.active = ((boolean) in.readValue((boolean.class.getClassLoader())));
        in.readList(this.notes, (Object.class.getClassLoader()));
        this.createdAt = ((int) in.readValue((int.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public RazorPayData() {
    }

    /**
     *
     * @param createdAt
     * @param notes
     * @param contact
     * @param name
     * @param active
     * @param id
     * @param type
     * @param batchId
     * @param entity
     * @param email
     * @param referenceId
     */
    public RazorPayData(String id, String entity, String name, String contact, String email, String type, String referenceId, String batchId, boolean active, List<Object> notes, int createdAt) {
        super();
        this.id = id;
        this.entity = entity;
        this.name = name;
        this.contact = contact;
        this.email = email;
        this.type = type;
        this.referenceId = referenceId;
        this.batchId = batchId;
        this.active = active;
        this.notes = notes;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Object> getNotes() {
        return notes;
    }

    public void setNotes(List<Object> notes) {
        this.notes = notes;
    }

    public int getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(int createdAt) {
        this.createdAt = createdAt;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(entity);
        dest.writeValue(name);
        dest.writeValue(contact);
        dest.writeValue(email);
        dest.writeValue(type);
        dest.writeValue(referenceId);
        dest.writeValue(batchId);
        dest.writeValue(active);
        dest.writeList(notes);
        dest.writeValue(createdAt);
    }

    public int describeContents() {
        return 0;
    }

}
