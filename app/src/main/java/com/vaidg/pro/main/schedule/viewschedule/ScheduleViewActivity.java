package com.vaidg.pro.main.schedule.viewschedule;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.ViewPagerAdapter;
import com.vaidg.pro.main.schedule.viewschedulelist.ScheduleViewListFragment;
import com.vaidg.pro.pojo.shedule.ScheduleData;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import dagger.android.support.DaggerAppCompatActivity;
import javax.inject.Inject;

/**
 * Created by murashid on 04-Oct-17.
 * <h1>ScheduleViewActivity</h1>
 * ScheduleViewActivity for showing the list of created shcedule
 */

public class ScheduleViewActivity extends DaggerAppCompatActivity implements ScheculeViewContract.View {

    private ProgressDialog progressDialog;
    private ScheduleViewListFragment scheduleViewActive, scheduleViewPast;
    @Inject
    ScheculeViewContract.Presenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_view);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * init the views
     */
    private void init() {
        SessionManager sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingSchedule));
        progressDialog.setCancelable(false);

        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_x);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.viewSchedule));
        tvTitle.setTypeface(fontBold);
        Utility.setTextColor(this, tvTitle, R.color.normalTextColor);
        tvTitle.setTextSize(Utility.pixelsToSp(this.getResources().getDimension(R.dimen.sp_20), this));
        scheduleViewPast = ScheduleViewListFragment.getInstance();
        scheduleViewActive = ScheduleViewListFragment.getInstance();
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        TabLayout tablayoutScheduleView = findViewById(R.id.tablayoutScheduleView);
        ViewPager vpScheduleView = findViewById(R.id.vpScheduleView);
        viewPagerAdapter.addFragment(scheduleViewActive, getString(R.string.active));
        viewPagerAdapter.addFragment(scheduleViewPast, getString(R.string.past));
        vpScheduleView.setAdapter(viewPagerAdapter);
        tablayoutScheduleView.setupWithViewPager(vpScheduleView);

        presenter.getShedule();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();
    }

    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(ScheduleData data) {
        scheduleViewActive.setValues(data.getActive(),true);
        scheduleViewPast.setValues(data.getPast(),false);

        if (data.getPast().size() == 0 && data.getActive().size() == 0) {
            Toast.makeText(this, getString(R.string.noScheduleFoumd), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }
}
