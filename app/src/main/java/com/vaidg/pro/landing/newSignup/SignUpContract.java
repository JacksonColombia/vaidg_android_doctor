package com.vaidg.pro.landing.newSignup;

import android.content.Intent;
import android.net.Uri;

import androidx.annotation.StringRes;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.landing.newSignup.consultaionFee.model.ConsultationData;
import com.vaidg.pro.landing.newSignup.findHospital.model.HospitalDataPojo;
import com.vaidg.pro.landing.newSignup.model.OnFileUploadListener;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.pojo.profile.DegreeDetailsData;
import com.vaidg.pro.pojo.signup.CityData;
import com.vaidg.pro.pojo.signup.SignUpData;

import java.io.File;
import java.util.ArrayList;

/**
 * <h1>SignUpContract</h1>
 * <p>This is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface SignUpContract {

    interface View extends BaseView {
        /*initializes the views */

        /**
         * <p>This method is used to initialize views</p>
         */
        void initViews();

        /**
         * <p>This method is handle view on click event.</p>
         *
         * @param view the view reference
         */
        void onClick(android.view.View view);

        /**
         * <p>This method is handle select city option and redirect to select city screen.</p>
         */
        void selectCity();

        /**
         * <p>This method is handle clinic details option and redirect to select clinic screen.</p>
         */
        void selectClinicalDetails();

        /**
         * <p>This method is handle select degree option and redirect to select degree screen.</p>
         */
        void selectDegree();

        /**
         * <p>This method is start screen for image cropping.</p>
         *
         * @param recentTemp the image path
         */
        void startCropImage(String recentTemp);

        /**
         * <p>This method is display error when incorrect phone number is entered or empty.</p>
         *
         * @param resId the string resource id
         */
        void onPhoneNumberInCorrect(@StringRes int resId);

        /**
         * <p>This method is validate entered phone number and return boolean flag.</p>
         *
         * @return the true if correct
         */
        boolean isPhoneNumberCorrect();

        /**
         * <p>This method is hide displayed error when enter correct phone number.</p>
         */
        void onPhoneNumberCorrect();

        /**
         * <p>This method is remove entered phone number and show error when phone number is invalid or already registered.</p>
         */
        void clearPhoneNumber();

        /**
         * <p>This method is display error message when invalid password entered.</p>
         */
        void onPasswordInCorrect();

        /**
         * <p>This method is hide displayed error when enter correct password.</p>
         */
        void onPasswordCorrect();

        /**
         * <p>This method is display error message when username invalid.</p>
         */
        void onUserNameInCorrect();

        /**
         * <p>This method is hide displayed error when enter correct username.</p>
         */
        void onUserNameCorrect();

        /**
         * <p>This method is display error message when title invalid.</p>
         */
        void onTitleInCorrect();

        /**
         * <p>This method is hide displayed error when enter correct title.</p>
         */
        void onTitleCorrect();

        /**
         * <p>This method is display error message when last name invalid.</p>
         */
        void onUserLastNameInCorrect();

        /**
         * <p>This method is hide displayed error when enter correct last name.</p>
         */
        void onUserLastNameCorrect();

        /**
         * <p>This method is display error message when date not selected or invalid selected.</p>
         */
        void onDobInCorrect();

        /**
         * <p>This method is hide displayed error when correct date selected.</p>
         */
        void onDobCorrect();

        /**
         * <p>This method is display error message when email address is not entered or invalid.</p>
         */
        void onEmailInCorrect();

        /**
         * <p>This method is display error message that come form email verification API.</p>
         *
         * @param errorMsg the received error message
         */
        void onEmailInCorrect(String errorMsg);

        /**
         * <p>This method is hide displayed error when enter correct email address.</p>
         */
        void onEmailCorrect();

        /**
         * <p>This method is display passed error message in snack bar.</p>
         *
         * @param error the error message
         */
        void showError(String error);

        /**
         * <p>This method is receive selected city and set it into display.</p>
         *
         * @param cityData the {@link CityData} object of selected city
         */
        void displaySelectedCity(CityData cityData);

        /**
         * <p>This method is redirect user to select specialization when tap on that option.</p>
         *
         * @param specializationData the {@link SelectSpecializationData} object of data
         */
        void launchConsultationFee(SelectSpecializationData specializationData);

        /**
         * <p>This method is open gallery for user to select media from local storage.</p>
         */
        void openGallery();

        /**
         * <p>This method is open camera with set pre defined image save path.</p>
         *
         * @param uri_path the path to save captured image
         */
        void openCamera(Uri uri_path);

        /**
         * <p>This method is receive selected specialization data and set it to display.</p>
         *
         * @param catName the name of specialization
         */
        void showSpecializationData(String catName);

        /**
         * <p>This method is receive selected educational details and set it to display.</p>
         *
         * @param degree the selected degree
         */
        void showEducationalData(String degree);

        /**
         * <p>This method is receive entered years of experience and set it to display.</p>
         *
         * @param year the entered years
         */
        void showYOE(String year);

        /**
         * <p>This method is receive entered consultation fee details and set it to display.</p>
         *
         * @param consultationData the list of selected consultation fees
         */
        void showConsultationFee(ArrayList<ConsultationData> consultationData);

        /**
         * <p>This method is receive entered clinic data or selected hospital details and
         * set it to display.</p>
         *
         * @param clinic the {@link HospitalDataPojo} object of clinic or hospital details
         */
        void showClinicData(HospitalDataPojo clinic);

        /**
         * <p>This method is call with receive {@link SignUpData} object when doctor is successfully registered.</p>
         *
         * @param signUpData the object of signup success response
         */
        void success(SignUpData signUpData);

        /**
         * <h2>checkForVersion</h2>
         * This method is used to check for version of the device
         */
        void checkForVersion();
    }

    interface Presenter extends BasePresenter {

        /**
         * <p>This method is receive selected image path and store it into model/</p>
         *
         * @param filePath the path of profile pic
         */
        void setImageProfile(String filePath);

        /**
         * <p>This method is take entered first name and validate it.</p>
         *
         * @param toString the entered first name
         * @return true if correct either false
         */
        boolean verifyFirstName(String toString);

        /**
         * <p>This method is take entered last name and validate it.</p>
         *
         * @param toString the entered last name
         * @return true if correct either last
         */
        boolean verifyLastName(String toString);

        /**
         * <p>This method is take selected date of birth and validate it.</p>
         *
         * @param dob the selected date of birth
         * @return true if correct either false
         */
        boolean verifyDob(String dob);

        /**
         * <p>This method is take entered password and validate it.</p>
         *
         * @param toString the entered password
         * @return true if correct either false
         */
        boolean verifyPassword(String toString);

        /**
         * <p>This method is take entered email address and validate it.</p>
         *
         * @param toString the entered email address
         * @return true if correct either false
         */
        boolean verifyEmail(String toString);

        /**
         * <p>This method is take entered email address and check it on server that it's not registered.</p>
         *
         * @param toString the entered email address
         */
        void verifyEmailApi(String toString);

        /**
         * <p>This method is take selected country code, entered phone number and validate it.</p>
         *
         * @param phone       the entered first name
         * @param countryCode the entered first name
         * @return true if correct either false
         */
        boolean verifyPhone(String phone, String countryCode);

        /**
         * <p>This method is take selected country code, entered phone number and validate it on server.</p>
         *
         * @param phone       the entered first name
         * @param countryCode the entered first name
         */
        void verifyPhoneApi(String phone, String countryCode);

        /**
         * <p>This method is handle onActivityResult response.</p>
         *
         * @param requestCode the set request code
         * @param resultCode  the received result code
         * @param data        the data with result
         */
        void parseOnActivityResult(int requestCode, int resultCode, Intent data);

        /**
         * <p>This method is redirect user to consultation fee selection when tap on that option.</p>
         */
        void consultationFee();

        /**
         * <p>This method is display bottom sheet with some options when tap on select profile.</p>
         */
        void openChooser();

        /**
         * <p>This method is upload given media file to server and return remote url of that media.</p>
         *
         * @param from      the request code
         * @param mFileTemp the local file path
         * @param listener  the listener for upload status
         */
        void upload(String from, File mFileTemp, OnFileUploadListener listener);

        /**
         * <p>This method is compress provided image and then upload it to server.</p>
         *
         * @param from                 the request code
         * @param filePath             the local file path
         * @param onFileUploadListener the listener for upload status
         */
        void compressImage(String from, String filePath, OnFileUploadListener onFileUploadListener);

        /**
         * <p>This method is compress provided video and then upload it to server.</p>
         *
         * @param from                 the request code
         * @param file_path            the local file path
         * @param onFileUploadListener the listener for upload status
         */
        void compressedVideo(String from, String file_path, OnFileUploadListener onFileUploadListener);

        /**
         * <p>This method that check file path is local and remote url and return boolean flag.</p>
         *
         * @param url the file path
         * @return truee if it's remote url either false
         */
        boolean isRemoteUrl(String url);

        /**
         * <p>This method is provide recent selected file path.</p>
         *
         * @return the path of file
         */
        String getRecentTemp();

        /**
         * <p>This method is take all input and selection from user then validate it and make api call for register.</p>
         *
         * @param title        the title e.g. Dr.
         * @param firstName    the first name
         * @param lastName     the last name
         * @param DOB          the date of birth
         * @param gender       the gender
         * @param referralCode the referral code if any
         * @param email        the email address
         * @param ccp          the phone number country code
         * @param phoneNumber  the phone number
         * @param Password     the password
         */
        void signUp(String title, String firstName, String lastName, String DOB, int gender, String referralCode, String email, String ccp, String phoneNumber, String Password);

        /**
         * <p>This method is make signup api request.</p>
         */
        void callSignUpAPI();

        /**
         * <p>This method is take all file path and make request to upload if it's local.</p>
         *
         * @return true if all media are uploaded either false
         */
        boolean allMediaUploaded();

        /**
         * <p>This method is take entered title and validate it.</p>
         *
         * @param toString the entered email address
         */
        boolean verifyTitle(String toString);

        /**
         * <p>This method is provide education details if before filled.</p>
         * @return the list of {@link com.vaidg.pro.pojo.profile.DegreeDetailsData} objects
         */
        ArrayList<DegreeDetailsData> getEducationDetails();

        /**
         * <p>This method is provide years of experience if before entered.</p>
         * @return the string of year experience
         */
        String getYearOfExp();

        /**
         * <p>This method is provide consultation fee details if before filled.</p>
         * @return the list of {@link ConsultationData} objects
         */
        ArrayList<ConsultationData> getConsultationData();

        /**
         * <h2>checkForKeystore</h2>
         * This method is used to check for keystore
         */
        void checkForKeystore();
        /**
         * <h2>generateEncryptionKeyPostLollipop</h2>
         * This method is used to generate the encryption key required for encryption for post lollipop
         * @param exportedPublicKey
         */
        void generateEncryptionPublicKeyPostLollipop(String exportedPublicKey);

        /**
         * <h2>generateEncryptionKeyPostLollipop</h2>
         * This method is used to generate the encryption key required for encryption for post lollipop
         * @param exportedPrivateKey
         */
        void generateEncryptionPrivateKeyPostLollipop(String exportedPrivateKey);
        /**
         * <h2>generateEncryptionKeyPostLollipop</h2>
         * This method is used to generate the encryption key required for encryption for pre marshmallow
         * @param exportedPublicKey
         */
        void generateEncryptionPublicKeyPreMarshMallow(String exportedPublicKey);
        /**
         * <h2>generateEncryptionKeyPostLollipop</h2>
         * This method is used to generate the encryption key required for encryption for pre marshmallow
         * @param exportedPrivateKey
         */
        void generateEncryptionPrivateKeyPreMarshMallow(String exportedPrivateKey);
        /**
         * <h2>encryptPrivateKeyPostLollipop</h2>
         * This method is used to encrypt the data for post lollipop
         * @param input data to be encrypted
         */
        void encryptPrivateKeyPostLollipop(String input);
        /**
         * <h2>encryptPrivateKeyPostLollipop</h2>
         * This method is used to encrypt the data for pre marshmallow
         * @param input data to be encrypted
         */
        void encryptPrivateKeyPreMarshMallow(String input);
        /**
         * <h2>encryptPublicKeyPostLollipop</h2>
         * This method is used to encrypt the data for post lollipop
         * @param input data to be encrypted
         */
        void encryptPublicKeyPostLollipop(String input);
        /**
         * <h2>encryptPublicKeyPostLollipop</h2>
         * This method is used to encrypt the data for pre marshmallow
         * @param input data to be encrypted
         */
        void encryptPublicKeyPreMarshMallow(String input);

        void setVirgilPublicKeyT(String exportedPublicKey);

        void setVirgilPrivateKeyT(String exportedPrivateKey);

        void upDateToGallery();
    }
}