package com.vaidg.pro.pojo.appconfig;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentGateways implements Serializable
{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("fixedCommission")
    @Expose
    private String fixedCommission;
    @SerializedName("percentageCommission")
    @Expose
    private String percentageCommission;
    @SerializedName("id")
    @Expose
    private String id;

    public String getName() {
        return name;
    }
}

