package com.vaidg.pro.landing.newSignup.findHospital.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import static com.vaidg.pro.utility.VariableConstant.PROVIDER_UNDER_HOSPITAL;

public class HospitalDataPojo implements Parcelable {
    public static final Creator<HospitalDataPojo> CREATOR = new Creator<HospitalDataPojo>() {
        @Override
        public HospitalDataPojo createFromParcel(Parcel source) {
            return new HospitalDataPojo(source);
        }

        @Override
        public HospitalDataPojo[] newArray(int size) {
            return new HospitalDataPojo[size];
        }
    };
    private int type = PROVIDER_UNDER_HOSPITAL;      // 1 for clinic     2 for hospital
    @SerializedName("clinicName")
    @Expose
    private String clinicName;
    @SerializedName("clinicLogoWeb")
    @Expose
    private String clinicLogoWeb;
    @SerializedName("clinicLogoApp")
    @Expose
    private String clinicLogoApp;
    @SerializedName("placeId")
    @Expose
    private String placeId;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("businessName")
    @Expose
    private String businessName;
    @SerializedName("billingname")
    @Expose
    private String billingname;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("businessID")
    @Expose
    private String businessID;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cityID")
    @Expose
    private String cityID;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("postcode")
    @Expose
    private String postcode;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("vatnumber")
    @Expose
    private String vatnumber;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("phone")
    @Expose
    private List<HospitalPhonePojo> phone = null;
    @SerializedName("userImage")
    @Expose
    private String userImage;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("businessLogo")
    @Expose
    private String businessLogo;
    @SerializedName("providerList")
    @Expose
    private List<Object> providerList = null;

    public HospitalDataPojo() {
    }

    protected HospitalDataPojo(Parcel in) {
        this.type = in.readInt();
        this.clinicName = in.readString();
        this.clinicLogoWeb = in.readString();
        this.clinicLogoApp = in.readString();
        this.placeId = in.readString();
        this.country = in.readString();
        this.id = in.readString();
        this.businessName = in.readString();
        this.billingname = in.readString();
        this.password = in.readString();
        this.businessID = (String) in.readValue(String.class.getClassLoader());
        this.email = in.readString();
        this.cityID = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.address = in.readString();
        this.postcode = in.readString();
        this.latitude = (Double) in.readValue(Double.class.getClassLoader());
        this.longitude = (Double) in.readValue(Double.class.getClassLoader());
        this.vatnumber = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.phone = new ArrayList<HospitalPhonePojo>();
        in.readList(this.phone, HospitalPhonePojo.class.getClassLoader());
        this.userImage = in.readString();
        this.status = (String) in.readValue(String.class.getClassLoader());
        this.businessLogo = in.readString();
        this.providerList = new ArrayList<Object>();
        in.readList(this.providerList, Object.class.getClassLoader());
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBillingname() {
        return billingname;
    }

    public void setBillingname(String billingname) {
        this.billingname = billingname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBusinessID() {
        return businessID;
    }

    public void setBusinessID(String businessID) {
        this.businessID = businessID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCityID() {
        return cityID;
    }

    public void setCityID(String cityID) {
        this.cityID = cityID;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getVatnumber() {
        return vatnumber;
    }

    public void setVatnumber(String vatnumber) {
        this.vatnumber = vatnumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<HospitalPhonePojo> getPhone() {
        return phone;
    }

    public void setPhone(List<HospitalPhonePojo> phone) {
        this.phone = phone;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBusinessLogo() {
        return businessLogo;
    }

    public void setBusinessLogo(String businessLogo) {
        this.businessLogo = businessLogo;
    }

    public List<Object> getProviderList() {
        return providerList;
    }

    public void setProviderList(List<Object> providerList) {
        this.providerList = providerList;
    }

    public String getClinicName() {
        return clinicName;
    }

    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }

    public String getClinicLogoWeb() {
        return clinicLogoWeb;
    }

    public void setClinicLogoWeb(String clinicLogoWeb) {
        this.clinicLogoWeb = clinicLogoWeb;
    }

    public String getClinicLogoApp() {
        return clinicLogoApp;
    }

    public void setClinicLogoApp(String clinicLogoApp) {
        this.clinicLogoApp = clinicLogoApp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
        dest.writeString(this.clinicName);
        dest.writeString(this.clinicLogoWeb);
        dest.writeString(this.clinicLogoApp);
        dest.writeString(this.placeId);
        dest.writeString(this.country);
        dest.writeString(this.id);
        dest.writeString(this.businessName);
        dest.writeString(this.billingname);
        dest.writeString(this.password);
        dest.writeValue(this.businessID);
        dest.writeString(this.email);
        dest.writeString(this.cityID);
        dest.writeString(this.city);
        dest.writeString(this.state);
        dest.writeString(this.address);
        dest.writeString(this.postcode);
        dest.writeValue(this.latitude);
        dest.writeValue(this.longitude);
        dest.writeString(this.vatnumber);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeList(this.phone);
        dest.writeString(this.userImage);
        dest.writeValue(this.status);
        dest.writeString(this.businessLogo);
        dest.writeList(this.providerList);
    }
}
