package com.vaidg.pro.pojo.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ali on 12/22/2017.
 */

public class ChatMqttResponce implements Serializable {

    @SerializedName("data")
    @Expose
    private ChatData data;

    public ChatData getData() {
        return data;
    }

    public void setData(ChatData data) {
        this.data = data;
    }


}
