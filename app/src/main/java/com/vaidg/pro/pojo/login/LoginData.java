package com.vaidg.pro.pojo.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h1>LoginData</h1>
 * <p>It's SignIn API response data model that contain logged in user data.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 08/06/2020
 */
public class LoginData implements Serializable {

    @SerializedName("lastName")
    @Expose
    private String lastName;

    @SerializedName("profilePic")
    @Expose
    private String profilePic;

    @SerializedName("mobile")
    @Expose
    private String mobile;

    @SerializedName("fcmTopic")
    @Expose
    private String fcmTopic;

    @SerializedName("token")
    @Expose
    private LoginData.Token token;

    @SerializedName("call")
    @Expose
    private LoginData.Call call;

    @SerializedName("firstName")
    @Expose
    private String firstName;

    @SerializedName("countryCode")
    @Expose
    private String countryCode;

    @SerializedName("referralCode")
    @Expose
    private String referralCode;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("bid")
    @Expose
    private boolean bid;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("requester_id")
    @Expose
    private String requester_id;

    @SerializedName("privateKey")
    @Expose
    private String privateKey;
    @SerializedName("publickKey")
    @Expose
    private String publickKey;

    public boolean isBid() {
        return bid;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public String getPublickKey() {
        return publickKey;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFcmTopic() {
        return fcmTopic;
    }

    public void setFcmTopic(String fcmTopic) {
        this.fcmTopic = fcmTopic;
    }

    public LoginData.Token getToken() {
        return token;
    }

    public void setToken(LoginData.Token token) {
        this.token = token;
    }

    public LoginData.Call getCall() {
        return call;
    }

    public void setCall(LoginData.Call call) {
        this.call = call;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getBid() {
        return bid;
    }

    public void setBid(boolean bid) {
        this.bid = bid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRequester_id() {
        return requester_id;
    }

    public void setRequester_id(String requester_id) {
        this.requester_id = requester_id;
    }


    public static class Call implements Serializable{
        @SerializedName("willTopic")
        @Expose
        private String willTopic;
        @SerializedName("authToken")
        @Expose
        private String authToken;

        public String getWillTopic() {
            return willTopic;
        }

        public void setWillTopic(String willTopic) {
            this.willTopic = willTopic;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

    }

    public static class Token implements Serializable{
        @SerializedName("accessExpireAt")
        @Expose
        private String accessExpireAt;
        @SerializedName("accessToken")
        @Expose
        private String accessToken;
        @SerializedName("refreshToken")
        @Expose
        private String refreshToken;

        public String getAccessExpireAt() {
            return accessExpireAt;
        }

        public void setAccessExpireAt(String accessExpireAt) {
            this.accessExpireAt = accessExpireAt;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getRefreshToken() {
            return refreshToken;
        }

        public void setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
        }

    }
}
