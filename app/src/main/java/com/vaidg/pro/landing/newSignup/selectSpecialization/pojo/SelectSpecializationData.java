package com.vaidg.pro.landing.newSignup.selectSpecialization.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vaidg.pro.pojo.Attribute;

import java.util.List;

public class SelectSpecializationData implements Parcelable {

    public static final Creator<SelectSpecializationData> CREATOR = new Creator<SelectSpecializationData>() {
        @Override
        public SelectSpecializationData createFromParcel(Parcel source) {
            return new SelectSpecializationData(source);
        }

        @Override
        public SelectSpecializationData[] newArray(int size) {
            return new SelectSpecializationData[size];
        }
    };
    boolean selected = false;
    @SerializedName("catId")
    @Expose
    private String catId;
    @SerializedName("catName")
    @Expose
    private String catName;
    @SerializedName("minimumFeesForConsultancy")
    @Expose
    private String minimumFeesForConsultancy;
    @SerializedName("maximumFeesForConsultancy")
    @Expose
    private String maximumFeesForConsultancy;
    @SerializedName("attributes")
    @Expose
    private List<Attribute> attributes = null;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("currencyAbbr")
    @Expose
    private String currencyAbbr;
    @SerializedName("currencySymbol")
    @Expose
    private String currencySymbol;
    @SerializedName("callType")
    @Expose
    private CallType callType;

    public SelectSpecializationData() {
    }

    protected SelectSpecializationData(Parcel in) {
        this.catId = in.readString();
        this.catName = in.readString();
        this.minimumFeesForConsultancy = (String) in.readValue(String.class.getClassLoader());
        this.maximumFeesForConsultancy = (String) in.readValue(String.class.getClassLoader());
        this.attributes = in.createTypedArrayList(Attribute.CREATOR);
        this.currency = in.readString();
        this.currencyAbbr = in.readString();
        this.currencySymbol = in.readString();
        this.callType = in.readParcelable(CallType.class.getClassLoader());
        this.selected = in.readByte() != 0;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getMinimumFeesForConsultancy() {
        return minimumFeesForConsultancy;
    }

    public void setMinimumFeesForConsultancy(String minimumFeesForConsultancy) {
        this.minimumFeesForConsultancy = minimumFeesForConsultancy;
    }

    public String getMaximumFeesForConsultancy() {
        return maximumFeesForConsultancy;
    }

    public void setMaximumFeesForConsultancy(String maximumFeesForConsultancy) {
        this.maximumFeesForConsultancy = maximumFeesForConsultancy;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyAbbr() {
        return currencyAbbr;
    }

    public void setCurrencyAbbr(String currencyAbbr) {
        this.currencyAbbr = currencyAbbr;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public CallType getCallType() {
        return callType;
    }

    public void setCallType(CallType callType) {
        this.callType = callType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.catId);
        dest.writeString(this.catName);
        dest.writeValue(this.minimumFeesForConsultancy);
        dest.writeValue(this.maximumFeesForConsultancy);
        dest.writeTypedList(this.attributes);
        dest.writeString(this.currency);
        dest.writeString(this.currencyAbbr);
        dest.writeString(this.currencySymbol);
        dest.writeParcelable(this.callType, flags);
        dest.writeByte(this.selected ? (byte) 1 : (byte) 0);
    }
}
