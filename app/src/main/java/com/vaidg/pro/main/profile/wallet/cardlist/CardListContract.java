package com.vaidg.pro.main.profile.wallet.cardlist;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.profile.wallet.CardData;
import com.vaidg.pro.pojo.profile.wallet.WalletData;
import java.util.ArrayList;
import okhttp3.RequestBody;
import org.json.JSONObject;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface CardListContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onFailure(String failureMsg);

    void onFailure();

    void onSuccessCardDetails(ArrayList<CardData> data);

    void onSuccessSelectCard(String msg);

    void onSuccessDeleteCard(String msg);

    void onNewToken(String newToken);

    void sessionExpired(String msg);
  }

  interface Presenter extends BasePresenter {

    /**
     * <h2>getWalletDetails</h2>
     * <p>
     * api call to get wallet setting details
     * method for calling api for geting the Card details
     * </p>
     */
    void getCardDetails();

    /**
     * <h2>getWalletDetails</h2>
     * <p>
     * api call to get wallet setting details
     * method for calling api for geting the Category details
     * </p>
     */
    void selectCard(final JSONObject jsonObjectRequest);

    void deleteCard(final JSONObject jsonObjectRequest);
  }
}