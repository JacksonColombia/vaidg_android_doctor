package com.vaidg.pro.landing.newSignup.findHospital;

import android.content.Intent;

import com.vaidg.pro.BaseView;

public interface FindHospitalContract {

    interface View extends BaseView {

        /**
         * <p>This method is used to initialize views</p>
         */
        void initViews();

        void notifyAdapter();

        /**
         * <p>This method is display passed error message in snack bar.</p>
         */
        void showError();

        void showError(String error);

        void sendBackData(Intent intent);
    }

    interface Presenter {

        void findHospital(String cityId, String place);
    }
}
