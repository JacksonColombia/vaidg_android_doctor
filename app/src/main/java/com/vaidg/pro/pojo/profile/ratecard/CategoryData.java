package com.vaidg.pro.pojo.profile.ratecard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 13-Feb-18.
 */

public class CategoryData implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("city_id")
    @Expose
    private String city_id;
    @SerializedName("service_type")
    @Expose
    private String service_type;
    @SerializedName("billing_model")
    @Expose
    private String billing_model;
    @SerializedName("cat_name")
    @Expose
    private String cat_name;
    @SerializedName("cat_desc")
    @Expose
    private String cat_desc;
    @SerializedName("minimumFeesForConsultancy")
    @Expose
    private String minimumFeesForConsultancy = "0";
    @SerializedName("maximumFeesForConsultancy")
    @Expose
    private String maximumFeesForConsultancy = "0";
    @SerializedName("consultancyFee")
    @Expose
    private String consultancyFee = "0";
    @SerializedName("minimum_fees")
    @Expose
    private String minimum_fees = "0";
    @SerializedName("miximum_fees")
    @Expose
    private String miximum_fees = "0";
    @SerializedName("price_per_fees")
    @Expose
    private String price_per_fees = "0";
    @SerializedName("callType")
    @Expose
    private CallType callType;
    @SerializedName("service")
    @Expose
    private ArrayList<CategoryService> service;
    @SerializedName("subCatArr")
    @Expose
    private ArrayList<SubCategoryRateCard> subCatArr;

    public String getMinimumFeesForConsultancy() {
        return minimumFeesForConsultancy;
    }

    public String getMaximumFeesForConsultancy() {
        return maximumFeesForConsultancy;
    }

    public String getConsultancyFee() {
        return consultancyFee;
    }

    public void setConsultancyFee(String consultancyFee) {
        this.consultancyFee = consultancyFee;
    }

    public CallType getCallType() {
        return callType;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getBilling_model() {
        return billing_model;
    }

    public void setBilling_model(String billing_model) {
        this.billing_model = billing_model;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getCat_desc() {
        return cat_desc;
    }

    public void setCat_desc(String cat_desc) {
        this.cat_desc = cat_desc;
    }

    public String getMinimum_fees() {
        return minimum_fees;
    }

    public void setMinimum_fees(String minimum_fees) {
        this.minimum_fees = minimum_fees;
    }

    public String getMiximum_fees() {
        return miximum_fees;
    }

    public void setMiximum_fees(String miximum_fees) {
        this.miximum_fees = miximum_fees;
    }

    public String getPrice_per_fees() {
        return price_per_fees;
    }

    public void setPrice_per_fees(String price_per_fees) {
        this.price_per_fees = price_per_fees;
    }

    public ArrayList<CategoryService> getService() {
        return service;
    }

    public void setService(ArrayList<CategoryService> service) {
        this.service = service;
    }

    public ArrayList<SubCategoryRateCard> getSubCatArr() {
        return subCatArr;
    }

    public void setSubCatArr(ArrayList<SubCategoryRateCard> subCatArr) {
        this.subCatArr = subCatArr;
    }

    @Override
    public String toString() {
        return "{" +
                "_id='" + _id + '\'' +
                ", city_id='" + city_id + '\'' +
                ", service_type='" + service_type + '\'' +
                ", billing_model='" + billing_model + '\'' +
                ", cat_name='" + cat_name + '\'' +
                ", cat_desc='" + cat_desc + '\'' +
                ", minimum_fees='" + minimum_fees + '\'' +
                ", miximum_fees='" + miximum_fees + '\'' +
                ", price_per_fees='" + price_per_fees + '\'' +
                ", service=" + service +
                ", subCatArr=" + subCatArr +
                '}';
    }

    public class CallType implements Serializable {
        @SerializedName("inCall")
        @Expose
        private boolean incall;
        @SerializedName("outCall")
        @Expose
        private boolean outcall;
        @SerializedName("teleCall")
        @Expose
        private boolean telecall;

        public boolean isIncall() {
            return incall;
        }

        public boolean isOutcall() {
            return outcall;
        }

        public boolean isTelecall() {
            return telecall;
        }
    }
}
