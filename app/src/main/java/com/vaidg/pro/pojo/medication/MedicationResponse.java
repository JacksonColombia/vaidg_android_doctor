package com.vaidg.pro.pojo.medication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h1>LogInResponse</h1>
 * <p>It's model for SignIn API response.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 08/06/2020
 */
public class MedicationResponse implements Serializable {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<MedicationData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MedicationData> getData() {
        return data;
    }

    public void setData(ArrayList<MedicationData> data) {
        this.data = data;
    }
}
