package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.vaidg.pro.R;
import com.vaidg.pro.main.profile.bank.BankBottomSheetFragment;
import com.vaidg.pro.main.profile.bank.bankdetails.BankDetailsActivity;
import com.vaidg.pro.pojo.profile.bank.AccountData;
import com.vaidg.pro.pojo.profile.bank.BankList;
import com.vaidg.pro.utility.Utility;

import java.io.Serializable;
import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * Created by murashid on 20-Mar-17.
 * <h1>BankListAdapter</h1>
 * BankList Recycler View adapter for bank list
 *
 * @see BankDetailsActivity
 */

public class BankListAdapter extends RecyclerView.Adapter<BankListAdapter.ViewHolder> implements View.OnClickListener {

    private static final String TAG = "BankListAdapter";
    private ArrayList<AccountData> bankLists;
    private Typeface fontMedium, fontLight;
    private RefreshBankDetails refreshBankDetails;
    private FragmentManager fragmentManager;
    private Context context;

    public BankListAdapter(Context context, ArrayList<AccountData> bankLists, FragmentManager fragmentManager, RefreshBankDetails refreshBankDetails) {
        this.bankLists = bankLists;
        this.context = context;
        fontMedium = Utility.getFontMedium(context);
        fontLight = Utility.getFontRegular(context);
        this.fragmentManager = fragmentManager;
        this.refreshBankDetails = refreshBankDetails;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BankListAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_bank_details, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AccountData accountData = bankLists.get(position);
        holder.tvAccountNo.setText("xxxxxxxx" +accountData.getLast4());
        holder.tvBankName.setText(accountData.getBankName());
        holder.tvRoutinNo.setText(context.getString(R.string.rountinNoLabel) + " " + accountData.getRoutingNumber());
        //change to default account when we get the proper responce
        if (accountData.isDefaultForCurrency()) {
            holder.ivTick.setVisibility(View.VISIBLE);
        } else {
            holder.ivTick.setVisibility(View.INVISIBLE);
        }

        holder.llBankDetails.setOnClickListener(this);
        holder.llBankDetails.setTag(holder);

    }

    @Override
    public int getItemCount() {
        return bankLists != null ? bankLists.size() : ZERO;
    }

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position = viewHolder.getAdapterPosition();

        switch (v.getId()) {
            case R.id.llBankDetails:
                BottomSheetDialogFragment bottomSheetDialogFragment = BankBottomSheetFragment.newInstance(bankLists.get(position), refreshBankDetails);
                bottomSheetDialogFragment.show(fragmentManager, bottomSheetDialogFragment.getTag());
                break;

        }
    }

    /**
     * <h1>RefreshBankDetails</h1>
     * Interface for refresh the bank list
     */

    public interface RefreshBankDetails extends Serializable {
        void onRefresh();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvAccountNo, tvBankName, tvRoutinNo;
        LinearLayout llBankDetails;
        ImageView ivTick;

        public ViewHolder(View itemView) {
            super(itemView);

            tvAccountNo = itemView.findViewById(R.id.tvAccountNo);
            tvBankName = itemView.findViewById(R.id.tvBankName);
            tvRoutinNo = itemView.findViewById(R.id.tvRoutinNo);
            llBankDetails = itemView.findViewById(R.id.llBankDetails);
            ivTick = itemView.findViewById(R.id.ivTick);

            tvAccountNo.setTypeface(fontLight);
            tvBankName.setTypeface(fontLight);
            tvRoutinNo.setTypeface(fontLight);
        }

    }

}
