package com.vaidg.pro.pojo.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 07-Mar-18.
 */

public class AdditionalService implements Serializable {
    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("price")
    @Expose
    private String price;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
