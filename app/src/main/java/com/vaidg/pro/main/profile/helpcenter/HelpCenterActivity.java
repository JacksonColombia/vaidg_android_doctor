package com.vaidg.pro.main.profile.helpcenter;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.TicketListAdapter;
import com.vaidg.pro.pojo.profile.helpcenter.Ticket;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import dagger.android.support.DaggerAppCompatActivity;
import java.util.ArrayList;
import javax.inject.Inject;

public class HelpCenterActivity extends DaggerAppCompatActivity implements View.OnClickListener, HelpCenterContract.View {


    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    @Inject
    HelpCenterContract.Presenter helpCenterPresenter;

    private TicketListAdapter ticketListAdapter;
    private ArrayList<Ticket> wholeTicketData;

    private ImageView ivNoTicket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_center);

        init();
    }

    private void init() {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingTicket));
        progressDialog.setCancelable(false);

        Typeface fonMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);

        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.helpCenter));
        tvTitle.setTypeface(fontBold);


        ImageView ivCloseButton = findViewById(R.id.ivCloseButton);
        ivNoTicket = findViewById(R.id.ivNoTicket);
        ivCloseButton.setVisibility(View.VISIBLE);
        ivCloseButton.setImageResource(R.drawable.vector_color_primary_plus);
        ivCloseButton.setOnClickListener(this);


        wholeTicketData = new ArrayList<>();
        ticketListAdapter = new TicketListAdapter(this, wholeTicketData);
        RecyclerView rvTickects = findViewById(R.id.rvTickects);
        rvTickects.setLayoutManager(new LinearLayoutManager(this));
        rvTickects.setAdapter(ticketListAdapter);

        helpCenterPresenter.getAllTickets(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), sessionManager.getEmail());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (VariableConstant.IS_TICKET_UPDATED) {
            VariableConstant.IS_TICKET_UPDATED = false;
            helpCenterPresenter.getAllTickets(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), sessionManager.getEmail());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        Utility.progressDialogCancel(this, progressDialog);
        finishAfterTransition();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivCloseButton:
                Intent helpCenterIntent = new Intent(this, NewTicketActivity.class);
                helpCenterIntent.putExtra("isAlreadyRaised", false);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    startActivity(helpCenterIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                } else {
                    startActivity(helpCenterIntent);
                    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }

                break;
        }
    }


    @Override
    public void startProgressBar() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void stopProgressBar() {
        Utility.progressDialogDismiss(this, progressDialog);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(ArrayList<Ticket> wholeTicketData) {
        this.wholeTicketData.clear();
        this.wholeTicketData.addAll(wholeTicketData);
        ticketListAdapter.notifyDataSetChanged();

        if (this.wholeTicketData.size() > 0) {
            ivNoTicket.setVisibility(View.GONE);
        } else {
            ivNoTicket.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {
    }
}
