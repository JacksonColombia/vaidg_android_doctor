package com.vaidg.pro.pojo.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RateNotification implements Serializable {
    @SerializedName("errNum")
    @Expose
    private String errNum;
    @SerializedName("data")
    @Expose
    private Data data;

    @Override
    public String toString() {
        return "{" +
                "errNum='" + errNum + '\'' +
                ", data=" + data +
                '}';
    }

    public String getErrNum() {
        return errNum;
    }

    public Data getData() {
        return data;
    }

    public class Data {
        private String bookingId;

        @Override
        public String toString() {
            return "{" +
                    "bookingId='" + bookingId + '\'' +
                    '}';
        }

        public String getBookingId() {
            return bookingId;
        }
    }

}
