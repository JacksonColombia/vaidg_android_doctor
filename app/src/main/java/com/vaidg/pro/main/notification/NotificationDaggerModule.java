package com.vaidg.pro.main.notification;

import android.app.Activity;
import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>LogInDaggerModule</h1>
 * <p>This dagger module created for LogInActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
@Module
public abstract class NotificationDaggerModule {

    @ActivityScoped
    @Binds
    abstract Activity getActivity(NotificationActivity notificationActivity);


    @ActivityScoped
    @Binds
    abstract NotificationContract.View getView(NotificationActivity mainActivity);

    @ActivityScoped
    @Binds
    abstract NotificationContract.Presenter getPresenter(NotificationPresenter notificationPresenter);

}
