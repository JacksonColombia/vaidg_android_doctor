package com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.pojo.PreDefined;

import java.util.ArrayList;

/**
 * <h1>@ListSelectionContract</h1>
 * <p>this is interface b.w view and presenter</p>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class ListSelectionContract {

    interface View {

        /**
         * <h2>@notifyAdapter</h2>
         * <p>this is used for notifying recycler view adapter</p>
         */
        void notifyAdapter();

        /**
         * <h2>@getData</h2>
         * <p>this is used for getting data from view to presenter</p>
         *
         * @return
         */
        ArrayList<PreDefined> getData();

        /**
         * <p>This method is return boolean flag either it's mandatory or not.</p>
         * @return the true if madatory else false
         */
        boolean isMandatoryForProvider();

        /**
         * <h2>@moveNextFrag</h2>
         * <p>for moving next screen</p>
         */
        void moveNextFrag();

        /**
         * <h2>@showError</h2>
         * <p>this is for showing error</p>
         *
         * @param error
         */
        void showError(String error);
    }

    interface Presenter extends BasePresenter<View> {

        /**
         * <h2>@isCheck</h2>
         */
        void isCheck();

        /**
         * <h2>@saveData</h2>
         *
         * @param current_item
         */
        void saveData(Attribute current_item);
    }
}
