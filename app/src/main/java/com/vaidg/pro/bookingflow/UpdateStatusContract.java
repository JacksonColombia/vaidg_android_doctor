package com.vaidg.pro.bookingflow;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.pojo.booking.CancelPojo;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import java.io.File;
import java.util.Map;
import org.json.JSONObject;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface UpdateStatusContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onSuccess(String msg, boolean bookingDetailsRefreshed);

    void onSuccessCancelReason(CancelPojo cancelPojo);

    void onCancelBooking(String msg);

    void onFailure(String failureMsg);

    void onFailure();

    void onNewToken(String newToken);

    void sessionExpired(String msg);

    void launchCallsScreen(String roomId, String callId, String callType);
  }

  interface Presenter extends BasePresenter {
    /**
     * method for calinng api for geting details for particular booking
     *
     * @param bookingId booking id
     */
    void getBookingById(final String bookingId);

    /**
     * Method for calling api for update the job status whether accept or reject
     *
     * @param jsonObject required field in json object
     */
    void acceptRejectJob(final JSONObject jsonObject);

    /**
     * Method for calling api for update the job status in ArrivedActivity and OnTheWayActivity
     *
     * @param object required field in json object
     */
    void updateStaus(JSONObject object, final boolean isTelecallBooking);

    /**
     * Method for calling api for update the job status in EvenStartedActivity
     * it internally calling timer api if its job started status
     *
     * @param jsonObject         required field in json object
     * @param jsonObjectTimer    required field in json object for starting timer api
     * @param isFromUpdateStatus boolean for checking is from updateStatus or normal start and stop the timer
     */
    void updateStaus(final JSONObject jsonObject, final JSONObject jsonObjectTimer, final boolean isFromUpdateStatus);

    /**
     * Method for calling api for update the job status in invoice screen and internally calling the method <h1>amazonUpload</h1> for upload singutre picture
     *
     * @param jsonObject required field in jsonobject
     * @param amazonS3   UploadFileAmazonS3 object
     * @param mFileTemp  signature file
     */
    void updateStaus(final JSONObject jsonObject, final UploadFileAmazonS3 amazonS3, final File mFileTemp);

    /**
     * method for calling api for start and stop the timer
     *
     * @param jsonObject         jsonObject that contain required field
     * @param isFromUpdateStatus check its from update status or normal start and stop the timer
     */
    void updateTimer(final JSONObject jsonObject, final boolean isFromUpdateStatus);

    /**
     * Method for calling api for getting the cancel reasion
     *
     * @param bookingId
     */
    void getCancelReason(final String bookingId);

    void initCallApi(String calltoken, final String currentRoomId, String customerId, final String callType, String bookingId);

    /**
     * method for uploading image to amazon
     *
     * @param amazonS3  object of the UploadFileAmazonS3
     * @param mFileTemp file which has to been upload in amazon
     */
    void amazonUpload(UploadFileAmazonS3 amazonS3, final File mFileTemp);

    /**
     * Method for calling api for update the job status
     *
     * @param jsonObject
     */
    void cancelBooking(final JSONObject jsonObject);
    /*Method calling api for updating the bid for the job*/

    void updateBidding(JSONObject jsonObject);

    String getBookingStr(Booking booking, String oldStr, String updatedStatus);

    String getDurationString(long seconds);

    long getTimeWhileInBackground(SessionManager sessionManager);
  }
}