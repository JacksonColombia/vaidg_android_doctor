package com.vaidg.pro.landing.newSignup.editEducation.selectdegree.model;

import com.vaidg.pro.BaseModel;
import com.vaidg.pro.pojo.signup.DegreeData;
import com.vaidg.pro.pojo.signup.DegreePojo;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import javax.inject.Inject;

public class SelectDegreeModel extends BaseModel {

    @Inject
    ArrayList<DegreeData> degreeData;

    @Inject
    Utility utility;

    @Inject
    public SelectDegreeModel() {
    }

    /**
     * This method is deserializes the specified city response into the {@link DegreePojo} class.
     *
     * @param degreePojo the response of city data
     */
    public void parseDegreeData(DegreePojo degreePojo) {
        degreeData.addAll(degreePojo.getData());
    }

    public DegreeData getSelectedDegree(int position) {
        return degreeData.get(position);
    }

}
