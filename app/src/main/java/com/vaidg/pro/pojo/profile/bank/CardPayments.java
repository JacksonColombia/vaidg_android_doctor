package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CardPayments implements Serializable {

	@SerializedName("statement_descriptor_prefix")
	@Expose
	private Object statementDescriptorPrefix;

	@SerializedName("decline_on")
	@Expose
	private DeclineOn declineOn;

	public Object getStatementDescriptorPrefix(){
		return statementDescriptorPrefix;
	}

	public DeclineOn getDeclineOn(){
		return declineOn;
	}

	@Override
 	public String toString(){
		return 
			"CardPayments{" + 
			"statement_descriptor_prefix = '" + statementDescriptorPrefix + '\'' + 
			",decline_on = '" + declineOn + '\'' + 
			"}";
		}
}