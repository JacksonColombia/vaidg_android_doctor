package com.vaidg.pro;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.bumptech.glide.request.target.ViewTarget;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.gson.Gson;

import com.vaidg.pro.account.AccountManagerHelper;

import com.vaidg.pro.dagger.AppComponent;
import com.vaidg.pro.dagger.AppUtilModule;
import com.vaidg.pro.dagger.DaggerAppComponent;
import com.vaidg.pro.mqtt.MqttHelper;
import com.vaidg.pro.network.NetworkModule;
import com.vaidg.pro.pojo.appconfig.AppConfigData;
import com.vaidg.pro.service.NewBookingRingtoneService;
import com.vaidg.pro.telecall.utility.CallHelper;
import com.vaidg.pro.utility.AppConfig;
import com.vaidg.pro.utility.LocaleUtil;
import com.vaidg.pro.utility.MixpanelHelper;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.TimezoneMapper;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.InitializationCallback;

import java.util.Locale;
import java.util.TimeZone;

import static com.vaidg.pro.pojo.appconfig.AppConfigData.*;

//import android.support.multidex.MultiDex;

/**
 * <h>AppController</h>
 * Created by murashid on 02-Oct-17.
 */
public class AppController extends DaggerApplication {
  public static final String TAG = AppController.class.getSimpleName();
  private static AppController mInstance;
  private static Context mContext;
  private static Thread.UncaughtExceptionHandler mDefaultUncaughtExceptionHandler;
  private static Thread.UncaughtExceptionHandler mCaughtExceptionHandler = new Thread.UncaughtExceptionHandler() {
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
//            if (AppController.getInstance().isActiveOnACall()) {
//                AppController.getInstance().cutCallOnKillingApp(true);
//            }
      mDefaultUncaughtExceptionHandler.uncaughtException(thread, ex);
    }
  };
  private MqttHelper mqttHelper;
  private CallHelper callHelper;
  private MixpanelHelper mixpanelHelper;
  private AccountManagerHelper accountManagerHelper;
  private Intent newBookingRingtoneServiceIntent;
  private SessionManager sessionManager;
  private TimeZone timeZone;
  private AppComponent appComponent;

  public static synchronized AppController getInstance() {
    return mInstance;
  }

  public static Context getContext() {
    return mContext;
  }

  public static synchronized void toast() {
    Toast.makeText(getInstance(), getInstance().getString(R.string.serverError), Toast.LENGTH_SHORT).show();
  }

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    //MultiDex.install(this);
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    Log.e("TAG", "onConfigurationChanged");
    setLanguageValueTosession();
  }

  @Override
  public void onCreate() {
    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    super.onCreate();
//        Fabric.with(this, new Crashlytics());
    Fresco.initialize(this);
    ViewTarget.setTagId(R.id.glide_tag);
    CrashlyticsCore core = new CrashlyticsCore.Builder()
        .disabled(BuildConfig.DEBUG)
        .build();
    Fabric.with(new Fabric.Builder(this).kits(new Crashlytics.Builder()
        .core(core)
        .build())
        .initializationCallback(new InitializationCallback<Fabric>() {
          @Override
          public void success(Fabric fabric) {
            mDefaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(mCaughtExceptionHandler);
          }

          @Override
          public void failure(Exception e) {
            e.printStackTrace();
          }
        })
        .build());
    sessionManager = SessionManager.getSessionManager(this);
    mInstance = this;
    mContext = this;
    mixpanelHelper = new MixpanelHelper(this);
    // mqttHelper = new MqttHelper(this);
    getMqttHelper();
  //  setLanguageValueTosession();
  }
  private void setLanguageValueTosession() {
    if(TextUtils.isEmpty(sessionManager.getLanguageCode()))
    {
      DEFAULT_DISPLAYLANGUAGE  = "English";
      DEFAULT_LANGUAGE = "en";
    }else{
      if(sessionManager.getLanguageCode().equals("en"))
      {
        DEFAULT_DISPLAYLANGUAGE  = "English";
        DEFAULT_LANGUAGE = "en";
      }else
      {
        DEFAULT_DISPLAYLANGUAGE  = "Portuguese";
        DEFAULT_LANGUAGE = "pt";
      }
    }
    sessionManager.setLanguageCode(DEFAULT_LANGUAGE);
    Utility.changeLanguageConfig(DEFAULT_LANGUAGE, this);
  }

  @Override
  protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
    appComponent = DaggerAppComponent.builder()
        .application(this)
        .appUtil(new AppUtilModule())
        .netModule(new NetworkModule())
        .build();
    appComponent.inject(this);
    return appComponent;
  }

  public TimeZone getTimeZone() {
      String timeZoneString = "";
      if (!sessionManager.getCurrentLat().equals("0.0")) {
          timeZoneString = TimezoneMapper.latLngToTimezoneString(Double.parseDouble(sessionManager.getCurrentLat()), Double.parseDouble(sessionManager.getCurrentLng()));
      } else {
          timeZoneString = TimeZone.getDefault().getID();
      }
      return TimeZone.getTimeZone(timeZoneString);
  }

  public MqttHelper getMqttHelper() {
    if (mqttHelper == null) {
      mqttHelper = new MqttHelper(this);
    }
    return mqttHelper;
  }

  public CallHelper getCallHelper() {
    if (callHelper == null) {
      callHelper = new CallHelper(this);
    }
    return callHelper;
  }

  public MixpanelHelper getMixpanelHelper() {
    if (mixpanelHelper == null) {
      mixpanelHelper = new MixpanelHelper(this);
    }
    return mixpanelHelper;
  }

  public AccountManagerHelper getAccountManagerHelper() {
    if (accountManagerHelper == null) {
      accountManagerHelper = new AccountManagerHelper(this);
    }
    return accountManagerHelper;
  }

  private Intent getNewBookingRingtoneServiceIntent() {
    if (newBookingRingtoneServiceIntent == null) {
      newBookingRingtoneServiceIntent = new Intent(this, NewBookingRingtoneService.class);
    }
    return newBookingRingtoneServiceIntent;
  }

  public void startNewBookingRingtoneService() {
    startService(getNewBookingRingtoneServiceIntent());
  }

  public void stopNewBookingRingtoneService() {
    stopService(getNewBookingRingtoneServiceIntent());
  }

  public synchronized void toast(String msg) {
    Toast.makeText(getInstance(), msg, Toast.LENGTH_SHORT).show();
  }

  public void appLogout() {
    // todo applogout when token expire
  }
}

