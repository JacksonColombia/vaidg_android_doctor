package com.vaidg.pro.service;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.main.MainActivity;
import com.vaidg.pro.mqtt.MqttEvents;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.ServiceFactory;
import com.vaidg.pro.utility.LocationUtil;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.HashMap;
import java.util.Map;
import okhttp3.ResponseBody;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;
import retrofit2.Response;

/**
 * Created by embed on 19/1/17.
 */
public class LocationPublishService extends Service implements LocationUtil.GetLocationListener {
    String transit = "0";
    private SessionManager sessionManager;
    private Timer mTimer;
    private TimerTask mTimerTask;
    private String TAG = "LocationPublishService";
    private LocationUtil locationUtil;
    private BroadcastReceiver mBatInfoReceiver;
    private String batteryLevel = "50";
    private boolean isLocationLogDpUpdated = false;
    private boolean isLocationLogApiCalling = false;
    private double prevLat, prevLng, strayLat = -1.0, strayLng = -1.0;
    private NetworkService service;


    private static void setNotificationChannel(NotificationManager notificationManager, String id, String name, String description) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_HIGH);
            mChannel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(mChannel);

        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mBatInfoReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent intent) {
                // TODO Auto-generated method stub
                int level = intent.getIntExtra("level", 0);
                batteryLevel = String.valueOf(level);
            }
        };

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(mBatInfoReceiver, ifilter);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            if (locationUtil == null) {
                locationUtil = new LocationUtil(this, this);
            }
            sessionManager = SessionManager.getSessionManager(LocationPublishService.this);
          service = ServiceFactory.getClient(NetworkService.class,AppController.getContext());

          if (intent.getAction().equals(VariableConstant.ACTION.STARTFOREGROUND_ACTION)) {
                Log.d(TAG, "onStartCommand: Serviceeeeeeeeeeeeeee starteeeeeeeeeedddddd");

                Intent notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_RECEIVER_FOREGROUND);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                        notificationIntent, 0);

                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                        R.mipmap.ic_launcher_round);

                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "foreground")
                        .setContentTitle(getString(R.string.app_name))
                        .setTicker("")
                        .setContentText(getString(R.string.online))
                        .setSmallIcon(R.drawable.ic_notification)
                        .setLargeIcon(icon)
                        .setContentIntent(pendingIntent)
                        .setOngoing(true);
                Notification notification = notificationBuilder.build();
                NotificationManager notificationManager = (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
                setNotificationChannel(notificationManager, "foreground", this.getString(R.string.notificationChannelNameChat), this.getString(R.string.notificationChannelDescriptionChat));
                notificationManager.notify(107, notification);

                startPublishingWithTimer();

                startForeground(107, notification);

                if (!locationUtil.isGoogleApiClientConnected()) {
                    locationUtil.connectGoogleApiClient();
                }
            } else if (intent.getAction().equals(VariableConstant.ACTION.STOPFOREGROUND_ACTION)) {
                Log.d(TAG, "onStartCommand: Serviceeeeeeeeeeeeeee Stoppppppppppppeddd");
                if (locationUtil != null) {
                    locationUtil.disconnectGoogleApiClient();
                }
                stopForeground(true);
                stopSelf();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Crashed in forground service" + e);
        }

        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mTimer != null) {
            mTimer.cancel();
        }
        if (mTimerTask != null) {
            mTimerTask.cancel();
        }

        unregisterReceiver(mBatInfoReceiver);
    }

    /**
     * method for creating Timer and TimerTask for contineous updating latlng
     * period is based on api response in appconfig
     */
    private void startPublishingWithTimer() {
        long countDown = 1000 * Long.parseLong(sessionManager.getLocationPublishInterval());
        mTimer = new Timer();
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
                if (Utility.isNetworkAvailable(getApplicationContext())) {
                    if (!isLocationLogDpUpdated) {
                        updateLocation();
                    } else if (!isLocationLogApiCalling) {
                        updateLocationLogs();
                    }
                } else {
                    isLocationLogDpUpdated = true;
                    sessionManager.updateLocationArray(sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
                }

                calculateRouteDistance();
            }
        };
        mTimer.schedule(mTimerTask, 000, countDown);
    }


    /**
     * method for calling api to update the updateLocation
     */
    private void updateLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.FOREGROUND_SERVICE)
            != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        String locationCheck = "0";
        if (Utility.isGpsEnabled(this)) {
            locationCheck = "1";
        }

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("latitude", Double.parseDouble(sessionManager.getCurrentLat()));
            jsonObject.put("longitude", Double.parseDouble(sessionManager.getCurrentLng()));
            jsonObject.put("status", VariableConstant.ONLINE_STATUS);
            jsonObject.put("batteryPercentage", batteryLevel);
            jsonObject.put("locationHeading", "car");
            jsonObject.put("locationCheck", locationCheck);
            jsonObject.put("appVersion", VariableConstant.APP_VERSION);
            jsonObject.put("transit", transit);
            jsonObject.put("bookingStr", sessionManager.getBookingStr());

            if (sessionManager.getIsDriverOnJob()) {

                JSONObject mqttJson = new JSONObject(jsonObject.toString());
                mqttJson.put("pid", sessionManager.getProviderId());

                AppController.getInstance().getMqttHelper().publish(MqttEvents.LiveTrack.value + "/" + sessionManager.getProviderId(), mqttJson, 1, false);
            }

            //Log.d(TAG, "updateLocation: request "+jsonObject);
            service.getLocation(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM,jsonObject.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    break;
                                case SESSION_EXPIRED:
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()),  sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, LocationPublishService.this);
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, LocationPublishService.this);
                                    break;
                                default:
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void calculateRouteDistance() {

        if (prevLat == 0.0 || prevLng == 0.0) {
            prevLat = Double.parseDouble(sessionManager.getCurrentLat());
            prevLng = Double.parseDouble(sessionManager.getCurrentLng());
        }

        double curLat = Double.parseDouble(sessionManager.getCurrentLat());
        double curLong = Double.parseDouble(sessionManager.getCurrentLng());

        double dis = Utility.distanceInMeter(prevLat, prevLng, curLat, curLong);
        double distanceInMtr = 0.0;
        double disFromStaryPts = -1.0;

        if (strayLat != -1.0 && strayLng != -1.0) {
            disFromStaryPts = Utility.distanceInMeter(strayLat, strayLng, curLat, curLong);
        }

        if (((dis >= sessionManager.getLatLongDisplacement()) && (dis <= 300)) || (disFromStaryPts != -1.0 && ((disFromStaryPts >= sessionManager.getLatLongDisplacement()) && (disFromStaryPts <= 300)))) {

            transit = "1";

            strayLat = prevLat = curLat;
            strayLng = prevLng = curLong;

            distanceInMtr = (dis > disFromStaryPts) ? dis : disFromStaryPts;

            String bookingIdWithStatus[] = sessionManager.getBookingStr().split(",");

            for (int i = 0; i < bookingIdWithStatus.length; i++) {
                String splitBookingIdStatus[] = bookingIdWithStatus[i].split("\\|");
                if (splitBookingIdStatus.length >= 2) {
                    String bookingId = splitBookingIdStatus[0];
                    String status = splitBookingIdStatus[1];

                    if (status.equals(VariableConstant.ON_THE_WAY)) {
                        //Distance in Arrived screen
                        double distance = distanceInMtr + sessionManager.getArrivedDistance(bookingId);
                        sessionManager.setArrivedDistance(bookingId, "" + distance);

                    } else if (status.equals(VariableConstant.JOB_TIMER_STARTED)) {
                        //Distance between Event stated and event completed
                        double distance = distanceInMtr + sessionManager.getOnJobDistance(bookingId);
                        sessionManager.setOnJobDistance(bookingId, "" + distance);
                    }
                }
            }

        } else if (dis > 300 && (disFromStaryPts > 300 || disFromStaryPts == -1.0)) {
            strayLat = curLat;
            strayLng = curLong;
        }
    }

    private void updateLocationLogs() {
        try {
            JSONObject jsonObject= new JSONObject();
            jsonObject.put("latitude_longitude", sessionManager.getLocationArray());
            Log.d(TAG, "updateLocationLogs: session jsonArray " + sessionManager.getLocationArray());
            isLocationLogApiCalling = true;


            service.getLocationLogs(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM,jsonObject.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int statusCode = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null
                                ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                ? responseBodyResponse.errorBody().string() : null;
                            switch (statusCode) {
                                case SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()){
                                        isLocationLogDpUpdated = false;
                                    isLocationLogApiCalling = false;
                                    sessionManager.clearLocationArray();
                            }else {
                                isLocationLogApiCalling = false;
                            }
                                    break;

                                case SESSION_EXPIRED:
                                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()),  sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                            updateLocationLogs();
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            getInstance().toast(Utility.getMessage(msg));
                                            Utility.logoutSessionExiperd(sessionManager, LocationPublishService.this);
                                        }
                                    });
                                    break;
                                case SESSION_LOGOUT:
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    Utility.logoutSessionExiperd(sessionManager, LocationPublishService.this);
                                    break;
                                default:
                                    isLocationLogApiCalling = false;
                                    getInstance().toast(Utility.getMessage(errorBody));
                                    break;
                            }
                        } catch (Exception e) {
                            isLocationLogApiCalling = false;
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        isLocationLogApiCalling = false;
                    }

                    @Override
                    public void onComplete() {
                    }
                });


        } catch (Exception e) {
            isLocationLogApiCalling = false;
            e.printStackTrace();
        }
    }

    @Override
    public void updateLocation(Location location) {
        sessionManager.setCurrentLat(String.valueOf(location.getLatitude()));
        sessionManager.setCurrentLng(String.valueOf(location.getLongitude()));
    }

    @Override
    public void location_Error(String error) {

    }
}
