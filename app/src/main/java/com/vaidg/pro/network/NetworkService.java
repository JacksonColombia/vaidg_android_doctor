package com.vaidg.pro.network;

import io.reactivex.Observable;
import java.io.Serializable;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

/**
 * @NetworkService is the interface provide
 */
public interface NetworkService {
    @GET("provider/city")
    Observable<Response<ResponseBody>> getCity(@Header("lan") String lang,
                                               @Header("platform") String platform);

    @GET("provider/language")
    Observable<Response<ResponseBody>> getLanguage(@Header("lan") String lang,
                                               @Header("platform") String platform);

    @GET("provider/config")
    Observable<Response<ResponseBody>> getAppConfig(@Header("authorization") String auth,
                                                    @Header("lan") String lang,
                                                    @Header("platform") String platform);

    @GET("cognitoToken")
    Observable<Response<ResponseBody>> onTOGetCognitoToken(@Header("authorization") String auth,
                                                           @Header("lan") String lang,
                                                           @Header("platform") String platform);

    @GET("server/serverTime")
    Observable<Response<ResponseBody>> serverTime(@Header("lan") String lang,
                                                  @Header("platform") String platform);

    @Multipart
    @POST("imageUpload")
    Observable<Response<ResponseBody>> imageUpload(@Header("lan") String lang,
                                                   @Header("platform") String platform,
                                                   @PartMap Map<String, RequestBody> parts,
                                                   @Part MultipartBody.Part file);

    @GET("provider/bookings")
    Observable<Response<ResponseBody>> getBookings(@Header("authorization") String auth,
                                                   @Header("lan") String lang,
                                                   @Header("platform") String platform,
                                                   @Query("from") String from,
                                                   @Query("to") String to);

    @GET("provider/booking")
    Observable<Response<ResponseBody>> getBooking(@Header("authorization") String auth,
                                                  @Header("lan") String lang,
                                                  @Header("platform") String platform);

    @GET("provider/booking/id")
    Observable<Response<ResponseBody>> getBookingId(@Header("authorization") String auth,
                                                    @Header("lan") String lang,
                                                    @Header("platform") String platform,
                                                    @Query("bookingId") String bookingId);

    @GET("chatHistory")
    Observable<Response<ResponseBody>> getChatHistory(@Header("authorization") String auth,
                                                      @Header("lan") String lang,
                                                      @Header("platform") String platform,
                                                      @Query("bookingId") String bookingId,
                                                      @Query("pageNo") String pageNo);

    @GET("provider/wallet/transction")
    Observable<Response<ResponseBody>> getwalletTransction(@Header("authorization") String auth,
                                                           @Header("lan") String lang,
                                                           @Header("platform") String platform,
                                                           @Query("pageIndex") String pageIndex);

    @GET("provider/prescription")
    Observable<Response<ResponseBody>> getPrescription(@Header("authorization") String auth,
                                                       @Header("lan") String lang,
                                                       @Header("platform") String platform);

    @GET("provider/paymentsettings")
    Observable<Response<ResponseBody>> getPaymentsettings(@Header("authorization") String auth,
                                                          @Header("lan") String lang,
                                                          @Header("platform") String platform);

    @GET("provider/schedule")
    Observable<Response<ResponseBody>> getSchedule(@Header("authorization") String auth,
                                                   @Header("lan") String lang,
                                                   @Header("platform") String platform);

    @GET("provider/booking/chat")
    Observable<Response<ResponseBody>> getChatBooking(@Header("authorization") String auth,
                                                      @Header("lan") String lang,
                                                      @Header("platform") String platform);

    @GET("provider/schedule/month")
    Observable<Response<ResponseBody>> getScheduleMonth(@Header("authorization") String auth,
                                                        @Header("lan") String lang,
                                                        @Header("platform") String platform,
                                                        @Query("month") String month);

    @Headers("Content-Type: application/json")
    @POST("message")
    Observable<Response<ResponseBody>> sendChatMessage(@Header("authorization") String auth,
                                                       @Header("lan") String lang,
                                                       @Header("platform") String platform,
                                                       @Body String body);

    @Headers("Content-Type: application/json")
    @POST("provider/schedule")
    Observable<Response<ResponseBody>> addSchedule(@Header("authorization") String auth,
                                                   @Header("lan") String lang,
                                                   @Header("platform") String platform,
                                                   @Body String body);

    //  @DELETE("provider/schedule")
    @Headers("Content-Type: application/json")
    @HTTP(method = "DELETE", path = "provider/schedule", hasBody = true)
    Observable<Response<ResponseBody>> deleteSchedule(@Header("authorization") String auth,
                                                      @Header("lan") String lang,
                                                      @Header("platform") String platform,
                                                      @Body String body);

    @Headers("Content-Type: application/json")
    @POST("provider/reviewAndRating")
    Observable<Response<ResponseBody>> reviewAndRating(@Header("authorization") String auth,
                                                       @Header("lan") String lang,
                                                       @Header("platform") String platform,
                                                       @Body String body);

    @Headers("Content-Type: application/json")
    @POST("provider/medication")
    Observable<Response<ResponseBody>> addMedication(@Header("authorization") String auth,
                                                     @Header("lan") String lang,
                                                     @Header("platform") String platform,
                                                     @Body String body);

    @Headers("Content-Type: application/json")
    @PATCH("provider/cancelBooking")
    Observable<Response<ResponseBody>> cancelBooking(@Header("authorization") String auth,
                                                     @Header("lan") String lang,
                                                     @Header("platform") String platform,
                                                     @Body String body);
    @Headers("Content-Type: application/json")
    @PATCH("provider/bookingAck")
    Observable<Response<ResponseBody>> bookingAck(@Header("authorization") String auth,
                                                     @Header("lan") String lang,
                                                     @Header("platform") String platform,
                                                     @Body String body);

    @GET("provider/medication")
    Observable<Response<ResponseBody>> getMedication(@Header("authorization") String auth,
                                                     @Header("lan") String lang,
                                                     @Header("platform") String platform,
                                                     @Query("bookingId") String bookingId,
                                                     @Query("medicationId") String medicationId);

    @Headers("Content-Type: application/json")
    @PATCH("provider/medication")
    Observable<Response<ResponseBody>> editMedication(@Header("authorization") String auth,
                                                      @Header("lan") String lang,
                                                      @Header("platform") String platform,
                                                      @Body String body);

    @DELETE("provider/medication")
    Observable<Response<ResponseBody>> deleteMedication(@Header("authorization") String auth,
                                                        @Header("lan") String lang,
                                                        @Header("platform") String platform,
                                                        @Query("bookingId") String bookingId,
                                                        @Query("medicationId") String medicationId);

    @GET("provider/serviceCateogries")
    Observable<Response<ResponseBody>> getSpecializationCategory(@Header("lan") String lang,
                                                                 @Header("platform") String platform,
                                                                 @Query("cityId") String cityId);

    @GET("provider/hospital")
    Observable<Response<ResponseBody>> findHospital(@Header("lan") String lang,
                                                    @Header("platform") String platform,
                                                    @Query("cityId") String cityId,
                                                    @Query("search") String search,
                                                    @Query("limit") double limit,
                                                    @Query("skip") double skip);

    @GET("provider/notification")
    Observable<Response<ResponseBody>> getNotification(@Header("authorization") String auth,
                                                       @Header("lan") String lang,
                                                       @Header("platform") String platform,
                                                       @Query("limit") double limit,
                                                       @Query("skip") double skip);

    @GET("provider/degree")
    Observable<Response<ResponseBody>> getDegree(@Header("lan") String lang,
                                                 @Header("platform") String platform);

    @GET("provider/cancelReasons")
    Observable<Response<ResponseBody>> getCancelReasons(@Header("authorization") String auth,
                                                        @Header("lan") String lang,
                                                        @Header("platform") String platform,
                                                        @Query("bookingId") String bookingId);

    @POST("provider/emailValidation")
    Observable<Response<ResponseBody>> validateEmail(@Header("lan") String lang,
                                                     @Header("platform") String platform,
                                                     @Body Map<String, Object> body);

    @POST("provider/phoneValidation")
    Observable<Response<ResponseBody>> validatePhoneNumber(@Header("lan") String lang,
                                                           @Header("platform") String platform,
                                                           @Body Map<String, Object> body);

    @POST("provider/signUp")
    Observable<Response<ResponseBody>> signUp(@Header("authorization") String auth,
                                              @Header("lan") String lang,
                                              @Header("platform") String platform,
                                              @Body String body);

    @Headers("Content-Type: application/json")
    @POST("provider/rechargeWallet")
    Observable<Response<ResponseBody>> addRechargeWallet(@Header("authorization") String auth,
                                                         @Header("lan") String lang,
                                                         @Header("platform") String platform,
                                                         @Body String body);

    @Headers("Content-Type: application/json")
    @POST("provider/signIn")
    Observable<Response<ResponseBody>> signIn(@Header("lan") String lang,
                                              @Header("platform") String platform,
                                              @Body String body);

    @GET("provider/accessToken")
    Observable<Response<ResponseBody>> getAccessToken(@Header("authorization") String auth,
                                                      @Header("refreshToken") String refreshToken,
                                                      @Header("lan") String lang,
                                                      @Header("platform") String platform);

    @GET("v1/connectAccount")
    Observable<Response<ResponseBody>> getConnectAccount(@Header("authorization") String auth,
                                                         @Header("lan") String lang,
                                                         @Header("platform") String platform);

    @Headers("Content-Type: application/json")
    @PATCH("provider/status")
    Observable<Response<ResponseBody>> status(@Header("authorization") String auth,
                                              @Header("lan") String lang,
                                              @Header("platform") String platform,
                                              @Body String body);

    @Headers("Content-Type: application/json")
    @POST("contact")
    Observable<Response<ResponseBody>> createRazorPayAccount(@Header("authorization") String auth,
                                                         @Header("lan") String lang,
                                                         @Header("platform") String platform,
                                                         @Body String body);

    @Headers("Content-Type: application/json")
    @POST("fundAccount")
    Observable<Response<ResponseBody>> createRazorPayfundAccount(@Header("authorization") String auth,
                                                          @Header("lan") String lang,
                                                          @Header("platform") String platform,
                                                          @Body String body);
    @GET("contact")
    Observable<Response<ResponseBody>> getContact(@Header("authorization") String auth,
                                                         @Header("lan") String lang,
                                                         @Header("platform") String platform);

    @GET("fundAccount")
    Observable<Response<ResponseBody>> getFundAccount(@Header("authorization") String auth,
                                                         @Header("lan") String lang,
                                                         @Header("platform") String platform);

    @Headers("Content-Type: application/json")
    @POST("v1/connectAccount")
    Observable<Response<ResponseBody>> addConnectAccount(@Header("authorization") String auth,
                                                         @Header("lan") String lang,
                                                         @Header("platform") String platform,
                                                         @Body String body);

    @Headers("Content-Type: application/json")
    @POST("v1/externalAccount")
    Observable<Response<ResponseBody>> addExternalAccount(@Header("authorization") String auth,
                                                          @Header("lan") String lang,
                                                          @Header("platform") String platform,
                                                          @Body String body);

    @Headers("Content-Type: application/json")
    @PATCH("v1/externalAccount")
    Observable<Response<ResponseBody>> editExternalAccount(@Header("authorization") String auth,
                                                           @Header("lan") String lang,
                                                           @Header("platform") String platform,
                                                           @Body String body);

    @Headers("Content-Type: application/json")
    @DELETE("v1/externalAccount")
    Observable<Response<ResponseBody>> deleteExternalAccount(@Header("authorization") String auth,
                                                             @Header("lan") String lang,
                                                             @Header("platform") String platform,
                                                             @Body String body);

    @Headers("Content-Type: application/json")
    @PATCH("provider/bookingResponse")
    Observable<Response<ResponseBody>> getBookingResponse(@Header("authorization") String auth,
                                                          @Header("lan") String lang,
                                                          @Header("platform") String platform,
                                                          @Body String stringObjectMap);

    @Headers("Content-Type: application/json")
    @PATCH("provider/bookingStatus")
    Observable<Response<ResponseBody>> getBookingStatus(@Header("authorization") String auth,
                                                        @Header("lan") String lang,
                                                        @Header("platform") String platform,
                                                        @Body String stringObjectMap);

    @Headers("Content-Type: application/json")
    @PATCH("provider/bookingTimer")
    Observable<Response<ResponseBody>> getBookingTimer(@Header("authorization") String auth,
                                                       @Header("lan") String lang,
                                                       @Header("platform") String platform,
                                                       @Body String stringObjectMap);

    @Headers("Content-Type: application/json")
    @PATCH("provider/location")
    Observable<Response<ResponseBody>> getLocation(@Header("authorization") String auth,
                                                   @Header("lan") String lang,
                                                   @Header("platform") String platform,
                                                   @Body String stringObjectMap);

    @Headers("Content-Type: application/json")
    @PATCH("provider/locationLogs")
    Observable<Response<ResponseBody>> getLocationLogs(@Header("authorization") String auth,
                                                       @Header("lan") String lang,
                                                       @Header("platform") String platform,
                                                       @Body String stringObjectMap);

    @Headers("Content-Type: application/json")
    @PATCH("provider/cancelBooking")
    Observable<Response<ResponseBody>> getCancelBooking(@Header("authorization") String auth,
                                                        @Header("lan") String lang,
                                                        @Header("platform") String platform,
                                                        @Body String stringObjectMap);

    @GET("provider/profile/me")
    Observable<Response<ResponseBody>> getProfile(@Header("authorization") String auth,
                                                  @Header("lan") String lang,
                                                  @Header("platform") String platform);

    @Headers("Content-Type: application/json")
    @PATCH("provider/profile/me")
    Observable<Response<ResponseBody>> updateProfile(@Header("authorization") String auth,
                                                     @Header("lan") String lang,
                                                     @Header("platform") String platform,
                                                     @Body String body);

    @GET("provider/callTypeSetting")
    Observable<Response<ResponseBody>> getCallTypeSetting(@Header("authorization") String auth,
                                                          @Header("lan") String lang,
                                                          @Header("platform") String platform);


    @Headers("Content-Type: application/json")
    @PATCH("provider/callTypeSetting")
    Observable<Response<ResponseBody>> updateCallTypeSetting(@Header("authorization") String auth,
                                                          @Header("lan") String lang,
                                                          @Header("platform") String platform,
                                                          @Body String body);


    @POST("provider/forgotPassword")
    Observable<Response<ResponseBody>> forgotPassword(@Header("lan") String lang,
                                                      @Header("platform") String platform,
                                                      @Body Map<String, Object> stringObjectMap);

    @POST("provider/resendOtp")
    Observable<Response<ResponseBody>> resendOtp(@Header("lan") String lang,
                                                 @Header("platform") String platform,
                                                 @Body Map<String, Object> stringObjectMap);

    @POST("provider/verifyPhoneNumber")
    Observable<Response<ResponseBody>> verifyPhoneNumber(@Header("lan") String lang,
                                                         @Header("platform") String platform,
                                                         @Body Map<String, Object> stringObjectMap);

    @POST("provider/verifyVerificationCode")
    Observable<Response<ResponseBody>> verifyVerificationCode(@Header("lan") String lang,
                                                              @Header("platform") String platform,
                                                              @Body Map<String, Object> stringObjectMap);


    @GET("publickKey")
    Observable<Response<ResponseBody>> getPublicKey(@Header("authorization") String auth,
                                                    @Header("lan") String lang,
                                                    @Header("platform") String platform,
                                                    @Query("userType") String userType,
                                                    @Query("userId") String userId);


    @Headers("Content-Type: application/json")
    @PATCH("provider/password/me")
    Observable<Response<ResponseBody>> updatePassword(@Header("authorization") String auth,
                                                      @Header("lan") String lang,
                                                      @Header("platform") String platform,
                                                      @Body Map<String, Object> stringObjectMap);

    @Headers("Content-Type: application/json")
    @PATCH("provider/password")
    Observable<Response<ResponseBody>> resetPassword(@Header("lan") String lang,
                                                     @Header("platform") String platform,
                                                     @Body Map<String, Object> stringObjectMap);

    @Headers("Content-Type: application/json")
    @PATCH("provider/email")
    Observable<Response<ResponseBody>> changeEmail(@Header("authorization") String auth,
                                                   @Header("lan") String lang,
                                                   @Header("platform") String platform,
                                                   @Body Map<String, Object> stringObjectMap);

    @Headers("Content-Type: application/json")
    @PATCH("provider/phoneNumber")
    Observable<Response<ResponseBody>> changePhoneNumber(@Header("authorization") String auth,
                                                         @Header("lan") String lang,
                                                         @Header("platform") String platform,
                                                         @Body Map<String, Object> stringObjectMap);


    @POST("provider/logout")
    Observable<Response<ResponseBody>> logout(@Header("authorization") String auth,
                                              @Header("lan") String lang,
                                              @Header("platform") String platform);

    @GET("provider/bookingHistoryByWeek")
    Observable<Response<ResponseBody>> bookingHistoryByWeek(@Header("authorization") String auth,
                                                            @Header("lan") String lang,
                                                            @Header("platform") String platform);

    @GET("provider/bookingHistory")
    Observable<Response<ResponseBody>> bookingHistory(@Header("authorization") String auth,
                                                      @Header("lan") String lang,
                                                      @Header("platform") String platform,
                                                      @Query("startDate") String startDate);

    @GET("provider/support")
    Observable<Response<ResponseBody>> support(@Header("authorization") String auth,
                                               @Header("lan") String lang,
                                               @Header("platform") String platform,
                                               @Query("userType") Integer userType);

  @GET("zendesk/user/ticket")
    Observable<Response<ResponseBody>> getTicket(@Header("authorization") String auth,
                                               @Header("lan") String lang,
                                               @Header("platform") String platform,
                                               @Query("emailId") String emailId);
 @GET("zendesk/ticket/history")
    Observable<Response<ResponseBody>> getTicketHistory(@Header("authorization") String auth,
                                               @Header("lan") String lang,
                                               @Header("platform") String platform,
                                               @Query("id") String id);

    @Headers("Content-Type: application/json")
    @POST("zendesk/ticket")
    Observable<Response<ResponseBody>> postTicket(@Header("authorization") String auth,
                                                         @Header("lan") String lang,
                                                         @Header("platform") String platform,
                                                         @Body String stringObjectMap);
  @Headers("Content-Type: application/json")
    @POST("zendesk/ticket/comments")
    Observable<Response<ResponseBody>> putTicket(@Header("authorization") String auth,
                                                         @Header("lan") String lang,
                                                         @Header("platform") String platform,
                                                         @Body String stringObjectMap);

    @Headers("Content-Type: application/json")
    @POST("meet/call")
    Observable<Response<ResponseBody>> calling(@Header("authorization") String auth,
                                               @Header("lan") String lang,
                                               @Header("platform") String platform,@Body String objectMap);

}
