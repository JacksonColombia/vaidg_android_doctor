package com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.content.FileProvider;

import com.vaidg.pro.BuildConfig;
import com.vaidg.pro.R;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection.model.MediaSelectItemCallBack;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection.model.MediaSelectModel;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.Attribute;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.MediaBottomSelector;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.fileUtil.AppFileManger;
import com.videocompressor.com.CompressImage;
import com.videocompressor.com.VideoCompressor;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_PICTURE_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_VIDEO_UPLOAD;

/**
 * <h1>@MediaSelectPresenter</h1>
 * <p>this is presenter used for business logic
 * used to compress the file
 * used to upload the file in aws3
 * </p>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public class MediaSelectPresenter implements MediaSelectContract.Presenter, MediaSelectItemCallBack, App_permission.Permission_Callback, MediaBottomSelector.Callback {

    private static final String TAG = MediaSelectPresenter.class.getSimpleName();
    private final String GALLERY = "gallery";
    private final String CAMERA = "camera";

    @Inject
    MediaSelectModel model;
    @Inject
    MediaBottomSelector mediaBottomSelector;
    @Inject
    App_permission app_permission;
    @Inject
    Activity activity;
    @Inject
    Utility utility;
    @Inject
    AppFileManger appFileManger;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    VideoCompressor videoCompressor;
    @Inject
    CompressImage compressImage;

    private int type;    //12 Picture, 13 Video
    private MediaSelectContract.View view;
    private MediaSelectFragment mediaSelectFragment;
    private File temp_file = null;

    @Inject
    public MediaSelectPresenter() {
    }

    @Override
    public void attachView(MediaSelectContract.View view) {
        this.view = view;
        this.mediaSelectFragment = (MediaSelectFragment) view;
    }

    @Override
    public void detachView() {
        view = null;
    }

//    @Override
//    public void onItemSelected(int position, int Type) {
//        view.launchViewer(model.getMedia(position));
//    }

    @Override
    public void onCancel(int position, int Type) {
        model.deleteMediaWith(position);
        view.notifyAdapter();
    }

    @Override
    public void openChooser(int type) {
        this.type = type;
        temp_file = null;
        mediaBottomSelector.showBottomSheet(this);
    }

    @Override
    public void onCamera() {
        ArrayList<App_permission.Permission> permissions = new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.CAMERA);
        app_permission.getPermission_for_Sup_v4Fragment(CAMERA, permissions, mediaSelectFragment, this);

    }

    @Override
    public void onGallery() {
        ArrayList<App_permission.Permission> permissions = new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
        app_permission.getPermission_for_Sup_v4Fragment(GALLERY, permissions, mediaSelectFragment, this);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        if (isAllGranted && tag.equals(CAMERA)) {
            if (view != null) {
                try {
                    if (type == SPECIALIZATION_VIEW_VIDEO_UPLOAD)
                        temp_file = appFileManger.getVideoFile();
                    else
                        temp_file = appFileManger.getImageFile();

                    view.openCamera(utility.getUri_Path(temp_file), type);
                } catch (Exception e) {
                    view.showError(e.getMessage());
                }
            }
        } else if (isAllGranted && tag.equals(GALLERY)) {
            if (view != null)
                view.openGallery(type);
        }
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        if (tag.equals(GALLERY)) {
            app_permission.show_Alert_Permission(activity.getString(R.string.photo_access_text), activity.getString(R.string.gallery_acess_subtitle),
                    activity.getString(R.string.gallery_acess_message), stringArray);
        } else if (tag.equals(CAMERA)) {
            app_permission.show_Alert_Permission(activity.getString(R.string.camera_access_text), activity.getString(R.string.camera_acess_subtitle),
                    activity.getString(R.string.camera_acess_message), stringArray);
        }
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean permanent) {
        if (permanent) {
            if (tag.equals(GALLERY)) {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.photo_denied_text), activity.getString(R.string.gallery_denied_subtitle),
                        activity.getString(R.string.gallery_denied_message));
            } else if (tag.equals(CAMERA)) {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.camera_denied_text), activity.getString(R.string.camera_denied_subtitle),
                        activity.getString(R.string.camera_denied_message));
            }
        }
    }

    @Override
    public boolean isValidMediaSize() {
        boolean valid = false;
        if (type != SPECIALIZATION_VIEW_VIDEO_UPLOAD)
            return true;
        if (temp_file != null) {
            valid = utility.isValidVideoSize(temp_file.length());
        }
        if (!valid) {
            if (view != null)
                view.showError(activity.getString(R.string.profile_size_limit_msg));
            model.deleteFile(temp_file);
        }
        return valid;
    }

    @Override
    public boolean isValidMediaSize(String filePath) {
        boolean valid = false;
        if (type != SPECIALIZATION_VIEW_VIDEO_UPLOAD)
            return true;
        try {
            File tempFile = new File(filePath);
            valid = utility.isValidVideoSize(tempFile.length());
            if (!valid) {
                if (view != null)
                    view.showError(activity.getString(R.string.profile_size_limit_msg));
                model.deleteFile(tempFile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valid;
    }

    @Override
    public void upDateToGallery() {
        if (temp_file == null)
            return;
      /*  Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(temp_file.getPath());
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);*/
        //Log.d(TAG, "upDateToGallery: 2"+temp_file.getPath()+ "=====>\n"+temp_file.getAbsolutePath());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            final Uri contentUri = Uri.fromFile(temp_file);
            scanIntent.setData(contentUri);
            activity.sendBroadcast(scanIntent);
        } else {
            activity.sendBroadcast(
                    new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(temp_file.getAbsolutePath())));
        }
    }

    @Override
    public String getRecentTemp() {
        return temp_file.getPath();
    }

    @Override
    public void addMedia(File mFileTemp) {
        model.addFilePathToAdapter(mFileTemp.getPath());
        view.notifyAdapter();
    }

    @Override
    public void saveData(Attribute attribute) {
        model.saveData(attribute);
    }

    @Override
    public void isUpload() {
        if (model.isUploaded() || !view.isMandatoryForProvider())
            view.moveNextFrag();
        else
            view.showError(activity.getString(R.string.media_upload_error));
    }

}
