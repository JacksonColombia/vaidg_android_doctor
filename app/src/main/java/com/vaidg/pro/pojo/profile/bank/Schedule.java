package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Schedule implements Serializable {

	@SerializedName("interval")
	@Expose
	private String interval;

	@SerializedName("delay_days")
	@Expose
	private int delayDays;

	public String getInterval(){
		return interval;
	}

	public int getDelayDays(){
		return delayDays;
	}

	@Override
 	public String toString(){
		return 
			"Schedule{" + 
			"interval = '" + interval + '\'' + 
			",delay_days = '" + delayDays + '\'' + 
			"}";
		}
}