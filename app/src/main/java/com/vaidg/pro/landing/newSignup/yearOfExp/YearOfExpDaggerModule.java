package com.vaidg.pro.landing.newSignup.yearOfExp;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>YearOfExpDaggerModule</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 28/04/2020.
 **/
@Module
public abstract class YearOfExpDaggerModule {

    @ActivityScoped
    @Binds
    abstract YearOfExpContract.Presenter providePresenter(YearOfExpPresenter presenter);

    @ActivityScoped
    @Binds
    abstract YearOfExpContract.View provideView(YearOfExpActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(YearOfExpActivity activity);
}
