package com.vaidg.pro.helpwithpassword;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Patterns;

import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.phonevalidation.PhoneValidationPojo;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

/**
 * <h1>HelpWithPasswordPresenterImple</h1>
 * <p>This class is contain override method of presenter with business logic.</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 23/06/2020
 **/
public class HelpWithPasswordPresenterImple implements HelpWithPasswordContract.Presenter {

    @Inject
    HelpWithPasswordContract.View view;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;
    @Inject
    Activity activity;
    @Inject
    NetworkService service;
    @Inject
    NetworkStateHolder networkStateHolder;

    @Inject
    HelpWithPasswordPresenterImple() {
    }

    @Override
    public void forgotPassword(String countryCode, String emailPhone, String type) {
        Utility.hideKeyboad(activity);
        if (type.equals("1")) {
            if (!verifyPhone(countryCode, emailPhone)) {
                return;
            }
        } else {
            if (!verifyEmail(emailPhone)) {
                return;
            }
        }

        if (networkStateHolder.isConnected()) {
            if (view != null)
            view.showProgress();
            HashMap<String, Object> params = new HashMap<>();
            params.put("emailOrPhone", emailPhone.replaceAll(" ", ""));
            params.put("countryCode", countryCode);
            params.put("userType", VariableConstant.USER_TYPE);
            params.put("type", type);
            Utility.basicAuth(service).forgotPassword(DEFAULT_LANGUAGE, PLATFORM, params)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                if (statusCode == SUCCESS_RESPONSE) {
                                    if (view != null)
                                    view.hideProgress();
                                    if (response != null && !response.isEmpty()) {
                                        PhoneValidationPojo phoneValidationPojo = gson.fromJson(response, PhoneValidationPojo.class);
                                        if (type.equals("1")) {
                                            if (view != null)
                                            view.onSuccessPhone(phoneValidationPojo);
                                        } else {
                                            if (view != null)
                                            view.onSuccessEmail(phoneValidationPojo.getMessage());
                                        }
                                    } else {
                                        if (view != null)
                                            view.showError(activity.getString(R.string.serverError));
                                    }
                                } else {
                                    if (view != null) {
                                        view.hideProgress();
                                        view.showError(Utility.getMessage(errorBody));
                                    }
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showError(activity.getString(R.string.serverError));
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.showError(activity.getString(R.string.serverError));
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public boolean verifyPhone(String phone, String countryCode) {
        if (!view.isPhoneNumberCorrect()) {
            view.onPhoneNumberInCorrect(Utility.isTextEmpty(phone) ? R.string.enterPhone : R.string.invalidPhoneNumber);
            return false;
        } else {
            view.onPhoneNumberCorrect();
            return true;
        }
    }

    @Override
    public boolean verifyEmail(String email) {
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            view.onEmailInCorrect();
            return false;
        } else {
            view.onEmailCorrect();
            return true;
        }
    }


    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }
}
