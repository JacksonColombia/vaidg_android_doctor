package com.vaidg.pro.main.acceptedbooking;

import com.vaidg.pro.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>LogInDaggerModule</h1>
 * <p>This dagger module created for LogInActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
@Module
public abstract class AcceptedBookingDaggerModule {

    @FragmentScoped
    @Binds
    abstract AcceptedBookingContract.Presenter getPresenter(AcceptedBookingPresenter presenter);

  /*  @FragmentScoped
    @Binds
    abstract AcceptedBookingContract.View provideView(AcceptedBookingFragment fragment);
*/
}
