package com.vaidg.pro.main.schedule.viewschedule;

import com.github.sundeepk.compactcalendarview.domain.Event;
import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.shedule.ScheduleData;
import com.vaidg.pro.pojo.shedule.ScheduleMonthData;
import com.vaidg.pro.pojo.shedule.ScheduleMonthPojo;
import com.vaidg.pro.pojo.shedule.Slot;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface ScheculeViewContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onFailure(String msg);

    void onFailure();

    void onSuccess(ScheduleData data);


  }

  interface Presenter extends BasePresenter {

    /**
     * method for calling api for getting the view schedule
     *
     */
    void getShedule();

  }
}