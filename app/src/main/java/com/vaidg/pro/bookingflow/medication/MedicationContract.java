package com.vaidg.pro.bookingflow.medication;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.pojo.medication.MedicationData;
import java.util.ArrayList;
import org.json.JSONObject;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface MedicationContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onFailure(String failureMsg);

    void onFailure();

    void onNewToken(String newToken);

    void sessionExpired(String msg);

    void onSuccess(String msg, boolean bookingDetailsRefreshed);

    void onSuccessResonse(ArrayList<MedicationData> data);
  }

  interface Presenter extends BasePresenter {
    /**
     * Method for calling api for update the job status in ArrivedActivity and OnTheWayActivity
     *
     * @param object required field in json object
     */
    void updateStaus(JSONObject object, final boolean isTelecallBooking);

    String getBookingStr(Booking booking, String oldStr, String updatedStatus);

    void getMedication(String bookidId, String medicationId);

    void delete(String bookidId, String medicationId);
  }
}