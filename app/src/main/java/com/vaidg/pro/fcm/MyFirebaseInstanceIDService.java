package com.vaidg.pro.fcm;

import android.content.Intent;
import android.util.Log;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.vaidg.pro.utility.SessionManager;
import org.jetbrains.annotations.NotNull;

/**
 * Created by murshid on 18/9/16.
 * <h2>MyFirebaseInstanceIDService</h2>
 * MyFirebaseInstanceIDService is used for generating the sessiontoken
 */
public class MyFirebaseInstanceIDService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseIIDService";
    // private SharedPrefs sharedPrefs;
    SessionManager sessionManager;

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onNewToken(@NotNull String token) {
        super.onNewToken(token);
        //  Log.e("NEW_TOKEN", refreshedToken);
        // Get updated InstanceID token.
        //   sharedPrefs = new SharedPrefs(this);

        sessionManager = SessionManager.getSessionManager(this);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {

                String refreshedToken = instanceIdResult.getToken();
                Log.d(TAG, "Refreshed token: " + refreshedToken);

                // If you want to send messages to this application instance or
                // manage this apps subscriptions on the server side, send the
                // Instance ID token to your app server.
                sendRegistrationToServer(refreshedToken);

            }
        });

        // Get updated InstanceID token.
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

        //TODO: Send Token to Server
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        // Check if message contains a data payload.
    }
 /*   @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
     //   sharedPrefs = new SharedPrefs(this);
        manager = SessionManager.getInstance(this);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }*/

    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token)
    {
        // TODO: Implement this method to send token to your app server.
        sessionManager.setPushToken(token);
        //  Log.i("Regist", "GCMTOKEN: " + sharedPrefs.getRegistrationId());

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "IDServiceonDestroy: ");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e(TAG, "IDServiceonTaskRemoved: " );
        //Code here
        stopSelf();
    }
}

