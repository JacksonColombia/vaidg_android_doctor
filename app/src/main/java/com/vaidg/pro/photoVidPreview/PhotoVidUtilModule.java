package com.vaidg.pro.photoVidPreview;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.pojo.profile.ImageData;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class PhotoVidUtilModule {

    @ActivityScoped
    @Provides
    ArrayList<ImageData> photoList() {
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    PhotoPagerAdapter photoPagerAdapter(Activity activity, ArrayList<ImageData> photoList) {
        return new PhotoPagerAdapter(activity, photoList);
    }

}
