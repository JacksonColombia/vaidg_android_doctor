package com.vaidg.pro.landing.newSignup.selectSpecialization.model;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ItemSpecializationSelectionBinding;
import com.vaidg.pro.landing.newSignup.selectCity.model.CitySelectionListener;
import com.vaidg.pro.landing.newSignup.selectSpecialization.pojo.SelectSpecializationData;
import com.vaidg.pro.pojo.signup.CityData;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * <h1>@CitySelectionAdapter</h1>
 * <p>CitySelectionAdapter adapter to list out the citys</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 25/04/2020.
 **/
public class SpecializationSelectionAdapter extends RecyclerView.Adapter<SpecializationSelectionAdapter.SpecializationSelectionViewHolder> {

    private Context context;
    ArrayList<SelectSpecializationData> specializationData;
    private CitySelectionListener callBack;
    private long mLastClickTime = System.currentTimeMillis();
    private static final long CLICK_TIME_INTERVAL = 300;

    public SpecializationSelectionAdapter(ArrayList<SelectSpecializationData> specializationData) {
        this.specializationData = specializationData;
    }

    public void provideCallBack(CitySelectionListener callBack) {
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public SpecializationSelectionAdapter.SpecializationSelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        ItemSpecializationSelectionBinding binding = ItemSpecializationSelectionBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new SpecializationSelectionAdapter.SpecializationSelectionViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SpecializationSelectionAdapter.SpecializationSelectionViewHolder holder, int position) {
        bindSpecializationData(holder, position);
    }

    private void bindSpecializationData(SpecializationSelectionViewHolder holder, int position) {
        holder.binding.tvName.setText(specializationData.get(position).getCatName());
        if (specializationData.get(position).isSelected()) {
            holder.binding.ivTick.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_checked));
            Utility.setTextColor(context, holder.binding.tvName, R.color.lighter_blue);
        } else {
            holder.binding.ivTick.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_unselected));
            Utility.setTextColor(context, holder.binding.tvName, R.color.darkTextColor);
        }
    }

    @Override
    public int getItemCount() {
        return specializationData != null ? specializationData.size() : ZERO;
    }

    class SpecializationSelectionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemSpecializationSelectionBinding binding;

        SpecializationSelectionViewHolder(@NonNull ItemSpecializationSelectionBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
            binding.llSpecializationSelection.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (callBack != null) {
                long now = System.currentTimeMillis();
                if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
                    return;
                }
                mLastClickTime = now;
                new Handler().postDelayed(() -> callBack.onItemSelected(getAdapterPosition(),specializationData.get(getAdapterPosition()).getCatName()), 250);
            }
        }
    }
    // method for filtering our recyclerview items.
    public void filterList(ArrayList<SelectSpecializationData> filterllist) {
        // below line is to add our filtered
        // list in our course array list.
        specializationData = filterllist;
        // below line is to notify our adapter
        // as change in recycler view data.
        notifyDataSetChanged();
    }
}
