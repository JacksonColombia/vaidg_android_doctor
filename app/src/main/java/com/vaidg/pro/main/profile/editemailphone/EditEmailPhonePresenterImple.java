package com.vaidg.pro.main.profile.editemailphone;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Patterns;

import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.NetworkStateHolder;
import com.vaidg.pro.pojo.phonevalidation.PhoneValidationPojo;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.PRECONDITION_FAILED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;
import static com.vaidg.pro.utility.VariableConstant.USER_TYPE;

public class EditEmailPhonePresenterImple implements EditEmailPhoneContract.Presenter {

    @Inject
    Activity activity;
    @Inject
    EditEmailPhoneContract.View view;
    @Inject
    NetworkService service;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    SessionManager sessionManager;
    @Inject
    Gson gson;

    @Inject
    public EditEmailPhonePresenterImple() {
    }

    @Override
    public boolean isEmailAddressValid(String email) {
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            view.onEmailIncorrect(activity.getString(R.string.invalidEmail));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isPhoneNumberValid(String countryCode, String phoneNumber) {
        if (!view.isPhoneNumberCorrect()) {
            view.onPhoneNumberIncorrect(activity.getString(Utility.isTextEmpty(phoneNumber) ? R.string.enterPhone : R.string.invalidPhoneNumber));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void changeEmail(String emailAddress) {
        if (!isEmailAddressValid(emailAddress)) {
            return;
        }

        if (networkStateHolder.isConnected()) {
            if (view != null)
            view.showProgress();
            Map<String, Object> params = new HashMap<>();
            params.put("userType", USER_TYPE);
            params.put("email", emailAddress);
            service.changeEmail(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, params)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        if (view != null)
                                        view.hideProgress();
                                        if (response != null && !response.isEmpty()) {
                                            if (view != null)
                                            view.onSuccessEmailChange(Utility.getMessage(response));
                                        } else {
                                            if (view != null)
                                                view.onFailure();
                                        }
                                        break;
                                    case PRECONDITION_FAILED:
                                        if (view != null) {
                                            view.hideProgress();
                                            view.onEmailIncorrect(Utility.getMessage(errorBody));
                                        }
                                        break;
                                    case SESSION_EXPIRED:
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                changeEmail(emailAddress);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                if (view != null) {
                                                    view.hideProgress();
                                                    view.onFailure();
                                                }
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, activity);
                                            }
                                        });
                                        break;
                                    case SESSION_LOGOUT:
                                        if (view != null)
                                        view.hideProgress();
                                        Utility.logoutSessionExiperd(sessionManager, activity);
                                        break;
                                    default:
                                        if (view != null) {
                                            view.hideProgress();
                                            view.showError(Utility.getMessage(errorBody));
                                        }
                                        break;
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.onFailure();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.onFailure();
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void changePhoneNumber(String countryCode, String phoneNumber) {
        if (!isPhoneNumberValid(countryCode, phoneNumber)) {
            return;
        }

        if (networkStateHolder.isConnected()) {
            view.showProgress();
            Map<String, Object> params = new HashMap<>();
            params.put("userType", USER_TYPE);
            params.put("countryCode", countryCode);
            params.put("phone", phoneNumber);
            service.changePhoneNumber(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM, params)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {
                            int statusCode = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;
                                switch (statusCode) {
                                    case SUCCESS_RESPONSE:
                                        if (view != null)
                                        view.hideProgress();
                                        if (response != null && !response.isEmpty()) {
                                            PhoneValidationPojo phoneValidationPojo = gson.fromJson(response, PhoneValidationPojo.class);
                                            if (view != null)
                                            view.onSuccessPhoneNoChange(phoneValidationPojo);
                                        } else {
                                            if (view != null)
                                                view.onFailure();
                                        }
                                        break;
                                    case SESSION_EXPIRED:
                                        onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                                                changePhoneNumber(countryCode, phoneNumber);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                if (view != null) {
                                                    view.hideProgress();
                                                    view.onFailure();
                                                }
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                if (view != null)
                                                    view.hideProgress();
                                                getInstance().toast(Utility.getMessage(msg));
                                                Utility.logoutSessionExiperd(sessionManager, activity);
                                            }
                                        });
                                        break;
                                    case SESSION_LOGOUT:
                                        if (view != null)
                                        view.hideProgress();
                                        Utility.logoutSessionExiperd(sessionManager, activity);
                                        break;
                                    default:
                                        if (view != null) {
                                            view.hideProgress();
                                            view.showError(Utility.getMessage(errorBody));
                                        }
                                        break;
                                }
                            } catch (Exception e) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.onFailure();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.hideProgress();
                                view.onFailure();
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }
}
