package com.vaidg.pro.main;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.vaidg.pro.AppController.getContext;
import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.MESSAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Intent;
import android.os.Build;
import android.util.Log;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.main.chats.ChattingActivity;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.network.ServiceFactory;
import com.vaidg.pro.pojo.JitsiMeet;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import okhttp3.ResponseBody;
import org.jitsi.meet.sdk.JitsiMeetActivity;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

public class TestActivity extends AppCompatActivity  implements Serializable {
  SessionManager sessionManager;
  String token = "";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_test);
/*
    try{
    JitsiMeetConferenceOptions options
        = new JitsiMeetConferenceOptions.Builder()
        .setToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImFwcHNjcmlwIn0.eyJjb250ZXh0Ijp7InVzZXIiOnsiYXZhdGFyIjoiaHR0cHM6Ly9zZXJ2aWNlZ2VuaWUuczMuYXAtc291dGgtMS5hbWF6b25hd3MuY29tL2lzZXJ2ZTIuMC9Qcm92aWRlci9Xb3JrSW1hZ2UvSU1BR0VfMjAyMDA5MDhfMTUzNDMwLmpwZyIsIm5hbWUiOiJEciBCYXB1IEdhbmRoaSIsImVtYWlsIjoiYmFwdUBtYWlsLmNvbSIsImlkIjoiNWYxZTg1ZDRjMzA3ZmI4NjE5ZWU5MjJmIiwidXNlclR5cGUiOiJkb2N0b3IifX0sImF1ZCI6ImRvY3RvciIsImlzcyI6IlZhaWRHIiwic3ViIjoiZG9jdG9yLmFwcHNjcmlwLmNvIiwicm9vbSI6IjE1OTk4MDU1NjAxNzkiLCJleHAiOjE1OTk4NDE5ODAsIm1vZGVyYXRvciI6ZmFsc2UsImlhdCI6MTU5OTgyNzY4OX0.NwpT83w-uG_Rm3lNoCJLEaoYFJXQRDgpFyMesQ3zGtQ")
        .setServerURL(new URL("https://doctor.vaidg.com"))
        .setRoom("1599805560179")
        .setWelcomePageEnabled(false)
        .build();

    JitsiMeetActivity.OnEventCallBack  eventCallBack = new JitsiMeetActivity.OnEventCallBack() {
      @Override
      public void onChatClick(Map<String, Object> map) {
        if(map != null && map.containsValue("true"))
        {
          //  Log.w(TAG, "onConferenceJoined: 1");
          Intent intent = new Intent(JitsiMeetActivity.mContext, ChattingActivity.class);
          intent.putExtra("isFromCall",true);
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
          }
          JitsiMeetActivity.mContext.startActivity(intent);
        }

      }

      @Override
      public void onConferenceJoined(Map<String, Object> map) {
        if(map != null) {
          // Log.w(TAG, "onConferenceJoined: 2");

        }
      }

      @Override
      public void onConferenceWillJoin(Map<String, Object> map) {
        if(map != null) {
          //Log.w(TAG, "onConferenceJoined: 3");

        }
      }

      @Override
      public void onConferenceTerminated(Map<String, Object> map) {
        if(map != null) {
          // Log.w(TAG, "onConferenceJoined: 4");

        }
      }
    };
    // Launch the new activity with the given options. The launch() method takes care
    // of creating the required Intent and passing the options.
    JitsiMeetActivity.launch(getApplicationContext(), options,eventCallBack);
  } catch (MalformedURLException e) {
    e.printStackTrace();
  }
*/
    sessionManager = SessionManager.getSessionManager(this);
    getCaalingid(Long.parseLong("1599805560179"));
  }

  JitsiMeetActivity.OnEventCallBack eventCallBack = new JitsiMeetActivity.OnEventCallBack() {
    @Override
    public void onChatClick(Map<String, Object> map) {
      if (map != null && map.containsValue("true")) {
        // Log.w(TAG, "onConferenceJoined: 1");
        Intent intent = new Intent(JitsiMeetActivity.mContext, ChattingActivity.class);
        intent.putExtra("isFromCall", true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
          intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        }
        JitsiMeetActivity.mContext.startActivity(intent);
      }
    }

    @Override
    public void onConferenceJoined(Map<String, Object> map) {
      if (map != null) {
        // Log.w(TAG, "onConferenceJoined: 2");
      }
    }

    @Override
    public void onConferenceWillJoin(Map<String, Object> map) {
      if (map != null) {
        //Log.w(TAG, "onConferenceJoined: 3");
      }
    }

    @Override
    public void onConferenceTerminated(Map<String, Object> map) {
      if (map != null) {
        // Log.w(TAG, "onConferenceJoined: 4");
      }
    }
  };

  private void getCaalingid(long bid) {
    JSONObject jsonParams = new JSONObject();
    try {
      jsonParams.put("bookingId", String.valueOf(bid));
      NetworkService service = ServiceFactory.getClient(NetworkService.class, getApplicationContext());
      Observable<Response<ResponseBody>> obsResponseBody = service.calling(Utility.getAuth(sessionManager.getEmail()),
          DEFAULT_LANGUAGE, PLATFORM, jsonParams.toString());
      obsResponseBody.subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(new Observer<Response<ResponseBody>>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Response<ResponseBody> responseBodyResponse) {
              int code = responseBodyResponse.code();
              // JSONObject jsonObject;
              try {
              /*   String response =
                    responseBodyResponse.body() != null ? responseBodyResponse.body().string()
                        : null;
                String errorBody =
                    responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string()
                        : null;*/
                // Log.w("TAG", "onNextResponse: "+responseBodyResponse.body().string() );
                switch (code) {
                  case SUCCESS_RESPONSE:
                    //jsonObject = new JSONObject(response);
                    // JSONObject data = jsonObject.getJSONObject("data");
                 /*   JitsiMeet jitsiMeet = new Gson().fromJson(response,JitsiMeet.class);
                    JitsiMeet.Data data = jitsiMeet.getData();
*/
                    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImFwcHNjcmlwIn0.eyJjb250ZXh0Ijp7InVzZXIiOnsiYXZhdGFyIjoiaHR0cHM6Ly9zZXJ2aWNlZ2VuaWUuczMuYXAtc291dGgtMS5hbWF6b25hd3MuY29tL2lzZXJ2ZTIuMC9Qcm92aWRlci9Xb3JrSW1hZ2UvSU1BR0VfMjAyMDA5MDhfMTUzNDMwLmpwZyIsIm5hbWUiOiJEciBCYXB1IEdhbmRoaSIsImVtYWlsIjoiYmFwdUBtYWlsLmNvbSIsImlkIjoiNWYxZTg1ZDRjMzA3ZmI4NjE5ZWU5MjJmIiwidXNlclR5cGUiOiJkb2N0b3IifX0sImF1ZCI6ImRvY3RvciIsImlzcyI6IlZhaWRHIiwic3ViIjoiZG9jdG9yLmFwcHNjcmlwLmNvIiwicm9vbSI6IjE1OTk4MDU1NjAxNzkiLCJleHAiOjE1OTk4NDE5ODAsIm1vZGVyYXRvciI6ZmFsc2UsImlhdCI6MTU5OTgyNzY4OX0.NwpT83w-uG_Rm3lNoCJLEaoYFJXQRDgpFyMesQ3zGtQ";//data.getString("token");
                    try {
                      JitsiMeetConferenceOptions options
                          = new JitsiMeetConferenceOptions.Builder()
                          .setToken(token)
                          .setServerURL(new URL("https://doctor.vaidg.com"))
                          .setRoom(String.valueOf(bid))
                          .setWelcomePageEnabled(false)
                          .build();
                      // Launch the new activity with the given options. The launch() method takes care
                      // of creating the required Intent and passing the options.
                      JitsiMeetActivity.launch(TestActivity.this, options, eventCallBack);
                    } catch (MalformedURLException e) {
                      e.printStackTrace();
                    }
                    break;
                  case SESSION_EXPIRED:
                    //  if (errorBody != null && !errorBody.isEmpty()) {
                    onRefreshToken(Utility.getAuth(sessionManager.getEmail()), sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                      @Override
                      public void onSuccessRefreshToken(String newToken) {
                        getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                        getCaalingid(bid);
                      }

                      @Override
                      public void onFailureRefreshToken() {
                      }

                      @Override
                      public void sessionExpired(String msg) {
                        getInstance().toast(Utility.getMessage(msg));
                        Utility.logoutSessionExiperd(sessionManager, getContext());
                      }
                    });
                    //  }
                    break;
                  default:
/*
                    if (errorBody != null && !errorBody.isEmpty()) {
                      jsonObject = new JSONObject(errorBody);
                      jsonObject.getString(MESSAGE);
                      if (!jsonObject.getString(
                          MESSAGE).isEmpty()) {
                      }
                    }
*/
                    break;
                }
              } catch (Exception e) {
                e.printStackTrace();
              }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
          });
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }
}
