package com.vaidg.pro.main.schedule.viewschedulelist;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.shedule.ScheduleData;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface ScheculeViewListContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onFailure(String msg);

    void onFailure();

    void onSuccessDeleteSchedule(String msg);

  }

  interface Presenter extends BasePresenter<View> {


    /**
     * method for calling the api for deleting the schedule based on the address id
     *
     * @param id           address id
     */
    void deleteSchedule(String id);

  }
}