package com.videocompressor.com;

import com.videocompressor.com.DataModel.CompressedData;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * @author Suresh.
 * @version 1.0.
 * @since 3/15/2018.
 */
public class RxCompressObservable extends Observable<CompressedData> {
    private static RxCompressObservable rxJava2Observable = null;
    private Observer<? super CompressedData> observer;
    private CompressedData lastData;

    private RxCompressObservable() {
    }

    static RxCompressObservable getInstance() {
        if (rxJava2Observable == null) {
            rxJava2Observable = new RxCompressObservable();
        }
        return rxJava2Observable;
    }

    @Override
    protected void subscribeActual(Observer<? super CompressedData> observer) {
        this.observer = observer;
        if (lastData != null) {
            this.observer.onNext(lastData);
            lastData = null;
        }
    }


    public void publishData(CompressedData data) {
        if (observer != null && data != null) {
            if (data.isError()) {
                observer.onError(new Throwable(data.getMessage()));
            } else {
                observer.onNext(data);
            }
            observer.onComplete();
        } else {
            this.lastData = data;
        }
    }
}

