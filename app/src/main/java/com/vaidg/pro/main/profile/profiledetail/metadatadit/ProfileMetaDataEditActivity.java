package com.vaidg.pro.main.profile.profiledetail.metadatadit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.vaidg.pro.BuildConfig;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityProfileMetaDataEditBinding;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.model.DocumentSelectionModel;
import com.vaidg.pro.main.profile.profiledetail.metadatadit.model.ProfileMetaDataEditAdapter;
import com.vaidg.pro.pojo.profile.metaData.MetaDataArr;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.FileUtils;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.inject.Inject;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.main.profile.profiledetail.metadatadit.ProfileMetaDataEditPresenterImple.DOWNLOAD_FILE;
import static com.vaidg.pro.utility.VariableConstant.DATA;
import static com.vaidg.pro.utility.VariableConstant.DOWNLOAD_FILE_DIR;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_TIME_0_23_HRS;
import static com.vaidg.pro.utility.VariableConstant.ITEM_POSITION;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DATE_FUTURE;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DATE_PAST;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DOCUMENT;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_VIDEO_UPLOAD;

public class ProfileMetaDataEditActivity extends BaseDaggerActivity implements ProfileMetaDataEditContract.View, View.OnClickListener {

    @Inject
    ProfileMetaDataEditContract.Presenter presenter;

    @Inject
    ProfileMetaDataEditAdapter profileMetaDataEditAdapter;

    @Inject
    App_permission app_permission;


    private ActivityProfileMetaDataEditBinding binding;

    private int itemPosition;
    private MetaDataArr metaDataArr;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProfileMetaDataEditBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
        initRv();
    }

    @Override
    public void initViews() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.updatingProfile));
        progressDialog.setCancelable(false);

        if (getIntent() != null && getIntent().hasExtra(ITEM_POSITION) && getIntent().hasExtra(DATA)) {
            itemPosition = getIntent().getIntExtra(ITEM_POSITION, -1);
            metaDataArr = getIntent().getParcelableExtra(DATA);
        } else {
            onBackPressed();
        }

        if (itemPosition == -1 || metaDataArr == null) {
            onBackPressed();
        }

        binding.includeToolbar.tvTitle.setText(metaDataArr.getName());
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        if (metaDataArr.getEditableOnProvider() && !Utility.isTextEmpty(metaDataArr.getType()) && Integer.parseInt(metaDataArr.getType()) != SPECIALIZATION_VIEW_DOCUMENT) {
            binding.includeToolbar.btnDone.setText(R.string.edit);
            binding.includeToolbar.btnDone.setOnClickListener(this);
            binding.includeToolbar.btnDone.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void initRv() {
        binding.rvMetaData.setLayoutManager(new LinearLayoutManager(this));
        binding.rvMetaData.setAdapter(profileMetaDataEditAdapter);
        profileMetaDataEditAdapter.setOnItemClickListener((ProfileMetaDataEditAdapter.OnItemClickListener) presenter);
        presenter.addMetaData(metaDataArr);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnDone) {
            if (binding.includeToolbar.btnDone.getText().equals(getString(R.string.edit))) {
                binding.includeToolbar.btnDone.setText(R.string.save);
                profileMetaDataEditAdapter.setEditMode(true);
            } else {
                presenter.isValid();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboad(this);
        finishAfterTransition();
    }

    @Override
    public void notifyAdapter() {
        profileMetaDataEditAdapter.notifyDataSetChanged();
    }

    @Override
    public void displayTimePicker() {
        final Calendar c = Calendar.getInstance();
        c.setTimeZone(getInstance().getTimeZone());
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                (view, hourOfDay, minute) -> {
                    c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    c.set(Calendar.MINUTE, minute);
                    Time time = new Time(c.getTimeInMillis());
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT_TIME_0_23_HRS, Locale.US);
                    simpleDateFormat.setTimeZone(getInstance().getTimeZone());
                    presenter.setData(simpleDateFormat.format(time));
                }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false);
        timePickerDialog.show();
    }

    @Override
    public void displayDatePicker(int type) {
        final Calendar c = Calendar.getInstance();
        c.setTimeZone(getInstance().getTimeZone());
        long currentTimeStamp = new GregorianCalendar(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH))
                .getTimeInMillis();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    presenter.setData(String.format(Locale.US, "%02d-%02d-%02d", year, (monthOfYear + 1), dayOfMonth));
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
        DatePicker dp = datePickerDialog.getDatePicker();
        switch (type) {
            case SPECIALIZATION_VIEW_DATE_FUTURE:
            case DocumentSelectionModel
                    .DATE_TIME_CURRENT_TO_FUTURE:
                dp.setMinDate(currentTimeStamp);
                break;
            case SPECIALIZATION_VIEW_DATE_PAST:
            case DocumentSelectionModel
                    .DATE_TIME_PAST_TO_CURRENT:
                dp.setMaxDate(currentTimeStamp);
                break;
        }
    }

    @Override
    public void openCamera(Uri uri, int mediaType) {
        Intent intent;
        if (!Utility.isTextEmpty(metaDataArr.getType()) && Integer.parseInt(metaDataArr.getType()) == SPECIALIZATION_VIEW_VIDEO_UPLOAD)
            intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        else
            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, VariableConstant.CAMERA_CODE);
    }

    @Override
    public void openGallery(int mediaType) {
        String chooseTitle = "";
        Intent intent = new Intent();
        if (!Utility.isTextEmpty(metaDataArr.getType()) && Integer.parseInt(metaDataArr.getType()) == SPECIALIZATION_VIEW_VIDEO_UPLOAD) {
            intent.setType("video/*");
            chooseTitle = "Select Video";
        } else {
            intent.setType("image/*");
            chooseTitle = "select Image";
        }
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, chooseTitle), VariableConstant.GALLERY_CODE);
    }

    @Override
    public void openDocPicker() {
        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
        String[] supportedTypes = {"application/msword",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "application/pdf",
                "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "image/*"};
        chooseFile.setType("*/*");
        chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, supportedTypes);
        startActivityForResult(chooseFile, VariableConstant.PICKFILE_RESULT_CODE);
    }

    @Override
    public void launchViewer(String filePath) {
        Utility.printLog(getClass().getSimpleName(), "Launch Path : " + filePath);
        if (filePath != null && filePath.startsWith("http") && (filePath.contains(".doc") || filePath.contains(".docx") ||
                filePath.contains(".ppt") || filePath.contains(".pptx") ||
                filePath.contains(".xls") || filePath.contains(".xlsx"))) {
            ArrayList<App_permission.Permission> permissions = new ArrayList<>();
            permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
            permissions.add(App_permission.Permission.WRITE_EXTERNAL_STORAGE);
            app_permission.getPermission(DOWNLOAD_FILE, permissions, (App_permission.Permission_Callback) presenter);
            return;
        }


        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        MimeTypeMap myMime = MimeTypeMap.getSingleton();
        String mimeType;
        if (filePath == null) {
            showError("No handler for this type of file.");
            return;
        }

        mimeType = myMime.getMimeTypeFromExtension(FileUtils.getExtension(filePath));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri apkURI = filePath.startsWith("http") ? Uri.parse(filePath) : FileProvider.getUriForFile(getApplicationContext(), BuildConfig.APPLICATION_ID + ".provider", new File(filePath));
            intent.setDataAndType(apkURI, mimeType);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            intent.setDataAndType(Uri.fromFile(new File(filePath)), mimeType);
        }

        try {
            startActivity(intent);
//            startActivity(Intent.createChooser(intent, "Open With"));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            showError("No handler for this type of file.");
        }
    }

    @Override
    public void downloadFile(String filePath) {
        new DownloadFileFromUrl().execute(filePath);
    }

    @SuppressLint("StaticFieldLeak")
    class DownloadFileFromUrl extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage(getString(R.string.msgDownloadingFile));
            Utility.progressDialogShow(ProfileMetaDataEditActivity.this, progressDialog);
        }

        @Override
        protected String doInBackground(String... strings) {
            int count;
            try {
                URL url = new URL(strings[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = connection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);


                File downloadFolder = new File(Environment
                        .getExternalStorageDirectory() + "/"
                        + DOWNLOAD_FILE_DIR);

                if (!downloadFolder.exists())
                    //noinspection ResultOfMethodCallIgnored
                    downloadFolder.mkdirs();

                File downloadFile = new File(downloadFolder, url.getPath().substring(url.getPath().lastIndexOf("/") + 1));
                // Output stream
                OutputStream output = new FileOutputStream(downloadFile);

                byte[] data = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                return downloadFile.getPath();
            } catch (Exception e) {
                e.printStackTrace();
                Utility.printLog("Error: ", e.getMessage());
                return null;
            }
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            Utility.printLog(getClass().getSimpleName(), "Executed Post : " + file_url);
            Utility.progressDialogDismiss(ProfileMetaDataEditActivity.this, progressDialog);
            launchViewer(file_url);
        }
    }


    @Override
    public boolean isMandatoryForProvider() {
        return metaDataArr.getMandatoryForProvider();
    }

    @Override
    public void sendBackData() {
        Utility.hideKeyboad(this);
        presenter.saveData(metaDataArr);
        Utility.printLog(getClass().getSimpleName(), "Updated Meta DAta : " + new Gson().toJson(metaDataArr));
        Intent intent = new Intent();
        intent.putExtra(ITEM_POSITION, itemPosition);
        intent.putExtra(DATA, metaDataArr);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void showError() {
        showError(getString(R.string.serverError));
    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clParent, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void showProgress() {
        Utility.progressDialogShow(this, progressDialog);
    }

    @Override
    public void hideProgress() {
        Utility.progressDialogDismiss(this, progressDialog);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        presenter.onActivityResultParse(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}