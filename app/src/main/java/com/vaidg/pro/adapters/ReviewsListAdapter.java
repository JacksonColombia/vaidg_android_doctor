package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vaidg.pro.R;
import com.vaidg.pro.pojo.profile.review.Reviews;
import com.vaidg.pro.utility.CircleTransform;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_DD;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_SPACE_MMMM_YYYY;
import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * Created by murashid on 11-Sep-17.
 * <h1>ReviewsListAdapter</h1>
 * Reviews Recycler adapter for displaying the list of review in ReviewActivity under ProfileFragment
 *
 * @see com.vaidg.pro.main.profile.review.ReviewActivity
 */

public class ReviewsListAdapter extends RecyclerView.Adapter<ReviewsListAdapter.ViewHolder> {

    private Typeface fontBold, fontMedium, fontReqular;
    private Context context;
    private ArrayList<Reviews> reviewses;
    private long currentTimeMills;

    public ReviewsListAdapter(Context context, ArrayList<Reviews> reviewses) {
        this.context = context;
        this.reviewses = reviewses;
        fontBold = Utility.getFontBold(context);
        fontMedium = Utility.getFontMedium(context);
        fontReqular = Utility.getFontRegular(context);
        currentTimeMills = System.currentTimeMillis();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_review, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvCustomerName.setText(reviewses.get(position).getReviewBy());
        holder.tvTime.setText(reviewses.get(position).getReviewAt());
        holder.tvReview.setText(reviewses.get(position).getReview());
        if (!reviewses.get(position).getProfilePic().equals("")) {
            Glide.with(context).setDefaultRequestOptions(new RequestOptions().transform(new CircleTransform(context))
                    .error(R.drawable.profile_default_image))
                    .load(reviewses.get(position).getProfilePic())
                    .into(holder.ivCutomer);
        }
        if (!reviewses.get(position).getRating().equals("")) {
            holder.ratingStar.setRating(Float.parseFloat(reviewses.get(position).getRating()));
        }
        holder.tvTime.setText(getDisplayTime(reviewses.get(position).getReviewAt()));
    }

    @Override
    public int getItemCount() {
        return reviewses != null ? reviewses.size() : ZERO;
    }

    private String getDisplayTime(String reviewedDate) {
        long minusTimeMillis = currentTimeMills - Utility.convertUTCToTimeStamp(reviewedDate);
        long timeInSeconds = minusTimeMillis / 1000;
        if (timeInSeconds < 60) {
            return timeInSeconds + " " + context.getString(R.string.secondsAgo);
        } else if (timeInSeconds < 3600) {
            return (int) (timeInSeconds * 0.0166667) + " " + context.getString(R.string.minutesAgo);
        } else if (timeInSeconds < 86400) {
            return (int) (timeInSeconds * 0.000277778) + " " + context.getString(R.string.hoursAgo);
        } else if (timeInSeconds < 31557600) {
            double days = timeInSeconds * 0.0000115741;
            if (days < 7) {
                return (int) days + " " + context.getString(R.string.daysAgo);
            } else if (days < 30) {
                return (int) (days * 0.142857) + " " + context.getString(R.string.weeksAgo);
            } else {
                return (int) (days * 0.0328549) + " " + context.getString(R.string.monthsAgo);
            }
        } else {
            try {
                String monthYear = Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(reviewedDate), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_SPACE_MMMM_YYYY);
                int date = Integer.parseInt(Utility.changeDateTimeFormat(Utility.convertUTCToServerFormat(reviewedDate), FORMAT_DASH_YYYY_MM_DD_TIME_0_12_HRS_SECONDS, FORMAT_DD));
                return Utility.getDayOfMonthSuffix(date, monthYear);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivCutomer;
        TextView tvCustomerName, tvTime, tvReview;
        RatingBar ratingStar;

        public ViewHolder(View itemView) {
            super(itemView);

            ivCutomer = itemView.findViewById(R.id.ivCutomer);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvReview = itemView.findViewById(R.id.tvReview);
            ratingStar = itemView.findViewById(R.id.ratingStar);

            tvCustomerName.setTypeface(fontReqular);
            tvTime.setTypeface(fontReqular);
            tvReview.setTypeface(fontReqular);
        }
    }
}
