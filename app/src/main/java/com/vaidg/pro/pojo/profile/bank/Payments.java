package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Payments implements Serializable {

	@SerializedName("statement_descriptor")
	@Expose
	private String statementDescriptor;

	@SerializedName("statement_descriptor_kanji")
	@Expose
	private Object statementDescriptorKanji;

	@SerializedName("statement_descriptor_kana")
	@Expose
	private Object statementDescriptorKana;

	public String getStatementDescriptor(){
		return statementDescriptor;
	}

	public Object getStatementDescriptorKanji(){
		return statementDescriptorKanji;
	}

	public Object getStatementDescriptorKana(){
		return statementDescriptorKana;
	}

	@Override
 	public String toString(){
		return 
			"Payments{" + 
			"statement_descriptor = '" + statementDescriptor + '\'' + 
			",statement_descriptor_kanji = '" + statementDescriptorKanji + '\'' + 
			",statement_descriptor_kana = '" + statementDescriptorKana + '\'' + 
			"}";
		}
}