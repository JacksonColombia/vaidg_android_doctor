package com.vaidg.pro.landing.newSignup.editEducation.selectdegree;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.landing.newSignup.editEducation.selectdegree.model.SelectDegreeAdapter;
import com.vaidg.pro.pojo.signup.DegreeData;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * <h1>@AddEditEducationUtil</h1>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 27/04/2020.
 **/
@Module
public class SelectDegreeUtil {

    @ActivityScoped
    @Provides
    ArrayList<DegreeData> provideDegreeData() {
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    SelectDegreeAdapter provideSelectDegreeAdapter(ArrayList<DegreeData> arrayList) {
        return new SelectDegreeAdapter(arrayList);
    }
}
