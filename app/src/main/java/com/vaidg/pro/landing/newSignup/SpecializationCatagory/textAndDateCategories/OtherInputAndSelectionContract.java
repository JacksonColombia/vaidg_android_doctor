package com.vaidg.pro.landing.newSignup.SpecializationCatagory.textAndDateCategories;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.pojo.Attribute;

/**
 * <h1>@OtherInputAndSelectionContract</h1>
 * <p>this interface b.wview and presenter
 * </P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public interface OtherInputAndSelectionContract {

    interface View {
        /**
         * <h2>@setPreferenceItem</h2>
         * <p>used to set the data from activity</p>
         *
         * @param item
         */
        void setPreferenceItem(Attribute item);

        /**
         * <p>This method is return boolean flag either it's mandatory or not.</p>
         * @return the true if madatory else false
         */
        boolean isMandatoryForProvider();

        /**
         * <h2>@showError</h2>
         *
         * @param string
         */
        void showError(String string);

        /**
         * <h2>@moveNextFrag</h2>
         */
        void moveNextFrag();
    }

    interface Presenter extends BasePresenter<View> {

        void valid(String enteredData);

    }
}
