package com.vaidg.pro.pojo.appconfig;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 26-Apr-18.
 */

public class WalletDataAppConfic implements Serializable {
    @SerializedName("enableWallet")
    @Expose
    private String enableWallet;
    @SerializedName("walletAmount")
    @Expose
    private String walletAmount;
    @SerializedName("softLimit")
    @Expose
    private String softLimit;
    @SerializedName("hardLimit")
    @Expose
    private String hardLimit;
    @SerializedName("reachedSoftLimit")
    @Expose
    private String reachedSoftLimit;
    @SerializedName("reachedHardLimit")
    @Expose
    private String reachedHardLimit;

    public String getEnableWallet() {
        return enableWallet;
    }

    public void setEnableWallet(String enableWallet) {
        this.enableWallet = enableWallet;
    }

    public String getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(String walletAmount) {
        this.walletAmount = walletAmount;
    }

    public String getSoftLimit() {
        return softLimit;
    }

    public void setSoftLimit(String softLimit) {
        this.softLimit = softLimit;
    }

    public String getHardLimit() {
        return hardLimit;
    }

    public void setHardLimit(String hardLimit) {
        this.hardLimit = hardLimit;
    }

    public String getReachedSoftLimit() {
        return reachedSoftLimit;
    }

    public void setReachedSoftLimit(String reachedSoftLimit) {
        this.reachedSoftLimit = reachedSoftLimit;
    }

    public String getReachedHardLimit() {
        return reachedHardLimit;
    }

    public void setReachedHardLimit(String reachedHardLimit) {
        this.reachedHardLimit = reachedHardLimit;
    }
}
