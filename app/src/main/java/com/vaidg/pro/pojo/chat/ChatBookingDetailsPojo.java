package com.vaidg.pro.pojo.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vaidg.pro.pojo.booking.Booking;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 31-May-18.
 */

public class ChatBookingDetailsPojo implements Serializable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Booking data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Booking getData() {
        return data;
    }

    public void setData(Booking data) {
        this.data = data;
    }
}
