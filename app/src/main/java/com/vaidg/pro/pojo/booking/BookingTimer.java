package com.vaidg.pro.pojo.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 07-Nov-17.
 */

public class BookingTimer implements Serializable {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("second")
    @Expose
    private String second;
    @SerializedName("startTimeStamp")
    @Expose
    private String startTimeStamp;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public String getStartTimeStamp() {
        return startTimeStamp;
    }

    public void setStartTimeStamp(String startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }
}
