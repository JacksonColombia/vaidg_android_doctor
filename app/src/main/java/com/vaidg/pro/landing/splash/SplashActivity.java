package com.vaidg.pro.landing.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.landing.introsilider2.IntroSliderNewActivity;
import com.vaidg.pro.main.MainActivity;
import com.vaidg.pro.utility.MixpanelEvents;
import com.vaidg.pro.utility.SessionManager;

import org.apache.http.util.TextUtils;

public class SplashActivity extends AppCompatActivity {

    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
    }

    /**
     * Initialize the Views
     * If doctor already login then open Main Activity after 3 seconds
     *
     * @see SplashActivity
     */
    private void init() {

        new Handler().postDelayed(() -> {
            sessionManager = SessionManager.getSessionManager(SplashActivity.this);
            if (TextUtils.isEmpty(sessionManager.getPushToken())) {
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> {

                    String refreshedToken = instanceIdResult.getToken();

                    // If you want to send messages to this application instance or
                    // manage this apps subscriptions on the server side, send the
                    // Instance ID token to your app server.
                    sessionManager.setPushToken(refreshedToken);
                });
            }
            if (!TextUtils.isEmpty(sessionManager.getFCMTopic())) {
                FirebaseMessaging.getInstance().subscribeToTopic(sessionManager.getFCMTopic());
            }

            if (sessionManager.getIsDriverLogin()) {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.stay, R.anim.fade_open);
                AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.AppOpenAfterLogin.value);
            } else {
                Intent intent = new Intent(SplashActivity.this, IntroSliderNewActivity.class);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.stay, R.anim.fade_open);
                AppController.getInstance().getMixpanelHelper().commonTrackBeforeLogin(MixpanelEvents.AppOpenBeforeLogin.value);
            }
        }, 3000);
    }
}
