package com.vaidg.pro.main.booking;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.vaidg.pro.AppController;
import com.vaidg.pro.R;
import com.vaidg.pro.adapters.ViewPagerAdapter;
import com.vaidg.pro.main.MainActivity;
import com.vaidg.pro.main.notification.NotificationActivity;
import com.vaidg.pro.main.profile.wallet.WalletActivity;
import com.vaidg.pro.pojo.booking.Booking;
import com.vaidg.pro.pojo.booking.BookingData;
import com.vaidg.pro.pojo.booking.BookingPojo;
import com.vaidg.pro.service.LocationPublishService;
import com.vaidg.pro.service.OfflineLocationPublishService;
import com.vaidg.pro.utility.App_permission;
import com.vaidg.pro.utility.MixpanelEvents;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;
import dagger.android.support.DaggerFragment;
import java.util.Objects;
import javax.inject.Inject;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>BookingFragment</h1>
 * BookingFragment for showing Events Details
 */
public class BookingFragment extends DaggerFragment implements View.OnClickListener, BookingContract.View, BookingListFragment.BookingListFragmentInteraction, App_permission.Permission_Callback {

    private static final String TAG = "BookingFragment";
    private static final String LOC = "location";
    private Typeface fondBold, fontRegular;
    private LayoutInflater inflater;
    private TextView tvCountFirstTab, tvCountSecondTab;
    private TextView tvOnlineOffline, tvStatus;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    @Inject
    BookingContract.Presenter presenter;
    @Inject
    App_permission app_permission;
    @Inject
    Activity activity;
    private BookingListFragment offersFragment, activeBidFragment;
    private ViewPager vpBooking;

    private ArrayList<Booking> offersBookings, activeBidBooking;
    private int acceptedBookingSize = 0;

    private BroadcastReceiver receiver;
    private IntentFilter filter;
    private Gson gson;

    private long lastRefreshTime;

    private BookingFragmentInteraction mListener;

    private TextView tvNotificationCount, tvWalletBalance;
    private boolean isnewBooking = false;
    private Context mContext;

    public static BookingFragment newInstance() {
        return new BookingFragment();
    }

    public BookingFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        this.inflater = inflater;
        View rootView = inflater.inflate(R.layout.fragment_booking, container, false);

        init(rootView);

        return rootView;
    }


    /**
     * init  the views
     *
     * @param rootView parent View
     */
    private void init(View rootView) {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getSessionManager(mContext);

        offersBookings = new ArrayList<>();
        activeBidBooking = new ArrayList<>();
        fondBold = Utility.getFontBold(mContext);
        fontRegular = Utility.getFontRegular(mContext);

        filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_MAKE_ONLINE);
        filter.addAction(VariableConstant.INTENT_ACTION_MAKE_OFFLINE);
        filter.addAction(VariableConstant.INTENT_ACTION_PROFILE_ACTIVATION);
        double[] location = Utility.getLocation(mContext);
        sessionManager.setCurrentLat(String.valueOf(location[0]));
        sessionManager.setCurrentLng(String.valueOf(location[1]));
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //notificationManager.cancelAll();
                if (intent.getAction().equals(VariableConstant.INTENT_ACTION_REFRESH_BOOKING)) {
                    if (lastRefreshTime + 700 < System.currentTimeMillis()) {
                        AppController.getInstance().startNewBookingRingtoneService();
                        progressDialog.setMessage(getString(R.string.refreshing));
                        presenter.getBooking( false);
                        if (vpBooking != null) {
                            vpBooking.setCurrentItem(0);
                        }
                    }
                    lastRefreshTime = System.currentTimeMillis();

                    isnewBooking = true;
                } else if (intent.getAction().equals(VariableConstant.INTENT_ACTION_CANCEL_BOOKING)) {
                    String cancelid = intent.getStringExtra("cancelid");
                    String msg = intent.getStringExtra("msg");
                    String header = intent.getStringExtra("header");
                    for (Booking booking : offersBookings) {
                        if (booking.getBookingId().equals(cancelid)) {
                            if (lastRefreshTime + 2000 < System.currentTimeMillis()) {
                                progressDialog.setMessage(getString(R.string.refreshing));
                                presenter.getBooking( false);
                            }
                            lastRefreshTime = System.currentTimeMillis();
                            return;
                        }
                    }
                    for (Booking booking : activeBidBooking) {
                        if (booking.getBookingId().equals(cancelid)) {
                            if (lastRefreshTime + 2000 < System.currentTimeMillis()) {
                                progressDialog.setMessage(getString(R.string.refreshing));
                                presenter.getBooking( false);
                            }
                            lastRefreshTime = System.currentTimeMillis();
                            return;
                        }
                    }
                } else if (intent.getAction().equals(VariableConstant.INTENT_ACTION_MAKE_ONLINE) && !sessionManager.getIsDriverOnline()) {
                    progressDialog.setMessage(getString(R.string.goingOnline));
                    presenter.updateProviderStatues( VariableConstant.ONLINE_STATUS, sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
                } else if (intent.getAction().equals(VariableConstant.INTENT_ACTION_MAKE_OFFLINE)) {
                    setOnOffTheJob(false);
                } else if (intent.getAction().equals(VariableConstant.INTENT_ACTION_PROFILE_ACTIVATION)) {
//                    progressDialog.setMessage(getString(R.string.refreshing));
//                    presenter.getBooking( false);

                    Intent intentNotification = new Intent(mContext, NotificationActivity.class);
                    startActivity(intentNotification);
                    ((Activity)mContext).overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);

                    //tvNotificationCount.setVisibility(View.GONE);

                }
            }
        };


        Log.d(TAG, "sessionToken: " + AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        TabLayout tablayoutBooking = rootView.findViewById(R.id.tablayoutBooking);
        vpBooking = rootView.findViewById(R.id.vpBooking);

        offersFragment = BookingListFragment.newInstance("offers");
        offersFragment.setBookingListFragmentInteraction(this);
        viewPagerAdapter.addFragment(offersFragment, "");

        if (sessionManager.getIsBidBooking()) {
            activeBidFragment = BookingListFragment.newInstance("bid");
            activeBidFragment.setBookingListFragmentInteraction(this);
            viewPagerAdapter.addFragment(activeBidFragment, "");
        } else {
            tablayoutBooking.setVisibility(View.GONE);
        }
        vpBooking.setAdapter(viewPagerAdapter);
        tablayoutBooking.setupWithViewPager(vpBooking);
        for (int i = 0; i < tablayoutBooking.getTabCount(); i++) {
            tablayoutBooking.getTabAt(i).setCustomView(getCustomTabView(i));
        }

        tvOnlineOffline = rootView.findViewById(R.id.tvOnlineOffline);
        tvOnlineOffline.setTypeface(fondBold);
        tvOnlineOffline.setOnClickListener(this);

        tvStatus = rootView.findViewById(R.id.tvStatus);
        tvStatus.setTypeface(fondBold);

        tvNotificationCount = rootView.findViewById(R.id.tvNotificationCount);
        tvWalletBalance = rootView.findViewById(R.id.tvWalletBalance);

        rootView.findViewById(R.id.rlNotification).setOnClickListener(this);
        rootView.findViewById(R.id.flWallet).setOnClickListener(this);
        tvNotificationCount.setTypeface(fontRegular);
        tvWalletBalance.setTypeface(fontRegular);

        progressDialog.setMessage(getString(R.string.gettingBookings));
        gson = new Gson();


        /*if (sessionManager.getMyBooking().equals("")) {*/
        presenter.getBooking(
                false);
       /* } else {
            presenter.getBooking( true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onSuccesBooking(sessionManager.getMyBooking(), false);
                }
            }, 750);
        }*/

    }

    private View getCustomTabView(int i) {
        View view = inflater.inflate(R.layout.custom_tab_view_header_with_count, null, false);
        TextView tvHeader = view.findViewById(R.id.tvHeader);
        TextView tvCount = view.findViewById(R.id.tvCount);
        tvHeader.setTypeface(fondBold);
        tvCount.setTypeface(fontRegular);
        if (i == 0) {
            if (sessionManager.getIsBidBooking()) {
                tvHeader.setText(getString(R.string.offers));
            } else {
                tvHeader.setText(getString(R.string.requests));
            }
            tvCountFirstTab = tvCount;
        } else {
            if (sessionManager.getIsBidBooking()) {
                tvHeader.setText(getString(R.string.activeBids));
            } else {
                tvHeader.setText(getString(R.string.upcoming));
            }
            tvCountSecondTab = tvCount;
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mContext.registerReceiver(receiver, filter);

        if (VariableConstant.IS_BOOKING_ACCEPTED) {
            VariableConstant.IS_BOOKING_ACCEPTED = false;
            if (mListener != null) {
                mListener.setAcceptedBookingFragment();
            }
            return;
        }

        VariableConstant.IS_MYBOOKING_OPENED = true;

        if (VariableConstant.IS_BOOKING_UPDATED) {
            VariableConstant.IS_BOOKING_UPDATED = false;
            progressDialog.setMessage(getString(R.string.refreshing));
            presenter.getBooking( false);
        } else if (VariableConstant.IS_WALLET_UPDATED) {
            VariableConstant.IS_WALLET_UPDATED = false;
            progressDialog.setMessage(getString(R.string.refreshing));
            presenter.getBooking( false);
        }
        if (sessionManager.getIsProfileAcivated()) {
            enableOnlineOffline(true);
        } else if (!sessionManager.getIsProfileAcivated()) {
            enableOnlineOffline(false);
        } else if (sessionManager.getIsDriverDeactive()) {
            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_grey);
            tvOnlineOffline.setText(getString(R.string.deactivated));
        } else if (sessionManager.getIsDriverOnline()) {
            tvOnlineOffline.setText(getResources().getString(R.string.goOffline));
            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_green);
        }


     /*   new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sessionManager.setLastBooking("");
            }
        }, 10000);*/

    }

    @Override
    public void onPause() {
        super.onPause();
        AppController.getInstance().stopNewBookingRingtoneService();

        mContext.unregisterReceiver(receiver);
        VariableConstant.IS_MYBOOKING_OPENED = false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.attachView(this);
        this.mContext = context;
        if (context instanceof BookingFragmentInteraction) {
            mListener = (BookingFragmentInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        mContext = null;
        mListener = null;
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        presenter.detach();
        super.onDestroyView();
        if ( progressDialog!=null && progressDialog.isShowing() ){
            progressDialog.cancel();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvOnlineOffline:
                ArrayList<App_permission.Permission> permissions = new ArrayList<>();
                permissions.add(App_permission.Permission.LOCATION);
                app_permission.getPermission_for_Sup_v4Fragment(LOC, permissions, this, this);
                break;


            case R.id.rlNotification:
                Intent intentNotification = new Intent(mContext, NotificationActivity.class);
                startActivity(intentNotification);
                ((Activity)mContext).overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                tvNotificationCount.setVisibility(View.GONE);
                break;

            case R.id.flWallet:
                Intent intentWallet = new Intent(mContext, WalletActivity.class);
                intentWallet.putExtra("isFromBookingFrag", true);
                startActivity(intentWallet);
                ((Activity)mContext).overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                break;
        }
    }

    @Override
    public void startProgressBar() {
/*
        if (mContext != null && !((Activity)mContext).isFinishing() && progressDialog != null)
            progressDialog.show();
*/
    }

    @Override
    public void stopProgressBar() {
        if (activeBidFragment != null)
            activeBidFragment.stopRefreshing();
        if (offersFragment != null)
            offersFragment.stopRefreshing();
        if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.cancel();
            }
    }


    @Override
    public void sessionExpired(String msg) {
        if(msg != null && !msg.isEmpty())
            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, mContext);
    }

    @Override
    public void onFailure(String msg) {
        if(msg != null && !msg.isEmpty())
            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        //Toast.makeText(mContext,mContext.getString(R.string.serverError),Toast.LENGTH_SHORT).show();
        AppController.toast();
    }

    @Override
    public void onSuccesBooking(String result, boolean isFromApi) {

        try {
            BookingPojo bookingPojo = gson.fromJson(result, BookingPojo.class);
            BookingData bookingData = bookingPojo.getData();
            if (!sessionManager.getMyBooking().equals(result)) {
                sessionManager.setMyBooking(result);
            }
            if (!(sessionManager.getIsBidBooking() == bookingData.isBid())) {
                sessionManager.setIsBidBooking(bookingData.isBid());
                MainActivity mainActivity = (MainActivity) mContext;
                mainActivity.refreshBookingFragment(1);
            } else {
                if (isAdded()) {
                    offersBookings.clear();
                    if(bookingData.getRequest() != null && bookingData.getRequest().size() > 0) {
                        offersBookings.addAll(bookingData.getRequest());
                    }
                    if(offersFragment != null)
                    offersFragment.notifyiDataChanged(offersBookings);

                    activeBidBooking.clear();
                    if(bookingData.getActiveBid() != null && bookingData.getActiveBid().size() > 0) {
                        if (sessionManager.getIsBidBooking()) {
                            activeBidBooking.addAll(bookingData.getActiveBid());
                            tvCountFirstTab.setText("" + offersBookings.size());
                            tvCountSecondTab.setText("" + activeBidBooking.size());
                        }
                    }
                    if(activeBidFragment != null)
                        activeBidFragment.notifyiDataChanged(activeBidBooking);

                    if (bookingData.getNotificationUnreadCount() != 0) {
                        tvNotificationCount.setVisibility(View.VISIBLE);
                        tvNotificationCount.setText(String.valueOf(bookingData.getNotificationUnreadCount()));
                    } else {
                        tvNotificationCount.setVisibility(View.GONE);
                    }

                    if (bookingData.getWalletAmount() != 0) {
                        tvWalletBalance.setVisibility(View.VISIBLE);
                        String price = Double.toString(Utility.getFormattedDoublePrice(bookingData.getWalletAmount()));
                        sessionManager.setWalletAmount(price);
                       // price = price.substring(0, Math.min(price.length(), 9));
                        tvWalletBalance.setText(price);
                    } else {
                        tvWalletBalance.setVisibility(View.GONE);
                    }

                    if (bookingData.getAccepted()!= null && bookingData.getAccepted().size() > 0) {
                        sessionManager.setIsDriverOnJob(true);
                    } else {
                        sessionManager.setIsDriverOnJob(false);
                    }
                    if (bookingData.getAccepted()!= null && bookingData.getAccepted().size() > 0) {
                        acceptedBookingSize = bookingData.getAccepted().size();
                    }
                  /*  if (isFromApi && offersBookings.size() == 0 ) {
                        AppController.getInstance().stopNewBookingRingtoneService();
                    }*/

                    if (isFromApi) {
                        if (bookingData.getStatus()== 3) {
                            setOnOffTheJob(true);
                        } else {
                            setOnOffTheJob(false);
                        }

                        if (bookingData.getProfileStatus()== 1) {
                            sessionManager.setIsDriverDeactive(false);
                        } else {
                            sessionManager.setIsDriverDeactive(true);
                            setOnOffTheJob(false);
                            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_grey);
                            tvOnlineOffline.setText(getString(R.string.deactivated));
                        }

                        if (bookingData.getProfileActivationStatus() == 1) {
                            enableOnlineOffline(true);
                        } else {
                            enableOnlineOffline(false);
                        }

                    }

                    vpBooking.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (VariableConstant.IS_BID_BOOKING_ACCEPTED) {
                                VariableConstant.IS_BID_BOOKING_ACCEPTED = false;
                                vpBooking.setCurrentItem(1);
                            }
                        }
                    }, 350);

                    StringBuilder bookingStr = new StringBuilder();
                    String prefix = "";
                    for (Booking booking : bookingData.getAccepted()) {
                        bookingStr.append(prefix);
                        prefix = ",";
                        bookingStr.append(booking.getBookingId()).append("|").append(booking.getStatus());
                    }
                    sessionManager.setBookingStr(bookingStr.toString());
                }
            }

            if (isnewBooking || sessionManager.getIsNewBookingFromMain()) {
                isnewBooking = false;
                Booking lastBooking = new Booking();
                JSONObject jsonObjectBooking = new JSONObject(sessionManager.getLastBooking());
                lastBooking.setBookingId(jsonObjectBooking.getString("bookingId"));
                if (bookingData.getAccepted() != null && bookingData.getAccepted().size() > 0) {
                    ArrayList<Booking> accepted = bookingData.getAccepted();
                    if(lastBooking.getBookingId() != null  && !lastBooking.getBookingId().isEmpty()) {
                        int index = accepted.indexOf(lastBooking);
                        //get the calltype of the booking and if calltype 2 or 3 redirect to second tab
                        if (index >= 0 && accepted.get(index).getCallType().equals("3") || accepted.get(index).getCallType().equals("1")) {
                            sessionManager.setIncmgCallBookingType(jsonObjectBooking.getString("callType"));
                            MainActivity mainActivity = (MainActivity) mContext;
                            mainActivity.refreshBookingFragment(2);

                        }
                    }
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSuccessUpdateProviderStatus(String msg) {
        //Toast.makeText(mContext,msg,Toast.LENGTH_SHORT).show();
        setOnOffTheJob(!sessionManager.getIsDriverOnline());
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

    private void setOnOffTheJob(boolean b) {
        Log.d(TAG, "setOnOffTheJob: " + b);
        if (b) {
            sessionManager.setIsDriverOnline(true);
            tvOnlineOffline.setText(getResources().getString(R.string.goOffline));
            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_green);

            if (!Utility.isMyServiceRunning(mContext, LocationPublishService.class)) {
                Intent startIntent = new Intent(mContext, LocationPublishService.class);
                startIntent.setAction(VariableConstant.ACTION.STARTFOREGROUND_ACTION);
               /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    ContextCompat.startForegroundService(mContext,startIntent);
                } else {
                    mContext.startService(startIntent);
                }*/
                mContext.startService(startIntent);

            }

            if (!AppController.getInstance().getMqttHelper().isMqttConnected()) {
                AppController.getInstance().getMqttHelper().createMQttConnection(sessionManager.getProviderId(), false);
            }

            if (Utility.isMyServiceRunning(mContext, OfflineLocationPublishService.class)) {
                Intent stopIntent = new Intent(mContext, OfflineLocationPublishService.class);
                stopIntent.setAction(VariableConstant.ACTION.STOPOFFLINELOCATIONSERVICE);
/*                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    ContextCompat.startForegroundService(mContext,stopIntent);
                } else {
                    mContext.startService(stopIntent);
                }*/
                mContext.startService(stopIntent);

            }
        } else {
            sessionManager.setIsDriverOnline(false);
            tvOnlineOffline.setText(getResources().getString(R.string.goOnline));
            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_red);

            if (Utility.isMyServiceRunning(mContext, LocationPublishService.class)) {
                Intent stopIntent = new Intent(mContext, LocationPublishService.class);
                stopIntent.setAction(VariableConstant.ACTION.STOPFOREGROUND_ACTION);
               /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    ContextCompat.startForegroundService(mContext,stopIntent);
                } else {
                    mContext.startService(stopIntent);
                }*/
                mContext.startService(stopIntent);

            }

            if (acceptedBookingSize > 0) {
                if (!Utility.isMyServiceRunning(mContext, OfflineLocationPublishService.class)) {
                    Intent startIntent = new Intent(mContext, OfflineLocationPublishService.class);
                    startIntent.setAction(VariableConstant.ACTION.STARTOFFLINELOCATIONSERVICE);
                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        ContextCompat.startForegroundService(mContext,startIntent);
                    } else {
                        mContext.startService(startIntent);
                    }*/
                    mContext.startService(startIntent);

                }
                if (!AppController.getInstance().getMqttHelper().isMqttConnected()) {
                    AppController.getInstance().getMqttHelper().createMQttConnection(sessionManager.getProviderId(), false);
                }
            } else {
                if (AppController.getInstance().getMqttHelper().isMqttConnected()) {
                    AppController.getInstance().getMqttHelper().disconnect(/*sessionManager.getPhoneNumber()*/);
                }
                if (Utility.isMyServiceRunning(mContext, OfflineLocationPublishService.class)) {
                    Intent stopIntent = new Intent(mContext, OfflineLocationPublishService.class);
                    stopIntent.setAction(VariableConstant.ACTION.STOPOFFLINELOCATIONSERVICE);
                   /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        ContextCompat.startForegroundService(mContext,stopIntent);
                    } else {
                        mContext.startService(stopIntent);
                    }*/
                    mContext.startService(stopIntent);

                }
            }


        }
    }

    private void enableOnlineOffline(boolean b) {
        if (b) {
            sessionManager.setIsProfileAcivated(true);
            tvOnlineOffline.setOnClickListener(this);
            tvStatus.setVisibility(View.GONE);
        } else {
            sessionManager.setIsProfileAcivated(false);
            tvOnlineOffline.setOnClickListener(null);
            tvStatus.setText(getResources().getString(R.string.profileUnderReview));
            tvStatus.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onRefresh() {
        Log.d(TAG, "onRefresh: " + AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
        presenter.getBooking( true);
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        if (isAllGranted && tag.equals(LOC)) {
            if (sessionManager.getIsDriverOnline()) {
                progressDialog.setMessage(getString(R.string.goingOffline));
                presenter.updateProviderStatues( VariableConstant.OFFLINE_STATUS, sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
                AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.GoesOffline.value);
                AppController.getInstance().getMixpanelHelper().offline();
            } else {
                progressDialog.setMessage(getString(R.string.goingOnline));
                presenter.updateProviderStatues( VariableConstant.ONLINE_STATUS, sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
                AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.GoesOnline.value);
                AppController.getInstance().getMixpanelHelper().online();
            }
        }
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        if (tag.equals(LOC)) {
            app_permission.show_Alert_Permission(activity.getString(R.string.location_access_denied), activity.getString(R.string.location_denied_subtitle),
                activity.getString(R.string.gallery_acess_message), stringArray);
        }
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean permanent) {
        if (permanent) {
            if (tag.equals(LOC)) {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.location_access_denied), activity.getString(R.string.location_denied_subtitle),
                    activity.getString(R.string.gallery_denied_message));
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    public interface BookingFragmentInteraction {
        void setAcceptedBookingFragment();
    }
}
