package com.vaidg.pro.pojo.profile.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Address implements Serializable {

  @SerializedName("country")
  @Expose
  private String country;
  @SerializedName("city")
  @Expose
  private String city;
  @SerializedName("state")
	@Expose
  private String state;
  @SerializedName("postal_code")
	@Expose
  private String postalCode;
  @SerializedName("line2")
	@Expose
  private String line2;
  @SerializedName("line1")
	@Expose
  private String line1;

  public String getCountry() {
    return country;
  }

  public String getCity() {
    return city;
  }

  public String getState() {
    return state;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public String getLine2() {
    return line2;
  }

  public String getLine1() {
    return line1;
  }

  @Override
  public String toString() {
    return "Address{" + "country = '" + country + '\'' + ",city = '" + city + '\'' + ",state = '" + state + '\'' + ",postal_code = '" + postalCode + '\'' + ",line2 = '" + line2 + '\'' + ",line1 = '" + line1 + '\'' + "}";
  }
}