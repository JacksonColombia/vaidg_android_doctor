package com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.model;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ItemDocumentSelectionBinding;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.utility.Utility;

import java.util.List;

public class DocumentSelectionAdapter extends RecyclerView.Adapter<DocumentSelectionAdapter.DocumentSelectionViewHolder> {

    private Context context;
    private List<PreDefined> preDefineds;
    private boolean isEditMode;
    private long mLastClickTime = System.currentTimeMillis();
    private static final long CLICK_TIME_INTERVAL = 300;
    private OnDocumentSelectionItemClickListener onDocumentSelectionItemClickListener;

    public void setOnDocumentSelectionItemClickListener(OnDocumentSelectionItemClickListener onDocumentSelectionItemClickListener) {
        this.onDocumentSelectionItemClickListener = onDocumentSelectionItemClickListener;
    }

    public DocumentSelectionAdapter(List<PreDefined> preDefined) {
        this(preDefined, true);
    }

    public DocumentSelectionAdapter(List<PreDefined> preDefineds, boolean isEditMode) {
        this.preDefineds = preDefineds;
        this.isEditMode = isEditMode;
    }

    public void setEditMode(boolean editMode) {
        isEditMode = editMode;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DocumentSelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        ItemDocumentSelectionBinding uploadBinding = ItemDocumentSelectionBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new DocumentSelectionViewHolder(uploadBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DocumentSelectionViewHolder holder, int position) {
        Utility.printLog(getClass().getSimpleName(), "Data Pos : " + position + " : " + new Gson().toJson(preDefineds.get(position)));
        holder.binding.tvDocumentType.setHint(preDefineds.get(position).getName());
        switch (preDefineds.get(position).getType()) {
            case DocumentSelectionModel.TEXT_BOX:
                holder.binding.tlDocumentSelectionTextBox.setVisibility(View.VISIBLE);
                holder.binding.etDocumentSelectionTextBox.setEnabled(isEditMode);
                if (preDefineds.get(position).getData() != null && !preDefineds.get(position).getData().trim().isEmpty()) {
                    holder.binding.etDocumentSelectionTextBox.setText(preDefineds.get(position).getData());
                }

                holder.binding.etDocumentSelectionTextBox.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (s != null && !s.toString().isEmpty())
                            preDefineds.get(position).setData(s.toString());
                    }
                });

                holder.binding.etDocumentSelectionTextBox.setOnFocusChangeListener((v, hasFocus) -> {
                    if (!hasFocus)
                        holder.binding.etDocumentSelectionTextBox.requestFocus();
                });
                holder.binding.etDocumentSelectionTextBox.setOnKeyListener((v, keyCode, event) -> {
                    if (keyCode == EditorInfo.IME_ACTION_SEND)
                    {
                        holder.binding.etDocumentSelectionTextBox.clearFocus();
                        holder.binding.etDocumentSelectionTextBox.setFocusable(false);
                        holder.binding.etDocumentSelectionTextBox.setFocusableInTouchMode(false);
                        holder.binding.etDocumentSelectionTextBox.setCursorVisible(false);
                        return true;
                    }
                    return false;
                });
                break;
            case DocumentSelectionModel.DATE_TIME_CURRENT_TO_FUTURE:
            case DocumentSelectionModel.DATE_TIME_PAST_TO_CURRENT:
                holder.binding.etDocumentSelectionDateTime.setEnabled(isEditMode);
                if (preDefineds.get(position).getData() != null && !preDefineds.get(position).getData().trim().isEmpty()) {
                    holder.binding.etDocumentSelectionDateTime.setText(preDefineds.get(position).getData());
                }
                holder.binding.tlDocumentSelectionDateTime.setVisibility(View.VISIBLE);
                holder.binding.etDocumentSelectionDateTime.setText(preDefineds.get(position).getData() == null ? "" : preDefineds.get(position).getData());
                break;
            case DocumentSelectionModel.UPLOAD_DOCUMENT:
                boolean isDocSelected = preDefineds.get(position).getData() != null && !preDefineds.get(position).getData().isEmpty();
                if (isDocSelected)
                    holder.binding.btnDocumentName.setText(preDefineds.get(position).getData().substring(preDefineds.get(position).getData().lastIndexOf("/") + 1));
                holder.binding.btnDocumentName.setVisibility(isDocSelected ? View.VISIBLE : View.GONE);
                holder.binding.btnAddDeleteDocument.setText(isDocSelected ? R.string.delete : R.string.select);
                int tintColor = isDocSelected ? R.color.red : R.color.colorPrimary;
                Utility.setTextColor(context, holder.binding.btnAddDeleteDocument, tintColor);
                holder.binding.btnAddDeleteDocument.setStrokeColorResource(tintColor);
                holder.binding.btnAddDeleteDocument.setVisibility(isEditMode ? View.VISIBLE : View.INVISIBLE);
                break;
        }
    }


    @Override
    public int getItemCount() {
        return preDefineds == null ? 0 : preDefineds.size();
    }

    class DocumentSelectionViewHolder extends RecyclerView.ViewHolder {

        ItemDocumentSelectionBinding binding;

        DocumentSelectionViewHolder(@NonNull ItemDocumentSelectionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.btnAddDeleteDocument.setOnClickListener(this::onClick);
            binding.btnDocumentName.setOnClickListener(this::onClick);
            binding.etDocumentSelectionDateTime.setOnClickListener(this::onClick);
        }

        private void onClick(View view) {
            if (onDocumentSelectionItemClickListener != null) {
                long now = System.currentTimeMillis();
                if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
                    return;
                }
                mLastClickTime = now;
                if (view.getId() == R.id.btnDocumentName) {
                    onDocumentSelectionItemClickListener.onDocumentClick(getAdapterPosition(), preDefineds.get(getAdapterPosition()));
                } else {
                    onDocumentSelectionItemClickListener.onItemClick(getAdapterPosition(), preDefineds.get(getAdapterPosition()));
                }
            }
        }
    }

}
