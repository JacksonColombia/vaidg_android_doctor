package com.vaidg.pro.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QueForBookingByLang implements Parcelable {
    public static final Creator<QueForBookingByLang> CREATOR = new Creator<QueForBookingByLang>() {
        @Override
        public QueForBookingByLang createFromParcel(Parcel source) {
            return new QueForBookingByLang(source);
        }

        @Override
        public QueForBookingByLang[] newArray(int size) {
            return new QueForBookingByLang[size];
        }
    };
    @SerializedName("ar")
    @Expose
    private String ar;
    @SerializedName("PT")
    @Expose
    private String pT;
    @SerializedName("en")
    @Expose
    private String en;

    public QueForBookingByLang() {
    }

    protected QueForBookingByLang(Parcel in) {
        this.ar = in.readString();
        this.pT = in.readString();
        this.en = in.readString();
    }

    public String getAr() {
        return ar;
    }

    public void setAr(String ar) {
        this.ar = ar;
    }

    public String getPT() {
        return pT;
    }

    public void setPT(String pT) {
        this.pT = pT;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ar);
        dest.writeString(this.pT);
        dest.writeString(this.en);
    }
}
