package com.vaidg.pro.main.profile.bank.bankdetails;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.profile.bank.AccountRazorPayData;
import com.vaidg.pro.pojo.profile.bank.RazorPayData;
import com.vaidg.pro.pojo.profile.bank.StripeData;

import java.util.ArrayList;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface BankDetailsContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onFailure(String msg);

    void onFailure();

    void onSuccess(StripeData bankLists);

    void showAddStipe(String msg);

    void showAddRazorPay(String msg, boolean active);

    void showAddRazorPayList(String msg, boolean active);

    void onRazorPaySuccess(RazorPayData razorPayData);

    void onRazorPayList(ArrayList<AccountRazorPayData> accountRazorPayData);
  }

  interface Presenter extends BasePresenter {

    /**
     * mehthod for calling api for getting the bank details
     *
     */
    void getBankDetails();

    void fetchContactAndCache();

    void fetchFundAccountAndCache();
  }
}