package com.vaidg.pro.main.profile.profiledetail.metadatadit;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.Nullable;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.landing.newLogin.LogInPresenter;
import com.vaidg.pro.landing.newLogin.NewLogInActivity;
import com.vaidg.pro.landing.newSignup.model.OnFileUploadListener;
import com.vaidg.pro.pojo.profile.metaData.MetaDataArr;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import com.vaidg.pro.utility.VariableConstant;

import java.io.File;
import java.util.List;

import static com.vaidg.pro.utility.VariableConstant.AMAZON_BASE_URL;
import static com.vaidg.pro.utility.VariableConstant.BUCKET_NAME;

/**
 * <h1>ProfileMetaDataEditContract</h1>
 * <p>This contract define interface with required methods for view and presenter</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @see ProfileMetaDataEditActivity
 * @see ProfileMetaDataEditPresenterImple
 * @since 08/07/2020
 **/
public interface ProfileMetaDataEditContract {

    interface View extends BaseView {

        /**
         * <p>This method is used to initialize views</p>
         */
        void initViews();

        /**
         * <p>This method is initialize RecyclerView and set data into that.</p>
         */
        void initRv();

        /**
         * <p>this is used for notifying recycler view adapter</p>
         */
        void notifyAdapter();

        /**
         * <p>This method is open time picker dialog for select time.</p>
         */
        void displayTimePicker();

        /**
         * <p>This method is open date picker dialog for select future date or past date based on type.</p>
         *
         * @param type the type of date selection
         *             9 - future date
         *             10 - past date
         */
        void displayDatePicker(int type);

        /**
         * <p>This method is intent to camera for click picture.</p>
         *
         * @param uri       the uri to storage captured image.
         * @param mediaType the media type picture or video
         */
        void openCamera(Uri uri, int mediaType);

        /**
         * <p>This method is intent to gallery picker screen.</p>
         *
         * @param mediaType the media type picture or video
         */
        void openGallery(int mediaType);

        /**
         * <p>This method is intent to document picker.</p>
         */
        void openDocPicker();

        /**
         * <p>This method is launch selected document or image viewer.</p>
         *
         * @param filePath the selected document or image viewer path
         */
        void launchViewer(String filePath);

        /**
         * <p>This method download file and intent to open.</p>
         *
         * @param filePath the selected document or image viewer path
         */
        void downloadFile(String filePath);

        /**
         * <p>This method is return boolean flag either it's mandatory or not.</p>
         *
         * @return the true if madatory else false
         */
        boolean isMandatoryForProvider();

        /**
         * <p>This method is put data into return intent and send it.</p>
         */
        void sendBackData();

        /**
         * <p>This method is display passed error message in snack bar.</p>
         */
        void showError();

        /**
         * This method is display passed error message in snack bar.
         *
         * @param error the error message
         */
        void showError(String error);

    }

    interface Presenter extends BasePresenter {

        /**
         * <p>This method is add meta data into list and displaying it in recyclerview.</p>
         *
         * @param data the {@link MetaDataArr} objects
         */
        void addMetaData(MetaDataArr data);

        /**
         * <p>This method is set selected or entered data.</p>
         *
         * @param data the selected or entered data
         */
        void setData(String data);

        /**
         * <p>This method is provide option to select video or image from camera or a gallery.</p>
         */
        void openChooser();

        /**
         * <p>This method is delete data from the clicked position of object.</p>
         *
         * @param position the position of clicked item
         */
        void deleteDocument(int position);

        /**
         * <p>This method is ensure that data selected or entered valid.</p>>
         */
        boolean isValid();

        /**
         * <p>This method is provided update data of {@link MetaDataArr} object.</p>
         *
         * @param metaDataArr the data object of {@link MetaDataArr}
         */
        void saveData(MetaDataArr metaDataArr);

        /**
         * <p>This method is validate selected picture or video is valid and under size limit.</p>
         *
         * @return the boolean flag
         */
        boolean isValidMediaSize();

        /**
         * <p>This method is validate selected picture or video is valid and under size limit from provided file path..</p>
         *
         * @param filePath
         * @return
         */
        boolean isValidMediaSize(String filePath);

        /**
         * <p>This method is validate selected document size.</p>
         *
         * @param filePath the path of selected document file
         * @return the true or false based on validation
         */
        boolean isValidDocumentSize(String filePath);

        /**
         * <p>This method is update the media list with clicked picture.</p>
         */
        void upDateToGallery();

        /**
         * <p>This method is receive onActivityResult data to handle result data in presenter.</p>
         *
         * @param requestCode the intent request code
         * @param resultCode  the intent result code
         * @param data        the intent data
         */
        void onActivityResultParse(int requestCode, int resultCode, @Nullable Intent data);

        /**
         * <p>This method is compress provided image and then upload it to server.</p>
         *
         * @param filePath the local file path
         * @param callBack the listener for upload status
         */
        void compressImage(String filePath, UploadFileAmazonS3.UploadCallBack callBack);

        /**
         * <p>This method is compress provided video and then upload it to server.</p>
         *
         * @param file_path the local file path
         * @param callBack  the listener for upload status
         */
        void compressedVideo(String file_path, UploadFileAmazonS3.UploadCallBack callBack);

        /**
         * <p>This method that check file path is local and remote url and return boolean flag.</p>
         *
         * @param url the file path
         * @return truee if it's remote url either false
         */
        boolean isRemoteUrl(String url);

        /**
         * method for uploading image to amazon
         *
         * @param mFileTemp file which has to been upload in amazon
         * @param callBack  the call back with status of upload
         */
        void amazonUpload(final File mFileTemp, UploadFileAmazonS3.UploadCallBack callBack);

        /**
         * <p>This method is check that all media files are uploaded and if not that make first request for that.</p>
         */
        void isAllMediaUploaded();

        /**
         * <p>This method is prepare updated data for upload on server.</p>
         */
        void updateMetaData();

        /**
         * <p>This method is call api for upload meta data changes on server.</p>
         */
        void callUpdateMetaDataApi();
    }
}
