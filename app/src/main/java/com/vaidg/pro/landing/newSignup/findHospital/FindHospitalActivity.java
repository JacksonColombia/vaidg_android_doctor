package com.vaidg.pro.landing.newSignup.findHospital;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivityFindHospitalBinding;
import com.vaidg.pro.landing.newSignup.findHospital.model.FindHospitalAddressAdapter;
import com.vaidg.pro.landing.newSignup.findHospital.model.FindHospitalCallback;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import javax.inject.Inject;

/**
 * <h1>@FindHospitalActivity</h1>
 * <p>  this is activity for finding hospital </P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 25/04/2020.
 **/
public class FindHospitalActivity extends BaseDaggerActivity implements FindHospitalContract.View {

    private static final String ALL = "0";
    @Inject
    FindHospitalContract.Presenter presenter;

    @Inject
    FindHospitalAddressAdapter mAddressSearchAdapter;

    private ActivityFindHospitalBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFindHospitalBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
        presenter.findHospital(getIntent().getStringExtra("cityId"), ALL);
    }

    @Override
    public void initViews() {

        binding.includeToolbar.tvTitle.setText(getString(R.string.selectHospitalTitle));
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        itemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_lightest, null));
        binding.rvFindHosp.addItemDecoration(itemDecoration);
        binding.rvFindHosp.setAdapter(mAddressSearchAdapter);
        mAddressSearchAdapter.setCallBack((FindHospitalCallback) presenter);
        binding.etFindHospSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String place = s.toString();
                new Handler().postDelayed(() -> {
                    if (place.length() > 0)
                        presenter.findHospital(getIntent().getStringExtra("cityId"), place);
                    else
                        presenter.findHospital(getIntent().getStringExtra("cityId"), ALL);
                }, 2000);
            }
        });
    }

    @Override
    public void showProgress() {
        runOnUiThread(() -> {
            binding.pbLoader.setVisibility(View.VISIBLE);
        });
    }

    @Override
    public void hideProgress() {
        runOnUiThread(() -> {
            binding.pbLoader.setVisibility(View.GONE);
        });
    }

    @Override
    public void notifyAdapter() {
        mAddressSearchAdapter.notifyDataSetChanged();
    }

    @Override
    public void showError() {
        showError(getString(R.string.serverError));
    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clParent, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void sendBackData(Intent intent) {
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}


