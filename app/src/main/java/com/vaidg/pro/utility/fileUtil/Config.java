package com.vaidg.pro.utility.fileUtil;

import com.vaidg.pro.BuildConfig;

/**
 * <h2>Config</h2>
 * <p>
 *
 * </P>
 *
 * @author 3Embed.
 * @version 1.0.
 * @since 2/20/2018.
 */
public interface Config {
    String APP_FOLDER = BuildConfig.PARENT_FOLDER;
    String VIDEOS_DIR = "/Videos/";
    String IMAGE_DIR = "/Images/";
    String DOC_DIR = "/document/";
}
