package com.vaidg.pro.main.profile.wallet.cardlist;

import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;

import android.content.Context;
import com.appscrip.stripe.AccountsDelegate;
import com.appscrip.stripe.UserAccounts;
import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.profile.wallet.CardData;
import com.vaidg.pro.pojo.profile.wallet.CardPojo;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import javax.inject.Inject;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 28-Mar-18.
 */

public class ChangeListPresenter implements CardListContract.Presenter{
    private static final String TAG = "ChangeListPresenter";

    @Inject
    Context context;
    @Inject
    CardListContract.View view;
    @Inject
    NetworkService service;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;

    @Inject
    ChangeListPresenter() {
    }

    @Override
    public void getCardDetails() {
        if(view != null)
        view.startProgressBar();

        UserAccounts.INSTANCE.getCards(Utility.getAuth(sessionManager.getEmail()),DEFAULT_LANGUAGE, sessionManager.getProviderId(), new AccountsDelegate() {
            @Override
            public void onSuccess(@NotNull Object successData) {
                CardPojo card_pojo = new Gson().fromJson(successData.toString(), CardPojo.class);
                view.onSuccessCardDetails(card_pojo.getData());
            }

            @Override
            public void onFailure(@NotNull String failure) {
                if(view != null)
                view.startProgressBar();
            }
        });
    }



    @Override
    public  void selectCard(final JSONObject jsonObjectRequest) {
        if(view != null)
            view.startProgressBar();
        try {
            UserAccounts.INSTANCE.updateCard(Utility.getAuth(sessionManager.getEmail()),DEFAULT_LANGUAGE,sessionManager.getProviderId(), jsonObjectRequest.getString("cardId"),
                new AccountsDelegate() {
                    @Override
                    public void onSuccess(@NotNull Object successData) {
                        view.onSuccessSelectCard(successData.toString());
                        //   view.onSuccessDeleteCard(jsonObject.getString("message"));
                    }

                    @Override
                    public void onFailure(@NotNull String failure) {
                        if(view != null)
                            view.stopProgressBar();
                    }
                });
        } catch (JSONException e) {
            e.printStackTrace();
        }

/*
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.CARD, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObjectRequest
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        view.onSuccessSelectCard(jsonObject.getString("message"));
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                selectCard(jsonObjectRequest);
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(jsonObject.getString("message"));
                                        break;
                                    default:
                                        view.onFailure(jsonObject.getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        } catch (Exception e) {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
*/
    }


    @Override
    public  void deleteCard(final JSONObject jsonObjectRequest) {
       // view.startProgressBar();
        try {
            UserAccounts.INSTANCE.deleteCard(Utility.getAuth(sessionManager.getEmail()),DEFAULT_LANGUAGE, jsonObjectRequest.getString("cardId"),
                new AccountsDelegate() {
                    @Override
                    public void onSuccess(@NotNull Object successData) {
                        getCardDetails();
                     //   view.onSuccessDeleteCard(jsonObject.getString("message"));
                    }

                    @Override
                    public void onFailure(@NotNull String failure) {

                    }
                });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    interface View {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String failureMsg);

        void onFailure();

        void onSuccessCardDetails(ArrayList<CardData> data);

        void onSuccessSelectCard(String msg);

        void onSuccessDeleteCard(String msg);

        void onNewToken(String newToken);

        void sessionExpired(String msg);
    }
}
