package com.vaidg.pro.landing.newSignup.educationBackground;

import android.content.Intent;

import com.vaidg.pro.pojo.profile.DegreeDetailsData;

import java.util.ArrayList;

/**
 * <h1>@EducationDetailsContract</h1>
 * <p> this is interface b.w activity and presenter</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 27/04/2020.
 **/
public interface EducationDetailsContract {

    interface View {

        void notifyAdapter();

        void EditOnSelect(DegreeDetailsData positionData);

        void sendBackSaveData(Intent intent);
    }

    interface Presenter {

        void parseResult(int requestCode, int resultCode, Intent data);

        void addAllDataToList(ArrayList<DegreeDetailsData> data);

        void sendBackSaveData();
    }

}
