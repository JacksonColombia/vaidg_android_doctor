package com.vaidg.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.pojo.booking.CancelData;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import java.util.ArrayList;

/**
 * Created by murashid on 08-Nov-17.
 */

public class CancelListAdapter extends RecyclerView.Adapter<CancelListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CancelData> cancelData;
    private CancelSelected cancelSelected;
    private TextView tvTemp;
    private RadioButton rbCancel;
    private Typeface fontMedium, fontRegular;

    public CancelListAdapter(Context context, ArrayList<CancelData> cancelData,
                             CancelSelected cancelSelected) {
        this.context = context;
        this.cancelData = cancelData;
        this.cancelSelected = cancelSelected;
        fontRegular = Utility.getFontRegular(context);
        fontMedium = Utility.getFontMedium(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.single_cancel_row, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.tvCancelReason.setText(cancelData.get(position).getReason());
        if (cancelData.get(position).isChecked()) {
            tvTemp = holder.tvCancelReason;
            rbCancel = holder.rbCancel;
        }
        if (holder.sessionManager.getLanguageCode().equals(VariableConstant.LANGUAGE)) {
            holder.rbCancel.setChecked(cancelData.get(position).isChecked());
        } else {
            holder.rbCancel.setChecked(cancelData.get(position).isChecked());
        }
        holder.tvCancelReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelSelected.onCancelSelected(cancelData.get(holder.getAdapterPosition()).getRes_id());
                Utility.setTextColor(context, holder.tvCancelReason, R.color.colorPrimary);
                holder.tvCancelReason.setTypeface(fontMedium);
                holder.rbCancel.setChecked(true);
                if (tvTemp != null && tvTemp != holder.tvCancelReason) {
                    Utility.setTextColor(context, tvTemp, R.color.darkTextColor);
                    rbCancel.setChecked(false);
                    tvTemp.setTypeface(fontRegular);
                }
                tvTemp = holder.tvCancelReason;
                rbCancel = holder.rbCancel;
            }
        });
    }

    @Override
    public int getItemCount() {
        return cancelData == null ? 0 : cancelData.size();
    }

    /**
     * <h1>SignupSelectionListener</h1>
     * Interface for selected the city callback
     */

    public interface CancelSelected {
        void onCancelSelected(String id);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCancelReason;
        RadioButton rbCancel;
        SessionManager sessionManager;

        ViewHolder(View itemView) {
            super(itemView);
            tvCancelReason = itemView.findViewById(R.id.tvCancelReason);
            rbCancel = itemView.findViewById(R.id.rbCancel);
            tvCancelReason.setTypeface(fontRegular);
            sessionManager = SessionManager.getSessionManager(context);
        }
    }
}