package com.vaidg.pro.pojo.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 10-Apr-18.
 */

public class ChatCutomerList implements Serializable {
    @SerializedName("bookingId")
    @Expose
    private String bookingId;
    @SerializedName("customerId")
    @Expose
    private String customerId;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("bookingRequestedFor")
    @Expose
    private String bookingRequestedFor;
    @SerializedName("bookingRequestedAt")
    @Expose
    private String bookingRequestedAt;
    @SerializedName("bookingExpireForProvider")
    @Expose
    private String bookingExpireForProvider;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("catName")
    @Expose
    private String catName;

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getBookingRequestedFor() {
        return bookingRequestedFor;
    }

    public void setBookingRequestedFor(String bookingRequestedFor) {
        this.bookingRequestedFor = bookingRequestedFor;
    }

    public String getBookingRequestedAt() {
        return bookingRequestedAt;
    }

    public void setBookingRequestedAt(String bookingRequestedAt) {
        this.bookingRequestedAt = bookingRequestedAt;
    }

    public String getBookingExpireForProvider() {
        return bookingExpireForProvider;
    }

    public void setBookingExpireForProvider(String bookingExpireForProvider) {
        this.bookingExpireForProvider = bookingExpireForProvider;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }
}
