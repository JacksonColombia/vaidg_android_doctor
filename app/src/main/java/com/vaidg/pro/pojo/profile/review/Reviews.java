package com.vaidg.pro.pojo.profile.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 14-Sep-17.
 */

public class Reviews implements Serializable {
    @SerializedName("review")
    @Expose
    private String review;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("reviewBy")
    @Expose
    private String reviewBy;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("reviewAt")
    @Expose
    private String reviewAt;

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReviewBy() {
        return reviewBy;
    }

    public void setReviewBy(String reviewBy) {
        this.reviewBy = reviewBy;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getReviewAt() {
        return reviewAt;
    }

    public void setReviewAt(String reviewAt) {
        this.reviewAt = reviewAt;
    }
}
