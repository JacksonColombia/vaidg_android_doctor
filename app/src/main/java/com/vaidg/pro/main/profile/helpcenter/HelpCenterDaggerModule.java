package com.vaidg.pro.main.profile.helpcenter;

import android.app.Activity;
import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.main.profile.editemailphone.EditEmailPhoneActivity;
import com.vaidg.pro.main.profile.editemailphone.EditEmailPhoneContract;
import com.vaidg.pro.main.profile.editemailphone.EditEmailPhonePresenterImple;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>EditEmailPhoneDaggerModule</h1>
 * <p>This class is collection of object that bind with dagger.</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @since 01/07/2020
 **/
@Module
public abstract class HelpCenterDaggerModule {

    @ActivityScoped
    @Binds
    abstract HelpCenterContract.Presenter providePresenter(HelpCenterPresenter helpCenterPresenter);

    @ActivityScoped
    @Binds
    abstract HelpCenterContract.View provideView(HelpCenterActivity helpCenterActivity);

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(HelpCenterActivity activity);

}
