package com.vaidg.pro.pojo.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vaidg.pro.pojo.history.Accounting;
import com.vaidg.pro.pojo.history.AdditionalService;
import com.vaidg.pro.pojo.history.CustomerData;
import com.vaidg.pro.pojo.history.ReviewByProvider;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 07-Nov-17.
 */

public class Booking implements Serializable {

    @SerializedName("bookingId")
    @Expose
    private String bookingId;
    @SerializedName("customerId")
    @Expose
    private String customerId;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("ageString")
    @Expose
    private String ageString;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("bookingRequestedFor")
    @Expose
    private String bookingRequestedFor;
    @SerializedName("bookingExpireTime")
    @Expose
    private String bookingExpireTime;
    @SerializedName("bookingRequestedAt")
    @Expose
    private String bookingRequestedAt;
    @SerializedName("bookingEndtime")
    @Expose
    private String bookingEndtime;
    @SerializedName("eventStartTime")
    @Expose
    private String eventStartTime;
    @SerializedName("bookingRequestedForProvider")
    @Expose
    private String bookingRequestedForProvider = "0";
    @SerializedName("bookingExpireForProvider")
    @Expose
    private String bookingExpireForProvider = "0";
    @SerializedName("serverTime")
    @Expose
    private String serverTime = "0";
    @SerializedName("currentTime")
    @Expose
    private long currentTime = 0;
    @SerializedName("addLine1")
    @Expose
    private String addLine1;
    @SerializedName("addLine2")
    @Expose
    private String addLine2;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("averageRating")
    @Expose
    private String averageRating;
    @SerializedName("typeofEvent")
    @Expose
    private String typeofEvent;
    //    @SerializedName("service")
//    @Expose
//        private GigTime service;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("paymentMethod")
    @Expose
    private String paymentMethod;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("bookingTimer")
    @Expose
    private BookingTimer bookingTimer;
    @SerializedName("pickupImages")
    @Expose
    private ArrayList<String> pickupImages;
    @SerializedName("dropImages")
    @Expose
    private ArrayList<String> dropImages;
    @SerializedName("pickupNotes")
    @Expose
    private String pickupNotes;
    @SerializedName("dropNotes")
    @Expose
    private String dropNotes;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("statusMsg")
    @Expose
    private String statusMsg;
    @SerializedName("signatureUrl")
    @Expose
    private String signatureUrl;
    @SerializedName("accounting")
    @Expose
    private Accounting accounting;
    @SerializedName("cartData")
    @Expose
    private ArrayList<ServiceItem> cartData;
    @SerializedName("questionAndAnswer")
    @Expose
    private /*ArrayList<QuestionAnswer>*/ String questionAndAnswer;
    @SerializedName("catName")
    @Expose
    private String catName = "";
    @SerializedName("category")
    @Expose
    private String category = "";
    @SerializedName("jobDescription")
    @Expose
    private String jobDescription = "";
    @SerializedName("serviceType")
    @Expose
    private String serviceType = "1"; // 1 => Fixed , 2 => Hourly
    @SerializedName("bookingType")
    @Expose
    private String bookingType;  // 1 => Now , 2 => Schedule , 3 => Repeat
    @SerializedName("bookingModel")
    @Expose
    private String bookingModel; // 1 => OnDemand , 2 => Marketplace , 3 => Bid
    @SerializedName("callType")
    @Expose
    private String callType; // 1 => InCall , 2 => OutCall , 3 => TeleCall
    @SerializedName("reminderId")
    @Expose
    private String reminderId;
    @SerializedName("quotedPrice")
    @Expose
    private String quotedPrice;
    @SerializedName("callButtonEnableForProvider")
    @Expose
    private String callButtonEnableForProvider;
    @SerializedName("scheduleTime")
    @Expose
    private String scheduleTime; // for bid and schdule booking
    @SerializedName("daysArr")
    @Expose
    private ArrayList<String> daysArr;
    @SerializedName("customerData")
    @Expose
    private CustomerData customerData; //This is for History and Schedule
    @SerializedName("reviewByProvider")
    @Expose
    private ReviewByProvider reviewByProvider;
    @SerializedName("additionalService")
    @Expose
    private ArrayList<AdditionalService> additionalService;
    @SerializedName("cancellationReason")
    @Expose
    private String cancellationReason;
    @SerializedName("dependentData")
    @Expose
    private DependentData dependentData;
    @SerializedName("pdfFile")
    @Expose
    private String pdfFile;

    public DependentData getDependentData() {
        return dependentData;
    }

    public void setDependentData(DependentData dependentData) {
        this.dependentData = dependentData;
    }

    public ArrayList<String> getPickupImages() {
        return pickupImages;
    }

    public void setPickupImages(ArrayList<String> pickupImages) {
        this.pickupImages = pickupImages;
    }

    public String getAge() {
        return age;
    }

    public String getAgeString() {
        return ageString;
    }

    public String getGender() {
        return gender;
    }

    public ArrayList<String> getDropImages() {
        return dropImages;
    }

    public void setDropImages(ArrayList<String> dropImages) {
        this.dropImages = dropImages;
    }

    public String getPickupNotes() {
        return pickupNotes;
    }

    public void setPickupNotes(String pickupNotes) {
        this.pickupNotes = pickupNotes;
    }

    public String getDropNotes() {
        return dropNotes;
    }

    public void setDropNotes(String dropNotes) {
        this.dropNotes = dropNotes;
    }

    public String getCallButtonEnableForProvider() {
        return callButtonEnableForProvider;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getBookingRequestedFor() {
        return bookingRequestedFor;
    }

    public void setBookingRequestedFor(String bookingRequestedFor) {
        this.bookingRequestedFor = bookingRequestedFor;
    }

    public String getBookingExpireTime() {
        return bookingExpireTime;
    }

    public void setBookingExpireTime(String bookingExpireTime) {
        this.bookingExpireTime = bookingExpireTime;
    }

    public String getBookingRequestedAt() {
        return bookingRequestedAt;
    }

    public void setBookingRequestedAt(String bookingRequestedAt) {
        this.bookingRequestedAt = bookingRequestedAt;
    }

    public String getBookingRequestedForProvider() {
        return bookingRequestedForProvider;
    }

    public void setBookingRequestedForProvider(String bookingRequestedForProvider) {
        this.bookingRequestedForProvider = bookingRequestedForProvider;
    }

    public String getBookingExpireForProvider() {
        return bookingExpireForProvider;
    }

    public void setBookingExpireForProvider(String bookingExpireForProvider) {
        this.bookingExpireForProvider = bookingExpireForProvider;
    }

    public String getAddLine1() {
        return addLine1;
    }

    public void setAddLine1(String addLine1) {
        this.addLine1 = addLine1;
    }

    public String getAddLine2() {
        return addLine2;
    }

    public void setAddLine2(String addLine2) {
        this.addLine2 = addLine2;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(String averageRating) {
        this.averageRating = averageRating;
    }

    public String getTypeofEvent() {
        return typeofEvent;
    }

    public void setTypeofEvent(String typeofEvent) {
        this.typeofEvent = typeofEvent;
    }


   /* public GigTime getService() {
        return service;
    }

    public void setService(GigTime service) {
        this.service = service;
    }*/

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BookingTimer getBookingTimer() {
        return bookingTimer;
    }

    public void setBookingTimer(BookingTimer bookingTimer) {
        this.bookingTimer = bookingTimer;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public String getSignatureUrl() {
        return signatureUrl;
    }

    public void setSignatureUrl(String signatureUrl) {
        this.signatureUrl = signatureUrl;
    }


    public String getBookingEndtime() {
        return bookingEndtime;
    }

    public void setBookingEndtime(String bookingEndtime) {
        this.bookingEndtime = bookingEndtime;
    }

    public String getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(String eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public Accounting getAccounting() {
        return accounting;
    }

    public void setAccounting(Accounting accounting) {
        this.accounting = accounting;
    }


    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getBookingModel() {
        return bookingModel;
    }

    public void setBookingModel(String bookingModel) {
        this.bookingModel = bookingModel;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public String getReminderId() {
        return reminderId;
    }

    public void setReminderId(String reminderId) {
        this.reminderId = reminderId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public ArrayList<ServiceItem> getCartData() {
        return cartData;
    }

    public void setCartData(ArrayList<ServiceItem> cartData) {
        this.cartData = cartData;
    }

    public /*ArrayList<QuestionAnswer>*/String getQuestionAndAnswer() {
        return questionAndAnswer;
    }

    public void setQuestionAndAnswer(/*ArrayList<QuestionAnswer>*/String questionAndAnswer) {
        this.questionAndAnswer = questionAndAnswer;
    }

    public CustomerData getCustomerData() {
        return customerData;
    }

    public void setCustomerData(CustomerData customerData) {
        this.customerData = customerData;
    }

    public ReviewByProvider getReviewByProvider() {
        return reviewByProvider;
    }

    public void setReviewByProvider(ReviewByProvider reviewByProvider) {
        this.reviewByProvider = reviewByProvider;
    }

    public ArrayList<AdditionalService> getAdditionalService() {
        return additionalService;
    }

    public void setAdditionalService(ArrayList<AdditionalService> additionalService) {
        this.additionalService = additionalService;
    }

    @Override
    public String toString() {
        return "{" +
                "bookingId='" + bookingId + '\'' +
                ", customerId='" + customerId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phone='" + phone + '\'' +
                ", profilePic='" + profilePic + '\'' +
                ", bookingRequestedFor='" + bookingRequestedFor + '\'' +
                ", bookingExpireTime='" + bookingExpireTime + '\'' +
                ", bookingRequestedAt='" + bookingRequestedAt + '\'' +
                ", bookingEndtime='" + bookingEndtime + '\'' +
                ", eventStartTime='" + eventStartTime + '\'' +
                ", bookingRequestedForProvider='" + bookingRequestedForProvider + '\'' +
                ", bookingExpireForProvider='" + bookingExpireForProvider + '\'' +
                ", serverTime='" + serverTime + '\'' +
                ", currentTime=" + currentTime +
                ", addLine1='" + addLine1 + '\'' +
                ", addLine2='" + addLine2 + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", averageRating='" + averageRating + '\'' +
                ", typeofEvent='" + typeofEvent + '\'' +
                ", distance='" + distance + '\'' +
                ", paymentMethod='" + paymentMethod + '\'' +
                ", status='" + status + '\'' +
                ", bookingTimer=" + bookingTimer +
                ", pickupImages=" + pickupImages +
                ", dropImages=" + dropImages +
                ", pickupNotes='" + pickupNotes + '\'' +
                ", dropNotes='" + dropNotes + '\'' +
                ", amount='" + amount + '\'' +
                ", statusMsg='" + statusMsg + '\'' +
                ", signatureUrl='" + signatureUrl + '\'' +
                ", accounting=" + accounting +
                ", cartData=" + cartData +
                ", questionAndAnswer=" + questionAndAnswer +
                ", catName='" + catName + '\'' +
                ", category='" + category + '\'' +
                ", jobDescription='" + jobDescription + '\'' +
                ", serviceType='" + serviceType + '\'' +
                ", bookingType='" + bookingType + '\'' +
                ", bookingModel='" + bookingModel + '\'' +
                ", callType='" + callType + '\'' +
                ", reminderId='" + reminderId + '\'' +
                ", quotedPrice='" + quotedPrice + '\'' +
                ", callButtonEnableForProvider='" + callButtonEnableForProvider + '\'' +
                ", scheduleTime='" + scheduleTime + '\'' +
                ", daysArr=" + daysArr +
                ", customerData=" + customerData +
                ", reviewByProvider=" + reviewByProvider +
                ", additionalService=" + additionalService +
                ", cancellationReason='" + cancellationReason + '\'' +
                '}';
    }

    public String getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public String getQuotedPrice() {
        return quotedPrice;
    }

    public void setQuotedPrice(String quotedPrice) {
        this.quotedPrice = quotedPrice;
    }

    public ArrayList<String> getDaysArr() {
        return daysArr;
    }

    public void setDaysArr(ArrayList<String> daysArr) {
        this.daysArr = daysArr;
    }

    public String getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    /*
     * Overriding the equals method for required data check*/
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Booking) {
            Booking temp = (Booking) obj;
            if (temp.getBookingId() != null) {
                return temp.getBookingId().trim().equals(this.getBookingId().trim());

            } else return false;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(bookingId);
    }

    public String getPdfFile() {
        return pdfFile;
    }

    public void setPdfFile(String pdfFile) {
        this.pdfFile = pdfFile;
    }
}
