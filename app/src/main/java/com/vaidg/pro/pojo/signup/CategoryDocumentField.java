package com.vaidg.pro.pojo.signup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 15-Nov-17.
 */

public class CategoryDocumentField implements Serializable {
    @SerializedName("fId")
    @Expose
    private String fId;
    @SerializedName("data")
    @Expose
    private String data;
    @SerializedName("fType")
    @Expose
    private String fType;
    @SerializedName("fName")
    @Expose
    private String fName;
    @SerializedName("isManadatory")
    @Expose
    private String isManadatory;

    public String getFid() {
        return fId;
    }

    public void setFid(String fid) {
        this.fId = fid;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getfType() {
        return fType;
    }

    public void setfType(String fType) {
        this.fType = fType;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getIsManadatory() {
        return isManadatory;
    }

    public void setIsManadatory(String isManadatory) {
        this.isManadatory = isManadatory;
    }
}
