package com.vaidg.pro.main.profile.editemailphone;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.phonevalidation.PhoneValidationPojo;

/**
 * <h1>EditEmailPhoneContract</h1>
 * <p>This contract define interface with required methods for view and presenter</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @see EditEmailPhoneActivity
 * @see EditEmailPhonePresenterImple
 * @since 01/07/2020
 **/
public interface EditEmailPhoneContract {

    interface View extends BaseView {

        /**
         * <p>This method is display provided error message when entered email is incorrect.</p>
         *
         * @param errorMessage the error message
         */
        void onEmailIncorrect(String errorMessage);

        /**
         * <p>This method is validate entered phone number and return boolean flag.</p>
         *
         * @return the true if correct
         */
        boolean isPhoneNumberCorrect();

        /**
         * <p>This method is display provided error message when entered phone number is incorrect.</p>
         *
         * @param errorMessage the error message
         */
        void onPhoneNumberIncorrect(String errorMessage);

        /**
         * <p>This method is provide {@link PhoneValidationPojo} success response object that received
         * from change phone number API.</p>
         *
         * @param phoneValidationPojo
         */
        void onSuccessPhoneNoChange(PhoneValidationPojo phoneValidationPojo);

        /**
         * <p>This method is provide success message that received from change email API.</p>
         *
         * @param msg
         */
        void onSuccessEmailChange(String msg);

        /**
         * <p>This method is display passed error message in snack bar.</p>
         *
         * @param error the error message
         */
        void showError(String error);

        /**
         * <p>This method is indicate API is failed.</p>
         */
        void onFailure();
    }

    interface Presenter extends BasePresenter {

        /**
         * <p>This method is check provided email address is valid or not and return boolean flag.</p>
         *
         * @param email the entered email
         * @return boolean true if valid either false
         */
        boolean isEmailAddressValid(String email);

        /**
         * <p>This method is check that provided country code is not black and valid phone number entered
         * according to that return boolean flag.</p>
         *
         * @param countryCode the selected country code
         * @param phoneNumber the entered phone number
         * @return boolean true if valid either false
         */
        boolean isPhoneNumberValid(String countryCode, String phoneNumber);

        /**
         * <p>This method is call API with provided email address to change it.</p>
         *
         * @param emailAddress the entered email address
         */
        void changeEmail(String emailAddress);

        /**
         * <p>This method is call API with provided country code and phone number to change it.</p>
         *
         * @param countryCode the selected country code
         * @param phoneNumber the entered phone number
         */
        void changePhoneNumber(String countryCode, String phoneNumber);
    }
}
