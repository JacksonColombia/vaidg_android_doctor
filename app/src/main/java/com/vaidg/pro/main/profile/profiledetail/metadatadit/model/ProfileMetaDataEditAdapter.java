package com.vaidg.pro.main.profile.profiledetail.metadatadit.model;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ItemMetaDataDateTimeSelectionBinding;
import com.vaidg.pro.databinding.ItemMetaDataNumberBinding;
import com.vaidg.pro.databinding.ItemMetaDataNumberSliderBinding;
import com.vaidg.pro.databinding.ItemMetaDataSelectionBinding;
import com.vaidg.pro.databinding.ItemMetaDataTextAreaBinding;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.model.DocumentSelectionAdapter;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.documentselection.model.OnDocumentSelectionItemClickListener;
import com.vaidg.pro.landing.newSignup.SpecializationCatagory.mediaSelection.model.MediaSelectAdapter;
import com.vaidg.pro.pojo.PreDefined;
import com.vaidg.pro.pojo.profile.metaData.MetaDataArr;
import com.vaidg.pro.utility.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.vaidg.pro.utility.VariableConstant.FORMAT_TIME_0_23_HRS;
import static com.vaidg.pro.utility.VariableConstant.FORMAT_TIME_1_12_HRS;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_CHECK_BOX;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DATE_FUTURE;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DATE_PAST;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DOCUMENT;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_DROPDOWN;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_FEE;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_NUMBER;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_NUMBER_SLIDER;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_PICTURE_UPLOAD;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_RADIO_BUTTON;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_TEXT_AREA;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_TEXT_AREA_COMMA_SEPARATED;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_TIME;
import static com.vaidg.pro.utility.VariableConstant.SPECIALIZATION_VIEW_VIDEO_UPLOAD;

public class ProfileMetaDataEditAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<MetaDataArr> list;
    private OnItemClickListener onItemClickListener;
    private boolean isEditMode = false;
    private long mLastClickTime = System.currentTimeMillis();
    private static final long CLICK_TIME_INTERVAL = 300;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setEditMode(boolean editMode) {
        isEditMode = editMode;
        notifyDataSetChanged();
    }

    public ProfileMetaDataEditAdapter(List<MetaDataArr> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case SPECIALIZATION_VIEW_RADIO_BUTTON:
            case SPECIALIZATION_VIEW_CHECK_BOX:
            case SPECIALIZATION_VIEW_DROPDOWN:
            case SPECIALIZATION_VIEW_PICTURE_UPLOAD:
            case SPECIALIZATION_VIEW_VIDEO_UPLOAD:
            case SPECIALIZATION_VIEW_DOCUMENT:
                return new MetaDataSelectionViewHolder(ItemMetaDataSelectionBinding.inflate(layoutInflater, parent, false));
            case SPECIALIZATION_VIEW_FEE:
            case SPECIALIZATION_VIEW_NUMBER:
                return new MetaDataInputNumberViewHolder(ItemMetaDataNumberBinding.inflate(layoutInflater, parent, false));
            case SPECIALIZATION_VIEW_NUMBER_SLIDER:
                return new MetaDataNumberSliderViewHolder(ItemMetaDataNumberSliderBinding.inflate(layoutInflater, parent, false));
            case SPECIALIZATION_VIEW_TEXT_AREA:
            case SPECIALIZATION_VIEW_TEXT_AREA_COMMA_SEPARATED:
                return new MetaDataTextAreaViewHolder(ItemMetaDataTextAreaBinding.inflate(layoutInflater, parent, false));
            case SPECIALIZATION_VIEW_DATE_FUTURE:
            case SPECIALIZATION_VIEW_DATE_PAST:
            case SPECIALIZATION_VIEW_TIME:
                return new MetaDataDateTimeSelectionViewHolder(ItemMetaDataDateTimeSelectionBinding.inflate(layoutInflater, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case SPECIALIZATION_VIEW_RADIO_BUTTON:
            case SPECIALIZATION_VIEW_CHECK_BOX:
            case SPECIALIZATION_VIEW_DROPDOWN:
                onBindMetaDataSelectionViewHolder(holder, position);
                break;
            case SPECIALIZATION_VIEW_PICTURE_UPLOAD:
            case SPECIALIZATION_VIEW_VIDEO_UPLOAD:
                onBindMetaPictureVideoViewHolder(holder, position);
                break;
            case SPECIALIZATION_VIEW_DOCUMENT:
                onBindMetaDocumentViewHolder(holder, position);
                break;
            case SPECIALIZATION_VIEW_FEE:
            case SPECIALIZATION_VIEW_NUMBER:
                onBindMetaDataInputNumberViewHolder(holder, position);
                break;
            case SPECIALIZATION_VIEW_NUMBER_SLIDER:
                onBindMetaDataNumberSliderViewHolder(holder, position);
                break;
            case SPECIALIZATION_VIEW_TEXT_AREA:
            case SPECIALIZATION_VIEW_TEXT_AREA_COMMA_SEPARATED:
                onBindMetaDataTextAreaViewHolder(holder, position);
                break;
            case SPECIALIZATION_VIEW_DATE_FUTURE:
            case SPECIALIZATION_VIEW_DATE_PAST:
            case SPECIALIZATION_VIEW_TIME:
                onBindMetDataDateTimeSelectionViewHolder(holder, position);
        }
    }

    private void onBindMetaDataSelectionViewHolder(RecyclerView.ViewHolder holder, int position) {
        boolean isSingleSelection = !Utility.isTextEmpty(list.get(position).getType()) && Integer.parseInt(list.get(position).getType()) != SPECIALIZATION_VIEW_CHECK_BOX;
        MetaDataSelectionViewHolder metaDataSelectionViewHolder = (MetaDataSelectionViewHolder) holder;
        ProfileMetaDataSelectionAdapter profileMetaDataSelectionAdapter;

        List<PreDefined> preDefineds = new ArrayList<>();
        String[] selectedIds = list.get(position).getData().contains(",") ? list.get(position).getData().split(",") : new String[]{list.get(position).getData()};
        if (isEditMode) {
            for (PreDefined preDefined : list.get(position).getPreDefined()) {
                for (String predefinedId : selectedIds) {
                    if (preDefined.getId().equals(predefinedId)) {
                        preDefined.setSelected(true);
                        break;
                    }
                }
                preDefineds.add(preDefined);
            }
        } else {
            for (String predefineId : selectedIds) {
                for (PreDefined preDefined : list.get(position).getPreDefined()) {
                    if (preDefined.getId().equals(predefineId)) {
                        preDefineds.add(preDefined);
                        break;
                    }
                }
            }
        }
        if (metaDataSelectionViewHolder.binding.rvMetaDataSelection.getAdapter() == null) {
            metaDataSelectionViewHolder.binding.rvMetaDataSelection.setLayoutManager(new LinearLayoutManager(context));
            DividerItemDecoration itemDecoration = new DividerItemDecoration(context, LinearLayoutManager.VERTICAL);
            itemDecoration.setDrawable(context.getResources().getDrawable(R.drawable.divider_light, null));
            metaDataSelectionViewHolder.binding.rvMetaDataSelection.addItemDecoration(itemDecoration);
            profileMetaDataSelectionAdapter = new ProfileMetaDataSelectionAdapter(preDefineds, isSingleSelection);
            profileMetaDataSelectionAdapter.setEditMode(isEditMode);
            profileMetaDataSelectionAdapter.setOnItemClickListener((position1, preDefined) -> {
                list.get(position).getPreDefined().set(position1, preDefined);
                Utility.printLog(getClass().getSimpleName(), "Updated List : " + new Gson().toJson(list));
            });
            metaDataSelectionViewHolder.binding.rvMetaDataSelection.setAdapter(profileMetaDataSelectionAdapter);
        } else {
            profileMetaDataSelectionAdapter = (ProfileMetaDataSelectionAdapter) metaDataSelectionViewHolder.binding.rvMetaDataSelection.getAdapter();
            profileMetaDataSelectionAdapter.addData(preDefineds);
            profileMetaDataSelectionAdapter.setEditMode(isEditMode);
        }
    }

    private void onBindMetaPictureVideoViewHolder(RecyclerView.ViewHolder holder, int position) {
        MetaDataSelectionViewHolder metaDataSelectionViewHolder = (MetaDataSelectionViewHolder) holder;
        MediaSelectAdapter mediaSelectAdapter;
        metaDataSelectionViewHolder.binding.btnMediaSelectUpload.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        metaDataSelectionViewHolder.binding.rvMetaDataSelection.setPaddingRelative(0, context.getResources().getDimensionPixelSize(R.dimen.dp_10), 0, 0);
        String[] medias = list.get(position).getData().contains(",") ? list.get(position).getData().split(",") : new String[]{list.get(position).getData()};
        ArrayList<String> mediaList = new ArrayList<>(Arrays.asList(medias));
        if (metaDataSelectionViewHolder.binding.rvMetaDataSelection.getAdapter() == null) {
            metaDataSelectionViewHolder.binding.rvMetaDataSelection.setLayoutManager(new GridLayoutManager(context, 3));
            mediaSelectAdapter = new MediaSelectAdapter(Utility.isTextEmpty(list.get(position).getType()) ? -1 : Integer.parseInt(list.get(position).getType()), mediaList, isEditMode);
            mediaSelectAdapter.provideCallBack((childPosition, Type) -> {
                if (onItemClickListener != null) {
                    mediaSelectAdapter.deleteData(childPosition);
                    onItemClickListener.onChildMediaDelete(mediaSelectAdapter.getAllData());
                }
            });
            metaDataSelectionViewHolder.binding.rvMetaDataSelection.setAdapter(mediaSelectAdapter);
        } else {
            mediaSelectAdapter = (MediaSelectAdapter) metaDataSelectionViewHolder.binding.rvMetaDataSelection.getAdapter();
            mediaSelectAdapter.addData(mediaList);
            mediaSelectAdapter.setEditMode(isEditMode);
        }
    }

    private void onBindMetaDocumentViewHolder(RecyclerView.ViewHolder holder, int position) {
        MetaDataSelectionViewHolder metaDataSelectionViewHolder = (MetaDataSelectionViewHolder) holder;
        DocumentSelectionAdapter documentSelectionAdapter;
        if (metaDataSelectionViewHolder.binding.rvMetaDataSelection.getAdapter() == null) {
            metaDataSelectionViewHolder.binding.rvMetaDataSelection.setLayoutManager(new LinearLayoutManager(context));
            DividerItemDecoration itemDecoration = new DividerItemDecoration(context, LinearLayoutManager.VERTICAL);
            itemDecoration.setDrawable(context.getResources().getDrawable(R.drawable.divider_light, null));
            metaDataSelectionViewHolder.binding.rvMetaDataSelection.addItemDecoration(itemDecoration);
            documentSelectionAdapter = new DocumentSelectionAdapter(list.get(position).getPreDefined(), false);
            documentSelectionAdapter.setOnDocumentSelectionItemClickListener(new OnDocumentSelectionItemClickListener() {
                @Override
                public void onItemClick(int childPosition, PreDefined preDefined) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onChildItemClick(position, childPosition, preDefined);
                    }
                }

                @Override
                public void onDocumentClick(int childPosition, PreDefined preDefined) {
                    onItemClickListener.onChildDocumentClick(position, childPosition, preDefined);
                }
            });
            documentSelectionAdapter.setEditMode(isEditMode);
            metaDataSelectionViewHolder.binding.rvMetaDataSelection.setAdapter(documentSelectionAdapter);
        } else {
            documentSelectionAdapter = (DocumentSelectionAdapter) metaDataSelectionViewHolder.binding.rvMetaDataSelection.getAdapter();
            documentSelectionAdapter.setEditMode(isEditMode);
        }
    }


    private void onBindMetaDataInputNumberViewHolder(RecyclerView.ViewHolder holder, int position) {
        MetaDataInputNumberViewHolder viewHolder = (MetaDataInputNumberViewHolder) holder;
        viewHolder.binding.etMetaDataInputNumber.setEnabled(isEditMode);
        viewHolder.binding.etMetaDataInputNumber.setText((list.get(position).getData() != null && !list.get(position).getData().trim().isEmpty())
                ? list.get(position).getData() : "");
        if (isEditMode) {
            viewHolder.binding.etMetaDataInputNumber.setImeOptions(EditorInfo.IME_ACTION_DONE);
            viewHolder.binding.etMetaDataInputNumber.setRawInputType(InputType.TYPE_CLASS_NUMBER);
            viewHolder.binding.etMetaDataInputNumber.requestFocus();
            viewHolder.binding.etMetaDataInputNumber.setSelection(
                    viewHolder.binding.etMetaDataInputNumber.getText() != null ?
                            viewHolder.binding.etMetaDataInputNumber.getText().length() : 0);
            viewHolder.binding.etMetaDataInputNumber.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    list.get(position).setData(s.toString());
                }
            });
            Utility.showKeyBoard(context, viewHolder.binding.etMetaDataInputNumber);
        }
    }

    private void onBindMetaDataNumberSliderViewHolder(RecyclerView.ViewHolder holder, int position) {
        MetaDataNumberSliderViewHolder viewHolder = (MetaDataNumberSliderViewHolder) holder;
        viewHolder.binding.tvSelectedNumber.setText((list.get(position).getData() != null && !list.get(position).getData().trim().isEmpty())
                ? list.get(position).getData() : "0");
        viewHolder.binding.sbSlider.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        viewHolder.binding.tvStartNumber.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        viewHolder.binding.tvEndNumber.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        viewHolder.binding.tvNumberUnit.setText(list.get(position).getUnit());
        viewHolder.binding.tvStartNumber.setText(String.format("%s %s", list.get(position).getMinimum(), list.get(position).getUnit()));
        viewHolder.binding.tvEndNumber.setText(String.format("%s %s", list.get(position).getMaximum(), list.get(position).getUnit()));
        viewHolder.binding.sbSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                list.get(position).setData(String.valueOf(progress));
                viewHolder.binding.tvSelectedNumber.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void onBindMetaDataTextAreaViewHolder(RecyclerView.ViewHolder holder, int position) {
        MetaDataTextAreaViewHolder viewHolder = (MetaDataTextAreaViewHolder) holder;
        viewHolder.binding.etMetaDataTextArea.setEnabled(isEditMode);
        viewHolder.binding.etMetaDataTextArea.setText((list.get(position).getData() != null && !list.get(position).getData().trim().isEmpty())
                ? list.get(position).getData() : "");
        if (isEditMode) {
            viewHolder.binding.etMetaDataTextArea.requestFocus();
            viewHolder.binding.etMetaDataTextArea.setSelection(
                    viewHolder.binding.etMetaDataTextArea.getText() != null ?
                            viewHolder.binding.etMetaDataTextArea.getText().length() : 0);
            viewHolder.binding.etMetaDataTextArea.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!Utility.isTextEmpty(list.get(position).getType()) && Integer.parseInt(list.get(position).getType()) == SPECIALIZATION_VIEW_TEXT_AREA_COMMA_SEPARATED) {
                        String str = s.toString();
                        if (str.endsWith("\n") && !str.endsWith(",\n")) {
                            str = str.substring(0, s.length() - 1).concat(",\n");
                            viewHolder.binding.etMetaDataTextArea.setText(str);
                            viewHolder.binding.etMetaDataTextArea.setSelection(str.length());
                        }
                    }
                    list.get(position).setData(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
            Utility.showKeyBoard(context, viewHolder.binding.etMetaDataTextArea);
        }
    }

    private void onBindMetDataDateTimeSelectionViewHolder(RecyclerView.ViewHolder holder, int position) {
        MetaDataDateTimeSelectionViewHolder viewHolder = (MetaDataDateTimeSelectionViewHolder) holder;
        viewHolder.binding.etOtherInputDateTime.setEnabled(isEditMode);
        if (!Utility.isTextEmpty(list.get(position).getType()) && Integer.parseInt(list.get(position).getType()) == SPECIALIZATION_VIEW_TIME) {
            String time;
            if (list.get(position).getData() != null && !list.get(position).getData().trim().isEmpty()) {
                time = Utility.changeDateTimeFormat(list.get(position).getData(), FORMAT_TIME_0_23_HRS, FORMAT_TIME_1_12_HRS);
            } else {
                time = "";
            }
            viewHolder.binding.etOtherInputDateTime.setText(time);
        } else {
            viewHolder.binding.etOtherInputDateTime.setText((list.get(position).getData() != null && !list.get(position).getData().trim().isEmpty())
                    ? list.get(position).getData() : "");
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list == null ? -1 : Utility.isTextEmpty(list.get(position).getType()) ? -1 : Integer.parseInt(list.get(position).getType());
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    /**
     * <p>This view holder class is used to display list data.</p>
     */
    class MetaDataSelectionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemMetaDataSelectionBinding binding;

        public MetaDataSelectionViewHolder(ItemMetaDataSelectionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.btnMediaSelectUpload.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                long now = System.currentTimeMillis();
                if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
                    return;
                }
                mLastClickTime = now;
                onItemClickListener.onChildMediaAdd();
            }
        }
    }

    /**
     * <p>This view holder class is used to display and take input number.</p>
     */
    class MetaDataInputNumberViewHolder extends RecyclerView.ViewHolder {

        ItemMetaDataNumberBinding binding;

        public MetaDataInputNumberViewHolder(ItemMetaDataNumberBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    /**
     * <p>This view holder class is used to display selected number and change through number slider.</p>
     */
    class MetaDataNumberSliderViewHolder extends RecyclerView.ViewHolder {

        ItemMetaDataNumberSliderBinding binding;

        public MetaDataNumberSliderViewHolder(ItemMetaDataNumberSliderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    /**
     * <p>This view holder class is used to display text in text area and able to change it.</p>
     */
    class MetaDataTextAreaViewHolder extends RecyclerView.ViewHolder {

        ItemMetaDataTextAreaBinding binding;

        public MetaDataTextAreaViewHolder(ItemMetaDataTextAreaBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    /**
     * <p>This view holder class is used to display selected date or time value and able to change it.</p>
     */
    class MetaDataDateTimeSelectionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemMetaDataDateTimeSelectionBinding binding;

        public MetaDataDateTimeSelectionViewHolder(ItemMetaDataDateTimeSelectionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.etOtherInputDateTime.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                long now = System.currentTimeMillis();
                if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
                    return;
                }
                mLastClickTime = now;
                onItemClickListener.onItemClick(getAdapterPosition(), list.get(getAdapterPosition()));
            }
        }
    }

    public interface OnItemClickListener {

        void onItemClick(int position, MetaDataArr metaDataArr);

        void onChildItemClick(int position, int childPosition, PreDefined preDefined);

        void onChildDocumentClick(int position, int childPosition, PreDefined preDefined);

        void onChildMediaDelete(ArrayList<String> data);

        void onChildMediaAdd();
    }
}
