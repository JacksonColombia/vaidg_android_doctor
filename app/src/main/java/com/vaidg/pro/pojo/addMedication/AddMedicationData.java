package com.vaidg.pro.pojo.addMedication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

public class AddMedicationData implements Serializable {
  @SerializedName("units")
  @Expose
  public ArrayList<Unit> units;
  @SerializedName("frequency")
  @Expose
  public ArrayList<Frequency> frequency;
  @SerializedName("drugRoute")
  @Expose
  public ArrayList<DrugRoute> drugRoute;
  @SerializedName("direction")
  @Expose
  public ArrayList<Direction> direction;

  public ArrayList<Unit> getUnits() {
    return units;
  }

  public ArrayList<Frequency> getFrequency() {
    return frequency;
  }

  public ArrayList<DrugRoute> getDrugRoute() {
    return drugRoute;
  }

  public ArrayList<Direction> getDirection() {
    return direction;
  }
}