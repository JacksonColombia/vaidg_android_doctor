package com.vaidg.pro.main.profile.profiledetail.metadatadit;

import android.app.Activity;

import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.landing.newLogin.LogInContract;
import com.vaidg.pro.landing.newLogin.LogInPresenter;
import com.vaidg.pro.landing.newLogin.NewLogInActivity;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>ProfileMetaDataEditDaggerModule</h1>
 * <p>This dagger module created for ProfileMetaDataEditActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @see com.vaidg.pro.dagger.ActivityBindingModule
 * @since 02/06/2020
 **/
@Module
public abstract class ProfileMetaDataEditDaggerModule {
    @ActivityScoped
    @Binds
    abstract Activity getActivity(ProfileMetaDataEditActivity activity);

    @ActivityScoped
    @Binds
    abstract ProfileMetaDataEditContract.View getView(ProfileMetaDataEditActivity activity);

    @ActivityScoped
    @Binds
    abstract ProfileMetaDataEditContract.Presenter getPresenter(ProfileMetaDataEditPresenterImple presenterImple);
}
