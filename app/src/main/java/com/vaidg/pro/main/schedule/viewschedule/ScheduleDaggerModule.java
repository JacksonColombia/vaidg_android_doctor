package com.vaidg.pro.main.schedule.viewschedule;

import android.app.Activity;
import com.vaidg.pro.dagger.ActivityScoped;
import com.vaidg.pro.dagger.FragmentScoped;
import com.vaidg.pro.main.MainActivity;
import com.vaidg.pro.main.schedule.viewschedulelist.ScheduleViewListDaggerModule;
import com.vaidg.pro.main.schedule.viewschedulelist.ScheduleViewListFragment;
import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * <h1>ProfileDaggerModule</h1>
 * <p>This dagger module created for ProfileFragment to bind injected objects</p>
 *
 * @author 3embed.
 * @version 1.0.20.
 * @since 19/06/2020.
 **/
@Module
public abstract class ScheduleDaggerModule {

    @ActivityScoped
    @Binds
    abstract Activity getActivity(ScheduleViewActivity mainActivity);


    @ActivityScoped
    @Binds
    abstract ScheculeViewContract.Presenter providePresenter(ScheduleViewPresenter presenter);

    @ActivityScoped
    @Binds
    abstract ScheculeViewContract.View provideView(ScheduleViewActivity activity);

    @FragmentScoped
    @ContributesAndroidInjector(modules = { ScheduleViewListDaggerModule.class})
    abstract ScheduleViewListFragment scheduleViewListFragment();

}
