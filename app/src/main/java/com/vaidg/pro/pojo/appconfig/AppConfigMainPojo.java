package com.vaidg.pro.pojo.appconfig;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Created by murashid on 07-Nov-17.
 */

public class AppConfigMainPojo implements Serializable {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private AppConfigData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AppConfigData getData() {
        return data;
    }

    public void setData(AppConfigData data) {
        this.data = data;
    }
}
