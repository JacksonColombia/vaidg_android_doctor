package com.vaidg.pro.adapters;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.pojo.language.LanguageData;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * Created by murashid on 24-Apr-18.
 */

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.ViewHolder> implements View.OnClickListener {

    private ArrayList<LanguageData> languageData;
    private Typeface fontRegular;
    private OnLanguageSelectListener onLanguageSelectListener;

    public LanguageAdapter(ArrayList<LanguageData> languageData, Typeface fontRegular, OnLanguageSelectListener onLanguageSelectListener) {
        this.languageData = languageData;
        this.fontRegular = fontRegular;
        this.onLanguageSelectListener = onLanguageSelectListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_text_view_language, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvName.setText(languageData.get(position).getLan_name());
        holder.tvName.setOnClickListener(this);
        holder.tvName.setTag(holder);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvName) {
            int adapterPosition = ((SupportListAdapter.SupportDetailViewHolder) v.getTag()).getAdapterPosition();
            onLanguageSelectListener.onLanguageSelect(languageData.get(adapterPosition).getLan_name(), languageData.get(adapterPosition).getCode());
        }
    }

    @Override
    public int getItemCount() {
        return languageData != null ? languageData.size() : ZERO;
    }

    public interface OnLanguageSelectListener {
        void onLanguageSelect(String language, String code);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvName.setTypeface(fontRegular);
        }
    }
}
