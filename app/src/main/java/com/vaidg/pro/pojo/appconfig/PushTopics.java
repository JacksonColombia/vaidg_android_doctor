package com.vaidg.pro.pojo.appconfig;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 21-Dec-17.
 */

public class PushTopics implements Serializable {
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("allProvider")
    @Expose
    private String allProvider;
    @SerializedName("allCities")
    @Expose
    private String allCities;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAllProvider() {
        return allProvider;
    }

    public void setAllProvider(String allProvider) {
        this.allProvider = allProvider;
    }

    public String getAllCities() {
        return allCities;
    }

    public void setAllCities(String allCities) {
        this.allCities = allCities;
    }

}
