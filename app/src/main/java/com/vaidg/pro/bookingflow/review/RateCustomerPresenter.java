package com.vaidg.pro.bookingflow.review;

import static com.vaidg.pro.AppController.getInstance;
import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.RetrofitRefreshToken.onRefreshToken;
import static com.vaidg.pro.utility.VariableConstant.PLATFORM;
import static com.vaidg.pro.utility.VariableConstant.SESSION_EXPIRED;
import static com.vaidg.pro.utility.VariableConstant.SESSION_LOGOUT;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

import android.content.Context;
import com.google.gson.Gson;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.utility.RetrofitRefreshToken;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by murashid on 06-Nov-17.
 * <h1>RateCustomerPresenter</h1>
 * RateCustomerPresenter presenter for RateCustomerActivity
 *
 * @see RateCustomerActivity
 */
public class RateCustomerPresenter implements RateCustomerContract.Presenter {

  @Inject
  RateCustomerContract.View view;
  @Inject
  Gson gson;
  @Inject
  SessionManager sessionManager;
  @Inject
  Context context;
  @Inject
  NetworkService service;

  @Inject
  public RateCustomerPresenter() {
  }

  /**
   * get the required booking by its id and rate it
   */
  @Override
  public void getBookingById( String bookingId) {
    if(view != null)
    view.startProgressBar();
   // model.getBookingById(sessionToken, bookingId);
    service.getBookingId(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM,bookingId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  if(view != null)
                  view.stopProgressBar();
                  if (response != null && !response.isEmpty()) {
                    if(view != null)
                    view.onSuccess(Utility.getData(response), true, true);
                  }else {
                    if (view != null)
                      view.onFailure();
                  }
                  break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()),  sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      getBookingById(bookingId);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                      if(view != null) {
                        view.stopProgressBar();
                        view.onFailure();
                      }
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if (view != null) {
                        view.stopProgressBar();
                        view.sessionExpired(Utility.getMessage(msg));
                      }
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  if(view != null) {
                    view.stopProgressBar();
                    view.sessionExpired(Utility.getMessage(errorBody));
                  }
                  break;
                default:
                  if(view != null) {
                    view.stopProgressBar();
                    view.onFailure(Utility.getMessage(errorBody));
                  }
                  break;
              }
            } catch (Exception e) {
              if(view != null) {
                view.stopProgressBar();
                view.onFailure();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.stopProgressBar();
              view.onFailure();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

  /**
   * method for passing value from view to model
   *
   * @param jsonObject
   */
  @Override
  public void setCustomerRating(JSONObject jsonObject) {
    if(view != null)
    view.startProgressBar();
    try {
          /*  if(jsonObject.getString("review").equals(""))
            {
                modelImplement.onEmptyReview();
                return;
            }*/
    } catch (Exception e) {
      e.printStackTrace();
    }

    service.reviewAndRating(Utility.getAuth(sessionManager.getEmail()), DEFAULT_LANGUAGE, PLATFORM,jsonObject.toString())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int statusCode = responseBodyResponse.code();
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (statusCode) {
                case SUCCESS_RESPONSE:
                  if(view != null)
                  view.stopProgressBar();
                  if (response != null && !response.isEmpty()) {
                    if(view != null)
                    view.onSuccess(Utility.getData(response) , false, false);
                  }else {
                    if(view != null)
                    view.onFailure();
                  }
                  break;
                case SESSION_EXPIRED:
                  onRefreshToken(Utility.getAuth(sessionManager.getEmail()),  sessionManager.getRefreshAuthToken(), service, new RetrofitRefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if(view != null)
                        view.hideProgress();
                      getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(), sessionManager.getPassword(), newToken);
                      setCustomerRating(jsonObject);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                      if(view != null) {
                        view.stopProgressBar();
                        view.onFailure();
                      }
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if (view != null) {
                        view.stopProgressBar();
                        view.sessionExpired(Utility.getMessage(msg));
                      }
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  if(view != null) {
                    view.stopProgressBar();
                    view.sessionExpired(Utility.getMessage(errorBody));
                  }
                  break;
                default:
                  if(view != null) {
                    view.stopProgressBar();
                    view.onFailure(Utility.getMessage(errorBody));
                  }
                  break;
              }
            } catch (Exception e) {
              if(view != null) {
                view.stopProgressBar();
                view.onFailure();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.stopProgressBar();
              view.onFailure();
            }
          }

          @Override
          public void onComplete() {
          }
        });

  }


  @Override
  public void attachView(Object view) {
  }

  @Override
  public void detachView() {
  }


}
