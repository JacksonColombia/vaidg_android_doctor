package com.vaidg.pro.landing.signup;

import android.util.Log;
import android.util.Patterns;

import com.google.gson.Gson;
import com.vaidg.pro.pojo.signup.CategoriesData;
import com.vaidg.pro.pojo.signup.CategoriesPojo;
import com.vaidg.pro.pojo.signup.CityData;
import com.vaidg.pro.pojo.signup.CityPojo;
import com.vaidg.pro.pojo.signup.SignUpPojo;
import com.vaidg.pro.utility.OkHttp3ConnectionStatusCode;
import com.vaidg.pro.utility.ServiceUrl;
import com.vaidg.pro.utility.UploadFileAmazonS3;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.vaidg.pro.utility.VariableConstant.AMAZON_BASE_URL;
import static com.vaidg.pro.utility.VariableConstant.BUCKET_NAME;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>SignupModel</h1>
 * SignupModel model for SignupActivity
 *
 * @see SignupActivity
 */

public class SignupModel {
    private static final String TAG = "SignupModel";
    private String subCategoryNonSelected = "";
    private String categoryDocument = "";
    private SignupModelImple modelImplement;
    private Gson gson;

    SignupModel(SignupModelImple modelImplement) {
        this.modelImplement = modelImplement;
        gson = new Gson();
    }

    void setInter(SignupModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api to get the city list
     */
    void getCity() {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", ServiceUrl.CITY, OkHttp3ConnectionStatusCode.Request_type.GET,
                new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Utility.printLog(TAG, "onSuccess getCity: " + statusCode + "\n" + result);
                                CityPojo cityPojo = gson.fromJson(result, CityPojo.class);
                                if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                    modelImplement.onSuccessCity(cityPojo.getData());
                                } else {
                                    modelImplement.onFailure(cityPojo.getMessage());
                                }
                            } else {
                                modelImplement.onFailure();
                            }
                        } catch (Exception e) {
                            modelImplement.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        modelImplement.onFailure();
                    }
                });
    }

    /**
     * method for calling api for getting the services based on the city
     *
     * @param city city id
     */
    void getSeriviceCategories(String city) {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", ServiceUrl.SERVICE_CATEGORIES + "/" + city, OkHttp3ConnectionStatusCode.Request_type.GET,
                new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Utility.printLog(TAG, "ccess getSeriviceCategories: " + statusCode + "\n" + result);
                                CategoriesPojo categoriesPojo = gson.fromJson(result, CategoriesPojo.class);
                                if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                    modelImplement.onSuccessServiceCategories(categoriesPojo.getData());
                                } else {
                                    modelImplement.onFailureServiceCategories();
                                }
                            } else {
                                modelImplement.onFailureServiceCategories();
                            }
                        } catch (Exception e) {
                            modelImplement.onFailureServiceCategories();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        modelImplement.onFailureServiceCategories();
                    }
                });
    }


    /**
     * method for verifying the email is exist or not
     *
     * @param jsonObject requried field
     */
    void verifyEmail(JSONObject jsonObject) {

        modelImplement.startProgressBar();
        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", ServiceUrl.EMAIL_VALIDATION, OkHttp3ConnectionStatusCode.Request_type.POST,
                jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Utility.printLog(TAG, "onSuccess verifyEmail: " + statusCode + "\n" + result);
                                JSONObject jsonObject = new JSONObject(result);
                                if (!statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                    modelImplement.onFailureEmailValidation(jsonObject.getString("message"));
                                } else {
                                    modelImplement.onSuccessPhoneEmailValidation();
                                }
                            } else {
                                modelImplement.onFailure();
                            }
                        } catch (Exception e) {
                            modelImplement.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        modelImplement.onFailure();
                    }
                });
    }


    /**
     * method for calling api to verifyi the mobile number is exist or not
     *
     * @param jsonObject required field
     */
    void verifyPhone(JSONObject jsonObject) {
        modelImplement.startProgressBar();
        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", ServiceUrl.PHONE_VALIDATION, OkHttp3ConnectionStatusCode.Request_type.POST,
                jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Utility.printLog(TAG, "onSuccess verifyPhone: " + statusCode + "\n" + result);
                                JSONObject jsonObject = new JSONObject(result);
                                if (!statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                    modelImplement.onFailurePhoneValidation(jsonObject.getString("message"));
                                } else {
                                    modelImplement.onSuccessPhoneEmailValidation();
                                }
                            } else {
                                modelImplement.onFailure();
                            }
                        } catch (Exception e) {
                            modelImplement.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        modelImplement.onFailure();
                    }
                });
    }

    /**
     * method for verifying the referal code
     *
     * @param singupJsonObject requried field
     */
    void verifyReferalCodeAndSignup(final JSONObject singupJsonObject, JSONObject referalJsonObject) {
        try {
            if (singupJsonObject.getString("profilePic").equals("")) {
                modelImplement.onProfilePicError();
                return;
            } else if (singupJsonObject.getString("firstName").equals("")) {
                modelImplement.onUserNameError();
                return;
            } else if (singupJsonObject.getString("lastName").equals("")) {
                modelImplement.onUserLastNameError();
                return;
            } else if (singupJsonObject.getString("email").equals("")) {
                modelImplement.onEmailError();
                return;
            } else if (!Patterns.EMAIL_ADDRESS.matcher(singupJsonObject.getString("email")).matches()) {
                modelImplement.onInValidEmailError();
                return;
            } else if (singupJsonObject.getString("cityId").equals("")) {
                modelImplement.onCitySelectionError();
                return;
            } else if (!isValidPassword(singupJsonObject.getString("password"))) {
                modelImplement.onPasswordError();
                return;
            } else if (singupJsonObject.getString("mobile").equals("")) {
                modelImplement.onPhoneNumberError();
                return;
            } else if (singupJsonObject.getString("dob").equals("")) {
                modelImplement.onDobError();
                return;
            } else if (singupJsonObject.getString("gender").equals("")) {
                modelImplement.onGenderError();
                return;
            } else if (singupJsonObject.getString("addLine1").equals("")) {
                modelImplement.onLocationError();
                return;
            } else if (singupJsonObject.getString("catlist").equals("")) {
                modelImplement.onCategorySelectionError();
                return;
            } else if (!isAtleastOneSubCategorySelected()) {
                modelImplement.onSubCategorySelectionError(subCategoryNonSelected);
                return;
            } else if (!isMandatoryUploaded()) {
                modelImplement.onCategoryDocumentUploadError(categoryDocument);
                return;
            }

            modelImplement.onSuccesLocalValidation();

            if (referalJsonObject.getString("code").equals("")) {
                modelImplement.startProgressBar();
                signUp(singupJsonObject);
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        modelImplement.startProgressBar();
        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", ServiceUrl.REFERAL_CODE_VALIDATION, OkHttp3ConnectionStatusCode.Request_type.POST,
                referalJsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Utility.printLog(TAG, "onSuccess referral code: " + statusCode + "\n" + result);
                                JSONObject jsonObject = new JSONObject(result);
                                if (!statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                    modelImplement.onFailureReferalValidation(jsonObject.getString("message"));
                                } else {
                                    signUp(singupJsonObject);
                                }
                            } else {
                                modelImplement.onFailure();
                            }
                        } catch (Exception e) {
                            modelImplement.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        modelImplement.onFailure();
                    }
                });
    }

    private boolean isMandatoryUploaded() {

        for (Map.Entry m : VariableConstant.getHashMapIsMandatoryUploaded().entrySet()) {
            if (!(boolean) m.getValue()) {
                categoryDocument = (String) m.getKey();
                return false;
            }
        }
        return true;
    }

    private boolean isAtleastOneSubCategorySelected() {

        for (Map.Entry m : VariableConstant.getHashMapIsSubCategorySelected().entrySet()) {
            if (!(boolean) m.getValue()) {
                subCategoryNonSelected = (String) m.getKey();
                return false;
            }
        }
        return true;
    }


    public boolean isValidPassword(String pass) {
        String regex = "(.)*(\\d)(.)*";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pass);

        if (pass.length() < 7) {
            return false;
        } else if (pass.equals(pass.toLowerCase())) {
            return false;
        } else if (pass.equals(pass.toUpperCase())) {
            return false;
        } else if (!matcher.matches()) {
            return false;
        }
        return true;
    }

    /**
     * method for calling api for signup and check the local validation
     *
     * @param jsonObject required field
     */
    void signUp(JSONObject jsonObject) {
        Log.d(TAG, "signUp: " + jsonObject);
        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", ServiceUrl.SIGNUP, OkHttp3ConnectionStatusCode.Request_type.POST,
                jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Utility.printLog(TAG, "onSuccess signUp: " + statusCode + "\n" + result);
                                Gson gson = new Gson();
                                SignUpPojo signUpPojo = gson.fromJson(result, SignUpPojo.class);

                                if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                    modelImplement.onSuccessSignUp(signUpPojo);
                                } else {
                                    modelImplement.onFailure(signUpPojo.getMessage());
                                }
                            } else {
                                modelImplement.onFailure();
                            }
                        } catch (Exception e) {
                            modelImplement.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        modelImplement.onFailure();
                    }
                });
    }

    /**
     * method for uploading image to amazon
     *
     * @param amazonS3  object of the UploadFileAmazonS3
     * @param mFileTemp file which has to been upload in amazon
     */
    void amazonUpload(UploadFileAmazonS3 amazonS3, File mFileTemp) {
        String BUCKETSUBFOLDER = VariableConstant.PROFILE_PIC;
        final String imageUrl = "https://" + BUCKET_NAME + "." + AMAZON_BASE_URL
                + BUCKETSUBFOLDER + "/"
                + mFileTemp.getName();
        Log.d(TAG, "amzonUpload: " + imageUrl);
        modelImplement.onSuccessImageUpload(imageUrl);

        amazonS3.Upload_data(BUCKETSUBFOLDER , mFileTemp.getName(), mFileTemp, new UploadFileAmazonS3.UploadCallBack() {
            @Override
            public void sucess(String success) {
                Log.d(TAG, "sucess: " + success);
            }

            @Override
            public void error(String errormsg) {
                Log.d(TAG, "error: " + errormsg);
            }
        });
    }

    /**
     * SignupModelImple interface for presenter implementation
     */
    interface SignupModelImple {
        void onFailure(String failureMsg);

        void onFailure();

        void onSuccessSignUp(SignUpPojo signUpPojo);

        void onSuccessCity(ArrayList<CityData> cityDataArrayList);

        void onSuccessServiceCategories(ArrayList<CategoriesData> categoriesData);

        void onFailureServiceCategories();

        void onSuccessImageUpload(String imgUrl);

        void onFailurePhoneValidation(String msg);

        void onFailureEmailValidation(String msg);

        void onSuccessPhoneEmailValidation();

        void onFailureReferalValidation(String msg);

        void startProgressBar();

        void nonValidateField();

        void onProfilePicError();

        void onUserNameError();

        void onUserLastNameError();

        void onInValidEmailError();

        void onEmailError();

        void onCitySelectionError();

        void onCategorySelectionError();

        void onSubCategorySelectionError(String category);

        void onCategoryDocumentUploadError(String category);

        void onPasswordError();

        void onPhoneNumberError();

        void onDobError();

        void onGenderError();

        void onLocationError();

        void onSuccesLocalValidation();
    }
}
