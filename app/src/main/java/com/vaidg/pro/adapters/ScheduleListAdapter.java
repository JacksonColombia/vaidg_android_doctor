package com.vaidg.pro.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.pro.R;
import com.vaidg.pro.main.history.historyinvoice.HistoryInvoiceActivity;
import com.vaidg.pro.main.schedule.bookingschedule.BookingScheduleActivity;
import com.vaidg.pro.pojo.shedule.Slot;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.VariableConstant;

import java.util.ArrayList;

import static com.vaidg.pro.utility.VariableConstant.ZERO;

/**
 * Created by murashid on 28-Sep-17.
 * <h1>ScheduleListAdapter</h1>
 * Shedule Recycler view adapter for displaying the list of schedule in ScheduleViewData Frament
 *
 * @see com.vaidg.pro.main.schedule.ScheduleFragment
 */

public class ScheduleListAdapter extends RecyclerView.Adapter<ScheduleListAdapter.ViewHolder> implements View.OnClickListener {
    private ArrayList<Slot> slots;
    private Activity mActivity;
    private Typeface fontRegular, fontMedium;

    public ScheduleListAdapter(Activity context, ArrayList<Slot> slots) {
        this.slots = slots;
        this.mActivity = context;
        fontRegular = Utility.getFontRegular(context);
        fontMedium = Utility.getFontMedium(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mActivity).inflate(R.layout.single_row_schedule_calendar, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        try {
            holder.tvStartHour.setText(slots.get(position).getSlotHour());
            holder.tvStartPeriod.setText(slots.get(position).getSlotPeriod());
         //  holder.tvStartPeriod.setVisibility(DateFormat.is24HourFormat(mActivity) ? View.INVISIBLE : View.VISIBLE);
            if (slots.get(position).getCutomerName() != null) {
                holder.tvTo.setVisibility(View.VISIBLE);
                holder.llScheduleBookingEndTime.setVisibility(View.VISIBLE);
                Log.d(slots.get(position).getStatus(), "onBindViewHolder: ");
                if (slots.get(position).getStatus().equals(VariableConstant.JOB_TIMER_INCOMPLETE)
                        || slots.get(position).getStatus().equals(VariableConstant.JOB_COMPLETED_RAISE_INVOICE)) {
                    Utility.setBackgroundColor(mActivity, holder.llBooked, R.color.customRed);
                } else {
                    Utility.setBackgroundColor(mActivity, holder.llBooked, R.color.customGreen);
                }
                holder.tvBookingEndHour.setText(slots.get(position).getSlotEndHourBooking());
                holder.tvBookingEndPeriod.setText(slots.get(position).getSlodp_10eriodBooking());
                holder.tvEventCustomerName.setText(new StringBuilder().append(slots.get(position).getEvent()).append(" - ").append(slots.get(position).getCutomerName()));
                holder.tvBookingTime.setText(new StringBuilder().append("( ").append(slots.get(position).getBookedStartHour()).append(" - ").append(slots.get(position).getBookedEndHour()).append(" )"));
                holder.llSchedule.setOnClickListener(this);
                holder.llSchedule.setTag(holder);
            } else {
                holder.tvTo.setVisibility(View.GONE);
                holder.llScheduleBookingEndTime.setVisibility(View.GONE);
                Utility.setBackgroundColor(mActivity, holder.llBooked, R.color.white);
                holder.tvEventCustomerName.setText("");
                holder.tvBookingTime.setText("");

                holder.llSchedule.setOnClickListener(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return slots != null ? slots.size() : ZERO;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.llSchedule) {
            ViewHolder holder = (ViewHolder) view.getTag();
            int position = holder.getAdapterPosition();

            String bookingId = slots.get(position).getBookingId();
            Intent intent;
            if (slots.get(position).getStatus().equals(VariableConstant.JOB_COMPLETED_RAISE_INVOICE)) {
                intent = new Intent(mActivity, HistoryInvoiceActivity.class);
                intent.putExtra("isFromSchedule", true);
            } else {
                intent = new Intent(mActivity, BookingScheduleActivity.class);
            }

            intent.putExtra("bookingId", bookingId);
            mActivity.startActivity(intent);
            mActivity.overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvEventCustomerName, tvBookingTime, tvStartHour, tvStartPeriod, tvBookingEndHour, tvBookingEndPeriod, tvTo;
        LinearLayout llSchedule, llBooked, llScheduleBookingEndTime;

        ViewHolder(View itemView) {
            super(itemView);
            tvStartHour = itemView.findViewById(R.id.tvStartHour);
            tvStartPeriod = itemView.findViewById(R.id.tvStartPeriod);
            tvBookingEndHour = itemView.findViewById(R.id.tvBookingEndHour);
            tvBookingEndPeriod = itemView.findViewById(R.id.tvBookingEndPeriod);
            tvTo = itemView.findViewById(R.id.tvTo);
            tvEventCustomerName = itemView.findViewById(R.id.tvEventCustomerName);
            tvBookingTime = itemView.findViewById(R.id.tvBookingTime);
            llSchedule = itemView.findViewById(R.id.llSchedule);
            llScheduleBookingEndTime = itemView.findViewById(R.id.llScheduleBookingEndTime);
            llBooked = itemView.findViewById(R.id.llBooked);

            tvStartHour.setTypeface(fontRegular);
            tvStartPeriod.setTypeface(fontRegular);
            tvBookingEndHour.setTypeface(fontRegular);
            tvBookingEndPeriod.setTypeface(fontRegular);
            tvTo.setTypeface(fontRegular);
            tvEventCustomerName.setTypeface(fontMedium);
            tvBookingTime.setTypeface(fontMedium);
        }
    }

}
