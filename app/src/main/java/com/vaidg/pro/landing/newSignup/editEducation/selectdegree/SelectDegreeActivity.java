package com.vaidg.pro.landing.newSignup.editEducation.selectdegree;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.vaidg.pro.R;
import com.vaidg.pro.databinding.ActivitySelectDegreeBinding;
import com.vaidg.pro.landing.newSignup.editEducation.selectdegree.model.SelectDegreeAdapter;
import com.vaidg.pro.utility.Utility;
import com.vaidg.pro.utility.baseActivity.BaseDaggerActivity;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class SelectDegreeActivity extends BaseDaggerActivity implements SelectDegreeContract.View {

    @Inject
    SelectDegreeAdapter selectDegreeAdapter;

    @Inject
    SelectDegreeContract.Presenter presenter;

    private ActivitySelectDegreeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySelectDegreeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initViews();
        presenter.getDegree();
        initRv();
    }

    @Override
    public void initViews() {
        setSupportActionBar(binding.includeToolbar.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.includeToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        binding.includeToolbar.tvTitle.setText(getString(R.string.selectDegreeTitle));
    }

    @Override
    public void initRv() {
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        itemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_dark, null));
        binding.rvDegrees.addItemDecoration(itemDecoration);
        binding.rvDegrees.setAdapter(selectDegreeAdapter);
        selectDegreeAdapter.setCallBack(presenter);

    }

    @Override
    public void notifyAdapter() {
        selectDegreeAdapter.notifyDataSetChanged();
    }

    @Override
    public void showError() {
        showError(getString(R.string.serverError));
    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(binding.clSelectDegree, error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        Utility.setBackgroundColor(this, snackBarView, R.color.colorAccent);
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        Utility.setTextColor(this, textView, R.color.white);
        snackbar.show();
    }

    @Override
    public void finishActivity(Intent intent) {
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void showProgress() {
        runOnUiThread(() -> binding.pbLoader.setVisibility(View.VISIBLE));
    }

    @Override
    public void hideProgress() {
        runOnUiThread(() -> binding.pbLoader.setVisibility(View.GONE));
    }
}