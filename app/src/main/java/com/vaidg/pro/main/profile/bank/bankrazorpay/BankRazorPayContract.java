package com.vaidg.pro.main.profile.bank.bankrazorpay;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;

import org.json.JSONObject;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface BankRazorPayContract {
    interface View extends BaseView {
        void startProgressBar();

        void stopProgressBar();

        void onSuccess(String msg);

        void onFailure();

        void onFailure(String msg);

        void onErrorFirstName();

        void onErrorLastName();

        void onErrorEmail();

        void onErrorCity();

    }

    interface Presenter extends BasePresenter {

        /**
         * method for passing values from view to model
         *
         * @param jsonObject required field in json object
         */
        void addBankDetails(JSONObject jsonObject);

    }
}