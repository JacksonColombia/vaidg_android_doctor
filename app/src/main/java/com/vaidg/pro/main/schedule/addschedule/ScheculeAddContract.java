package com.vaidg.pro.main.schedule.addschedule;

import android.content.Context;
import android.widget.TimePicker;
import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface ScheculeAddContract {
  interface View extends BaseView {
    void startProgressBar();

    void stopProgressBar();

    void onFailure(String msg);

    void onFailure();

    void onSuccess(String msg);

    void onScheduleError();

    void onDaysError();

    void onDurationError();

    void onStartTimeError();

    void onEndTimeError();

    void onTimeSelectedError();

    void onJobTypeError();

    void onSlotDurationError();

    void onLocationError();

    void selectedMode(String mode);

    void selectedModeConstantDays(String mode, String days, ArrayList<String> sentingDays);

    void selectedDays(String days, ArrayList<String> sentingDays);

    void selectedDuration(String duration);

    void seletedStartTime(String displayTime, String sentingTime);

    void seletedEndTime(String displayTime, String sentingTime);
  }

  interface Presenter extends BasePresenter {
    void getServerTime(final JSONObject jsonObject);

    void addSchedule(final JSONObject jsonObject) ;

    void selectTime(final int timePickerDifference, String lastSelectedTime, Context applicationContext);

    void selectMode(int mode);

    void sortDays(ArrayList<String> days);

    void selectDuration(int selectedMonth, String startDay, String endDay);

    void setTimePickerInterval(TimePicker timePicker);
    /**
     * method for returin the number from time
     *
     * @param startTime start time
     * @param endTime,  end time
     * @return number
     */
    boolean isValidScheduleTime(String startTime, String endTime);
  }
}