package com.vaidg.pro.main.acceptedbooking;

import com.vaidg.pro.BasePresenter;
import com.vaidg.pro.BaseView;
import com.vaidg.pro.pojo.booking.Booking;
import java.util.ArrayList;

/**
 * <h1>@SignUpContract</h1>
 * <p>  this is interface between view and presenter class</P>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 23/04/2020.
 **/
public interface AcceptedBookingContract {

    interface View extends BaseView {

        void onFailure(String msg);

        void onFailure();

        void onSuccesBooking(ArrayList<Booking> bookingArrayList);

        void onNewToken(String token);

        void sessionExpired(String msg);

    }

    interface Presenter extends BasePresenter<View> {
        void getBookingByDate(final String startDate, final String endDate, final boolean isBackground);
    }
}