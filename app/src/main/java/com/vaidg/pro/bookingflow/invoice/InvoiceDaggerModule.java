package com.vaidg.pro.bookingflow.invoice;

import android.app.Activity;
import com.vaidg.pro.bookingflow.UpdateStatusContract;
import com.vaidg.pro.bookingflow.UpdateStatusPresenterImple;
import com.vaidg.pro.dagger.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>LogInDaggerModule</h1>
 * <p>This dagger module created for LogInActivity to bind injected objects</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
@Module
public abstract class InvoiceDaggerModule {
  @ActivityScoped
  @Binds
  abstract Activity getActivity(InvoiceActivity activity);

  @ActivityScoped
  @Binds
  abstract UpdateStatusContract.View getView(InvoiceActivity activity);

  @ActivityScoped
  @Binds
  abstract UpdateStatusContract.Presenter getPresenter(UpdateStatusPresenterImple presenterImple);
}
