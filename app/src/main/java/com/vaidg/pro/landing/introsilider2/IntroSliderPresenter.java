package com.vaidg.pro.landing.introsilider2;

import android.app.Activity;

import com.google.gson.Gson;
import com.vaidg.pro.R;
import com.vaidg.pro.landing.introsilider2.model.IntroSliderModel;
import com.vaidg.pro.network.NetworkService;
import com.vaidg.pro.pojo.introslider.SliderData;
import com.vaidg.pro.pojo.language.LanguagePojo;
import com.vaidg.pro.utility.SessionManager;
import com.vaidg.pro.utility.Utility;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.pro.pojo.appconfig.AppConfigData.DEFAULT_LANGUAGE;
import static com.vaidg.pro.utility.VariableConstant.SUCCESS_RESPONSE;

/**
 * <h1>IntroSliderPresenter</h1>
 * <p>This presenter is define handle activity interactions done by user.</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 **/
public class IntroSliderPresenter implements IntroSliderContract.Presenter {

    @Inject
    IntroSliderContract.View view;

    @Inject
    IntroSliderModel model;

    @Inject
    Activity activity;

    @Inject
    NetworkService service;

    @Inject
    Gson gson;

    @Inject
    SessionManager sessionManager;

    @Inject
    public IntroSliderPresenter() {
    }

    /**
     * This method is add objects of SliderData into adapter
     */
    @Override
    public void addSliderData() {
        model.addSliderToAdapter(new SliderData(R.drawable.intro_slider_img1, R.string.intro_slider_title_1, R.string.intro_slider_description_1));
        model.addSliderToAdapter(new SliderData(R.drawable.intro_slider_img2, R.string.intro_slider_title_2, R.string.intro_slider_description_2));
        model.addSliderToAdapter(new SliderData(R.drawable.intro_slider_img3, R.string.intro_slider_title_3, R.string.intro_slider_description_3));
    }

    /**
     * This method is used to change screen content on view pager page changed through it's updated position
     *
     * @param position int position of view pager item
     */
    @Override
    public void changeSliderData(int position) {
        view.setSliderContent(model.getSliderData(position));
    }

    @Override
    public void getLanguage() {
      sessionManager.setLanguageCode(DEFAULT_LANGUAGE);
        Utility.basicAuth(service).getLanguage(model.getLanguage(), model.getPlatform())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
              @Override
              public void onSubscribe(Disposable d) {
              }

              @Override
              public void onNext(Response<ResponseBody> value) {
                int statusCode = value.code();
                try {
                  String response = value.body() != null
                      ? value.body().string() : null;
                  String errorBody = value.errorBody() != null
                      ? value.errorBody().string() : null;
                  if (statusCode == SUCCESS_RESPONSE) {
                    LanguagePojo languagePojo = gson.fromJson(response, LanguagePojo.class);
                    sessionManager.setLanguageList(response);
                    view.onSuccess(languagePojo.getData());
                  }
                } catch (Exception e) {
                  e.printStackTrace();
                }
              }

              @Override
              public void onError(Throwable e) {
              }

              @Override
              public void onComplete() {
              }
            });
      }
}
