package com.vaidg.pro.landing.newSignup.SpecializationCatagory.listSelection.model;

/**
 * <h1>@ListSelectItemCallBack</h1>
 * <p>this is used for callback</p>
 *
 * @author hemanth.
 * @version 1.0.20.
 * @since 2/05/2020.
 **/
public interface ListSelectItemCallBack {

    /**
     * on selecting
     *
     * @param position
     */
    void onItemSelected(int position, boolean isSingleSelection);
}
